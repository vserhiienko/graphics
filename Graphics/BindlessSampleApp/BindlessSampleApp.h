#pragma once

#include <NV/NvLogs.h>
#include <NV/NvMath.h>
#include <NV/NvStopWatch.h>
#include <NvUI/NvTweakBar.h>
#include <NvGLUtils/NvImage.h>
#include <NvAppBase/NvSampleApp.h>
#include <NvGLUtils/NvGLSLProgram.h>
#include <NvAssetLoader/NvAssetLoader.h>
#include <NvAppBase/NvFramerateCounter.h>
#include <NvAppBase/NvInputTransformer.h>

#include "Mesh.h"

namespace nyx
{
  class BindlessApp
    : public NvSampleApp
  {
  public:
    static uint32_t const BuildingCountPerDim = 100;
    static uint32_t const BuildingCount = BuildingCountPerDim * BuildingCountPerDim;
    static uint32_t const SQRT_BUILDING_COUNT = BuildingCountPerDim;

  public:
    BindlessApp(NvPlatformContext* platform);
    ~BindlessApp();

    void initUI();
    void initRendering(void);
    void draw(void);
    void reshape(int32_t width, int32_t height);
    void updatePerMeshUniforms(float t);

    void configurationCallback(NvEGLConfiguration& config);

  private:

    struct TransformUniforms
    {
      nv::matrix4f ModelView;
      nv::matrix4f ModelViewProjection;
      int32_t      UseBindlessUniforms;
    };

    struct PerMeshUniforms
    {
      float r, g, b, a;
    };

    void createBuilding(Mesh& mesh, nv::vec3f pos, nv::vec3f dim);
    void createGround(Mesh& mesh, nv::vec3f pos, nv::vec3f dim);
    void randomColor(float &r, float &g, float &b);


    // Simple collection of meshes to render
    std::vector<Mesh>             m_meshes;

    // Shader stuff
    NvGLSLProgram*                m_shader;
    GLuint                        m_bindlessPerMeshUniformsPtrAttribLocation;

    // uniform buffer object (UBO) for tranform data
    GLuint                        m_transformUniforms;
    TransformUniforms             m_transformUniformsData;
    nv::matrix4f                  m_projectionMatrix;

    // uniform buffer object (UBO) for mesh param data
    GLuint                        m_perMeshUniforms;
    std::vector<PerMeshUniforms>  m_perMeshUniformsData;
    GLuint64EXT                   m_perMeshUniformsGPUPtr;

    // UI stuff
    NvUIValueText*                m_drawCallsPerSecondText;
    bool                          m_useBindlessUniforms;
    bool                          m_updateUniformsEveryFrame;
    bool                          m_usePerMeshUniforms;

    // Timing related stuff
    float                         m_t;
    float                         m_minimumFrameDeltaTime;


  };
}