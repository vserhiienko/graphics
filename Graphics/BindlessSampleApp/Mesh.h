#pragma once

#include <vector>
#include <algorithm>
#include <NvFoundation.h>
#include <NV/NvPlatformGL.h>

class Vertex
{
public:
  Vertex(
    float x, float y, float z, // position
    float r, float g, float b, float a = 1.0f // color
    );

  float                m_position[3];
  uint8_t              m_color[4];

  // heavy format attributes
  float                m_attrib0[4];
  float                m_attrib1[4];
  float                m_attrib2[4];
  float                m_attrib3[4];
  float                m_attrib4[4];
  float                m_attrib5[4];
  float                m_attrib6[4];

  static const int32_t PositionOffset = 0; // 12
  static const int32_t ColorOffset = 12;   // 4
  static const int32_t Attrib0Offset = 16; // 16
  static const int32_t Attrib1Offset = 32; // 16
  static const int32_t Attrib2Offset = 48; // 16
  static const int32_t Attrib3Offset = 64; // 16
  static const int32_t Attrib4Offset = 72; // 16
  static const int32_t Attrib5Offset = 96; // 16
  static const int32_t Attrib6Offset = 112; // 16
};

class Mesh
{
public:
  int32_t         m_vertexCount;            //!< Number of vertices in mesh
  int32_t         m_indexCount;             //!< Number of indices in mesh
  GLuint          m_vertexBuffer;           //!< vertex buffer object for vertices
  GLuint          m_indexBuffer;            //!< vertex buffer object for indices
  GLuint          m_paramsBuffer;           //!< uniform buffer object for params
  GLint           m_vertexBufferSize;
  GLint           m_indexBufferSize;
  GLuint64EXT     m_vertexBufferGPUPtr;     //!< GPU pointer to m_vertexBuffer data
  GLuint64EXT     m_indexBufferGPUPtr;      //!< GPU pointer to m_indexBuffer data

  static bool     m_enableVBUM;
  static bool     m_setVertexFormatOnEveryDrawCall;
  static bool     m_useHeavyVertexFormat;
  static uint32_t m_drawCallsPerState;

  Mesh(void);
  ~Mesh(void);


  // Sets up the vertex format state
  static void renderPrep();
  // Does the actual rendering of the mesh
  static void renderFinish();

  // Resets state related to the vertex format
  void render(void);

  // This method is called to update the vertex and index data for the mesh.
  // For GL_NV_vertex_buffer_unified_memory (VBUM), we ask the driver to give us
  // GPU pointers for the buffers. Later, when we render, we use these GPU pointers
  // directly. By using GPU pointers, the driver can avoid many system memory 
  // accesses which pollute the CPU caches and reduce performance.
  void update(
    std::vector<Vertex>& vertices, 
    std::vector<uint16_t>& indices
    );

private:

  // TODO:
  // should assert in copy constructor 
  // since it can incorrectly delete 
  // the OpenGL buffer objects on copy
  //Mesh(Mesh const&) = default;

};
