#pragma once

#include <iostream>
#include <memory>
#include <fovis.hpp>
#include <GaiaRawStreams.h>

namespace gaia
{
  class VisualOdometry
    : public gaia::IRawStreams::INotify
  {
  public:

    typedef std::unique_ptr<fovis::Rectification> RectificationUniquePointer;
    typedef std::unique_ptr<fovis::VisualOdometry> VisualOdometryCoreUniquePointer;

    class DepthImage
      : public fovis::DepthSource
    {
    public:
      typedef DepthImage *Pointer;
      typedef Eigen::Matrix<float, 3, Eigen::Dynamic> Rays;
      typedef std::unique_ptr<DepthImage> UniquePointer;

    public: /// fovis::DepthSource

      virtual bool haveXyz(
        int u, int v
        );
      virtual void getXyz(
        fovis::OdometryFrame * frame
        );
      virtual void refineXyz(
        fovis::FeatureMatch * matches, int num_matches,
        fovis::OdometryFrame * frame
        );
      virtual float getBaseline(
        ) const;

    public:

      DepthImage(VisualOdometry *);
      ~DepthImage();

      virtual void onFrameAcquired(IRawStreams *streams);
      virtual void onFrameAcquired(utils::DepthF32Frame const &frame);

    protected:

      int rgbToDepthIndex(float u, float v) const;
      bool getXyzInterp(fovis::KeypointData* kpdata);

      VisualOdometry *m_core;
      utils::DepthF32Frame const *m_depth32_frame_ref;

      int _rgb_width;
      int _rgb_height;
      int _depth_width;
      int _depth_height;

      Rays* _rays;
      float _fx_inv;
      float _fy_inv;
      float _x_scale;
      float _y_scale;
      float _neg_cx_div_fx;
      float _neg_cy_div_fy;

    public:

    };

  public:
    VisualOdometry(IRawStreams*);
    ~VisualOdometry();
    void onRawStreamsServiceInitialized(IRawStreams*);
    void onRawStreamsServiceReleased(IRawStreams*);

    virtual void onFrameAcquired(
      gaia::utils::Grayscale8Frame const&,
      gaia::utils::DepthF32Frame const&
      );
    virtual void onFrameAcquired(
      IRawStreams const*
      );

  protected:

    bool m_is_instance_valid;

    IRawStreams *m_streams;
    RectificationUniquePointer m_rect;
    fovis::VisualOdometryOptions m_vis_odom_opts;
    fovis::CameraIntrinsicsParameters m_color_camera_params;
    fovis::CameraIntrinsicsParameters m_depth_camera_params;

    DepthImage::UniquePointer m_depth_image;
    VisualOdometryCoreUniquePointer m_vis_odom_core;

  private:
    friend VisualOdometry;

  private:

  };
}


