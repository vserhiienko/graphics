#include "GaiaVisualOdometry.h"

gaia::VisualOdometry::VisualOdometry(IRawStreams *streams) //: State(NULL)
{
  m_streams = streams;
  m_vis_odom_core = NULL;
  m_is_instance_valid = false;
}

gaia::VisualOdometry::~VisualOdometry()
{
  //quit();
}

void gaia::VisualOdometry::onRawStreamsServiceInitialized(IRawStreams *streams)
{
  if (streams != 0)
  //if (m_streams == state)
  {
    gaia::vec2f dfl, cfl, dpp, cpp, dd, cd;
    if (streams->getCameraIntrinsicsParameters(
      cfl, dfl, cpp, dpp, cd, dd
      ))
    {
      m_depth_camera_params.cx = dpp.x;
      m_depth_camera_params.cy = dpp.y;
      m_depth_camera_params.fx = dfl.x;
      m_depth_camera_params.fy = dfl.y;
      m_depth_camera_params.width = (int)dd.x;
      m_depth_camera_params.height = (int)dd.y;

      m_color_camera_params.cx = cpp.x;
      m_color_camera_params.cy = cpp.y;
      m_color_camera_params.fx = cfl.x;
      m_color_camera_params.fy = cfl.y;
      m_color_camera_params.width = (int)cd.x;
      m_color_camera_params.height = (int)cd.y;

      m_vis_odom_opts = fovis::VisualOdometry::getDefaultOptions();

      m_rect = std::make_unique<fovis::Rectification>(m_color_camera_params);
      //m_rect.reset(new fovis::Rectification(m_color_camera_params));
      m_vis_odom_core = std::make_unique<fovis::VisualOdometry>(m_rect.get(), m_vis_odom_opts);
      //m_vis_odom_core.reset(new fovis::VisualOdometry(m_rect.get(), m_vis_odom_opts));

      m_depth_image = std::make_unique<DepthImage>(this);
      //m_depth_image.reset(new DepthImage(this));

      m_is_instance_valid = !!m_vis_odom_core.get();
    }

    /*if (m_is_instance_valid && m_streams)
      m_streams->registerNotify(this);*/
  }
}

void gaia::VisualOdometry::onRawStreamsServiceReleased(IRawStreams *streams)
{
  if (streams != 0)
    //if (m_streams == streams)
  {
    /*if (m_is_instance_valid && m_streams)
      m_streams->unregisterNotify(this);*/
  }
}



void gaia::VisualOdometry::onFrameAcquired(
  IRawStreams const *rawStreams
  )
{
  onFrameAcquired(
    rawStreams->getGrayscale8FrameRef(),
    rawStreams->getDepthF32FrameRef()
    );
}

void gaia::VisualOdometry::onFrameAcquired(
  gaia::utils::Grayscale8Frame const &grayscaleFrame,
  gaia::utils::DepthF32Frame const &depthFrame
  )
{
  if (m_is_instance_valid)
  {
    m_depth_image->onFrameAcquired(
      depthFrame
      );

    m_vis_odom_core->processFrame(
      (const uint8_t*)grayscaleFrame.data[0],
      m_depth_image.get()
      );

    auto status = m_vis_odom_core->getMotionEstimateStatus();
    auto iso_pose = m_vis_odom_core->getPose();
    auto pose_xyz = iso_pose.translation();
    auto pose_rpy = iso_pose.rotation().eulerAngles(0, 1, 2);

    auto iso_motion = m_vis_odom_core->getMotionEstimate();
    auto motion_xyz = iso_motion.translation();
    auto motion_rpy = iso_motion.rotation().eulerAngles(0, 1, 2);

#if 1
    if (
      pose_xyz(0) != 0.0f
      || pose_xyz(1) != 0.0f
      || pose_xyz(2) != 0.0f
      || status != fovis::INSUFFICIENT_INLIERS
      )
      gaia::utils::trace(
      "gaia:visualOdometry:frame: w:%u: "
      "iso: t:[%.2f %.2f %.2f] o:[%.3f %.3f %.3f]"
      //"\tm(iso): t [%.2f %.2f %.2f] o [%.2f %.2f %.2f]"
      , status
      , pose_xyz(0)
      , pose_xyz(1)
      , pose_xyz(2)
      , pose_rpy(0) //* 180 / M_PI
      , pose_rpy(1) //* 180 / M_PI
      , pose_rpy(2) //* 180 / M_PI
      /*, motion_xyz(0)
      , motion_xyz(1)
      , motion_xyz(2)
      , motion_rpy(0) * 180 / M_PI
      , motion_rpy(1) * 180 / M_PI
      , motion_rpy(2) * 180 / M_PI*/
      );
#endif

  }
}

//void gaia::VisualOdometry::OnDeviceReconnected(SenzRawStreams *streams)
//{
//
//}
//
//void gaia::VisualOdometry::OnDeviceDisconnected(SenzRawStreams *streams)
//{
//
//}
