/*
 (C) 2013 SAP AG or an SAP affiliate company. All rights reserved.
*/
/**
	@file	DVLRenderer.h

	This header defines the Renderer interface which does all rendering in DVL
*/
#pragma once



/**
*	This structure is used to retrieve hit-testing information
*
*	\b NOTES 
*	\li You need to initialize the \b m_uSizeOfDVLHitTest member of the sDVLHitTest structure with its size.
*/
#pragma pack(push, 1)
struct sDVLHitTest
{
	/// Size of the structure. You must set it to sizeof(sDVLHitTest) before calling HitTest()
	uint16_t m_uSizeOfDVLHitTest;

	/// Input: X, Y coordinates of the point in question
	float m_fScreenCoordinateX;
	float m_fScreenCoordinateY;

	/// Output: identifier of the node that was hit. Or DVLID_INVALID if there are no 3D objects underneath the X, Y screen coordinate
	DVLID m_idNode;

	/// Output: world coordinate of the hit (only valid if m_idNode != DVLID_INVALID)
	float m_fWorldCoordinateX;
	float m_fWorldCoordinateY;
	float m_fWorldCoordinateZ;

	/// Output: local coordinate of the hit (only valid if m_idNode != DVLID_INVALID). In local coordinates of m_idNode node.
	float m_fLocalCoordinateX;
	float m_fLocalCoordinateY;
	float m_fLocalCoordinateZ;
};
#pragma pack(pop)



/**
*	This class defines the main interface for interaction with the rendering system of DVL
*/
class IDVLRenderer
{
protected:
	virtual ~IDVLRenderer() {}//can't be deleted using this interface

public:
	/**
	*	Sets dimensions of the canvas
	*
	*	You usually call this in OnResize() handler of your application. You also need to call
	*	it once the renderer is created to let it know the target resolution.
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT SetDimensions(uint32_t width, uint32_t height) = 0;

	/**
	*	Sets the color which is used to clear the screen. Can be gradient from top to bottom.
	*
	*	Some examples are below:
	*
	*	\code{.cpp}
	*		pRenderer->SetBackgroundColor(0.f, 0.f, 0.f, 	0.f, 0.f, 0.f); // black
	*		pRenderer->SetBackgroundColor(1.f, 1.f, 1.f, 	1.f, 1.f, 1.f); // white
	*		pRenderer->SetBackgroundColor(1.f, 1.f, 0.f, 	1.f, 0.5f, 0.f); // yellow to orange gradient
	*	\endcode
	*
	*	@note	Default color is top black (0, 0, 0) + bottom black (0, 0, 0)
	*/
	virtual void SetBackgroundColor(float fTopRed, float fTopGreen, float fTopBlue, float fBottomRed, float fBottomGreen, float fBottomBlue) = 0;

	/**
	*	Attaches a scene that will be displayed through this interface
	*
	*	The typical usage would be:
	*
	*	\code{.cpp}
	*		IDVLScene *pScene = NULL;
	*		pCore->LoadScene("file://path/to/file.vds", NULL, &pScene);
	*		pRenderer->AttachScene(pScene);
	*		pScene->Release();
	*	\endcode
	*
	*	\li AttachScene() increments the reference counter of the scene object so you may release the original pointer if it not needed.
	*	\li AttachScene() method releases the previously attached scene (if it exists).
	*	\li Pass \a NULL value to release the current scene without attaching a new one.
	*
	*	@note	All auxiliary scenes are automatically detached when this method is called
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT AttachScene(IDVLScene *pScene) = 0;

	/**
	*	Returns a pointer to the currently attached scene
	*
	*	\li It returns \a NULL if no scene is attached.
	*	\li Does not change reference count. You don't need to release this pointer.
	*/
	virtual IDVLScene *GetAttachedScenePtr() = 0;

	/**
	*	Master scene (set in AttachScene()) is rendered in each frame. But it is also possible to render additional scenes in the same frame.
	*	Such scenes are called auxiliary. By default, there are no auxiliary scenes.
	*/
	virtual size_t GetAuxiliaryScenesCount() = 0;

	/**
	*	Attaches an auxiliary scene to the renderer.
	*	Index of this scene == GetAuxiliaryScenesCount(). Increases the total number of auxiliary scenes by one.
	*
	*	@note	It is possible to attach the same scene multiple times.
	*	@note	Anchor node is set to DVLID_INVALID (i.e. anchoring disabled) and matrix is set to identity.
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_ACCESSDENIED	If master scene is not set using AttachScene() method
	*	@retval	DVLRESULT_BADARG	If pScene is NULL
	*	@retval	DVLRESULT_OUTOFMEMORY	If no memory to store pointer
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT AttachAuxiliaryScene(IDVLScene *pScene) = 0;

	/**
	*	Detaches an auxiliary scene from the renderer.
	*
	*	@param	index	Index of the scene to be detached
	*/
	virtual DVLRESULT DetachAuxiliaryScene(size_t index) = 0;

	/**
	*	Retrieves auxiliary scene information by index:
	*		1) Scene pointer
	*		2) ID of anchor node (scene matrix is set to be equal to the matrix of anchor node). DVLID_INVALID if anchoring is not used.
	*		3) scene matrix
	*
	*	@param	index	Index of the scene to be queried
	*	@param	ppScene	Pointer to the scene. Set to NULL, if not needed
	*	@param	pIDAnchorNode	Pointer to anchor DVLID. Set to NULL, if not needed
	*	@param	pTMWorld	Pointer to world matrix of the scene. Set to NULL, if not needed
	*/
	virtual DVLRESULT GetAuxiliarySceneInfo(size_t index, IDVLScene **ppScene, DVLID *pIDAnchorNode, sDVLMatrix *pTMWorld) = 0;

	/**
	*	Sets ID of a node in master scene that will act as anchor for a given auxiliary scene.
	*	This means that scene matrix will be equal to world matrix of anchor node.
	*	If anchor node changes matrix, auxiliary scene will also change matrix automatically.
	*
	*	@param	index	Index of the scene
	*	@param	idAnchorNode	ID of the node from master scene to be acting as an anchor
	*
	*	@note	Use DVLID_INVALID to remove anchor
	*/
	virtual DVLRESULT SetAuxiliarySceneAnchor(size_t index, DVLID idAnchorNode) = 0;

	/**
	*	Sets anchor matrix for the auxiliary scene.
	*
	*	@param	index	Index of the scene
	*	@param	tmWorld	Transformation matrix in local coordinates. All nodes in auxiliary scene will be pre-multiplied by this matrix during rendering.
	*/
	virtual DVLRESULT SetAuxiliarySceneAnchorMatrix(size_t index, const sDVLMatrix &tmLocal) = 0;


	/**
	*	Sets world matrix for the auxiliary scene.
	*
	*	@param	index	Index of the scene
	*	@param	tmWorld	Transformation matrix in world coordinates. All nodes in auxiliary scene will be pre-multiplied by this matrix during rendering.
	*/
	virtual DVLRESULT SetAuxiliarySceneMatrix(size_t index, const sDVLMatrix &tmWorld) = 0;

	/**
	*	Checks if the scene has been somehow modified and requires repaint
	*
	*	Use this method to check if the scene needs repainting. There are two reasons why scenes may need to be redrawn: if something is changed by the user
	*	or if something is changed inside the core, like mesh has been moved by animation. You can handle events of the first type yourself, but you can't
	*	do this for the internal events. So there is a method that tells you if the scene has been changed "inside" and needs updating.
	*
	*/
	virtual bool ShouldRenderFrame() const = 0;

	/**
	*	Renders a single frame using currently activated camera
	*
	*	Call this method to draw the attached scene. It requires OpenGL context to work.
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT RenderFrame() = 0;

	/**
	*	Renders a single frame using explicitly defined View and Projection matrices
	*
	*	Call this method to draw the attached scene. It requires OpenGL context to work.
	*
	*	@param	matView	View matrix
	*	@param	matProjection	Projection matrix
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT RenderFrameEx(const sDVLMatrix &matView, const sDVLMatrix &matProjection) = 0;

	/**
	*	Gets camera matrices
	*
	*	@param	matView	View matrix
	*	@param	matProjection	Projection matrix
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT GetCameraMatrices(sDVLMatrix &matView, sDVLMatrix &matProjection) const = 0;

	/**
	*	Sets the specified rendering option to ON or OFF
	*
	*	See the DVLRENDEROPTION enum for the details.
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_HARDWAREERROR	If new option is not supported by hardware
	*	@retval	DVLRESULT_BADARG	If such option does not exist
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT SetOption(DVLRENDEROPTION type, bool bEnable) = 0;

	/**
	*	Returns the current state of rendering options
	*
	*	See the DVLRENDEROPTION enum for the details.
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_BADARG	If such option does not exist
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT GetOption(DVLRENDEROPTION type, bool &bEnabled) = 0;

	/**
	*	Sets the specified rendering option value
	*
	*	See the DVLRENDEROPTIONF enum for the details.
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_HARDWAREERROR	If new option is not supported by hardware
	*	@retval	DVLRESULT_BADARG	If such option does not exist
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT SetOptionF(DVLRENDEROPTIONF type, float fValue) = 0;

	/**
	*	Returns the current value of rendering options
	*
	*	See the DVLRENDEROPTIONF enum for the details.
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_BADARG	If such option does not exist
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT GetOptionF(DVLRENDEROPTIONF type, float &fValue) const = 0;

	/**
	*	Changes view to default viewport (the "Home" mode)
	*
	*	After calling this method the scene will look like just loaded.
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT ResetView() = 0;

	/**
	*	Begins a gesture by computing target hit point, touch direction etc. Should be called at the beginning of each gesture.
	*
	*	@note	The number of currently active gestures is tracked and only the very first one is really processed
	*	@note	Rotation (using Rotate() method) of the scene will be performed around a 3D point calculated in BeginGesture() by hit-testing 3D scene with given "x", "y" 2D coordinates. If nothing is hit, rotation will be around scene bounding box center.
	*
	*	@param	x	Horizontal coordinate in points
	*	@param	y	Vertical coordinate in points
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT BeginGesture(float x, float y) = 0;

	/**
	*	Ends a gesture, should be called at the end of each gesture to decrease the internal counter
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_PROCESSED	Gesture was not started, thus ending gesture had no effect. This is not an error, you can call this method many times
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT EndGesture() = 0;

	/**
	*	Pans the scene
	*
	*	@param	dx	Horizontal delta in points
	*	@param	dy	Vertical delta in points
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT Pan(float dx, float dy) = 0;

	/**
	*	Rotates the scene around 3d orbit rotation center (which is calculated in BeginGesture())
	*
	*	@param	dx	Horizontal delta in points
	*	@param	dy	Vertical delta in points
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT Rotate(float dx, float dy) = 0;

	/**
	*	Zooms the scene
	*
	*	@param	f	Zoom velocity in points per second
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT Zoom(float f) = 0;

	/**
	*	Checks if the provided node can be isolated (by seeing if there are any visible geometry underneath it)
	*/
	virtual bool CanIsolateNode(DVLID id) = 0;

	/**
	*	Sets/clears isolated node
	*
	*	@param	id	Specifies the node to be isolated. Set to DVLID_INVALID to clear isolation.
	*/
	virtual DVLRESULT SetIsolatedNode(DVLID id) = 0;

	/**
	*	Returns the ::DVLID of currently isolated node or ::DVLID_INVALID if nothing is isolated
	*/
	virtual DVLID GetIsolatedNode() = 0;

	/**
	*	Zooms the scene to a bounding box created from a particular set of nodes
	*
	*	@param	what	What set of nodes to zoom to
	*	@param	fCrossFadeSeconds	Time to perform the "fly to" animation. Set to 0.0f to do this immediately
	*	@param	idNode	Is only used if what == ::DVLZOOMTO_NODE
	*
	*	@note	IDVLRenderer::ZoomTo(all|visible|selected|node) saves current viewport state and it can be later restored with IDVLRenderer::ZoomTo() call with the ::DVLZOOMTO_RESTORE parameter.
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene was not properly initialized
	*	@retval	DVLRESULT_NOTFOUND	If the nodes set was empty (for example ::DVLZOOMTO_VISIBLE and all is hidden)
	*	@retval	DVLRESULT_NOTINITIALIZED	If ::DVLZOOMTO_RESTORE and there is no saved view
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT ZoomTo(DVLZOOMTO what, DVLID idNode, float fCrossFadeSeconds) = 0;

	/**
	*	Sends the "tap" event to the core (for selection)
	*
	*	@param	x	Horizontal coordinate in points
	*	@param	y	Vertical coordinate in points
	*	@param	bDouble	Is that double or single tap
	*
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_OK	If worked correctly
	*/
	virtual DVLRESULT Tap(float x, float y, bool bDouble) = 0;

	/**
	*	Performs hit testing and finds a 3D object under the X, Y coordinates
	*
	*	@retval	DVLRESULT_BADFORMAT	If \b m_uSizeOfDVLHitTest is wrong
	*	@retval	DVLRESULT_BADARG	If \b pHitTest == \a NULL
	*	@retval	DVLRESULT_NOTINITIALIZED	If renderer initialization was not performed
	*	@retval	DVLRESULT_PROCESSED	If worked correctly and no 3D objects were hit
	*	@retval	DVLRESULT_OK	If worked correctly and 3D object was hit
	*/
	virtual DVLRESULT HitTest(sDVLHitTest *pHitTest) = 0;
};
