/*
 (C) 2013 SAP AG or an SAP affiliate company. All rights reserved.
*/
/**
	@file	DVLTypes.h

	This header defines basic data types.
*/
#pragma once

/// DVLID is the main identifier for all items inside a DVL scene. Materials, nodes, meshes, lights, cameras, animations - they all have unique DVLID
typedef uint64_t DVLID;

/// The "invalid" value of ::DVLID type
#define DVLID_INVALID						0xFFFFFFFFFFFFFFFFull

#define DVL_DOUBLE_MAX						1.7976931348623158e+308
#define DVL_DOUBLE_MIN						2.2250738585072014e-308 
#define DVL_FLOAT_MIN						1.175494351e-38F
#define DVL_FLOAT_MAX						3.402823466e+38F


/// DVLFAILED() macro allows to check if ::DVLRESULT signifies error
#define DVLFAILED(status)					((DVLRESULT)(status) < 0)

/// DVLSUCCEEDED() macro allows to check if ::DVLRESULT is "ok"
#define DVLSUCCEEDED(status)				((DVLRESULT)(status) > 0)



//
//DVLRESULT
//
/// Defines the result of the operation. May be successful or not.
enum DVLRESULT
{
	/// The file is encrypted and password was either not provided or is incorrect
	DVLRESULT_ENCRYPTED = -19,

	/// The file is not found
	DVLRESULT_FILENOTFOUND = -18,

	/// The library has not been initialized properly
	DVLRESULT_NOTINITIALIZED = -17,

	/// The version is wrong (file version, library version, etc)
	DVLRESULT_WRONGVERSION = -16,

	/// The name does not have an extension
	DVLRESULT_MISSINGEXTENSION = -15,

	/// Access is denied
	DVLRESULT_ACCESSDENIED = -14,

	/// There is no such interface
	DVLRESULT_NOINTERFACE = -13,

	/// Out of memory
	DVLRESULT_OUTOFMEMORY = -12,

	/// Invalid call
	DVLRESULT_INVALIDCALL = -11,

	/// The item or file is not found
	DVLRESULT_NOTFOUND = -10,

	/// The argument is invalid
	DVLRESULT_BADARG = -9,

	/// Failure, something went completely wrong
	DVLRESULT_FAIL = -8,

	/// Invalid thread
	DVLRESULT_BADTHREAD = -7,

	/// Incorrect format
	DVLRESULT_BADFORMAT = -6,

	/// File reading error
	DVLRESULT_FILEERROR = -5,

	/// The requested feature is not yet implemented
	DVLRESULT_NOTIMPLEMENTED = -4,

	/// Hardware error
	DVLRESULT_HARDWAREERROR = -3,

	/// The process has been interrupted
	DVLRESULT_INTERRUPTED = -1,

	/// Negative result
	DVLRESULT_FALSE = 0,

	/// Everything is OK
	DVLRESULT_OK = 1,

	/// Nothing was changed as a result of processing/action (similar to OK), for example if you want to "hide a node that is already hidden"
	DVLRESULT_PROCESSED,

	/// The initialization has been made already (it is OK to initialize multiple times, just not optimal)
	DVLRESULT_ALREADYINITIALIZED,
};



/// Defines the possible operation on the node flags. See IDVLScene::ChangeNodeFlags() for more details.
///
/// \b Notes
/// \li ::DVLFLAGOP_INVERT_INDIVIDUAL and ::DVLFLAGOP_INVERT only differ if ::DVLFLAGOP_MODIFIER_RECURSIVE is set. If not, there is no difference in how they are interpreted.
/// \li Use static_cast<DVLFLAGOPERATION>(::DVLFLAGOP_INVERT | ::DVLFLAGOP_MODIFIER_RECURSIVE) if using ::DVLFLAGOP_MODIFIER_RECURSIVE
enum DVLFLAGOPERATION
{
	/// Set the flag
	DVLFLAGOP_SET,

	/// Clear the flag
	DVLFLAGOP_CLEAR,

	/// if ::DVLFLAGOP_MODIFIER_RECURSIVE, then child node flags are set to parent values. So for example if parent is visible and child is hidden, after this operation both will be hidden
	DVLFLAGOP_INVERT,

	/// if ::DVLFLAGOP_MODIFIER_RECURSIVE, then child nodes are inverted. So for example if parent is visible and child is hidden, after this operation parent will be hidden and child visible
	DVLFLAGOP_INVERT_INDIVIDUAL,

	DVLFLAGOP_VALUES_BITMASK = 0x7F,

	/// Do the operation recursively for all the subitems
	DVLFLAGOP_MODIFIER_RECURSIVE = 0x80,
};



//
//DVLZOOMTO
//
/// Defines the \a Zoom \a To options
enum DVLZOOMTO
{
	/// bounding box of the whole scene
	DVLZOOMTO_ALL,

	/// bounding box of visible nodes
	DVLZOOMTO_VISIBLE,

	/// bounding box of selected nodes
	DVLZOOMTO_SELECTED,

	/// bounding box of a specific node and its children
	DVLZOOMTO_NODE,

	/// same as ::DVLZOOMTO_NODE, but also does IDVLRenderer::SetIsolatedNode() for the node
	DVLZOOMTO_NODE_SETISOLATION,

	/// previously saved view [view is saved every time IDVLRenderer::ZoomTo() is executed]
	DVLZOOMTO_RESTORE,

	/// same as ::DVLZOOMTO_RESTORE, but also does IDVLRenderer::SetIsolatedNode() with ::DVLID_INVALID parameter
	DVLZOOMTO_RESTORE_REMOVEISOLATION
};



//
//DVLEXECUTE
//
/// Defines the language type of the string passed into Execute()
enum DVLEXECUTE
{
	/// VE query language, for example "everything() select()". Only for 3D files.
	DVLEXECUTE_QUERY,

	/// SAP Paint XML, for example "<PAINT_LIST ASSEMBLY_PAINTING_ENABLED="true" ASSEMBLY_LEVEL="5"><PAINT COLOR="#008000" OPACITY="1.0" VISIBLE="true" ALLOW_OVERRIDE="false"><NODE ID="0__moto_x_asm"></NODE></PAINT></PAINT_LIST>". Only for 3D files.
	DVLEXECUTE_PAINTXML,

	/// CGM navigate action, for example "pictid(engine_top).id(oil-pump-t,full+newHighlight)". Only for 2D files. See http://www.w3.org/TR/webcgm20/WebCGM20-IC.html
	DVLEXECUTE_CGMNAVIGATEACTION,

	/// Dynamic labels XML
	DVLEXECUTE_DYNAMICLABELS,
};



//
//DVLRENDEROPTION
//
/// Defines the rendering options
enum DVLRENDEROPTION
{
	/// Show Debug Info like FPS or not, default: OFF
	DVLRENDEROPTION_SHOW_DEBUG_INFO,

	/// Display backfacing triangles or not, default: OFF
	DVLRENDEROPTION_SHOW_BACKFACING,

	/// Show shadow or not, default: ON
	DVLRENDEROPTION_SHOW_SHADOW,

	/// Orbit or Turntable, default: OFF (Turntable)
	DVLRENDEROPTION_CAMERA_ROTATION_MODE_ORBIT,

	/// Clear the color buffer during each Renderer::RenderFrame() or not, default: ON.
	/// By setting this option OFF, you can draw a textured background or paint video camera frame.
	/// The caller application would need to clear color buffer itself before calling RenderFrame() if option is OFF.
	DVLRENDEROPTION_CLEAR_COLOR_BUFFER,

	/// Ambient Occlusion effect. If turned ON, would disable stereo effect.
	/// It is not possible to have both Stereo and Ambient occlusion.
	DVLRENDEROPTION_AMBIENT_OCCLUSION,

	/// Anaglyph Stereo effect. If turned ON, would disable left+right stereo effect and ambient occlusion effect.
	/// It is not possible to have both Stereo and Ambient occlusion.
	DVLRENDEROPTION_ANAGLYPH_STEREO,

	/// Left+Right Stereo effect. If turned ON, would disable anaglyph stereo effect and ambient occlusion effect.
	/// It is not possible to have both Stereo and Ambient occlusion.
	DVLRENDEROPTION_LEFT_RIGHT_STEREO,

	/// Half resolution render (downsampling). If turned ON, would disable supersampling.
	DVLRENDEROPTION_HALF_RESOLUTION,

	/// Double resolution render (supersampling). If turned ON, would disable downsampling.
	DVLRENDEROPTION_DOUBLE_RESOLUTION,

	/// Show all hotspots or not, default: OFF. This is only working for 2D .cgm scenes.
	DVLRENDEROPTION_SHOW_ALL_HOTSPOTS,
};



//
//DVLRENDEROPTIONF
//
/// Defines the rendering options
enum DVLRENDEROPTIONF
{
	/// DPI (Dots Per Inch) setting. Defaults to 132.0 on iPad and 96.0 on other platforms.
	/// Use in calculating size of sprites and polyline thickness. Highly recommended to set it properly.
	DVLRENDEROPTIONF_DPI,

	/// Amount of millions of triangles in scene for using "dynamic loading".
	/// If scene has less than the given number of triangles, normal rendering is performed.
	/// Otherwise, "dynamic loading" is done: meshes are loaded on demand and rendering via occlusion culling is performed.
	/// Default: 3.0 (3,000,000 triangles in scene is needed to use "dynamic loading")
	/// Set to 0.0f to always have dynamic loading
	/// Set to -1.0f to disable dynamic loading
	DVLRENDEROPTIONF_DYNAMIC_LOADING_THRESHOLD,

	/// Maximum amount of video memory (in megabytes) that DVL Core may use for loading meshes
	/// Default: 256 MB on iPad and 512 MB on other platforms
	DVLRENDEROPTIONF_VIDEO_MEMORY_SIZE,
};



//
//DVLSCENEACTION
//
/// Defines the scene actions
enum DVLSCENEACTION
{
	/// Make all nodes in the scene 'visible'
	DVLSCENEACTION_SHOW_ALL,

	/// Make all nodes in the scene 'hidden'
	DVLSCENEACTION_HIDE_ALL,

	/// Make selected nodes and all their children 'visible'
	DVLSCENEACTION_SHOW_SELECTED,

	/// Make selected nodes and all their children 'hidden'
	DVLSCENEACTION_HIDE_SELECTED,
};



/**
*	This structure is used to retrieve thumbnails for folders, files, step views, etc
*
*	\b Notes
*	\li You need to initialize the \b m_uSizeOfDVLImage member of the sDVLImage structure with its size.
*	\li You need to call ReleaseThumbnail() after each RetrieveThumbnail()
*	\li Supported image types are PNG and JPG (you can guess the type by first 4 bytes of image buffer. PNG is 0x474E5089, JPG is 0xE1FFD8FF or 0xE0FFD8FF)
*/
#pragma pack(push, 1)
struct sDVLImage
{
	/// Set it to sizeof(sDVLImage) before calling RetrieveThumbnail()
	uint16_t m_uSizeOfDVLImage;

	/// Zero if no thumbnail, the size of the data in the buffer otherwise
	uint32_t m_uImageSize;

	/// The buffer of the m_uImageSize size with the image data, or NULL if no image
	void *m_pImageBuffer;
};
#pragma pack(pop)



/**
*	This is a wrapper for 4x4 row-major matrix
*
*	\b Notes
*	\li DVL SDK does not supply any methods for matrix manipulations. Use 3rd-party library or write your own code.
*/
#pragma pack(push, 1)
struct sDVLMatrix
{
	/// Matrix values
	float m[4][4];
};
#pragma pack(pop)



/// Defines the node flag bits. Flags can be combined.
enum DVLNODEFLAG
{
	/// The node is visible
	DVLNODEFLAG_VISIBLE								= 0x01,

	/// The node is selected
	DVLNODEFLAG_SELECTED							= 0x02,

	/// The node is closed (the node itself and all children are treated as a single node)
	DVLNODEFLAG_CLOSED								= 0x08,

	/// The node is single-sided
	DVLNODEFLAG_SINGLE_SIDED						= 0x10,

	/// The node is double-sided
	DVLNODEFLAG_DOUBLE_SIDED						= 0x20,

	/// The node can't be hit (transparent to the mouse clicks or taps)
	DVLNODEFLAG_UNHITABLE							= 0x40,

	/// The node is a common billboard, scales with camera but is always orthogonal
	DVLNODEFLAG_BILLBOARD_VIEW						= 0x80,

	/// The node is positioned on a 2D layer on top of the screen
	DVLNODEFLAG_BILLBOARD_LOCK_TO_VIEWPORT			= 0x100,



	//mapped node flags (flags that don't really exist and are emulated for DVL purposes)

	/// The node has children
	DVLNODEFLAG_MAPPED_HASCHILDREN					= 0x0200, //== (m_pChildren != NULL)

	/// The node has a name
	DVLNODEFLAG_MAPPED_HASNAME						= 0x0400, //== m_Name.NotEmpty()

	/// The node has an asset identifier
	DVLNODEFLAG_MAPPED_HASASSETID					= 0x0800, //== m_AssetID.NotEmpty()

	/// The node has an asset identifier
	DVLNODEFLAG_MAPPED_HASUNIQUEID					= 0x1000, //== m_UniqueID.NotEmpty()



	//internal node flags (not exposed)

	DVLNODEFLAG_INTERNAL_QUERY_BIT0					= 0x00010000,
	DVLNODEFLAG_INTERNAL_QUERY_BIT1					= 0x00020000,
	DVLNODEFLAG_INTERNAL_QUERY_BIT2					= 0x00040000,
	DVLNODEFLAG_INTERNAL_QUERY_BIT3					= 0x00080000,
	DVLNODEFLAG_INTERNAL_QUERY_BIT4					= 0x00100000,
	DVLNODEFLAG_INTERNAL_QUERY_BIT5					= 0x00200000,
	DVLNODEFLAG_INTERNAL_QUERY_BIT6					= 0x00400000,
	DVLNODEFLAG_INTERNAL_QUERY_BIT7					= 0x00800000,



	//temporary node flags (used for some purposes, but not saved/loaded from file)

	DVLNODEFLAG_TEMPORARY_PREVIOUS_VISIBILITY		= 0x10000000, // Temporary flag: visibility in previous frame
	DVLNODEFLAG_TEMPORARY_ORIGINAL_VISIBILITY		= 0x20000000, // Temporary flag: visibility when the file was just loaded
	DVLNODEFLAG_TEMPORARY_CONSUMED					= 0x40000000, // Temporary flag: consumed in this step or not
};
