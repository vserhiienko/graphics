#pragma once

#include <vector>
#include <algorithm>
#include <string>
#include <sstream>

#include <NV/NvLogs.h>
#include <NV/NvMath.h>
#include <NvFoundation.h>
#include <NV/NvStopWatch.h>
#include <NvUI/NvTweakBar.h>
#include <NV/NvPlatformGL.h>
#include <NvGLUtils/NvImage.h>
#include <NvAppBase/NvSampleApp.h>
#include <NvGLUtils/NvGLSLProgram.h>
#include <NvAssetLoader/NvAssetLoader.h>
#include <NvAppBase/NvFramerateCounter.h>
#include <NvAppBase/NvInputTransformer.h>
#define _degs2radsf(d) (d * NV_PI / 180.0f)

#ifndef WIN32_CLEAN_AND_MEAN
#define WIN32_CLEAN_AND_MEAN
#endif

#include <Windows.h>
#include <GL/GLU.h>

#include "DVLInclude.h"
#include "VEUtilities.h"

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif
