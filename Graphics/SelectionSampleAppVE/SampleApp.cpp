#include "pch.h"
#include "Shapes.h"
#include "SampleApp.h"

#define _Render_with_my_matrices 1

#include <Scene.h>

NvAppBase* NvAppFactory(NvPlatformContext* platform)
{
  return new nyx::SampleApp(platform);
}

nyx::SampleApp::SampleApp(NvPlatformContext* platform) : NvSampleApp(platform)
{
  m_dvlCore = NULL;
  m_dvlRenderer = NULL;

  m_leftMoved = false;
  m_leftPressed = false;
  m_rightPressed = false;
  m_projectItems = false;
  m_selectionMode = false;
  m_enableMultipleSelections = false;
  m_redmarkingMode = false;
  m_renderScene = false;
  m_useCustomCamera = false;
}

nyx::SampleApp::~SampleApp(void)
{

}

void nyx::SampleApp::initUI(void)
{
  if (mTweakBar)
  {
    // add controls
    mTweakBar->addValue("Selection mode", m_selectionMode);
    mTweakBar->addValue("Draw projected centres", m_projectItems);
    mTweakBar->addValue("Enable multiple selections", m_enableMultipleSelections);
    mTweakBar->addValue("Enable custom camera", m_useCustomCamera);
    mTweakBar->addValue("Enable redmarking", m_redmarkingMode);
    mTweakBar->addValue("Render scene", m_renderScene);

    //mTweakBar->addPadding();
    //mTweakBar->addValue("Reset selection", m_resetButtonVar, true);
    mTweakBar->syncValues();
  }

  // Change the filtering for the framerate
  mFramerate->setMaxReportRate(.2f);
  mFramerate->setReportFrames(20);

  // Disable wait for vsync
  //getGLContext()->setSwapInterval(0);
}

void nyx::SampleApp::initRendering(void)
{
#if 0

  DVLRESULT dvlResult;

  // Set the initial view 
  //m_transformer->setTranslationVec(nv::vec3f(-500.0f, 0.0f, -1000.0f));
  m_transformer->setTranslationVec(nv::vec3f(-339.265076f, -305.155823, -1000.0f));
  //m_transformer->setTranslationVec(nv::vec3f(-400.0f, -200.0f, -1000.0f));
  //m_transformer->setTranslationVec(nv::vec3f(0.0f, 0.0f, -1000.0f));
  m_transformer->setMaxTranslationVel(100.0f);
  //m_transformer->setRotationVec(nv::vec3f(_degs2radsf(1.0f), _degs2radsf(1.0f), _degs2radsf(0.0f)));

  m_dvlCore = DVLCreateCoreInstance();
  if (!m_dvlCore) return;

  dvlResult = m_dvlCore->Init(this, DVL_VERSION_MAJOR, DVL_VERSION_MINOR);
  if (DVLFAILED(dvlResult)) return;

  dvlResult = m_dvlCore->InitRenderer();
  if (DVLFAILED(dvlResult)) return;

  m_dvlRenderer = m_dvlCore->GetRendererPtr();
  m_sceneCamera.coreRenderer = m_dvlRenderer;

  if (!m_dvlRenderer) return;

  m_dvlRenderer->SetBackgroundColor(0.9f, 0.9f, 0.9f, 0.8f, 0.8f, 0.8f);
  m_dvlRenderer->SetOption(DVLRENDEROPTION_AMBIENT_OCCLUSION, false);
  //m_dvlRenderer->SetOption(DVLRENDEROPTION_SHOW_DEBUG_INFO, true);
  //m_dvlRenderer->SetOption(DVLRENDEROPTION_SHOW_SHADOW, true);

  if (!loadScene())
  {
    NVWindowsLog("failed to load scene");
  }

  m_groundPlane.distance = -0.0001f;
  m_groundPlane.normal = 0.0f;
  m_groundPlane.normal.z = -1.0f;

#else

  ve::soko::SceneManager::initialize();
  loadScene();

#endif
}

bool nyx::SampleApp::loadScene()
{
#if 0


  DVLRESULT dvlResult;
  char buf2[MAX_PATH * 2] = {};
  wchar_t buf[MAX_PATH + 10] = {};

  wcscpy_s(buf, L"file://");
  GetModuleFileNameW(NULL, buf + 7, _countof(buf) - 7);

  //wcscat_s(buf, L"visualization_-_conference_room.dwg");
  //wcscat_s(buf, L"998 Clutch.vds");
  //wcscat_s(buf, L"998 Clutch.vds");
  //wcscat_s(buf, L"Teapot.vds");
  //wcscat_s(buf, L"LOWER BOP STACK_NDT_5Y_meta2-1.vds");
  //wcscat_s(buf, L"BOP_1.2.vds");
  //wcscat_s(buf, L"BOP_1.1.vds");
  //wcscat_s(buf, L"VIK-07-1083-021-851_Sheet2.vds");
  //wcscat_s(buf, L"X-1011196-77_sheet_1+TB.vds");
  //wcscat_s(buf, L"X-1011196-77_sheet_3+TB.vds");
  //wcscat_s(buf, L"SAP Pocket Knife.vds");

  wcsrchr(buf, L'\\')[1] = 0;
  //wcscat_s(buf, L"Test_rectangle.vds");
  //wcscat_s(buf, L"Test_rectangle2.vds");
  //wcscat_s(buf, L"LOWER BOP STACK_NDT_5Y_meta2-2.vds");
  wcscat_s(buf, L"SMD400429400000_v2.vds");
  WideCharToMultiByte(CP_UTF8, 0, buf, -1, buf2, _countof(buf2), NULL, NULL);

  Scene::CoreObject *dvlScene;
#if DVL_VERSION_MAJOR >= 4
  dvlResult = m_dvlCore->LoadScene(buf2, NULL, &dvlScene, 0);
#else
  dvlResult = m_dvlCore->LoadScene(buf2, &dvlScene, 0);
#endif
  if (DVLFAILED(dvlResult))
  {
    NVWindowsLog("failed to load scene from file \"%s\"", buf2);
  }

  dvlResult = m_dvlRenderer->AttachScene(dvlScene);
  dvlScene->Release();
  dvlScene = NULL;

  m_scene.initialize(m_dvlRenderer->GetAttachedScenePtr());
  if (!m_scene.modelNode) errorExit("failed to find model node");

  m_sceneCamera.setDrawingCentre(m_scene.centre.x, m_scene.centre.y);
  m_sceneCamera.setDrawingDimensions(m_scene.dimensions.x, m_scene.dimensions.y);

  return true;

#else

  return ve::soko::SceneManager::loadScene("SMD000000000000000000400425802000_v3.vds");

#endif
}

void nyx::SampleApp::resetSceneTransform()
{
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
}

void nyx::SampleApp::draw(void)
{
  glClearColor(0.5, 0.5, 0.5, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);

  if (m_redmarkingMode)
  {
    if (m_renderScene)
    {
      DVLRESULT onRender = DVLRESULT_OK;
#if 0
      if (m_useCustomCamera)
      {
        onRender = m_dvlRenderer->RenderFrameEx(
          (sDVLMatrix&)m_sceneCamera.viewMatrix,
          (sDVLMatrix&)m_sceneCamera.projectionMatrix
          );
      }
      else
      {
        m_viewMatrix = m_transformer->getModelViewMat();
        onRender = m_dvlRenderer->RenderFrameEx(
          (sDVLMatrix&)m_viewMatrix,
          (sDVLMatrix&)m_projectionMatrix
          );
      }
#else

      ve::soko::SceneManager::getRenderer()->render(
        ve::soko::SceneManager::getCamera(),  
        1.0f / 60.0f
        );

#endif

      switch (onRender)
      {
      case DVLRESULT_OK: break;
      default:
        NVWindowsLog("failed to render scene");
        break;
      }
    }

    resetSceneTransform();
    m_selectionAssist.updateProjectedPoints(
      m_viewMatrix._array,
      m_projectionMatrix._array
      );
    m_selectionAssist.draw();

    CHECK_GL_ERROR();
  }
  else
  {
    //if (DVLFAILED(m_dvlRenderer->RenderFrame()))
    //{
    //  //NVWindowsLog("failed to draw scene");
    //}

   /* m_dvlRenderer->ZoomTo(DVLZOOMTO_ALL, 0, 0);
    m_dvlRenderer->GetCameraMatrices(
      (sDVLMatrix&)m_dvlViewMatrix,
      (sDVLMatrix&)m_dvlProjectionMatrix
      );*/
  }

}

void nyx::SampleApp::configurationCallback(
  NvEGLConfiguration &config
  )
{
  config.depthBits = 24;
  config.stencilBits = 0;
  config.apiVer = NvGfxAPIVersionGL4();
}

void nyx::SampleApp::reshape(
  int32_t width,
  int32_t height
  )
{
  static float orthoMult = 1;
  static float farPlane = 2000.0f;

  m_screemDims.x = width;
  m_screemDims.y = height;
  m_selectionAssist.setScreenDimensions(m_screemDims.x, m_screemDims.y);

#if 0
  m_sceneCamera.setScreenDimensions(m_screemDims.x, m_screemDims.y);

  glViewport(0, 0, (GLint)width, (GLint)height);
  nv::perspective(m_projectionMatrix, _degs2radsf(45.0f), (float)width / (float)height, 0.1f, farPlane);
  nv::ortho3D(m_orthoProjectionMatrix, 0.0f, (float)width * orthoMult, 0.0f, (float)height * orthoMult, 0.1f, farPlane);
  if (m_dvlRenderer) m_dvlRenderer->SetDimensions(width, height);
#else

  ve::soko::SceneManager::getCamera()->setScreenSize(m_screemDims);

#endif
}

void nyx::SampleApp::savePointerPosition(NvPointerEvent *e)
{
  m_lastPosition.x = e->m_x;
  m_lastPosition.y = e->m_y;
}

void nyx::SampleApp::beginGesture(float x, float y)
{
  if (m_redmarkingMode)
  {
    if (m_useCustomCamera)
    {
      ve::soko::SceneManager::getCamera()->beginGesture(x, y);
      //m_sceneCamera.beginGesture(x, y);
    }
  }
  else if (m_dvlRenderer)
  {
    m_dvlRenderer->BeginGesture(x, y);
  }
}

void nyx::SampleApp::tap(float x, float y)
{
  if (m_redmarkingMode)
  {
    if (m_useCustomCamera)
    {
      ve::soko::SceneManager::getCamera()->tap(x, y);
      //m_sceneCamera.tap(x, y);
    }
  }
  else if (m_dvlRenderer)
  {
    m_dvlRenderer->Tap(x, y, false);
  }
}

void nyx::SampleApp::pan(float x, float y)
{
  if (m_redmarkingMode)
  {
    if (m_useCustomCamera)
    {
      ve::soko::SceneManager::getCamera()->pan(x, y);
      //m_sceneCamera.pan(x, y);
    }
  }
  else if (m_dvlRenderer)
  {
    m_dvlRenderer->Pan(x, y);
  }
}

void nyx::SampleApp::zoom(float x)
{
  if (m_redmarkingMode)
  {
    if (m_useCustomCamera)
    {
      ve::soko::SceneManager::getCamera()->zoom(x);
      //m_sceneCamera.zoom(x);
    }

  }
  else if (m_dvlRenderer)
  {
    m_dvlRenderer->Zoom(x);
  }
}

void nyx::SampleApp::endGesture()
{
  if (m_redmarkingMode)
  {
    if (m_useCustomCamera)
    {
      ve::soko::SceneManager::getCamera()->endGesture();
      //m_sceneCamera.endGesture();
    }
  }
  else if (m_dvlRenderer)
  {
    m_dvlRenderer->EndGesture();
  }
}

bool nyx::SampleApp::handlePointerInput(
  NvInputDeviceType::Enum device,
  NvPointerActionType::Enum action,
  uint32_t modifiers, int32_t count,
  NvPointerEvent *point
  )
{

  if (m_redmarkingMode)
  {
    if (m_selectionMode && point->m_id == NvMouseButton::LEFT)
    {
      nv::vec2f screenPos;
      screenPos.x = point->m_x;
      screenPos.y = point->m_y;

      switch (action)
      {
      case NvPointerActionType::UP:
        m_selectionAssist.endSelection();

        /*m_selectionAssist.findPointsInSelection(
          &m_itemProjectedCenters[0],
          &m_selectedItems[0],
          m_itemCenters.size(),
          !m_enableMultipleSelections
          );*/
        m_selectionAssist.unprojectPoints(
          m_groundPlane,
          m_viewMatrix._array,
          m_projectionMatrix._array
          );

        break;
      case NvPointerActionType::DOWN:
        m_selectionAssist.beginSelection();
        break;
      case NvPointerActionType::MOTION:
        m_selectionAssist.addPoint(screenPos);
        break;
      }
      // 339.265076, 305.155823
      // 678.53,610.312

      return true;
    }
    else
    {
      if (point->m_id == NvMouseButton::LEFT)
      {
        if (action == NvPointerActionType::UP)
        {
          //print("view", (sDVLMatrix&)m_viewMatrix);
          //print("proj", (sDVLMatrix&)m_projectionMatrix);
        }

        if (!m_useCustomCamera)
          return true;
      }
    }
  }


  if (!m_selectionMode)
  {
    nv::vec2f delta;
#if DVL_VERSION_MAJOR >= 4
    sDVLHitTest hitTest;
    nyx::initInfo(hitTest);
#endif
    nv::vec2f prevPosition;
    bool startGesture = false, endGesture = false;
    bool leftButton = point->m_id == NvMouseButton::LEFT;

    switch (action)
    {
    case NvPointerActionType::UP:
      savePointerPosition(point);

      endGesture
        = (leftButton && !m_rightPressed)
        || (!leftButton && !m_leftPressed);

      if (leftButton) m_leftPressed = false;
      else m_rightPressed = false;

      if (endGesture)
      {
        //NVWindowsLog("end gesture");
        //m_dvlRenderer->EndGesture();
        this->endGesture();

        if (leftButton && !m_leftMoved) // it was a mouse click
        {
          /*NVWindowsLog(
            "tap: %.2f %.2f"
            , m_lastPosition.x
            , m_lastPosition.y
            );*/
          /*m_dvlRenderer->Tap(
             m_lastPosition.x,
             m_lastPosition.y,
             false
             );*/
          tap(
            m_lastPosition.x,
            m_lastPosition.y
            );
//
//#if DVL_VERSION_MAJOR >= 4
//
//          sDVLMatrix v, p;
//          nv::vec4f worldPos;
//          worldPos.w = 1.0f;
//          nv::vec2f mousePos;
//          nv::vec2f projectedPos;
//          mousePos.x = point->m_x;
//          mousePos.y = point->m_y;
//          hitTest.m_fScreenCoordinateX = point->m_x;
//          hitTest.m_fScreenCoordinateY = point->m_y;
//          switch (m_dvlRenderer->HitTest(&hitTest))
//          {
//          case DVLRESULT_OK:
//            worldPos.x = hitTest.m_fWorldCoordinateX;
//            worldPos.y = hitTest.m_fWorldCoordinateY;
//            worldPos.z = hitTest.m_fWorldCoordinateZ;
//            m_dvlRenderer->GetCameraMatrices(v, p);
//            //print("view", v);
//            //print("proj", p);
//            break;
//          case DVLRESULT_PROCESSED:
//            //NVWindowsLog("hit test: processed");
//            break;
//          case DVLRESULT_NOTINITIALIZED:
//            NVWindowsLog("hit test failed: not initialized");
//            break;
//          case DVLRESULT_BADFORMAT:
//            NVWindowsLog("hit test failed: bad format");
//            break;
//          case DVLRESULT_BADARG:
//            NVWindowsLog("hit test failed: bad arg");
//            break;
//          case DVLRESULT_FAIL:
//            NVWindowsLog("hit test failed: fail");
//            break;
//          };
//#endif

        }
      }

      break;

    case NvPointerActionType::DOWN:
      savePointerPosition(point);

      startGesture
        = (leftButton && !m_rightPressed)
        || (!leftButton && !m_leftPressed);

      if (leftButton)
      {
        m_leftMoved = false;
        m_leftPressed = true;
      }
      else
      {
        if (!startGesture)
          m_leftMoved = true; // not a click
        m_rightPressed = true;
      }

      if (startGesture)
      {
        //NVWindowsLog("begin gesture");
        /*m_dvlRenderer->BeginGesture(
          m_lastPosition.x,
          m_lastPosition.y
          );*/
        beginGesture(
          m_lastPosition.x,
          m_lastPosition.y
          );
      }

      break;

    case NvPointerActionType::MOTION:
      prevPosition = m_lastPosition;
      savePointerPosition(point);
      delta = m_lastPosition - prevPosition;

      if (nv::length(delta) == 0.0f) break;
      m_leftMoved = true;

      if (m_leftPressed && m_rightPressed) // pan
      {
        pan(delta.x, delta.y);
      }
      else if (m_leftPressed) // rotate
      {
        //m_dvlRenderer->Rotate(delta.x, delta.y);
      }
      else if (m_rightPressed) // zoom
      {
        float z = m_redmarkingMode ? delta.y : std::min(std::max(1.0f - (delta.y / 150.f), 0.1f), 10.0f);
        zoom(z);
      }

      break;
    }
  }

  return false;
}

bool nyx::SampleApp::handleKeyInput(
  uint32_t code,
  NvKeyActionType::Enum action
  )
{
  return false;
}


void nyx::SampleApp::OnNodeSelectionChanged(
  IDVLScene *pScene,
  size_t uNumberOfSelectedNodes,
  DVLID idFirstSelectedNode
  )
{
  NVWindowsLog(
    "node selection changed in scene 0x%x (%u, 0x%0x)"
    , pScene
    , uNumberOfSelectedNodes
    , (uint32_t)(idFirstSelectedNode & 0xffffffff)
    );
}

void nyx::SampleApp::OnStepEvent(
  DVLSTEPEVENT type,
  DVLID stepId
  )
{
  NVWindowsLog("step event 0x%x with id 0x%x", type, stepId);
}

void nyx::SampleApp::LogMessage(
  DVLCLIENTLOGTYPE type,
  const char *szSource,
  const char *szText
  )
{
  //char buf[2048] = {};
  //_snprintf_s(buf, _countof(buf), "log> type=%d, source=%s, text=%s", type, szSource, szText);
  //NVWindowsLog(buf);
}

bool nyx::SampleApp::NotifyFileLoadProgress(
  float fProgress
  )
{
  /*char buf[128] = {};
  sprintf_s(buf, "loading: %f", fProgress);
  NVWindowsLog(buf);*/
  return true;
}