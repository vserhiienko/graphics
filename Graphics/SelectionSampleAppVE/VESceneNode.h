#pragma once

#include "pch.h"

namespace nyx
{
  class Scene;
  class SceneNode
  {
  public:
    friend Scene;
    typedef DVLID NodeId;
    typedef sDVLNodeInfo CoreNodeInfo;
    typedef SceneNode *SceneNodePtr;
    typedef std::vector<SceneNode*> Collection;
    typedef Collection::iterator Iterator;

  public:
    NodeId id;
    nv::matrix4f pose;

    Scene *scene;
    Collection kids;
    Collection parents;

    nv::vec2f centre;
    nv::vec2f dimensions;

    bool centreAvailable;
    bool dimensionsAvailable;

  public:

    SceneNode(Scene *scene);
    void printInfo(CoreNodeInfo &info);
    void printInfo(sDVLMetadata &meta);
    bool find(sDVLMetadata &meta, std::string const &ns, std::string const &field, std::string &value);

  public:

  private:
    bool findField(sDVLMetadata *meta, std::string const &field, std::string &value);

  };
}