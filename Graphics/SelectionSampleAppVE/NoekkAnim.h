#pragma once

#ifndef NOMINMAX
#define NOMINMAX
#endif

#include <algorithm>
#include <NV/NvMath.h>

#define XM_CALLCONV
#define _noekk_forceInlining inline

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

namespace noekk
{
  namespace anim
  {
    struct EasingFunctionBase
    {
      typedef float Real;
      typedef int Boolean;

      typedef union Input
      {
        struct { EasingFunctionBase::Real offset, distance, total, elapsed; };
        struct { EasingFunctionBase::Real b, c, d, t; };
      } Input;

      typedef EasingFunctionBase::Real(* OnFrameCallback)(EasingFunctionBase::Input);

      EasingFunctionBase::Input params;
      EasingFunctionBase::Boolean saturated = 0;
      EasingFunctionBase::OnFrameCallback onFrameFunction;
      EasingFunctionBase(OnFrameCallback on_frame) : onFrameFunction(on_frame) { }

      _noekk_forceInlining void reset()
      {
        params.elapsed = 0.0f;
        saturated = 0;
      }
      _noekk_forceInlining EasingFunctionBase::Real onFrame()
      {
        return onFrameFunction(params);
      }
      _noekk_forceInlining void operator += (EasingFunctionBase::Real tick_time)
      {
        if (!saturated)
        {
          if (params.elapsed < params.total)
            params.elapsed += tick_time;
          else
            params.elapsed = params.total,
            saturated = 1;
        }
      }
      _noekk_forceInlining void operator >>= (EasingFunctionBase::Real tick_time)
      {
        params.elapsed += tick_time;
      }

    };

    typedef enum class EasingStage
    {
      kSuspending,	//! @brief moving to the left away, unresponsive
      kSuspended,		//! @brief not visible, unresponsive
      kResuming,		//! @brief becoming visible, unresponsive
      kResumed,		  //! @brief visible, responsive
      kMax,			    //! @brief easing stages count
      kDefault = kSuspended
    } Stage;

    typedef enum class EasingFunctionType : unsigned
    {
      kLin,		    //! @brief no easing, no acceleration 
      kQuaIn,		  //! @brief accelerating from zero velocity 
      kQuaOut,	  //! @brief decelerating to zero velocity 
      kQuaInOut,	//! @brief acceleration until halfway, then deceleration
      kCubIn,		  //! @brief accelerating from zero velocity 
      kCubOut,	  //! @brief decelerating to zero velocity
      kCubInOut,	//! @brief acceleration until halfway, then deceleration
      kQrtIn,		  //! @brief accelerating from zero velocity
      kQrtOut,	  //! @brief decelerating to zero velocity
      kQrtInOut,	//! @brief acceleration until halfway, then deceleration
      kQntIn,		  //! @brief accelerating from zero velocity
      kQntOut,	  //! @brief decelerating to zero velocity
      kQntInOut,	//! @brief acceleration until halfway, then deceleration
      kSinIn,		  //! @brief accelerating from zero velocity
      kSinOut,	  //! @brief decelerating to zero velocity
      kSinInOut,	//! @brief acceleration until halfway, then deceleration
      kExpIn,		  //! @brief accelerating from zero velocity
      kExpOut,	  //! @brief decelerating to zero velocity
      kExpInOut,	//! @brief acceleration until halfway, then deceleration
      kCirIn,		  //! @brief accelerating from zero velocity
      kCirOut,	  //! @brief decelerating to zero velocity
      kCirInOut,	//! @brief acceleration until halfway, then deceleration

      kMax = kCirInOut,

      kLinIn = kLin,
      kLinOut = kLin,
      kLinInOut = kLin,
      kLinPair = kLinIn,
      kQuaPair = kQuaIn,
      kCubPair = kCubIn,
      kQrtPair = kQrtIn,
      kQntPair = kQntIn,
      kSinPair = kSinIn,
      kExpPair = kExpIn,
      kCirPair = kCirIn,

    } EasingFunctionType;

    template< typename T > struct UnitSteppableEasingFunctionType { enum { value = 0 }; };
    template<> struct UnitSteppableEasingFunctionType < EasingFunctionType > { enum { value = 1 }; };
    template< typename T > typename std::enable_if< UnitSteppableEasingFunctionType< T >::value, T >::type operator++(T value) { return T(value + 1); }
    template< typename T > typename std::enable_if< UnitSteppableEasingFunctionType< T >::value, T >::type operator+(T value, int inc) { return T(value + inc); }
    template< typename T > typename std::enable_if< UnitSteppableEasingFunctionType< T >::value, T >::type operator+(T value, unsigned inc) { return T(value + inc); }

    struct EasingFunctions
    {
#pragma region Easing functions implementation
      _noekk_forceInlining static EasingFunctionBase::Real linear(EasingFunctionBase::Input i)
      {
        return i.c*i.t / i.d + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real quadraticIn(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        return i.c * i.t * i.t + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real quadraticOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        return -i.c * i.t * (i.t - 2.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real quadraticInOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d / 2.0f;
        if (i.t < 1.0f) return i.c / 2.0f * i.t * i.t + i.b;
        i.t--;
        return -i.c / 2.0f * (i.t * (i.t - 2.0f) - 1.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real cubicIn(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        return i.c*i.t * i.t * i.t + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real cubicOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        i.t--;
        return i.c*(i.t * i.t * i.t + 1.0f) + i.b;

      }
      _noekk_forceInlining static EasingFunctionBase::Real cubicInOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d / 2.0f;
        if (i.t < 1.0f) return i.c / 2.0f * i.t * i.t * i.t + i.b;
        i.t -= 2.0f;
        return i.c / 2.0f * (i.t * i.t * i.t + 2.0f) + i.b;

      }
      _noekk_forceInlining static EasingFunctionBase::Real quarticIn(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        return i.c*i.t * i.t * i.t * i.t + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real quarticOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        i.t--;
        return -i.c * (i.t * i.t * i.t * i.t - 1.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real quarticInOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d / 2.0f;
        if (i.t < 1.0f) return i.c / 2.0f * i.t * i.t * i.t * i.t + i.b;
        i.t -= 2.0f;
        return -i.c / 2.0f * (i.t * i.t * i.t * i.t - 2.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real quinticIn(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        return i.c*i.t * i.t * i.t * i.t * i.t + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real quinticOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        i.t--;
        return -i.c * (i.t * i.t * i.t * i.t * i.t + 1.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real quinticInOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d / 2.0f;
        if (i.t < 1.0f) return i.c / 2.0f * i.t * i.t * i.t * i.t * i.t + i.b;
        i.t -= 2.0f;
        return i.c / 2.0f * (i.t * i.t * i.t * i.t * i.t + 2.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real sinusoidalIn(EasingFunctionBase::Input i)
      {
        return -i.c * cosf(i.t / i.d * (NV_PI / 2.0f)) + i.c + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real sinusoidalOut(EasingFunctionBase::Input i)
      {
        return i.c * sinf(i.t / i.d * (NV_PI / 2.0f)) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real sinusoidalInOut(EasingFunctionBase::Input i)
      {
        return -i.c / 2.0f * (cosf(NV_PI*i.t / i.d) - 1.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real exponencialIn(EasingFunctionBase::Input i)
      {
        return i.c * powf(2.0f, 10.0f * (i.t / i.d - 1.0f)) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real exponencialOut(EasingFunctionBase::Input i)
      {
        return i.c * (-powf(2.0f, -10.0f * i.t / i.d) + 1.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real exponencialInOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d / 2.0f;
        if (i.t < 1.0f) return i.c / 2.0f * powf(2.0f, 10.0f * (i.t - 1.0f)) + i.b;
        i.t--;
        return i.c / 2.0f * (-powf(2.0f, -10.0f * i.t) + 2.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real circularIn(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        return -i.c * (sqrtf(1.0f - i.t * i.t) - 1.0f) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real circularOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d;
        i.t--;
        return i.c * sqrtf(1.0f - i.t * i.t) + i.b;
      }
      _noekk_forceInlining static EasingFunctionBase::Real circularInOut(EasingFunctionBase::Input i)
      {
        i.t /= i.d / 2.0f;
        if (i.t < 1.0f) return -i.c / 2.0f * (sqrtf(1.0f - i.t * i.t) - 1.0f) + i.b;
        i.t -= 2.0f;
        return i.c / 2.0f * (sqrtf(1.0f - i.t * i.t) + 1.0f) + i.b;
      }
#pragma endregion
    };

    template <EasingFunctionType _FuncTy> struct EasingFunction;

    typedef EasingFunctionBase::Input EasingFunctionInput;
    template <> struct EasingFunction<EasingFunctionType::kLin> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::linear) {} };
    template <> struct EasingFunction<EasingFunctionType::kQuaIn> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::quadraticIn) {} };
    template <> struct EasingFunction<EasingFunctionType::kCubIn> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::cubicIn) {} };
    template <> struct EasingFunction<EasingFunctionType::kQrtIn> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::quarticIn) {} };
    template <> struct EasingFunction<EasingFunctionType::kQntIn> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::quinticIn) {} };
    template <> struct EasingFunction<EasingFunctionType::kSinIn> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::sinusoidalIn) {} };
    template <> struct EasingFunction<EasingFunctionType::kExpIn> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::exponencialIn) {} };
    template <> struct EasingFunction<EasingFunctionType::kCirIn> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::circularIn) {} };
    template <> struct EasingFunction<EasingFunctionType::kQuaOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::quadraticOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kCubOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::cubicOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kQrtOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::quarticOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kQntOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::quinticOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kSinOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::sinusoidalOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kExpOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::exponencialOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kCirOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::circularOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kQuaInOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::quadraticInOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kCubInOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::cubicInOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kQrtInOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::quarticInOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kQntInOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::quinticInOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kSinInOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::sinusoidalInOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kExpInOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::exponencialInOut) {} };
    template <> struct EasingFunction<EasingFunctionType::kCirInOut> : public EasingFunctionBase{ EasingFunction() : EasingFunctionBase(&EasingFunctions::circularInOut) {} };

    typedef EasingFunction<EasingFunctionType::kLin> LinearEasingFunction;
    typedef EasingFunction<EasingFunctionType::kQuaIn> QuadraticInEasingFunction;
    typedef EasingFunction<EasingFunctionType::kCubIn> CubicInEasingFunction;
    typedef EasingFunction<EasingFunctionType::kQrtIn> QuarticInEasingFunction;
    typedef EasingFunction<EasingFunctionType::kQntIn> QuinticInEasingFunction;
    typedef EasingFunction<EasingFunctionType::kSinIn> SinusoidalInEasingFunction;
    typedef EasingFunction<EasingFunctionType::kExpIn> ExponencialInEasingFunction;
    typedef EasingFunction<EasingFunctionType::kCirIn> CircularInEasingFunction;
    typedef EasingFunction<EasingFunctionType::kQuaOut> QuadraticOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kCubOut> CubicOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kQrtOut> QuarticOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kQntOut> QuinticOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kSinOut> SinusoidalOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kExpOut> ExponencialOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kCirOut> CircularOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kQuaInOut> QuadraticInOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kCubInOut> CubicInOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kQrtInOut> QuarticInOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kQntInOut> QuinticInOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kSinInOut> SinusoidalInOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kExpInOut> ExponencialInOutEasingFunction;
    typedef EasingFunction<EasingFunctionType::kCirInOut> CircularInOutEasingFunction;

    template <EasingFunctionType _FuncResTy, EasingFunctionType _FuncSusTy> struct EasingFunctionPair
    {
      EasingStage currentStage = EasingStage::kDefault;
      EasingFunctionBase::Real lastValue = 0.0f;
      EasingFunction<_FuncResTy> suspendingValue;
      EasingFunction<_FuncSusTy> resumingValue;

      inline EasingFunctionBase::Real onFrame()
      {
        switch (currentStage)
        {
        case EasingStage::kSuspending:
          return suspendingValue.onFrame();
        case EasingStage::kResuming:
          return resumingValue.onFrame();
        }

        return lastValue;
      }
      inline void operator += (EasingFunctionBase::Real tick)
      {
        switch (currentStage)
        {
        case EasingStage::kResuming:
          resumingValue += tick;
          if (resumingValue.saturated)
            currentStage = EasingStage::kResumed,
            lastValue = resumingValue.onFrame(),
            suspendingValue.reset();
          return;
        case EasingStage::kSuspending:
          suspendingValue += tick;
          if (suspendingValue.saturated)
            currentStage = EasingStage::kSuspended,
            lastValue = suspendingValue.onFrame(),
            resumingValue.reset();
          return;
        }
      }
      inline EasingFunctionBase::Boolean isResponsive()
      {
        return suspendingValue.saturated || resumingValue.saturated;
      }
      inline void SetTotal(EasingFunctionBase::Real value)
      {
        resumingValue.params.total = value;
        suspendingValue.params.total = value * 0.5f;
      }
      inline void isSaturated()
      {
        switch (currentStage)
        {
        case EasingStage::kSuspending:
        case EasingStage::kResuming:
          return 0;
        default:
          return 1;
        }
      }
      inline void reset()
      {
        suspendingValue.reset();
        resumingValue.reset();
      }
      inline void resume()
      {
        reset(); currentStage = EasingStage::kResuming;
      }
      inline void suspend()
      {
        reset(); currentStage = EasingStage::kSuspending;
      }
    };

    typedef EasingFunctionPair<EasingFunctionType::kLinIn, EasingFunctionType::kLinOut> LinearEasingFunctionPair;
    typedef EasingFunctionPair<EasingFunctionType::kQuaIn, EasingFunctionType::kQuaOut> QuadricEasingFunctionPair;
    typedef EasingFunctionPair<EasingFunctionType::kCubIn, EasingFunctionType::kCubOut> CubicEasingFunctionPair;
    typedef EasingFunctionPair<EasingFunctionType::kQrtIn, EasingFunctionType::kQrtOut> QuarticEasingFunctionPair;
    typedef EasingFunctionPair<EasingFunctionType::kQntIn, EasingFunctionType::kQntOut> QuinticEasingFunctionPair;
    typedef EasingFunctionPair<EasingFunctionType::kSinIn, EasingFunctionType::kSinOut> SinusoidalEasingFunctionPair;
    typedef EasingFunctionPair<EasingFunctionType::kExpIn, EasingFunctionType::kExpOut> ExponencialEasingFunctionPair;
    typedef EasingFunctionPair<EasingFunctionType::kCirIn, EasingFunctionType::kCirOut> CircularEasingFunctionPair;

    typedef struct BasicCountdown
    {
      EasingFunctionBase::Real totalSecs = 3.0f;
      EasingFunctionBase::Real totalElapsedSecs = 0.0f;
      EasingFunctionBase::Boolean hasFired = 0;

      EasingFunctionBase::Boolean onFrameMove(
        EasingFunctionBase::Real elapsed_secs
        )
      {
        if (hasFired) return hasFired;
        totalElapsedSecs += elapsed_secs;
        totalElapsedSecs = std::min(totalElapsedSecs, totalSecs);
        return hasFired = totalElapsedSecs == totalSecs;
      }

      EasingFunctionBase::Real progress() { return totalElapsedSecs / totalSecs; }
      void reset() { totalElapsedSecs = 0.0f; hasFired = 0; }
    } BasicCountdown;


  }
}
