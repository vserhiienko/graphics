#include "pch.h"
#include "SelectionAssist.h"


nyx::SelectionAssits::SelectionAssits(void)
{
  m_reservationSize = 1000;
  m_maxDistance = 30.0f;

  m_nextPointPosition = 0;
  m_pointCount = 0;

  m_addPoints = false;
}

nyx::SelectionAssits::~SelectionAssits(void)
{

}

void nyx::SelectionAssits::beginSelection()
{
  m_points.clear();
  m_lineSegs.clear();
  m_unprojPoints.clear();
  m_points.resize(m_reservationSize);
  m_lineSegs.resize(m_reservationSize);
  m_unprojPoints.resize(m_reservationSize);

  m_nextPointPosition = 0;
  m_addPoints = true;
}

void nyx::SelectionAssits::setScreenDimensions(float width, float height)
{
  screenDimensions.x = width;
  screenDimensions.y = height;
}

void nyx::SelectionAssits::addPoint(nv::vec2f const &point)
{
  if (m_points.empty() || m_nextPointPosition >= m_points.size() || !m_addPoints) return;

  if (m_pointCount)
  {
    nv::vec2f lineSegment = point - m_points[m_nextPointPosition - 1];
    float dist = nv::length(lineSegment);
    if (dist < m_maxDistance) return;

  }

  NVWindowsLog("adding point %2.2f %2.2f", point.x, point.y);
  m_points[m_nextPointPosition] = point;
  m_nextPointPosition++;
  m_pointCount = m_nextPointPosition;
}

void nyx::SelectionAssits::endSelection()
{
  // compile & optimize
  m_addPoints = false;

  generateLineSegs();
}

void nyx::SelectionAssits::generateLineSegs()
{
  if (m_pointCount > 1)
  {
    uint32_t pointIndex = 0;
    uint32_t pointNextIndex = 0;
    while (pointIndex < m_pointCount)
    {
      pointNextIndex = (pointIndex + 1) % m_pointCount;
      m_lineSegs[pointIndex].a = m_points[pointIndex];
      m_lineSegs[pointIndex].b = m_points[pointNextIndex];
      nyx::updateLineSegment(m_lineSegs[pointIndex]);
      pointIndex++;
    }
  }
}

void nyx::SelectionAssits::unprojectPoints(
  nyx::planef const &ground,
  float const *view,
  float const *projection,
  float const *viewInv
  )
{
  nv::matrix4f viewMat(view), viewInvMat, projMat(projection);
  if (!viewInv) viewInvMat = nv::inverse(viewMat);
  else viewInvMat.set_value(viewInv);

  if (m_pointCount > 1 && !m_addPoints)
  {
    uint32_t pointIndex = 0;
    while (pointIndex < m_pointCount)
    {
      nv::vec2f mouse = m_points[pointIndex];
      //mouse *= 1.0f; mouse += screenDimensions;

      mouse.x = screenDimensions.x - mouse.x;
      mouse.y = screenDimensions.y - mouse.y;
      nyx::unprojectPointOnPlane(
        mouse,
        ground,
        screenDimensions,
        viewInvMat._array,
        projMat._array,
        m_unprojPoints[pointIndex]
        );

      auto aspect = screenDimensions.x / screenDimensions.y;

      /*std::swap(
        m_unprojPoints[pointIndex].x,
        m_unprojPoints[pointIndex].z
        );*/

      //m_unprojPoints[pointIndex].x *= -1.0f; // / aspect;
      //m_unprojPoints[pointIndex].y *= -1.0f;
      //m_unprojPoints[pointIndex].z *= -1.0f; // / aspect;

      pointIndex++;
    }
  }
}

void nyx::SelectionAssits::updateProjectedPoints(
  float const *view,
  float const *projection
  )
{
  if (m_pointCount > 1 && !m_addPoints)
  {
    nv::vec4f obj;
    uint32_t pointIndex = 0;

    while (pointIndex < m_pointCount)
    {
      obj.x = m_unprojPoints[pointIndex].x;
      obj.y = m_unprojPoints[pointIndex].y;
      obj.z = m_unprojPoints[pointIndex].z;
      obj.w = 1.0f;

      m_points[pointIndex] = nyx::projectPoint(
        obj, screenDimensions,
        view, projection
        );

      pointIndex++;
    }
  }
}

void nyx::SelectionAssits::findPointsInSelection(nv::vec2f const *points, int *isSelected, uint32_t count, bool resetSelection)
{
  uint32_t pointIndex = 0;
  uint32_t selectedCount = 0;

  while (pointIndex < count)
  {
    auto isInside = nyx::isPointInsidePolygon(points[pointIndex], &m_points[0], m_pointCount);
    if (resetSelection) isSelected[pointIndex] = isInside;
    else if (isInside) isSelected[pointIndex] = isInside;
    if (isInside) selectedCount++;

    pointIndex++;
  }

  NVWindowsLog("%u points were selected", selectedCount);
}

void nyx::SelectionAssits::draw()
{
  uint32_t pointIndex = 0;
  if (pointIndex < m_pointCount)
  {
    if (m_addPoints) drawCircle(
      m_points[pointIndex].x,
      m_points[pointIndex].y,
      7.0f, 5,
      0.7f, 0.7f, 0.7f
      );
    else drawCircle(
      m_points[pointIndex].x,
      m_points[pointIndex].y,
      3.0f, 5
      );
    pointIndex++;
  }

  while (pointIndex < m_pointCount)
  {
    drawLine(
      m_points[pointIndex - 1].x,
      m_points[pointIndex - 1].y,
      m_points[pointIndex].x,
      m_points[pointIndex].y
      );
    drawCircle(
      m_points[pointIndex].x,
      m_points[pointIndex].y,
      3.0f, 5
      );

    pointIndex++;
  }

  if (m_pointCount > 1)
  {
    pointIndex--;

    if (m_addPoints) drawLine(
      m_points[0].x,
      m_points[0].y,
      m_points[pointIndex].x,
      m_points[pointIndex].y,
      0.0f, 0.7f, 0.1f
      );
    else drawLine(
      m_points[0].x,
      m_points[0].y,
      m_points[pointIndex].x,
      m_points[pointIndex].y
      );
  }

}

void nyx::SelectionAssits::drawLine(float x1, float y1, float x2, float y2, float r, float b, float g, float a)
{
  float aspect = screenDimensions.x / screenDimensions.y;
  y1 *= -1.0f;
  y2 *= -1.0f;
  x1 -= screenDimensions.x * 0.5f;
  x1 /= screenDimensions.x * 0.5f;
  y1 += screenDimensions.y * 0.5f;
  y1 /= screenDimensions.y * 0.5f;
  x2 -= screenDimensions.x * 0.5f;
  x2 /= screenDimensions.x * 0.5f;
  y2 += screenDimensions.y * 0.5f;
  y2 /= screenDimensions.y * 0.5f;

  //glLineWidth(10.0f);
  glBegin(GL_LINES);
  glColor4f(r, g, b, a);
  glVertex2f(x1, y1);
  glVertex2f(x2, y2);
  glEnd();
}

void nyx::SelectionAssits::drawCircle(
  float cx, float cy, float radius, int num_segments, float r, float b, float g, float a)
{
  radius /= std::min(screenDimensions.x, screenDimensions.y);
  float aspect = screenDimensions.x / screenDimensions.y;
  cy *= -1.0f;
  cx -= screenDimensions.x * 0.5f;
  cy += screenDimensions.y * 0.5f;
  cx /= screenDimensions.x * 0.5f;
  cy /= screenDimensions.y * 0.5f;

  float theta = 2 * 3.1415926 / float(num_segments);

  //precalculate the sine and cosine
  float c = cosf(theta);
  float s = sinf(theta);
  float t;

  //we start at angle = 0 
  float x = radius;
  float y = 0;

  //glLineWidth(10.0f);
  glBegin(GL_LINE_LOOP);
  glColor4f(r, g, b, a);
  for (int ii = 0; ii < num_segments; ii++)
  {
    //output vertex 
    glVertex2f(x + cx, y * aspect + cy);

    //apply the rotation matrix
    t = x;
    x = c * x - s * y;
    y = s * t + c * y;
  }
  glEnd();
}