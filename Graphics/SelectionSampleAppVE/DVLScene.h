/**
	@file	DVLScene.h

	This header defines the Scene interface
*/
#pragma once
struct sDVLMetadata;



/// Defines the scene information flags to be used in IDVLScene::RetrieveSceneInfo()
enum DVLSCENEINFO
{
	/// Retrieve the list of child nodes
	DVLSCENEINFO_CHILDREN							= 1,

	/// Retrieve the list of selected nodes
	DVLSCENEINFO_SELECTED							= 2,

	/// Retrieve the prefix for scene localization
	DVLSCENEINFO_LOCALIZATION_PREFIX				= 4,
};

/**
*	This structure is used to retrieve scene information from IDVLScene interface
*
*	\b NOTES 
*	\li You need to initialize the \b m_uSizeOfDVLSceneInfo member of the sDVLSceneInfo structure with its size.
*	\li You need to call ReleaseSceneInfo() after each RetrieveSceneInfo()
*/
#pragma pack(push, 1)
struct sDVLSceneInfo
{
	/// Size of the structure. You must set it to sizeof(sDVLSceneInfo) before calling RetrieveSceneInfo()
	uint16_t m_uSizeOfDVLSceneInfo;

	/// Number of child nodes if DVLSCENEINFO_CHILDREN flag is passed. Otherwise 0.
	uint32_t m_uChildNodesCount;

	/// Array of child node identifiers. The size of the array is defined by \b m_uChildNodesCount member.
	/// If number of child nodes is 0, then m_pChildNodes is \a NULL.
	DVLID *m_pChildNodes;

	/// Number of selected nodes if DVLSCENEINFO_SELECTED flag is passed. Otherwise 0.
	uint32_t m_uSelectedNodesCount;

	/// Array of identifiers of selected nodes. The size of the array is defined by \b m_uSelectedNodesCount member.
	/// If number of selected nodes is 0, then m_pSelectedNodes is \a NULL.
	DVLID *m_pSelectedNodes;

	/// If DVLSCENEINFO_LOCALIZATION_PREFIX flag is specified, this member variable contains a UTF-8 prefix of the scene for localization. May be (and usually is) \a NULL.
	char *m_szLocalizationPrefix;
};
#pragma pack(pop)



/// Defines the node information flags to be used in IDVLScene::RetrieveNodeInfo()
enum DVLNODEINFO
{
	/// Retrieve the node name
	DVLNODEINFO_NAME								= 0x0001,

	/// Retrieve the node asset id
	DVLNODEINFO_ASSETID								= 0x0002,

	/// Retrieve the node unique id
	DVLNODEINFO_UNIQUEID							= 0x0004,

	/// Retrieve parents of the node
	DVLNODEINFO_PARENTS								= 0x0008,

	/// Retrieve children of the node
	DVLNODEINFO_CHILDREN							= 0x0010,

	/// Retrieve node flags
	DVLNODEINFO_FLAGS								= 0x0020,

	/// Retrieve node opacity
	DVLNODEINFO_OPACITY								= 0x0040,

	/// Retrieve node highlight color
	DVLNODEINFO_HIGHLIGHT_COLOR						= 0x0080,
};

/**
*	This structure is used to retrieve node information from IDVLScene interface
*
*	\b NOTES
*	\li You need to initialize the \b m_uSizeOfDVLNodeInfo member of the sDVLNodeInfo structure with its size.
*	\li You need to call ReleaseNodeInfo() after each RetrieveNodeInfo()
*/
#pragma pack(push, 1)
struct sDVLNodeInfo
{
	/// You must set it to sizeof(sDVLNodeInfo) before calling RetrieveNodeInfo()
	uint16_t m_uSizeOfDVLNodeInfo;

	/// Receives the name of the node (UTF-8) if DVLNODEINFO_NAME is used. Otherwise \a NULL.
	char *m_szNodeName;

	/// Receives the asset id of the node (UTF-8) if DVLNODEINFO_ASSETID is used. Otherwise \a NULL.
	char *m_szAssetID;

	/// Receives the unique id of the node (UTF-8) if DVLNODEINFO_UNIQUEID is used. Otherwise \a NULL.
	char *m_szUniqueID;

	/// Receives the length of the parent node chain if DVLNODEINFO_PARENTS is used. Otherwise 0. May also be 0 if the node is a top level one.
	uint32_t m_uParentNodesCount;

	/// If DVLNODEINFO_PARENTS flag is specified it points to the array of \b m_uParentNodesCount items, containing DVLIDs of parent nodes
	/// starting from the scene tree top down to the current node.
	///
	/// If the scene is a top-level, the array is empty. If DVLNODEINFO_PARENTS flag is not used, it is \a NULL.
	DVLID *m_pParentNodes;

	/// Receives the number of child nodes if DVLNODEINFO_CHILDREN is used. Otherwise 0. May also be 0 if the node has no children.
	uint32_t m_uChildNodesCount;

	/// If DVLNODEINFO_CHILDREN flag is specified, it points to an array of \b m_uChildNodesCount items, containing DVLIDs of child nodes.
	///
	/// If a node has no children, the array is empty. If DVLNODEINFO_CHILDREN flag is not used, it is \a NULL.
	DVLID *m_pChildNodes;

	/// Receives a set of DVLNODEINFO_XXX flags that show which members of the structure are valid.
	uint32_t m_uFlags;

	/// Receives node opacity, which is a floating point value in the range of 0.0f .. 1.0f (0.0 - fully transparent (and thus invisible), 1.0 - fully opaque (the usual case))
	float m_fOpacity;

	/// Receives node highlight color which is a 32-bit ABGR value, where A is is highlight intensity.
	///
	/// If A=0, then there is no highlighting whatsoever.
	/// If A=255, then object if rendered in RGB color of m_uHighlightColor
	/// If A=128, then object color is blended as 50% of original color and 50% of highlight color
	uint32_t m_uHighlightColor;
};
#pragma pack(pop)



/**
*	This structure is used to retrieve metadata from IDVLScene interface
*
*	\b NOTES
*	\li You should not initialize members of sDVLMetadataInfo. RetrieveMetadata() will initialize all of them (even those that you do not query on)
*	\li You need to call ReleaseMetadata() after each RetrieveMetadata()
*/
#pragma pack(push, 1)
struct sDVLMetadataNamespace
{
	/// Receives the name of the metadata namespace (UTF-8)
	char *m_szName;

	/// A pointer to the sDVLMetadata structure, describing this namespace
	sDVLMetadata *m_pMetadata;
};

/// \struct sDVLMetadataNameValuePair
///
/// The structure describes a key-value pair of metadata. For example: "Price"="$100". Both strings are in the UTF-8 encoding.
struct sDVLMetadataNameValuePair
{
	/// The key of the pair
	char *m_szName;

	/// The value of the pair
	char *m_szValue;
};

/// \struct sDVLMetadata
/// The structure represents a single metadata namespace.
///
/// Namespaces may contain nested namespaces and key-value pairs.
struct sDVLMetadata
{
	/// Number of the nested namespaces
	uint32_t m_uNamespacesCount;

	/// Array of the nested namespaces of the \b m_uNamespacesCount size
	sDVLMetadataNamespace *m_pNamespaces;

	/// Number of the key-value pairs for this namespace
	uint32_t m_uNameValuePairsCount;

	/// Array of the key-value pairs of the \b m_uNameValuePairsCount size
	sDVLMetadataNameValuePair *m_pNameValuePairs;
};

/// \struct sDVLMetadataInfo
/// The only metadata-related structure that you initialize yourself. No need to fill all the fields, just set the correct \b m_uSizeOfDVLMetadataInfo value.
struct sDVLMetadataInfo
{
	/// The size of the structure, needs to be sizeof(sDVLMetadataInfo)
	uint16_t m_uSizeOfDVLMetadataInfo;

	/// A pointer to the private data, do not touch it.
	void *pimpl;

	/// A pointer to the sDVLMetadata structure that contains the root namespace of the metadata. If the node has no metadata, it is \a NULL
	sDVLMetadata *m_pMetadata;
};
#pragma pack(pop)



/// Recommended number of parts for a BuildPartsList() call
#define DVLPARTSLIST_RECOMMENDED_uMaxParts					1000

/// Recommended limitation of nodes in a single part for BuildPartsList() call
#define DVLPARTSLIST_RECOMMENDED_uMaxNodesInSinglePart		1000

/// Recommended limitation of part name's length for BuildPartsList() call
#define DVLPARTSLIST_RECOMMENDED_uMaxPartNameLength			200

/// Do not limit the number of parts in BuildPartsList() call
#define DVLPARTSLIST_UNLIMITED_uMaxParts					0

/// Do not limit the number of nodes in a part for BuildPartsList() call
#define DVLPARTSLIST_UNLIMITED_uMaxNodesInSinglePart		0

/// Do not limit the part's name length for BuildPartsList() call
#define DVLPARTSLIST_UNLIMITED_uMaxPartNameLength			0

/// \enum eDVLPartsListType
/// Defines the type of parts to put in the list
enum eDVLPartsListType
{
	/// Build a list using all the nodes
	DVLPARTSLISTTYPE_ALL,

	/// Build a list using only the visible nodes
	DVLPARTSLISTTYPE_VISIBLE,

	/// Build a list using only the nodes, consumed by a particular step (step DVLID is passed as a parameter to the BuildPartsList() call)
	DVLPARTSLISTTYPE_CONSUMED_BY_STEP,
};

/// \enum eDVLPartsListSort
/// Defines the sorting order for the parts list
enum eDVLPartsListSort
{
	/// Sort from A to Z
	DVLPARTSLISTSORT_NAME_ASCENDING,

	/// Sort from Z to A
	DVLPARTSLISTSORT_NAME_DESCENDING,

	/// Sort by the number of nodes in the part, parts with smaller number of nodes go first
	DVLPARTSLISTSORT_COUNT_ASCENDING,

	/// Sort by the number of nodes in the part, parts with larger number of nodes go first
	DVLPARTSLISTSORT_COUNT_DESCENDING,
};

/// \struct sDVLPartsListItem
/// Contains description of a single part.
#pragma pack(push, 1)
struct sDVLPartsListItem
{
	/// Name of the part in UTF-8 encoding, can be \a NULL.
	char *m_szName;

	/// Number of nodes belonging to this part.
	uint32_t m_uNodesCount;

	/// Array of nodes belonging to the part of the \b m_uNodesCount length.
	DVLID *m_pNodes;
};
#pragma pack(pop)

/**
*	This structure is used for building a parts list by IDVLScene interface
*
*	\b NOTES
*	\li You need to initialize the \b m_uSizeOfDVLPartsListInfo member with the size of the sDVLPartsListInfo structure.
*	\li You need to call IDVLScene::ReleasePartsList() after each IDVLScene::BuildPartsList()
*/
#pragma pack(push, 1)
struct sDVLPartsListInfo
{
	/// The size of the sDVLPartsListInfo structure. Needs to be filled before calling IDVLScene::BuildPartsList()
	uint16_t m_uSizeOfDVLPartsListInfo;

	/// A pointer to the private data, do not touch it.
	void *pimpl;

	/// The number of parts in the list
	uint32_t m_uPartsCount;

	/// Array of sDVLPartsListItem structures, defining the parts list. The length is defined in the \b m_uPartsCount member. May be \a NULL if the list is empty.
	sDVLPartsListItem *m_pParts;
};
#pragma pack(pop)



#pragma pack(push, 1)
/// \struct sDVLStep
/// Defines a single step or model view
///
/// @note	Use sDVLStep::m_ID to query a thumbnail using the IDVLScene::RetrieveThumbnail() method
struct sDVLStep
{
	/// The DVLID identifier of the step or model view.
	DVLID m_ID;

	/// The name of the step or model view, UTF-8 encoding. May be \a NULL.
	char *m_szName;

	/// The description of the step or model view, UTF-8 encoding. May be \a NULL.
	char *m_szDescription;
};

/// \struct sDVLProcedure
/// Defines a single procedure or portfolio. There is no internal difference between them, so the same structure is used for both.
struct sDVLProcedure
{
	/// The DVLID identifier of the procedure or portfolio
	DVLID m_ID;

	/// The name of the procedure or portfolio, UTF-8 encoding.
	char *m_szName;

	/// The number of steps in the procedure or the number of model views in the portfolio
	uint32_t m_uStepsCount;

	/// The array of sDVLStep structures defining the steps or model views. The length is defined by the \b m_uStepsCount member.
	sDVLStep *m_pSteps;
};

/**
*	This structure is used for retrieving steps & procedures via IDVLScene interface
*
*	\b Notes
*	\li You need to initialize the \b m_uSizeOfDVLProceduresInfo member with the size of the sDVLProceduresInfo structure
*	\li You need to call IDVLScene::ReleaseProcedures() after each IDVLScene::RetrieveProcedures()
*/
struct sDVLProceduresInfo
{
	/// The size of the \b sDVLProceduresInfo structure. Needs to be filled before calling IDVLScene::RetreiveProcedures()
	uint16_t m_uSizeOfDVLProceduresInfo;

	/// A pointer to the private data, do not touch it.
	void *pimpl;

	/// The number of the procedures in the scene
	uint32_t m_uProceduresCount;

	/// The array of procedures of the \b m_uProceduresCount size. May be \a NULL.
	sDVLProcedure *m_pProcedures;

	/// The number of portfolios in the scene
	uint32_t m_uPortfoliosCount;

	/// The array of portfolios of the \b m_uPortfoliosCount size. May be \a NULL.
	sDVLProcedure *m_pPortfolios;
};
#pragma pack(pop)



/// \enum eDVLFindNodeType
/// Defines the type of node search to perform
enum eDVLFindNodeType
{
	/// Find node by "node name"
	DVLFINDNODETYPE_NODE_NAME,

	/// Find node by "asset id" (asset id is stored inside some VDS files [it is optional])
	DVLFINDNODETYPE_ASSET_ID,

	/// Find node by "unique id" (unique id is stored inside some VDS files [it is optional])
	DVLFINDNODETYPE_UNIQUE_ID,

    /// Find node by "DS selector id" (unique id is stored inside some VDS files [it is optional])
    DVLFINDNODETYPE_DSSELECTOR_ID,
};

/// \enum eDVLFindNodeMode
/// Defines the string comparison mode
enum eDVLFindNodeMode
{
	/// Match nodes by comparing node name/assetid/uniqueid with "str" (case sensitive, fastest option [does buffer compare without UTF8 parsing])
	DVLFINDNODEMODE_EQUAL,

	/// Match nodes by comparing node name/assetid/uniqueid with "str" (case insensitive, UTF8-aware)
	DVLFINDNODEMODE_EQUAL_CASE_INSENSITIVE,

	/// Match nodes by finding "str" substring in node name/assetid/uniqueid (case sensitive, UTF8-aware)
	DVLFINDNODEMODE_SUBSTRING,

	/// Match nodes by finding "str" substring in node name/assetid/uniqueid (case insensitive, UTF8-aware)
	DVLFINDNODEMODE_SUBSTRING_CASE_INSENSITIVE,

	/// Match nodes by comparing first "strlen(str)" symbols of node name/assetid/uniqueid with "str" (case sensitive, UTF8-aware)
	DVLFINDNODEMODE_STARTS_WITH,

	/// Match nodes by comparing first "strlen(str)" symbols of node name/assetid/uniqueid with "str" (case insensitive, UTF8-aware)
	DVLFINDNODEMODE_STARTS_WITH_CASE_INSENSITIVE,
};

/// \struct sDVLNodeIDsArrayInfo
#pragma pack(push, 1)
struct sDVLNodeIDsArrayInfo
{
	/// The size of the \b sDVLNodeIDsArrayInfo structure. Needs to be filled before using sDVLNodeIDsArrayInfo with SDK methods
	uint16_t m_uSizeOfDVLFindNodesList;

	/// Number of nodes
	uint32_t m_uNodesCount;

	/// Array of node IDs. Array size is \b m_uNodesCount.
	DVLID *m_pNodes;
};
#pragma pack(pop)



/**
*	This class defines the scene interface
*
*	@note	IDVLScene uses reference counting to manage its lifetime. Do not forget to call the Release() method when you don't need the scene.
*/
class IDVLScene
{
protected:
	virtual ~IDVLScene() {}//can't be deleted using this interface, use Release()

public:
	/**
	*	Retrieves scene information into the provided structure
	*
	*	@param	flags	Bitfield combination of one or more ::DVLSCENEINFO flags
	*	@param	pInfo	A pointer to the sDVLSceneInfo structure that receives the scene information
	*
	*	@retval	DVLRESULT_BADFORMAT	If \b m_uSizeOfDVLSceneInfo is wrong
	*	@retval	DVLRESULT_BADARG	If \b pInfo == \a NULL
	*	@retval	DVLRESULT_OUTOFMEMORY	If some info could not be retrieved due to memory failure
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*
	*	\b Notes
	*	\li You need to initialize the \b m_uSizeOfDVLSceneInfo member of the sDVLSceneInfo structure with its size.
	*	\li	This method may be quite expensive
	*	\li	Try to limit the number of RetrieveSceneInfo() calls
	*	\li	Retrieve all info that you need in a single call with multiple flags set (it's faster than issuing multiple calls with less flags)
	*
	*
	*	\b Example
	*	\code{.cpp}
	*		sDVLSceneInfo si = {};
	*		si.m_uSizeOfDVLSceneInfo = sizeof(si);
	*		if (DVLSUCCEEDED(pScene->RetrieveSceneInfo(DVLSCENEINFO_SELECTED | DVLSCENEINFO_CHILDREN, &si)))
	*		{
	*			printf("%d nodes selected, %d top level items\n", si.m_uSelectedNodesCount, si.m_uChildNodesCount);
	*
	*			for (uint32_t i = 0; i < si.m_uSelectedNodesCount; i++)
	*			{
	*				DVLID selId = si.m_pSelectedNodes[i];
	*				// do something with selId
	*			}
	*
	*			for (uint32_t i = 0; i < si.m_uChildNodesCount; i++)
	*			{
	*				DVLID childId = si.m_pChildNodes[i];
	*				// do something with childId
	*			}
	*
	*			// release the data
	*			pScene->ReleaseSceneInfo(&si);
	*		}
	*
	*	\endcode
	*
	*/
	virtual DVLRESULT RetrieveSceneInfo(uint32_t flags, sDVLSceneInfo *pInfo) = 0;

	/**
	*	Releases all memory that was initialized in a previous call to RetrieveSceneInfo()
	*
	*	@param	pInfo	A pointer to a sDVLSceneInfo structure filled by RetrieveSceneInfo() call
	*
	*	@note	If \b m_uSizeOfDVLSceneInfo is wrong, \b pInfo will not be deleted (and you'll get a memory leak)
	*/
	virtual void ReleaseSceneInfo(sDVLSceneInfo *pInfo) = 0;

	/**
	*	Used for quick interrogation of "node selection", for example "Show selected nodes" is only available if there is at least 1 invisible node selected
	*
	*	@param	pTotalSelectedNodesCount	Pointer to the total number of selected nodes. Set to \a NULL if not needed.
	*	@param	pVisibleSelectedNodesCount	Pointer to the number of visible selected nodes. Set to \a NULL if not needed.
	*	@param	pHiddenSelectedNodesCount	Pointer to the number of hidden selected nodes. Set to \a NULL if not needed.
	*/
	virtual void GetNodeSelectionInfo(uint32_t *pTotalSelectedNodesCount, uint32_t *pVisibleSelectedNodesCount, uint32_t *pHiddenSelectedNodesCount) = 0;

	/**
	*	Executes a particular action on the scene, see ::DVLSCENEACTION enum
	*
	*	\b Example
	*
	*	\code{.cpp}
	*		pScene->PerformAction(DVLSCENEACTION_SHOW_ALL);
	*		pScene->PerformAction(DVLSCENEACTION_HIDE_SELECTED);
	*	\endcode
	*
	*/
	virtual void PerformAction(DVLSCENEACTION action) = 0;

	/**
	*	Retrieves node information into the provided structure
	*
	*	@param	id	::DVLID of the node that you query on
	*	@param	flags	Bitfield combination of one or more ::DVLNODEINFO flags
	*	@param	pInfo	A pointer to the sDVLNodeInfo structure that receives the data
	*
	*	@retval	DVLRESULT_BADFORMAT	If \b m_uSizeOfDVLNodeInfo is wrong
	*	@retval	DVLRESULT_BADARG	If \b pInfo == \a NULL
	*	@retval	DVLRESULT_NOINTERFACE	If "id" is pointing to an item of different type
	*	@retval	DVLRESULT_OUTOFMEMORY	If some info could not be retrieved due to memory failure
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*
	*	\b Notes
	*	\li You need to initialize the \b m_uSizeOfDVLNodeInfo member of the sDVLNodeInfo structure with its size.
	*	\li This method may be quite expensive
	*	\li Try to limit the number of RetrieveNodeInfo() calls
	*	\li Retrieve all info that you need in a single call with multiple flags set (it's faster than issuing multiple calls with less flags)
	*
	*	\b Example
	*
	*	\code{.cpp}
	*		
	*		DVLID nodeId = ....; // get a node id somehow, for example via RetrieveSceneInfo()
	*		sDVLNodeInfo ni = {};
	*		ni.m_uSizeOfDVLNodeInfo = sizeof(ni);
	*		if (DVLSUCCEEDED(pScene->RetrieveNodeInfo(nodeId, DVLNODEINFO_NAME | DVLNODEINFO_CHILDREN, &ni)))
	*		{
	*			printf("node '%s' has %d children\n", ni.m_szNodeName, ni.m_uChildNodesCount);
	*
	*			for (uint32_t i = 0; i < ni.m_uChildNodesCount; i++)
	*			{
	*				DVLID childId = ni.m_pChildNodes[i];
	*				// do something with childId
	*			}
	*
	*			// release the data
	*			pScene->ReleaseNodeInfo(&ni);
	*		}
	*
	*	\endcode
	*/
	virtual DVLRESULT RetrieveNodeInfo(DVLID id, uint32_t flags, sDVLNodeInfo *pInfo) = 0;

	/**
	*	Releases all memory that was initialized in a previous call to RetrieveNodeInfo()
	*
	*	@param	pInfo	A pointer to the sDVLNodeInfo structure filled by the RetrieveNodeInfo() call
	*
	*	@note	If \b m_uSizeOfDVLNodeInfo is wrong, \b pInfo will not be deleted (and you'll get a memory leak)
	*/
	virtual void ReleaseNodeInfo(sDVLNodeInfo *pInfo) = 0;



	/**
	*	Retrieves metadata into the provided structure
	*
	*	@param	id		::DVLID of the item that you query on (can be node, material, step view, etc...)
	*	@param	pInfo	A pointer to the sDVLMetadataInfo structure that receives the data
	*
	*	@retval	DVLRESULT_BADFORMAT	If \b m_uSizeOfDVLMetadata is wrong
	*	@retval	DVLRESULT_BADARG	If \b pInfo == \a NULL
	*	@retval	DVLRESULT_OUTOFMEMORY	If metadata data could not be created due to memory failure
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_PROCESSED	If item does not have metadata (in such case sDVLMetadataInfo::m_pMetadata == \a NULL)
	*	@retval	DVLRESULT_OK	If some metadata has been retrieved
	*
	*	\b Notes
	*	\li You need to initialize the \b m_uSizeOfDVLMetadata member of the sDVLMetadataInfo structure before calling the function.
	*
	*	\b Example
	*
	*	\code{.cpp}
	*		DVLID nodeId = ....; // get a node id somehow, for example via RetrieveSceneInfo()
	*		sDVLMetadataInfo mi = {};
	*		mi.m_uSizeOfDVLMetadata = sizeof(mi);
	*		if (DVLSUCCEEDED(pScene->RetrieveMetadata(nodeId, &mi)))
	*		{
	*			sDVLMetadata *pRoot = mi.m_pMetadata;
	*			if (pRoot)
	*				printf("The root medatadata has %d keys and %d namespaces\n", pRoot->m_uNameValuePairsCount, pRoot->m_uNamespacesCount);
	*			else
	*				printf("The node doesn't have any metadata\n");
	*			pScene->ReleaseMetadata(&mi);
	*		}
	*	\endcode
	*
	*/
	virtual DVLRESULT RetrieveMetadata(DVLID id, sDVLMetadataInfo *pInfo) = 0;

	/**
	*	Releases all memory that was initialized in a previous call to RetrieveMetadata()
	*
	*	@param	pInfo	A pointer to the sDVLMetadataInfo structure filled by the RetrieveMetadata() call
	*
	*	@note	If \b m_uSizeOfDVLMetadata is wrong, \b pInfo will not be deleted (and you'll get a memory leak)
	*/
	virtual void ReleaseMetadata(sDVLMetadataInfo *pInfo) = 0;



	/**
	*	Retrieves a thumbnail for specified ::DVLID into the provided buffer
	*
	*	@param	id	::DVLID of an item (item type may be arbitrary)
	*	@param 	pThumbnail	A pointer to the sDVLImage structure that receives the data.
	*
	*	@retval	DVLRESULT_BADFORMAT	If \b m_uSizeOfDVLMetadata is wrong
	*	@retval	DVLRESULT_BADARG	If \b pInfo == \a NULL
	*	@retval	DVLRESULT_OUTOFMEMORY	If thumbnail could not be retrieved due to memory failure
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_NOTFOUND	If item does not have thumbnail (in such case sDVLImage::m_pImageBuffer == \a NULL)
	*	@retval	DVLRESULT_OK	If thumbnail was successfully retrieved
	*	
	*	\b Notes
	*	\li Most item types don't have thumbnails
	*	\li The data you receive are encoded (either JPEG or PNG), so you have to decode them first. This needs to be done using the OS-specific routines.
	*
	*	\b Example
	*
	*	\code{.cpp}
	*		DVLID stepId = ...; // get a step identifier somehow
	*		sDVLImage img = {};
	*		img.m_uSizeOfDVLImage = sizeof(img);
	*		if (DVLSUCCEEDED(pScene->RetrieveThumbnail(stepId, &img)))
	*		{
	*			LoadImageFromData(img.m_uImageSize, img.m_pImageBuffer); // use the image data
	*			pScene->ReleaseThumbnail(&mi);
	*		}
	*	\endcode
	*
	*/
	virtual DVLRESULT RetrieveThumbnail(DVLID id, sDVLImage *pThumbnail) = 0;

	/**
	*	Releases all memory that was initialized in a previous call to RetrieveThumbnail()
	*
	*	@param	pThumbnail	A pointer to a sDVLImage structure filled by the RetrieveThumbnail() call
	*
	*	@note	If \b m_uSizeOfDVLThumbnail is wrong, \b pThumbnail will not be deleted (and you'll get a memory leak)
	*/
	virtual void ReleaseThumbnail(sDVLImage *pThumbnail) = 0;



	/**
	*	Builds a parts list and stores it inside the provided structure
	*
	*	@param	uMaxParts	Maximum number of parts required. For example, it doesn't make sense to have 100,000 parts on iPad. 0 for unlimited.
	*	@param	uMaxNodesInSinglePart	Maximum number of nodes in a single part to be saved. If more nodes belong to a part, they will be ignored. For example, more than 1000 nodes per part is probably more than enough for iPad. 0 for unlimited.
	*	@param	uMaxPartNameLength	Maximum length of part name. For example, no point in having 1000 symbol strings on iPad (cause there is no way to display them anyways). 0 for unlimited.
	*	@param	eType	Type of listing: full, visible only, only consumed by \b idConsumedStep step
	*	@param	eSort	Type of sorting: A..Z, Z..A, etc
	*	@param	idConsumedStep	Only used when \b eType == ::DVLPARTSLISTTYPE_CONSUMED_BY_STEP, set to ::DVLID_INVALID or any other value if not ::DVLPARTSLISTTYPE_CONSUMED_BY_STEP
	*	@param	szSubstring	Only parts that include \b szSubstring are returned. Set to \a NULL if filtering is not required.
	*	@param	pInfo	A pointer to a sDVLPartsListInfo structure that receives the data
	*
	*	@retval	DVLRESULT_BADFORMAT	If \b m_uSizeOfDVLPartsListInfo is wrong
	*	@retval	DVLRESULT_BADARG	If \b pInfo == \a NULL
	*	@retval	DVLRESULT_BADARG	If \b eType == ::DVLPARTSLISTTYPE_CONSUMED_BY_STEP and \b idConsumedStep == ::DVLID_INVALID
	*	@retval	DVLRESULT_OUTOFMEMORY	If parts list could not be created due to memory failure
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_OK	If all went fine
	*
	*	\b Notes
	*	\li This call may be expensive!!! Draw hourglass cursor or something like that.
	*	\li Scenes may be huge (millions of nodes), it may be a good idea to set the appropriate limitations for the list
	*	\li You need to initialize the \b m_uSizeOfDVLPartsListInfo member with the size of the \b sDVLPartsListInfo structure
	*	\li Use ReleasePartsList() method to release the data
	*
	*	\b Example
	*	
	*	\code{.cpp}
	*		sDVLPartsListInfo pl = {};
	*		pl.m_uSizeOfDVLPartsListInfo = sizeof(pl);
	*		if (DVLSUCCEEDED(pScene->BuildPartsList(
	*							DVLPARTSLIST_RECOMMENDED_uMaxParts, 			// use recommended list length limit
	*							DVLPARTSLIST_RECOMMENDED_uMaxNodesInSinglePart,	// use recommended nodes in the part limit
	*							DVLPARTSLIST_UNLIMITED_uMaxPartNameLength,		// do not limit the length of the name
	*							DVLPARTSLISTTYPE_ALL,							// build the list using all the nodes
	*							DVLPARTSLISTSORT_NAME_ASCENDING,				// sort the list ascending
	*							DVLID_INVALID,									// do not provide a consumed step id
	*							NULL,											// do not filter
	*							&pl)))
	*		{
	*			printf("There are %d parts in the list\n", pl.m_uPartsCount);
	*			for (uint32_t i = 0; i < pl.m_uPartsCount; i++)
	*			{
	*				sDVLPartListItem *item = pl.m_pParts[i];
	*				printf("%d. name = '%s', has %d nodes\n", i, item->m_szName, item->m_uNodesCount);
	*			}
	*			pScene->ReleasePartsList(&pl);
	*		}
	*	\endcode
	*
	*/
	virtual DVLRESULT BuildPartsList(uint32_t uMaxParts, uint32_t uMaxNodesInSinglePart, uint32_t uMaxPartNameLength,
		eDVLPartsListType eType, eDVLPartsListSort eSort, DVLID idConsumedStep, const char *szSubstring, sDVLPartsListInfo *pInfo) = 0;

	/**
	*	Releases all memory that was initialized in a previous call to BuildPartsList()
	*
	*	@param	pInfo	A pointer to a sDVLPartsListInfo structure filled by a previous BuildPartsList() call
	*
	*	@note	If \b m_uSizeOfDVLPartsListInfo is wrong, \b pInfo will not be deleted (and you'll get a memory leak)
	*/
	virtual void ReleasePartsList(sDVLPartsListInfo *pInfo) = 0;



	/**
	*	Finds a list of scene nodes by matching them using a string parameter [different search types are possible: by name, asset id or unique id]
	*
	*	@param	type	One of eDVLFindNodeType values
	*	@param	mode	One of eDVLFindNodeMode values
	*	@param	str		String identifier to search on (depends on "type" value)
	*	@param	pInfo	A pointer to the sDVLNodeIDsArrayInfo structure that receives the list of found nodes
	*
	*	@retval	DVLRESULT_BADARG	If \b pInfo == \a NULL
	*	@retval	DVLRESULT_BADFORMAT	If \b m_uSizeOfDVLFindNodesList is wrong
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_PROCESSED	If scene does not have nodes which satisfy a query
	*	@retval	DVLRESULT_OK	If some nodes have been retrieved
	*/
	virtual DVLRESULT FindNodes(eDVLFindNodeType type, eDVLFindNodeMode mode, const char *str, sDVLNodeIDsArrayInfo *pInfo) = 0;

	/**
	*	Releases all memory that was allocated by SDK in the provided pInfo structure (For example after using FindNodes())
	*
	*	@param	pInfo	A pointer to a sDVLNodeIDsArrayInfo structure
	*
	*	@note	If \b m_uSizeOfDVLFindNodesList is wrong, \b pInfo will not be deleted (and you'll get a memory leak)
	*/
	virtual void ReleaseNodeIDsArrayInfo(sDVLNodeIDsArrayInfo *pInfo) = 0;



	/**
	*	Retrieves a list of procedures and portfolios in the scene
	*
	*	@param pInfo A pointer to a sDVLProceduresInfo structure that receives the data
	*
	*	@retval	DVLRESULT_BADFORMAT	If \b m_uSizeOfDVLProceduresInfo is wrong
	*	@retval	DVLRESULT_BADARG	If \b pInfo == \a NULL
	*	@retval	DVLRESULT_OUTOFMEMORY	In case of memory failure
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_OK	If all went fine
	*
	*	\b Notes
	*	\li You need to initialize the \b m_uSizeOfDVLProceduresInfo member of the structure with its size
	*	\li Use ReleaseProcedures() to release the data
	*	\li Both procedures and portfolios use the same \b sDVLProcedure structure
	*
	*	\b Example
	*
	*	\code{.cpp}
	*		sDVLProceduresInfo procs = {};
	*		procs.m_uSizeOfDVLProceduresInfo = sizeof(procs);
	*		if (DVLSUCCEEDED(pScene->RetrieveProcedures(&procs)))
	*		{
	*			printf("The scene has %d procedures and %d portfolios\n", procs.m_uProceduresCount, procs.m_uPortfoliosCount);
	*			for (uint32_t i = 0; i < procs.m_uProceduresCount; i++)
	*			{
	*				sDVLProcedure *p = procs.m_pProcedures[i];
	*				printf("procedure #%d: name = '%s', steps = %d\n", i, p->m_szName, p->m_uStepsCount);
	*			}
	*
	*			for (uint32_t i = 0; i < procs.m_uPortfoliosCount; i++)
	*			{
	*				sDVLProcedure *p = procs.m_pPortfolios[i];
	*				printf("portfolio #%d: name = '%s', model views = %d\n", i, p->m_szName, p->m_uStepsCount);
	*			}
	*
	*			pScene->ReleaseProcedures(&procs);
	*		}
	*	\endcode
	*
	*/
	virtual DVLRESULT RetrieveProcedures(sDVLProceduresInfo *pInfo) = 0;

	/**
	*	Releases all memory that was initialized in a previous call to RetrieveSteps()
	*
	*	@param	pInfo	A pointer to a sDVLProceduresInfo structure filled by the RetrieveSteps() call
	*
	*	@note	If \b m_uSizeOfDVLProceduresInfo is wrong, \b pInfo will not be deleted (and you'll get a memory leak)
	*/
	virtual void ReleaseProcedures(sDVLProceduresInfo *pInfo) = 0;



	/**
	*	Activates a step by playing its animation. Optionally plays steps that go after this step
	*
	*	@param	id					The identifier of the step or model view to activate
	*	@param	bFromTheBeginning	Play step from beginning or from currently paused position
	*	@param	bContinueToTheNext	What to do after finishing playing the current step: play next steps or stop
	*
	*	@retval	DVLRESULT_NOTFOUND	If Step with such ID does not exist
	*	@retval	DVLRESULT_PROCESSED	If \b bFromTheBeginning is \a false and current step is already playing (not an error, the step just continues to play)
	*	@retval	DVLRESULT_INVALIDCALL	If \b bFromTheBeginning is \a false, \b bContinueToTheNext is \a true and current step has stopped playing and doesn't have next step (i.e. we're at the end of all steps)
	*	@retval	DVLRESULT_OK	If a step was activated
	*
	*	\b Notes
	*	\li If \b bFromTheBeginning is \a false and current step is not the same as \b id, then \b bFromTheBeginning is treated as \a true
	*	\li If current step has finished playing and \b id points to current step, then one of the three things happen:
	*	\li If \b bContinueToTheNext is \a true and next step exists, then next step is activated
	*	\li If \b bContinueToTheNext is \a true and next step doesn't exist, then ::DVLRESULT_INVALIDCALL is returned
	*	\li If \b bContinueToTheNext is \a false then current step is played again from the start (as if \b bFromTheBeginning is \a true)
	*/
	virtual DVLRESULT ActivateStep(DVLID id, bool bFromTheBeginning, bool bContinueToTheNext) = 0;

	/**
	*	Pauses the current step, if any
	*
	*	@retval	DVLRESULT_PROCESSED	If current step is already paused (in this case it is still kept paused)
	*	@retval	DVLRESULT_NOTFOUND	If there is no "current step" playing
	*	@retval	DVLRESULT_OK	If current step was paused
	*/
	virtual DVLRESULT PauseCurrentStep() = 0;



	/**
	*	Changes some node flags
	*
	*	@param	id	DVLID of the node that you need to change flags on
	*	@param	flags	Bitfield combination of one or more ::DVLNODEFLAG flags
	*	@param	flagop	::DVLFLAGOP_SET, ::DVLFLAGOP_CLEAR, ::DVLFLAGOP_INVERT possibly ORed with ::DVLFLAGOP_MODIFIER_RECURSIVE
	*
	*	@retval	DVLRESULT_NOINTERFACE	If "id" is pointing to an item of different type
	*	@retval	DVLRESULT_PROCESSED	If flags were not changed (not an error, DVLSUCCEEDED(DVLRESULT_PROCESSED) == true). For example when hiding a hidden node
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_OK	If some flags were changed
	*/
	virtual DVLRESULT ChangeNodeFlags(DVLID id, uint32_t flags, DVLFLAGOPERATION flagop) = 0;



	/**
	*	Sets node opacity
	*
	*	@param	id	DVLID of the node that you need to set opacity
	*	@param	opacity	Opacity amount (0.0 - fully transparent, 1.0 - fully opaque)
	*
	*	@retval	DVLRESULT_FAIL	If "id" is incorrect
	*	@retval	DVLRESULT_NOTFOUND	If node with specified "id" doesn't exist
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_OK	If node opacity was modified
	*/
	virtual DVLRESULT SetNodeOpacity(DVLID id, float opacity) = 0;

	/**
	*	Sets node highlight color
	*
	*	@param	id	DVLID of the node that you need to set highlight color for
	*	@param	color	Highlight color value (32-bit ABGR, where A is amount of blending between material color and highlight color)
	*
	*	@note	Make sure you set 'A' component to non-zero value, or otherwise highlight will not be visible (as the amount would be '0').
	*			For example, 0xFF0000FF gives 100% red highlight. 0x7F00FF00 gives 50% green highlight.
	*	@note	Set "color" to 0 to clear highlighting
	*
	*	@retval	DVLRESULT_FAIL	If "id" is incorrect
	*	@retval	DVLRESULT_NOTFOUND	If node with specified "id" doesn't exist
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_OK	If node highlight color was modified
	*/
	virtual DVLRESULT SetNodeHighlightColor(DVLID id, uint32_t color) = 0;



	/**
	*	Retrieves node world matrix
	*
	*	@param	id	DVLID of the node
	*	@param	wtm	Node world matrix will be put into this variable
	*
	*	@note	This matrix is re-evaluated every frame during animation.
	*	@note	If you specify matrix via SetNodeWorldMatrix(), it will override node matrix until SetNodeWorldMatrix(id, NULL) is performed.
	*
	*	\b Example
	*
	*	\code{.cpp}
	*		sDVLMatrix mat;
	*		g_pScene->GetNodeWorldMatrix(id, mat);
	*		mat.m[3][2] += 1.f;//Z up
	*		g_pScene->SetNodeWorldMatrix(id, &mat);
	*	\endcode
	*
	*	@retval	DVLRESULT_FAIL	If "id" is incorrect
	*	@retval	DVLRESULT_NOTFOUND	If node with specified "id" doesn't exist
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_OK	If matrix was successfully retrieved
	*/
	virtual DVLRESULT GetNodeWorldMatrix(DVLID id, sDVLMatrix &wtm) const = 0;

	/**
	*	Sets node world matrix
	*
	*	@param	id	DVLID of the node
	*	@param	pwtm	Pointer to a world matrix to set. Set to NULL if you want to restore default matrix.
	*
	*	@note	If you set matrix with this call, it will override node matrix evaluation in DVL until SetNodeWorldMatrix(id, NULL) is performed. I.e. animation for this node will not play.
	*
	*	\b Example
	*
	*	\code{.cpp}
	*		sDVLMatrix mat;
	*		g_pScene->GetNodeWorldMatrix(id, mat);
	*		mat.m[3][2] += 1.f;//Z up
	*		g_pScene->SetNodeWorldMatrix(id, &mat);
	*	\endcode
	*
	*	@retval	DVLRESULT_FAIL	If "id" is incorrect
	*	@retval	DVLRESULT_NOTFOUND	If node with specified "id" doesn't exist
	*	@retval	DVLRESULT_NOTINITIALIZED	If scene is not properly initialized
	*	@retval	DVLRESULT_OK	If matrix was successfully modified
	*/
	virtual DVLRESULT SetNodeWorldMatrix(DVLID id, const sDVLMatrix *pwtm) = 0;

	/**
	*	Executes a query
	*	
	*	@param	str	Query string
	*
	*	@retval	DVLRESULT_FAIL	If query wasn't performed
	*	@retval DVLRESULT_OK	If query was successfully performed
	*/
	virtual DVLRESULT ExecuteQuery(const char *str) = 0;



	/**
	*	Increases the internal reference counter
	*
	*	The scene object maintains a counter of references to itself. Each Retain() call increments it, each Release() call decrements. The scene gets deleted
	*		when the counter becomes zero. When you load a scene, the counter is set to 1. You then need to call Release() to delete the scene. However, if you
	*		attach the scene to a renderer, it will keep its own reference, so your Release() call will not delete the scene unless the renderer releases it, as well.
	*/
	virtual void Retain() = 0;

	/**
	*	Releases the scene and deletes it if reference count is zero.
	*
	*	See Retain() method for more details on reference counting.
	*/
	virtual void Release() = 0;
};
