/**
	@file	DVLVersion.h

	This header defines current DVL version.
*/
#pragma once


/// The major version of the library.
#define DVL_VERSION_MAJOR											2

/// The minor version of the library
#define DVL_VERSION_MINOR											2

/// The build number of the library
#define DVL_VERSION_BUILD_NUMBER									0


// Version history:
// * 1.0.0 - "SAP Visual Enterprise Viewer" 1.0.0 AppStore release on 23.08.2012. http://itunes.apple.com/us/app/sap-visual-enterprise-viewer/id552912296

// * 1.1.0 - "SAP Visual Enterprise SDK for Apps" 1.0 SAP community release on 24.08.2012. https://community.wdf.sap.corp/sbs/groups/sap-visual-enterprise-developer-sdk-for-apps
//		Added Bump mapping support
//		Added ground shadow

// * 2.0.0
//		VDS2.0 file format: line styles, detail views
//		FindNodes(), SetNodeOpacity() and SetNodeHighlightColor() methods added
//
// * 2.0.6 - FPS calculation improved
// * 2.0.7 - moved to 1.1-COR branch
// * 2.0.8 (30.10.2012) - FPS calculation improved. VE Viewer 1.1 is based on this.

// * 2.1.0
//		New rendering algorithm (via occlusion culling) work in progress
//		Anaglyph stereo, Skylight, 2X FSAA and 1/2 upsampling
//		Fixed target camera animation
//		HTTP streaming protocol now supported
//		Dynamic loading and other major changes
//		NotifyFrameStarted(), NotifyClientFrameFinished() and GetDebugInfoString()
//		SetNodeWorldMatrix() / GetNodeWorldMatrix()
//		many rendering fixes

// * 2.2.0 (07.09.2013)
//		ExecuteQuery() - query engine integration
//		RenderFrameEx() - ability to specify view and projection matrices explicitly
