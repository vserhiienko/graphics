#include "pch.h"
#include "VEScene.h"

#include <iostream>
#include <sstream>

bool nyx::Scene::getWorldMatrix(
  CoreObject *scene,
  SceneNode::NodeId const &id,
  nv::matrix4f &worldMatrix
  )
{
  DVLRESULT dvlResult = scene->GetNodeWorldMatrix(
    id, (sDVLMatrix&)worldMatrix
    );
  if (DVLSUCCEEDED(dvlResult)) return true;
  else switch (dvlResult)
  {
  case DVLRESULT_NOTINITIALIZED:
    NVWindowsLog(
      "error: failed to get world matrix for 0x%0x: scene is not properly initialized"
      , (uint32_t)(id & 0xffffffff)
      );
    break;
  case DVLRESULT_NOTFOUND:
    NVWindowsLog(
      "error: failed to get world matrix for 0x%0x: id does not exist"
      , (uint32_t)(id & 0xffffffff)
      );
    break;
  case DVLRESULT_FAIL:
    NVWindowsLog(
      "error: failed to get world matrix for 0x%0x: incorrect id"
      , (uint32_t)(id & 0xffffffff)
      );
    break;
  default:
    NVWindowsLog(
      "error: failed to get world matrix for 0x%0x: (%d)"
      , (uint32_t)(id & 0xffffffff)
      , (int)dvlResult
      );
    break;
  }

  /*switch (dvlResult)
  {
  case DVLRESULT_FILENOTFOUND:
  break;
  case DVLRESULT_NOTINITIALIZED:
  break;
  case DVLRESULT_WRONGVERSION:
  break;
  case DVLRESULT_MISSINGEXTENSION:
  break;
  case DVLRESULT_ACCESSDENIED:
  break;
  case DVLRESULT_NOINTERFACE:
  break;
  case DVLRESULT_OUTOFMEMORY:
  break;
  case DVLRESULT_INVALIDCALL:
  break;
  case DVLRESULT_NOTFOUND:
  break;
  case DVLRESULT_BADARG:
  break;
  case DVLRESULT_FAIL:
  break;
  case DVLRESULT_BADTHREAD:
  break;
  case DVLRESULT_BADFORMAT:
  break;
  case DVLRESULT_FILEERROR:
  break;
  case DVLRESULT_NOTIMPLEMENTED:
  break;
  case DVLRESULT_HARDWAREERROR:
  break;
  case DVLRESULT_INTERRUPTED:
  break;
  case DVLRESULT_FALSE:
  break;
  case DVLRESULT_OK:
  break;
  case DVLRESULT_PROCESSED:
  break;
  case DVLRESULT_ALREADYINITIALIZED:
  break;
  default:
  break;
  }*/


  return false;
}

bool nyx::Scene::getWorldMatrix(
  SceneNode::NodeId const &id,
  nv::matrix4f &worldMatrix
  )
{
  return getWorldMatrix(coreObj, id, worldMatrix);
}

nyx::Scene::Scene()
{
  modelNode = NULL;
  modelNodeName = "Model";
}

void nyx::Scene::initialize(SceneNode::NodeId const &nodeId)
{
  if (nodeId == NULL || nodeId == DVLID_INVALID)
  {
    NVWindowsLog("error: invalid node id");
    return;
  }

  nodes.push_back(new SceneNode(this));
  nodes.back()->id = nodeId;
  if (!getWorldMatrix(nodes.back()->id, nodes.back()->pose))
  {
    NVWindowsLog(
      "error: failed to get world matrix for 0x%0x"
      , (uint32_t)(nodes.back()->id & 0xffffffff)
      );
  }

  DVLRESULT dvlResult;
  sDVLNodeInfo nodeInfo;
  nyx::initInfo(nodeInfo);

  dvlResult = coreObj->RetrieveNodeInfo(nodeId, nodeFlags, &nodeInfo);
  if (DVLFAILED(dvlResult))
  {
    NVWindowsLog("error: failed to retrieve scene info");
    return;
  }
  else
  {
    nodes.back()->printInfo(nodeInfo);
    for (uint32_t nodeIndex = 0; nodeIndex < nodeInfo.m_uChildNodesCount; nodeIndex++)
      initialize(nodeInfo.m_pChildNodes[nodeIndex]);
    coreObj->ReleaseNodeInfo(&nodeInfo);
  }
}

void nyx::Scene::initialize(CoreObject *scene)
{
  if (!scene)
  {
    NVWindowsLog("error: null scene");
    return;
  }

  DVLRESULT dvlResult;
  sDVLSceneInfo sceneInfo;
  nyx::initInfo(sceneInfo);

  coreObj = scene;
  dvlResult = coreObj->RetrieveSceneInfo(DVLSCENEINFO_CHILDREN, &sceneInfo);
  if (DVLFAILED(dvlResult))
  {
    NVWindowsLog("error: failed to retrieve scene info");
    return;
  }

  for (uint32_t nodeIndex = 0; nodeIndex < sceneInfo.m_uChildNodesCount; nodeIndex++)
    initialize(sceneInfo.m_pChildNodes[nodeIndex]);
  coreObj->ReleaseSceneInfo(&sceneInfo);

  initializeModelNode();
}

static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
  std::string item;
  std::stringstream ss(s);
  while (std::getline(ss, item, delim)) elems.push_back(item);
  return elems;
}

static std::vector<std::string> split(const std::string &s, char delim)
{
  std::vector<std::string> elems;
  split(s, delim, elems);
  return elems;
}

void nyx::Scene::initializeModelNode()
{
  centreFound = false;
  dimensionsFound = false;

  SceneNode::Iterator nodeIt = nodes.begin();
  for (; nodeIt != nodes.end(); nodeIt++)
  {
    sDVLNodeInfo nodeInfo;
    nyx::initInfo(nodeInfo);

    SceneNode *node = (*nodeIt);
    if (DVLSUCCEEDED(coreObj->RetrieveNodeInfo(node->id, DVLNODEINFO_NAME, &nodeInfo)))
    {
      if (modelNodeName == nodeInfo.m_szNodeName)
      {
        std::string dims;
        sDVLMetadataInfo metadataInfo;

        modelNode = node;
        nyx::initInfo(metadataInfo);

        if (DVLSUCCEEDED(coreObj->RetrieveMetadata(node->id, &metadataInfo)) && metadataInfo.m_pMetadata)
        {
          if (node->find(*metadataInfo.m_pMetadata, "Keel", "Keel_Size", dims))
          {
            std::vector<std::string> elements;
            split(dims, ';', elements);

            if (elements.size() == 2)
            {
              dimensions.x = (float)atof(elements[0].c_str());
              dimensions.y = (float)atof(elements[1].c_str());
              centreFound = true;
            }
          }
          if (node->find(*metadataInfo.m_pMetadata, "Keel", "Keel_Centre", dims))
          {
            std::vector<std::string> elements;
            split(dims, ';', elements);

            if (elements.size() == 2)
            {
              centre.x = (float)atof(elements[0].c_str());
              centre.y = (float)atof(elements[1].c_str());
              //centre.y = centre.x = 0.0;
              dimensionsFound = true;
            }
          }
        }

        break;
      }
    }
  }
}