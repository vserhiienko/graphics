#include "pch.h"
#include "VESceneCamera.h"

namespace nyx
{
  static float clamp(float value, float minima, float maxima)
  {
    return std::min(std::max(value, minima), maxima);
  }
}

nv::vec3f nyx::SceneCustomCameraTraits::extractDirection()
{
  nv::vec3f direction;
  direction.x = viewInverseMatrix._31;
  direction.y = viewInverseMatrix._32;
  direction.z = viewInverseMatrix._33;
  return direction;
}

nv::vec3f nyx::SceneCustomCameraTraits::extractPosition()
{
  nv::vec3f position;
  position.x = viewInverseMatrix._41;
  position.y = viewInverseMatrix._42;
  position.z = viewInverseMatrix._43;
  return position;
}

void nyx::SceneCustomCameraTraits::updateViewInverse()
{
  viewInverseMatrix = nv::inverse(viewMatrix);
}

nyx::SceneCustomCameraTraits::SceneCustomCameraTraits()
{
  farPlane = 1000.0f;
  nearPlane = 0.001f;
  fieldOfView = _degs2radsf(45.0f);
}

void nyx::SceneCustomCameraTraits::setDrawingCentre(float x, float y)
{
  homeDrawingCentre.x = x;
  homeDrawingCentre.y = y;

  NVWindowsLog("home centre: %.3f, %.3f", homeDrawingCentre.x, homeDrawingCentre.y);

  onDimensionsChanged();
}

void nyx::SceneCustomCameraTraits::setDrawingDimensions(float width, float height)
{
  drawingDimensions.x = width;
  drawingDimensions.y = height;

  NVWindowsLog("drawing dimensions: %.3f, %.3f", drawingDimensions.x, drawingDimensions.y);

  onDimensionsChanged();
}

void nyx::SceneCustomCameraTraits::setScreenDimensions(float width, float height)
{
  screenDimensions.x = width;
  screenDimensions.y = height;

  NVWindowsLog("screen dimensions: %.3f, %.3f", screenDimensions.x, screenDimensions.y);

  onDimensionsChanged();
}

void nyx::SceneCustomCameraTraits::updateView()
{
  viewMatrix.make_identity();
  viewMatrix._41 = -currentDrawingCentre.x;
  viewMatrix._42 = -currentDrawingCentre.y;
  viewMatrix._43 = (nearPlane - farPlane) * 0.5f - nearPlane;
  updateViewInverse();
}

void nyx::SceneCustomCameraTraits::onDimensionsChanged()
{
  currentDrawingCentre = homeDrawingCentre;
  updateView();

  aspectRatio = screenDimensions.x / screenDimensions.y;

  float dimensionScale;
  nv::vec2f dimensionScales;
  dimensionScales.x = screenDimensions.x / drawingDimensions.x;
  dimensionScales.y = screenDimensions.y / drawingDimensions.y;
  dimensionScale = std::min(dimensionScales.x, dimensionScales.y);
  homeScreenScale = dimensionScale;

  // drawing should fit to screen
  drawingHomeDimensions = drawingDimensions * dimensionScale;
  // screen should fit to drawing
  homeDimensions = screenDimensions / dimensionScale;

  NVWindowsLog("home dimensions: %.3f, %.3f", homeDimensions.x, homeDimensions.y);
  NVWindowsLog("default scale: %.3f", dimensionScale);

  // set current
  currentDimensions = homeDimensions;
  // set current
  currentScreenScale = dimensionScale;
  // set current
  zoomFactor = 1.0f;

  updateProjection();
}

void nyx::SceneCustomCameraTraits::updateProjection()
{
  currentDimensions = screenDimensions / currentScreenScale;

  float
    x = currentDimensions.x * -0.5f, y = currentDimensions.y * -0.5f,
    w = currentDimensions.x, h = currentDimensions.y;

  NVWindowsLog("actual dimensions: %.3f, %.3f", currentDimensions.x, currentDimensions.y);

  nv::orthographic(projectionMatrix, x, x + w, y, y + h, nearPlane, farPlane);
}

void nyx::SceneCustomCameraTraits::resetView()
{
  setDrawingDimensions(
    drawingDimensions.x,
    drawingDimensions.y
    );
}

void nyx::SceneCustomRenderer::beginGesture(float x, float y)
{
}

nv::vec2f nyx::SceneCustomCameraTraits::castCurrentScreenToWorld(nv::vec2f viewTap)
{
  viewTap /= currentScreenScale;
  viewTap.y = currentDimensions.y - viewTap.y;
  nv::vec2f viewPivot = currentDrawingCentre - currentDimensions / 2.0f;
  nv::vec2f world = viewPivot + viewTap;
  return world;
}

nv::vec2f nyx::SceneCustomCameraTraits::castWorldToCurrentScreen(nv::vec2f world)
{
  nv::vec2f viewPivot = currentDrawingCentre - currentDimensions / 2.0f;
  nv::vec2f viewTap = world - viewPivot;
  viewTap.y = currentDimensions.y - viewTap.y;
  viewTap *= currentScreenScale;
  return viewTap;
}

void nyx::SceneCustomRenderer::tap(float x, float y)
{
  nv::vec2f viewTap(x, y);
  nv::vec2f world = castCurrentScreenToWorld(viewTap);

  NVWindowsLog("actual tap: %.3f, %.3f", x, y);
  NVWindowsLog("actual world: %.3f, %.3f", world.x, world.y);

  nv::vec2f viewTapTest = castWorldToCurrentScreen(world);
  NVWindowsLog("casted tap: %.3f, %.3f", viewTapTest.x, viewTapTest.y);

  if (coreRenderer)
  {
    nv::vec2f homeTap = homeDimensions / 2.0f + (world - homeDrawingCentre);
    homeTap.y = homeDimensions.y - homeTap.y;

    nv::vec2f screenTap = homeTap * homeScreenScale;

    NVWindowsLog("relative tap: %.3f, %.3f", screenTap.x, screenTap.y);
    coreRenderer->Tap(screenTap.x, screenTap.y, false);
  }

  NVWindowsLog("-----------------------------------");

}

void nyx::SceneCustomRenderer::pan(float x, float y)
{
  currentDrawingCentre.x -= x / currentScreenScale;
  currentDrawingCentre.y += y / currentScreenScale;
  viewMatrix._41 = -currentDrawingCentre.x;
  viewMatrix._42 = -currentDrawingCentre.y;
  NVWindowsLog("actual centre: %.3f, %.3f", -currentDrawingCentre.x, -currentDrawingCentre.y);

  cameraStateChanged = true;
}

void nyx::SceneCustomRenderer::zoom(float f)
{
  currentScreenScale += f * 0.01f * currentScreenScale;
  zoomFactor = currentScreenScale / homeScreenScale;
  updateProjection();

  NVWindowsLog("zoom: actual %.3f, relative %.3f", currentScreenScale, zoomFactor);
  cameraStateChanged = true;
}

void nyx::SceneCustomRenderer::evaluateDrawingParameters()
{
  coreRenderer->ZoomTo(DVLZOOMTO_ALL, 0, 0.0f);
  coreRenderer->RenderFrame();
  coreRenderer->RenderFrame();
#if 0

  sDVLMatrix view, proj;
  coreRenderer->GetCameraMatrices(view, proj);
  homeDrawingCentre.x = -view.m[3][0];
  homeDrawingCentre.y = -view.m[3][1];

  int
    x = 0,
    y = 0;
  float
    minX = 0,
    maxX = 0,
    minY = 0,
    maxY = 0;

  bool valueFound = false;

  nv::vec2i dims((int)screenDimensions.x, (int)screenDimensions.y);
  nv::vec2i halfDims((int)screenDimensions.x / 2, (int)screenDimensions.y / 2);

  // search for left

  DVLRESULT result;
  sDVLHitTest hitTest;
  nyx::initInfo(hitTest);

  const int precisenessX = 2; // lower is better
  const int precisenessY = 2; // lower is better

  valueFound = false;
  for (x = 0; x < halfDims.x; x += precisenessX)
  {
    for (y = 0; y < screenDimensions.y; y += precisenessY)
    {
      hitTest.m_fScreenCoordinateX = x;
      hitTest.m_fScreenCoordinateY = y;

      if ((result = coreRenderer->HitTest(&hitTest)) == DVLRESULT_OK)
      {
        minX = hitTest.m_fWorldCoordinateX;
        valueFound = true;
        break;
      }
    }

    if (valueFound)
      break;
  }

  valueFound = false;
  for (y = screenDimensions.y - 1; y >= halfDims.y; y -= precisenessY)
  {
    for (x = 0; x < screenDimensions.x; x += precisenessX)
    {
      hitTest.m_fScreenCoordinateX = x;
      hitTest.m_fScreenCoordinateY = y;

      if ((result = coreRenderer->HitTest(&hitTest)) == DVLRESULT_OK)
      {
        minY = hitTest.m_fWorldCoordinateY;
        valueFound = true;
        break;
      }
    }

    if (valueFound)
      break;
  }

  maxX = minX + (homeDrawingCentre.x - minX) * 2;
  maxY = minY + (homeDrawingCentre.y - minY) * 2;

  homeDimensions.x = maxX - minX;
  homeDimensions.y = maxY - minY;
#endif
}

void nyx::SceneCustomRenderer::endGesture()
{
}

nyx::SceneCustomRenderer::SceneCustomRenderer() : SceneCustomCameraTraits()
{
  coreRenderer = 0;
}
