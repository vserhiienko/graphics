#pragma once
#include "pch.h"

namespace nyx
{
  class SceneCustomCameraTraits
  {
  public:

    float zoomFactor;
    float homeScreenScale;
    float currentScreenScale;
    float nearPlane, farPlane;
    float fieldOfView, aspectRatio;

    nv::vec2f homeDimensions;
    nv::vec2f screenDimensions;
    nv::vec2f currentDimensions;
    nv::vec2f homeDrawingCentre;
    nv::vec2f currentDrawingCentre;
    nv::vec2f drawingDimensions;
    nv::vec2f drawingHomeDimensions;

    nv::vec4f offsetLimits;
    nv::matrix4f viewMatrix; // view matrix
    nv::matrix4f viewInverseMatrix; // view inverse matrix
    nv::matrix4f projectionMatrix; // view projection matrix

  public:
    SceneCustomCameraTraits();

    void setScreenDimensions(float width, float height);
    void setDrawingCentre(float x, float y);
    void setDrawingDimensions(float width, float height);
    void onDimensionsChanged();

    void updateProjection();
    void updateView();

    //nv::vec2f castWorldToReferenceScreen(nv::vec2f world);
    nv::vec2f castCurrentScreenToWorld(nv::vec2f screen);
    nv::vec2f castWorldToCurrentScreen(nv::vec2f world);
    //nv::vec2f castCurrentScreenToWorld(nv::vec2f screen);

    nv::vec3f extractDirection();
    nv::vec3f extractPosition();
    void updateViewInverse();

    virtual void resetView();
  };

  class SceneCustomRenderer
    : public SceneCustomCameraTraits
  {
  protected:
    bool cameraStateChanged;

  public:
    IDVLRenderer *coreRenderer;
    SceneCustomRenderer();

  public:
    virtual void beginGesture(float x, float y);
    virtual void tap(float x, float y);
    virtual void pan(float x, float y);
    virtual void zoom(float f);
    virtual void endGesture();

    void evaluateDrawingParameters();

  };

}