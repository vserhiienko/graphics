#pragma once
#include "pch.h"

#include "DVLInclude.h"

class VeClient : public IDVLClient
{
public:
  VeClient() {}

  void OnNodeSelectionChanged(
    IDVLScene *pScene, 
    size_t uNumberOfSelectedNodes,
    DVLID idFirstSelectedNode
    ) 
  {
    NVWindowsLog("node selection changed in scene 0x%x (%u)", pScene, uNumberOfSelectedNodes);
  }

  void OnStepEvent(
    DVLSTEPEVENT type,
    DVLID stepId
    ) 
  {
    NVWindowsLog("step event 0x%x with id 0x%x", type, stepId);
  }

  void LogMessage(
    DVLCLIENTLOGTYPE type, 
    const char *szSource, 
    const char *szText
    )
  {
    char buf[512] = {};
    _snprintf_s(buf, _countof(buf), "log: type=%d, source=%s, text=%s\n", type, szSource, szText);
    NVWindowsLog(buf);
  }

  bool NotifyFileLoadProgress(
    float fProgress
    )
  {
    char buf[128] = {};
    sprintf_s(buf, "loading: %f\n", fProgress);
    NVWindowsLog(buf);
    return true;
  }
};
