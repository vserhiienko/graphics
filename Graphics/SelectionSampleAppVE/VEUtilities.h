#pragma once
#include <pch.h>

namespace nyx
{
  template <typename _TyDvlDesc> 
  static void zeroMemory(_TyDvlDesc &desc)
  {
    memset(&desc, 0, sizeof(desc));
  }
  template <typename _TyDvlDesc>
  static size_t zeroMemorySz(_TyDvlDesc &desc)
  {
    zeroMemory(desc);
    return sizeof(desc);
  }

  template <typename _TyDvlInfo> 
  static void initInfo(_TyDvlInfo &desc);

  template <> static void initInfo<sDVLSceneInfo>(sDVLSceneInfo &desc)
  {
    desc.m_uSizeOfDVLSceneInfo = zeroMemorySz(desc);
  }
  template <> static void initInfo<sDVLMetadataInfo>(sDVLMetadataInfo &desc)
  {
    desc.m_uSizeOfDVLMetadataInfo = zeroMemorySz(desc);
  }
  template <> static void initInfo<sDVLNodeIDsArrayInfo>(sDVLNodeIDsArrayInfo &desc)
  {
    desc.m_uSizeOfDVLFindNodesList = zeroMemorySz(desc);
  }
  template <> static void initInfo<sDVLNodeInfo>(sDVLNodeInfo &desc)
  {
    desc.m_uSizeOfDVLNodeInfo = zeroMemorySz(desc);
  }
  template <> static void initInfo<sDVLPartsListInfo>(sDVLPartsListInfo &desc)
  {
    desc.m_uSizeOfDVLPartsListInfo = zeroMemorySz(desc);
  }
  template <> static void initInfo<sDVLProceduresInfo>(sDVLProceduresInfo &desc)
  {
    desc.m_uSizeOfDVLProceduresInfo = zeroMemorySz(desc);
  }

#if DVL_VERSION_MAJOR >= 4
  template <> static void initInfo<sDVLHitTest>(sDVLHitTest &desc)
  {
    desc.m_uSizeOfDVLHitTest = zeroMemorySz(desc);
  }
#endif

  static void print(char const *n, sDVLMatrix const &m)
  {
    NVWindowsLog(
      "%s:"
      "\n\t%2.2f %2.2f %2.2f %2.2f"
      "\n\t%2.2f %2.2f %2.2f %2.2f"
      "\n\t%2.2f %2.2f %2.2f %2.2f"
      "\n\t%2.2f %2.2f %2.2f %2.2f"
      , n
      , m.m[0][0], m.m[0][1], m.m[0][2], m.m[0][3]
      , m.m[1][0], m.m[1][1], m.m[1][2], m.m[1][3]
      , m.m[2][0], m.m[2][1], m.m[2][2], m.m[2][3]
      , m.m[3][0], m.m[3][1], m.m[3][2], m.m[3][3]
      );
  }

#if 0
  static void print(char const *n, sDVLHitTest const &test)
  {

  }
#endif

  static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
  {
    std::string item;
    std::stringstream ss(s);
    while (std::getline(ss, item, delim)) elems.push_back(item);
    return elems;
  }

  static std::vector<std::string> split(const std::string &s, char delim)
  {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
  }

  static nv::vec2f toVec2f(const std::string &s)
  {
    std::vector<std::string> elements;
    split(s, ';', elements);
    nv::vec2f vec; if (elements.size() == 2)
    {
      vec.x = (float)atof(elements[0].c_str());
      vec.y = (float)atof(elements[1].c_str());
    }

    return vec;
  }

  static nv::vec3f toVec3f(const std::string &s)
  {
    std::vector<std::string> elements;
    split(s, ';', elements);
    nv::vec3f vec; if (elements.size() == 3)
    {
      vec.x = (float)atof(elements[0].c_str());
      vec.y = (float)atof(elements[1].c_str());
      vec.z = (float)atof(elements[2].c_str());
    }
    return vec;
  }

}

