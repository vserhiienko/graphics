/**
	@file	DVLInclude.h

	This header includes all DVL headers.

	@note	You should include only this header into your project.
*/
#pragma once

#define DVL_LIBRARY //means that source is being compiled as part of DVL library

//Android platform
#ifdef ANDROID
	#define DVL_PLATFORM_ANDROID
#endif

//Windows platform
#ifdef _WIN32
	#define DVL_PLATFORM_WINDOWS
#endif

//iPhone/iPad/Mac platform
#ifdef __APPLE__
	//Note: TARGET_OS_IPHONE isn't defined when building a static library, see:
	//http://stackoverflow.com/questions/3742525/target-os-iphone-and-applicationtests
	#if TARGET_OS_IPHONE
		#define DVL_PLATFORM_IOS
	#else
		#define DVL_PLATFORM_OSX
		#ifdef __LP64__
			#define DVL_PLATFORM_64BIT
		#endif
	#endif
#endif

//Emscripten platform (yes, DVL can be converted into JavaScript, see http://emscripten.org)
#ifdef __EMSCRIPTEN__
	#define DVL_PLATFORM_EMSCRIPTEN
#endif

//Debug/Release
#ifdef NDEBUG
	#define DVL_PLATFORM_RELEASE
#else
	#define DVL_PLATFORM_DEBUG
#endif

//64 bit support
#ifdef _WIN64
	#define DVL_PLATFORM_64BIT
#endif



//
//standard includes
//
#include <stddef.h>
#include <stdint.h>



//
//DVL includes
//
#include "DVLVersion.h"
#include "DVLTypes.h"
#include "DVLScene.h"
#include "DVLClient.h"
#include "DVLRenderer.h"
#include "DVLLibrary.h"
#include "DVLCore.h"
