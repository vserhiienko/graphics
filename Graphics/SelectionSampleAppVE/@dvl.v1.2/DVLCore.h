/**
	@file	DVLCore.h

	This header defines the core interface of DVL.
*/
#pragma once
class IDVLRenderer;



/**
	This class defines the main interface for interaction with DVL.

	The typical usage scheme is:

	\code{.cpp}
		IDVLCore *pCore = DVLCreateCoreInstance();

		pCore->Init(NULL, DVL_VERSION_MAJOR, DVL_VERSION_MINOR);

		pCore->InitRender();
		IDVLRenderer *pRenderer = pCore->GetRendererPtr();

		IDVLScene *pScene = NULL;
		pCore->LoadScene("file://path/to/file.vds", &pScene);

		pRenderer->AttachScene(pScene);
		pScene->Release();

		// do something with the scene

		pCore->DoneRenderer();

		pCore->Release();
	\endcode
*/
class IDVLCore
{
protected:
	virtual ~IDVLCore() {}//use Release() instead

public:

	/**
	*	Performs all class initialization and verifies that library is compatible (by checking major/minor version)
	*
	*	Pass a pointer to the IDVLClient derived class to be able to get the notifications from the core. Pass \a NULL if you don't need this.
	*
	*	@retval	DVLRESULT_WRONGVERSION	If version of library is not compatible with requested uVersionMajor, uVersionMinor pair
	*	@retval	DVLRESULT_OUTOFMEMORY	When not enough RAM
	*
	*	@note	Must be called before any other methods of this class
	*/
	virtual DVLRESULT Init(IDVLClient *pDVLClient, uint32_t uVersionMajor, uint32_t uVersionMinor) = 0;

	/**
	*	Performs all renderer initialization
	*
	*	@retval	DVLRESULT_HARDWAREERROR If some problems with OpenGL
	*	@retval	DVLRESULT_OUTOFMEMORY	When not enough RAM
	*	@retval	DVLRESULT_MISSINGEXTENSION	If some OpenGL extensions are missing
	*	@retval	DVLRESULT_NOTINITIALIZED	When Init() was not performed before InitRenderer()
	*	@retval	DVLRESULT_ALREADYINITIALIZED	If renderer is already initialized
	*
	*	@note	Must be called after Init()
	*/
	virtual DVLRESULT InitRenderer() = 0;

	/**
	*	Deletes the current renderer and releases all allocated resources
	*
	*	@retval	DVLRESULT_HARDWAREERROR If some problems with OpenGL
	*	@retval	DVLRESULT_NOTINITIALIZED	When renderer is not initialized
	*	@retval	DVLRESULT_OK	If worked correctly
	*
	*/
	virtual DVLRESULT DoneRenderer() = 0;

	/**
	*	Deletes the core
	*
	*	Call this when you finish using the library. This releases all the internal structures allocated by the core.
	*/
	virtual void Release() = 0;

	/**
	*	Returns major version of DVL
	*
	*	@note	Different versions of DVL with same major version are backwards-compatible with each other (see GetMinorVersion()). For example you can use DVL v4.7 on code compiled with DVL v4.5
	*/
	virtual uint32_t GetMajorVersion() = 0;

	/**
	*	Returns minor version of DVL
	*
	*	@note	Minor version is increased every time some classes are extended, some new enums/defines added, new interfaces created, etc.
	*	@note	It is guaranteed that code compiled with version (MAJOR, MINOR) will be compatible with (MAJOR, MINOR+1), (MAJOR, MINOR+2), ... (MAJOR, MINOR+N).
	*	@note	It is guaranteed that none of existing interfaces will be truncated or removed in future versions of DVL with same Major Version.
	*/
	virtual uint32_t GetMinorVersion() = 0;

	/**
	*	Returns a build number of DVL
	*
	*	@note	Build number is automatically increased with each build of DVL.
	*/
	virtual uint32_t GetBuildNumber() = 0;

	/**
	*	Creates a new scene by loading it from the specified URL
	*
	*	@param	szURL	URL of the scene
	*	@param	ppScene	Pointer to the resulted scene
	*	@param	uBallastBufferSize	This buffer will be allocated prior to loading the scene and released once the scene has finished loading / failed to load. Useful for IOS where it can kill the app.
	*
	*	@note 	Only file URLs are currently supported (in the form of "file://path/to/file.vds")
	*	@note	Call IDVLScene::Release() once finished with it
	*
	*	@note 	If the \b uBallastBufferSize is non-zero, DVL will allocate that memory as a "ballast" to release it if the IDVLCore::OnLowMemory() method is called. This prevents the iOS
	*				applications to be closed if the really large scene is loaded. 1-2 Mb is a reasonably large size for the ballast buffer.
	*
	*	@retval	DVLRESULT_BADARG	If \b szURL or ppScene is \a NULL
	*	@retval	DVLRESULT_OUTOFMEMORY	If could not allocate enough RAM
	*	@retval	DVLRESULT_INTERRUPTED	If user has aborted the load process
	*	@retval	DVLRESULT_NOTFOUND	If supplied URL is incorrect
	*	@retval	DVLRESULT_ACCESSDENIED	If could not connect to remote server
	*	@retval	DVLRESULT_FILENOTFOUND	If can not open file
	*	@retval	DVLRESULT_WRONGVERSION	If file version is not supported
	*	@retval	DVLRESULT_BADFORMAT	If the file is invalid
	*	@retval	DVLRESULT_HARDWAREERROR	If some hardware error has happened
	*	@retval	DVLRESULT_FAIL	If some other error occurred
	*/
	virtual DVLRESULT LoadScene(const char *szURL, IDVLScene **ppScene, size_t uBallastBufferSize = 0) = 0;

	/**
	*	Returns IDVLRenderer interface
	*
	*	@note	It is guaranteed that GetRendererPtr() will return the same value always, so you can save this pointer and use it directly.
	*	@note	It is guaranteed that GetRendererPtr() will return a non-\a NULL pointer after each successfull InitRenderer().
	*/
	virtual IDVLRenderer *GetRendererPtr() = 0;

	/**
	*	Returns IDVLLibrary interface
	*
	*	@note	It is guaranteed that GetLibraryPtr() will return the same value always, so you can save this pointer and use it directly.
	*	@note	It is guaranteed that GetLibraryPtr() will return a non-\a NULL pointer after each successfull Init().
	*/
	virtual IDVLLibrary *GetLibraryPtr() = 0;

	/**
	*	Releases internal caches and any other information that can be recreated by DVL core when necessary
	*
	*	This is called by the hosting environment if there's not enough free memory for the application. The function is mostly for the iOS apps
	*	and must be called if the application receives the \e -didReceiveMemoryWarning event.
	*/
	virtual void OnLowMemory() = 0;
};



/**
*	This function creates a new instance of IDVLCore. Use the IDVLCore::Release() method to delete it when no longer used.
*/
IDVLCore *DVLCreateCoreInstance();



#ifdef DVL_PLATFORM_ANDROID
/**
*	This functions needs to be called in JNI_OnLoad() to initialize Java-related stuff inside DVL
*/
void DVL_JNI_OnLoad(void *pJavaVM);
#endif
