/**
	@file	DVLLibrary.h

	This header defines the Library interface which performs file listing and icon display
*/
#pragma once

#define DVLLIBRARYLISTING_FILENAMES							1
#define DVLLIBRARYLISTING_FILESIZES							2
#define DVLLIBRARYLISTING_FOLDERNAMES						4

/**
*	This structure is used to retrieve the file list from the library.
*
*	@note	You should not initialize any members but m_uSizeOfDVLLibraryFolderListing. RetrieveFolderListing() will initialize all of them (even those that you do not query on)
*	@note	You need to call ReleaseFolderListing() after each RetrieveFolderListing()
*/
#pragma pack(push, 1)
struct sDVLLibraryFolderListing
{
	///you must set it to sizeof(sDVLLibraryFolderListing) before calling RetrieveFolderListing()
	uint16_t m_uSizeOfDVLLibraryFolderListing;

	uint32_t m_uFilesCount;
	uint32_t m_uFoldersCount;

	char **m_pFileNames;//[m_uFilesCount], only when DVLLIBRARYLISTING_FILENAMES is set
	uint64_t *m_pFileSizes;//[m_uFilesCount], only when DVLLIBRARYLISTING_FILESIZES is set
	char **m_pFolderNames;//[m_uFoldersCount], only when DVLLIBRARYLISTING_FOLDERNAMES is set
};
#pragma pack(pop)



/**
*	This class defines the interface for interaction with file library.
*
*	@note The folder listing feature is designed to be used with non-standard file systems, like remote ones.
*			If all you need is to load files from the standard file system, there's no need to use the folder listing
*			functions of IDVLLibrary, use the standard file system API instead. However, RetrieveThumbnail() function
*			is still useful.
*/
class IDVLLibrary
{
protected:
	virtual ~IDVLLibrary() {}//can't be deleted using this interface

public:
	/**
	*	Retrieves folder listing information into the provided structure
	*
	*	@param	szURL	URL of the folder to query on. Set to NULL for local listing. Can be http://login:pwd@server:port/folder1/folder2/.
	*	@param	flags	Bitfield combination of one or more DVLLIBRARYLISTING_XXX defines
	*
	*	@note	Do *NOT* initialize the "pListing" structure as RetrieveFolderListing() fully initializes it (even the fields that are not requested)
	*	@note	Only set m_uSizeOfDVLLibraryFolderListing to sizeof(sDVLLibraryFolderListing)
	*	@note	Retrieve all info that you need in a single call with multiple flags set (it's faster than issuing multiple calls with less flags)
	*
	*	@retval	DVLRESULT_BADFORMAT	If m_uSizeOfDVLLibraryFolderListing is wrong or URL format incorrect
	*	@retval	DVLRESULT_BADARG	If pListing == NULL
	*/
	virtual DVLRESULT RetrieveFolderListing(const char *szURL, uint32_t flags, sDVLLibraryFolderListing *pListing) = 0;

	/**
	*	Releases all memory that was initialized in a previous call to RetrieveFolderListing()
	*
	*	@note	If "pListing" is NULL or m_uSizeOfDVLLibraryFolderListing is wrong, "pListing" will not be deleted (and you'll get a memory leak)
	*/
	virtual void ReleaseFolderListing(sDVLLibraryFolderListing *pListing) = 0;

	/**
	*	Retrieves file thumbnail into the provided buffer
	*
	*	@param	szURL	URL of a file to retrieve thumbnail for. The format is "file://path/to/file.vds", the encoding is UTF-8
	*	@param	pThumbnail	Thumbnail will be retrieved into this data structure.
	*
	*	@retval	DVLRESULT_BADARG	If either \b szURL or \b pThumbnail is \a NULL
	*	@retval	DVLRESULT_BADARG	If \b m_uSizeOfDVLImage is wrong or \b szURL format incorrect
	*	@retval	DVLRESULT_FILENOTFOUND	Such file/folder does not exist
	*	@retval	DVLRESULT_NOTFOUND	File does not have a thumbnail (in this case you need to show some default thumbnail)
	*	@retval	DVLRESULT_OUTOFMEMORY	If could not allocate thumbnail buffer
	*
	*	\b Notes
	*	\li You need to initialize the \b m_uSizeOfDVLImage member of the sDVLImage structure with its size.
	*
	*	\b Example
	*
	*	\code{.cpp}
	*		sDVLImage img = {};
	*		img.m_uSizeOfDVLImage = sizeof(img);
	*		DVLRESULT res = pLibrary->RetrieveThumbnail("file://path/to/file.vds", &img);
	*		if (DVLSUCCEEDED(res))
	*		{
	*			// use the thumbnail
	*			pLibrary->ReleaseThumbnail(&img);
	*		}
	*	\endcode
	*/
	virtual DVLRESULT RetrieveThumbnail(const char *szURL, sDVLImage *pThumbnail) = 0;

	/**
	*	Releases all memory that was initialized in a previous call to RetrieveThumbnail()
	*
	*	@note	If \b m_uSizeOfDVLImage has invalid value the memory will not be released.
	*/
	virtual void ReleaseThumbnail(sDVLImage *pThumbnail) = 0;
};
