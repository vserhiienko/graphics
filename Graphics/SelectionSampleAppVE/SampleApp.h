#pragma once

#include "pch.h"
#include "VEScene.h"
#include "VESceneCamera.h"
#include "SelectionAssist.h"

namespace nyx
{
  class SampleApp
    : public NvSampleApp
    , public IDVLClient
  {
  public: // NvSampleApp, NvAppBase 

    SampleApp(NvPlatformContext* platform);
    ~SampleApp(void);

    virtual void initUI(void) override;
    virtual void initRendering(void) override;
    virtual void draw(void) override;

    virtual void configurationCallback(
      NvEGLConfiguration &config
      ) override;

    virtual void reshape(
      int32_t width,
      int32_t height
      ) override;
    virtual bool handlePointerInput(
      NvInputDeviceType::Enum device,
      NvPointerActionType::Enum action,
      uint32_t modifiers, int32_t count,
      NvPointerEvent *points
      ) override;
    virtual bool handleKeyInput(
      uint32_t code,
      NvKeyActionType::Enum action
      ) override;

  public: // IDVLClient

    void OnNodeSelectionChanged(
      IDVLScene *pScene,
      size_t uNumberOfSelectedNodes,
      DVLID idFirstSelectedNode
      );
    void OnStepEvent(
      DVLSTEPEVENT type,
      DVLID stepId
      );
    void LogMessage(
      DVLCLIENTLOGTYPE type,
      const char *szSource,
      const char *szText
      );
    bool NotifyFileLoadProgress(
      float fProgress
      );

  public:

    bool loadScene();
    void parseScene();

    void beginGesture(float x, float y);
    void tap(float x, float y);
    void pan(float x, float y);
    void zoom(float f);
    void endGesture();

  private:
    void resetSceneTransform();
    void savePointerPosition(NvPointerEvent *e);

  private:

    IDVLCore                               *m_dvlCore;
    IDVLRenderer                           *m_dvlRenderer;

    bool                                    m_projectItems;
    bool                                    m_selectionMode;
    bool                                    m_resetButtonVar;
    bool                                    m_enableMultipleSelections;
    bool                                    m_leftMoved;
    bool                                    m_leftPressed;
    bool                                    m_rightPressed;
    bool                                    m_redmarkingMode;
    bool                                    m_renderScene;
    bool                                    m_useCustomCamera;
    nv::vec2f                               m_lastPosition;

    nyx::SelectionAssits                    m_selectionAssist;
    nv::vec2f                               m_screemDims;
    nyx::Scene                              m_scene;
    nyx::SceneCustomRenderer                m_sceneCamera;
    nyx::planef                             m_groundPlane;
    nv::matrix4f                            m_viewMatrix;
    nv::matrix4f                            m_projectionMatrix;
    nv::matrix4f                            m_orthoProjectionMatrix;
    nv::matrix4f                            m_dvlViewMatrix;
    nv::matrix4f                            m_dvlProjectionMatrix;

    static bool const                       s_useInputTransformer = false;

  };
}