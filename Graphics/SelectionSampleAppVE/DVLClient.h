/**
	@file	DVLClient.h

	This header defines the Client interface which abstracts data interaction between DVL library and the client
*/
#pragma once


//
//DVLCLIENTLOGTYPE
//
/// Defines the type of the logged information
enum DVLCLIENTLOGTYPE
{
	/// Debugging information, usually can be ignored
	DVLLOGTYPE_DEBUG = 0,

	/// Information message, like name of the loading file or ID of the activated step
	DVLLOGTYPE_INFO,

	/// Warning message, usually means that something went wrong but you can proceed
	DVLLOGTYPE_WARNING,

	/// Error message, means that something has failed
	DVLLOGTYPE_ERROR,
};



//
//DVLSTEPEVENT
//
/// Defines the type of a step event
enum DVLSTEPEVENT
{
	/// step has started (stepId is a new step)
	DVLSTEPEVENT_STARTED,

	/// the previous step has finished and the new one is started (stepId is a new step)
	DVLSTEPEVENT_SWITCHED,

	/// the step has finished playing and no more steps are to be played (stepId is the old step)
	DVLSTEPEVENT_FINISHED
};



/// Logging source: OpenGL rendering layer
#define DVLLOGSOURCE_OPENGL										"OpenGL"

/// Logging source: Memory management routines
#define DVLLOGSOURCE_MEMORY										"Memory"

/// Logging source: Local file system manager
#define DVLLOGSOURCE_LOCALFILESYSTEM							"LocalFileSystem"

/// Logging source: HTTP file system manager
#define DVLLOGSOURCE_HTTPFILESYSTEM								"HTTPFileSystem"

/// Logging source: Remote file system manager
#define DVLLOGSOURCE_REMOTEFILESYSTEM							"RemoteFileSystem"

/// Logging source: VDS file reading routines
#define DVLLOGSOURCE_VDS										"VDS"

/// Logging source: Scene manipulation routines
#define DVLLOGSOURCE_SCENE										"Scene"



/**
*	This class defines the main interface for interaction between DVL and the client
*
*	Default implementations of all methods do nothing. You may override them if you'd like to get some information
*	from the core, like logging messages or selection events.
*/
class IDVLClient
{
public:
	virtual ~IDVLClient() {}

	/**
	*	Called when selection list changes. There are three possible scenarios:
	*	\li Selection list is empty, in such case \b uNumberOfSelectedNodes == 0, \b idFirstSelectedNode == ::DVLID_INVALID
	*	\li Selection list has exactly 1 node, in such case \b uNumberOfSelectedNodes == 1, \b idFirstSelectedNode equals to ::DVLID of selected node
	*	\li Selection list has more than 1 nodes, in such case \b uNumberOfSelectedNodes gives total count and \b idFirstSelectedNode == ::DVLID_INVALID.
	*		You need to call IDVLScene::RetrieveSceneInfo() with ::DVLSCENEINFO_SELECTED flag to get a full list.
	*
	*	@note	The default implementation does nothing
	*/
	virtual void OnNodeSelectionChanged(IDVLScene *pScene, size_t uNumberOfSelectedNodes, DVLID idFirstSelectedNode) {}

	/**
	*	Called when library wants to report a warning, error, etc
	*
	*	@param	type	Type of the message, see ::DVLCLIENTLOGTYPE
	*	@param	szSource	Text representation of the message source, see DVLLOGSOURCE_XXX defines. Can be \a NULL.
	*	@param	szText	The text message in UTF-8. Can be \a NULL.
	*
	*	@note	The default implementation does nothing
	*/
	virtual void LogMessage(DVLCLIENTLOGTYPE type, const char *szSource, const char *szText) {}

	/**
	*	Called to notify about the updated step status
	*	
	*	@param	type	The type of the event that happened to the step
	*	@param	stepId	The identifier of the step. See the ::DVLSTEPEVENT enum comments for details
	*
	*	@note	The default implementation does nothing
	*/
	virtual void OnStepEvent(DVLSTEPEVENT type, DVLID stepId) {}

	/**
	*	Called to notify about file loading progress (which may be quite time consuming) and to check if user wants to abort file loading
	*	
	*	@param	fProgress	A value from 0.0f to 1.0f indicating the progress
	*
	*	@note	The method will be called several times with \b fProgress == 0.0f while the hierarchy is loading
	*
	*	@retval	true	OK to proceed with file loading
	*	@retval	false	If the loading needs to be canceled (for example, user abort in UI)
	*/
	virtual bool NotifyFileLoadProgress(float fProgress) {return true;}

	/**
	*	Called to notify when frame rendering has started
	*
	*	@note	The default implementation does nothing
	*/
	virtual void NotifyFrameStarted() {}

	/**
	*	Called to notify when frame rendering has finished
	*
	*	@note	The default implementation does nothing
	*/
	virtual void NotifyFrameFinished() {}

	/**
	*	If DVLRENDEROPTION_SHOW_DEBUG_INFO is ON, this method will be used to display custom information in the top of rendered image together with debug info from the renderer.
	*
	*	@retval	NULL if you don't need to display any custom text
	*	@retval	ANSI string with custom text if you need to display something. Note: you are responsible to allocating and releasing the string buffer.
	*/
	virtual const char *GetDebugInfoString() {return NULL;}
};
