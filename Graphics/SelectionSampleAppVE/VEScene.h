#pragma once

#include "pch.h"
#include "DVLInclude.h"
#include "VESceneNode.h"

namespace nyx
{
  class Scene
  {
  public:
    typedef IDVLScene CoreObject;

  public:
    static const DVLNODEINFO nodeFlags = (DVLNODEINFO)
      ( DVLNODEINFO_HIGHLIGHT_COLOR
      | DVLNODEINFO_CHILDREN
      | DVLNODEINFO_UNIQUEID
      | DVLNODEINFO_ASSETID
      | DVLNODEINFO_PARENTS
      | DVLNODEINFO_NAME );

    CoreObject *coreObj;
    SceneNode *modelNode;
    SceneNode::Collection nodes;

    bool centreFound;
    bool dimensionsFound;
    nv::vec2f centre;
    nv::vec2f dimensions;
    std::string modelNodeName;

  public:
    Scene();
    void initialize(CoreObject *);
    bool getWorldMatrix(SceneNode::NodeId const &, nv::matrix4f &);
    static bool getWorldMatrix(CoreObject *, SceneNode::NodeId const &, nv::matrix4f &);

  public:
    //operator CoreObject *() { return m_coreObj; }

  private:
    void initialize(SceneNode::NodeId const &);
    void initializeModelNode();

  };
}