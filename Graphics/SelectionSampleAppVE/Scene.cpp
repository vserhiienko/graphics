#include <pch.h>
#include <Scene.h>
#include <NoekkAnim.h>

namespace ve
{
  namespace soko
  {
    namespace impl
    {
      class Scene;
      class SceneNode;
      class Renderer;
      class RendererCamera;
      class Manager;
    }
  }
}

class ve::soko::impl::SceneNode : public virtual ISceneNode
{
public:

  DVLID id;
  Scene *host;
  bool zoomable;
  std::string name;
  std::string keelId;
  nv::vec2f centre;
  nv::vec2f dimensions;

public:

  SceneNode(Scene *scene)
    : host(scene)
  {
    id = 0;
    name = "";
    keelId = "";
    centre.x = 0;
    centre.y = 0;
    dimensions.x = 0;
    dimensions.y = 0;
    zoomable = false;
  }

  virtual ~SceneNode(void) { }

  virtual IScene *getScene(void) override;
  virtual bool canZoomTo(void) const override { return zoomable; }
  virtual DVLID SceneNode::getId(void) const override { return id; }
  virtual std::string const &getName(void) const override { return name; }
  virtual std::string const &getKeelId(void) const override { return keelId; }
  virtual nv::vec2f const &getCentre(void) const override { return centre; }
  virtual nv::vec2f const &getSize(void) const const override{ return dimensions; }

  bool find(sDVLMetadata &meta, std::string const &ns, std::string const &field, std::string &value);
  bool findField(sDVLMetadata *meta, std::string const &field, std::string &value);
};

class ve::soko::impl::Scene
  : public virtual IScene
  , public virtual SceneNode
{
public:
  typedef std::vector<SceneNode*> NodeVector;
  typedef std::map<DVLID, SceneNode*> NodeIdMap;
  typedef std::map<std::string, SceneNode*> NodeStringMap;

  static const DVLNODEINFO nodeFlags = (DVLNODEINFO)
    (DVLNODEINFO_HIGHLIGHT_COLOR
    | DVLNODEINFO_CHILDREN
    | DVLNODEINFO_UNIQUEID
    | DVLNODEINFO_ASSETID
    | DVLNODEINFO_PARENTS
    | DVLNODEINFO_NAME);

public:
  DVLRESULT dvlResult;
  IDVLScene *dvlScene;

public:
  NodeVector nodes;
  NodeIdMap nodesById;
  NodeStringMap nodesByKeelId;
  SceneNode *rootNode;
  SceneNode *modelNode;

public:
  Scene()
    : SceneNode(0)
    , rootNode(0)
    , modelNode(0)
  {
  }

  virtual ~Scene(void)
  {

  }

  void readNodes();
  void readNodes(DVLID const &nodeId);
  bool reload(std::string url);
  bool verify(void)
  {
    return getRoot() && getRoot()->canZoomTo();
  }

public:

  virtual ISceneNode const *getRoot(void) const
  {
    if (modelNode != 0) return modelNode; else return rootNode;
  }

  virtual ISceneNode const *lookupNodeById(DVLID id) const override
  {
    if (nodesById.find(id) == nodesById.end()) return 0;
    else return (ISceneNode const*)nodesById.at(id);
  }

  virtual ISceneNode const *lookupNodeByKeelId(std::string keelId) const override
  {
    if (nodesByKeelId.find(keelId) == nodesByKeelId.end()) return 0;
    else return (ISceneNode const*)(nodesByKeelId.at(keelId));
  }

};

class ve::soko::impl::RendererCamera : public ISceneCamera
{
public:
  class IDependent
  {
  public:
    typedef std::vector<IDependent*> Vector;
    typedef IDependent::Vector::iterator It;
  public:
    virtual void onScreenSizeChanged(nv::vec2f const&) { }
  };

public:
  float zoomFactor;
  float homeScreenScale;
  float currentScreenScale;
  float nearPlane, farPlane;
  float fieldOfView, aspectRatio;
  bool cameraStateChanged;

  bool zoomingInProgress;
  float targetScale;
  nv::vec2f targetCentre;
  noekk::anim::QuadraticInOutEasingFunction scaleEasing;
  noekk::anim::CubicInOutEasingFunction centreXEasing;
  noekk::anim::CubicInOutEasingFunction centreYEasing;

  nv::vec2f homeDimensions;
  nv::vec2f screenDimensions;
  nv::vec2f currentDimensions;
  nv::vec2f homeDrawingCentre;
  nv::vec2f currentDrawingCentre;
  nv::vec2f drawingDimensions;
  nv::vec2f drawingHomeDimensions;

  nv::vec4f offsetLimits;
  nv::matrix4f viewMatrix; // view matrix
  nv::matrix4f viewInverseMatrix; // view inverse matrix
  nv::matrix4f projectionMatrix; // view projection matrix

  IDependent::Vector listeners;

  Renderer *renderer;

public:

  RendererCamera()
  {
    const float totalEasing = 1.f;

    farPlane = 1000.0f;
    nearPlane = 0.001f;
    fieldOfView = _degs2radsf(45.0f);
    scaleEasing.reset();
    centreXEasing.reset();
    centreYEasing.reset();
    scaleEasing.params.total = totalEasing;
    centreXEasing.params.total = totalEasing;
    centreYEasing.params.total = totalEasing;
    zoomingInProgress = false;
  }

  virtual ~RendererCamera() { }
  virtual void beginGesture(float x, float y) override { }
  virtual void endGesture(void) override { }

  virtual nv::vec2f tap(float x, float y) override;

  virtual void pan(float x, float y) override
  {
    if (zoomingInProgress) return;

    currentDrawingCentre.x -= x / currentScreenScale;
    currentDrawingCentre.y += y / currentScreenScale;
    viewMatrix._41 = -currentDrawingCentre.x;
    viewMatrix._42 = -currentDrawingCentre.y;
    NVWindowsLog("actual centre: %.3f, %.3f", -currentDrawingCentre.x, -currentDrawingCentre.y);

    cameraStateChanged = true;
  }

  virtual void zoom(float f) override
  {
    if (zoomingInProgress) return;

    currentScreenScale += f * 0.01f * currentScreenScale;
    zoomFactor = currentScreenScale / homeScreenScale;
    updateProjection();

    NVWindowsLog("zoom: actual %.3f, relative %.3f", currentScreenScale, zoomFactor);
    cameraStateChanged = true;
  }

public:
  virtual nv::matrix4f const &getView(void)
  {
    return viewMatrix;
  }
  virtual nv::matrix4f const &getProjection(void)
  {
    return projectionMatrix;
  }
  virtual nv::vec2f castCurrentScreenToWorld(nv::vec2f viewTap) override
  {
    viewTap /= currentScreenScale;
    viewTap.y = currentDimensions.y - viewTap.y;
    nv::vec2f viewPivot = currentDrawingCentre - currentDimensions / 2.0f;
    nv::vec2f world = viewPivot + viewTap;
    return world;
  }

  virtual nv::vec2f castWorldToCurrentScreen(nv::vec2f world) override
  {
    nv::vec2f viewPivot = currentDrawingCentre - currentDimensions / 2.0f;
    nv::vec2f viewTap = world - viewPivot;
    viewTap.y = currentDimensions.y - viewTap.y;
    viewTap *= currentScreenScale;
    return viewTap;
  }

  float calculateScreenScale(nv::vec2f const &targetSize)
  {
    float dimensionScale;
    nv::vec2f dimensionScales;
    dimensionScales.x = screenDimensions.x / targetSize.x;
    dimensionScales.y = screenDimensions.y / targetSize.y;
    dimensionScale = std::min(dimensionScales.x, dimensionScales.y);
    return dimensionScale;
  }

  void setCurrentDrawingCentre(nv::vec2f const &desiredCentre)
  {
    currentDrawingCentre = desiredCentre;
    viewMatrix._41 = -currentDrawingCentre.x;
    viewMatrix._42 = -currentDrawingCentre.y;
  }

  float calculateScalingDistanceForNode(nv::vec2f const &nodeSize)
  {
    return normalizeScreenScale(calculateScreenScale(nodeSize)) - currentScreenScale;
  }

  nv::vec2f calculateCentreDistancesForNode(nv::vec2f const &nodeCentre)
  {
    return nodeCentre - currentDrawingCentre;
  }

  float normalizeScreenScale(float screenScale)
  {
    float currentZoomFactor = screenScale / homeScreenScale;
    screenScale /= sqrtf(currentZoomFactor);
    return screenScale;
  }

  void normalizeCurrentScreenScale(void)
  {
    zoomFactor = currentScreenScale / homeScreenScale;
    currentScreenScale /= sqrtf(zoomFactor);
    zoomFactor = currentScreenScale / homeScreenScale;
    updateProjection();
  }

  void setCurrentScreenScale(float screenScale)
  {
    currentScreenScale = screenScale;
    zoomFactor = currentScreenScale / homeScreenScale;
    updateProjection();
  }

  void retargetCamera(void)
  {
    currentDrawingCentre = homeDrawingCentre;
    updateView();

    aspectRatio = screenDimensions.x / screenDimensions.y;

    float dimensionScale;
    dimensionScale = homeScreenScale = calculateScreenScale(drawingDimensions);

    // drawing should fit to screen
    drawingHomeDimensions = drawingDimensions * dimensionScale;
    // screen should fit to drawing
    homeDimensions = screenDimensions / dimensionScale;

    NVWindowsLog("home dimensions: %.3f, %.3f", homeDimensions.x, homeDimensions.y);
    NVWindowsLog("default scale: %.3f", dimensionScale);

    // set current
    currentDimensions = homeDimensions;
    // set current
    currentScreenScale = dimensionScale;
    // set current
    zoomFactor = 1.0f;

    updateProjection();

  }

  virtual void setScreenSize(nv::vec2f screenSize);

  virtual bool zoomTo(ISceneNode const &node) override
  {
    if (node.canZoomTo())
    {
      NVWindowsLog("zooming to node %s", node.getKeelId().c_str());

#if 0
      /// this is the code for instance 'zoom-to'

      currentDrawingCentre = node.getCentre(); // <- set current drawing centre
      viewMatrix._41 = -currentDrawingCentre.x;
      viewMatrix._42 = -currentDrawingCentre.y;
      currentScreenScale = calculateScreenScale(node.getSize()); // + normalize current scale
      zoomFactor = currentScreenScale / homeScreenScale;
      currentScreenScale /= sqrtf(zoomFactor);
      zoomFactor = currentScreenScale / homeScreenScale;
      updateProjection();

#else 
      /// this is the code for eased 'zoom-to'

      scaleEasing.reset();
      centreXEasing.reset();
      centreYEasing.reset();

      scaleEasing.params.offset = currentScreenScale;
      centreXEasing.params.offset = currentDrawingCentre.x;
      centreYEasing.params.offset = currentDrawingCentre.y;

      nv::vec2f centreDistances = calculateCentreDistancesForNode(node.getCentre());
      scaleEasing.params.distance = calculateScalingDistanceForNode(node.getSize());
      centreXEasing.params.distance = centreDistances.x;
      centreYEasing.params.distance = centreDistances.y;

      zoomingInProgress = true;
#endif

      return true;
    }
    else
    {
      NVWindowsLog("cannot zoom to node %s", node.getKeelId().c_str());
      return false;
    }
  }

  bool initialZoomToAll(ISceneNode const &node)
  {
    if (node.canZoomTo())
    {
      nv::vec2f cr = node.getCentre();
      nv::vec2f sz = node.getSize();

      homeDrawingCentre.x = cr.x;
      homeDrawingCentre.y = cr.y;
      drawingDimensions.x = sz.x;
      drawingDimensions.y = sz.y;

      retargetCamera();
      normalizeCurrentScreenScale();
      return true;
    }

    return false;
  }

  virtual nv::vec3f extractDirection() override
  {
    nv::vec3f direction;
    direction.x = viewInverseMatrix._31;
    direction.y = viewInverseMatrix._32;
    direction.z = viewInverseMatrix._33;
    return direction;
  }

  virtual nv::vec3f extractPosition() override
  {
    nv::vec3f position;
    position.x = viewInverseMatrix._41;
    position.y = viewInverseMatrix._42;
    position.z = viewInverseMatrix._43;
    return position;
  }

  void updateViewInverse()
  {
    viewInverseMatrix = nv::inverse(viewMatrix);
  }

  virtual void updateProjection() override
  {
    currentDimensions = screenDimensions / currentScreenScale;
    float
      x = currentDimensions.x * -0.5f, y = currentDimensions.y * -0.5f,
      w = currentDimensions.x, h = currentDimensions.y;
    NVWindowsLog("actual dimensions: %.3f, %.3f", currentDimensions.x, currentDimensions.y);
    nv::orthographic(projectionMatrix, x, x + w, y, y + h, nearPlane, farPlane);
  }

  virtual void updateView() override
  {
    viewMatrix.make_identity();
    viewMatrix._41 = -currentDrawingCentre.x;
    viewMatrix._42 = -currentDrawingCentre.y;
    viewMatrix._43 = (nearPlane - farPlane) * 0.5f - nearPlane;
    updateViewInverse();
  }

public:
  virtual void onFrameMove(float elapsed) override
  {
    if (zoomingInProgress)
    {
      int saturatedEasings = 0;
      bool updateCentre = false;

      if (!scaleEasing.saturated)
      {
        scaleEasing += elapsed;
        currentScreenScale = scaleEasing.onFrame();
        setCurrentScreenScale(currentScreenScale);
      }
      else
      {
        saturatedEasings++;
      }

      if (!centreXEasing.saturated)
      {
        centreXEasing += elapsed;
        currentDrawingCentre.x = centreXEasing.onFrame();
        updateCentre = true;
      }
      else
      {
        saturatedEasings++;
      }

      if (!centreYEasing.saturated)
      {
        centreYEasing += elapsed;
        currentDrawingCentre.y = centreYEasing.onFrame();
        updateCentre = true;
      }
      else
      {
        saturatedEasings++;
      }

      if (updateCentre) setCurrentDrawingCentre(currentDrawingCentre);
      if (saturatedEasings == 3) zoomingInProgress = false;
    }
  }

};

class ve::soko::impl::Renderer
  : public ISceneRenderer
  , public RendererCamera::IDependent
{
public:
  DVLRESULT dvlResult;
  IDVLRenderer *dvlRenderer;
  Scene *attachedScene;

public:
  Renderer(void) : dvlRenderer(0), attachedScene(0) { }
  virtual ~Renderer(void) { }

  bool initialize(void);
  virtual void render(ISceneCamera*, float) override;
  virtual void onScreenSizeChanged(nv::vec2f const &size)
  {
    if (dvlRenderer)
    {
      dvlResult = dvlRenderer->SetDimensions((uint32_t)size.x, (uint32_t)size.y);
      dvlResult = dvlRenderer->ZoomTo(DVLZOOMTO_ALL, 0, 0);
    }
  }

};

class ve::soko::impl::Manager : public IDVLClient
{
public:
  Scene scene;
  Renderer renderer;
  RendererCamera camera;

  IDVLCore *dvlCore;
  DVLRESULT dvlResult;

public:
  Manager()
  {
    dvlCore = 0;
    dvlResult = DVLRESULT_FALSE;
  }

  bool initialize(void);
  void destroy(void);

public:
  static ve::soko::impl::Manager &getInstance(void)
  {
    static ve::soko::impl::Manager manager;
    return manager;
  }

public:
  virtual void OnNodeSelectionChanged(IDVLScene *, size_t nodeCount, DVLID nodeId)
  {
    ISceneNode const *node = scene.lookupNodeById(nodeId);
    if (node) camera.zoomTo((*node));
  }

  virtual void LogMessage(DVLCLIENTLOGTYPE type, const char *szSource, const char *szText) { }
  virtual void OnStepEvent(DVLSTEPEVENT type, DVLID stepId) { }
  virtual void NotifyFrameStarted() { }
  virtual void NotifyFrameFinished() { }
  virtual bool NotifyFileLoadProgress(float fProgress) { return true; }
  virtual const char *GetDebugInfoString() { return NULL; }
};

ve::soko::IScene *ve::soko::impl::SceneNode::getScene(void)
{
  return host;
}

ve::soko::IScene *ve::soko::SceneManager::getScene(void)
{
  return &ve::soko::impl::Manager::getInstance().scene;
}

ve::soko::ISceneCamera *ve::soko::SceneManager::getCamera(void)
{
  return &ve::soko::impl::Manager::getInstance().camera;
}

ve::soko::ISceneRenderer *ve::soko::SceneManager::getRenderer(void)
{
  return &ve::soko::impl::Manager::getInstance().renderer;
}

bool ve::soko::SceneManager::initialize(void)
{
  bool managerInitialized = impl::Manager::getInstance().initialize();
  if (managerInitialized)
  {
    impl::Manager::getInstance().camera.listeners.push_back(
      &impl::Manager::getInstance().renderer
      );

  }
  return managerInitialized;
}

bool ve::soko::SceneManager::loadScene(std::string sceneUrl)
{
  bool sceneLoaded = impl::Manager::getInstance().scene.reload(sceneUrl);
  return sceneLoaded;
}

void ve::soko::SceneManager::destroy(void)
{
  impl::Manager::getInstance().destroy();
}

bool ve::soko::impl::Renderer::initialize(void)
{
  dvlResult = Manager::getInstance().dvlCore->InitRenderer();
  if (DVLFAILED(dvlResult)) return false;

  dvlRenderer = Manager::getInstance().dvlCore->GetRendererPtr();
  dvlRenderer->SetBackgroundColor(0.9f, 0.9f, 0.9f, 0.6f, 0.6f, 0.6f);
  dvlResult = dvlRenderer->SetOption(DVLRENDEROPTION_AMBIENT_OCCLUSION, false);
  dvlResult = dvlRenderer->SetOption(DVLRENDEROPTION_SHOW_DEBUG_INFO, false);
  dvlResult = dvlRenderer->SetOption(DVLRENDEROPTION_SHOW_SHADOW, false);

  return true;
}

void ve::soko::impl::Renderer::render(ISceneCamera *camera, float elapsed)
{
  if (dvlRenderer)
  {
    // non-verified scene wont be attached to this renderer
    if (attachedScene /*&& attachedScene->verify()*/ && camera)
    {
      camera->onFrameMove(elapsed);
      dvlResult = dvlRenderer->RenderFrameEx(
        (sDVLMatrix const&)camera->getView(),
        (sDVLMatrix const&)camera->getProjection()
        );
    }
    else
    {
      dvlResult = dvlRenderer->RenderFrame();
    }
  }
  else
  {
    glClearColor(1.0f, 0.0f, 0.0f, 1.0f); // read
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  }
}

bool ve::soko::impl::Manager::initialize(void)
{
  if (!(dvlCore = DVLCreateCoreInstance())) return false;
  if (DVLFAILED(dvlResult = dvlCore->Init(this, DVL_VERSION_MAJOR, DVL_VERSION_MINOR))) return false;
  if (!renderer.initialize()) return false;
  camera.renderer = &renderer;
  return true;
}

void ve::soko::impl::Manager::destroy(void)
{
}

bool ve::soko::impl::SceneNode::find(sDVLMetadata &meta, std::string const &ns, std::string const &field, std::string &value)
{
  for (uint32_t nsIndex = 0; nsIndex < meta.m_uNamespacesCount; nsIndex++)
  {
    if (ns == meta.m_pNamespaces[nsIndex].m_szName)
    {
      return findField(meta.m_pNamespaces[nsIndex].m_pMetadata, field, value);
    }
    else
    {
      if (find(*meta.m_pNamespaces[nsIndex].m_pMetadata, ns, field, value))
        return true;
    }
  }

  return false;
}

bool ve::soko::impl::SceneNode::findField(sDVLMetadata *meta, std::string const &field, std::string &value)
{
  for (uint32_t nvpIndex = 0; nvpIndex < meta->m_uNameValuePairsCount; nvpIndex++)
  {
    if (field == meta->m_pNameValuePairs[nvpIndex].m_szName)
    {
      value = meta->m_pNameValuePairs[nvpIndex].m_szValue;
      return true;
    }
  }

  return false;
}

void ve::soko::impl::Scene::readNodes()
{
  if (dvlScene == 0) return;

  sDVLSceneInfo sceneInfo;
  nyx::initInfo(sceneInfo);

  dvlResult = dvlScene->RetrieveSceneInfo(DVLSCENEINFO_CHILDREN, &sceneInfo);
  if (SUCCEEDED(dvlResult))
  {
    for (uint32_t nodeIndex = 0; nodeIndex < sceneInfo.m_uChildNodesCount; nodeIndex++)
      readNodes(sceneInfo.m_pChildNodes[nodeIndex]);
    dvlScene->ReleaseSceneInfo(&sceneInfo);
  }
  else
  {
    NVWindowsLog("error: failed to retrieve scene info");
  }
}

void ve::soko::impl::Scene::readNodes(DVLID const &nodeId)
{
  if (dvlScene == 0 || nodeId == NULL || nodeId == DVLID_INVALID)
  {
    NVWindowsLog("error: invalid node id");
    return;
  }

  nodes.push_back(new SceneNode(this));
  nodes.back()->id = nodeId;

  sDVLNodeInfo nodeInfo;
  nyx::initInfo(nodeInfo);
  if (SUCCEEDED(dvlResult = dvlScene->RetrieveNodeInfo(nodeId, nodeFlags, &nodeInfo)))
  {
    nodes.back()->name = nodeInfo.m_szNodeName;

    if (nodes.back()->name == "Model" && nodes.back()->host->modelNode == 0)
      nodes.back()->host->modelNode = nodes.back();

    if (nodeInfo.m_uParentNodesCount == 0 && nodes.back()->host->rootNode == 0)
      nodes.back()->host->rootNode = nodes.back();

    sDVLMetadataInfo metadataInfo;
    nyx::initInfo(metadataInfo);
    if (DVLSUCCEEDED(dvlScene->RetrieveMetadata(nodes.back()->id, &metadataInfo)) && metadataInfo.m_pMetadata)
    {
      std::string metaValue;
      if (find((*metadataInfo.m_pMetadata), "Keel", "Keel_Centre", metaValue))
      {
        nv::vec2f centre = nyx::toVec2f(metaValue);
        nodes.back()->centre.x = centre.x;
        nodes.back()->centre.y = centre.y;
        nodes.back()->zoomable = true;
      }
      else nodes.back()->zoomable = false;

      if (nodes.back()->zoomable && find((*metadataInfo.m_pMetadata), "Keel", "Keel_Size", metaValue))
      {
        nv::vec2f size = nyx::toVec2f(metaValue);
        nodes.back()->dimensions.x = size.x;
        nodes.back()->dimensions.y = size.y;
      }
      else nodes.back()->zoomable = false;

      find((*metadataInfo.m_pMetadata), "Keel", "Keel_ID", nodes.back()->keelId);

      dvlScene->ReleaseMetadata(&metadataInfo);
    }

    for (uint32_t nodeIndex = 0; nodeIndex < nodeInfo.m_uChildNodesCount; nodeIndex++)
      readNodes(nodeInfo.m_pChildNodes[nodeIndex]);

    dvlScene->ReleaseNodeInfo(&nodeInfo);
  }
  else
  {
    NVWindowsLog("error: failed to retrieve scene info");
  }
}

bool ve::soko::impl::Scene::reload(std::string url)
{
  // read all nodes (+ read sizes and centres)
  // map nodes to ids and keel ids

  char buf2[MAX_PATH * 2] = {};
  wchar_t buf[MAX_PATH + 10] = {};

  wcscpy_s(buf, L"file://");
  GetModuleFileNameW(NULL, buf + 7, _countof(buf) - 7);

  //wcscat_s(buf, L"visualization_-_conference_room.dwg");
  //wcscat_s(buf, L"998 Clutch.vds");
  //wcscat_s(buf, L"998 Clutch.vds");
  //wcscat_s(buf, L"Teapot.vds");
  //wcscat_s(buf, L"LOWER BOP STACK_NDT_5Y_meta2-1.vds");
  //wcscat_s(buf, L"BOP_1.2.vds");
  //wcscat_s(buf, L"BOP_1.1.vds");
  //wcscat_s(buf, L"VIK-07-1083-021-851_Sheet2.vds");
  //wcscat_s(buf, L"X-1011196-77_sheet_1+TB.vds");
  //wcscat_s(buf, L"X-1011196-77_sheet_3+TB.vds");
  //wcscat_s(buf, L"SAP Pocket Knife.vds");
  //wcscat_s(buf, L"Test_rectangle.vds");
  //wcscat_s(buf, L"Test_rectangle2.vds");
  //wcscat_s(buf, L"LOWER BOP STACK_NDT_5Y_meta2-2.vds");

  wcsrchr(buf, L'\\')[1] = 0;
  std::wstring sceneUrl(url.begin(), url.end());
  wcscat_s(buf, sceneUrl.c_str());
  WideCharToMultiByte(CP_UTF8, 0, buf, -1, buf2, _countof(buf2), NULL, NULL);

#if DVL_VERSION_MAJOR >= 4
  dvlResult = Manager::getInstance().dvlCore->LoadScene(buf2, NULL, &dvlScene, 0);
#else
  dvlResult = Manager::getInstance().dvlCore->LoadScene(buf2, &dvlScene, 0);
#endif

  if (DVLFAILED(dvlResult))
  {
    NVWindowsLog("failed to load scene from file \"%s\"", buf2);
    return false;
  }

  dvlResult = Manager::getInstance().renderer.dvlRenderer->AttachScene(dvlScene); dvlScene->Release(), dvlScene = NULL;
  dvlScene = Manager::getInstance().renderer.dvlRenderer->GetAttachedScenePtr();

  readNodes();

  NodeVector::iterator nodeIt = nodes.begin();
  for (; nodeIt != nodes.end(); nodeIt++)
  {
    SceneNode *node = (*nodeIt);
    nodesById[node->id] = node;
    nodesByKeelId[node->keelId] = node;
  }

  if (verify())
    Manager::getInstance().camera.initialZoomToAll((*getRoot())),
    Manager::getInstance().renderer.attachedScene = this;

  return true;
}


nv::vec2f ve::soko::impl::RendererCamera::tap(float x, float y)
{
  nv::vec2f const viewTap(x, y);
  nv::vec2f const world = castCurrentScreenToWorld(viewTap);

  NVWindowsLog("actual tap: %.3f, %.3f", x, y);
  NVWindowsLog("actual world: %.3f, %.3f", world.x, world.y);

  nv::vec2f viewTapTest = castWorldToCurrentScreen(world);
  NVWindowsLog("casted tap: %.3f, %.3f", viewTapTest.x, viewTapTest.y);

  nv::vec2f homeTap = homeDimensions / 2.0f + (world - homeDrawingCentre);
  homeTap.y = homeDimensions.y - homeTap.y;

  nv::vec2f screenTap = homeTap * homeScreenScale;

  NVWindowsLog("relative tap: %.3f, %.3f", screenTap.x, screenTap.y);
  NVWindowsLog("-----------------------------------");

  if (renderer)
    renderer->dvlRenderer->Tap(screenTap.x, screenTap.y, false);
  return screenTap;
}

void ve::soko::impl::RendererCamera::setScreenSize(nv::vec2f screenSize)
{
  screenDimensions.x = screenSize.x;
  screenDimensions.y = screenSize.y;

  NVWindowsLog("screen dimensions: %.3f, %.3f", screenDimensions.x, screenDimensions.y);

  retargetCamera();
  if (renderer) renderer->onScreenSizeChanged(screenDimensions);
  /*IDependent::It listenerIt = listeners.begin();
  for (; listenerIt != listeners.end(); listenerIt++)
  (*listenerIt)->onScreenSizeChanged(screenDimensions);*/
}