#include "pch.h"
#include "VEScene.h"
#include "VESceneNode.h"

nyx::SceneNode::SceneNode(Scene *s)
{
  scene = s;
  id = DVLID_INVALID;
  pose.make_identity();
  centreAvailable = false;
  dimensionsAvailable = false;

  centre.x = 0.0f;
  centre.y = 0.0f;
  dimensions.x = 0.0f;
  dimensions.y = 0.0f;
}

void nyx::SceneNode::printInfo(sDVLMetadata &meta)
{
  for (uint32_t nvpIndex = 0; nvpIndex < meta.m_uNameValuePairsCount; nvpIndex++)
  {
    NVWindowsLog(
      "    name=\"%s\" value=\"%s\""
      , meta.m_pNameValuePairs[nvpIndex].m_szName
      , meta.m_pNameValuePairs[nvpIndex].m_szValue
      );
  }

  for (uint32_t nsIndex = 0; nsIndex < meta.m_uNamespacesCount; nsIndex++)
  {
    if (meta.m_pNamespaces[nsIndex].m_pMetadata)
    {
      NVWindowsLog(
        "  meta-ns=\"%s\":"
        , meta.m_pNamespaces[nsIndex].m_szName
        );
      printInfo((*meta.m_pNamespaces[nsIndex].m_pMetadata));
    }

  }

}

bool nyx::SceneNode::findField(sDVLMetadata *meta, std::string const &field, std::string &value)
{
  for (uint32_t nvpIndex = 0; nvpIndex < meta->m_uNameValuePairsCount; nvpIndex++)
  {
    if (field == meta->m_pNameValuePairs[nvpIndex].m_szName)
    {
      value = meta->m_pNameValuePairs[nvpIndex].m_szValue;
      return true;
    }
  }

  return false;
}

bool nyx::SceneNode::find(sDVLMetadata &meta, std::string const &ns, std::string const &field, std::string &value)
{
  for (uint32_t nsIndex = 0; nsIndex < meta.m_uNamespacesCount; nsIndex++)
  {
    if (ns == meta.m_pNamespaces[nsIndex].m_szName)
    {
      return findField(meta.m_pNamespaces[nsIndex].m_pMetadata, field, value);
    }
    else
    {
      if (find(*meta.m_pNamespaces[nsIndex].m_pMetadata, ns, field, value))
        return true;
    }
  }

  return false;
}

void nyx::SceneNode::printInfo(CoreNodeInfo &nodeInfo)
{
  NVWindowsLog(
    "<node%s> \n  id=0x%0x\n  name=\"%s\"\n  parents=%u\n  kids=%u\n  r=(%f, %f, %f)\n    (%f, %f, %f)\n    (%f, %f, %f)\n  t=(%f, %f, %f)"
    //"node%s: \n\tid=%I64x\n\tname=%s\n\tparents=%u\n\tkids=%u"
    //"node%s: \n\tid=%I64x\n\tname=%s\n\tparents=%u\n\tkids=%u\n\tassetid=%s"
    //"node%s: \n\tid=%I64x\n\tname=%s\n\tparents=%u\n\tkids=%u\n\tassetid=%s\n\tuniqueid=%s"
    //"node%s: \n\tid=%0x%0x\n\tname=%s\n\tparents=%u\n\tkids=%u\n\tassetid=%s\n\tuniqueid=%s"
    , (nodeInfo.m_uParentNodesCount ? "" : " (root)")
    , (uint32_t)(id & 0xffffffff)
    //, nodeId
    //, node_id.idh, node_id.idh
    , nodeInfo.m_szNodeName
    , nodeInfo.m_uParentNodesCount
    , nodeInfo.m_uChildNodesCount
    /*, pose.element(3, 0)
    , pose.element(3, 1)
    , pose.element(3, 2)*/
    , pose.element(0, 0)
    , pose.element(1, 0)
    , pose.element(2, 0)
    , pose.element(0, 1)
    , pose.element(1, 1)
    , pose.element(2, 1)
    , pose.element(0, 2)
    , pose.element(1, 2)
    , pose.element(2, 2)
    , pose.element(0, 3)
    , pose.element(1, 3)
    , pose.element(2, 3)
    //, nodeInfo.m_szAssetID
    //, nodeInfo.m_szUniqueID
    );

  for (uint32_t index = 0; index < nodeInfo.m_uParentNodesCount; index++)
  {
    NVWindowsLog("   <(%u)par id=0x%0x", index, nodeInfo.m_pParentNodes[index]);
  }
  for (uint32_t index = 0; index < nodeInfo.m_uChildNodesCount; index++)
  {
    NVWindowsLog("   >(%u)kid id=0x%0x", index, nodeInfo.m_pChildNodes[index]);
  }

  DVLRESULT dvlResult;
  sDVLMetadataInfo meta;
  nyx::initInfo(meta);

  dvlResult = scene->coreObj->RetrieveMetadata(id, &meta);
  if (DVLFAILED(dvlResult))
  {
    NVWindowsLog("error: failed to retrieve meta info");
    return;
  }

  if (meta.m_pMetadata)
    printInfo((*meta.m_pMetadata));

  scene->coreObj->ReleaseMetadata(&meta);
  NVWindowsLog("</node>\n");
}
