#pragma once

#include <Windows.h>
#include <memory>
#include <wrl.h>
#include <mutex>

#ifdef GetNextSibling
#undef GetNextSibling
#endif
#ifdef GetFirstSibling
#undef GetFirstSibling
#endif
#ifdef GetFirstChild
#undef GetFirstChild
#endif

namespace noekk
{
  template <typename _TySimpleType>
  static void zeroMemory(_Outref_ _TySimpleType &ref)
  {
    memset(&ref, 0, sizeof(ref));
  }
  template <typename _TySimpleType>
  static size_t zeroMemorySz(_Outref_ _TySimpleType &ref)
  {
    zeroMemory(ref);
    return sizeof(ref);
  }
  template <typename _TySimpleType>
  static void safeDelete(_Outref_ _TySimpleType *&desc)
  {
    if (desc) delete desc, desc = 0;
  }
  template <typename _TySimpleType>
  static void safeDeleteArray(_Outref_ _TySimpleType *&desc)
  {
    if (desc) delete[] desc, desc = 0;
  }
  template <typename _TySimpleType>
  static void safeRelease(_Outref_ _TySimpleType *&desc)
  {
    if (desc) desc->Release(), desc = 0;
  }

  inline static unsigned roundUp(_In_ unsigned numToRound, _In_ unsigned multiple)
  {
    if (multiple == 0) return numToRound;
    unsigned remainder = numToRound % multiple;
    if (remainder == 0) return numToRound;
    else return numToRound + multiple - remainder;
  }

  inline static int roundUp(_In_ int numToRound, _In_ int multiple)
  {
    if (multiple == 0) return numToRound;
    int remainder = abs(numToRound) % multiple;
    if (remainder == 0) return numToRound;
    else if (numToRound < 0) return -(abs(numToRound) - remainder);
    else return numToRound + multiple - remainder;
  }

  struct handle_closer { void operator()(HANDLE h) { if (h) CloseHandle(h); } };
  typedef public std::unique_ptr<void, handle_closer> ScopedHandle;
  inline HANDLE safeHandle(HANDLE h) { return (h == INVALID_HANDLE_VALUE) ? 0 : h; }
  template<class T> class ScopedObject : public Microsoft::WRL::ComPtr < T > {};


  // __declspec(align(16)) struct MyAlignedType : public AlignedNew<MyAlignedType>
  template<typename TDerived> struct AlignedNew
  {
    static void* operator new (size_t size)
    {
      static const size_t alignment = __alignof(TDerived);
      static_assert(alignment > 8,
        "AlignedNew is only useful for types with > 8 byte alignment. "
        "Did you forget a __declspec(align) on TDerived?"
        );
      void* ptr = _aligned_malloc(size, alignment);
      if (!ptr) throw std::bad_alloc();
      return ptr;
    }
    static void* operator new (size_t size, void *memory)
    {
      static const size_t alignment = __alignof(TDerived);
      static_assert(alignment > 8,
        "AlignedNew is only useful for types with > 8 byte alignment. "
        "Did you forget a __declspec(align) on TDerived?"
        );
      void* ptr = _aligned_realloc(memory, size, alignment);
      if (!ptr) throw std::bad_alloc();
      return ptr;
    }

    static void operator delete (void* ptr) { _aligned_free(ptr); }
    static void* operator new[](size_t size) { return operator new(size); }
    static void operator delete[](void* ptr) { operator delete(ptr); }
  };

  // Helper for lazily creating a D3D resource.
  template<typename T, typename TCreateFunc>
  static T* demandCreate(Microsoft::WRL::ComPtr<T>& comPtr, std::mutex& mutex, TCreateFunc createFunc, HRESULT &handle)
  {
    T* result = comPtr.Get();

    // Double-checked lock pattern.
    MemoryBarrier();

    if (!result)
    {
      std::lock_guard<std::mutex> lock(mutex);

      result = comPtr.Get();

      if (!result)
      {
        // Create the new object.
        handle = createFunc(&result);
        MemoryBarrier();
        comPtr.Attach(result);
      }
    }

    return result;
  }

  template<typename _Ty> __forceinline _Ty &toRef(_Ty *_Ptr) { return (*_Ptr); }
  template<typename _Ty> __forceinline _Ty const &toRef(_Ty const *_Ptr) { return (*_Ptr); }

}

#define _Dx_hresult_assert(resultHandle) assert(SUCCEEDED(resultHandle))
#define _Dx_hresult_assert_msg(resultHandle, message) _ASSERTE(SUCCEEDED(resultHandle), message)
#define _Dx_void_ret_on_hresult_failed(resultHandle) if(FAILED(resultHandle)) return;
#define _Dx_error_ret_on_hresult_failed(resultHandle) if(FAILED(resultHandle)) return 1;
#define _Dx_noerror_ret_on_hresult_failed(resultHandle) if(FAILED(resultHandle)) return 0;
