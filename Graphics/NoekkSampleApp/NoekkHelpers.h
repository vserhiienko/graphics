#pragma once

#include <windows.h>
#include <windowsx.h>
#include <wrl.h>
#include <d3d12.h>
#include <d3dcompiler.h>
#include <stdint.h>
#include <comdef.h>
#include <memory>

namespace noekk
{
  typedef HANDLE CoreHandle, HObject;
  typedef HWND WindowHandle, HWindow;
  typedef HINSTANCE InstanceHandle, HInstance;
  typedef HRESULT ResultHandle, HResult;

  template <unsigned int _BufferSize = 512>
  inline static void trace(_In_ const char* fmt, _In_opt_ ...)
  {
    char buffer[_BufferSize];
    va_list ap;

    va_start(ap, fmt);
    vsnprintf_s(buffer, _BufferSize - 1, fmt, ap);
    OutputDebugStringA(buffer);
    OutputDebugStringA("\n");
    va_end(ap);
  }

  template <unsigned int _BufferSize = 512>
  inline static bool traceFailed(HRESULT resultHandle)
  {
    if (FAILED(resultHandle))
    {
      _com_error comError(resultHandle);
      const char *comErrorMessage = comError.ErrorMessage();
      trace<_BufferSize>("0x%x (%s)", resultHandle, comErrorMessage);
      return true;
    }

    return false;
  }

  template <unsigned int _BufferSize = 512>
  inline static bool traceFailed(HRESULT resultHandle, const char *traceMessage)
  {
    if (FAILED(resultHandle))
    {
      _com_error comError(resultHandle);
      const char *comErrorMessage = comError.ErrorMessage();
      trace<_BufferSize>("%s0x%x (%s)", traceMessage, resultHandle, comErrorMessage);
      return true;
    }

    return false;
  }

  inline static bool traceSucceeded(HRESULT resultHandle)
  {
    if (FAILED(resultHandle))
    {
      _com_error comError(resultHandle);
      const char *comErrorMessage = comError.ErrorMessage();
      trace("0x%x (%s)", resultHandle, comErrorMessage);
      return false;
    }

    return true;
  }

  inline static bool traceSucceeded(HRESULT resultHandle, const char *traceMessage)
  {
    if (FAILED(resultHandle))
    {
      _com_error comError(resultHandle);
      const char *comErrorMessage = comError.ErrorMessage();
      trace("%s0x%x (%s)", traceMessage, resultHandle, comErrorMessage);
      return false;
    }

    return true;
  }
}
