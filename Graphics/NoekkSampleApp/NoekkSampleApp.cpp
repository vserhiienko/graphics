
#include "DxUtils.h"
#include "NoekkHelpers.h"
#include "NoekkSampleApp.h"

noekk::SampleApp::SampleApp()
{
  sampleName = "Noekk";
  renderTargetFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
  depthStencilFormat = DXGI_FORMAT_D32_FLOAT;
  renderTargetUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
}

noekk::SampleApp::~SampleApp()
{
  /* depthStencilDescHeap = nullptr;
   renderTargetDescHeap = nullptr;
   depthStencil = nullptr;
   renderTarget = nullptr;
   commandAllocator = nullptr;
   commandQueue = nullptr;
   commandList = nullptr;
   swapChain = nullptr;
   device = nullptr;*/
}

void noekk::SampleApp::mainLoop()
{
  MSG msg = { 0 };

  while (TRUE)
  {
    // check to see if any messages are waiting in the queue
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      TranslateMessage(&msg);  // translate keystroke messages into the right format
      DispatchMessage(&msg); // send the message to the WindowProc function
      if (msg.message == WM_QUIT) break; // check to see if it's time to quit
    }
  }

  destroy();
}

bool noekk::SampleApp::init()
{
  setWindow();
  return loadPipeline(), loadAssets();
}

bool noekk::SampleApp::setWindow(void)
{
  window.hooks() = this;
  window.create();
  window.setWindowTitle(sampleName);
  return true;
}

bool noekk::SampleApp::loadPipeline()
{
  if (window.handle == 0) return false;

  // create swap chain descriptor
  DXGI_SWAP_CHAIN_DESC descSwapChain;
  ZeroMemory(&descSwapChain, sizeof(descSwapChain));
  descSwapChain.BufferCount = 2;
  descSwapChain.BufferDesc.Format = renderTargetFormat;
  descSwapChain.BufferUsage = renderTargetUsage;
  descSwapChain.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
  descSwapChain.OutputWindow = window.handle;
  descSwapChain.SampleDesc.Count = 1;
  descSwapChain.Windowed = TRUE;

  // try to create device with hardware driver support
  HResult hardwareDriverError = D3D12CreateDeviceAndSwapChain(
    nullptr,
    D3D_DRIVER_TYPE_HARDWARE,
    D3D12_CREATE_DEVICE_DEBUG,
    D3D_FEATURE_LEVEL_11_0,
    //D3D_FEATURE_LEVEL_9_1,
    D3D12_SDK_VERSION,
    &descSwapChain,
    IID_PPV_ARGS(swapChain.GetAddressOf()),
    IID_PPV_ARGS(device.GetAddressOf())
    );

  if (traceFailed(hardwareDriverError, "D3D12CreateDeviceAndSwapChain(D3D_DRIVER_TYPE_HARDWARE,D3D_FEATURE_LEVEL_11_0): "))
  {
    ResultHandle warpDriverError = (D3D12CreateDeviceAndSwapChain(
      nullptr,
      D3D_DRIVER_TYPE_WARP,
      D3D12_CREATE_DEVICE_DEBUG,
      D3D_FEATURE_LEVEL_9_1,
      D3D12_SDK_VERSION,
      &descSwapChain,
      IID_PPV_ARGS(swapChain.GetAddressOf()),
      IID_PPV_ARGS(device.GetAddressOf())
      ));

    if (traceFailed(warpDriverError, "D3D12CreateDeviceAndSwapChain(D3D_DRIVER_TYPE_WARP,D3D_FEATURE_LEVEL_9_1): ")) return false;
  }

  if (traceSucceeded(swapChain->GetDesc(&descSwapChain), "GetDesc: "))
  {
    backbufferW = descSwapChain.BufferDesc.Width;
    backbufferH = descSwapChain.BufferDesc.Height;
  }
  else return false;

  HResult commandAllocError = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, commandAllocator.GetAddressOf());
  if (traceFailed(commandAllocError, "CreateCommandAllocator: ")) return false;

  device->GetDefaultCommandQueue(commandQueue.GetAddressOf());
  return true;
}

bool noekk::SampleApp::loadAssets()
{
  if (device.Get() == 0) return false;

  // create direct command list

  HResult commandListError = device->CreateCommandList(
    D3D12_COMMAND_LIST_TYPE_DIRECT,
    commandAllocator.Get(),
    nullptr,
    &commandList
    );

  if (traceFailed(commandListError, "CreateCommandList: "))
    return false;

  // create render target view descriptor heap

  D3D12_DESCRIPTOR_HEAP_DESC descHeapRtv;
  noekk::zeroMemory(descHeapRtv);
  descHeapRtv.NumDescriptors = 1;
  descHeapRtv.Type = D3D12_RTV_DESCRIPTOR_HEAP;
  descHeapRtv.Flags = 0; // shader invisible

  HResult descHeapRtvErr = device->CreateDescriptorHeap(&descHeapRtv, IID_PPV_ARGS(&renderTargetDescHeap));
  if (traceFailed(descHeapRtvErr, "CreateDescriptorHeap: ")) return false;

  // create backbuffer/rendertarget
  if (traceSucceeded(swapChain->GetBuffer(0, IID_PPV_ARGS(&renderTarget)), "GetBuffer: "))
  {
    device->CreateRenderTargetView(renderTarget.Get(), nullptr, renderTargetDescHeap->GetCPUDescriptorHandleForHeapStart());
  }
  else return false;

  // create depth stencil view descriptor heap

  D3D12_DESCRIPTOR_HEAP_DESC descHeapDsv;
  noekk::zeroMemory(descHeapDsv);
  descHeapDsv.NumDescriptors = 1;
  descHeapDsv.Type = D3D12_DSV_DESCRIPTOR_HEAP;
  descHeapDsv.Flags = 0; // shader invisible

  HResult descHeapDsvErr = device->CreateDescriptorHeap(&descHeapDsv, IID_PPV_ARGS(&depthStencilDescHeap));
  if (traceFailed(descHeapDsvErr, "CreateDescriptorHeap: ")) return false;

  D3D12_DEPTH_STENCIL_VIEW_DESC depthStencilDesc;
  noekk::zeroMemory(depthStencilDesc);
  depthStencilDesc.Format = depthStencilFormat;
  depthStencilDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
  depthStencilDesc.Flags = 0;

  CD3D12_HEAP_PROPERTIES depthStencilResourceHeapProps(
    D3D12_HEAP_TYPE_DEFAULT
    );
  CD3D12_RESOURCE_DESC depthStencilResourceDesc = CD3D12_RESOURCE_DESC::Tex2D(
    depthStencilFormat,
    backbufferW, backbufferH,
    (UINT) 1U, (UINT) 0U, // array size / mip levels
    (UINT) 1U, (UINT) 0U, // sample count / quality
    D3D12_RESOURCE_MISC_DEPTH_STENCIL
    // unknown layout
    // no alignment
    );
  HResult depthStencilError = device->CreateCommittedResource(
    &depthStencilResourceHeapProps,
    D3D12_HEAP_MISC_NONE,
    &depthStencilResourceDesc,
    D3D12_RESOURCE_USAGE_INITIAL,
    IID_PPV_ARGS(&depthStencil)
    );
  if (traceFailed(depthStencilError, "CreateCommittedResource: ")) return false;

  device->CreateDepthStencilView(depthStencil.Get(), &depthStencilDesc, depthStencilDescHeap->GetCPUDescriptorHandleForHeapStart());
  setResourceBarrier(commandList, depthStencil, D3D12_RESOURCE_USAGE_INITIAL, D3D12_RESOURCE_USAGE_DEPTH);

  commandList->Close();
  commandQueue->ExecuteCommandList(commandList.Get());

  // set the viewport for later use
  viewport.TopLeftX = 0;
  viewport.TopLeftY = 0;
  viewport.MinDepth = 0.0f;
  viewport.MaxDepth = 1.0f;
  viewport.Width = static_cast<float>(backbufferW);
  viewport.Height = static_cast<float>(backbufferH);

  rectScissor.left = 0;
  rectScissor.top = 0;
  rectScissor.right = static_cast<long>(backbufferW);
  rectScissor.bottom = static_cast<long>(backbufferW);

  awaitGPUResources();
  return true;
}

void noekk::SampleApp::destroy()
{
  renderTarget = nullptr;
  depthStencil = nullptr;
  renderTargetDescHeap = nullptr;
  depthStencilDescHeap = nullptr;
  commandList = nullptr;
  commandQueue = nullptr;
  commandAllocator = nullptr;
  swapChain = nullptr;
  device = nullptr;

  window.close();
}

void noekk::SampleApp::setResourceBarrier(
  Microsoft::WRL::ComPtr<ID3D12CommandList> &cmd,
  Microsoft::WRL::ComPtr<ID3D12Resource> &res,
  uint32_t stateBefore,
  uint32_t stateAfter
  )
{
  D3D12_RESOURCE_BARRIER_DESC descBarrier = {};
  descBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
  descBarrier.Transition.pResource = res.Get();
  descBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
  descBarrier.Transition.StateBefore = stateBefore;
  descBarrier.Transition.StateAfter = stateAfter;
  cmd->ResourceBarrier(1, &descBarrier);
}

void noekk::SampleApp::awaitGPUResources()
{
  /*
  WAITING FOR THE FRAME TO COMPLETE BEFORE CONTINUING IS NOT BEST PRACTICE.
  This is code implemented as such for simplicity.
  More advanced topics such as using fences for efficient resource usage.
  */
  {
    noekk::ScopedHandle eventHandle;
    eventHandle.reset(noekk::safeHandle(CreateEventEx(nullptr, FALSE, FALSE, EVENT_ALL_ACCESS)));

    const UINT64 fence = commandQueue->AdvanceFence();
    if (commandQueue->GetLastCompletedFence() < fence)
      // set event on fence completion
      if (traceSucceeded(commandQueue->SetEventOnFenceCompletion(fence, eventHandle.get())))
        // wait for this event
        //noekk::trace("noekk: waiting for fence"),
        WaitForSingleObject(eventHandle.get(), INFINITE)/*,
        noekk::trace("noekk: fence completed")*/;
  }
}

//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
//{
//  using namespace noekk;
//
//  SampleApp app;
//  if (app.init()) app.mainLoop();
//  else EXIT_FAILURE;
//  return EXIT_SUCCESS;
//}