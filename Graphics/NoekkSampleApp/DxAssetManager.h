#pragma once
#include <Windows.h>
#include <string>
#include <vector>
#include <concurrent_queue.h>
#include <atomic>
#include <ppl.h>
#include <ppltasks.h>

namespace noekk
{
  class AssetBlob;
  class AssetReader;
  class AssetReadingCompletionHook;

  class AssetBlob
  {
  public:
    unsigned char *data;
    size_t dataLength;

  public:
    AssetBlob(void) : data(0), dataLength(0) { }
    bool alloc(_In_ size_t);
    void dealloc(void);
    void copyTo(AssetBlob &) const;
  };

  class AssetReadingCompletionHook
  {
  public:
    virtual void onAssetLoaded(_In_ unsigned assetId, _In_ AssetBlob const &asset) = 0;
    virtual void onAssetNotFound(_In_ unsigned assetId) { }
  };

  class AssetReader
  {
  public:
    static std::atomic_uint loadingResourceCount;
    static std::vector<std::string> searchPath;
    static void addSearchPath(_In_ std::string const &path);
    static void removeSearchPath(_In_ std::string const &path);
    static void loadAsset(_In_ std::string const &assetName, _In_ unsigned assetId, _In_ AssetReadingCompletionHook *assetHook);
    static void findAndReadTo(_In_ std::wstring const &assetName, _In_ unsigned assetId, _In_ AssetReadingCompletionHook *assetHook);
    static void loadAssetAsync(_In_ std::string const &assetName, _In_ unsigned assetId, _In_ AssetReadingCompletionHook *assetHook);
    static void loadAssetAsync(_In_ std::wstring const &assetName, _In_ unsigned assetId, _In_ AssetReadingCompletionHook *assetHook);

  };
}