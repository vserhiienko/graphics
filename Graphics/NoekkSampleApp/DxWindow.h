#pragma once

// win
#include <vector>
#include <string>
#include <sstream>
#include <DirectXMath.h>

// dx
#include <DxNeon.h>
#include <DxVirtualKeys.h>

namespace noekk
{
  struct GenericEventArgs
  {
    noekk::neon::n128 data;
    GenericEventArgs(void) {}
    inline float &mouseX(void) { return data.f32[0]; }
    inline float mouseX(void) const { return data.f32[0]; }
    inline float &mouseY(void) { return data.f32[1]; }
    inline float mouseY(void) const { return data.f32[1]; }
    inline bool mouseShift(void) const { return data.u8[8] != 0ui8; }
    inline bool mouseControl(void) const { return data.u8[9] != 0ui8; }
    inline int &hwheel(void) { return data.i32[2]; }
    inline int hwheel(void) const { return data.i32[2]; }
    inline int &wheelDelta(void) { return data.i32[1]; }
    inline int wheelDelta(void) const { return data.i32[1]; }
    inline unsigned &wheelKeystate(void) { return data.u32[0]; }
    inline unsigned wheelKeystate(void) const { return data.u32[0]; }
    inline unsigned &keyCode(void) { return data.u32[0]; }
    inline unsigned keyCode(void) const { return data.u32[0]; }
    inline unsigned &keyPrevState(void) { return data.u32[1]; }
    inline unsigned keyPrevState(void) const { return data.u32[1]; }
    inline unsigned &keyRepeatCount(void) { return data.u32[2]; }
    inline unsigned keyRepeatCount(void) const{ return data.u32[2]; }
    inline unsigned &width(void) { return data.u32[0]; }
    inline unsigned width(void) const { return data.u32[0]; }
    inline unsigned &height(void) { return data.u32[1]; }
    inline unsigned height(void) const { return data.u32[1]; }
    inline unsigned &message(void) { return data.u32[3]; }
    inline unsigned message(void) const { return data.u32[3]; }
  };

  class Window;
  class WindowEvents
  {
  public:
    void *userData;
    inline WindowEvents() : userData(0) {}
    inline virtual void handlePaint(Window const*) {}
    inline virtual void handleClosed(Window const*) {}
    inline virtual void handleSizeChanged(Window const*) {}
    inline virtual void handleKeyPressed(Window const*, GenericEventArgs const&) {}
    inline virtual void handleKeyReleased(Window const*, GenericEventArgs const&) {}
    inline virtual void handleMouseWheel(Window const*, GenericEventArgs const&) {}
    inline virtual void handleMouseMoved(Window const*, GenericEventArgs const&) {}
    inline virtual void handleMouseLeftPressed(Window const*, GenericEventArgs const&) {}
    inline virtual void handleMouseLeftReleased(Window const*, GenericEventArgs const&) {}
    inline virtual void handleMouseRightPressed(Window const*, GenericEventArgs const&) {}
    inline virtual void handleMouseRightReleased(Window const*, GenericEventArgs const&) {}
    inline virtual void handleMouseMiddlePressed(Window const*, GenericEventArgs const&) {}
    inline virtual void handleMouseMiddleReleased(Window const*, GenericEventArgs const&) {}
  };

  enum DxWindowState
  {
    eDxWindowState_MaxHide = SIZE_MAXHIDE,
    eDxWindowState_MaxShow = SIZE_MAXSHOW,
    eDxWindowState_Minimized = SIZE_MINIMIZED,
    eDxWindowState_Maximized = SIZE_MAXIMIZED,
    eDxWindowState_Restored = SIZE_RESTORED,
  };

  class DxMonitorTraits
  {
  public:
    typedef std::vector<DxMonitorTraits*> Collection;

  public:
    int index;
    int area;
    bool primary;

    RECT deviceWorkRect;
    HMONITOR deviceHandle;
    std::string deviceName;

    inline int x() const { return deviceWorkRect.left; }
    inline int y() const { return deviceWorkRect.top; }
    inline int width() const { return deviceWorkRect.right - x(); }
    inline int height() const { return deviceWorkRect.bottom - y(); }

  public:
    DxMonitorTraits();
  };

  class DxWindowTraits
  {
  public:
    HWND handle;
    DxWindowState state;
    DirectX::XMINT2 pos;
    DirectX::XMINT2 dimensions;
    DirectX::XMINT2 screenDimensions;

  public:
    DxWindowTraits(void);
  };

  class Window : public DxWindowTraits
  {
    WindowEvents *m_hooks;
    DxMonitorTraits *m_currentMonitor;
    DxMonitorTraits *m_largestMonitor;
    DxMonitorTraits *m_primaryMonitor;
    DxMonitorTraits::Collection m_monitors;

  public:
    Window(void);

    void create(void);
    void update(void);
    void close(void);

    void setWindowTitle(std::string const &);

  public:
    inline WindowEvents *&hooks(void) { return m_hooks; }
    inline DxWindowTraits &traits(void) { return (*this); }

  public:
    inline WindowEvents const *&hooks(void) const { return hooks(); }
    inline DxWindowTraits const &traits(void) const { return (*this); }

  private:
    friend class DxWindowProc;
    void onClosed(void);
    void updateMonitors(void);
    void updateWindowSpatialInfo(void);

  };
}