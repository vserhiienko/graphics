#pragma once
#include <DxWindow.h>
#include <NoekkHelpers.h>

namespace noekk
{
  // class DeviceResources
  // class RenderTargetResources

  class SampleApp
    : public noekk::WindowEvents
  {

  public:
    std::string sampleName;
    D3D12_VIEWPORT viewport;
    D3D12_RECT rectScissor;
    noekk::Window window; // host surface
    int backbufferW, backbufferH;

    DXGI_FORMAT renderTargetFormat;
    DXGI_FORMAT depthStencilFormat;
    DXGI_USAGE renderTargetUsage;

    // pipeline and asset objects
    Microsoft::WRL::ComPtr<ID3D12Device> device;
    Microsoft::WRL::ComPtr<IDXGISwapChain> swapChain;
    Microsoft::WRL::ComPtr<ID3D12Resource> renderTarget;
    Microsoft::WRL::ComPtr<ID3D12Resource> depthStencil;
    Microsoft::WRL::ComPtr<ID3D12CommandList> commandList;
    Microsoft::WRL::ComPtr<ID3D12CommandQueue> commandQueue;
    Microsoft::WRL::ComPtr<ID3D12CommandAllocator> commandAllocator;
    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> renderTargetDescHeap;
    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> depthStencilDescHeap;

  public:
    SampleApp(void);
    ~SampleApp(void);

    bool init(void);
    bool setWindow(void);
    bool loadPipeline(void);
    bool loadAssets(void);
    void destroy(void);
    void mainLoop(void);

    /// <summary>
    /// Wait for the GPU to be done with all resources.
    /// </summary>
    /// <remarks>
    /// WAITING FOR THE FRAME TO COMPLETE BEFORE CONTINUING IS NOT BEST PRACTICE.
    /// </remarks>
    void awaitGPUResources(void);

  public:
    static void setResourceBarrier(
      Microsoft::WRL::ComPtr<ID3D12CommandList> &commandList,
      Microsoft::WRL::ComPtr<ID3D12Resource> &resource,
      uint32_t stateBefore,
      uint32_t stateAfter
      );

  };
}

