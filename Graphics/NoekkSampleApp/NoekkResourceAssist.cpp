#include "NoekkResourceAssist.h"

size_t noekk::ResourceAssist::nearestPow2(size_t x)
{
  x -= 1;
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  return x + 1;
}

size_t noekk::ResourceAssist::nearest256mult(size_t x)
{
  return (x + 255) & ~255;
}

void noekk::ResourceAssist::setResourceBarrier(
  Microsoft::WRL::ComPtr<ID3D12CommandList> &cmd,
  Microsoft::WRL::ComPtr<ID3D12Resource> &res,
  uint32_t stateBefore,
  uint32_t stateAfter
  )
{
  D3D12_RESOURCE_BARRIER_DESC descBarrier = {};
  descBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
  descBarrier.Transition.pResource = res.Get();
  descBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
  descBarrier.Transition.StateBefore = stateBefore;
  descBarrier.Transition.StateAfter = stateAfter;
  cmd->ResourceBarrier(1, &descBarrier);
}

noekk::HResult noekk::ResourceAssist::createVertexBufferAsCommittedDefault(
  Microsoft::WRL::ComPtr<ID3D12Device>& device, 
  Microsoft::WRL::ComPtr<ID3D12CommandList>& commandList, 
  uint8_t * buffer, 
  size_t bufferOffset, 
  size_t bufferStride, 
  size_t bufferSize, 
  Microsoft::WRL::ComPtr<ID3D12Resource>& bufferResource, 
  Microsoft::WRL::ComPtr<ID3D12Resource>& bufferUploadHeap,
  D3D12_VERTEX_BUFFER_VIEW_DESC & bufferViewDesc
  )
{
  noekk::HResult result;

  result = device->CreateCommittedResource(
    &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
    D3D12_HEAP_MISC_NONE,
    &CD3D12_RESOURCE_DESC::Buffer(bufferSize),
    D3D12_RESOURCE_USAGE_INITIAL,
    IID_PPV_ARGS(&bufferResource)
    );

  if (noekk::traceFailed(result))
  {
    return result;
  }

  result = device->CreateCommittedResource(
    &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
    D3D12_HEAP_MISC_NONE,
    &CD3D12_RESOURCE_DESC::Buffer(bufferSize),
    D3D12_RESOURCE_USAGE_GENERIC_READ,
    IID_PPV_ARGS(&bufferUploadHeap)
    );

  if (noekk::traceFailed(result))
  {
    bufferResource = nullptr;
    return result;
  }

  D3D12_SUBRESOURCE_DATA bufferSubresourceData = {};
  bufferSubresourceData.pData = buffer + bufferOffset;
  bufferSubresourceData.RowPitch = bufferSize;
  bufferSubresourceData.SlicePitch = bufferSize;

  setResourceBarrier(commandList, bufferResource, D3D12_RESOURCE_USAGE_INITIAL, D3D12_RESOURCE_USAGE_COPY_DEST);
  UpdateSubresources<1>(commandList.Get(), bufferResource.Get(), bufferUploadHeap.Get(), 0, 0, 1, &bufferSubresourceData);
  setResourceBarrier(commandList, bufferResource, D3D12_RESOURCE_USAGE_COPY_DEST, D3D12_RESOURCE_USAGE_GENERIC_READ);

  bufferViewDesc.OffsetInBytes = 0;
  bufferViewDesc.StrideInBytes = (uint32_t) bufferStride;
  bufferViewDesc.SizeInBytes = (uint32_t) bufferSize;
  return result;
}

noekk::HResult noekk::ResourceAssist::createIndexBufferAsCommittedDefault(
  Microsoft::WRL::ComPtr<ID3D12Device> &device,
  Microsoft::WRL::ComPtr<ID3D12CommandList> &commandList,
  uint8_t *buffer,
  size_t bufferOffset,
  size_t bufferStride,
  size_t bufferSize,
  Microsoft::WRL::ComPtr<ID3D12Resource> &bufferResource,
  Microsoft::WRL::ComPtr<ID3D12Resource> &bufferUploadHeap,
  D3D12_INDEX_BUFFER_VIEW_DESC &bufferViewDesc
  ) 
{
  noekk::HResult result;

  result = device->CreateCommittedResource(
    &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
    D3D12_HEAP_MISC_NONE,
    &CD3D12_RESOURCE_DESC::Buffer(bufferSize),
    D3D12_RESOURCE_USAGE_INITIAL,
    IID_PPV_ARGS(&bufferResource)
    );

  if (noekk::traceFailed(result))
  {
    return result;
  }

  result = device->CreateCommittedResource(
    &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
    D3D12_HEAP_MISC_NONE,
    &CD3D12_RESOURCE_DESC::Buffer(bufferSize),
    D3D12_RESOURCE_USAGE_GENERIC_READ,
    IID_PPV_ARGS(&bufferUploadHeap)
    );

  if (noekk::traceFailed(result))
  {
    bufferResource = nullptr;
    return result;
  }

  D3D12_SUBRESOURCE_DATA bufferSubresourceData = {};
  bufferSubresourceData.pData = buffer + bufferOffset;
  bufferSubresourceData.RowPitch = bufferSize;
  bufferSubresourceData.SlicePitch = bufferSize;

  setResourceBarrier(commandList, bufferResource, D3D12_RESOURCE_USAGE_INITIAL, D3D12_RESOURCE_USAGE_COPY_DEST);
  UpdateSubresources<1>(commandList.Get(), bufferResource.Get(), bufferUploadHeap.Get(), 0, 0, 1, &bufferSubresourceData);
  setResourceBarrier(commandList, bufferResource, D3D12_RESOURCE_USAGE_COPY_DEST, D3D12_RESOURCE_USAGE_GENERIC_READ);

  bufferViewDesc.OffsetInBytes = bufferOffset;
  bufferViewDesc.Format = DXGI_FORMAT_R16_UINT;
  bufferViewDesc.SizeInBytes = bufferSize;

  return result;
}

noekk::HResult noekk::ResourceAssist::createConstantBufferAsCommittedDefault(Microsoft::WRL::ComPtr<ID3D12Device>& device, Microsoft::WRL::ComPtr<ID3D12CommandList>& commandList, uint8_t * buffer, size_t bufferSize, Microsoft::WRL::ComPtr<ID3D12Resource>& bufferUploadHeap, D3D12_CONSTANT_BUFFER_VIEW_DESC & bufferViewDesc, bool map, void ** heapStart)
{
  noekk::HResult result = E_NOTIMPL;

  return result;
}

noekk::ConstantBuffer::ConstantBuffer()
{
  dataSize = 0;
  dataAlignment = 0;
  dataSizeAligned = 0;
  uploadHeapStart = 0;
}

noekk::ConstantBuffer::~ConstantBuffer()
{
  if (mapped && uploadHeap.Get()) uploadHeap->Unmap(0);
}

size_t noekk::ConstantBuffer::setSize(size_t size, size_t alignment)
{
  dataSize = size;
  dataAlignment = alignment ? std::max(ResourceAssist::nearestPow2(alignment), 16ull) : 0;
  dataSizeAligned = (dataSize + 255) & ~255;
  return dataSizeAligned;
}

void noekk::ConstantBuffer::create(Microsoft::WRL::ComPtr<ID3D12Device>& device)
{
  noekk::HResult result;

  D3D12_DESCRIPTOR_HEAP_DESC descHeapDesc;
  noekk::zeroMemory(descHeapDesc);
  descHeapDesc.NumDescriptors = 1;
  // this flag indicates that this descriptor heap can be bound to the pipeline and that descriptors contained in it can be referenced by a root table
  descHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_SHADER_VISIBLE;
  descHeapDesc.Type = D3D12_CBV_SRV_UAV_DESCRIPTOR_HEAP;
  result = device->CreateDescriptorHeap(&descHeapDesc, IID_PPV_ARGS(&descHeap));
  assert(SUCCEEDED(result));

  // create an upload heap
  result = device->CreateCommittedResource(
    &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
    D3D12_HEAP_MISC_NONE,
    &CD3D12_RESOURCE_DESC::Buffer(dataSizeAligned, D3D12_RESOURCE_MISC_NONE, dataAlignment ? D3D12_RESOURCE_MAPPING_ALIGNMENT : 0),
    D3D12_RESOURCE_USAGE_GENERIC_READ,
    IID_PPV_ARGS(uploadHeap.ReleaseAndGetAddressOf())
    );
  assert(SUCCEEDED(result));

  // create constant buffer view description to access the upload buffer
  D3D12_CONSTANT_BUFFER_VIEW_DESC viewDesc;
  viewDesc.OffsetInBytes = 0;
  viewDesc.SizeInBytes = (uint32_t) dataSizeAligned;
  device->CreateConstantBufferView(uploadHeap.Get(), &viewDesc, descHeap->GetCPUDescriptorHandleForHeapStart());

  uploadHeap->Map(0, (void**) &uploadHeapStart);
}

void noekk::ConstantBuffer::copyFrom(void const* data)
{
  if (uploadHeapStart) memcpy(uploadHeapStart, data, dataSize);
  else noekk::trace("mapTo: uploadHeapStart=0x%0x", uploadHeapStart);
}

noekk::LoadShaderBlobHook::~LoadShaderBlobHook()
{
  csBlob.dealloc();
  vsBlob.dealloc();
  hsBlob.dealloc();
  dsBlob.dealloc();
  gsBlob.dealloc();
  psBlob.dealloc();
}

void noekk::LoadShaderBlobHook::onAssetLoaded(unsigned assetId, noekk::AssetBlob const & asset)
{
  switch (assetId)
  {
  case csAssetId: asset.copyTo(csBlob); break;
  case vsAssetId: asset.copyTo(vsBlob); break;
  case hsAssetId: asset.copyTo(hsBlob); break;
  case dsAssetId: asset.copyTo(dsBlob); break;
  case gsAssetId: asset.copyTo(gsBlob); break;
  case psAssetId: asset.copyTo(psBlob); break;
  }
}
