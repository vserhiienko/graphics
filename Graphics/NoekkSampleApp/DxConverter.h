#pragma once

#include <Windows.h>
#include <string>
#include <sal.h>

namespace noekk
{
  class DxConverter
  {
  public:
    inline static void string8To16(_In_z_ const std::string &szValue8, _Outref_ std::wstring &szValue16) { szValue16 = std::wstring(szValue8.begin(), szValue8.end()); }
    inline static void string16To8(_Outref_ std::string &szValue8, _In_z_ const std::wstring &szValue16) { szValue8 = std::string(szValue16.begin(), szValue16.end()); }
    inline static std::wstring string8To16(_In_z_ const std::string &szValue8) { return std::wstring(szValue8.begin(), szValue8.end()); }
    inline static std::string string16To8(_In_z_ const std::wstring &szValue16) { return std::string(szValue16.begin(), szValue16.end()); }

    inline static void toNumber(_In_z_ const std::wstring &szValue16, _Outref_ double &fValue64) { fValue64 = std::stod(szValue16.c_str()); }
    inline static void toNumber(_In_z_ const std::wstring &szValue16, _Outref_ float &fValue32) { fValue32 = std::stof(szValue16.c_str()); }
    inline static void toNumber(_In_z_ const std::wstring &szValue16, _Outref_ uint64_t &iValue64) { iValue64 = std::stoull(szValue16.c_str()); }
    inline static void toNumber(_In_z_ const std::wstring &szValue16, _Outref_ uint32_t &iValue32) { iValue32 = std::stoul(szValue16.c_str()); }
    inline static void toNumber(_In_z_ const std::wstring &szValue16, _Outref_ int64_t &iValue64) { iValue64 = std::stoll(szValue16.c_str()); }
    inline static void toNumber(_In_z_ const std::wstring &szValue16, _Outref_ int32_t &iValue32) { iValue32 = std::stoi(szValue16.c_str()); }

    inline static void toNumber(_In_z_ const std::string &szValue8, _Outref_ double &fValue64) { fValue64 = std::stod(szValue8.c_str()); }
    inline static void toNumber(_In_z_ const std::string &szValue8, _Outref_ float &fValue32) { fValue32 = std::stof(szValue8.c_str()); }
    inline static void toNumber(_In_z_ const std::string &szValue8, _Outref_ uint64_t &iValue64) { iValue64 = std::stoull(szValue8.c_str()); }
    inline static void toNumber(_In_z_ const std::string &szValue8, _Outref_ uint32_t &iValue32) { iValue32 = std::stoul(szValue8.c_str()); }
    inline static void toNumber(_In_z_ const std::string &szValue8, _Outref_ int64_t &iValue64) { iValue64 = std::stoll(szValue8.c_str()); }
    inline static void toNumber(_In_z_ const std::string &szValue8, _Outref_ int32_t &iValue32) { iValue32 = std::stoi(szValue8.c_str()); }

    inline static void toString(_Outref_ std::wstring &szValue16, _In_ const double &fValue64) { szValue16 = std::to_wstring((long double) fValue64); }
    inline static void toString(_Outref_ std::wstring &szValue16, _In_ const float &fValue32) { szValue16 = std::to_wstring((long double) fValue32); }
    inline static void toString(_Outref_ std::wstring &szValue16, _In_ const int64_t &iValue64) { szValue16 = std::to_wstring((_Longlong) iValue64); }
    inline static void toString(_Outref_ std::wstring &szValue16, _In_ const int32_t &iValue32) { szValue16 = std::to_wstring((_Longlong) iValue32); }
    inline static void toString(_Outref_ std::wstring &szValue16, _In_ const uint64_t &uiValue64) { szValue16 = std::to_wstring((_ULonglong) uiValue64); }
    inline static void toString(_Outref_ std::wstring &szValue16, _In_ const uint32_t &uiValue32) { szValue16 = std::to_wstring((_ULonglong) uiValue32); }

    inline static void toString(_Outref_ std::string &szValue8, _In_ const DOUBLE &fValue64) { szValue8 = std::to_string((long double) fValue64); }
    inline static void toString(_Outref_ std::string &szValue8, _In_ const float &fValue32) { szValue8 = std::to_string((long double) fValue32); }
    inline static void toString(_Outref_ std::string &szValue8, _In_ const int64_t &iValue64) { szValue8 = std::to_string((_Longlong) iValue64); }
    inline static void toString(_Outref_ std::string &szValue8, _In_ const int32_t &iValue32) { szValue8 = std::to_string((_Longlong) iValue32); }
    inline static void toString(_Outref_ std::string &szValue8, _In_ const uint64_t &uiValue64) { szValue8 = std::to_string((_ULonglong) uiValue64); }
    inline static void toString(_Outref_ std::string &szValue8, _In_ const uint32_t &uiValue32) { szValue8 = std::to_string((_ULonglong) uiValue32); }
  };
};
