#include "DxWindow.h"
#include "DxUtils.h"
#include "NoekkHelpers.h"
#include <Windowsx.h>
#include <algorithm>

class noekk::DxWindowProc
{
public:
  template <uint32_t _EventId> static GenericEventArgs create(WPARAM wparam, LPARAM lparam);
  template <> static GenericEventArgs create<noekk::Message::KeyReleased>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.keyCode() = (unsigned)wparam;
    args.message() = noekk::Message::KeyReleased;
    noekk::CorePhysicalKeyStatus::ExtractSlim(lparam, args.keyPrevState(), args.keyRepeatCount());
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::KeyPressed>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.keyCode() = (uint32_t)wparam;
    args.message() = noekk::Message::KeyPressed;
    noekk::CorePhysicalKeyStatus::ExtractSlim(lparam, args.keyPrevState(), args.keyRepeatCount());
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::LeftButtonReleased>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = noekk::Message::LeftButtonReleased;
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::LeftButtonPressed>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = noekk::Message::LeftButtonPressed;
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::RightButtonReleased>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = noekk::Message::RightButtonReleased;
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::RightButtonPressed>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = noekk::Message::RightButtonPressed;
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::MiddleButtonReleased>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = noekk::Message::MiddleButtonReleased;
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::MiddleButtonPressed>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = noekk::Message::MiddleButtonPressed;
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::MouseMove>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.message() = noekk::Message::MouseMove;
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::MouseHWheel>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.wheelKeystate() = GET_KEYSTATE_WPARAM(wparam);
    args.wheelDelta() = GET_WHEEL_DELTA_WPARAM(wparam);
    args.message() = noekk::Message::MouseHWheel;
    args.hwheel() = 1;
    return args;
  }
  template <> static GenericEventArgs create<noekk::Message::MouseWheel>(WPARAM wparam, LPARAM lparam)
  {
    GenericEventArgs args; //args.data = { 0 };
    args.wheelKeystate() = GET_KEYSTATE_WPARAM(wparam);
    args.wheelDelta() = GET_WHEEL_DELTA_WPARAM(wparam);
    args.message() = noekk::Message::MouseWheel;
    args.hwheel() = 0;
    return args;
  }

  static LRESULT CALLBACK internalProcess(noekk::Window &window, HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
  {
    if (window.handle == hwnd && window.m_hooks) switch (msg)
    {
    case noekk::Message::Paint:                      window.m_hooks->handlePaint(&window); return NULL;
    case noekk::Message::MouseMove:                  window.m_hooks->handleMouseMoved(&window, create<noekk::Message::MouseMove>(wparam, lparam)); return NULL;
    case noekk::Message::LeftButtonPressed:          window.m_hooks->handleMouseLeftPressed(&window, create<noekk::Message::LeftButtonPressed>(wparam, lparam)); return NULL;
    case noekk::Message::LeftButtonReleased:         window.m_hooks->handleMouseLeftReleased(&window, create<noekk::Message::LeftButtonReleased>(wparam, lparam)); return NULL;
    case noekk::Message::KeyPressed:                 window.m_hooks->handleKeyPressed(&window, create<noekk::Message::KeyPressed>(wparam, lparam)); return NULL;
    case noekk::Message::KeyReleased:                window.m_hooks->handleKeyReleased(&window, create<noekk::Message::KeyReleased>(wparam, lparam)); return NULL;
    case noekk::Message::RightButtonPressed:         window.m_hooks->handleMouseRightPressed(&window, create<noekk::Message::LeftButtonPressed>(wparam, lparam)); return NULL;
    case noekk::Message::RightButtonReleased:        window.m_hooks->handleMouseRightReleased(&window, create<noekk::Message::LeftButtonPressed>(wparam, lparam)); return NULL;
    case noekk::Message::MouseHWheel:                window.m_hooks->handleMouseWheel(&window, create<noekk::Message::MouseWheel>(wparam, lparam)); return NULL;
    case noekk::Message::MouseWheel:                 window.m_hooks->handleMouseWheel(&window, create<noekk::Message::MouseWheel>(wparam, lparam)); return NULL;
    case noekk::Message::Size:                       window.state = (DxWindowState)wparam; return DefWindowProc(hwnd, msg, wparam, lparam);
    case noekk::Message::ExitSizeMove:               window.m_hooks->handleSizeChanged(&window); return DefWindowProc(hwnd, msg, wparam, lparam);
    case noekk::Message::Destroy:                    window.onClosed(); return DefWindowProc(hwnd, msg, wparam, lparam);
    }

    return DefWindowProc(hwnd, msg, wparam, lparam);
  }
};

static LRESULT CALLBACK internalProcess(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  LONG_PTR impl = GetWindowLongPtrA(hwnd, 0);
  if (impl == NULL) return DefWindowProc(hwnd, msg, wparam, lparam);
  else return noekk::DxWindowProc::internalProcess(*(noekk::Window*)impl, hwnd, msg, wparam, lparam);
}

static BOOL WINAPI enumMonitorsProc(HMONITOR handle, HDC context, LPRECT rect, LPARAM data)
{
  MONITORINFOEXA info;
  noekk::DxMonitorTraits::Collection *monitors;

  info.cbSize = sizeof(info);
  monitors = (noekk::DxMonitorTraits::Collection *)data;
  if (monitors && GetMonitorInfoA(handle, &info))
  {
    monitors->push_back(new noekk::DxMonitorTraits());

    monitors->back()->deviceHandle = handle;
    monitors->back()->deviceName = info.szDevice;
    monitors->back()->deviceWorkRect = info.rcWork;
    monitors->back()->primary = info.dwFlags == MONITORINFOF_PRIMARY;

    monitors->back()->index = (int)monitors->size() - 1;
    monitors->back()->area = monitors->back()->width() * monitors->back()->height();
  }

  return true;
}

noekk::DxMonitorTraits::DxMonitorTraits()
{
  deviceHandle = 0;
  noekk::zeroMemory(deviceWorkRect);
}

noekk::DxWindowTraits::DxWindowTraits()
{
  noekk::zeroMemory((*this));
  state = eDxWindowState_Minimized;
}

noekk::Window::Window()
{
  hooks() = 0;
  updateMonitors();
  updateWindowSpatialInfo();
}

void noekk::Window::onClosed()
{
  if (!handle) return;

  UnregisterClass("DxSampleAppWindow", GetModuleHandle(NULL));
  ShowCursor(true);
  handle = nullptr;
  m_hooks->handleClosed(this);

  PostQuitMessage(0);
}

void noekk::Window::setWindowTitle(std::string const &title)
{
  if (!handle) return;
  SetWindowTextA(handle, title.c_str());
}

void noekk::Window::create()
{
  static const unsigned long style
    = WS_CLIPSIBLINGS
    | WS_CLIPCHILDREN
    | WS_VISIBLE
    | WS_POPUP;

  WNDCLASSEX wc;
  unsigned short atom;

  noekk::zeroMemory(wc);
  wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
  wc.hCursor = LoadCursor(NULL, IDC_ARROW);
  wc.lpszClassName = "DxSampleAppWindow";
  wc.lpszMenuName = NULL;
  wc.lpfnWndProc = &internalProcess;
  wc.hIconSm = wc.hIcon;
  wc.hInstance = GetModuleHandleA(NULL);
  wc.cbWndExtra = sizeof(this);
  wc.cbSize = sizeof wc;
  wc.cbClsExtra = 0;

  atom = RegisterClassEx(&wc);
  if (atom == INVALID_ATOM)
  {
    auto error = GetLastError();
    return;
  }

  handle = CreateWindowEx(
    NULL, "DxSampleAppWindow", "DxSampleApp", style,
    pos.x, pos.y, dimensions.x, dimensions.y,
    NULL, NULL, GetModuleHandleA(NULL), NULL
    );

  if (handle)
  {
    SetWindowLongPtr(handle, 0, (LONG_PTR)this);
    ShowWindow(handle, SW_SHOW);
    SetFocus(handle);
    UpdateWindow(handle);
  }
}

void noekk::Window::update()
{
  if (handle) SetWindowPos(handle, NULL, pos.x, pos.y, dimensions.x, dimensions.y, SWP_SHOWWINDOW), UpdateWindow(handle);
}

void noekk::Window::close()
{
  if (handle) SendMessage(handle, WM_CLOSE, NULL, NULL), onClosed();
}

void noekk::Window::updateMonitors()
{
  EnumDisplayMonitors(NULL, NULL, enumMonitorsProc, (LPARAM)&m_monitors);
  std::sort(m_monitors.begin(), m_monitors.end(), [](noekk::DxMonitorTraits *m1, noekk::DxMonitorTraits *m2) { return m1->area > m2->area; });
  m_primaryMonitor = *std::find_if(m_monitors.begin(), m_monitors.end(), [](noekk::DxMonitorTraits *m) { return m->primary; });
  m_largestMonitor = m_monitors.front();
  m_currentMonitor = m_largestMonitor;
}

void noekk::Window::updateWindowSpatialInfo()
{
  screenDimensions.x = m_currentMonitor->width();
  screenDimensions.y = m_currentMonitor->height();
  int desiredW = screenDimensions.x * 4 / 5;
  int desiredH = screenDimensions.y * 4 / 5;
  dimensions.x = noekk::roundUp(desiredW, 16);
  dimensions.y = noekk::roundUp(desiredH, 16);
  pos.x = m_currentMonitor->x() + (screenDimensions.x - dimensions.x) / 2;
  pos.y = m_currentMonitor->y() + (screenDimensions.y - dimensions.y) / 2;
}

#pragma region Win32 API

// GetMonitorInfo function
/*
GetMonitorInfo function
The GetMonitorInfo function retrieves information about a display monitor.
Syntax

bool GetMonitorInfo(
_In_   HMONITOR hMonitor,
_Out_  LPMONITORINFO lpmi
);

Parameters

hMonitor [in]
A handle to the display monitor of interest.
lpmi [out]
A pointer to a MONITORINFO or MONITORINFOEX structure that receives information about the specified display monitor.
You must set the cbSize member of the structure to sizeof(MONITORINFO) or sizeof(MONITORINFOEX) before calling the GetMonitorInfo function. Doing so lets the function determine the type of structure you are passing to it.
The MONITORINFOEX structure is a superset of the MONITORINFO structure. It has one additional member: a string that contains a name for the display monitor. Most applications have no use for a display monitor name, and so can save some bytes by using a MONITORINFO structure.
Return value

If the function succeeds, the return value is nonzero.
If the function fails, the return value is zero.
*/

// EnumDisplayMonitors function
/*
EnumDisplayMonitors function
The EnumDisplayMonitors function enumerates display monitors (including invisible pseudo-monitors associated with the mirroring drivers) that intersect a region formed by the intersection of a specified clipping rectangle and the visible region of a device context. EnumDisplayMonitors calls an application-defined MonitorEnumProc callback function once for each monitor that is enumerated. Note that GetSystemMetrics (SM_CMONITORS) counts only the display monitors.
Syntax

C++

bool EnumDisplayMonitors(
_In_  HDC hdc,
_In_  LPCRECT lprcClip,
_In_  MONITORENUMPROC lpfnEnum,
_In_  LPARAM dwData
);

Parameters

hdc [in]
A handle to a display device context that defines the visible region of interest.
If this parameter is NULL, the hdcMonitor parameter passed to the callback function will be NULL, and the visible region of interest is the virtual screen that encompasses all the displays on the desktop.
lprcClip [in]
A pointer to a RECT structure that specifies a clipping rectangle. The region of interest is the intersection of the clipping rectangle with the visible region specified by hdc.
If hdc is non-NULL, the coordinates of the clipping rectangle are relative to the origin of the hdc. If hdc is NULL, the coordinates are virtual-screen coordinates.
This parameter can be NULL if you don't want to clip the region specified by hdc.
lpfnEnum [in]
A pointer to a MonitorEnumProc application-defined callback function.
dwData [in]
Application-defined data that EnumDisplayMonitors passes directly to the MonitorEnumProc function.
Return value

If the function succeeds, the return value is nonzero.
If the function fails, the return value is zero.
*/

#pragma endregion