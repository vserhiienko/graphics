#pragma once
#include <DxUtils.h>
#include <NoekkHelpers.h>
#include <DxAssetManager.h>

namespace noekk
{
  class ResourceAssist
  {
  public:

    static noekk::HResult createVertexBufferAsCommittedDefault(
      _In_ Microsoft::WRL::ComPtr<ID3D12Device> &device,
      _Inout_ Microsoft::WRL::ComPtr<ID3D12CommandList> &commandList,
      _In_ uint8_t *buffer,
      _In_ size_t bufferOffset,
      _In_ size_t bufferStride,
      _In_ size_t bufferSize,
      _Outref_ Microsoft::WRL::ComPtr<ID3D12Resource> &bufferResource,
      _Outref_ Microsoft::WRL::ComPtr<ID3D12Resource> &bufferUploadHeap,
      _Outref_ D3D12_VERTEX_BUFFER_VIEW_DESC &bufferViewDesc
      );
    static noekk::HResult createIndexBufferAsCommittedDefault(
      _In_ Microsoft::WRL::ComPtr<ID3D12Device> &device,
      _Inout_ Microsoft::WRL::ComPtr<ID3D12CommandList> &commandList,
      _In_ uint8_t *buffer,
      _In_ size_t bufferOffset,
      _In_ size_t bufferStride,
      _In_ size_t bufferSize,
      _Outref_  Microsoft::WRL::ComPtr<ID3D12Resource> &bufferResource,
      _Outref_ Microsoft::WRL::ComPtr<ID3D12Resource> &bufferUploadHeap,
      _Outref_ D3D12_INDEX_BUFFER_VIEW_DESC &bufferViewDesc
      );
    /// <see><c>noekk::ConstantBuffer</c></see>
    static noekk::HResult createConstantBufferAsCommittedDefault(
      _In_ Microsoft::WRL::ComPtr<ID3D12Device> &device,
      _Inout_ Microsoft::WRL::ComPtr<ID3D12CommandList> &commandList,
      _In_ uint8_t *buffer,
      _In_ size_t bufferSize,
      _Outref_ Microsoft::WRL::ComPtr<ID3D12Resource> &bufferUploadHeap,
      _Outref_ D3D12_CONSTANT_BUFFER_VIEW_DESC &bufferViewDesc,
      _In_opt_ bool map = true,
      _In_opt_ void **heapStart = 0
      );
    static void setResourceBarrier(
      _Inout_ Microsoft::WRL::ComPtr<ID3D12CommandList> &cmd,
      _Inout_ Microsoft::WRL::ComPtr<ID3D12Resource> &res,
      _In_ uint32_t stateBefore,
      _In_ uint32_t stateAfter
      );

    static size_t nearestPow2(size_t);
    static size_t nearest256mult(size_t);

  };

  class LoadShaderBlobHook : public noekk::AssetReadingCompletionHook
  {
  public:
    static const unsigned int csAssetId = 0;
    static const unsigned int vsAssetId = 1;
    static const unsigned int hsAssetId = 2;
    static const unsigned int dsAssetId = 3;
    static const unsigned int gsAssetId = 4;
    static const unsigned int psAssetId = 5;

  public:
    noekk::AssetBlob csBlob;
    noekk::AssetBlob vsBlob;
    noekk::AssetBlob hsBlob;
    noekk::AssetBlob dsBlob;
    noekk::AssetBlob gsBlob;
    noekk::AssetBlob psBlob;

  public:
    virtual ~LoadShaderBlobHook(void);
    virtual void onAssetLoaded(_In_ unsigned assetId, _In_ noekk::AssetBlob const &asset);

  };

  class ConstantBuffer
  {
    bool mapped;
    size_t dataSize;
    size_t dataAlignment;
    size_t dataSizeAligned;
    void *uploadHeapStart;

  public:
    Microsoft::WRL::ComPtr<ID3D12Resource> uploadHeap;
    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> descHeap;

  public:
    ConstantBuffer(void);
    ~ConstantBuffer(void);

    /// <summary>
    /// Sets buffer size and calculates actual buffer size
    /// </summary>
    /// <returns>
    /// Actual buffer size (aligned)
    /// </returns>
    size_t setSize(_In_ size_t size, _In_ size_t alignment = 0);
    /// <summary>
    /// Create buffer resource and upload heap
    /// </summary>
    void create(_In_ Microsoft::WRL::ComPtr<ID3D12Device> &device);
    /// <summary>
    /// Copies provided data to heap
    /// </summary>
    void copyFrom(_In_ void const *data);

  public:
    /// <summary>
    /// Create buffer resource and upload heap
    /// </summary>
    template <typename _Ty>
    inline void create(_In_ Microsoft::WRL::ComPtr<ID3D12Device> &device)
    {
      setSize(sizeof(_Ty));
      create(device);
    }
    /// <summary>
    /// Create buffer resource and upload heap
    /// *** NOT IMPLEMENTED
    /// </summary>
    template <typename _Ty>
    inline void createAligned(_In_ Microsoft::WRL::ComPtr<ID3D12Device> &device)
    {
      setSize(sizeof(_Ty), __alignof(_Ty));
      create(device);
    }
    /// <summary>
    /// Sets buffer size and calculates actual buffer size
    /// </summary>
    /// <returns>
    /// Actual buffer size (aligned)
    /// </returns>
    template <typename _Ty>
    inline size_t setSize()
    {
      return setSize(sizeof(_Ty));
    }
    /// <summary>
    /// Copies provided data to heap
    /// </summary>
    template <typename _Ty>
    inline void copyFrom(_Ty const &data)
    {
      copyFrom((void const*)&data);
    }
    /// <summary>
    /// Maps upload heap start pointer to provided data pointer
    /// </summary>
    template <typename _Ty>
    inline _Ty *mapTo()
    {
      if (uploadHeapStart && sizeof(_Ty) <= dataSizeAligned) return new (uploadHeapStart) _Ty();
      else return 0;
    }
  };

}