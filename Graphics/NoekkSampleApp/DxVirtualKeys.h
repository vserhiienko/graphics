#pragma once

#include <sal.h>
#include <Windows.h>

namespace noekk
{
  struct VirtualKey
  {
    static const unsigned None = 0; // No virtual key value.

    // Letters

    static const unsigned A = L'A'; // The letter "A" key. 
    static const unsigned B = L'B'; // The letter "B" key. 
    static const unsigned C = L'C'; // The letter "C" key. 
    static const unsigned D = L'D'; // The letter "D" key.
    static const unsigned E = L'E'; // The letter "E" key. 
    static const unsigned F = L'F'; // The letter "F" key. 
    static const unsigned G = L'G'; // The letter "G" key. 
    static const unsigned H = L'H'; // The letter "H" key. 
    static const unsigned I = L'I'; // The letter "I" key. 
    static const unsigned J = L'J'; // The letter "J" key. 
    static const unsigned K = L'K'; // The letter "K" key. 
    static const unsigned L = L'L'; // The letter "L" key. 
    static const unsigned M = L'M'; // The letter "M" key. 
    static const unsigned N = L'N'; // The letter "N" key. 
    static const unsigned O = L'O'; // The letter "O" key. 
    static const unsigned P = L'P'; // The letter "P" key. 
    static const unsigned Q = L'Q'; // The letter "Q" key. 
    static const unsigned R = L'R'; // The letter "R" key. 
    static const unsigned S = L'S'; // The letter "S" key. 
    static const unsigned T = L'T'; // The letter "T" key. 
    static const unsigned U = L'U'; // The letter "U" key. 
    static const unsigned V = L'V'; // The letter "V" key. 
    static const unsigned W = L'W'; // The letter "W" key. 
    static const unsigned X = L'X'; // The letter "X" key. 
    static const unsigned Y = L'Y'; // The letter "Y" key. 
    static const unsigned Z = L'Z'; // The letter "Z" key. 

    // Numbers

    static const unsigned NumericKeyLock = VK_NUMLOCK; // The Num Lock key.
    static const unsigned Number0 = L'0';	 // The number "0" key. 
    static const unsigned Number1 = Number0 + 1; // The number "1" key. 
    static const unsigned Number2 = Number0 + 2; // The number "2" key. 
    static const unsigned Number3 = Number0 + 3; // The number "3" key. 
    static const unsigned Number4 = Number0 + 4; // The number "4" key. 
    static const unsigned Number5 = Number0 + 5; // The number "5" key. 
    static const unsigned Number6 = Number0 + 6; // The number "6" key. 
    static const unsigned Number7 = Number0 + 7; // The number "7" key. 
    static const unsigned Number8 = Number0 + 8; // The number "8" key. 
    static const unsigned Number9 = Number0 + 9; // The number "9" key. 
    static const unsigned NumberPad0 = VK_NUMPAD0; // The number "0" key as located on a numeric pad.
    static const unsigned NumberPad1 = NumberPad0 + 1; // The number "1" key as located on a numeric pad.
    static const unsigned NumberPad2 = NumberPad0 + 2; // The number "2" key as located on a numeric pad.
    static const unsigned NumberPad3 = NumberPad0 + 3; // The number "3" key as located on a numeric pad.
    static const unsigned NumberPad4 = NumberPad0 + 4; // The number "4" key as located on a numeric pad.
    static const unsigned NumberPad5 = NumberPad0 + 5; // The number "5" key as located on a numeric pad.
    static const unsigned NumberPad6 = NumberPad0 + 6; // The number "6" key as located on a numeric pad.
    static const unsigned NumberPad7 = NumberPad0 + 7; // The number "7" key as located on a numeric pad.
    static const unsigned NumberPad8 = NumberPad0 + 8; // The number "8" key as located on a numeric pad.
    static const unsigned NumberPad9 = NumberPad0 + 9; // The number "9" key as located on a numeric pad.

    // Math 

    static const unsigned Add = VK_ADD; // The add (+) operation key as located on a numeric pad.
    static const unsigned Divide = VK_DIVIDE; // The divide (/) operation key as located on a numeric pad.
    static const unsigned Multiply = VK_MULTIPLY; // The multiply (*) operation key as located on a numeric pad.
    static const unsigned Subtract = VK_SUBTRACT; // The subtract (-) operation key as located on a numeric pad.

    // Function Keys

    static const unsigned F1 = VK_F1;	// The function key 1.
    static const unsigned F2 = VK_F2;	// The function key 2. 
    static const unsigned F3 = VK_F3;	// The function key 3.
    static const unsigned F4 = VK_F4;	// The function key 4.
    static const unsigned F5 = VK_F5;	// The function key 5.
    static const unsigned F6 = VK_F6;	// The function key 6.
    static const unsigned F7 = VK_F7;	// The function key 7.
    static const unsigned F8 = VK_F8;	// The function key 8.
    static const unsigned F9 = VK_F9;	// The function key 9.
    static const unsigned F10 = VK_F10; // The function key 10.
    static const unsigned F11 = VK_F11; // The function key 11.
    static const unsigned F12 = VK_F12; // The function key 12.
    static const unsigned F13 = VK_F13; // The function key 13.
    static const unsigned F14 = VK_F14; // The function key 14.
    static const unsigned F15 = VK_F15; // The function key 15.
    static const unsigned F16 = VK_F16; // The function key 16. 
    static const unsigned F17 = VK_F17; // The function key 17.
    static const unsigned F18 = VK_F18; // The function key 18.
    static const unsigned F19 = VK_F19; // The function key 19.
    static const unsigned F20 = VK_F20; // The function key 20.
    static const unsigned F21 = VK_F21; // The function key 21.
    static const unsigned F22 = VK_F22; // The function key 22.
    static const unsigned F23 = VK_F23; // The function key 23.
    static const unsigned F24 = VK_F24; // The function key 24. 

    // Special

    static const unsigned Accept = VK_ACCEPT; // The accept button or key.
    static const unsigned Application = VK_APPS; // The application key or button.
    static const unsigned Back = VK_BACK;	 // The virtual back key or button.
    static const unsigned Cancel = VK_CANCEL; // The cancel key or button.
    static const unsigned CapitalLock = VK_CAPITAL; // The Caps Lock key or button.
    static const unsigned Clear = VK_CLEAR; // The Clear key or button.
    static const unsigned Convert = VK_CONVERT; // The convert button or key.
    static const unsigned NonConvert = VK_NONCONVERT; // The nonconvert button or key.
    static const unsigned Decimal = VK_DECIMAL; // The Ctrl key. 
    static const unsigned Delete = VK_DELETE; // The Delete key.
    static const unsigned End = VK_END; // The End key.
    static const unsigned Enter = VK_RETURN; // The Enter key.
    static const unsigned Escape = VK_ESCAPE; // The Esc key.
    static const unsigned Execute = VK_EXECUTE; // The execute key or button.
    static const unsigned Final = VK_FINAL; // The Final symbol key-shift button.
    static const unsigned Help = VK_HELP; // The Help key or button.
    static const unsigned Home = VK_HOME; // The Home key.
    static const unsigned Insert = VK_INSERT; // The Insert key.
    static const unsigned ModeChange = VK_MODECHANGE; // The mode change key.
    static const unsigned Pause = VK_PAUSE; // The Pause key or button. 
    static const unsigned Print = VK_PRINT; // The Print key or button. 
    static const unsigned Scroll = VK_SCROLL; // The Scroll Lock (ScrLk) key.
    static const unsigned Select = VK_SELECT; // The Select key or button.
    static const unsigned Separator = VK_SEPARATOR; // The separator key as located on a numeric pad.
    static const unsigned Sleep = VK_SLEEP; // The sleep key or button.
    static const unsigned Snapshot = VK_SNAPSHOT; // The snapshot key or button.
    static const unsigned Space = VK_SPACE; // The Spacebar key or button.
    static const unsigned Tab = VK_TAB; // The Tab key.

    // Pairs 

    static const unsigned LeftButton = VK_LBUTTON; // The left mouse button.
    static const unsigned RightButton = VK_RBUTTON; // The right mouse button.
    static const unsigned MiddleButton = VK_MBUTTON; // The middle mouse button.
    static const unsigned Control = VK_CONTROL; // The Ctrl key. 
    static const unsigned LeftControl = VK_LCONTROL; // The left Ctrl key.
    static const unsigned RightControl = VK_RCONTROL; // The right Ctrl key.
    static const unsigned Menu = VK_MENU; // The menu key or button.
    static const unsigned LeftMenu = VK_LMENU; // The left menu key.
    static const unsigned RightMenu = VK_RMENU; // The right menu key.
    static const unsigned Shift = VK_SHIFT; // The Shift key.
    static const unsigned LeftShift = VK_LSHIFT; // The left Shift key.
    static const unsigned RightShift = VK_RSHIFT; // The right Shift key.
    static const unsigned LeftWindows = VK_LWIN; // The left Windows key.
    static const unsigned RightWindows = VK_RWIN; // The right Windows key.
    static const unsigned PageDown = VK_NEXT; // The Page Down key.
    static const unsigned PageUp = VK_PRIOR; // The Page Up key.

    // Arrows

    static const unsigned Left = VK_LEFT; // The Left Arrow key.
    static const unsigned Up = VK_UP; // The Up Arrow key.
    static const unsigned Down = VK_DOWN; // The Down Arrow key.
    static const unsigned Right = VK_RIGHT; // The Right Arrow key.

    // Extra

    static const unsigned XButton1 = VK_XBUTTON1; // An additional "extended" device key or button (for example; an additional mouse button).
    static const unsigned XButton2 = VK_XBUTTON2; // An additional "extended" device key or button (for example; an additional mouse button).

    // Wtf

    static const unsigned Hangul = VK_HANGUL; // The Hangul symbol key-shift button.
    static const unsigned Hanja = VK_HANJA; // The Hanja symbol key shift button.
    static const unsigned Junja = VK_JUNJA; // The Junja symbol key-shift button.
    static const unsigned Kana = VK_KANA; // The Kana symbol key-shift button.
    static const unsigned Kanji = VK_KANJI; // The Kanji symbol key-shift button.

  };

  struct VirtualKeyModifiers
  {
    static const unsigned None = 0 << 0; // No virtual key modifier.
    static const unsigned Control = 1 << 0; // The Ctrl (control) virtual key.
    static const unsigned Shift = 1 << 2; // The Shift virtual key.
    static const unsigned Menu = 1 << 1; // The Menu virtual key.
    static const unsigned Windows = 1 << 3; // The Windows virtual key.
  };

  struct Message
  {
    static const unsigned Other = 0;
    static const unsigned MouseMove = WM_MOUSEMOVE;
    static const unsigned LeftButtonPressed = WM_LBUTTONDOWN;
    static const unsigned LeftButtonReleased = WM_LBUTTONUP;
    static const unsigned RightButtonPressed = WM_RBUTTONDOWN;
    static const unsigned RightButtonReleased = WM_RBUTTONUP;
    static const unsigned MiddleButtonPressed = WM_MBUTTONDOWN;
    static const unsigned MiddleButtonReleased = WM_MBUTTONUP;
    static const unsigned MouseWheel = WM_MOUSEWHEEL;
    static const unsigned MouseHWheel = WM_MOUSEHWHEEL;
    static const unsigned KeyPressed = WM_KEYDOWN;
    static const unsigned KeyReleased = WM_KEYUP;
    static const unsigned Destroy = WM_DESTROY;
    static const unsigned Close = WM_CLOSE;
    static const unsigned Quit = WM_QUIT;
    static const unsigned Size = WM_SIZE;
    static const unsigned Paint = WM_PAINT;
    static const unsigned Timer = WM_TIMER;
    static const unsigned Gesture = WM_USER + 1;
    static const unsigned HandLost = WM_USER + 2;
    static const unsigned HandCaught = WM_USER + 3;
    static const unsigned UserChanged = WM_USERCHANGED;
    static const unsigned ExitSizeMove = WM_EXITSIZEMOVE;
  };

  struct CorePhysicalKeyStatus
  {
    bool wasKeyDown; // Whether a key is currently pressed down.
    bool isKeyReleased; // Whether a key has moved from a pressed to a released status.
    bool isMenuKeyDown; // Whether the menu key is currently pressed down.
    bool isExtendedKey; // Whether the key that was pressed maps to an extended ASCII character.
    unsigned repeatCount; // The number of times a key was pressed.
    unsigned scanCode; // The scan code for a key that was pressed.

    CorePhysicalKeyStatus()
    {
      wasKeyDown = false; // Whether a key is currently pressed down.
      isKeyReleased = true; // Whether a key has moved from a pressed to a released status.
      isMenuKeyDown = false; // Whether the menu key is currently pressed down.
      isExtendedKey = false; // Whether the key that was pressed maps to an extended ASCII character.
      repeatCount = 1u; // The number of times a key was pressed.
      scanCode = 0u; // The scan code for a key that was pressed.
    }

    static inline bool extractWasKeyDown(_In_ LPARAM v) { return (v & 0x02000000) != 0; } // bits=[29-29]
    static inline bool extractIsKeyReleased(_In_ LPARAM v) { return (v & 0x04000000) != 0; } // bits=[30-30]
    static inline bool extractIsMenuKeyDown(_In_ LPARAM v) { return (v & 0x08000000) != 0; } // bits=[31-31]
    static inline bool extractIsExtendedKey(_In_ LPARAM v) { return (v & 0x00100000) != 0; } // bits=[24-24] + reserved=[25-28]
    static inline unsigned extractRepeatCount(_In_ LPARAM v) { return (v & 0x0000ffff) != 0; } // bits=[00-15]
    static inline unsigned extractScanCode(_In_ LPARAM v) { return (v & 0x00ff0000) != 0; } // bits=[16-23]

    static inline void ExtractSlim(_In_ LPARAM param, _Out_ unsigned &prevState, _Out_ unsigned &count)
    {
      prevState = extractIsKeyReleased(param);
      count = extractRepeatCount(param);
    }    
  };
}