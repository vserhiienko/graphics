#include "DxUtils.h"
#include "DxConverter.h"
#include "NoekkHelpers.h"
#include "DxAssetManager.h"

std::atomic_uint noekk::AssetReader::loadingResourceCount;
std::vector<std::string> noekk::AssetReader::searchPath;

void noekk::AssetReader::addSearchPath(std::string const &path)
{
  std::vector<std::string>::iterator src = searchPath.begin();

  while (src != searchPath.end())
  {
    if (!(*src).compare(path)) return;
    src++;
  }

  searchPath.push_back(path);
}

void noekk::AssetReader::removeSearchPath(std::string const &path)
{
  std::vector<std::string>::iterator src = searchPath.begin();

  while (src != searchPath.end())
  {
    if (!(*src).compare(path))
    {
      searchPath.erase(src);
      return;
    }

    src++;
  }
}


bool noekk::AssetBlob::alloc(size_t sz)
{
  if (data) dealloc();
  data = (unsigned char*)malloc(sz + 1);
  dataLength = sz;
  data[dataLength] = '\0';
  return !!data;
}

void noekk::AssetBlob::dealloc()
{
  if (data) free((void *) data), data = 0;
}

void noekk::AssetBlob::copyTo(AssetBlob &otherAsset) const
{
  otherAsset.dealloc();
  otherAsset.alloc(dataLength);
  memcpy(otherAsset.data, data, dataLength);
}


void noekk::AssetReader::findAndReadTo(std::wstring const &filePath16, unsigned assetId, noekk::AssetReadingCompletionHook *assetHook)
{
  if (filePath16.size())
  {
    std::string filePath;
    DxConverter::string16To8(filePath, filePath16);
    loadAsset(filePath, assetId, assetHook);
  }
}


void noekk::AssetReader::loadAsset(std::string const &filePath, unsigned assetId, noekk::AssetReadingCompletionHook *assetHook)
{
  if (assetHook)
  {
    FILE *fp = NULL;

    // loop N times up the hierarchy, testing at each level
    std::string upPath;
    std::string fullPath;

    for (int32_t i = 0; i < 10; i++)
    {
      std::vector<std::string>::iterator src = searchPath.begin();
      bool looping = true;

      while (looping)
      {
        fullPath.assign(upPath);  // reset to current upPath.
        if (src != searchPath.end())
        {
          //sprintf_s(fullPath, "%s%s/assets/%s", upPath, *src, filePath);
          fullPath.append(*src);
          fullPath.append("/assets/");
          src++;
        }
        else {
          //sprintf_s(fullPath, "%sassets/%s", upPath, filePath);
          fullPath.append("assets/");
          looping = false;
        }
        fullPath.append(filePath);

#ifdef DEBUG
        fprintf(stderr, "Trying to open %s\n", fullPath.c_str());
#endif
        if ((fopen_s(&fp, fullPath.c_str(), "rb") != 0) || (fp == NULL)) fp = NULL;
        else looping = false;
      }

      if (fp) break;

      upPath.append("../");
    }

    if (fp)
    {
      long length;

      fseek(fp, 0, SEEK_END);
      length = ftell(fp);
      fseek(fp, 0, SEEK_SET);

      noekk::AssetBlob buffer;

      if (buffer.alloc(length))
      {
        fread(buffer.data, 1, length, fp);
      }

      fclose(fp);

#ifdef _DEBUG
      noekk::trace("dx:asset: %s (%s) [%d, 0x%0x] [%.1fKb]", filePath.c_str(), fullPath.c_str(), assetId, assetHook, float(buffer.dataLength) / 1024.f);
#endif

      assetHook->onAssetLoaded(assetId, buffer);
      buffer.dealloc();
      return;
    }
  }

  assetHook->onAssetNotFound(assetId);
}

void noekk::AssetReader::loadAssetAsync(std::wstring const &filePath16, unsigned assetId, noekk::AssetReadingCompletionHook *assetHook)
{
  if (filePath16.size())
  {
    std::string filePath;
    DxConverter::string16To8(filePath, filePath16);
    loadAssetAsync(filePath, assetId, assetHook);
  }
}

void noekk::AssetReader::loadAssetAsync(std::string const &assetName, unsigned assetId, noekk::AssetReadingCompletionHook *assetHook)
{
  (concurrency::create_task([&]()
  {
    loadingResourceCount++;
    loadAsset(assetName, assetId, assetHook);
    loadingResourceCount--;
    return;
  }));
}

