#pragma once

#include "pch.h"
namespace nyx
{
  class NvShaderUtils
  {
  public:
    static std::string loadShaderSourceWithUniformTag(
      const char* uniformsFile, 
      const char* srcFile
      );
  };

}