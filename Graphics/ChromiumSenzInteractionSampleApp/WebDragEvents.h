#pragma once
#include "pch.h"

namespace nyx
{
  class DragEvents
  {
  public:
    virtual CefBrowserHost::DragOperationsMask OnDragEnter(CefRefPtr<CefDragData> drag_data, CefMouseEvent ev, CefBrowserHost::DragOperationsMask effect) = 0;
    virtual CefBrowserHost::DragOperationsMask OnDragOver(CefMouseEvent ev, CefBrowserHost::DragOperationsMask effect) = 0;
    virtual CefBrowserHost::DragOperationsMask OnDrop(CefMouseEvent ev, CefBrowserHost::DragOperationsMask effect) = 0;
    virtual void OnDragLeave() = 0;

  };

  class DropTarget : public IDropTarget {
  public:
    static CComPtr<DropTarget> Create(DragEvents* callback, HWND hWnd);

    CefBrowserHost::DragOperationsMask StartDragging(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDragData> drag_data, CefRenderHandler::DragOperationsMask allowed_ops, int x, int y);

    // IDropTarget implementation:
    HRESULT __stdcall DragEnter(IDataObject* data_object, DWORD key_state, POINTL cursor_position, DWORD* effect);
    HRESULT __stdcall DragOver(DWORD key_state, POINTL cursor_position, DWORD* effect);
    HRESULT __stdcall Drop(IDataObject* data_object, DWORD key_state, POINTL cursor_position, DWORD* effect);
    HRESULT __stdcall DragLeave();

    HRESULT __stdcall QueryInterface(const IID& iid, void** object);
    ULONG __stdcall AddRef();
    ULONG __stdcall Release();

  protected:
    explicit DropTarget(DragEvents* callback, HWND hWnd) :
      ref_count_(0),
      callback_(callback),
      hWnd_(hWnd) {}
    virtual ~DropTarget() {}

  protected:
    ULONG ref_count_;

  private:
    CefRefPtr<CefDragData> current_drag_data_;
    DragEvents* callback_;
    HWND hWnd_;

  };

  class DragEnumFormatEtc : public IEnumFORMATETC {
  public:
    static HRESULT CreateEnumFormatEtc(UINT cfmt, FORMATETC* afmt, IEnumFORMATETC** ppEnumFormatEtc);

    // IEnumFormatEtc members
    HRESULT __stdcall Next(ULONG celt, FORMATETC * pFormatEtc, ULONG * pceltFetched);
    HRESULT __stdcall Skip(ULONG celt);
    HRESULT __stdcall Reset(void);
    HRESULT __stdcall Clone(IEnumFORMATETC ** ppEnumFormatEtc);

    // Construction / Destruction
    DragEnumFormatEtc(FORMATETC *pFormatEtc, int nNumFormats);
    ~DragEnumFormatEtc();

    static void DeepCopyFormatEtc(FORMATETC *dest, FORMATETC *source);
    HRESULT __stdcall QueryInterface(const IID& iid, void** object);
    ULONG __stdcall AddRef();
    ULONG __stdcall Release();
  protected:
    ULONG ref_count_;

  private:
    ULONG m_nIndex;  // current enumerator index
    ULONG m_nNumFormats;  // number of FORMATETC members
    FORMATETC* m_pFormatEtc;  // array of FORMATETC objects
  };

  class DropSource : public IDropSource
  {
  public:
    static CComPtr<DropSource> Create();

    // IDropSource implementation:
    HRESULT __stdcall GiveFeedback(DWORD dwEffect);
    HRESULT __stdcall QueryContinueDrag(BOOL fEscapePressed, DWORD grfKeyState);
    HRESULT __stdcall QueryInterface(const IID& iid, void** object);
    ULONG __stdcall AddRef();
    ULONG __stdcall Release();

  protected:
    explicit DropSource() : ref_count_(0) {}
    virtual ~DropSource() {}
    ULONG ref_count_;
  };

  class DataObject : public IDataObject {
  public:
    static CComPtr<DataObject> Create(FORMATETC* fmtetc, STGMEDIUM* stgmed, int count);

    // IDataObject memberS
    HRESULT __stdcall GetDataHere(FORMATETC* pFormatEtc, STGMEDIUM *pmedium);
    HRESULT __stdcall QueryGetData(FORMATETC* pFormatEtc);
    HRESULT __stdcall GetCanonicalFormatEtc(FORMATETC* pFormatEct, FORMATETC* pFormatEtcOut);
    HRESULT __stdcall SetData(FORMATETC* pFormatEtc, STGMEDIUM* pMedium, BOOL fRelease);
    HRESULT __stdcall DAdvise(FORMATETC* pFormatEtc, DWORD advf, IAdviseSink*, DWORD*);
    HRESULT __stdcall DUnadvise(DWORD dwConnection);
    HRESULT __stdcall EnumDAdvise(IEnumSTATDATA **ppEnumAdvise);
    HRESULT __stdcall EnumFormatEtc(DWORD dwDirection, IEnumFORMATETC **ppEnumFormatEtc);
    HRESULT __stdcall GetData(FORMATETC *pFormatEtc, STGMEDIUM *pMedium);
    HRESULT __stdcall QueryInterface(const IID& iid, void** object);
    ULONG __stdcall AddRef();
    ULONG __stdcall Release();

  protected:
    int m_nNumFormats;
    FORMATETC* m_pFormatEtc;
    STGMEDIUM* m_pStgMedium;
    ULONG ref_count_;

    static HGLOBAL DupGlobalMem(HGLOBAL hMem);
    int LookupFormatEtc(FORMATETC *pFormatEtc);
    explicit DataObject(FORMATETC *fmtetc, STGMEDIUM *stgmed, int count);
  };

}