#include "pch.h"
#include "WebClientApp.h"
#include "WebClientRenderer.h"

CefRefPtr<CefPrintHandler> nyx::WebClientApp::GetPrintHandler()  { return NULL; }
CefRefPtr<CefRenderProcessHandler> nyx::WebClientApp::GetRenderProcessHandler()  { return this; }
CefRefPtr<CefBrowserProcessHandler> nyx::WebClientApp::GetBrowserProcessHandler()  { return this; }

nyx::WebClientApp::WebClientApp()
{
  NVWindowsLog("creating web client app");
}

void nyx::WebClientApp::OnRegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar> registrar)
{
  NVWindowsLog("web client app: registering custom schemes");

  // Default schemes that support cookies.
  cookieable_schemes_.push_back("http");
  cookieable_schemes_.push_back("https");

  RegisterCustomSchemes(registrar, cookieable_schemes_);
}

void nyx::WebClientApp::OnContextInitialized()
{
  NVWindowsLog("web client app: context initialized");

  CreateBrowserDelegates(browser_delegates_);

  // Register cookieable schemes with the global cookie manager.
  CefRefPtr<CefCookieManager> manager = CefCookieManager::GetGlobalManager();

  DCHECK(manager.get());
  manager->SetSupportedSchemes(cookieable_schemes_);

  WebClientBrowserDelegate::Set::iterator it = browser_delegates_.begin();
  for (; it != browser_delegates_.end(); ++it) (*it)->OnContextInitialized(this);
}

void nyx::WebClientApp::OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line)
{
  NVWindowsLog("web client app: before child process launched");

  WebClientBrowserDelegate::Set::iterator it = browser_delegates_.begin();
  for (; it != browser_delegates_.end(); ++it) (*it)->OnBeforeChildProcessLaunch(this, command_line);
}

void nyx::WebClientApp::OnRenderProcessThreadCreated(CefRefPtr<CefListValue> extra_info)
{
  NVWindowsLog("web client app: render process thread created");

  WebClientBrowserDelegate::Set::iterator it = browser_delegates_.begin();
  for (; it != browser_delegates_.end(); ++it) (*it)->OnRenderProcessThreadCreated(this, extra_info);
}

void nyx::WebClientApp::OnRenderThreadCreated(CefRefPtr<CefListValue> extra_info)
{
  NVWindowsLog("web client app: render thread created");

  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end(); ++it) (*it)->OnRenderThreadCreated(this, extra_info);
}

void nyx::WebClientApp::OnWebKitInitialized()
{
  NVWindowsLog("web client app: webkit initialized");
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end(); ++it) (*it)->OnWebKitInitialized(this);
}

void nyx::WebClientApp::OnBrowserCreated(CefRefPtr<CefBrowser> browser)
{
  NVWindowsLog("web client app: browser created");
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end(); ++it) (*it)->OnBrowserCreated(this, browser);
}

void nyx::WebClientApp::OnBrowserDestroyed(CefRefPtr<CefBrowser> browser)
{
  NVWindowsLog("web client app: browser destroyed");
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end(); ++it) (*it)->OnBrowserDestroyed(this, browser);
}

CefRefPtr<CefLoadHandler> nyx::WebClientApp::GetLoadHandler()
{
  CefRefPtr<CefLoadHandler> load_handler;
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end() && !load_handler.get(); ++it)
    load_handler = (*it)->GetLoadHandler(this);
  return load_handler;
}

bool nyx::WebClientApp::OnBeforeNavigation(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, NavigationType navigation_type, bool is_redirect)
{
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end(); ++it)
  {
    if ((*it)->OnBeforeNavigation(this, browser, frame, request, navigation_type, is_redirect))
    {
      return true;
    }
  }

  return false;
}

void nyx::WebClientApp::OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context)
{
  NVWindowsLog("web client app: context created");
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end(); ++it) (*it)->OnContextCreated(this, browser, frame, context);
}

void nyx::WebClientApp::OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context)
{
  NVWindowsLog("web client app: context released");
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end(); ++it) (*it)->OnContextReleased(this, browser, frame, context);
}

void nyx::WebClientApp::OnUncaughtException(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context, CefRefPtr<CefV8Exception> exception, CefRefPtr<CefV8StackTrace> stackTrace)
{
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end(); ++it) (*it)->OnUncaughtException(this, browser, frame, context, exception, stackTrace);
}

void nyx::WebClientApp::OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefDOMNode> node)
{
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end(); ++it) (*it)->OnFocusedNodeChanged(this, browser, frame, node);
}

bool nyx::WebClientApp::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message)
{
  DCHECK_EQ(source_process, PID_BROWSER);

  bool handled = false;
  WebClientRendererDelegate::Set::iterator it = render_delegates_.begin();
  for (; it != render_delegates_.end() && !handled; ++it)
    handled = (*it)->OnProcessMessageReceived(this, browser, source_process, message);
  return handled;
}


/*static*/ void nyx::WebClientApp::CreateBrowserDelegates(
  nyx::WebClientBrowserDelegate::Set& delegates
  )
{
}

/*static*/ void nyx::WebClientApp::CreateRenderDelegates(
  nyx::WebClientRendererDelegate::Set& delegates
  ) 
{
  client_renderer::CreateRenderDelegates(delegates);
  //dom_test::CreateRenderDelegates(delegates);
  //performance_test::CreateRenderDelegates(delegates);
}

// static
void nyx::WebClientApp::RegisterCustomSchemes(
  CefRefPtr<CefSchemeRegistrar> registrar,
  std::vector<CefString>& cookiable_schemes
  ) 
{
  //scheme_test::RegisterCustomSchemes(registrar, cookiable_schemes);
}
