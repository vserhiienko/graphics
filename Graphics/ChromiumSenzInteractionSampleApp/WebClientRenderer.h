#pragma once
#include "pch.h"
#include "WebTexture.h"
#include "WebClientAppDelegates.h"

namespace nyx
{
  class WebRenderer
  {
  public:
    WebRenderer(bool transparent);
    ~WebRenderer();

  public:
    void initialize();
    void cleanup();
    void render();

  public: // Forwarded from CefRenderHandler callbacks.
    void onPopupShow(CefRefPtr<CefBrowser> browser, bool show);
    void onPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect);
    void onPaint(CefRefPtr<CefBrowser> browser, CefRenderHandler::PaintElementType type, const CefRenderHandler::RectList& dirtyRects, const void* buffer, int width, int height);

  public:
    void setSpin(float, float);
    void incrementSpin(float, float);
    bool isTransparent();
    void setIsTransparent(bool);
    int getViewWidth();
    int getViewHeight();
    const CefRect& popup_rect() const;
    const CefRect& original_popup_rect() const;
    CefRect getPopupRectInWebView(const CefRect& original_rect);
    void clearPopupRects();

  public:
    inline Texture &getTexture() { return m_texture; }
    inline Texture const &getTexture() const { return m_texture; }

  public:
    float m_spin_x;
    float m_spin_y;
    int m_view_width;
    int m_view_height;
    bool m_transparent;
    bool m_initialized;

    Texture m_texture;
    CefRect m_popup_rect;
    CefRect m_original_popup_rect;

  };
}

namespace client_renderer
{
  extern const char kFocusedNodeChangedMessage[];                           // Message sent when the focused node changes.
  void CreateRenderDelegates(nyx::WebClientRendererDelegate::Set& delegates);    // Create the render delegate.
}

namespace nyx
{
  using namespace client_renderer;
}