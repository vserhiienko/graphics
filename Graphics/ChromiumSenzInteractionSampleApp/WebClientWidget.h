#pragma once
#include "pch.h"

#include "WebDragEvents.h"
#include "WebClientHandler.h"
#include "WebClientRenderer.h"
#include "WebBrowserProvider.h"



namespace nyx
{
  //class WebClientApp;
  class WebClientWidget
    : public WebClientHandler::RenderHandler
    , public DragEvents
  {
  public: // WebClientHandler::RenderHandler methods
    virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser) override;

  public: // CefRenderHandler methods
    virtual bool GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override;
    virtual bool GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override;
    virtual bool GetScreenPoint(CefRefPtr<CefBrowser> browser, int viewX, int viewY, int& screenX, int& screenY) override;
    virtual bool GetScreenInfo(CefRefPtr<CefBrowser> browser, CefScreenInfo& screen_info) override;
    virtual void OnPopupShow(CefRefPtr<CefBrowser> browser, bool show) override;
    virtual void OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect) override;
    virtual void OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height) override;
    virtual void OnCursorChange(CefRefPtr<CefBrowser> browser, CefCursorHandle cursor) override;
    virtual bool StartDragging(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDragData> drag_data, CefRenderHandler::DragOperationsMask allowed_ops, int x, int y) override;
    virtual void UpdateDragCursor(CefRefPtr<CefBrowser> browser, CefRenderHandler::DragOperation operation) override;

  public: // CefRenderHandler methods
    virtual CefBrowserHost::DragOperationsMask OnDragEnter(CefRefPtr<CefDragData> drag_data, CefMouseEvent ev, CefBrowserHost::DragOperationsMask effect) override;
    virtual CefBrowserHost::DragOperationsMask OnDragOver(CefMouseEvent ev, CefBrowserHost::DragOperationsMask effect) override;
    virtual CefBrowserHost::DragOperationsMask OnDrop(CefMouseEvent ev, CefBrowserHost::DragOperationsMask effect) override;
    virtual void OnDragLeave() override;

  public:
    WebClientWidget(WebClientWidgetBrowserProvider *browserProvider, bool transparent);
    virtual ~WebClientWidget();

    void render();
    void setWindowHandle(HWND windowHandle);
    HWND getWindowHandle();
    int &getWidth();
    int &getHeight();
    int getWidth() const;
    int getHeight() const;

  private:

    bool painting_popup_;
    bool render_task_pending_;

    HWND m_windowHandle;

    int m_width;
    int m_height;
    WebRenderer m_renderer;
    CComPtr<DropTarget> drop_target_;
    CefRenderHandler::DragOperation current_drag_op_;
    WebClientWidgetBrowserProvider *m_browserProvider;

    IMPLEMENT_REFCOUNTING(WebClientWidget);

  public:
    WebRenderer &getRenderer() { return m_renderer; }
    WebRenderer const &getRenderer() const { return m_renderer; }

  };
}