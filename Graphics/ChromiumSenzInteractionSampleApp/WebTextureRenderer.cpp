#include "pch.h"
#include "WebTextureRenderer.h"


void nyx::TextureRenderer::init()
{
  blitProg = NvGLSLProgram::createFromFiles(
    "shaders/plain.vert",
    "shaders/plain.frag"
    );

  positionAttr = blitProg->getAttribLocation("aPosition");
  texCoordsAttr = blitProg->getAttribLocation("aTexCoord");
  sourceTexLocation = blitProg->getUniformLocation("uSourceTex");
}

void nyx::TextureRenderer::cleanup()
{
  delete blitProg;
  blitProg = NULL;
}

void nyx::TextureRenderer::render(Texture const &resource)
{
  if (resource.textureId)
  {
    float const aspect = resource.viewWidth / resource.viewHeight;

    float const vertexPosition[] =
    {
      +aspect, +1.0f,
      -aspect, +1.0f,
      +aspect, -1.0f,
      -aspect, -1.0f,
    };

    float const textureCoords[] =
    {
      1.0f, 0.0f,
      0.0f, 0.0f,
      1.0f, 1.0f,
      0.0f, 1.0f
    };

    glUseProgram(blitProg->getProgram());
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, resource.textureId);

    glUniform1i(sourceTexLocation, 0);
    glVertexAttribPointer(positionAttr, 2, GL_FLOAT, GL_FALSE, 0, vertexPosition);
    glVertexAttribPointer(texCoordsAttr, 2, GL_FLOAT, GL_FALSE, 0, textureCoords);
    glEnableVertexAttribArray(positionAttr);
    glEnableVertexAttribArray(texCoordsAttr);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    CHECK_GL_ERROR();
  }
}