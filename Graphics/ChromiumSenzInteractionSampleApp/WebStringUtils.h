#pragma once
#include "pch.h"

namespace nyx
{
  void DumpRequestContents(CefRefPtr<CefRequest> request, std::string& str);
  std::string StringReplace(const std::string& in, const std::string& what, const std::string& to);
}