#include "pch.h"
#include "NuiFeed.h"

class nyx::NuiFeedPipeline : public UtilPipeline
{
  static const unsigned confidenceThreshold = 80;
  nyx::NuiFeed *feed;

public:
  NuiFeedPipeline(nyx::NuiFeed *feed)
    : feed(feed)
  {
    assert(feed != NULL);
  }
  virtual void PXCAPI OnGesture(PXCGesture::Gesture *data)
  {
    auto gestureLambda = [data](INuiGestureNotify *notify)
    {
      notify->onGestureReceived(nyx::NuiGesture(data->label), (bool)data->active, data->confidence);
    };

    if (data->confidence >= confidenceThreshold)
    {
      switch (data->label)
      {
      case PXCGesture::Gesture::LABEL_NAV_SWIPE_LEFT:
      case PXCGesture::Gesture::LABEL_NAV_SWIPE_RIGHT:
      case PXCGesture::Gesture::LABEL_NAV_SWIPE_UP:
      case PXCGesture::Gesture::LABEL_NAV_SWIPE_DOWN:
      case PXCGesture::Gesture::LABEL_HAND_CIRCLE:
      case PXCGesture::Gesture::LABEL_HAND_WAVE:
        std::for_each(
          feed->gestureCallbacks.begin(),
          feed->gestureCallbacks.end(),
          gestureLambda
          );
        break;
      case PXCGesture::Gesture::LABEL_POSE_THUMB_DOWN:
      case PXCGesture::Gesture::LABEL_POSE_THUMB_UP:
      case PXCGesture::Gesture::LABEL_POSE_PEACE:
      case PXCGesture::Gesture::LABEL_POSE_BIG5:
        //if (data->active) 
        std::for_each(
          feed->gestureCallbacks.begin(),
          feed->gestureCallbacks.end(),
          gestureLambda
          );
        break;
      };
    }
    else
    {
      NVWindowsLog("nui feed pipeline: gesture was filtered due to low confidence");
    }
  }
};

class nyx::NuiFeedGestureProc
{
public:

  static bool failed(pxcStatus status)
  {
    return status < PXC_STATUS_NO_ERROR;
  }
  static void onHandCaught(nyx::NuiFeed &feed)
  {
    static auto handEventNotifier = [](INuiHandNotify *notify)
    {
      notify->onHandCaught();
    };

    std::for_each(
      feed.handCallbacks.begin(),
      feed.handCallbacks.end(),
      handEventNotifier
      );
  }
  static void onHandLost(nyx::NuiFeed &feed)
  {
    static auto handEventNotifier = [](INuiHandNotify *notify)
    {
      notify->onHandLost();
    };

    std::for_each(
      feed.handCallbacks.begin(),
      feed.handCallbacks.end(),
      handEventNotifier
      );
  }
  static void handleGesture(nyx::NuiFeed &feed, PXCGesture *gesture)
  {
    static bool handIsVisible[2] = { false, false };

    if (gesture)
    {
      auto sts = PXC_STATUS_NO_ERROR;

      if (failed(sts = gesture->QueryNodeData(0, PXCGesture::GeoNode::LABEL_BODY_HAND_PRIMARY, &feed.frameDetails.nodes[0][0])))
        sts = gesture->QueryNodeData(0, PXCGesture::GeoNode::LABEL_BODY_HAND_SECONDARY, &feed.frameDetails.nodes[0][0]);

      if (failed(sts))
      {
        if (handIsVisible[0])
        {
          handIsVisible[0] = false;
          onHandLost(feed);
        }
      }
      else
      {
        if (!handIsVisible[0])
        {
          handIsVisible[0] = true;
          onHandCaught(feed);
        }
      }
    }
  }

  bool IsPressed[2];//! per-frame updated
  bool IsActive[2]; //! per-frame updated
  bool ShouldSlow[2]; //! per-frame updated
  bool IsDragging[2]; //! per-frame updated
  bool IsVisible[2]; //! user-defined
  bool IsHoverable[2]; //! user-defined
  unsigned HandsToProcess = 1u; //! user-defined (has defaults)
  unsigned NodesToProcess = 1u; //! user-defined (has defaults)
  float AdjustingFactor = 0.6f; //! user-defined (has defaults)
  nv::vec2f ScreenSize; //! user-defined (has defaults)
  nv::vec2f DepthBufferDimensions; //! user-defined (has defaults)
  nv::vec2f LastActualPosition[2]; //! per-frame updated
  nv::vec2f LastRelativePosition[2]; //! per-frame updated
  nv::vec2f LastAdjustedActualPosition[2]; //! per-frame updated
  nv::vec2f LastAdjustedRelativePosition[2]; //! per-frame updated
  nv::vec2f SmoothingPositions[2][16];
  unsigned SmoothingIndices[2];
  float IsPressedConfidence[2];

public:

  NuiFeedGestureProc()
  {
    NVWindowsLog("creating nui feed gesture proc");

    SmoothingIndices[0] = 0;
    SmoothingIndices[1] = 0;
    IsPressedConfidence[0] = 0.0f;
    IsPressedConfidence[1] = 0.0f;

    IsPressed[0] = false;
    IsPressed[1] = false;
    IsActive[0] = false;
    IsActive[1] = false;
    IsVisible[0] = true;
    IsVisible[1] = false;
    IsDragging[0] = false;
    IsDragging[1] = false;
    IsHoverable[0] = true;
    IsHoverable[1] = false;
    LastActualPosition[0].x = 0.0f;
    LastActualPosition[1].y = 0.0f;
    LastRelativePosition[0].x = 0.0f;
    LastRelativePosition[1].y = 0.0f;
    LastAdjustedRelativePosition[0].x = 0.0f;
    LastAdjustedRelativePosition[1].y = 0.0f;
    DepthBufferDimensions.x = 320.0f;
    DepthBufferDimensions.y = 240.0f;
  }

  void processFeedFrameTraits(nyx::NuiFeed &feed)
  {
    IsActive[0] = false;
    IsActive[1] = false;
    IsDragging[0] = false;
    IsDragging[1] = false;

    auto ri = &feed.frameDetails;

    for (UINT32 hand_index = 0; hand_index < HandsToProcess; hand_index++)
    for (UINT32 node_index = 0; node_index < NodesToProcess; node_index++)
    if ((ri->nodes[hand_index][node_index].body == PXCGesture::GeoNode::LABEL_BODY_HAND_PRIMARY) ||
      (ri->nodes[hand_index][node_index].body == PXCGesture::GeoNode::LABEL_BODY_HAND_SECONDARY) ||
      (ri->nodes[hand_index][node_index].confidence >= 98.0f))
    {
      IsActive[hand_index]
        = true;
      IsDragging[hand_index]
        = ri->nodes[hand_index][node_index].openness <= 10;
      ShouldSlow[hand_index]
        = ri->nodes[hand_index][node_index].openness <= 60
        && ri->nodes[hand_index][node_index].openness > 10;

      IsPressedConfidence[hand_index] += IsDragging[hand_index] ? 0.1f : -0.1f;
      IsPressedConfidence[hand_index] = std::min(std::max(0.0f, IsPressedConfidence[hand_index]), 1.0f);
      IsDragging[hand_index] = IsPressedConfidence[hand_index] > 0.8f;

      if (IsHoverable[hand_index])
      {
        nv::vec2f depth_buffer_position;
        depth_buffer_position.x = (DepthBufferDimensions.x - ri->nodes[hand_index][node_index].positionImage.x);
        depth_buffer_position.y = ri->nodes[hand_index][node_index].positionImage.y;

        nv::vec2f actual_position;
        actual_position.x = ScreenSize.x * depth_buffer_position.x / DepthBufferDimensions.x;
        actual_position.y = ScreenSize.y * depth_buffer_position.y / DepthBufferDimensions.y;
        LastActualPosition[hand_index] = actual_position;

        nv::vec2f relative_position;
        relative_position.x = actual_position.x / ScreenSize.x; // [0 1]
        relative_position.y = actual_position.y / ScreenSize.y; // [0 1]
        relative_position.x -= 0.5f; relative_position.x *= 2.0f; // [-1 1]
        relative_position.y -= 0.5f; relative_position.y *= 2.0f; // -[1 1]

        nv::vec2f relative_position_adj;
        relative_position_adj = relative_position / AdjustingFactor;
        auto diffRelAdj = relative_position_adj - LastRelativePosition[hand_index];
        //if (ShouldSlow[hand_index] && nv::length(diffRelAdj) < 0.1f) continue;
        //else if (IsDragging[hand_index] && nv::length(diffRelAdj) < 0.2f) continue;
        //else if (!IsDragging[hand_index] && nv::length(diffRelAdj) < 0.005f) continue;

        SmoothingPositions[hand_index][SmoothingIndices[hand_index]] = relative_position_adj;
        LastRelativePosition[hand_index] = relative_position_adj;

        static const auto smoothing_size = ARRAYSIZE(SmoothingPositions[hand_index]);

        nv::vec2f smoothed_relative_position_adj;
        for (uint32_t i = 0; i < smoothing_size; i++)
          smoothed_relative_position_adj += SmoothingPositions[hand_index][i];
        smoothed_relative_position_adj /= smoothing_size;

        nv::vec2f actual_position_adj;
        actual_position_adj = smoothed_relative_position_adj * 0.5f;
        actual_position_adj += nv::vec2f(0.5f);
        actual_position_adj *= nv::vec2f(ScreenSize.x, ScreenSize.y);

        SmoothingIndices[hand_index] = (SmoothingIndices[hand_index] + 1) % smoothing_size;

        LastAdjustedActualPosition[hand_index] = actual_position_adj;
        LastAdjustedRelativePosition[hand_index] = smoothed_relative_position_adj;
      }

      break;
    }

    LastActualPosition[0] = LastAdjustedRelativePosition[0];
    LastActualPosition[0] /= AdjustingFactor;
    LastActualPosition[0] /= 2.0f;
    LastActualPosition[0] += 0.50f;
    LastActualPosition[0] *= ScreenSize;

    for (UINT32 hand_index = 0; hand_index < HandsToProcess; hand_index++)
    if (IsHoverable[hand_index])
    {
      auto dragCallback = [&](INuiHandNotify *notify)
      {
        notify->onHandStateChanged(
          hand_index, true,
          LastActualPosition[0].x,
          LastActualPosition[0].y
          );
      };

      auto visibleCallback = [&](INuiHandNotify *notify)
      {
        notify->onHandStateChanged(
          hand_index, false,
          LastActualPosition[0].x,
          LastActualPosition[0].y
          );
      };

      if (IsActive[hand_index] && IsDragging[hand_index])
      {
        //NVWindowsLog("nui feed gesture proc: pressed");
        std::for_each(
          feed.handCallbacks.begin(),
          feed.handCallbacks.end(),
          dragCallback
          //visibleCallback
          );
      }
      else if (IsActive[hand_index])
      {
        std::for_each(
          feed.handCallbacks.begin(),
          feed.handCallbacks.end(),
          visibleCallback
          );
      }

      /*else if (IsActive[hand_index] && IsPressed[hand_index])
      {
      NVWindowsLog("nui feed gesture proc: released");
      IsPressed[hand_index] = false;

      std::for_each(
      feed.handCallbacks.begin(),
      feed.handCallbacks.end(),
      visibleCallback
      );
      }*/
    }
  }
};

nyx::NuiFeed::NuiFeed()
{
  NVWindowsLog("creating nui feed");
  pipeline = NULL;
  isValid = false;
  awaitFrame = false;
}

nyx::NuiFeed::~NuiFeed()
{
  NVWindowsLog("destroying nui feed");
  cleanup();
}

void nyx::NuiFeed::init()
{
  if (pipeline == NULL)
  {
    
    NVWindowsLog("nui feed: initializing pipeline");
    pipeline = new NuiFeedPipeline(this);
    //pipeline->EnableImage(PXCImage::COLOR_FORMAT_RGB24, 640, 480);
    pipeline->EnableGesture();
    //pipeline->EnableEmotion();

    isValid = pipeline->Init();

    if (!isValid)
    {
      NVWindowsLog("nui feed: failed to initialize pipeline");
      cleanup();
      return;
    }

    if (gestureProc == NULL && isValid)
    {
      gestureProc = new NuiFeedGestureProc;
    }
  }
}

void nyx::NuiFeed::setScreenSize(int x, int y)
{
  if (pipeline)
  {
    if (gestureProc == NULL)
    {
      gestureProc = new NuiFeedGestureProc;
    }

    gestureProc->ScreenSize.x = (float)x;
    gestureProc->ScreenSize.y = (float)y;
  }
}

void nyx::NuiFeed::cleanup()
{
  if (pipeline)
  {
    NVWindowsLog("nui feed: cleanup");
    pipeline->Close();
    pipeline->Release();
    pipeline = NULL;
  }

  if (gestureProc)
  {
    delete gestureProc;
    gestureProc = NULL;
  }
}

void nyx::NuiFeed::addCallback(INuiNotify *notify)
{
  if (auto handCallback = dynamic_cast<INuiHandNotify*>(notify)) handCallbacks.push_back(handCallback);
  if (auto gestureCallback = dynamic_cast<INuiGestureNotify*>(notify)) gestureCallbacks.push_back(gestureCallback);
}

void nyx::NuiFeed::pollEvents()
{
#ifdef NDEBUG
  if ((pipeline == NULL) || (pipeline &&pipeline->IsDisconnected()))
  {
    cleanup();

    NVWindowsLog("nui feed: external device is offline");
    NVWindowsLog("nui feed: please check device connection state");
    NVWindowsLog("nui feed: restarting in 15 seconds");
    //Sleep(15000);

    NVWindowsLog("nui feed: restarting...");
    init();
  }
#endif 

  if (pipeline && pipeline->AcquireFrame(awaitFrame))
  {
    NuiFeedGestureProc::handleGesture((*this), pipeline->QueryGesture());
    // ...

    if (gestureProc) gestureProc->processFeedFrameTraits((*this));
    pipeline->ReleaseFrame();
  }
}


nyx::NuiFeedVisuals::NuiFeedVisuals(void)
{
  feed = NULL;
}

void nyx::NuiFeedVisuals::setFeed(NuiFeed *f)
{
  feed = f;
}

nyx::NuiFeed *nyx::NuiFeedVisuals::getFeed(void)
{
  return feed;
}


void nyx::NuiFeedVisuals::drawCircle(
  float cx, float cy,
  float radius,
  int num_segments,
  float r,
  float b,
  float g,
  float a,
  float lineWidth
  )
{
  radius /= std::min(feed->gestureProc->ScreenSize.x, feed->gestureProc->ScreenSize.y);
  float aspect = feed->gestureProc->ScreenSize.x / feed->gestureProc->ScreenSize.y;

  cy *= -1.0f;
  cx -= feed->gestureProc->ScreenSize.x * 0.5f;
  cy += feed->gestureProc->ScreenSize.y * 0.5f;
  cx /= feed->gestureProc->ScreenSize.x * 0.5f;
  cy /= feed->gestureProc->ScreenSize.y * 0.5f;

  float theta = 2 * 3.1415926 / float(num_segments);

  //precalculate the sine and cosine
  float c = cosf(theta);
  float s = sinf(theta);
  float t;

  //we start at angle = 0 
  float x = radius;
  float y = 0;

  glLineWidth(lineWidth);
  glBegin(GL_LINE_LOOP);
  glColor4f(r, g, b, a);
  for (int ii = 0; ii < num_segments; ii++)
  {
    //output vertex 
    glVertex2f(x + cx, y * aspect + cy);

    //apply the rotation matrix
    t = x;
    x = c * x - s * y;
    y = s * t + c * y;
  }
  glEnd();
}

void nyx::NuiFeedVisuals::drawHands(void)
{
  if (feed && feed->pipeline)
  {
    int 
      sc = 3;
    float 
      r = 1.0f, g = 1.0f, b = 1.0f, a = 1.0f,
      t = 30.0f, s = 50.0f;

    if (feed->gestureProc->IsDragging[0])
    {
      t = 40.0f;
      s = 45.0f;
      s = 3;
    }
    else if (feed->gestureProc->ShouldSlow[0])
    {
      t = 35.0f;
      s = 50.0f;
      sc = 4;
    }
    else
    {
      t = 30.0f;
      s = 55.0f;
      sc = 15;
      sc = 5;
    }

    drawCircle(
      feed->gestureProc->LastActualPosition[0].x, 
      feed->gestureProc->LastActualPosition[0].y, 
      s, 20, r, g, b, a, 12.0f
      );
    drawCircle(
      feed->gestureProc->LastActualPosition[0].x,
      feed->gestureProc->LastActualPosition[0].y,
      t, sc, r, g, b, a, 6.0f
      );
  }
}