#include "pch.h"
#include "SampleApp.h"
#include "NvShaderUtils.h"

bool nyx::SampleApp::onMain()
{
    void* sandboxInfo = NULL;
    CefMainArgs mainArgs(GetModuleHandle(NULL));
    m_clientApp = new WebClientApp();
    int exit_code = CefExecuteProcess(mainArgs, m_clientApp, sandboxInfo);
    return exit_code == -1;
}

NvAppBase* NvAppFactory(NvPlatformContext* platform)
{
    //AllocConsole();
    //printf("sample app: process id: %d", GetCurrentProcessId());
    return new nyx::SampleApp(platform);
}

void nyx::SampleApp::configurationCallback(NvEGLConfiguration& config)
{
    config.depthBits = 24;
    config.stencilBits = 0;
    config.apiVer = NvGfxAPIVersionGL4();
}

nyx::SampleApp::SampleApp(NvPlatformContext* platform) : NvSampleApp(platform, "Interactive TV Host")
{
    m_widget = NULL;
    m_clientHandler = NULL;
    m_clientApp = NULL;
    m_clientInitialized = false;
    m_enableBrowsing = false;
    m_wasDragging = false;
    m_ignoreMouseInput = false;


    // Required in all subclasses to avoid silent link issues
    forceLinkHack();
}

nyx::SampleApp::~SampleApp(void)
{
    m_widget = NULL;
    m_clientHandler = NULL;
    m_clientApp = NULL;

    m_nui.cleanup();
    CefShutdown();
}

void nyx::SampleApp::initUI(void)
{
    if (mTweakBar)
    {
        //mTweakBar->addValue("Enable browsing", m_enableBrowsing);
        //mTweakBar->addValue("Ignore mouse input", m_ignoreMouseInput);
        //mTweakBar->syncValues();
    }

    // Change the filtering for the framerate
    mFramerate->setMaxReportRate(.2f);
    mFramerate->setReportFrames(20);
    //getGLContext()->setSwapInterval(0);
}

CefRefPtr<CefBrowser> nyx::SampleApp::GetBrowser(
    )
{
    if (m_clientHandler.get()) return m_clientHandler->getBrowser();
    return NULL;
}

void nyx::SampleApp::initRendering(void)
{
    if (!requireMinAPIVersion(NvGfxAPIVersionGL4_4())) return;
    NvAssetLoaderAddSearchPath("ChromiumSenzInteractionSampleApp");

    m_textureRenderer.init();

    m_nui.init();
    m_nui.addCallback(this);
    m_nuiVisuals.setFeed(&m_nui);
}

void nyx::SampleApp::update(void)
{
    CefDoMessageLoopWork();
    m_nui.pollEvents();
}

void nyx::SampleApp::onHandStateChanged(int hand, bool drag, float x, float y)

{
    NvInputDeviceType::Enum device = NvInputDeviceType::TOUCH;
    NvPointerActionType::Enum actionType = NvPointerActionType::MOTION;
    if (m_wasDragging && !drag) m_wasDragging = false, actionType = NvPointerActionType::UP;
    else if (!m_wasDragging && drag) m_wasDragging = true, actionType = NvPointerActionType::DOWN;

    NvPointerEvent event;
    event.m_id = 1;
    event.m_x = x;
    event.m_y = y;
    handlePointerInput(device, actionType, 0, 1, &event);

    //if (drag) NVWindowsLog("hand#%d is dragging @ (%.2f %.2f)", hand, x, y);
    //else NVWindowsLog("hand#%d is visible @ (%.2f %.2f)", hand, x, y);
}

void nyx::SampleApp::onGestureReceived(NuiGesture gesture, bool active, uint32_t confidence)
{
    static char const *urls[] =
    {
        "http://localhost:9000/",
        "file:///F:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/DraggableDualViewSlideshow/DraggableDualViewSlideshow/index.html",
        "file:///F:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/DraggableDualViewSlideshow/DraggableDualViewSlideshow/index.html",
        "file:///F:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/FlipboardPageLayout/FlipboardPageLayout/index.html",
        "file:///F:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/3DGalleryRoom/3DGalleryRoom/index.html",
        "file:///F:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/HoverEffectIdeas/HoverEffectIdeas/index.html",
        "file:///F:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/PerspectivePageViewNavigation/index.html",
        "http://localhost:9000/",
    };

    static uint32_t nextUrl = 0;
    static uint32_t urlCount = ARRAYSIZE(urls);

    switch (gesture)
    {
    case nyx::NuiGesture_Wave:
        NVWindowsLog("gesture received: wave (%u%%)", confidence);
        handleKeyInput(NvKey::K_W, NvKeyActionType::DOWN);
        handleKeyInput(NvKey::K_W, NvKeyActionType::UP);
        break;
    case nyx::NuiGesture_Circle:
        NVWindowsLog("gesture received: circle (%u%%)", confidence);
        GetBrowser()->GetMainFrame()->LoadURL(urls[(nextUrl += 1) % urlCount]);
        handleKeyInput(NvKey::K_C, NvKeyActionType::DOWN);
        handleKeyInput(NvKey::K_C, NvKeyActionType::UP);
        break;
    case nyx::NuiGesture_Palm:
        NVWindowsLog("gesture received: palm (%3s) (%u%%)", (active ? "on" : "off"), confidence);
        break;
    case nyx::NuiGesture_Peace:
        NVWindowsLog("gesture received: peace (%3s) (%u%%)", (active ? "on" : "off"), confidence);
        break;
    case nyx::NuiGesture_Like:
        NVWindowsLog("gesture received: like (%3s) (%u%%)", (active ? "on" : "off"), confidence);
        break;
    case nyx::NuiGesture_Dislike:
        NVWindowsLog("gesture received: dislike (%3s) (%u%%)", (active ? "on" : "off"), confidence);
        break;
    case nyx::NuiGesture_NavSwipeUp:
        NVWindowsLog("gesture received: nav up (%u%%)", confidence);
        handleKeyInput(NvKey::K_ARROW_DOWN, NvKeyActionType::DOWN);
        handleKeyInput(NvKey::K_ARROW_DOWN, NvKeyActionType::UP);
        break;
    case nyx::NuiGesture_NavSwipeDown:
        NVWindowsLog("gesture received: nav down (%u%%)", confidence);
        handleKeyInput(NvKey::K_ARROW_UP, NvKeyActionType::DOWN);
        handleKeyInput(NvKey::K_ARROW_UP, NvKeyActionType::UP);
        break;
    case nyx::NuiGesture_NavSwipeLeft:
        NVWindowsLog("gesture received: nav left (%u%%)", confidence);
        handleKeyInput(NvKey::K_ARROW_RIGHT, NvKeyActionType::DOWN);
        handleKeyInput(NvKey::K_ARROW_RIGHT, NvKeyActionType::UP);
        break;
    case nyx::NuiGesture_NavSwipeRight:
        NVWindowsLog("gesture received: nav right (%u%%)", confidence);
        handleKeyInput(NvKey::K_ARROW_LEFT, NvKeyActionType::DOWN);
        handleKeyInput(NvKey::K_ARROW_LEFT, NvKeyActionType::UP);
        break;
    default:
        NVWindowsLog("gesture received: unknown");
        break;
    }

}

void nyx::SampleApp::onHandCaught()
{
    NVWindowsLog("hand caught");
}

void nyx::SampleApp::onHandLost()
{
    NVWindowsLog("hand lost");
}

void nyx::SampleApp::draw(void)
{
    glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_textureRenderer.render(m_widget->getRenderer().getTexture());
    m_nuiVisuals.drawHands();
}

void nyx::SampleApp::initClient()
{
    if (m_clientInitialized)
        return;

    // Retrieve the current working directory.
    if (_getcwd(m_workingDirectory, sizeof(m_workingDirectory)) == NULL)
    {
        m_workingDirectory[0] = 0;
        NVWindowsLog("failed to get working directory");
    }
    else
    {
        m_commandLine = CefCommandLine::CreateCommandLine();
        m_commandLine->InitFromString(::GetCommandLine());

        void* sandboxInfo = NULL;

        CefSettings settings;
        settings.no_sandbox = true;
        settings.multi_threaded_message_loop = false;
        settings.windowless_rendering_enabled = true;

        bool result = CefInitialize(CefMainArgs(GetModuleHandle(NULL)), settings, m_clientApp, sandboxInfo);
        if (!result)
        {
            errorExit("failed to initialized cef client");
        }

        bool transparent = false;
        m_clientHandler = new WebClientHandler();

        HWND windowHandle = (HWND)getGLContext()->nativeGetWindowHandle();
        m_widget = new WebClientWidget(this, transparent);
        m_widget->setWindowHandle(windowHandle);
        m_clientHandler->setRenderHandler(m_widget);

        CefWindowInfo info;
        CefBrowserSettings browserSettings;
        browserSettings.file_access_from_file_urls = STATE_ENABLED;
#ifdef NDEBUG
        browserSettings.windowless_frame_rate = 60;
#endif

        info.parent_window = windowHandle;
        info.windowless_rendering_enabled = true;
        info.transparent_painting_enabled = transparent;

        //m_clientHandler->getStartupURL() = "http://localhost:9000/";
        m_clientHandler->getStartupURL() = "file:///F:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/FlipboardPageLayout/FlipboardPageLayout/index.html";
        m_clientHandler->getStartupURL() = "file:///F:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/FlipboardPageLayout/FlipboardPageLayout/index.html";
        CefBrowserHost::CreateBrowser(info, m_clientHandler, m_clientHandler->getStartupURL(), browserSettings, NULL);


        m_enableBrowsing = true;
        m_ignoreMouseInput = true;
        m_clientInitialized = true;
    }

}

void nyx::SampleApp::reshape(
    int32_t width,
    int32_t height
    )
{
    // update projection matrix in camera data
    glViewport(0, 0, (GLint)width, (GLint)height);
    m_nui.setScreenSize(width, height);

    initClient();
    if (m_widget.get())
        m_widget->getWidth() = width,
        m_widget->getHeight() = height;

    CefRefPtr<CefBrowser> browser;
    CefRefPtr<CefBrowserHost> browserHost;

    if (m_enableBrowsing
        && (browser = GetBrowser())
        && (browserHost = browser->GetHost()))
    {
        browserHost->WasResized();
    }

}

bool nyx::SampleApp::handlePointerInput(
    NvInputDeviceType::Enum device,
    NvPointerActionType::Enum action,
    uint32_t modifiers,
    int32_t count,
    NvPointerEvent *points
    )
{
    CefMouseEvent mouseEvent;
    CefRefPtr<CefBrowser> browser;
    CefRefPtr<CefBrowserHost> browserHost;
    CefBrowserHost::MouseButtonType mouseButton;

    if (
        m_enableBrowsing
        && ((m_ignoreMouseInput && device == NvInputDeviceType::TOUCH)
            || (!m_ignoreMouseInput && device == NvInputDeviceType::MOUSE))
        && (browser = GetBrowser()) && (browserHost = browser->GetHost())
        )
    {
        mouseEvent.x = points->m_x;
        mouseEvent.y = points->m_y;

        switch (points->m_id)
        {
        case 1: mouseButton = CefBrowserHost::MouseButtonType::MBT_LEFT; break;
        case 2: mouseButton = CefBrowserHost::MouseButtonType::MBT_RIGHT; break;
        case 4: mouseButton = CefBrowserHost::MouseButtonType::MBT_MIDDLE; break;
        }
        switch (action)
        {
        case NvPointerActionType::MOTION: browserHost->SendMouseMoveEvent(mouseEvent, false); break;
        case NvPointerActionType::UP: browserHost->SendMouseClickEvent(mouseEvent, mouseButton, true, 1); break;
        case NvPointerActionType::DOWN: browserHost->SendMouseClickEvent(mouseEvent, mouseButton, false, 1); break;
        }
    }

    return false;
}

bool nyx::SampleApp::handleKeyInput(
    uint32_t code,
    NvKeyActionType::Enum action
    )
{
    CefRefPtr<CefBrowser> browser;
    CefRefPtr<CefBrowserHost> browserHost;

    if (m_enableBrowsing
        && (browser = GetBrowser())
        && (browserHost = browser->GetHost()))
    {
        CefKeyEvent keyEvent;
        switch (action)
        {
        case NvKeyActionType::UP:
            keyEvent.type = cef_key_event_type_t::KEYEVENT_KEYUP;
            break;
        default:
            //case NvKeyActionType::DOWN:
            keyEvent.type = cef_key_event_type_t::KEYEVENT_KEYDOWN;
            break;
        }

        switch (code)
        {
        case NvKey::K_ARROW_DOWN:
            keyEvent.windows_key_code = VK_DOWN;
            break;
        case NvKey::K_ARROW_UP:
            keyEvent.windows_key_code = VK_UP;
            break;
        case NvKey::K_ARROW_RIGHT:
            keyEvent.windows_key_code = VK_RIGHT;
            break;
        case NvKey::K_ARROW_LEFT:
            keyEvent.windows_key_code = VK_LEFT;
            break;
        }

        browserHost->SendKeyEvent(keyEvent);
    }


    /*CefRefPtr<CefBrowser> browser;
    CefRefPtr<CefBrowserHost> browserHost;
    CefBrowserHost::MouseButtonType mouseButton;

    if (m_enableBrowsing
    && (browser = GetBrowser())
    && (browserHost = browser->GetHost()))
    {
    CefKeyEvent keyEvent;
    keyEvent.native_key_code = code;

    switch (action)
    {
    case NvKeyActionType::UP:
    break;
    case NvKeyActionType::DOWN:
    break;
    }

    browserHost->SendKeyEvent(keyEvent);
    }*/

    return false;
}