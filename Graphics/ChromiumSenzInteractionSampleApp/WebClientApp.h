#pragma once
#include "pch.h"
#include "WebClientAppDelegates.h"

namespace nyx
{
  class WebClientApp
    : public CefApp
    , public CefRenderProcessHandler
    , public CefBrowserProcessHandler
  {
  public:
    // Creates all of the BrowserDelegate objects. Implemented in client_app_delegates.
    static void CreateBrowserDelegates(WebClientBrowserDelegate::Set& delegates);
    // Creates all of the RenderDelegate objects. Implemented in client_app_delegates.
    static void CreateRenderDelegates(WebClientRendererDelegate::Set& delegates);
    // Registers custom schemes. Implemented in client_app_delegates.
    static void RegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar> registrar, std::vector<CefString>& cookiable_schemes);

  public:
    WebClientApp();
    IMPLEMENT_REFCOUNTING(WebClientApp);

  public: // CefApp methods.
    virtual void OnRegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar> registrar) override;
    virtual CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() override;
    virtual CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() override;

  public: // CefBrowserProcessHandler methods.
    virtual void OnContextInitialized() override;
    virtual void OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line) override;
    virtual void OnRenderProcessThreadCreated(CefRefPtr<CefListValue> extra_info)      override;
    virtual CefRefPtr<CefPrintHandler> GetPrintHandler() override;

  public: // CefRenderProcessHandler methods.
    virtual void OnRenderThreadCreated(CefRefPtr<CefListValue> extra_info) override;
    virtual void OnWebKitInitialized() override;
    virtual void OnBrowserCreated(CefRefPtr<CefBrowser> browser) override;
    virtual void OnBrowserDestroyed(CefRefPtr<CefBrowser> browser) override;
    virtual CefRefPtr<CefLoadHandler> GetLoadHandler() override;
    virtual bool OnBeforeNavigation(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, NavigationType navigation_type, bool is_redirect) override;
    virtual void OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) override;
    virtual void OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) override;
    virtual void OnUncaughtException(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context, CefRefPtr<CefV8Exception> exception, CefRefPtr<CefV8StackTrace> stackTrace)      override;
    virtual void OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefDOMNode> node) override;
    virtual bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message) override;

  private:

    WebClientBrowserDelegate::Set browser_delegates_; // Set of supported BrowserDelegates. Only used in the browser process.
    WebClientRendererDelegate::Set render_delegates_; // Set of supported RenderDelegates. Only used in the renderer process.
    std::vector<CefString> cookieable_schemes_; // Schemes that will be registered with the global cookie manager. Used in both the browser and renderer process.

  };
}