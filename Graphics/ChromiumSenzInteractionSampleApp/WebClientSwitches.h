#pragma once

namespace cefclient {

  extern const char kUrl[];
  extern const char kCachePath[];
  extern const char kOffScreenFrameRate[];
  extern const char kMultiThreadedMessageLoop[];
  extern const char kOffScreenRenderingEnabled[];
  extern const char kMouseCursorChangeDisabled[];
  extern const char kTransparentPaintingEnabled[];

}  // namespace cefclient

namespace nyx
{
  using namespace cefclient;
}