#pragma once
#include "pch.h"

namespace nyx
{
  template <class T>
  class NvShaderBuffer 
  {
  public:
    NvShaderBuffer(size_t size);  // size in number of elements
    ~NvShaderBuffer();

    void bind();
    void unbind();

    T *map(GLbitfield access = GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    void unmap();

    GLuint getBuffer() { return m_buffer; }
    size_t getSize() const { return m_size; }

    void dump();

  private:
    static const GLenum target = GL_SHADER_STORAGE_BUFFER;

    size_t m_size;

    GLuint m_buffer;
  };

#include "NvShaderBuffer.inl"

}