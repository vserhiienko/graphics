#pragma once
#include "pch.h"

namespace nyx
{
  enum NuiGesture
  {
    NuiGesture_Wave = PXCGesture::Gesture::LABEL_HAND_WAVE,
    NuiGesture_Circle = PXCGesture::Gesture::LABEL_HAND_CIRCLE,
    NuiGesture_Palm = PXCGesture::Gesture::LABEL_POSE_BIG5,
    NuiGesture_Peace = PXCGesture::Gesture::LABEL_POSE_PEACE,
    NuiGesture_Like = PXCGesture::Gesture::LABEL_POSE_THUMB_UP,
    NuiGesture_Dislike = PXCGesture::Gesture::LABEL_POSE_THUMB_DOWN,
    NuiGesture_NavSwipeUp = PXCGesture::Gesture::LABEL_NAV_SWIPE_UP,
    NuiGesture_NavSwipeDown = PXCGesture::Gesture::LABEL_NAV_SWIPE_DOWN,
    NuiGesture_NavSwipeLeft = PXCGesture::Gesture::LABEL_NAV_SWIPE_LEFT,
    NuiGesture_NavSwipeRight = PXCGesture::Gesture::LABEL_NAV_SWIPE_RIGHT,
  };

  class INuiNotify { public: virtual ~INuiNotify(void) {} };

  class INuiGestureNotify : public virtual INuiNotify
  {
  public:
    typedef std::vector<INuiGestureNotify*> Collection;
    virtual void onGestureReceived(NuiGesture, bool, uint32_t) = 0;

  };

  class INuiHandNotify : public virtual INuiNotify
  {
  public:
    typedef std::vector<INuiHandNotify*> Collection;
    virtual void onHandStateChanged(int hand, bool drag, float x, float y) = 0;
    virtual void onHandCaught(void) { }
    virtual void onHandLost(void) { }
  };

  struct NuiFrameTraits
  {
    PXCGesture::GeoNode nodes[2][11];
  };

  class NuiFeed
  {
    friend class NuiFeedVisuals;
    friend class NuiFeedPipeline;
    friend class NuiFeedFaceProc;
    friend class NuiFeedEmotionProc;
    friend class NuiFeedGestureProc;

  protected:
    bool isValid;
    bool awaitFrame;
    UtilPipeline *pipeline;
    NuiFrameTraits frameDetails;
    INuiHandNotify::Collection handCallbacks;
    INuiGestureNotify::Collection gestureCallbacks;

    NuiFeedGestureProc *gestureProc;

  public:
    NuiFeed(void);
    ~NuiFeed(void);
    void addCallback(INuiNotify *notify);

  public:
    void init(void);
    void setScreenSize(int x, int y);
    void pollEvents(void);
    void cleanup(void);

  };

  class NuiFeedVisuals
  {
    NuiFeed *feed;

  public:
    NuiFeedVisuals(void);

    void setFeed(NuiFeed *);
    NuiFeed *getFeed(void);
    void drawHands(void);

  private:

    void drawCircle(
      float cx, 
      float cy, 
      float radius, 
      int num_segments,
      float r = 0.0f, 
      float b = 0.0f,
      float g = 0.0f, 
      float a = 1.0f,
      float lw = 10.0f
      );


  };

}

