#include "pch.h"
#include "WebClientApp.h"
#include "WebClientRenderer.h"

const char kFocusedNodeChangedMessage[] = "ClientRenderer.FocusedNodeChanged";

nyx::WebRenderer::WebRenderer(bool transparent)
{
  NVWindowsLog("creating web renderer");

  m_transparent = transparent;
  m_initialized = false;
  m_view_width = 0;
  m_view_height = 0;
  m_spin_x = 0;
  m_spin_y = 0;

  //m_texture_id = 0;
}

nyx::WebRenderer::~WebRenderer()
{
  NVWindowsLog("destroying web renderer");
  cleanup();
}

void nyx::WebRenderer::initialize()
{
  if (m_initialized) return;

  /*if (!CefCurrentlyOn(TID_UI))
  {
    NVWindowsLog(
      "web renderer: "
      "posting to ui thread: "
      "initialize"
      );

    CefPostTask(TID_UI, base::Bind(
      &WebRenderer::initialize, this
      ));

    return;
  }*/

  NVWindowsLog("initializing web renderer");

  m_texture.init();
  m_initialized = m_texture.textureId != NULL;
}

void nyx::WebRenderer::cleanup()
{
  m_texture.cleanup();
  NVWindowsLog("web renderer: cleanup");
  //if (m_texture_id != 0) glDeleteTextures(1, &m_texture_id);
}

void nyx::WebRenderer::render()
{
  //CEF_REQUIRE_RENDERER_THREAD();
  //CHECK_GL_ERROR();
}

void nyx::WebRenderer::onPaint(
  CefRefPtr<CefBrowser> browser,
  CefRenderHandler::PaintElementType type,
  const CefRenderHandler::RectList& dirtyRects,
  const void* buffer,
  int width,
  int height
  )
{
  //CEF_REQUIRE_RENDERER_THREAD();

  //if (!CefCurrentlyOn(TID_UI))
  //{
  //  // Execute on the UI thread.
  //  CefPostTask(TID_UI, base::Bind(
  //    &WebRenderer::onPaint, this,
  //    browser,
  //    type,
  //    dirtyRects,
  //    buffer,
  //    width,
  //    height
  //    ));

  //  return;
  //}

  if (m_initialized == false) initialize();

  /*if ((m_view_width == 0)
    || (m_view_height == 0)
    || (m_texture_id == 0)
    || (m_initialized == false)) return;*/

  m_texture.update(type, dirtyRects, buffer, width, height);
}

void nyx::WebRenderer::onPopupShow(CefRefPtr<CefBrowser> browser, bool show)
{
  if (!show)
  {
    NVWindowsLog("web renderer popup show");
    // Clear the popup rectangle.
    clearPopupRects();
  }
}

void nyx::WebRenderer::onPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect)
{
  if (rect.width <= 0 || rect.height <= 0) return;
  NVWindowsLog("web renderer popup size");

  m_original_popup_rect = rect;
  m_popup_rect = getPopupRectInWebView(m_original_popup_rect);
}

void nyx::WebRenderer::setSpin(float sx, float sy)
{
  m_spin_x = sx;
  m_spin_y = sy;
}

void nyx::WebRenderer::incrementSpin(float dsx, float dsy)
{
  m_spin_x -= dsx;
  m_spin_y -= dsy;
}

bool nyx::WebRenderer::isTransparent()
{
  return m_transparent;
}

void nyx::WebRenderer::setIsTransparent(bool value)
{
  m_transparent = value;
}

int nyx::WebRenderer::getViewWidth()
{
  return m_view_width;
}

int nyx::WebRenderer::getViewHeight()
{
  return m_view_height;
}

const CefRect& nyx::WebRenderer::popup_rect() const
{
  return m_popup_rect;
}

const CefRect& nyx::WebRenderer::original_popup_rect() const
{
  return m_original_popup_rect;
}

CefRect nyx::WebRenderer::getPopupRectInWebView(const CefRect& original_rect)
{
  CefRect rc(original_rect);

  // if x or y are negative, move them to 0.
  if (rc.x < 0) rc.x = 0;
  if (rc.y < 0) rc.y = 0;
  // if popup goes outside the view, try to reposition origin
  if (rc.x + rc.width > m_view_width) rc.x = m_view_width - rc.width;
  if (rc.y + rc.height > m_view_height) rc.y = m_view_height - rc.height;
  // if x or y became negative, move them to 0 again.
  if (rc.x < 0) rc.x = 0;
  if (rc.y < 0) rc.y = 0;

  return rc;
}

void nyx::WebRenderer::clearPopupRects()
{
  NVWindowsLog("web renderer clear popups");
  m_popup_rect.Set(0, 0, 0, 0);
  m_original_popup_rect.Set(0, 0, 0, 0);
}

namespace client_renderer
{
  using namespace nyx;
  const char kFocusedNodeChangedMessage[] = "ClientRenderer.FocusedNodeChanged";

  class ClientRenderDelegate
    : public WebClientRendererDelegate
  {
  public:
    ClientRenderDelegate()
      : last_node_is_editable_(false)
    {
      NVWindowsLog("creating client render delegate");
    }

    virtual void OnWebKitInitialized(
      CefRefPtr<WebClientApp> app
      ) override
    {
      NVWindowsLog("client render delegate: webkit initialized");

      // Create the renderer-side router for query handling.
      CefMessageRouterConfig config;
      message_router_ = CefMessageRouterRendererSide::Create(config);
    }

    virtual void OnContextCreated(
      CefRefPtr<WebClientApp> app,
      CefRefPtr<CefBrowser> browser,
      CefRefPtr<CefFrame> frame,
      CefRefPtr<CefV8Context> context
      ) override
    {
      NVWindowsLog("client render delegate: context created");

      message_router_->OnContextCreated(
        browser, frame, context
        );
    }

    virtual void OnContextReleased(
      CefRefPtr<WebClientApp> app,
      CefRefPtr<CefBrowser> browser,
      CefRefPtr<CefFrame> frame,
      CefRefPtr<CefV8Context> context
      ) override
    {
      NVWindowsLog("client render delegate: context released");

      message_router_->OnContextReleased(
        browser, frame, context
        );
    }

    virtual void OnFocusedNodeChanged(
      CefRefPtr<WebClientApp> app,
      CefRefPtr<CefBrowser> browser,
      CefRefPtr<CefFrame> frame,
      CefRefPtr<CefDOMNode> node
      ) override
    {
      NVWindowsLog("client render delegate: focused node changed");

      bool is_editable = (node.get() && node->IsEditable());
      if (is_editable != last_node_is_editable_)
      {
        // Notify the browser of the change in focused element type.
        last_node_is_editable_ = is_editable;
        CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create(kFocusedNodeChangedMessage);
        message->GetArgumentList()->SetBool(0, is_editable);
        browser->SendProcessMessage(PID_BROWSER, message);
      }
    }

    virtual bool OnProcessMessageReceived(
      CefRefPtr<WebClientApp> app,
      CefRefPtr<CefBrowser> browser,
      CefProcessId source_process,
      CefRefPtr<CefProcessMessage> message
      ) override
    {
      return message_router_->OnProcessMessageReceived(
        browser, source_process, message
        );
    }

  private:

    bool last_node_is_editable_;
    CefRefPtr<CefMessageRouterRendererSide> message_router_; // Handles the renderer side of query routing.

    IMPLEMENT_REFCOUNTING(ClientRenderDelegate);
  };

  void CreateRenderDelegates(WebClientRendererDelegate::Set& delegates)
  {
    NVWindowsLog("creating client delegates");
    delegates.insert(new ClientRenderDelegate);
  }

}