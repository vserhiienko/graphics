#pragma once

#ifndef NOMINMAX
#define NOMINMAX
#endif
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#ifndef SAFE_DELETE
#define SAFE_DELETE(p) { if (p) { delete (p); (p)=NULL; } }
#endif
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if (p) { delete[] (p); (p)=NULL; } }
#endif
#ifndef SAFE_FUSION_RELEASE_IMAGE_FRAME
#define SAFE_FUSION_RELEASE_IMAGE_FRAME(p) { if (p) { static_cast<void>(NuiFusionReleaseImageFrame(p)); (p)=NULL; } }
#endif

#ifndef _INTSAFE_H_INCLUDED_
#define _INTSAFE_H_INCLUDED_
#endif 

#ifndef UINT_ERROR
#define UINT_ERROR 0xffffffff
#endif

#ifndef SHORT_MAX
#define SHORT_MAX 0xffff
#endif

#ifndef _Oasis_stringify
#define _Oasis_stringify(x) #x
#endif

#define AssertOwnThread() _ASSERT_EXPR(GetCurrentThreadId() == m_threadId, __FUNCTIONW__ L" called on wrong thread!");
#define AssertOtherThread() _ASSERT_EXPR(GetCurrentThreadId() != m_threadId, __FUNCTIONW__ L" called on wrong thread!");

#include <Windows.h>

#ifndef _STDINT
#include <stdint.h>
//#include <safeint.h> //?
#endif

#include <algorithm>
#include <set>
#include <vector>
#include <assert.h>

#include <pxcimage.h>
#include <pxcsession.h>
#include <pxccapture.h>
#include <pxcmetadata.h>
#include <pxcprojection.h>
#include <pxcsegmentation.h>

#include <util_capture.h>
#include <util_pipeline.h>

namespace gaia
{
  struct vec2f { float x, y; };
  struct vec3f { float x, y, z; };

  namespace utils
  {
    struct SizeU
    {
      uint32_t width;
      uint32_t height;
    };

    inline static void trace(_In_ const char* fmt, _In_opt_ ...)
    {
      const int length = 512;
      char buffer[length];
      va_list ap;

      va_start(ap, fmt);
      vsnprintf_s(buffer, length - 1, fmt, ap);
      OutputDebugStringA(buffer);
      OutputDebugStringA("\n");
      va_end(ap);
    }

    /// <summary>
    /// Provides some common methods for image frames
    /// </summary>
    template <class _TyPixel> struct FrameBase
    {
      /// Types

      typedef _TyPixel Pixel;
      typedef std::vector<_TyPixel> PixelVector;
      static size_t const pixelSize = sizeof(_TyPixel);
      static unsigned const pixelSizeU = sizeof(_TyPixel);

      /// Data

      PixelVector data;
      SizeU dims;

      FrameBase() : data()
      {
        dims.width = 0, dims.height = 0;
      }

      /// Funcs

      inline _TyPixel *operator[](unsigned r)
      {
        return &data[0] + r * dims.height;
      }

      inline void *getMem() { return (void *)(&data[0]); }
      inline uint8_t *getBytes() { return (uint8_t *)(&data[0]); }
      inline void const *getMem() const { return (const void *)(&data[0]); }
      inline uint8_t const *getBytes() const { return (const uint8_t *)(&data[0]); }

      inline size_t getSize() const  { return data.size(); }
      inline unsigned getSizeU() const  { return (unsigned)data.size(); }
      inline size_t getByteSize() const  { return getSize() * pixelSize; }
      inline unsigned getByteSizeU() const  { return getSizeU() * pixelSizeU; }
      inline size_t getPitch() const { return dims.width * pixelSize; }
      inline unsigned getPitchU() const { return dims.width * pixelSizeU; }

      inline void copySettingsTo(FrameBase &frame) const
      {
        frame.dims.width = dims.width;
        frame.dims.height = dims.height;
        frame.data.resize(getSize());
      }
      inline void copySettingsFrom(FrameBase const &frame)
      {
        dims.width = frame.dims.width;
        dims.height = frame.dims.height;
        data.resize(frame.getSize());
      }

      inline void copyDataTo(FrameBase &frame) const 
      {
        assert(frame.getByteSize() == getByteSize());
        memcpy((void*)frame.data.data(), (const void*)data.data(), getByteSize());
      }
      inline void copyDataFrom(FrameBase const &frame)
      {
        assert(frame.getByteSize() == getByteSize());
        memcpy((void*)data.data(), (const void*)frame.data.data(), getByteSize());
      }

    };

    /// <summary>
    /// The 32-bit coords 2d format.
    /// </summary>
    struct CoordsL32
    {
      long x, y;
    };

    /// <summary>
    /// The 32-bit 2d coords format.
    /// </summary>
    struct ColorCoordsFrame : public FrameBase<CoordsL32> {};

    /// <summary>
    /// The 32-bit float depth format.
    /// </summary>
    struct DepthF32Frame : public FrameBase<float>
    {
      float invalids[2];
    };

    /// <summary>
    /// The 8-bit grayscale format.
    /// </summary>
    struct Grayscale8Frame : public FrameBase<uint8_t>
    {
      ColorCoordsFrame depth_projection;
      bool depth_projection_is_available;
    };


    // Safe release for interfaces
    template<class _TyInterface>
    inline void safeRelease(_TyInterface *& pInterfaceToRelease)
    {
      if (pInterfaceToRelease != NULL)
      {
        pInterfaceToRelease->Release();
        pInterfaceToRelease = NULL;
      }
    }
    template<typename _Ty> 
    inline _Ty &toRef(_Ty *_Ptr)
    {
      return (*_Ptr);
    }
    template <typename _Ty>
    void zeroValue(_Ty *data, size_t sz = sizeof _Ty) 
    { 
      memset(data, 0, sz); 
    }
    template <typename _Ty>
    size_t zeroValueSz(_Ty *data, size_t sz = sizeof _Ty)
    {
      memset(data, 0, sz); return sz;
    }
    template <typename _Ty> 
    size_t zeroValueByRefSz(_Ty &data, size_t sz = sizeof _Ty)
    { 
      memset(&data, 0, sz); return sz;
    }

  }
}

