#pragma once
#include "GaiaUtilFrames.h"

namespace gaia
{
  class IRawStreams
  {
  public:
    typedef IRawStreams *Pointer;
    typedef std::set<IRawStreams::Pointer> Set;
    typedef std::vector<IRawStreams::Pointer> Vector;

    /// <summary>
    /// Interface that provides methods to trace streaming state
    /// </summary>
    class INotify
    {
    public:
      typedef INotify *Pointer;
      typedef std::set<INotify::Pointer> Set;
      typedef std::vector<INotify::Pointer> Vector;

    public:
      virtual void onFrameAcquired(IRawStreams const*) = 0;
      virtual void onDeviceReconnected(IRawStreams*) {};
      virtual void onDeviceDisconnected(IRawStreams*) {};
    };

  public:
    inline virtual void init(void) { }
    inline virtual void quit(void) { }
    inline virtual void resume(void) { }
    inline virtual void suspend(void) { }
    inline virtual void update(void) { }
    virtual void registerNotify(INotify::Pointer notify) = 0;
    virtual void unregisterNotify(INotify::Pointer notify) = 0;

  public:
    virtual utils::DepthF32Frame *getDepthF32Frame() = 0;
    virtual utils::DepthF32Frame &getDepthF32FrameRef() = 0;
    virtual utils::Grayscale8Frame *getGrayscale8Frame() = 0;
    virtual utils::Grayscale8Frame &getGrayscale8FrameRef() = 0;
    virtual utils::DepthF32Frame const *getDepthF32Frame() const = 0;
    virtual utils::DepthF32Frame const &getDepthF32FrameRef() const = 0;
    virtual utils::Grayscale8Frame const *getGrayscale8Frame() const = 0;
    virtual utils::Grayscale8Frame const &getGrayscale8FrameRef() const = 0;

  public:
    virtual bool getCameraIntrinsicsParameters(
      gaia::vec2f &color_focal_lengths,
      gaia::vec2f &depth_focal_lengths,
      gaia::vec2f &color_principal_point,
      gaia::vec2f &depth_principal_point,
      gaia::vec2f &color_dimensions,
      gaia::vec2f &depth_dimensions
      ) = 0;
    virtual bool getAccelerometerReadings(
      gaia::vec3f &accel_readings
      ) = 0;

  };
}
