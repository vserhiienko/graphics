#include "GaiaKinectStreams.h"

#include <ppl.h>
#include <ppltasks.h>

void gaia::KinectRawStreams::createFirstConnectedSensor()
{
  HRESULT hr;
  NuiSensorComPtr pNuiSensor;

  int iSensorCount = 0;
  hr = NuiGetSensorCount(&iSensorCount);
  if (FAILED(hr))
    return;

  // Look at each Kinect sensor
  for (int i = 0; i < iSensorCount; ++i)
  {
    // Create the sensor so we can check status, if we can't create it, move on to the next
    hr = NuiCreateSensorByIndex(i, &pNuiSensor);
    if (FAILED(hr))
    {
      continue;
    }

    // Get the status of the sensor, and if connected, then we can initialize it
    hr = pNuiSensor->NuiStatus();
    if (S_OK == hr)
    {
      m_sensor = pNuiSensor;
      break;
    }
  }

  if (NULL == m_sensor)
    return;

  // Initialize the Kinect and specify that we'll be using depth
  hr = m_sensor->NuiInitialize(
    NUI_INITIALIZE_FLAG_USES_COLOR
    | NUI_INITIALIZE_FLAG_USES_DEPTH
    //| NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX
    );

  if (FAILED(hr))
    m_sensor.Reset();
}

void gaia::KinectRawStreams::init()
{
  DWORD dw_width, dw_height;
  unsigned color_pels, depth_pels;

  m_depth_stream_handle = INVALID_HANDLE_VALUE;
  m_color_stream_handle = INVALID_HANDLE_VALUE;
  m_depth_next_event_handle = INVALID_HANDLE_VALUE;
  m_color_next_event_handle = INVALID_HANDLE_VALUE;

  m_color_res = NUI_IMAGE_RESOLUTION_640x480;
  NuiImageResolutionToSize(m_color_res, dw_width, dw_height);
  m_color_dimensions.width = (unsigned)dw_width;
  m_color_dimensions.height = (unsigned)dw_height;
  color_pels = m_color_dimensions.width * m_color_dimensions.height;

  m_depth_res = NUI_IMAGE_RESOLUTION_640x480;
  NuiImageResolutionToSize(m_depth_res, dw_width, dw_height);
  m_depth_dimensions.width = (unsigned)dw_width;
  m_depth_dimensions.height = (unsigned)dw_height;
  depth_pels = m_depth_dimensions.width * m_depth_dimensions.height;

  m_depth_frame.data.resize(depth_pels);
  m_depth_frame.dims.width = m_depth_dimensions.width;
  m_depth_frame.dims.height = m_depth_dimensions.height;

  m_raw_depth_frame.data.resize(depth_pels);
  m_raw_depth_frame.dims.width = m_depth_dimensions.width;
  m_raw_depth_frame.dims.height = m_depth_dimensions.height;

  m_raw_depth_with_player_index_frame.data.resize(depth_pels);
  m_raw_depth_with_player_index_frame.dims.width = m_depth_dimensions.width;
  m_raw_depth_with_player_index_frame.dims.height = m_depth_dimensions.height;

  m_grayscale_frame.data.resize(color_pels);
  m_grayscale_frame.dims.width = m_color_dimensions.width;
  m_grayscale_frame.dims.height = m_color_dimensions.height;

  m_color_frame.data.resize(color_pels);
  m_color_frame.dims.width = m_color_dimensions.width;
  m_color_frame.dims.height = m_color_dimensions.height;

  m_grayscale_frame.depth_projection.data.resize(color_pels);
  m_grayscale_frame.depth_projection.dims.width = m_color_dimensions.width;
  m_grayscale_frame.depth_projection.dims.height = m_color_dimensions.height;
  m_grayscale_frame.depth_projection_is_available = false;

  createFirstConnectedSensor();
  m_is_instance_valid = !!m_sensor.Get();

  ResultHandle hr;

  if (m_is_instance_valid)
  {
    hr = m_sensor->NuiGetCoordinateMapper(
      m_coords_mapper.ReleaseAndGetAddressOf()
      );
    m_is_coords_mapper_available = SUCCEEDED(hr);
  }

  if (m_is_instance_valid)
  {
    m_depth_next_event_handle
      = CreateEvent(NULL, TRUE, FALSE, NULL);
    hr = m_sensor->NuiImageStreamOpen(
      NUI_IMAGE_TYPE_DEPTH,
      //NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX,
      m_depth_res, 0, 2,
      m_depth_next_event_handle,
      &m_depth_stream_handle
      );
    m_is_instance_valid
      = SUCCEEDED(hr);
  }

  if (m_is_instance_valid)
  {
    m_color_next_event_handle
      = CreateEvent(NULL, TRUE, FALSE, NULL);
    hr = m_sensor->NuiImageStreamOpen(
      NUI_IMAGE_TYPE_COLOR,
      m_color_res, 0, 2,
      m_color_next_event_handle,
      &m_color_stream_handle
      );
    m_is_instance_valid
      = SUCCEEDED(hr);
  }

  if (m_is_instance_valid)
  {
    m_stream_event_handles[0] = m_depth_next_event_handle;
    m_stream_event_handles[1] = m_color_next_event_handle;
    m_stream_event_handle_count = ARRAYSIZE(m_stream_event_handles);

    m_wait_all_succeeded_result = 0;
    for (DWORD ehi = 0; ehi < m_stream_event_handle_count; ehi++)
      m_wait_all_succeeded_result += WAIT_OBJECT_0 + ehi;
  }

  if (m_is_instance_valid)
  {
    auto interval = m_wait_for_frames_interval;
    m_wait_for_frames_interval = INFINITE;

    update();
    calculateColorProjection();

    m_wait_for_frames_interval = interval;
  }

  if (m_is_instance_valid)
  {
    //Initted(this);
  }

}

void gaia::KinectRawStreams::quit()
{
  if (m_is_instance_valid)
  {
    //// Failes to release next frame event handles, so we will simply omit those lines
    //if (m_depth_next_event_handle != INVALID_HANDLE_VALUE) CloseHandle(m_depth_next_event_handle);
    //if (m_color_next_event_handle != INVALID_HANDLE_VALUE) CloseHandle(m_color_next_event_handle);

    if (m_sensor.Get()) m_sensor->NuiShutdown();
  }

  //Quitted(this);
}

void gaia::KinectRawStreams::resume()
{
}

void gaia::KinectRawStreams::suspend()
{
}

void gaia::KinectRawStreams::update()
{
#pragma region Static callbacks
  static auto notify_on_frame_move = [this](INotify::Pointer notify)
  {
    notify->onFrameAcquired(this);
  };
  static auto notify_on_device_reconnected = [this](INotify::Pointer notify)
  {
    notify->onDeviceReconnected(this);
  };
  static auto notify_on_device_disconnected = [this](INotify::Pointer notify)
  {
    notify->onDeviceDisconnected(this);
  };
#pragma endregion

  // Here we should ensure 
  // both streams are synchornized
  if (WaitForMultipleObjects(
    m_stream_event_handle_count, m_stream_event_handles,
    true, m_wait_for_frames_interval
    ) == WAIT_OBJECT_0)
  {
    processStreamFrame(
      m_depth_stream_handle,
      m_raw_depth_frame.getMem(),
      m_raw_depth_frame.getByteSizeU()
      );
    processStreamFrame(
      m_color_stream_handle,
      m_color_frame.getMem(),
      m_color_frame.getByteSizeU()
      );

    processRawDepth();
    generateGrayscaleFrame();

    //// Its enough to do it once (init method)
    //CalculateColorProjection();

    std::_For_each(
      m_notifies.begin(),
      m_notifies.end(),
      notify_on_frame_move
      );
  }
}


bool gaia::KinectRawStreams::processStreamFrame(
  ObjectHandle stream_handle,
  ObjectHandle target_frame_mem,
  unsigned expected_size
  )
{
  if (m_is_instance_valid)
  {
    ResultHandle hr = S_OK;
    NUI_IMAGE_FRAME image_frame;
    NUI_LOCKED_RECT locked_rect;

    if (
      SUCCEEDED(hr = m_sensor->NuiImageStreamGetNextFrame(
      stream_handle, 0, &image_frame))
      &&
      SUCCEEDED(hr = image_frame.pFrameTexture->LockRect(
      0, &locked_rect, NULL, 0))
#if _DEBUG
      &&
      unsigned(locked_rect.size) == expected_size
#endif
      )
    {
      // copy to our internal containers
      memcpy(
        target_frame_mem,
        locked_rect.pBits,
        locked_rect.size
        );

      // release allocated resources
      hr = image_frame.pFrameTexture->UnlockRect(0);
      hr = m_sensor->NuiImageStreamReleaseFrame(
        stream_handle,
        &image_frame
        );
    }
  }

  return false;
}


void gaia::KinectRawStreams::processRawDepth()
{
  // *Hardcoded*
  // TODO: Get a valid depth range from sdk
  static auto const range_min = 0x0001;
  static auto const range_max = 0xffff;

  auto depth32 = &m_depth_frame.data[0];
  auto depth16 = &m_raw_depth_frame.data[0];
  auto depthPi = &m_raw_depth_with_player_index_frame.data[0];
  auto pixel_count = m_raw_depth_frame.getSizeU();

  concurrency::parallel_for(unsigned(0), pixel_count, [depth16, depth32, depthPi](unsigned i)
  {
    // depth is out of range or invalid
    if (depth16[i] < range_min || depth16[i] > range_max)
    {
      depth32[i] = NAN;
      depthPi[i].depth = USHRT_MAX;
      depthPi[i].playerIndex = USHRT_MAX;
    }
    else
    {
      depthPi[i].depth = depth16[i];
      depthPi[i].playerIndex = USHRT_MAX;
      depth32[i] = float(depth16[i]) / 8000.0f;
    }
  });
}

void gaia::KinectRawStreams::generateGrayscaleFrame()
{
  auto color = &m_color_frame.data[0];
  auto grayscale = &m_grayscale_frame.data[0];
  auto pixel_count = m_color_frame.getSizeU();
  concurrency::parallel_for(unsigned(0), pixel_count, [color, grayscale](unsigned i)
  {
    float
      r = float(color[i].r) / 255.0f,
      g = float(color[i].g) / 255.0f,
      b = float(color[i].b) / 255.0f;
    float gray
      = 0.299f * r
      + 0.587f * g
      + 0.114f * b;
    gray *= 255.0f;

    grayscale[i]
      = uint8_t(gray);
  });

}

void gaia::KinectRawStreams::calculateColorProjection()
{
  ResultHandle hr;
  if (m_is_coords_mapper_available)
  {
    hr = m_coords_mapper->MapDepthFrameToColorFrame(
      m_depth_res,
      (DWORD)m_raw_depth_with_player_index_frame.data.size(),
      m_raw_depth_with_player_index_frame[0],
      NUI_IMAGE_TYPE_COLOR,
      m_color_res,
      (DWORD)m_grayscale_frame.data.size(),
      reinterpret_cast<NUI_COLOR_IMAGE_POINT*>(m_grayscale_frame.depth_projection[0])
      );

    m_grayscale_frame.depth_projection_is_available
      = SUCCEEDED(hr);
  }
}

bool gaia::KinectRawStreams::getCameraIntrinsicsParameters(
  gaia::vec2f &color_focal_lengths,
  gaia::vec2f &depth_focal_lengths,
  gaia::vec2f &color_principal_point,
  gaia::vec2f &depth_principal_point,
  gaia::vec2f &color_dimensions,
  gaia::vec2f &depth_dimensions
  )
{
  color_dimensions.x = (float)m_color_dimensions.width;
  depth_dimensions.x = (float)m_depth_dimensions.width;
  color_dimensions.y = (float)m_color_dimensions.height;
  depth_dimensions.y = (float)m_depth_dimensions.height;

  color_focal_lengths.x = NUI_CAMERA_COLOR_NOMINAL_FOCAL_LENGTH_IN_PIXELS;
  depth_focal_lengths.x = NUI_CAMERA_DEPTH_NOMINAL_FOCAL_LENGTH_IN_PIXELS;
  color_focal_lengths.y = NUI_CAMERA_COLOR_NOMINAL_FOCAL_LENGTH_IN_PIXELS;
  depth_focal_lengths.y = NUI_CAMERA_DEPTH_NOMINAL_FOCAL_LENGTH_IN_PIXELS;

  // These values should be some how extracted from the NuiSdk
  // See SenzRawStreams implementation for an example

  /// *Hardcoded*
  color_principal_point.x = (float)(m_color_dimensions.width / 2);
  depth_principal_point.x = (float)(m_depth_dimensions.width / 2);
  color_principal_point.y = (float)(m_color_dimensions.height / 2);
  depth_principal_point.y = (float)(m_depth_dimensions.height / 2);

  return true;
}

bool gaia::KinectRawStreams::getAccelerometerReadings(
  gaia::vec3f &accel_readings
  )
{
  if (m_is_instance_valid)
  {
    ResultHandle hr;
    KinectVec4 accel_current_readings;
    if (SUCCEEDED(hr = m_sensor->NuiAccelerometerGetCurrentReading(&accel_current_readings)))
    {
      accel_readings.x = accel_current_readings.x;
      accel_readings.y = accel_current_readings.y;
      accel_readings.z = accel_current_readings.z;
      return true;
    }
    else
    {
      // see the reason of failure
    }
  }

  return false;
}

#pragma region Ctor/dtor

gaia::KinectRawStreams::KinectRawStreams() //: State(NULL)
{
  m_last_notify_count = 0;
  m_last_consumer_count = 0;

  m_block_execution = false;
  m_is_instance_valid = false;
  m_was_state_resumed = false;
  m_was_device_disconnected = false;
  m_wait_for_frames_interval = 0;
}

gaia::KinectRawStreams::~KinectRawStreams()
{
  quit();
}

#pragma endregion

#pragma region Notify
void gaia::KinectRawStreams::registerNotify(INotify::Pointer notify)
{
  INotify::Set unique_notifies(
    m_notifies.begin(),
    m_notifies.end()
    );
  unique_notifies.insert(notify);

  m_notifies.clear();
  m_notifies.insert(
    m_notifies.begin(),
    unique_notifies.begin(),
    unique_notifies.end()
    );

  m_last_notify_count
    = (unsigned)m_notifies.size();
}

void gaia::KinectRawStreams::unregisterNotify(INotify::Pointer notify)
{
  if (m_notifies.empty()) return;

  auto notify_iterator = std::find(
    m_notifies.begin(),
    m_notifies.end(),
    notify
    );

  if (notify_iterator != m_notifies.end())
  {
    m_notifies.erase(notify_iterator);
    m_last_notify_count = (unsigned)m_notifies.size();
  }
}
#pragma endregion

#pragma region Singleton
gaia::KinectRawStreams *gaia::KinectRawStreams::s_instance = NULL;
void gaia::KinectRawStreams::destroyInstance() { SAFE_DELETE(s_instance); }
gaia::KinectRawStreams *gaia::KinectRawStreams::getInstance() { return s_instance; }
gaia::KinectRawStreams *gaia::KinectRawStreams::createInstance() { return s_instance ? s_instance : s_instance = new KinectRawStreams(); }
#pragma endregion


