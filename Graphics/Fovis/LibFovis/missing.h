#pragma once
#ifndef _GAIA_FOVIS_MISSIGN_INCLUDED_
#define _GAIA_FOVIS_MISSIGN_INCLUDED_
#include<vector>

#include < time.h >
#include <windows.h> //I've ommited this line.
#include <algorithm>
#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

#ifdef NO_DATA
#undef NO_DATA
#endif
#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

inline static int isnan(float x) { return x != x; }
inline static int isinf(float x) { return !isnan(x) && isnan(x - x); }

inline static int posix_memalign(void **memptr, size_t alignment, size_t size)
{
	(*memptr) = _aligned_malloc(size, alignment);
	return memptr == NULL;
}

struct timezone
{
	int  tz_minuteswest; /* minutes W of Greenwich */
	int  tz_dsttime;     /* type of dst correction */
};

inline static int gettimeofday(struct timeval *tv, struct timezone *tz)
{
	FILETIME ft;
	unsigned __int64 tmpres = 0;
	static int tzflag;

	if (NULL != tv)
	{
		GetSystemTimeAsFileTime(&ft);

		tmpres |= ft.dwHighDateTime;
		tmpres <<= 32;
		tmpres |= ft.dwLowDateTime;

		/*converting file time to unix epoch*/
		tmpres -= DELTA_EPOCH_IN_MICROSECS;
		tmpres /= 10;  /*convert into microseconds*/
		tv->tv_sec = (long)(tmpres / 1000000UL);
		tv->tv_usec = (long)(tmpres % 1000000UL);
	}

	if (NULL != tz)
	{
		if (!tzflag)
		{
			_tzset();
			tzflag++;
		}
    long minuteswest;
    tz->tz_minuteswest = minuteswest / 60;
    //tz->tz_minuteswest = _timezone / 60;
    _get_daylight(&tz->tz_dsttime);
    //tz->tz_dsttime = _daylight;
  }

	return 0;
}

namespace gaia_missing
{

  template <typename _Ty>
  struct Array
  {
    //_Ty *data;
    std::vector<_Ty> data;
    Array(unsigned size)
    {
      data.resize(size);
      //data = new _Ty[size];
      //memset(data, 0, size * sizeof(_Ty))
    }
    ~Array()
    {
      //delete[] data;
    }

    operator _Ty *() { return &data[0]; }
    operator _Ty const *() const { return &data[0]; }
    _Ty &operator[](int index) { return data[index]; }
    _Ty operator[](int index) const { return data[index]; }
    _Ty &operator[](unsigned index) { return data[index]; }
    _Ty operator[](unsigned index) const { return data[index]; }
  };

  template <typename _Ty, unsigned _Alignment = 16>
  struct AlignedArray
  {
    _Ty *data;

    AlignedArray(unsigned size) 
    { 
#if _DEBUG
      data = (_Ty *)_aligned_malloc_dbg(size * sizeof(_Ty), _Alignment, __FILE__, __LINE__);
      //data = (_Ty *)_aligned_recalloc_dbg((void*)data, size, sizeof(_Ty), _Alignment, __FILE__, __LINE__);
#else
      data = (_Ty *)_aligned_malloc(size * sizeof(_Ty), _Alignment);
      //data = (_Ty *)_aligned_recalloc((void*)data, size, sizeof(_Ty), _Alignment);
#endif
      memset(data, 0, _aligned_msize(data, _Alignment, 0));
    }

    ~AlignedArray() 
    {
#if _DEBUG
      _aligned_free_dbg((void*)data);
#else
      _aligned_free((void*)data);
#endif
    }

    operator _Ty *() { return data; }
    operator _Ty const *() const { return data; }
    _Ty &operator[](int index) { return data[index]; }
    _Ty operator[](int index) const { return data[index]; }
    _Ty &operator[](unsigned index) { return data[index]; }
    _Ty operator[](unsigned index) const { return data[index]; }
  };
  

}

#endif