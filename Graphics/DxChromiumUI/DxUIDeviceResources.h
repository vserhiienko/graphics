#pragma once
#include <DxUICore.h>
#include <DxResourceManager.h>
#include <DxDeviceResources.h>

namespace dxui
{
  class DxUIDeviceResources
  {
  public:
    dx::DxDeviceResources *d3d;

  public:
    DxUIDeviceResources(dx::DxDeviceResources *d3d);
    void onPaint(CefRenderHandler::PaintElementType type, const CefRenderHandler::RectList& dirtyRects, const void *buffer, int width, int height);
    dx::SRView &getShaderResourceView(void);
    void setViewInfo(int width, int height);
    void getViewInfo(int &width, int &height) const;

  protected:
    void swapSurfaces(void);
    void onDeviceLost(dx::DxDeviceResources *);
    void onDeviceRestored(dx::DxDeviceResources *);
    void onSurfaceResized(const void *, int, int);

  protected:
    unsigned pitch;
    unsigned slicePitch;
    dx::Texture2 surfaces[2];
    dx::Texture2 stagingSurface;
    dx::SRView shaderResourceViews[2];
    dx::CTexture2Description surfaceDesc;
    dx::CTexture2Description stagingSurfaceDesc;

  };


}