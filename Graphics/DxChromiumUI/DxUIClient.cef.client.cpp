#include "DxUIClient.h"

CefRefPtr<CefRenderHandler>       dxui::DxUIClient::GetRenderHandler() { return this; }
CefRefPtr<CefDisplayHandler>      dxui::DxUIClient::GetDisplayHandler() { return this; }
CefRefPtr<CefRequestHandler>      dxui::DxUIClient::GetRequestHandler() { return this; }
CefRefPtr<CefLifeSpanHandler>     dxui::DxUIClient::GetLifeSpanHandler() { return this; }
