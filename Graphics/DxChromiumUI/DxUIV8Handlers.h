#pragma once
#include <DxUICore.h>

namespace dxui
{
  class DxUIV8Handler
    : public CefV8Handler
  {
    IMPLEMENT_REFCOUNTING(DxUIV8Handler);

  public:
    virtual bool Execute(
      const CefString& name,
      CefRefPtr<CefV8Value> object,
      const CefV8ValueList& arguments,
      CefRefPtr<CefV8Value>& retval,
      CefString& exception
      );
    
  };
}