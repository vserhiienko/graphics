#include <DxUtils.h>
#include "DxUIClient.h"

namespace dxui
{
  namespace internal
  {
    class DxUIV8Handler
      : public CefV8Handler
    {
      IMPLEMENT_REFCOUNTING(DxUIV8Handler);

    public:
      DxUIClient *client; // host reference
      std::string dxuiSendEvent; // send event to browser
      std::string dxuiOnShow; // register onShow
      std::string dxuiOnHide; // register onHide
      CefV8ValueList emptyArgs;
      CefRefPtr<CefV8Value> dxuiOnShowCallback;
      CefRefPtr<CefV8Value> dxuiOnHideCallback;
      CefRefPtr<CefV8Context> dxuiOnShowContext;
      CefRefPtr<CefV8Context> dxuiOnHideContext;

    public:
      DxUIV8Handler(DxUIClient *client)
        : client(client)
      {
        dxuiSendEvent = "dxuiSendEvent";
        dxuiOnShow = "dxuiOnShow";
        dxuiOnHide = "dxuiOnHide";
      }

      virtual bool Execute(
        const CefString &name,
        CefRefPtr<CefV8Value> object,
        const CefV8ValueList &arguments,
        CefRefPtr<CefV8Value> &retval,
        CefString &exception
        )
      {
        if (name == dxuiSendEvent)
        {
          CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create("$e");
          message->GetArgumentList()->SetInt(0, arguments.size());
          CefRefPtr<CefBrowser> browser = CefV8Context::GetCurrentContext()->GetBrowser();
          retval = CefV8Value::CreateBool(browser->SendProcessMessage(PID_BROWSER, message));
          return true;
        }
        else if (name == dxuiOnShow && arguments.size() == 1 && arguments[0]->IsFunction())
        {
          // store callbacks
          dxuiOnShowCallback = arguments[0];
          dxuiOnShowContext = CefV8Context::GetCurrentContext();
          retval = CefV8Value::CreateBool(true);
          return true;
        }
        else if (name == dxuiOnHide && arguments.size() == 1 && arguments[0]->IsFunction())
        {
          // store callbacks
          dxuiOnHideCallback = arguments[0];
          dxuiOnHideContext = CefV8Context::GetCurrentContext();
          retval = CefV8Value::CreateBool(true);
          return true;
        }

        return false;
      }

      void sendV8Show()
      {
        if (dxuiOnShowCallback && dxuiOnShowContext)
        {
          dxuiOnShowCallback->ExecuteFunctionWithContext(dxuiOnShowContext, 0, emptyArgs);
        }
      }
      void sendV8Hide()
      {
        if (dxuiOnHideCallback && dxuiOnHideContext)
        {
          dxuiOnHideCallback->ExecuteFunctionWithContext(dxuiOnHideContext, 0, emptyArgs);
        }
      }

    };
  }

  class DxUIClient::DXUIClientPrivate
  {
  public:
    DxUIClient *client;
    CefRefPtr<internal::DxUIV8Handler> v8Handler;
    DXUIClientPrivate(DxUIClient *client) : client(client)
    {
    }
  };
}

dxui::DxUIClient::DXUIClientPrivate *dxui::DxUIClient::getClientDetails()
{
  return implDetails ? implDetails : implDetails = new DXUIClientPrivate(this);
}

void dxui::DxUIClient::onUIShow()
{
  CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create("$<");
  browser->SendProcessMessage(PID_RENDERER, message);
}

void dxui::DxUIClient::onUIHide()
{
  CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create("$>");
  browser->SendProcessMessage(PID_RENDERER, message);
}

void dxui::DxUIClient::OnWebKitInitialized()
{
  dxui::trace("dxui:client: webkit initialized");

  // register v8 extensions

  // Define the extension contents.
  std::string extensionCode =
    "var test;"
    "if (!test)"
    "  test = {};"
    "(function() {"
    "  test.myval = 'My Value!';"
    "})();";

  // Register the extension.
  CefRegisterExtension("v8/test", extensionCode, NULL);

  DxUIClientRendererDelegate::Set::iterator it = renderDelegates.begin();
  for (; it != renderDelegates.end(); ++it) (*it)->onWebKitInitialized(this);
}

void dxui::DxUIClient::OnBrowserCreated(CefRefPtr<CefBrowser> browser)
{
  DxUIClientRendererDelegate::Set::iterator it = renderDelegates.begin();
  for (; it != renderDelegates.end(); ++it) (*it)->onBrowserCreated(this, browser);
}

void dxui::DxUIClient::OnBrowserDestroyed(CefRefPtr<CefBrowser> browser)
{
  DxUIClientRendererDelegate::Set::iterator it = renderDelegates.begin();
  for (; it != renderDelegates.end(); ++it) (*it)->onBrowserDestroyed(this, browser);
}

bool dxui::DxUIClient::OnBeforeNavigation(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, NavigationType navigation_type, bool is_redirect)
{
  DxUIClientRendererDelegate::Set::iterator it = renderDelegates.begin();
  for (; it != renderDelegates.end(); ++it)
  {
    if ((*it)->onBeforeNavigation(this, browser, frame, request, navigation_type, is_redirect))
    {
      return true;
    }
  }

  return false;
}

void dxui::DxUIClient::OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context)
{
  using namespace internal;
  CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create("$@(renderer) context created");
  browser->SendProcessMessage(PID_BROWSER, message);

  // window v8 bindings

  if (context)
  {
    CefRefPtr<CefV8Value> v8window = context->GetGlobal();
    CefRefPtr<DxUIV8Handler> v8UIHandler = new DxUIV8Handler(this);
    CefRefPtr<CefV8Value> v8eventFunction = CefV8Value::CreateFunction(v8UIHandler->dxuiSendEvent, v8UIHandler);
    CefRefPtr<CefV8Value> v8onShowFunction = CefV8Value::CreateFunction(v8UIHandler->dxuiOnShow, v8UIHandler);
    CefRefPtr<CefV8Value> v8onHideFunction = CefV8Value::CreateFunction(v8UIHandler->dxuiOnHide, v8UIHandler);
    v8window->SetValue(v8UIHandler->dxuiSendEvent, v8eventFunction, V8_PROPERTY_ATTRIBUTE_NONE);
    v8window->SetValue(v8UIHandler->dxuiOnShow, v8onShowFunction, V8_PROPERTY_ATTRIBUTE_NONE);
    v8window->SetValue(v8UIHandler->dxuiOnHide, v8onHideFunction, V8_PROPERTY_ATTRIBUTE_NONE);

    implDetails = getClientDetails();
    implDetails->v8Handler = v8UIHandler;
    isContextAvailable = true;

    CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create("$@(renderer) dxui callbacks were registered");
    browser->SendProcessMessage(PID_BROWSER, message);
  }

  DxUIClientRendererDelegate::Set::iterator it = renderDelegates.begin();
  for (; it != renderDelegates.end(); ++it) (*it)->onContextCreated(this, browser, frame, context);
}

void dxui::DxUIClient::OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context)
{
  isContextAvailable = false;

  // release all references
  implDetails->v8Handler->dxuiOnHideContext = 0;
  implDetails->v8Handler->dxuiOnShowContext = 0;
  implDetails->v8Handler->dxuiOnHideCallback = 0;
  implDetails->v8Handler->dxuiOnShowCallback = 0;
  implDetails->v8Handler = 0;
  dx::safeDelete(implDetails);

  DxUIClientRendererDelegate::Set::iterator it = renderDelegates.begin();
  for (; it != renderDelegates.end(); ++it) (*it)->onContextReleased(this, browser, frame, context);
}

void dxui::DxUIClient::OnUncaughtException(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context, CefRefPtr<CefV8Exception> exception, CefRefPtr<CefV8StackTrace> stackTrace)
{
  DxUIClientRendererDelegate::Set::iterator it = renderDelegates.begin();
  for (; it != renderDelegates.end(); ++it) (*it)->onUncaughtException(this, browser, frame, context, exception, stackTrace);
}

void dxui::DxUIClient::OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefDOMNode> node)
{
  DxUIClientRendererDelegate::Set::iterator it = renderDelegates.begin();
  for (; it != renderDelegates.end(); ++it) (*it)->onFocusedNodeChanged(this, browser, frame, node);
}

bool dxui::DxUIClient::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message)
{
  if (source_process == PID_RENDERER)
  {
    std::string eventName = message->GetName().ToString();
    if (eventName[0] == '$')
    {
      if (eventName[1] == 'e')
      {
        // dxui event
        dxui::trace("v8:dxui: event");
      }
      else if (eventName[1] == '@')
      {
        dxui::trace("v8:dxui:verbose: %s", eventName.c_str() + 2);
      }
      return true;
    }
  }
  else if (source_process == PID_BROWSER)
  {
    std::string eventName = message->GetName().ToString();
    if (eventName[0] == '$')
    {
      if (eventName[1] == '<')
      {
        if (isContextAvailable)
          implDetails->v8Handler->sendV8Show();
      }
      else if (eventName[1] == '>')
      {
        if (isContextAvailable)
          implDetails->v8Handler->sendV8Hide();
      }

      return true;
    }

  }

  return false;
}
