#include "DxUIV8Handlers.h"

bool dxui::DxUIV8Handler::Execute(
  const CefString& name,
  CefRefPtr<CefV8Value> object,
  const CefV8ValueList& arguments,
  CefRefPtr<CefV8Value>& retval,
  CefString& exception
  )
{
  std::string n = name.ToString();
  dxui::trace("dxui:v8: %s (%u nargs)", n.c_str(), arguments.size());
  retval = CefV8Value::CreateNull();
  return true;
}