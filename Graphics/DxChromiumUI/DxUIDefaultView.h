#pragma once
#include <sal.h>
#include <DxUIDefaultCamera.h>

namespace dxui
{
  class DxUIDefaultView
    : public DxUIDefaultCamera
  {
    float viewStartX; // indicates x-coordinate where surface starts from in screen space

  public:
    float 
      surfaceW, // surface width
      surfaceHiddenW; // 10px by default - most left 10-pixels-width-column of the ui page is still visible it is hidden
    float 
      offsetShown, // offsets for translating ui surface when it is visible
      offsetCurrent, // offsets for translating ui surface when it is visible
      offsetHidden; // offsets for translating ui surface when it is invisible
    DirectX::Matrix 
      poseShown, // actual translation matrices for ui surface when it is visible
      poseCurrent, // actual translation matrices for ui surface when it is visible
      poseHidden; // actual translation matrices for ui surface when it is invisible

  public:
    DxUIDefaultView(void);
    /// <summary>
    /// Update translation matrices for surface.
    /// </summary>
    void updatePoses(void);
    /// <summary>
    /// Update current translation matrices for surface.
    /// </summary>
    void updateCurrentPose(void);
    /// <summary>
    /// Casts mouse pointer position to ui rect position.
    /// Returns false if pointer does not hover over ui rect, 
    /// however in case it does, the provided values will be changed.
    /// </summary>
    bool castScreenToUI(_Inout_ DirectX::Vector2 &screenPos);

  };
}