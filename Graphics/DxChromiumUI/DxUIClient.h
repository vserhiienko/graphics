#pragma once
#include <DxDelegates.h>
#include <DxUICore.h>
#include <DxUIClientDelegates.h>
#include <DxUIDeviceResources.h>

namespace dxui
{
  class DxUIClient
    : public CefApp
    , public CefClient
    , public CefRenderHandler
    , public CefRequestHandler
    , public CefDisplayHandler
    , public CefLifeSpanHandler
    , public CefRenderProcessHandler
    , public CefBrowserProcessHandler
  {
  public:
    typedef std::set<CefMessageRouterBrowserSide::Handler*> MessageHandlerSet;
    typedef dx::Event<void, DxUIClient*> Event;
    // Handles the browser side of query routing. The renderer side is handled in client_renderer.cpp.
    typedef CefRefPtr<CefMessageRouterBrowserSide> MsgRouterBSCefRefPtr;
    typedef CefRefPtr<dxui::DxUIClient> Pointer;

  public: // CefApp
    virtual void OnRegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar> registrar) override;
    virtual CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() override;
    virtual CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() override;
  public: // CefClient
    virtual CefRefPtr<CefDisplayHandler> GetDisplayHandler() override;
    virtual CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() override;
    virtual CefRefPtr<CefRenderHandler> GetRenderHandler() override;
    virtual CefRefPtr<CefRequestHandler> GetRequestHandler() override;
  public: // CefRenderHandler methods
    virtual bool GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override;
    virtual bool GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override;
    virtual bool GetScreenPoint(CefRefPtr<CefBrowser> browser, int viewX, int viewY, int& screenX, int& screenY) override;
    virtual void OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height) override;
  public: // CefRequestHandler methods
    virtual bool OnBeforeBrowse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, bool is_redirect) override;
    virtual bool OnQuotaRequest(CefRefPtr<CefBrowser> browser, const CefString& origin_url, int64 new_size, CefRefPtr<CefQuotaCallback> callback) override;
    virtual void OnProtocolExecution(CefRefPtr<CefBrowser> browser, const CefString& url, bool& allow_os_execution) override;
    virtual void OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser, TerminationStatus status) override;
  public: // CefDisplayHandler
    virtual void OnAddressChange(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& url) override;
    virtual void OnTitleChange(CefRefPtr<CefBrowser> browser, const CefString& title) override;
    virtual bool OnConsoleMessage(CefRefPtr<CefBrowser> browser, const CefString& message, const CefString& source, int line) override;
  public: // CefLifeSpanHandler methods
    virtual bool DoClose(CefRefPtr<CefBrowser> browser) override;
    virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser) override;
    virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser) override;
    virtual bool OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name, const CefPopupFeatures& popupFeatures, CefWindowInfo& windowInfo, CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access) override;
  public: // CefBrowserProcessHandler
    virtual void OnContextInitialized() override;
    virtual void OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line) override;
    virtual void OnRenderProcessThreadCreated(CefRefPtr<CefListValue> extra_info) override;
  public: // CefRenderProcessHandler
    virtual void OnRenderThreadCreated(CefRefPtr<CefListValue> extra_info) override;
    virtual void OnWebKitInitialized() override;
    virtual void OnBrowserCreated(CefRefPtr<CefBrowser> browser) override;
    virtual void OnBrowserDestroyed(CefRefPtr<CefBrowser> browser) override;
    virtual bool OnBeforeNavigation(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, NavigationType navigation_type, bool is_redirect) override;
    virtual void OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) override;
    virtual void OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) override;
    virtual void OnUncaughtException(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context, CefRefPtr<CefV8Exception> exception, CefRefPtr<CefV8StackTrace> stackTrace) override;
    virtual void OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefDOMNode> node) override;
    virtual bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message) override;

  public:
    DxUIClient(dx::DxDeviceResources *d3d, std::string startupUrl);
    virtual ~DxUIClient();
    static bool initializeApplication(DxUIClient::Pointer uiClient);
    static void shutdownApplication(void);
    static void updateApplication(void);
    static bool launchUI(DxUIClient::Pointer uiClient, bool allowTransparency = true);

    void onUIShow(void);
    void onUIHide(void);

  public:
    mutable Event browserCreated;
    mutable Event contextCreated;
    mutable Event browserDestoyed;
    mutable Event contextDestroyed;
    mutable Event createMessageHandlers;
    mutable Event createBrowserDelegates;
    mutable Event createRenderDelegates;
    mutable Event registerCustomSchemes;

    uint64 allowedQuota;
    std::string startupUrl;
    DxUIDeviceResources uiResources;
    CefRefPtr<CefBrowser> browser; // The child browser window.
    std::vector<CefString> cookieableSchemes; // Schemes that will be registered with the global cookie manager. Used in both the browser and renderer process.
    DxUIClientBrowserDelegate::Set browserDelegates; // Set of supported BrowserDelegates. Only used in the browser process.
    DxUIClientRendererDelegate::Set renderDelegates; // Set of supported RenderDelegates. Only used in the renderer process.

  private:
    class DXUIClientPrivate;
    friend DXUIClientPrivate;

    bool isContextAvailable;
    DXUIClientPrivate *implDetails;
    DXUIClientPrivate *getClientDetails();

    //
    // UI THREAD ACCESS ONLY MEMBERS
    //
    MsgRouterBSCefRefPtr messageRouter; // Handles the browser side of query routing. The renderer side is handled in client_renderer.cpp.
    MessageHandlerSet messageHandlerSet; // Set of Handlers registered with the message router.
    //
    // LOCK PROTECTED MEMBERS
    //
    mutable base::Lock lock; // Lock used to protect members accessed on multiple threads. Make it mutable so that it can be used from const methods.
    int browserId; // The child browser id.
    bool isClosing; // True if the main browser window is currently closing.
    //
    static int s_browser_count; // Number of currently existing browser windows. The application will exit when the number of windows reaches 0.
    //
    IMPLEMENT_REFCOUNTING(DxUIClient);
  };
}