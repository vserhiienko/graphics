#pragma once
#include <DxUICore.h>

namespace dxui
{
  class DxUIClient;
  // Interface for browser delegates. All BrowserDelegates must be returned via CreateBrowserDelegates. Do not perform work in the BrowserDelegate constructor. See CefBrowserProcessHandler for documentation.
  class DxUIClientBrowserDelegate : public virtual CefBase
  {
  public:
    typedef std::set<CefRefPtr<DxUIClientBrowserDelegate>> Set;
  public:
    virtual void onContextInitialized(CefRefPtr<DxUIClient> app) {}
    virtual void onBeforeChildProcessLaunch(CefRefPtr<DxUIClient> app, CefRefPtr<CefCommandLine> command_line) {}
    virtual void onRenderProcessThreadCreated(CefRefPtr<DxUIClient> app, CefRefPtr<CefListValue> extra_info) {}
  };
  // Interface for renderer delegates. All RenderDelegates must be returned via CreateRenderDelegates.
  class DxUIClientRendererDelegate : public CefBase
  {
  public:
    typedef std::set<CefRefPtr<DxUIClientRendererDelegate>> Set;
  public:
    virtual void onWebKitInitialized(CefRefPtr<DxUIClient> app) {}
    virtual void onBrowserCreated(CefRefPtr<DxUIClient> app, CefRefPtr<CefBrowser> browser) {}
    virtual void onBrowserDestroyed(CefRefPtr<DxUIClient> app, CefRefPtr<CefBrowser> browser) {}
    virtual void onRenderThreadCreated(CefRefPtr<DxUIClient> app, CefRefPtr<CefListValue> extra_info) {}
    virtual bool onBeforeNavigation(CefRefPtr<DxUIClient> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, cef_navigation_type_t navigation_type, bool is_redirect) { return false; }
    virtual void onContextCreated(CefRefPtr<DxUIClient> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) {}
    virtual void onContextReleased(CefRefPtr<DxUIClient> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) {}
    virtual void onUncaughtException(CefRefPtr<DxUIClient> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context, CefRefPtr<CefV8Exception> exception, CefRefPtr<CefV8StackTrace> stackTrace) {}
    virtual void onFocusedNodeChanged(CefRefPtr<DxUIClient> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefDOMNode> node) {}
    // Called when a process message is received. Return true if the message was handled and should not be passed on to other handlers. RenderDelegates should check for unique message names to avoid interfering with each other.
    virtual bool onProcessMessageReceived(CefRefPtr<DxUIClient> app, CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message) { return false; }
  };
}