#include "DxUIClient.h"

int dxui::DxUIClient::s_browser_count;

// CefLifeSpanHandler methods
bool dxui::DxUIClient::DoClose(CefRefPtr<CefBrowser> browser)
{
  CEF_REQUIRE_UI_THREAD();

  // Closing the main window requires special handling. See the DoClose()
  // documentation in the CEF header for a detailed destription of this
  // process.
  if (browserId == browser->GetIdentifier())
  {
    base::AutoLock lock_scope(this->lock);

    // Set a flag to indicate that the window close should be allowed.
    isClosing = true;
  }

  // Allow the close. For windowed browsers this will result in the OS close
  // event being sent.
  return false;
}

void dxui::DxUIClient::OnBeforeClose(CefRefPtr<CefBrowser> browser)
{
  CEF_REQUIRE_UI_THREAD();

  messageRouter->OnBeforeClose(browser);

  if (this->browserId == browser->GetIdentifier())
  {
    {
      base::AutoLock lock_scope(this->lock);
      // Free the browser pointer so that the browser can be destroyed
      this->browser = NULL;
    }

    /*if (m_osr_handler.get()) {
      m_osr_handler->OnBeforeClose(browser);
      m_osr_handler = NULL;
      }*/
  }
  else if (browser->IsPopup())
  {
    return;

    // Remove from the browser popup list.
    /* BrowserList::iterator bit = m_popup_browsers.begin();
     for (; bit != m_popup_browsers.end(); ++bit)
     {
     if ((*bit)->IsSame(browser))
     {
     m_popup_browsers.erase(bit);
     break;
     }
     }*/
  }

  if (--s_browser_count == 0)
  {
    // All browser windows have closed.
    // Remove and delete message router handlers.
    MessageHandlerSet::const_iterator it = messageHandlerSet.begin();
    for (; it != messageHandlerSet.end(); ++it)
    {
      messageRouter->RemoveHandler(*(it));
      delete *(it);
    }

    messageHandlerSet.clear();
    messageRouter = NULL;

    browserDestoyed(this);
    dxui::trace("dxui:client: all browsers were closed");

    // Quit the application message loop.
    //AppQuitMessageLoop();
  }
}

void dxui::DxUIClient::OnAfterCreated(CefRefPtr<CefBrowser> browser)
{
  CEF_REQUIRE_UI_THREAD();

  if (!messageRouter) {
    // Create the browser-side router for query handling.
    CefMessageRouterConfig config;
    messageRouter = CefMessageRouterBrowserSide::Create(config);

    // Register handlers with the router.
    createMessageHandlers(this);
    MessageHandlerSet::const_iterator it = messageHandlerSet.begin();
    for (; it != messageHandlerSet.end(); ++it)
      messageRouter->AddHandler(*(it), false);
  }

  // Disable mouse cursor change if requested via the command-line flag.
  browser->GetHost()->SetMouseCursorChangeDisabled(true);

  if (!this->browser)
  {
    base::AutoLock lock_scope(this->lock);
    // We need to keep the main child window, but not popup windows
    this->browser = browser;
    this->browserId = browser->GetIdentifier();
    browserCreated(this);

    dxui::trace("dxui:client: browser id = %d", this->browserId);
  }
  else if (browser->IsPopup()) {
    browser->Release();
    // Add to the list of popup browsers.
    //m_popup_browsers.push_back(browser);

    // Give focus to the popup browser. Perform asynchronously because the
    // parent window may attempt to keep focus after launching the popup.
    //CefPostTask(TID_UI, base::Bind(&CefBrowserHost::SetFocus, browser->GetHost().get(), true));
    return;
  }

  s_browser_count++;
}

bool dxui::DxUIClient::OnBeforePopup(
  CefRefPtr<CefBrowser> browser,
  CefRefPtr<CefFrame> frame,
  const CefString& target_url,
  const CefString& target_frame_name,
  const CefPopupFeatures& popupFeatures,
  CefWindowInfo& windowInfo,
  CefRefPtr<CefClient>& client,
  CefBrowserSettings& settings,
  bool* no_javascript_access
  )
{
  CEF_REQUIRE_IO_THREAD();

  if (browser->GetHost()->IsWindowRenderingDisabled())
  {
    dxui::trace("dxui:client: cancelling popup");
    // Cancel popups in off-screen rendering mode.
    return true;
  }

  return false;
}
