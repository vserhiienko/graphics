#include "DxUIDefaultCamera.h"

dxui::DxUIDefaultCamera::DxUIDefaultCamera()
{
  screenW = 0.0f;
  screenH = 0.0f;
  farPlane = 100.0f;
  nearPlane = 0.01f;
  upDirection = DirectX::XMVectorSet(0.f, 1.f, 0.f, 1.f);
  eyePosition = DirectX::XMVectorSet(0.f, 0.f, -1.f, 1.f);
  focusPosition = DirectX::XMVectorSet(0.f, 0.f, +1.f, 1.f);
}

void dxui::DxUIDefaultCamera::updateView()
{
  view = DirectX::XMMatrixLookAtLH(eyePosition, focusPosition, upDirection);
}

void dxui::DxUIDefaultCamera::updateProjection()
{
  projection = DirectX::XMMatrixOrthographicLH(screenW, screenH, nearPlane, farPlane);
}