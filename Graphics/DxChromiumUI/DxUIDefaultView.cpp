#include "DxUIDefaultView.h"

dxui::DxUIDefaultView::DxUIDefaultView()
{
  surfaceHiddenW = 10.0f; // 10px
}
void dxui::DxUIDefaultView::updateCurrentPose()
{
  viewStartX = offsetCurrent + screenW / 2 - surfaceW / 2 - surfaceHiddenW;
  poseCurrent = DirectX::Matrix::CreateTranslation(offsetCurrent, 0, 0);
}
void dxui::DxUIDefaultView::updatePoses()
{
  viewStartX = screenW - surfaceW;
  offsetShown = viewStartX / 2.0f;
  offsetHidden = offsetShown + surfaceW;
  poseShown = DirectX::Matrix::CreateTranslation(offsetShown, 0, 0);
  poseHidden = DirectX::Matrix::CreateTranslation(offsetHidden, 0, 0);
}

bool dxui::DxUIDefaultView::castScreenToUI(DirectX::Vector2 &pos)
{
  if (pos.x >= viewStartX)
  {
    pos.x -= viewStartX + surfaceHiddenW;
    return true;
  }

  return false;
}
