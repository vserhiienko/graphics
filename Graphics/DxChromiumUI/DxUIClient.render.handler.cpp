#include "DxUIClient.h"

// CefRenderHandler methods
bool dxui::DxUIClient::GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
#if 0
  if (uiResources.d3d->host)
  {
    RECT windowRect;
    if (GetWindowRect(uiResources.d3d->host, &windowRect))
    {
      rect.x = windowRect.left;
      rect.y = windowRect.top;
      rect.width = windowRect.right - windowRect.left;
      rect.height = windowRect.bottom - windowRect.top;
      return true;
    }
  }
#endif

  return false;
}

bool dxui::DxUIClient::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
  if (uiResources.d3d)
  {
#if 0
    if (uiResources.d3d->host)
    {
      RECT windowRect;
      if (GetClientRect(uiResources.d3d->host, &windowRect))
      {
        rect.x = windowRect.left;
        rect.y = windowRect.top;
        rect.width = windowRect.right - windowRect.left;
        rect.height = windowRect.bottom - windowRect.top;
        return true;
      }
#else
    rect.x = 0;
    rect.y = 0;
    uiResources.getViewInfo(rect.width, rect.height);
    return true;
#endif

  }

  return false;
}

bool dxui::DxUIClient::GetScreenPoint(CefRefPtr<CefBrowser> browser, int viewX, int viewY, int& screenX, int& screenY)
{
#if 0
  if (uiResources.d3d->host)
  {
    POINT view;
    view.x = viewX;
    view.y = viewY;
    if (ClientToScreen(uiResources.d3d->host, &view))
    {
      screenX = view.x;
      screenY = view.y;
      return true;
    }
  }
#endif

  return false;
}

void dxui::DxUIClient::OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height)
{
  CEF_REQUIRE_UI_THREAD();
  uiResources.onPaint(type, dirtyRects, buffer, width, height);
}
