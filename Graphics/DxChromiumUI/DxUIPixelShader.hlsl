
struct PsInput
{
  float4 Position : SV_POSITION;
  float3 Normal : NORMAL;
  float2 Coords : TEXCOORD0;
};

Texture2D<float4> ColorTexture : register (t0);
SamplerState ColorSampler : register (s0);

float4 main(in PsInput input) : SV_TARGET
{
  //printf("ps : main()");
  float4 pelColor = ColorTexture.Sample(ColorSampler, input.Coords);
  //printf("Ps: [Color %.2f %.2f %.2f]", pelColor.x, pelColor.y, pelColor.z);
  return float4(pelColor.rgb, 1.0f);
}
