#pragma once
#include <DxSampleApp.h>

#ifdef GetNextSibling
#undef GetNextSibling
#endif
#ifdef GetFirstSibling
#undef GetFirstSibling
#endif
#ifdef GetFirstChild
#undef GetFirstChild
#endif

#include <DxAnim.h>
#include <DxUIClient.h>
#include <DxUIClientDelegates.h>
#include <DxUIDefaultRenderer.h>
#include <DxUIV8Handlers.h>

namespace dxui
{
  class DxUISampleApp
    : public dx::DxSampleApp
    , public dxui::DxUIClientRendererDelegate
  {
    IMPLEMENT_LOCKING(DxUISampleApp);
    IMPLEMENT_REFCOUNTING(DxUISampleApp);

  public: // dx::DxSampleApp
    virtual bool onMain(void);
    virtual bool onAppLoop(void);
    virtual void handleSizeChanged(dx::DxWindow const*);
    virtual void handleKeyPressed(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseWheel(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseMoved(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseLeftPressed(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseLeftReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseRightPressed(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseRightReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseMiddlePressed(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseMiddleReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);

  public:
    DxUISampleApp(bool showUI = false, bool autoShowHide = true, D3D_DRIVER_TYPE driver = D3D_DRIVER_TYPE_HARDWARE);
    virtual ~DxUISampleApp();

  public:
    virtual std::string getUIURL(void) = 0;
    virtual void initializeSample(void) = 0;
    virtual void sizeChanged(void) = 0;

    //
    // add some function to get JS handlers 
    //

  public:
    void updateUI(float elapsed);
    void renderUI(void);
    void show(void);
    void hide(void);

  public:
    inline virtual void onUICreated(void) {}
    inline virtual void onUIDestroyed(void) {}
    inline virtual void keyPressed(dx::DxGenericEventArgs const&) { }
    inline virtual void keyReleased(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerWheel(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerMoved(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerLeftPressed(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerLeftReleased(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerRightPressed(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerRightReleased(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerMiddlePressed(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerMiddleReleased(dx::DxGenericEventArgs const&) { }

  public:
    int desiredUISurfaceWidth;
    bool startShowingUI;
    bool autoShowHide;
    bool isUIActive;
    bool isBrowserAvailable;
    bool isJSContextAvailable;
    DirectX::Vector2 lastUIMousePos;

    dxui::DxUIClient::Pointer ui;
    dxui::DxUIDefaultRenderer uiRenderer;
    dxui::DxUIDefaultRenderer::PerFrame perCallInfo;
    dx::anim::ExponencialEasingFunctionPair uiEasing;

  private:
    void onBrowserCreated(DxUIClient*);
    void onBrowserDestroyed(DxUIClient*);
    void onContextInitialized(DxUIClient*);

  private:
    bool isUIHidden;
    float hideUISecs;
    float outsideUIRange;

  };

}