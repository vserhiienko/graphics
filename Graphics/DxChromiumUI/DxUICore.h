#pragma once

#include <set>
#include <iostream>
#include <Windows.h>
//#include <cef_app.h>
//#include <cef_browser.h>
//#include <cef_client.h>
//#include <cef_sandbox_win.h>
//#include <cef_frame.h>
//#include <cef_cookie.h>
//#include <cef_task.h>
//#include <cef_v8.h>
//#include <cef_process_message.h>
//#include <cef_stream.h>
//#include <cef_url.h>
//#include <cef_trace.h>
//#include <cef_path_util.h>
//#include <cef_process_util.h>
//#include <base/cef_lock.h>
//#include <base/cef_bind.h>
//#include <wrapper/cef_helpers.h>
//#include <wrapper/cef_closure_task.h>
//#include <wrapper/cef_message_router.h>
//#include <wrapper/cef_stream_resource_handler.h>

namespace dxui
{
  static void trace(const char* fmt, ...)
  {
    const int length = 512;
    char buffer[length];
    va_list ap;

    va_start(ap, fmt);
    vsnprintf_s(buffer, length - 1, fmt, ap);
    std::cout << buffer << std::endl;
    OutputDebugStringA(buffer);
    OutputDebugStringA("\n");
    va_end(ap);
  }
}