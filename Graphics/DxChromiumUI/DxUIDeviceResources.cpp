#include <DxUtils.h>
#include "DxUIDeviceResources.h"
dxui::DxUIDeviceResources::DxUIDeviceResources(dx::DxDeviceResources *device)
  : surfaceDesc(DXGI_FORMAT_B8G8R8A8_UNORM, 0, 0, 1, 1, D3D11_BIND_SHADER_RESOURCE, D3D11_USAGE_DEFAULT)
  , stagingSurfaceDesc(DXGI_FORMAT_B8G8R8A8_UNORM, 0, 0, 1, 1, 0, D3D11_USAGE_STAGING, D3D11_CPU_ACCESS_WRITE)
  , pitch(0)
  , slicePitch(0)
{
  onDeviceRestored(device);
}

void dxui::DxUIDeviceResources::onDeviceLost(dx::DxDeviceResources *lostDevice)
{
  dxui::trace("dxui:resources: device lost");

  d3d = 0;
}

void dxui::DxUIDeviceResources::setViewInfo(int width, int height)
{
  if (d3d) onSurfaceResized(0, width, height);
}

void dxui::DxUIDeviceResources::getViewInfo(int &width, int &height) const
{
  width = (int)stagingSurfaceDesc.Width;
  height = (int)stagingSurfaceDesc.Height;
}

void dxui::DxUIDeviceResources::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  dxui::trace("dxui:resources: device restored");

  if (restoredDevice)
  {
    d3d = restoredDevice;
    d3d->deviceLost += _Dx_delegate_to(this, &DxUIDeviceResources::onDeviceLost);
    d3d->deviceRestored += _Dx_delegate_to(this, &DxUIDeviceResources::onDeviceRestored);
    onSurfaceResized(0, (int)surfaceDesc.Width, (int)surfaceDesc.Height);
  }
}

void dxui::DxUIDeviceResources::onSurfaceResized(const void* buffer, int width, int height)
{
  if (width > 0 && height > 0)
  {
    dxui::trace("dxui:resources: surface resized");

    surfaceDesc.Width = (unsigned)width;
    surfaceDesc.Height = (unsigned)height;
    stagingSurfaceDesc.Width = surfaceDesc.Width;
    stagingSurfaceDesc.Height = surfaceDesc.Height;

    pitch = surfaceDesc.Width * 4;
    slicePitch = pitch * surfaceDesc.Height;

    dx::Subresource subresource;
    dx::Subresource *subresourceRef = 0;

    if (buffer)
    {
      subresource.pSysMem = buffer;
      subresource.SysMemPitch = pitch;
      subresource.SysMemSlicePitch = slicePitch;
      subresourceRef = &subresource;
    }

    {
      dx::HResult result;
      result = d3d->device->CreateTexture2D(&surfaceDesc, subresourceRef, surfaces[0].ReleaseAndGetAddressOf());
      result = d3d->device->CreateTexture2D(&surfaceDesc, subresourceRef, surfaces[1].ReleaseAndGetAddressOf());
      result = d3d->device->CreateTexture2D(&stagingSurfaceDesc, subresourceRef, stagingSurface.ReleaseAndGetAddressOf());

      dx::CSRVDescription
        viewDesc0(surfaces[0].Get(), D3D11_SRV_DIMENSION_TEXTURE2D, surfaceDesc.Format),
        viewDesc1(surfaces[1].Get(), D3D11_SRV_DIMENSION_TEXTURE2D, surfaceDesc.Format);
      result = d3d->device->CreateShaderResourceView(surfaces[0].Get(), &viewDesc0, shaderResourceViews[0].ReleaseAndGetAddressOf());
      result = d3d->device->CreateShaderResourceView(surfaces[1].Get(), &viewDesc1, shaderResourceViews[1].ReleaseAndGetAddressOf());
    }

  }
}

void dxui::DxUIDeviceResources::swapSurfaces()
{
  // note: using COM swaps

  surfaces[0].Swap(surfaces[1]);
  //std::swap(surfaces[0], surfaces[1]);
  shaderResourceViews[0].Swap(shaderResourceViews[1]);
  //std::swap(shaderResourceViews[0], shaderResourceViews[1]);
}

void dxui::DxUIDeviceResources::onPaint(
  CefRenderHandler::PaintElementType type,
  const CefRenderHandler::RectList& dirtyRects,
  const void* buffer,
  int width,
  int height
  )
{
  if (d3d && buffer && dirtyRects.size()) switch (type)
  {
  case PET_VIEW:
  {
    if (surfaceDesc.Width != (unsigned)width || surfaceDesc.Height != (unsigned)height)
    {
      onSurfaceResized(buffer, width, height);
    }
    else
    {
      dx::Box dirtyRegion;
      dx::MappedSubresource mapped;

#if 0

      // this approach should be fixed if necessary, it does not work

      for (unsigned rectInd = 0; rectInd < dirtyRects.size(); rectInd++)
      {
        dirtyRegion.front = 0;
        dirtyRegion.back = 1;
        dirtyRegion.left = dirtyRects[rectInd].x;
        dirtyRegion.top = dirtyRects[rectInd].y;
        dirtyRegion.right = dirtyRects[rectInd].width + dirtyRects[rectInd].x;
        dirtyRegion.bottom = dirtyRects[rectInd].height + dirtyRects[rectInd].y;
        d3d->context->UpdateSubresource(surfaces->Get(), 0, &dirtyRegion, buffer, 0, 0);
      }

#else

      // it appears that this function always passes one window rect
      // the suggested approach according to http://eatplayhate.me/2013/09/29/d3d11-texture-update-costs/ 
      // + double buffering is implemented

      dirtyRegion.front = 0;
      dirtyRegion.back = 1;
      dirtyRegion.left = 0;
      dirtyRegion.top = 0;
      dirtyRegion.right = width;
      dirtyRegion.bottom = height;

      // note: D3D11_MAP_WRITE_DISCARD flag does not work for staging resources
      if (SUCCEEDED(d3d->context->Map(stagingSurface.Get(), 0, D3D11_MAP_WRITE, 0, &mapped)))
      {
        memcpy(mapped.pData, buffer, slicePitch);
        d3d->context->Unmap(stagingSurface.Get(), 0);
        d3d->context->CopySubresourceRegion(surfaces[0].Get(), 0, 0, 0, 0, stagingSurface.Get(), 0, &dirtyRegion);
      }

#endif

      swapSurfaces();
    }
  } break;
  }
}

dx::SRView &dxui::DxUIDeviceResources::getShaderResourceView()
{
  return shaderResourceViews[1]; // returns updated resource
}


