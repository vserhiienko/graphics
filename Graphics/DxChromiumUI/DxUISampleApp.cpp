#include <DxUtils.h>
#include "DxUISampleApp.h"

namespace dxui
{
  int _cdecl onDxUISampleAppExit()
  {
    return 0;
  }
}

dxui::DxUISampleApp::DxUISampleApp(bool showUI, bool autoShowHide, D3D_DRIVER_TYPE driver)
  : DxSampleApp(driver)
  , uiRenderer(&deviceResources)
  , autoShowHide(autoShowHide)
  , startShowingUI(showUI)
{
  hideUISecs = 0.4f;
  isUIActive = false;
  outsideUIRange = 0.0f;
  isBrowserAvailable = false;
}

dxui::DxUISampleApp::~DxUISampleApp()
{
  dxui::DxUIClient::shutdownApplication();
  ui = nullptr;
}

void dxui::DxUISampleApp::show()
{
  if (isBrowserAvailable)
  {
    ui->browser->GetHost()->SetFocus(true);
    ui->browser->GetHost()->WasHidden(false);
    ui->browser->GetHost()->SetWindowVisibility(true);
    ui->onUIShow();
  }

  uiEasing.resume();
}

void dxui::DxUISampleApp::hide()
{
  if (isBrowserAvailable)
  {
    ui->browser->GetHost()->SetFocus(false);
    ui->browser->GetHost()->WasHidden(true);
    ui->browser->GetHost()->SetWindowVisibility(false);
    ui->onUIHide();
  }

  uiEasing.suspend();
}

void dxui::DxUISampleApp::updateUI(float elapsed)
{
  dxui::DxUIClient::updateApplication();

  if (autoShowHide)
  {
    if (!isUIActive)
    {
      outsideUIRange += elapsed;
      if (outsideUIRange >= hideUISecs && !isUIHidden)
      {
        isUIHidden = true;
        hide();
      }
    }
    else if (isUIHidden && uiEasing.currentStage == dx::anim::EasingStage::kSuspended)
    {
      outsideUIRange = 0.0f;
      isUIHidden = false;
      show();
    }
  }

  uiEasing += elapsed;
  uiRenderer.surfaceView.offsetCurrent = uiEasing.onFrame();
  uiRenderer.surfaceView.updateCurrentPose();
  perCallInfo.surfaceWorld = uiRenderer.surfaceView.poseCurrent;
  uiRenderer.update(perCallInfo);
}

void dxui::DxUISampleApp::renderUI()
{
  uiRenderer.render(ui->uiResources);
}

bool dxui::DxUISampleApp::onMain(void)
{
  ui = new dxui::DxUIClient(&deviceResources, getUIURL());
  ui->browserCreated += _Dx_delegate_to(this, &DxUISampleApp::onBrowserCreated);
  ui->contextCreated += _Dx_delegate_to(this, &DxUISampleApp::onContextInitialized);
  ui->browserDestoyed += _Dx_delegate_to(this, &DxUISampleApp::onBrowserDestroyed);

  if (dxui::DxUIClient::initializeApplication(ui))
  {
    dx::DxSampleApp::onMain();
    return true;
  }
  else
  {
    ui = nullptr;
    return false;
  }
}

bool dxui::DxUISampleApp::onAppLoop(void)
{
  int screenW = (int)uiRenderer.d3d->actualRenderTargetSize.Width;
  int screenH = (int)uiRenderer.d3d->actualRenderTargetSize.Height;
  int fixedSurfaceW = dx::roundUp(screenW / 3, 16);
  ui->uiResources.setViewInfo(screenW, screenH);
  //ui->uiResources.setViewInfo(fixedSurfaceW, screenH);

  if (dxui::DxUIClient::launchUI(ui))
  {
    int viewX, viewY;
    ui->uiResources.getViewInfo(viewX, viewY);
    uiRenderer.surfaceView.surfaceW = (float)viewX;
    uiRenderer.surfaceView.screenW = uiRenderer.d3d->actualRenderTargetSize.Width;
    uiRenderer.surfaceView.screenH = uiRenderer.d3d->actualRenderTargetSize.Height;

    uiRenderer.surfaceView.updateView();
    uiRenderer.surfaceView.updateProjection();
    uiRenderer.createDeviceResources(ui->uiResources);

    perCallInfo.surfaceWorld = uiRenderer.surfaceView.poseShown;
    perCallInfo.cameraView = uiRenderer.surfaceView.view;
    perCallInfo.cameraProjection = uiRenderer.surfaceView.projection;

    float distance = uiRenderer.surfaceView.offsetHidden - uiRenderer.surfaceView.offsetShown;

    uiEasing.suspendingValue.params.offset = uiRenderer.surfaceView.offsetShown;
    uiEasing.resumingValue.params.offset = uiRenderer.surfaceView.offsetHidden;
    uiEasing.suspendingValue.params.distance = distance;
    uiEasing.resumingValue.params.distance = -distance;
    uiEasing.suspendingValue.params.total = 0.4f;
    uiEasing.resumingValue.params.total = 0.4f;

    if (startShowingUI)
      uiEasing.resume();

    initializeSample();
  }

  return true;
}

void dxui::DxUISampleApp::handleSizeChanged(dx::DxWindow const*)
{
  if (isBrowserAvailable)
  {
    ui->browser->GetHost()->WasResized();
  }

  sizeChanged();
}

void dxui::DxUISampleApp::handleKeyPressed(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  if (isBrowserAvailable && isUIActive)
  {
    CefKeyEvent keyEvent;
    keyEvent.windows_key_code = args.keyCode();
    keyEvent.type = cef_key_event_type_t::KEYEVENT_KEYDOWN;
    ui->browser->GetHost()->SendKeyEvent(keyEvent);
  }
  else
  {
    keyPressed(args);
  }
}

void dxui::DxUISampleApp::handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  if (isBrowserAvailable && isUIActive)
  {
    CefKeyEvent keyEvent;
    keyEvent.windows_key_code = args.keyCode();
    keyEvent.type = cef_key_event_type_t::KEYEVENT_KEYUP;
    ui->browser->GetHost()->SendKeyEvent(keyEvent);
  }
  else
  {
    keyReleased(args);
  }
}

void dxui::DxUISampleApp::handleMouseWheel(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  if (isBrowserAvailable && isUIActive)
  {
    CefMouseEvent e;
    e.x = int(lastUIMousePos.x);
    e.y = int(lastUIMousePos.y);
    ui->browser->GetHost()->SendMouseWheelEvent(e, 0, args.wheelDelta());
  }
  else
  {
    pointerWheel(args);
  }
}

void dxui::DxUISampleApp::handleMouseMoved(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  DirectX::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  if (isBrowserAvailable && uiRenderer.surfaceView.castScreenToUI(mousePos))
  {
    CefMouseEvent e;
    lastUIMousePos = mousePos;
    e.x = int(lastUIMousePos.x);
    e.y = int(lastUIMousePos.y);
    ui->browser->GetHost()->SendMouseMoveEvent(e, false);
    isUIActive = true;
  }
  else
  {
    pointerMoved(args);
    isUIActive = false;
  }
}

void dxui::DxUISampleApp::handleMouseLeftPressed(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  DirectX::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  if (isBrowserAvailable && uiRenderer.surfaceView.castScreenToUI(mousePos))
  {
    CefMouseEvent e;
    e.x = int(mousePos.x);
    e.y = int(mousePos.y);
    ui->browser->GetHost()->SendMouseClickEvent(e, CefBrowserHost::MouseButtonType::MBT_LEFT, false, 1);
  }
  else
  {
    pointerLeftPressed(args);
  }
}

void dxui::DxUISampleApp::handleMouseLeftReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  DirectX::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  if (isBrowserAvailable && uiRenderer.surfaceView.castScreenToUI(mousePos))
  {
    CefMouseEvent e;
    e.x = int(mousePos.x);
    e.y = int(mousePos.y);
    ui->browser->GetHost()->SendMouseClickEvent(e, CefBrowserHost::MouseButtonType::MBT_LEFT, true, 1);
  }
  else
  {
    pointerLeftReleased(args);
  }
}

void dxui::DxUISampleApp::handleMouseRightPressed(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  DirectX::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  if (isBrowserAvailable && uiRenderer.surfaceView.castScreenToUI(mousePos))
  {
    CefMouseEvent e;
    e.x = int(mousePos.x);
    e.y = int(mousePos.y);
    ui->browser->GetHost()->SendMouseClickEvent(e, CefBrowserHost::MouseButtonType::MBT_RIGHT, false, 1);
  }
  else
  {
    pointerRightPressed(args);
  }
}

void dxui::DxUISampleApp::handleMouseRightReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  DirectX::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  if (isBrowserAvailable && uiRenderer.surfaceView.castScreenToUI(mousePos))
  {
    CefMouseEvent e;
    e.x = int(mousePos.x);
    e.y = int(mousePos.y);
    ui->browser->GetHost()->SendMouseClickEvent(e, CefBrowserHost::MouseButtonType::MBT_RIGHT, true, 1);
  }
  else
  {
    pointerRightReleased(args);
  }
}

void dxui::DxUISampleApp::handleMouseMiddlePressed(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  DirectX::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  if (isBrowserAvailable && uiRenderer.surfaceView.castScreenToUI(mousePos))
  {
    CefMouseEvent e;
    e.x = int(mousePos.x);
    e.y = int(mousePos.y);
    ui->browser->GetHost()->SendMouseClickEvent(e, CefBrowserHost::MouseButtonType::MBT_MIDDLE, false, 1);
  }
  else
  {
    pointerMiddlePressed(args);
  }
}

void dxui::DxUISampleApp::handleMouseMiddleReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  DirectX::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  if (isBrowserAvailable && uiRenderer.surfaceView.castScreenToUI(mousePos))
  {
    CefMouseEvent e;
    e.x = int(mousePos.x);
    e.y = int(mousePos.y);
    ui->browser->GetHost()->SendMouseClickEvent(e, CefBrowserHost::MouseButtonType::MBT_MIDDLE, true, 1);
  }
  else
  {
    pointerMiddleReleased(args);
  }
}

void dxui::DxUISampleApp::onBrowserCreated(DxUIClient*)
{
  dxui::trace("dxui:sample: browser created");
  isBrowserAvailable = true;

  onUICreated();
}

void dxui::DxUISampleApp::onBrowserDestroyed(DxUIClient*)
{
  dxui::trace("dxui:sample: browser destroyed");
  isBrowserAvailable = false;
  onUIDestroyed();
}

void dxui::DxUISampleApp::onContextInitialized(DxUIClient*)
{
  dxui::trace("dxui:sample: context created");
  isJSContextAvailable = true;
}
