#pragma once
#include <DxMath.h>

namespace dxui
{
  class DxUIDefaultCamera
  {
  protected:
    float nearPlane, farPlane;
    DirectX::Vector4 upDirection;
    DirectX::Vector4 eyePosition;
    DirectX::Vector4 focusPosition;

  public:
    DirectX::Matrix view;
    DirectX::Matrix projection;

  public:
    float screenW, screenH;

  public:
    DxUIDefaultCamera(void);

  public:
    void updateView(void);
    void updateProjection(void);

  };
}