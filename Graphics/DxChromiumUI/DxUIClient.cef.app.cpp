#include <DxUtils.h>
#include "DxUIClient.h"

CefRefPtr<CefRenderProcessHandler> dxui::DxUIClient::GetRenderProcessHandler()  { return this; }
CefRefPtr<CefBrowserProcessHandler> dxui::DxUIClient::GetBrowserProcessHandler()  { return this; }

dxui::DxUIClient::DxUIClient(dx::DxDeviceResources *d3d, std::string startupUrl)
  : uiResources(d3d)
  , allowedQuota(100)
  , implDetails(0)
  , isContextAvailable(false)
  , startupUrl(startupUrl)
{
  dxui::trace("dxui:client: creating web client app");
}

dxui::DxUIClient::~DxUIClient()
{
  dx::safeDelete(implDetails);
}

void dxui::DxUIClient::OnRegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar> registrar)
{
  dxui::trace("dxui:client: registering custom schemes");

  // Default schemes that support cookies.
  cookieableSchemes.push_back("http");
  cookieableSchemes.push_back("https");
  registerCustomSchemes(this);
}

void dxui::DxUIClient::OnRenderThreadCreated(CefRefPtr<CefListValue> extra_info)
{
  dxui::trace("dxui:client: render thread created");

  createRenderDelegates(this);
  DxUIClientRendererDelegate::Set::iterator it = renderDelegates.begin();
  for (; it != renderDelegates.end(); ++it) (*it)->onRenderThreadCreated(this, extra_info);
}

bool dxui::DxUIClient::initializeApplication(DxUIClient::Pointer uiClient)
{
  void* sandboxInfo = NULL;
  CefMainArgs mainArgs(GetModuleHandle(NULL));
  int exitCode = CefExecuteProcess(mainArgs, uiClient, sandboxInfo);
  return exitCode == -1;
}

void dxui::DxUIClient::shutdownApplication()
{
  CefShutdown();
}

void dxui::DxUIClient::updateApplication()
{
  CefDoMessageLoopWork();
}

bool dxui::DxUIClient::launchUI(DxUIClient::Pointer uiClient, bool allowTransparency)
{
  CefRefPtr<CefCommandLine> commandLine = CefCommandLine::CreateCommandLine();
  commandLine->InitFromString(::GetCommandLine());

  void* sandboxInfo = NULL;

  CefSettings settings;
  settings.no_sandbox = !sandboxInfo;
  //settings.single_process = true;
  settings.multi_threaded_message_loop = false;
  settings.windowless_rendering_enabled = true;

  CefWindowInfo info;
  info.parent_window = uiClient->uiResources.d3d->host;
  info.windowless_rendering_enabled = true;
  info.transparent_painting_enabled = allowTransparency;

  CefBrowserSettings browserSettings;
  browserSettings.file_access_from_file_urls = STATE_ENABLED;

  if (CefInitialize(CefMainArgs(GetModuleHandle(NULL)), settings, uiClient, sandboxInfo))
  {
    CefBrowserHost::CreateBrowser(info, uiClient, uiClient->startupUrl, browserSettings, NULL);
    return true;
  }

  return false;
}
