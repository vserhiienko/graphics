
#include "DxUIDefaultRenderer.h"

#include <DxUtils.h>
#include <DxResourceManager.h>

dxui::DxUIDefaultRenderer::DxUIDefaultRenderer(dx::DxDeviceResources *d3dResources)
  : d3d(d3dResources)
{
  surfaceView.screenW = 0.0f;
  surfaceView.screenH = 0.0f;
  recreateDeviceResources = true;
  onDeviceRestored(d3dResources);
}

void dxui::DxUIDefaultRenderer::createDeviceResources(DxUIDeviceResources const &deviceResources)
{
  if (d3d)
  {
    dx::HResult result = S_OK;

    int viewX, viewY;
    deviceResources.getViewInfo(viewX, viewY);

    surfaceView.surfaceW = float(viewX);
    surfaceView.updatePoses();
    surfaceView.updateView();
    surfaceView.updateProjection();

    if (viewX > 0 && viewY > 0)
    {
      float
        w = (float)viewX * 0.5f,
        h = (float)viewY * 0.5f;
      Vertex squareVertices[4] =
      {
        { DirectX::Vector4(-w, +h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(0, 0) },
        { DirectX::Vector4(+w, +h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(1, 0) },
        { DirectX::Vector4(+w, -h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(1, 1) },
        { DirectX::Vector4(-w, -h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(0, 1) },
      };
      Index squareIndices[6] =
      {
        0, 1, 2, 0, 2, 3,
      };
      result = dx::DxResourceManager::createVertexBuffer(
        d3d->device.Get(), ARRAYSIZE(squareVertices), squareVertices,
        vertexBuffer.ReleaseAndGetAddressOf()
        );
      result = dx::DxResourceManager::createIndexBuffer(
        d3d->device.Get(), ARRAYSIZE(squareIndices), squareIndices,
        indexBuffer.ReleaseAndGetAddressOf()
        );

      dxui::trace("dxui:render: vertex and index buffers created");
    }

    if (recreateDeviceResources)
    {
      dx::SamplerStateDescription samplerDesc;
      dx::RasterizerDescription rasterizerStateDesc;
      dx::BlendStateDescription blendStateDescription;
      dx::DepthStencilDescription depthDisabledStencilDesc;

      perFrameSource.surfaceWorld = DirectX::Matrix::Identity();


      dx::DxResourceManager::defaults(samplerDesc);
      samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
      samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
      samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

      dx::VSInputElementDescription layout_element[] =
      {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 16
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 28, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 12 (28)
      };

      dx::zeroMemory(blendStateDescription);
      blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
      blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
      blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
      blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
      blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
      blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
      blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
      blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;

      dx::zeroMemory(rasterizerStateDesc);
      rasterizerStateDesc.AntialiasedLineEnable = false;
      rasterizerStateDesc.CullMode = D3D11_CULL_BACK;
      rasterizerStateDesc.DepthBias = 0;
      rasterizerStateDesc.DepthBiasClamp = 0.0f;
      rasterizerStateDesc.DepthClipEnable = true;
      rasterizerStateDesc.FillMode = D3D11_FILL_SOLID;
      rasterizerStateDesc.FrontCounterClockwise = false;
      rasterizerStateDesc.MultisampleEnable = false;
      rasterizerStateDesc.ScissorEnable = false;
      rasterizerStateDesc.SlopeScaledDepthBias = 0.0f;

      dx::zeroMemory(depthDisabledStencilDesc);
      dx::DxResourceManager::defaults(depthDisabledStencilDesc);
      depthDisabledStencilDesc.DepthEnable = false;
      depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
      depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
      depthDisabledStencilDesc.StencilEnable = true;
      depthDisabledStencilDesc.StencilReadMask = 0xff;
      depthDisabledStencilDesc.StencilWriteMask = 0xff;
      depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
      depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
      depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
      depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
      depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
      depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
      depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
      depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

      dx::DxAssetManager::addSearchPath("DxChromiumUI");
      result = dx::DxResourceManager::loadShader(
        d3d->device.Get(),
        L"shaders/DxUIPixelShader.cso",
        pixelShader.ReleaseAndGetAddressOf()
        );
      result = dx::DxResourceManager::loadShader(
        d3d->device.Get(),
        L"shaders/DxUIVertexShader.cso",
        layout_element,
        ARRAYSIZE(layout_element),
        vertexShader.ReleaseAndGetAddressOf(),
        inputLayout.ReleaseAndGetAddressOf()
        );
      result = d3d->device->CreateSamplerState(
        &samplerDesc, samplerState.ReleaseAndGetAddressOf()
        );
      result = d3d->device.Get()->CreateDepthStencilState(
        &depthDisabledStencilDesc,
        depthStencilState.ReleaseAndGetAddressOf()
        );
      result = d3d->device.Get()->CreateRasterizerState1(
        &rasterizerStateDesc,
        rasterizerState.ReleaseAndGetAddressOf()
        );
      result = d3d->device.Get()->CreateBlendState1(
        &blendStateDescription,
        blendState.ReleaseAndGetAddressOf()
        );
      result = perFrameBuffer.create(
        d3d->device.Get(),
        "dxui:default:render:cbuffer"
        );

      dxui::trace("dxui:render: device resources created");
      recreateDeviceResources = false;
    }
  }
}


void dxui::DxUIDefaultRenderer::update(PerFrame const &perCallInfo)
{
  perFrameBuffer.setData(d3d->context.Get(), perCallInfo);
}

void dxui::DxUIDefaultRenderer::render(DxUIDeviceResources &deviceResources)
{
  static const auto mask = 0xffffffffu;
  static const FLOAT factor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
  static const UINT32 offsets[] = { 0u };
  static const UINT32 strides[] = { sizeof Vertex };
  static const UINT32 stencil_ref = 0x01u;
  static dx::ISRView *nullSRV[1] = { 0 };

  auto device = d3d->device.Get();
  auto context = d3d->context.Get();

  context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  context->IASetVertexBuffers(0u, 1u, vertexBuffer.GetAddressOf(), strides, offsets);
  context->IASetIndexBuffer(indexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0u);
  context->IASetInputLayout(inputLayout.Get());

  context->VSSetShader(vertexShader.Get(), nullptr, 0u);
  context->PSSetShader(pixelShader.Get(), nullptr, 0u);

  context->VSSetConstantBuffers(0u, 1u, perFrameBuffer.getBufferAddress());
  context->PSSetSamplers(0u, 1u, samplerState.GetAddressOf());
  context->PSSetShaderResources(0u, 1u, deviceResources.getShaderResourceView().GetAddressOf());
  context->OMSetDepthStencilState(depthStencilState.Get(), stencil_ref);

  context->DrawIndexed(6u, 0u, 0u);

  context->PSSetShaderResources(0u, 1u, nullSRV);
  context->PSSetShader(nullptr, nullptr, 0u);
  context->VSSetShader(nullptr, nullptr, 0u);
}

void dxui::DxUIDefaultRenderer::onDeviceLost(dx::DxDeviceResources *d3dResources)
{
  d3d = 0;
  recreateDeviceResources = true;
}

void dxui::DxUIDefaultRenderer::onDeviceRestored(dx::DxDeviceResources *d3dResources)
{
  dxui::trace("dxui:render: device restored");

  d3d = d3dResources;
  d3d->deviceLost += _Dx_delegate_to(this, &DxUIDefaultRenderer::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &DxUIDefaultRenderer::onDeviceRestored);
  //createDeviceResources();
}
