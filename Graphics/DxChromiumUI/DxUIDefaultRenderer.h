#pragma once

#include <DxUIDeviceResources.h>
#include <DxConstantBuffer.h>
#include <DxUIDefaultView.h>

namespace dxui
{
  class DxUIDefaultRenderer
  {
  public:
    struct Vertex
    {
      DirectX::Vector4 position;
      DirectX::Vector3 normal;
      DirectX::Vector2 tex;
    };

    struct PerFrame
    {
      DirectX::Matrix surfaceWorld;
      DirectX::Matrix cameraView;
      DirectX::Matrix cameraProjection;
    };

    typedef Vertex VertexUnit;
    typedef unsigned __int16 Index, IndexUnit;

  private:
    bool recreateDeviceResources;

  public:
    PerFrame perFrameSource;
    DxUIDefaultView surfaceView;

    dx::DxDeviceResources *d3d;
    dx::Buffer indexBuffer;
    dx::Buffer vertexBuffer;
    dx::VShader vertexShader;
    dx::VSLayout inputLayout;
    dx::PShader pixelShader;
    dx::BlendState blendState;
    dx::SamplerState samplerState;
    dx::RasterizerState rasterizerState;
    dx::DepthStencilState depthStencilState;
    dx::ConstantBuffer<PerFrame> perFrameBuffer;

  public:
    DxUIDefaultRenderer(_In_ dx::DxDeviceResources*);
    void createDeviceResources(_In_ DxUIDeviceResources const&);
    void update(_In_ PerFrame const&);
    void render(_In_ DxUIDeviceResources &);

  private:
    void onDeviceLost(_In_ dx::DxDeviceResources*);
    void onDeviceRestored(_In_ dx::DxDeviceResources*);

  };
}