#include "DxUIClient.h"

// CefRequestHandler methods
bool dxui::DxUIClient::OnBeforeBrowse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, bool is_redirect)
{
  CEF_REQUIRE_UI_THREAD();
  messageRouter->OnBeforeBrowse(browser, frame);
  return false;
}

bool dxui::DxUIClient::OnQuotaRequest(CefRefPtr<CefBrowser> browser, const CefString& origin_url, int64 new_size, CefRefPtr<CefQuotaCallback> callback)
{
  CEF_REQUIRE_IO_THREAD();

  int64 maxQuota = int64(allowedQuota * 1024ull * 1024ull);
  bool allowNewSize = (new_size <= maxQuota);

  dxui::trace(
    "dxui:client: requested quota = %.2fmb, approved: %s"
    , float(new_size) / (1024.0f * 1024.0f)
    , (allowNewSize ? "yes" : "no")
    );

  // Grant the quota request if the size is reasonable.
  callback->Continue(allowNewSize);
  return true;
}

void dxui::DxUIClient::OnProtocolExecution(CefRefPtr<CefBrowser> browser, const CefString& url, bool& allow_os_execution)
{
  CEF_REQUIRE_UI_THREAD();

  std::string urlStr = url;

  // Allow OS execution of Spotify URIs.
  if (urlStr.find("spotify:") == 0) allow_os_execution = true;
}

void dxui::DxUIClient::OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser, TerminationStatus status)
{
  CEF_REQUIRE_UI_THREAD();

  dxui::trace("dxui:client: render process terminated");

  messageRouter->OnRenderProcessTerminated(browser);

  // Load the startup URL if that's not the website that we terminated on.
  CefRefPtr<CefFrame> frame = browser->GetMainFrame();
  std::string url = frame->GetURL();
  std::transform(url.begin(), url.end(), url.begin(), tolower);

  std::string startupURL = this->startupUrl;
  if (
    startupURL != "chrome://crash"
    && !url.empty()
    && url.find(startupURL) != 0
    )
  {
    frame->LoadURL(startupURL);
  }
}


