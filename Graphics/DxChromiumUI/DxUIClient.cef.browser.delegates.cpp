#include "DxUIClient.h"

void dxui::DxUIClient::OnContextInitialized()
{
  dxui::trace("dxui:client: context initialized");

  contextCreated(this);
  createBrowserDelegates(this);

  // Register cookieable schemes with the global cookie manager.
  CefRefPtr<CefCookieManager> manager = CefCookieManager::GetGlobalManager();

  DCHECK(manager.get());
  manager->SetSupportedSchemes(cookieableSchemes);

  DxUIClientBrowserDelegate::Set::iterator it = browserDelegates.begin();
  for (; it != browserDelegates.end(); ++it) (*it)->onContextInitialized(this);
}

void dxui::DxUIClient::OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line)
{
  dxui::trace("dxui:client: before child process launched");

  DxUIClientBrowserDelegate::Set::iterator it = browserDelegates.begin();
  for (; it != browserDelegates.end(); ++it) (*it)->onBeforeChildProcessLaunch(this, command_line);
}

void dxui::DxUIClient::OnRenderProcessThreadCreated(CefRefPtr<CefListValue> extra_info)
{
  dxui::trace("dxui:client: render process thread created");

  DxUIClientBrowserDelegate::Set::iterator it = browserDelegates.begin();
  for (; it != browserDelegates.end(); ++it) (*it)->onRenderProcessThreadCreated(this, extra_info);
}
