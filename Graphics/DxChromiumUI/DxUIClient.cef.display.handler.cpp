#include "DxUIClient.h"

void dxui::DxUIClient::OnAddressChange(
  CefRefPtr<CefBrowser> browser,
  CefRefPtr<CefFrame> frame,
  const CefString& url
  )
{
  //std::string address = url.ToString();
  //dxui::trace("dxui:client >> %s", address.c_str());
}

void dxui::DxUIClient::OnTitleChange(
  CefRefPtr<CefBrowser> browser,
  const CefString& content
  )
{
  std::string title = content.ToString();
  dxui::trace("dxui:client >> %s", title.c_str());
}

bool dxui::DxUIClient::OnConsoleMessage(
  CefRefPtr<CefBrowser> browser,
  const CefString& message,
  const CefString& source,
  int line
  )
{
  CEF_REQUIRE_UI_THREAD();

  std::string msg = message.ToString();
  dxui::trace(msg.c_str());
  return true;
}


