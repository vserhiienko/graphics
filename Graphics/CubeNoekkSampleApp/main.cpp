
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include <DxMath.h>
#include <DxAssetManager.h>
#include <DxUtils.h>
#include <NoekkSampleApp.h>
#include <GeometryPrimitives.h>

class LoadPipelineShadersHook : public noekk::AssetReadingCompletionHook
{
public:
  static const unsigned int vsAssetId = 1;
  static const unsigned int psAssetId = 2;

public:
  noekk::AssetBlob vsBlob;
  noekk::AssetBlob psBlob;

public:
  virtual ~LoadPipelineShadersHook()
  {
    vsBlob.dealloc();
    psBlob.dealloc();
  }

  virtual void onAssetLoaded(_In_ unsigned assetId, _In_ noekk::AssetBlob const &asset)
  {
    switch (assetId)
    {
    case vsAssetId: asset.copyTo(vsBlob); break;
    case psAssetId: asset.copyTo(psBlob); break;
    }
  }

};


class CubeSampleApp
  : protected noekk::SampleApp
{

  struct PerFrameVS : public noekk::AlignedNew<PerFrameVS>
  {
    DirectX::XMMATRIX world;
    DirectX::XMMATRIX cameraView;
    DirectX::XMMATRIX cameraProj;

    PerFrameVS()
    {
      using namespace DirectX;
      auto eye = XMFLOAT4(15, 15, 15, 1);
      auto focus = XMFLOAT4(0.1f, 0.1f, 0.1f, 1);
      auto up = XMFLOAT4(0, 1, 0, 1);
      world = XMMatrixIdentity();
      cameraView = XMMatrixLookAtLH(
        XMLoadFloat4(&eye),
        XMLoadFloat4(&focus),
        XMLoadFloat4(&up)
        );
    }

    void updateProj(float fovy, float aspect)
    {
      using namespace DirectX;
      cameraProj = XMMatrixPerspectiveFovLH(
        fovy, aspect, 0.01f, 100.0f
        );
    }

  };

public:
  uint8_t *perVSCallHeapStart;
  PerFrameVS perVSCall;

public:
  D3D12_INDEX_BUFFER_VIEW_DESC indexBufferViewDesc;
  D3D12_VERTEX_BUFFER_VIEW_DESC vertexBufferViewDesc;
  Microsoft::WRL::ComPtr<ID3D12Resource> indexBuffer;
  Microsoft::WRL::ComPtr<ID3D12Resource> vertexBuffer;
  Microsoft::WRL::ComPtr<ID3D12Resource> indexUploadHeap;
  Microsoft::WRL::ComPtr<ID3D12Resource> vertexUploadHeap;
  Microsoft::WRL::ComPtr<ID3D12Resource> perVSCallUploadHeap;
  Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> perVSCallDescHeap;

  Microsoft::WRL::ComPtr<ID3D12CommandList> drawBundle;
  Microsoft::WRL::ComPtr<ID3D12CommandAllocator> drawBundleAllocator;
  Microsoft::WRL::ComPtr<ID3D12PipelineState> pipelineState;
  Microsoft::WRL::ComPtr<ID3D12RootSignature> rootSignature;


public:
  CubeSampleApp()
  {
  }

  virtual ~CubeSampleApp()
  {
    perVSCallUploadHeap->Unmap(0);
  }

  void init()
  {
    SampleApp::init();
    loadAssets();
  }

  void run()
  {
    SampleApp::mainLoop();
  }

  void populateCmdLists()
  {
    noekk::HResult result = S_OK;
    result = commandAllocator->Reset();
  }

  static noekk::HResult createVertexBufferAsCommittedDefault(
    Microsoft::WRL::ComPtr<ID3D12Device> &device,
    Microsoft::WRL::ComPtr<ID3D12CommandList> &commandList,
    uint8_t *buffer,
    size_t bufferOffset,
    size_t bufferStride,
    size_t bufferSize,
    Microsoft::WRL::ComPtr<ID3D12Resource> &bufferResource,
    Microsoft::WRL::ComPtr<ID3D12Resource> &bufferUploadHeap,
    D3D12_VERTEX_BUFFER_VIEW_DESC &bufferViewDesc
    )
  {
    noekk::HResult result;

    result = device->CreateCommittedResource(
      &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
      D3D12_HEAP_MISC_NONE,
      &CD3D12_RESOURCE_DESC::Buffer(bufferSize),
      D3D12_RESOURCE_USAGE_INITIAL,
      IID_PPV_ARGS(&bufferResource)
      );

    if (noekk::traceFailed(result, "CreateCommittedResource(D3D12_HEAP_TYPE_DEFAULT,D3D12_RESOURCE_USAGE_INITIAL): "))
    {
      return result;
    }

    result = device->CreateCommittedResource(
      &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
      D3D12_HEAP_MISC_NONE,
      &CD3D12_RESOURCE_DESC::Buffer(bufferSize),
      D3D12_RESOURCE_USAGE_GENERIC_READ,
      IID_PPV_ARGS(&bufferUploadHeap)
      );

    if (noekk::traceFailed(result, "CreateCommittedResource(D3D12_HEAP_TYPE_UPLOAD,D3D12_RESOURCE_USAGE_GENERIC_READ): "))
    {
      bufferResource = nullptr;
      return result;
    }

    D3D12_SUBRESOURCE_DATA bufferSubresourceData = {};
    bufferSubresourceData.pData = buffer + bufferOffset;
    bufferSubresourceData.RowPitch = bufferSize;
    bufferSubresourceData.SlicePitch = bufferSize;

    setResourceBarrier(commandList, bufferResource, D3D12_RESOURCE_USAGE_INITIAL, D3D12_RESOURCE_USAGE_COPY_DEST);
    UpdateSubresources<1>(commandList.Get(), bufferResource.Get(), bufferUploadHeap.Get(), 0, 0, 1, &bufferSubresourceData);
    setResourceBarrier(commandList, bufferResource, D3D12_RESOURCE_USAGE_COPY_DEST, D3D12_RESOURCE_USAGE_GENERIC_READ);

    bufferViewDesc.OffsetInBytes = 0;
    bufferViewDesc.StrideInBytes = (uint32_t) bufferStride;
    bufferViewDesc.SizeInBytes = (uint32_t) bufferSize;
    return result;
  }

  static noekk::HResult createIndexBufferAsCommittedDefault(
    Microsoft::WRL::ComPtr<ID3D12Device> &device,
    Microsoft::WRL::ComPtr<ID3D12CommandList> &commandList,
    uint8_t *buffer,
    size_t bufferOffset,
    size_t bufferStride,
    size_t bufferSize,
    Microsoft::WRL::ComPtr<ID3D12Resource> &bufferResource,
    Microsoft::WRL::ComPtr<ID3D12Resource> &bufferUploadHeap,
    D3D12_INDEX_BUFFER_VIEW_DESC &bufferViewDesc
    )
  {
    noekk::HResult result;

    result = device->CreateCommittedResource(
      &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
      D3D12_HEAP_MISC_NONE,
      &CD3D12_RESOURCE_DESC::Buffer(bufferSize),
      D3D12_RESOURCE_USAGE_INITIAL,
      IID_PPV_ARGS(&bufferResource)
      );

    if (noekk::traceFailed(result, "CreateCommittedResource(D3D12_HEAP_TYPE_DEFAULT,D3D12_RESOURCE_USAGE_INITIAL): "))
    {
      return result;
    }

    result = device->CreateCommittedResource(
      &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
      D3D12_HEAP_MISC_NONE,
      &CD3D12_RESOURCE_DESC::Buffer(bufferSize),
      D3D12_RESOURCE_USAGE_GENERIC_READ,
      IID_PPV_ARGS(&bufferUploadHeap)
      );

    if (noekk::traceFailed(result, "CreateCommittedResource(D3D12_HEAP_TYPE_UPLOAD,D3D12_RESOURCE_USAGE_GENERIC_READ): "))
    {
      bufferResource = nullptr;
      return result;
    }

    D3D12_SUBRESOURCE_DATA bufferSubresourceData = {};
    bufferSubresourceData.pData = buffer + bufferOffset;
    bufferSubresourceData.RowPitch = bufferSize;
    bufferSubresourceData.SlicePitch = bufferSize;

    setResourceBarrier(commandList, bufferResource, D3D12_RESOURCE_USAGE_INITIAL, D3D12_RESOURCE_USAGE_COPY_DEST);
    UpdateSubresources<1>(commandList.Get(), bufferResource.Get(), bufferUploadHeap.Get(), 0, 0, 1, &bufferSubresourceData);
    setResourceBarrier(commandList, bufferResource, D3D12_RESOURCE_USAGE_COPY_DEST, D3D12_RESOURCE_USAGE_GENERIC_READ);

    bufferViewDesc.OffsetInBytes = bufferOffset;
    bufferViewDesc.Format = DXGI_FORMAT_R16_UINT;
    bufferViewDesc.SizeInBytes = bufferSize;

    return result;
  }

  void loadAssets()
  {
    using Microsoft::WRL::ComPtr;

    noekk::HResult result;

    perVSCall.updateProj(DirectX::XM_PI / 6, (float) backbufferW / (float) backbufferH);
    //perVSCall.updateProj(DirectX::XM_PIDIV4, backbufferW / backbufferH);

    result = commandList->Reset(commandAllocator.Get(), pipelineState.Get());

    LoadPipelineShadersHook loadShadersHook;
    noekk::AssetReader::findAndReadTo(L"shaders/pipelineVS.cso", loadShadersHook.vsAssetId, &loadShadersHook);
    noekk::AssetReader::findAndReadTo(L"shaders/pipelinePS.cso", loadShadersHook.psAssetId, &loadShadersHook);

    D3D12_DESCRIPTOR_RANGE descRangeDesc;
    descRangeDesc.Init(D3D12_DESCRIPTOR_RANGE_CBV, 1, 0);

    D3D12_ROOT_PARAMETER rootParameterSlotDesc;
    rootParameterSlotDesc.InitAsDescriptorTable(1, &descRangeDesc, D3D12_SHADER_VISIBILITY_VERTEX);

    D3D12_ROOT_SIGNATURE rootSignatureDesc;
    // the root signature contains only one slot to be used
    rootSignatureDesc.pParameters = &rootParameterSlotDesc;
    rootSignatureDesc.NumParameters = 1;
    rootSignatureDesc.Flags = 0;

    ComPtr<ID3DBlob> outBlob, errBlob;
    result = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_V1, outBlob.ReleaseAndGetAddressOf(), errBlob.ReleaseAndGetAddressOf());
    result = device->CreateRootSignature(outBlob->GetBufferPointer(), outBlob->GetBufferSize(), IID_PPV_ARGS(rootSignature.ReleaseAndGetAddressOf()));

    const auto inputElementCount = noekk::VertexPositionNormalTexture::inputElementCount;
    D3D12_INPUT_ELEMENT_DESC d3d12InputElements[inputElementCount];
    noekk::VertexPositionNormalTexture::setD3DInputElements(d3d12InputElements);

    // create a PSO description
    D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
    noekk::zeroMemory(pipelineStateDesc);
    pipelineStateDesc.InputLayout = { d3d12InputElements, inputElementCount };
    pipelineStateDesc.pRootSignature = rootSignature.Get();
    pipelineStateDesc.VS = { loadShadersHook.vsBlob.data, loadShadersHook.vsBlob.dataLength };
    pipelineStateDesc.PS = { loadShadersHook.psBlob.data, loadShadersHook.psBlob.dataLength };
    pipelineStateDesc.BlendState = CD3D12_BLEND_DESC(D3D12_DEFAULT);
    pipelineStateDesc.RasterizerState = CD3D12_RASTERIZER_DESC(D3D12_DEFAULT);
    pipelineStateDesc.RasterizerState.FrontCounterClockwise = true;
    pipelineStateDesc.DepthStencilState = CD3D12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
    pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
    pipelineStateDesc.RTVFormats[0] = renderTargetFormat;
    pipelineStateDesc.NumRenderTargets = 1;
    pipelineStateDesc.SampleDesc.Count = 1;
    pipelineStateDesc.SampleMask = UINT_MAX;

    result = device->CreateGraphicsPipelineState(&pipelineStateDesc, pipelineState.ReleaseAndGetAddressOf());

    /// cube geometry

    D3D12_SUBRESOURCE_DATA bufferSubresource = {};
    noekk::GeometricPrimitives::VertexCollection vertices;
    noekk::GeometricPrimitives::IndexCollection indices;
    //noekk::GeometricPrimitives::createTorus(vertices, indices, 6, 2, 32, false);
    //noekk::GeometricPrimitives::createSphere(vertices, indices, 6, 16, false);
    //noekk::GeometricPrimitives::createTetrahedron(vertices, indices, 6, false);
    //noekk::GeometricPrimitives::createCylinder(vertices, indices, 6, 2, 32, false);
    //noekk::GeometricPrimitives::createOctahedron(vertices, indices, 6, false);
    //noekk::GeometricPrimitives::createIcosahedron(vertices, indices, 6, false);
    //noekk::GeometricPrimitives::createDodecahedron(vertices, indices, 6, false);
    //noekk::GeometricPrimitives::createGeoSphere(vertices, indices, 6, 3, false);
    //noekk::GeometricPrimitives::createCone(vertices, indices, 6, 5, 32, false);
    noekk::GeometricPrimitives::createCube(vertices, indices, 10, false);
    const size_t vertexByteSize = sizeof(noekk::VertexPositionNormalTexture);
    const size_t indexByteSize = sizeof(noekk::Index);
    const size_t vertexBufferByteSize = vertexByteSize * vertices.size();
    const size_t indexBufferByteSize = indexByteSize * indices.size();
    const uint32_t indexCountPerCall = (uint32_t) indices.size();

    result = createVertexBufferAsCommittedDefault(
      device,
      commandList,
      reinterpret_cast<uint8_t*>(&vertices[0]),
      0,
      vertexByteSize,
      vertexBufferByteSize,
      vertexBuffer,
      vertexUploadHeap,
      vertexBufferViewDesc
      );
    result = createIndexBufferAsCommittedDefault(
      device,
      commandList,
      reinterpret_cast<uint8_t*>(&indices[0]),
      0,
      indexByteSize,
      indexBufferByteSize,
      indexBuffer,
      indexUploadHeap,
      indexBufferViewDesc
      );

    D3D12_DESCRIPTOR_HEAP_DESC perVSCallDescHeapDesc;
    noekk::zeroMemory(perVSCallDescHeapDesc);
    perVSCallDescHeapDesc.NumDescriptors = 1;
    // this flag indicates that this descriptor heap can be bound to the pipeline and that descriptors contained in it can be referenced by a root table
    perVSCallDescHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_SHADER_VISIBLE;
    perVSCallDescHeapDesc.Type = D3D12_CBV_SRV_UAV_DESCRIPTOR_HEAP;
    result = device->CreateDescriptorHeap(&perVSCallDescHeapDesc, IID_PPV_ARGS(&perVSCallDescHeap));

    size_t perVSCallHeapSize = (sizeof(PerFrameVS) + 255) & ~255;

    // create an upload heap
    result = device->CreateCommittedResource(
      &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
      D3D12_HEAP_MISC_NONE,
      &CD3D12_RESOURCE_DESC::Buffer(1024 * 64),
      //&CD3D12_RESOURCE_DESC::Buffer(perVSCallHeapSize), // ??? 1024 * 64
      D3D12_RESOURCE_USAGE_GENERIC_READ,
      IID_PPV_ARGS(perVSCallUploadHeap.GetAddressOf())
      );

    // create constant buffer view description to access the upload buffer
    D3D12_CONSTANT_BUFFER_VIEW_DESC perVSCallViewDesc;
    perVSCallViewDesc.OffsetInBytes = 0;
    perVSCallViewDesc.SizeInBytes = (uint32_t) perVSCallHeapSize;
    device->CreateConstantBufferView(perVSCallUploadHeap.Get(), &perVSCallViewDesc, perVSCallDescHeap->GetCPUDescriptorHandleForHeapStart());

    perVSCallUploadHeap->Map(0, (void**) &perVSCallHeapStart);
    memcpy(perVSCallHeapStart, &perVSCall, sizeof(perVSCall));

    /// draw bundle

    result = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_BUNDLE, drawBundleAllocator.GetAddressOf());
    result = device->CreateCommandList(D3D12_COMMAND_LIST_TYPE_BUNDLE, drawBundleAllocator.Get(), pipelineState.Get(), drawBundle.GetAddressOf());
    // record commands into the bundle
    drawBundle->SetGraphicsRootSignature(rootSignature.Get());
    drawBundle->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    drawBundle->SetIndexBufferSingleUse(indexBuffer.Get(), &indexBufferViewDesc);
    drawBundle->SetVertexBuffersSingleUse(0, vertexBuffer.GetAddressOf(), &vertexBufferViewDesc, 1);
    drawBundle->SetGraphicsRootDescriptorTable(0, perVSCallDescHeap->GetGPUDescriptorHandleForHeapStart());
    drawBundle->DrawIndexedInstanced(indexCountPerCall, 1, 0, 0, 0);
    result = drawBundle->Close();

    //result = commandList->Close();
    //commandQueue->ExecuteCommandList(commandList.Get());

    awaitGPUResources();
  }

  virtual void handlePaint(noekk::Window const*) override
  {
    render();
  }

  void update()
  {
    static float angleY = 0.0f;
    angleY += 0.01f;
    if (angleY >= DirectX::XM_2PI)
      angleY = 0.0f;

    perVSCall.world = DirectX::XMMatrixRotationY(angleY);
    memcpy(perVSCallHeapStart, &perVSCall, sizeof(perVSCall));
  }

  void populate()
  {
    noekk::HResult result;


    // command list allocators can be only be reset when the associated command lists have finished execution on the GPU; apps should use fences to determine GPU execution progress
    result = commandAllocator->Reset();

    // HOWEVER, when ExecuteCommandList() is called on a particular command list, that command list can then be reset anytime and must be before rerecording
    result = commandList->Reset(commandAllocator.Get(), pipelineState.Get());

    // set the viewport and scissor rectangle
    commandList->RSSetViewports(1, &viewport);
    commandList->RSSetScissorRects(1, &rectScissor);

    // indicate this resource will be in use as a render target
    setResourceBarrier(
      commandList, renderTarget,
      D3D12_RESOURCE_USAGE_PRESENT,
      D3D12_RESOURCE_USAGE_RENDER_TARGET
      );

    // record commands
    float clearColor [] = { 0.0f, 0.2f, 0.4f, 1.0f };

    commandList->ClearRenderTargetView(
      renderTargetDescHeap->GetCPUDescriptorHandleForHeapStart(),
      clearColor, nullptr, 0
      );
    commandList->ClearDepthStencilView(
      depthStencilDescHeap->GetCPUDescriptorHandleForHeapStart(),
      D3D11_CLEAR_DEPTH, 1.0f, 0, nullptr, 0
      );

    // set descriptor heaps
    ID3D12DescriptorHeap* heaps [] = { perVSCallDescHeap.Get() };
    commandList->SetDescriptorHeaps(heaps, ARRAYSIZE(heaps));

    D3D12_CPU_DESCRIPTOR_HANDLE RTVDescriptors [] = { renderTargetDescHeap->GetCPUDescriptorHandleForHeapStart() };
    D3D12_CPU_DESCRIPTOR_HANDLE DSVDescriptor [] = { depthStencilDescHeap->GetCPUDescriptorHandleForHeapStart() };
    commandList->SetRenderTargets(RTVDescriptors, true, 1, DSVDescriptor);
    commandList->ExecuteBundle(drawBundle.Get());

    // indicate that the render target will now be used to present when the command list is done executing
    setResourceBarrier(
      commandList, renderTarget,
      D3D12_RESOURCE_USAGE_RENDER_TARGET,
      D3D12_RESOURCE_USAGE_PRESENT
      );

    // all we need to do now is execute the command list
    result = (commandList->Close());
  }

  void render()
  {
    static unsigned backbufferIndex = 0;
    static unsigned backbufferCount = 2;
    if (commandList.Get() == 0) return;

    noekk::HResult result;

    update();
    populate();

    // execute the command list
    commandQueue->ExecuteCommandList(commandList.Get());

    // swap the back and front buffers
    result = (swapChain->Present(1, 0));
    backbufferIndex = (1 + backbufferIndex) % backbufferCount;
    swapChain->GetBuffer(backbufferIndex, IID_PPV_ARGS(renderTarget.ReleaseAndGetAddressOf()));
    device->CreateRenderTargetView(renderTarget.Get(), nullptr, renderTargetDescHeap->GetCPUDescriptorHandleForHeapStart());

    // wait and reset EVERYTHING
    awaitGPUResources();
  }



};


void setCheckForMemortLeaks()
{
  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
}


int main(int argc, char *argv [])
{
#if _DEBUG
  //setCheckForMemortLeaks();
#endif

  CubeSampleApp app;
  app.init();
  app.run();
  return EXIT_SUCCESS;
}