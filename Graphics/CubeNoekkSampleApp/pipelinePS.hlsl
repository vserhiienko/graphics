
struct PIn
{
  float4 position : SV_POSITION;
  float3 normal : NORMAL;
  float2 tex : TEXCOORD;
};

float4 main(in PIn input) : SV_TARGET
{
  float4 outputColor = 0.5f;
  outputColor.x = input.tex.y;
  outputColor.y = input.tex.x;
  outputColor.w = 1.0f;
  return outputColor;
}