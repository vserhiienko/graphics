// Geometric Tools LLC, Redmond WA 98052
// Copyright (c) 1998-2015
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 1.0.3 (2014/12/13)

#pragma once

#include "GTEngineDEF.h"
#include <functional>

// Compute a root of a function F(t) on an interval [t0, t1].  The caller
// specifies the maximum number of iterations, in case you want limited
// accuracy for the root.  However, the function is designed for native types
// (Real = float/double).  If you specify a sufficiently large number of
// iterations, the root finder bisects until either F(t) is identically zero
// [a condition dependent on how you structure F(t) for evaluation] or the
// midpoint (t0 + t1)/2 rounds numerically to tmin or tmax.  Of course, it
// is required that t0 < t1.  The return value Find is the number of
// iterations actually used:  1 if an endpoint is the root, 2 or larger
// if a root is found using bisection, or 0 if invalid t0 and t1 are
// passed in or if F(t0)*F(t1) > 0 (no guaranteed root on the interval).

namespace gte
{

template <typename Real>
class RootsBisection
{
public:
    // It is necessary that F(t0)*F(t1) <= 0, in which case the function
    // returns a positive value and the 'root' is valid; otherwise, the
    // function returns 0 and 'root' is invalid (do not use it).  When
    // F(t0)*F(t1) > 0, the interval may very well contain a root but we
    // cannot know that, so 0 is returned.  The function also returns 0
    // when t0 >= t1.

    static unsigned int Find(std::function<Real(Real)> const& F, Real t0,
        Real t1,  unsigned int maxIterations, Real& root);

    // If f0 = F(t0) and f1 = F(t1) are already known, pass them to the
    // bisector.  This is useful when |f0| or |f1| is infinite, and you can
    // pass sign(f0) or sign(f1) rather than then infinity because the
    // bisector cares only about the signs of f.

    static unsigned int Find(std::function<Real(Real)> const& F, Real t0,
        Real t1, Real f0, Real f1, unsigned int maxIterations, Real& root);
};

//----------------------------------------------------------------------------
template <typename Real>
unsigned int RootsBisection<Real>::Find(std::function<Real(Real)> const& F,
    Real t0, Real t1, unsigned int maxIterations, Real& root)
{
    if (t0 < t1)
    {
        // Test the endpoints to see whether F(t) is zero.
        Real f0 = F(t0);
        if (f0 == (Real)0)
        {
            root = t0;
            return 1;
        }

        Real f1 = F(t1);
        if (f1 == (Real)0)
        {
            root = t1;
            return 1;
        }

        if (f0*f1 > (Real)0)
        {
            // It is not known whether the interval bounds a root.
            return 0;
        }

        for (unsigned int i = 2; i <= maxIterations; ++i)
        {
            root = ((Real)0.5) * (t0 + t1);
            if (root == t0 || root == t1)
            {
                // The numbers t0 and t1 are consecutive floating-point
                // numbers.
                return true;
            }

            Real fm = F(root);
            Real product = fm * f0;
            if (product < (Real)0)
            {
                t1 = root;
                f1 = fm;
            }
            else if (product >(Real)0)
            {
                t0 = root;
                f0 = fm;
            }
            else
            {
                return i;
            }
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
template <typename Real>
unsigned int RootsBisection<Real>::Find(std::function<Real(Real)> const& F,
    Real t0, Real t1, Real f0, Real f1, unsigned int maxIterations,
    Real& root)
{
    if (t0 < t1)
    {
        // Test the endpoints to see whether F(t) is zero.
        if (f0 == (Real)0)
        {
            root = t0;
            return 1;
        }

        if (f1 == (Real)0)
        {
            root = t1;
            return 1;
        }

        if (f0*f1 > (Real)0)
        {
            // It is not known whether the interval bounds a root.
            return 0;
        }

        for (unsigned int i = 1; i <= maxIterations; ++i)
        {
            root = ((Real)0.5) * (t0 + t1);
            if (root == t0 || root == t1)
            {
                // The numbers t0 and t1 are consecutive floating-point
                // numbers.
                return true;
            }

            Real fm = F(root);
            Real product = fm * f0;
            if (product < (Real)0)
            {
                t1 = root;
                f1 = fm;
            }
            else if (product >(Real)0)
            {
                t0 = root;
                f0 = fm;
            }
            else
            {
                return i;
            }
        }
    }

    return 0;
}
//----------------------------------------------------------------------------

}
