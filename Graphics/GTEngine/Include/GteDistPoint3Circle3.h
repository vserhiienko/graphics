// Geometric Tools LLC, Redmond WA 98052
// Copyright (c) 1998-2015
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 1.0.2 (2014/12/13)

#pragma once

#include "GteDCPQuery.h"
#include "GteCircle3.h"
#include <limits>

namespace gte
{

template <typename Real>
class DCPQuery<Real, Vector3<Real>, Circle3<Real>>
{
public:
    struct Result
    {
        Real distance, sqrDistance;

        // The possibilities are 1 or std::numeric_limits<int>::max().  In the
        // latter case, the query point is on the line containing the circle
        // center and having direction the circle normal, and one closest point
        // is returned. 
        int numClosestPoints;
        Vector3<Real> circleClosestPoint;
    };

    Result operator()(Vector3<Real> const& point,
        Circle3<Real> const& circle);
};

//----------------------------------------------------------------------------
template <typename Real>
typename DCPQuery<Real, Vector3<Real>, Circle3<Real>>::Result
DCPQuery<Real, Vector3<Real>, Circle3<Real>>::operator()(
Vector3<Real> const& point, Circle3<Real> const& circle)
{
    Result result;

    // Projection of P-C onto plane is Q-C = P-C - Dot(N,P-C)*N.
    Vector3<Real> PmC = point - circle.center;
    Vector3<Real> QmC = PmC - Dot(circle.normal, PmC)*circle.normal;
    Real lengthQmC = Length(QmC);
    if (lengthQmC > (Real)0)
    {
        result.numClosestPoints = 1;
        result.circleClosestPoint =
            circle.center + circle.radius*QmC / lengthQmC;
    }
    else
    {
        // All circle points are equidistant from P.  Return one of them.
        result.numClosestPoints = std::numeric_limits<int>::max();
        Vector3<Real> basis[3];
        basis[0] = circle.normal;
        ComputeOrthogonalComplement(1, basis);
        result.circleClosestPoint =
            circle.center + circle.radius*basis[1];
    }

    Vector3<Real> diff = point - result.circleClosestPoint;
    result.sqrDistance = Dot(diff, diff);
    result.distance = sqrt(result.sqrDistance);
    return result;
}
//----------------------------------------------------------------------------

}
