// Geometric Tools LLC, Redmond WA 98052
// Copyright (c) 1998-2015
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 1.6.1 (2014/11/25)

#include "GTEnginePCH.h"
#include "GteBSPrecision.h"
#include <algorithm>
using namespace gte;

//----------------------------------------------------------------------------
BSPrecision::BSPrecision(bool isFloat)
{
    // NOTE:  It is not clear what the rationale is for the C++ Standard
    // Library to set numeric_limits<float>::min_exponent to -125
    // instead of -126; same issue with numeric_limits<double>::min_exponent
    // set to -1021 instead of -1022.  Similarly, it is not clear why
    // numeric_limits<float>::max_exponent is set to 128 when the maximum
    // finite 'float' has exponent 127.  The exponent 128 is used in the
    // 'float' infinity, but treating this as 2^{128} does not seem to be
    // consistent with the IEEE 754-2008 standard.  Same issue with
    // numeric_limits<double>::max_exponent set to 1024 rather than 1023.

    if (isFloat)
    {
        mNumBits = std::numeric_limits<float>::digits;  // 24
        mMinBiasedExponent =
            std::numeric_limits<float>::min_exponent - mNumBits;  // -149
        mMaxExponent = std::numeric_limits<float>::max_exponent - 1;  // 127
    }
    else
    {
        mNumBits = std::numeric_limits<double>::digits;  // 53
        mMinBiasedExponent =
            std::numeric_limits<double>::min_exponent - mNumBits;  // -1074
        mMaxExponent = std::numeric_limits<double>::max_exponent - 1;  // 1023
    }
}
//----------------------------------------------------------------------------
BSPrecision::BSPrecision(bool isFloat, int32_t maxExponent)
{
    if (isFloat)
    {
        mNumBits = std::numeric_limits<float>::digits;  // 24
        mMinBiasedExponent =
            std::numeric_limits<float>::min_exponent - mNumBits;  // -149
        mMaxExponent = maxExponent;
    }
    else
    {
        mNumBits = std::numeric_limits<double>::digits;  // 53
        mMinBiasedExponent =
            std::numeric_limits<double>::min_exponent - mNumBits;  // -1074
        mMaxExponent = maxExponent;
    }
}
//----------------------------------------------------------------------------
BSPrecision::BSPrecision(int32_t numBits, int32_t minBiasedExponent,
    int32_t maxExponent)
    :
    mNumBits(numBits),
    mMinBiasedExponent(minBiasedExponent),
    mMaxExponent(maxExponent)
{
}
//----------------------------------------------------------------------------
int32_t BSPrecision::GetNumWords() const
{
    return mNumBits / 32 + ((mNumBits % 32) > 0 ? 1 : 0);
}
//----------------------------------------------------------------------------
int32_t BSPrecision::GetNumBits() const
{
    return mNumBits;
}
//----------------------------------------------------------------------------
int32_t BSPrecision::GetMinBiasedExponent() const
{
    return mMinBiasedExponent;
}
//----------------------------------------------------------------------------
int32_t BSPrecision::GetMinExponent() const
{
    return mMinBiasedExponent + mNumBits - 1;
}
//----------------------------------------------------------------------------
int32_t BSPrecision::GetMaxExponent() const
{
    return mMaxExponent;
}
//----------------------------------------------------------------------------
BSPrecision BSPrecision::operator*(BSPrecision const& precision)
{
    int32_t numBits = mNumBits + precision.mNumBits;
    int32_t maxExponent = mMaxExponent + precision.mMaxExponent + 1;
    int32_t minBiasedExponent =
        mMinBiasedExponent + precision.mMinBiasedExponent;
    return BSPrecision(numBits, minBiasedExponent, maxExponent);
}
//----------------------------------------------------------------------------
BSPrecision BSPrecision::operator+(BSPrecision const& precision)
{
    int32_t minBiasedExponent =
        std::min(mMinBiasedExponent, precision.mMinBiasedExponent);
    int32_t maxExponent = std::max(mMaxExponent, precision.mMaxExponent) + 1;
    int32_t numBits = maxExponent - minBiasedExponent;
    return BSPrecision(numBits, minBiasedExponent, maxExponent);
}
//----------------------------------------------------------------------------
BSPrecision BSPrecision::operator-(BSPrecision const& precision)
{
    return operator+(precision);
}
//----------------------------------------------------------------------------
