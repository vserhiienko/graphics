#pragma once
#include "pch.h"
#include "Shapes.h"

namespace nyx
{
  class SelectionAssits
  {


  public:
    bool m_addPoints;
    nv::vec2f screenDimensions;
    uint32_t m_reservationSize;
    uint32_t m_nextPointPosition;
    uint32_t m_pointCount;
    float m_maxDistance; // maximal distance between two points on the screen
    std::vector<nv::vec2f> m_points;
    std::vector<nv::vec3f> m_unprojPoints;
    std::vector<nyx::linesegf> m_lineSegs;

  public:
    SelectionAssits(void);
    ~SelectionAssits(void);

    void generateLineSegs(void);
    void updateProjectedPoints(
      float const *view,
      float const *proj
      );

    void unprojectPoints(
      nyx::planef const &ground,
      float const *view,
      float const *proj,
      float const *viewInv = nullptr
      );

    void beginSelection(void);
    void addPoint(nv::vec2f const &point);
    void endSelection(void);
    void setScreenDimensions(float width, float height);
    void findPointsInSelection(nv::vec2f const *points, int *isSelected, uint32_t pointCount, bool resetSelection);

    void draw(void);
    void drawLine(float x1, float y1, float x2, float y2, float r = 0.0f, float b = 0.0f, float g = 0.0f, float a = 1.0f);
    void drawCircle(float cx, float cy, float radius, int num_segments, float r = 0.0f, float b = 0.0f, float g = 0.0f, float a = 1.0f);

  };
}