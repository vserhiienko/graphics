#include "pch.h"
#include "Shapes.h"

#if 0
// Inverse of 4x4 Matrix, Cramer's rule and Streaming SIMD Extensions, March 1999
void nyx::inverseMatrix4(float *src)
{
  /*
  Variables (Streaming SIMD Extensions registers) which will contain cofactors and, later, the
  lines of the inverted matrix are declared.
  */

  __m128 minor0, minor1, minor2, minor3;

  /*
  Variables which will contain the lines of the reference matrix and, later (after the transposition),
  the columns of the original matrix are declared.
  */

  __m128 row0, row1, row2, row3;

  /*
  Temporary variables and the variable that will contain the matrix determinant are declared.
  */

  __m128 det, tmp1;

  // Matrix transposition.
  tmp1 = _mm_loadh_pi(_mm_loadl_pi(tmp1, (__m64*)(src)), (__m64*)(src + 4));
  row1 = _mm_loadh_pi(_mm_loadl_pi(row1, (__m64*)(src + 8)), (__m64*)(src + 12));
  row0 = _mm_shuffle_ps(tmp1, row1, 0x88);
  row1 = _mm_shuffle_ps(row1, tmp1, 0xDD);
  tmp1 = _mm_loadh_pi(_mm_loadl_pi(tmp1, (__m64*)(src + 2)), (__m64*)(src + 6));
  row3 = _mm_loadh_pi(_mm_loadl_pi(row3, (__m64*)(src + 10)), (__m64*)(src + 14));
  row2 = _mm_shuffle_ps(tmp1, row3, 0x88);
  row3 = _mm_shuffle_ps(row3, tmp1, 0xDD);

  /*
  Cofactors calculation. Because in the process of cofactor computation some pairs in three
  element products are repeated, it is not reasonable to load these pairs anew every time. The
  values in the registers with these pairs are formed using shuffle instruction. Cofactors are
  calculated row by row (4 elements are placed in 1 SP FP SIMD floating point register).
  */

  tmp1 = _mm_mul_ps(row2, row3);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
  minor0 = _mm_mul_ps(row1, tmp1);
  minor1 = _mm_mul_ps(row0, tmp1);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
  minor0 = _mm_sub_ps(_mm_mul_ps(row1, tmp1), minor0);
  minor1 = _mm_sub_ps(_mm_mul_ps(row0, tmp1), minor1);
  minor1 = _mm_shuffle_ps(minor1, minor1, 0x4E);

  tmp1 = _mm_mul_ps(row1, row2);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
  minor0 = _mm_add_ps(_mm_mul_ps(row3, tmp1), minor0);
  minor3 = _mm_mul_ps(row0, tmp1);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
  minor0 = _mm_sub_ps(minor0, _mm_mul_ps(row3, tmp1));
  minor3 = _mm_sub_ps(_mm_mul_ps(row0, tmp1), minor3);
  minor3 = _mm_shuffle_ps(minor3, minor3, 0x4E);

  tmp1 = _mm_mul_ps(_mm_shuffle_ps(row1, row1, 0x4E), row3);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
  row2 = _mm_shuffle_ps(row2, row2, 0x4E);
  minor0 = _mm_add_ps(_mm_mul_ps(row2, tmp1), minor0);
  minor2 = _mm_mul_ps(row0, tmp1);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
  minor0 = _mm_sub_ps(minor0, _mm_mul_ps(row2, tmp1));
  minor2 = _mm_sub_ps(_mm_mul_ps(row0, tmp1), minor2);
  minor2 = _mm_shuffle_ps(minor2, minor2, 0x4E); 

  tmp1 = _mm_mul_ps(row0, row1);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
  minor2 = _mm_add_ps(_mm_mul_ps(row3, tmp1), minor2);
  minor3 = _mm_sub_ps(_mm_mul_ps(row2, tmp1), minor3);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
  minor2 = _mm_sub_ps(_mm_mul_ps(row3, tmp1), minor2);
  minor3 = _mm_sub_ps(minor3, _mm_mul_ps(row2, tmp1));

  tmp1 = _mm_mul_ps(row0, row3);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
  minor1 = _mm_sub_ps(minor1, _mm_mul_ps(row2, tmp1));
  minor2 = _mm_add_ps(_mm_mul_ps(row1, tmp1), minor2);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
  minor1 = _mm_add_ps(_mm_mul_ps(row2, tmp1), minor1);
  minor2 = _mm_sub_ps(minor2, _mm_mul_ps(row1, tmp1));

  tmp1 = _mm_mul_ps(row0, row2);
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
  minor1 = _mm_add_ps(_mm_mul_ps(row3, tmp1), minor1);
  minor3 = _mm_sub_ps(minor3, _mm_mul_ps(row1, tmp1));
  tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
  minor1 = _mm_sub_ps(minor1, _mm_mul_ps(row3, tmp1));
  minor3 = _mm_add_ps(_mm_mul_ps(row1, tmp1), minor3);

  /*
  Evaluation of determinant and its reciprocal value. 1/det is evaluated using a fast rcpps
  command with subsequent approximation using the Newton-Raphson algorithm.
  */

  det = _mm_mul_ps(row0, minor0);
  det = _mm_add_ps(_mm_shuffle_ps(det, det, 0x4E), det);
  det = _mm_add_ss(_mm_shuffle_ps(det, det, 0xB1), det);
  tmp1 = _mm_rcp_ss(det);
  det = _mm_sub_ss(_mm_add_ss(tmp1, tmp1), _mm_mul_ss(det, _mm_mul_ss(tmp1, tmp1)));
  det = _mm_shuffle_ps(det, det, 0x00);

  /*
  Multiplication of cofactors by 1/det. Storing the inverse matrix to the address in pointer src
  */

  minor0 = _mm_mul_ps(det, minor0);
  _mm_storel_pi((__m64*)(src), minor0);
  _mm_storeh_pi((__m64*)(src + 2), minor0);

  minor1 = _mm_mul_ps(det, minor1);
  _mm_storel_pi((__m64*)(src + 4), minor1);
  _mm_storeh_pi((__m64*)(src + 6), minor1);

  minor2 = _mm_mul_ps(det, minor2);
  _mm_storel_pi((__m64*)(src + 8), minor2);
  _mm_storeh_pi((__m64*)(src + 10), minor2);

  minor3 = _mm_mul_ps(det, minor3);
  _mm_storel_pi((__m64*)(src + 12), minor3);
  _mm_storeh_pi((__m64*)(src + 14), minor3);

}

#endif

nyx::rayf nyx::calculatePickingRay(
  nv::vec2f const &mouse,
  nv::vec2f const &screen,
  nv::matrix4f const &viewInv,
  nv::matrix4f const &projection
  )
{
  nv::vec2f v;
  nyx::rayf ray;

  v.x = (2.0f * mouse.x / screen.x - 1.0f) / projection._11;
  v.y = (1.0f - 2.0f * mouse.y / screen.y) / projection._22;

  ray.direction.x = v.x * viewInv._11 + v.y * viewInv._21 + viewInv._31;
  ray.direction.y = v.x * viewInv._12 + v.y * viewInv._22 + viewInv._32;
  ray.direction.z = v.x * viewInv._13 + v.y * viewInv._23 + viewInv._33;
  ray.direction = nv::normalize(ray.direction);

  ray.origin.x = viewInv._41; // eye.x
  ray.origin.y = viewInv._42; // eye.y
  ray.origin.z = viewInv._43; // eye.z

  return ray;
}

nv::vec3f nyx::rayAt(nyx::rayf const &ray, float t)
{
  return ray.direction * t + ray.origin;
}

bool nyx::intersects(
  nyx::rayf const &ray,
  nyx::planef const &plane,
  float &t
  )
{
  float ddn = nv::dot(ray.direction, plane.normal);
  if (ddn == 0.0f) return false;
  t = (plane.distance - nv::dot(ray.origin, plane.normal)) / ddn;
  return true;
}


void nyx::createBoxMesh(nyx::Mesh &mesh, nv::vec3f pos, nv::vec3f dim, nv::vec4f rgb)
{
  typedef nyx::Mesh::Vertex Vertex;
  typedef nyx::Mesh::Index  Index;

  nyx::Mesh::VertexVector   vertices;
  nyx::Mesh::IndexVector    indices;

  dim.x *= 0.5f;
  dim.z *= 0.5f;

  // +Z face
  vertices.push_back(Vertex(-dim.x + pos.x, 0.0f + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, 0.0f + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, dim.y + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(-dim.x + pos.x, dim.y + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));

  // -Z face
  vertices.push_back(Vertex(-dim.x + pos.x, dim.y + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, dim.y + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, 0.0f + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(-dim.x + pos.x, 0.0f + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));

  // +X face
  vertices.push_back(Vertex(+dim.x + pos.x, 0.0f + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, 0.0f + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, dim.y + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, dim.y + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));

  // -X face
  vertices.push_back(Vertex(-dim.x + pos.x, dim.y + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(-dim.x + pos.x, dim.y + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(-dim.x + pos.x, 0.0f + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(-dim.x + pos.x, 0.0f + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));

  // +Y face
  vertices.push_back(Vertex(-dim.x + pos.x, dim.y + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, dim.y + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, dim.y + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(-dim.x + pos.x, dim.y + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));

  // -Y face
  vertices.push_back(Vertex(-dim.x + pos.x, 0.0f + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, 0.0f + pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(+dim.x + pos.x, 0.0f + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(Vertex(-dim.x + pos.x, 0.0f + pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));


  // Create the indices
  for (int32_t i = 0; i < 24; i += 4)
  {
    indices.push_back((Index)(0 + i));
    indices.push_back((Index)(1 + i));
    indices.push_back((Index)(2 + i));

    indices.push_back((Index)(0 + i));
    indices.push_back((Index)(2 + i));
    indices.push_back((Index)(3 + i));
  }

  mesh.update(vertices, indices);
}
void nyx::createGroundMesh(nyx::Mesh &mesh, nv::vec3f pos, nv::vec3f dim, nv::vec4f rgb)
{
  nyx::Mesh::VertexVector   vertices;
  nyx::Mesh::IndexVector    indices;

  dim.x *= 0.5f;
  dim.z *= 0.5f;

  // +Y face
  vertices.push_back(nyx::Mesh::Vertex(-dim.x + pos.x, pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(nyx::Mesh::Vertex(+dim.x + pos.x, pos.y, +dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(nyx::Mesh::Vertex(+dim.x + pos.x, pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));
  vertices.push_back(nyx::Mesh::Vertex(-dim.x + pos.x, pos.y, -dim.z + pos.z, rgb.x, rgb.y, rgb.z, rgb.w));

  // Create the indices
  indices.push_back(0); indices.push_back(1); indices.push_back(2);
  indices.push_back(0); indices.push_back(2); indices.push_back(3);

  mesh.update(vertices, indices);
}

void nyx::randomColor(float &r, float &g, float &b)
{
  r = float(rand() % 255) / 255.0f;
  g = float(rand() % 255) / 255.0f;
  b = float(rand() % 255) / 255.0f;
};



float nyx::substituteX(nv::vec2f const &p, nyx::linef const &l)
{
  return l.k * p.x + l.b;
}

nyx::linef nyx::createLineFromPoints(nv::vec2f const &a, nv::vec2f const &b)
{
  nyx::linef l;
  float dx = (b.x - a.x);
  if (dx != 0) l.k = (b.y - a.y) / dx;
  else l.k = 0.0f;
  l.b = a.y - a.x * l.k;
  return l;
}

nyx::linesegf nyx::createLineSegFromPoints(nv::vec2f const &a, nv::vec2f const &b)
{
  nyx::linesegf seg;
  seg.a = a, seg.b = b;
  updateLineSegment(seg);
  return seg;
}

nv::vec2f nyx::createPointFromLineSegs(nyx::linesegf const &ls)
{
  return ls.b - ls.a;
}

void nyx::updateLineSegment(nyx::linesegf &seg)
{
  seg.l = nyx::createLineFromPoints(seg.a, seg.b);
}

bool nyx::isMouseOverPoint(nv::vec2f const &p1, nv::vec2f const &p2, float r)
{
  return nv::length(p2 - p1) <= r;
}

bool nyx::linesCross(nyx::linef const &a, nyx::linef const &b)
{
  return a.k != b.k;
}

float nyx::dot(nv::vec2f const &a, nv::vec2f const &b)
{
  return a.x * b.x + a.y * b.y;
}

float nyx::cross(nv::vec2f const &a, nv::vec2f const &b)
{
  return a.x * b.y - b.x * a.y;
}

bool nyx::lineSegsCross(nyx::linesegf const &a, nyx::linesegf const &b)
{
  if (areLinesParallel(a.l, b.l))
    return false;

  nv::vec2f const
    p1p2 = a.b - a.a,
    p3p4 = b.b - b.a,
    p1p3 = b.a - a.a,
    p1p4 = b.b - a.a,
    p3p1 = a.a - b.a,
    p3p2 = a.b - b.a;

  // 1st case - both (p3, p4) points should lay on the different sides of <p1p2>
  // this far their cross products product should be negative

  float const cross_p1p2_p1p3 = nyx::cross(p1p2, p1p3);
  float const cross_p1p2_p1p4 = nyx::cross(p1p2, p1p4);
  bool const bp1p2 = (cross_p1p2_p1p3 * cross_p1p2_p1p4) < -0.0f;

  // 2nd case - both points (p1, p2) should lay on the different sides of <p3p4>
  // this far their cross products product should be negative

  float const cross_p3p4_p3p1 = nyx::cross(p3p4, p3p1);
  float const cross_p3p4_p3p2 = nyx::cross(p3p4, p3p2);
  bool const bp3p4 = (cross_p3p4_p3p1 * cross_p3p4_p3p2) < -0.0f;

  return bp1p2 && bp3p4;
}

bool nyx::isPointInsidePolygon(nv::vec2f const &startPoint, nv::vec2f const *polygon, uint32_t polygonSize, float testDistance)
{
  nv::vec2f endPoint;
  endPoint.x = startPoint.x + testDistance;
  endPoint.y = startPoint.y + testDistance;

  nyx::linesegf chkLs, tmpLs;
  chkLs.a = startPoint;
  chkLs.b = endPoint;
  nyx::updateLineSegment(chkLs);

  uint16_t occurances = 0;
  for (uint16_t i = 0; i < polygonSize; i++)
  {
    tmpLs.a = polygon[i];
    tmpLs.b = polygon[(i + 1) % polygonSize];
    nyx::updateLineSegment(tmpLs);

    if (nyx::lineSegsCross(chkLs, tmpLs))
      occurances++;
  }

  return (occurances % 2) == 1;
}

bool nyx::calculateCrossPoint(nyx::linesegf const &a, nyx::linesegf const &b, nv::vec2f &crossPoint)
{
  if (!nyx::lineSegsCross(a, b)) return false;
  else return nyx::calculateCrossPoint(a.l, b.l, crossPoint);
}

bool nyx::areLinesEqual(nyx::linef const &a, nyx::linef const &b)
{
  return a.k == b.k && a.b == b.b;
}

bool nyx::areLinesParallel(nyx::linef const &a, nyx::linef const &b)
{
  return a.k == b.k;
}

bool nyx::pointBelongsToLine(nv::vec2f const &p, nyx::linef const &l)
{
  return nyx::substituteX(p, l) == p.y;
}

bool nyx::areLinesPerpendicular(nyx::linef const &a, nyx::linef const &b)
{
  return (-1.0f) == (a.k * b.k);
}

bool nyx::calculateCrossPoint(nyx::linef const &a, nyx::linef const &b, nv::vec2f &cross)
{
  if (areLinesParallel(a, b))
  {
    return false;
  }
  else
  {
    cross.x = (b.b - a.b) / (a.k - b.k);
    cross.x = nyx::substituteX(cross, b);
    return true;
  }
}


nv::vec2f nyx::projectPoint(
  nv::vec4f const &pos,
  nv::vec2f const &screenDimensions,
  float const *viewPtr,
  float const *projPtr
  )
{
  auto width = screenDimensions.x;
  auto height = screenDimensions.y;
  auto objx = pos.x, objy = pos.y, objz = pos.z;
  auto modelview = viewPtr, projection = projPtr;

  nv::vec2f winxy;
  float normalizedValues[8];

  normalizedValues[0] = modelview[0] * objx + modelview[4] * objy + modelview[8] * objz + modelview[12];  //w is always 1
  normalizedValues[1] = modelview[1] * objx + modelview[5] * objy + modelview[9] * objz + modelview[13];
  normalizedValues[2] = modelview[2] * objx + modelview[6] * objy + modelview[10] * objz + modelview[14];
  normalizedValues[3] = modelview[3] * objx + modelview[7] * objy + modelview[11] * objz + modelview[15];

  normalizedValues[4] = projection[0] * normalizedValues[0] + projection[4] * normalizedValues[1] + projection[8] * normalizedValues[2] + projection[12] * normalizedValues[3];
  normalizedValues[5] = projection[1] * normalizedValues[0] + projection[5] * normalizedValues[1] + projection[9] * normalizedValues[2] + projection[13] * normalizedValues[3];
  normalizedValues[6] = projection[2] * normalizedValues[0] + projection[6] * normalizedValues[1] + projection[10] * normalizedValues[2] + projection[14] * normalizedValues[3];
  normalizedValues[7] = -normalizedValues[2];

  //if (normalizedValues[7] == 0.0) return 0;
  normalizedValues[7] = 1.0 / normalizedValues[7];

  normalizedValues[4] *= normalizedValues[7];
  normalizedValues[5] *= normalizedValues[7];
  normalizedValues[6] *= normalizedValues[7];

  winxy.x = (1.0f + normalizedValues[4]) * 0.5f * width;
  winxy.y = (1.0f - normalizedValues[5]) * 0.5f * height;

  return winxy;
}

bool nyx::unprojectPointOnPlane(
  nv::vec2f const &screenXY,
  nyx::planef const &plane, 
  nv::vec2f const &screenDims, 
  float const *viewInv,
  float const *projection,
  nv::vec3f &world
  )
{
  float where;
  nv::matrix4f viewInvMat(viewInv), projMat(projection);
  auto pickingRay = calculatePickingRay(screenXY, screenDims, viewInvMat, projMat);

  if (intersects(pickingRay, plane, where))
  {
    world = nyx::rayAt(pickingRay, where);
    return true;
  }

  return false;
}