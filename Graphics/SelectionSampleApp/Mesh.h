#pragma once

#include "pch.h"

namespace nyx
{
  class Mesh
  {
  public:

    struct Vertex
    {
      Vertex(
      float x, float y, float z, // position
      float r, float g, float b, float a = 1.0f // color
      );

      float                     position[3];
      uint8_t                   color[4]; // normalized

      static const uintptr_t    PositionOffset = 0;
      static const uintptr_t    ColorOffset = 12;
      
      // For Vertex Array Objects (VAO), enable the vertex attributes
      static void enable(); // Sets up the vertex format state
      // Reset state
      static void disable(); // Resets state related to the vertex format

      // Specify the vertex format
      // Enable Vertex Buffer Unified Memory (VBUM) for the vertex attributes
      // Enable Vertex Buffer Unified Memory (VBUM) for the indices
      static void enableVBUM(); // Sets up the vertex format state
      // Reset state
      static void disableVBUM(); // Resets state related to the vertex format

      typedef std::vector<Vertex>   Vector;
    };

    typedef uint16_t                Index;
    typedef std::vector<Index>      IndexVector;
    typedef Vertex::Vector          VertexVector;
    typedef std::vector<Mesh*>      Vector;

  public:

    int32_t                 vertexCount;            // Number of vertices in mesh
    int32_t                 indexCount;             // Number of indices in mesh
    GLuint                  vertexBuffer;           // vertex buffer object for vertices
    GLuint                  indexBuffer;            // vertex buffer object for indices
    GLuint                  paramsBuffer;           // uniform buffer object for params
    GLint                   vertexBufferSize;
    GLint                   indexBufferSize;
    GLuint64EXT             vertexBufferGPUPtr;     // GPU pointer to vertexBuffer data
    GLuint64EXT             indexBufferGPUPtr;      // GPU pointer to indexBuffer data

    static const uint32_t   VertexSize  = sizeof(nyx::Mesh::Vertex);
    static const uint32_t   IndexSize   = sizeof(nyx::Mesh::Index);

  public:

    Mesh(void);
    ~Mesh(void);

    // Does the actual rendering of the mesh
    void renderUsingVBUM(void) const;
    void renderUsingVAO(void) const;

    void setupBuffersUsingVBUM(void) const;
    void setupBuffersUsingVAO(void) const;
    void draw(void) const;

    // This method is called to update the vertex and index data for the mesh.
    // For GL_NV_vertex_buffer_unified_memory (VBUM), we ask the driver to give us
    // GPU pointers for the buffers. Later, when we render, we use these GPU pointers
    // directly. By using GPU pointers, the driver can avoid many system memory 
    // accesses which pollute the CPU caches and reduce performance.
    void update(
      VertexVector const &vertices,
      IndexVector const &indices
      );
  };
}