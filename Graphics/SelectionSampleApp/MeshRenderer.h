#pragma once

#include "pch.h"
#include "Mesh.h"

namespace nyx
{
  class MeshRenderer
  {
  public:
    struct TransformUniforms
    {
      nv::matrix4f  modelView;
      nv::matrix4f  modelViewProjection;
      int32_t       useBindlessUniforms;

    public:
      typedef std::vector<TransformUniforms> Vector;
    };

    _declspec(align(64)) struct PerMeshUniforms
    {
      nv::matrix4f  model;
      bool selected;
      //bool selected[16];
      //nv::matrix4<bool> selected;

    public:
      typedef std::vector<PerMeshUniforms> Vector;
    };

  public:
    NvGLSLProgram                *shader;
    GLuint                        perMeshUniforms;
    GLuint64EXT                   perMeshUniformsGPUPtr;
    GLsizei                       perMeshUniformsDataSize;
    GLint                         perMeshUniformsAttribLocation;
    GLuint                        transformUniforms;
    GLint                         transformUniformsLocation;
    GLuint                        perMeshUniformsCount;

  public:
    static uint32_t const         TransformUniformsSize   = sizeof(TransformUniforms);
    static uint32_t const         PerMeshUniformsSize     = sizeof(PerMeshUniforms);

  public:

    MeshRenderer(void);
    ~MeshRenderer(void);

    void initRendering(void);
    void updateUniforms(TransformUniforms const &uniforms);
    void updateUniforms(PerMeshUniforms::Vector const &uniforms);
    void updateUniforms(PerMeshUniforms const *uniforms, uint32_t uniformsCount);

    void renderForEachUniforms(Mesh const &mesh, uint32_t uniformsOffset = 0);
    void renderForUniformsAt(Mesh const &mesh, uint32_t uniformsIndex = 0);
    void renderCorrespondingly(Mesh::Vector const &meshes, uint32_t uniformsOffset = 0);

  private:

  };
}