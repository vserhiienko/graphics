#pragma once

#include "pch.h"
#include "Shapes.h"
#include "MeshRenderer.h"
#include "SelectionAssist.h"

namespace nyx
{
  class SampleApp : public NvSampleApp
  {
  public: // NvSampleApp, NvAppBase 

    SampleApp(NvPlatformContext* platform);
    ~SampleApp(void);

    virtual void initUI(void) override;
    virtual void initRendering(void) override;
    virtual void draw(void) override;

    void configurationCallback(
      NvEGLConfiguration &config
      );

    virtual void reshape(
      int32_t width, 
      int32_t height
      ) override;
    virtual bool handlePointerInput(
      NvInputDeviceType::Enum device,
      NvPointerActionType::Enum action,
      uint32_t modifiers, int32_t count,
      NvPointerEvent *points
      );
    virtual bool handleKeyInput(
      uint32_t code,
      NvKeyActionType::Enum action
      ) override;

  public:

    void projectItems();


  private:

    nyx::SelectionAssits                    m_selectionAssist;
    MeshRenderer                           *m_renderer;

    Mesh::Vector                            m_meshes;

    nv::matrix4f                            m_projectionMatrix;
    MeshRenderer::TransformUniforms         m_transformUniformsData;
    MeshRenderer::PerMeshUniforms::Vector   m_perMeshUniformsData;

    nv::vec2f                               m_groundDims;
    nv::vec2f                               m_itemDims;
    nv::vec2f                               m_indentDims;
    nv::vec2<uint16_t>                      m_itemsCount;
    std::vector<nv::vec3f>                  m_itemCenters;
    std::vector<int>                        m_selectedItems;
    std::vector<nv::vec2f>                  m_itemProjectedCenters;

    bool                                    m_projectItems;
    bool                                    m_selectionMode;
    bool                                    m_resetButtonVar;
    bool                                    m_enableMultipleSelections;

    nyx::planef                             m_groundPlane;


    // Timing related stuff
    float                                   m_t;
    float                                   m_minimumFrameDeltaTime;
  };
}

