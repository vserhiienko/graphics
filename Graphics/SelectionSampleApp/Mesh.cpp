
#include "pch.h"
#include "Mesh.h"


void nyx::Mesh::Vertex::enable()
{
  glEnableVertexArrayAttribEXT(0, 0);
  glEnableVertexArrayAttribEXT(0, 1);
}

void nyx::Mesh::Vertex::disable()
{
  glDisableVertexArrayAttribEXT(0, 0);
  glDisableVertexArrayAttribEXT(0, 1);
}

void nyx::Mesh::Vertex::enableVBUM()
{
  // Specify the vertex format
  glVertexAttribFormatNV(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex));
  glVertexAttribFormatNV(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex));
  // Enable the relevent attributes
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  // Enable Vertex Buffer Unified Memory (VBUM) for the vertex attributes
  glEnableClientState(GL_VERTEX_ATTRIB_ARRAY_UNIFIED_NV);
  // Enable Vertex Buffer Unified Memory (VBUM) for the indices
  glEnableClientState(GL_ELEMENT_ARRAY_UNIFIED_NV);
}

void nyx::Mesh::Vertex::disableVBUM()
{
  // Reset state
  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glDisableVertexAttribArray(2);
  glDisableClientState(GL_VERTEX_ATTRIB_ARRAY_UNIFIED_NV);
  glDisableClientState(GL_ELEMENT_ARRAY_UNIFIED_NV);
}

nyx::Mesh::Vertex::Vertex(
  float x, float y, float z, // position
  float r, float g, float b, float a // color
  )
{
  position[0] = x;
  position[1] = y;
  position[2] = z;
  color[0] = (uint8_t)(std::max(std::min(r, 1.0f), 0.0f) * 255.5f);
  color[1] = (uint8_t)(std::max(std::min(g, 1.0f), 0.0f) * 255.5f);
  color[2] = (uint8_t)(std::max(std::min(b, 1.0f), 0.0f) * 255.5f);
  color[3] = (uint8_t)(std::max(std::min(a, 1.0f), 0.0f) * 255.5f);
}

nyx::Mesh::Mesh(void)
{
  vertexBuffer = 0;
  indexBuffer = 0;
  vertexCount = 0;
  indexCount = 0;

  vertexBufferGPUPtr = 0;
  indexBufferGPUPtr = 0;
  vertexBufferSize = 0;
  indexBufferSize = 0;
}

nyx::Mesh::~Mesh(void)
{
  if (vertexBuffer != 0) glDeleteBuffers(1, &vertexBuffer);
  if (indexBuffer != 0) glDeleteBuffers(1, &indexBuffer);
  vertexBuffer = 0;
  indexBuffer = 0;
}

void nyx::Mesh::renderUsingVBUM(void) const
{
  setupBuffersUsingVBUM();
  draw();
}

void nyx::Mesh::setupBuffersUsingVBUM() const
{
  if (vertexBuffer == 0 || indexBuffer == 0) return;

  // Set up the pointers in GPU memory to the vertex attributes.

  glBufferAddressRangeNV(
    GL_VERTEX_ATTRIB_ARRAY_ADDRESS_NV, 0, // index
    vertexBufferGPUPtr + Vertex::PositionOffset,
    vertexBufferSize - Vertex::PositionOffset
    );
  glBufferAddressRangeNV(
    GL_VERTEX_ATTRIB_ARRAY_ADDRESS_NV, 1, // index
    vertexBufferGPUPtr + Vertex::ColorOffset,
    vertexBufferSize - Vertex::ColorOffset
    );

  // Set up the pointer in GPU memory to the index buffer

  glBufferAddressRangeNV(
    GL_ELEMENT_ARRAY_ADDRESS_NV, 0, // index
    indexBufferGPUPtr,
    indexBufferSize
    );
}

void nyx::Mesh::renderUsingVAO(void) const
{
  setupBuffersUsingVAO();
  draw();
}


void nyx::Mesh::setupBuffersUsingVAO(void) const
{
  // Set up attribute 0 for the position (3 floats)
  glVertexArrayVertexAttribOffsetEXT(
    0, // vaobj
    vertexBuffer,
    0, // index
    3, // size
    GL_FLOAT, // type
    GL_FALSE, // normalized 
    sizeof(Vertex), // stride
    Vertex::PositionOffset // offset
    );

  // Set up attribute 1 for the color (4 unsigned bytes)
  glVertexArrayVertexAttribOffsetEXT(
    0,  // vaobj
    vertexBuffer,
    1, // index
    4, // size
    GL_UNSIGNED_BYTE,  // type
    GL_TRUE,  // normalized 
    sizeof(Vertex), // stride
    Vertex::ColorOffset // offset
    );

  // Set up the indices
  glBindBuffer(
    GL_ELEMENT_ARRAY_BUFFER,
    indexBuffer
    );
}

void nyx::Mesh::draw(void) const
{
  if (vertexBuffer == 0 || indexBuffer == 0) return;

  // Do the actual drawing
  glDrawElements(
    GL_TRIANGLES,
    indexCount,
    GL_UNSIGNED_SHORT,
    0
    );
}

void nyx::Mesh::update(
  std::vector<Vertex> const &vertices,
  std::vector<Index> const &indices
  )
{
  // create vertex buffer
  if (vertexBuffer == 0) glGenBuffers(1, &vertexBuffer);
  // create index buffer
  if (indexBuffer == 0) glGenBuffers(1, &indexBuffer);

  // save vertex buffer element count
  vertexCount = vertices.size();
  // save index buffer element count
  indexCount = indices.size();

  // fill vertex buffer
  glNamedBufferDataEXT(vertexBuffer, VertexSize * vertices.size(), &vertices[0], GL_STATIC_DRAW);
  // fill index buffer
  glNamedBufferDataEXT(indexBuffer, IndexSize * indices.size(), &indices[0], GL_STATIC_DRAW);

  // make vertex buffer resident on the GPU
  glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
  glGetBufferParameterui64vNV(GL_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &vertexBufferGPUPtr);
  glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &vertexBufferSize);
  glMakeBufferResidentNV(GL_ARRAY_BUFFER, GL_READ_ONLY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // make index buffer resident on the GPU
  glBindBuffer(GL_ARRAY_BUFFER, indexBuffer);
  glGetBufferParameterui64vNV(GL_ARRAY_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &indexBufferGPUPtr);
  glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &indexBufferSize);
  glMakeBufferResidentNV(GL_ARRAY_BUFFER, GL_READ_ONLY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  CHECK_GL_ERROR();
}
