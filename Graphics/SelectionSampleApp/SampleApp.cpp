#include "pch.h"
#include "SampleApp.h"

#include <GL/GLU.h>

NvAppBase* NvAppFactory(NvPlatformContext* platform)
{
  return new nyx::SampleApp(platform);
}

void  nyx::SampleApp::configurationCallback(NvEGLConfiguration& config)
{
  config.depthBits = 24;
  config.stencilBits = 0;
  config.apiVer = NvGfxAPIVersionGL4();
}

nyx::SampleApp::SampleApp(NvPlatformContext* platform)
: NvSampleApp(platform)
{
  m_selectionMode = false;
  m_projectItems = false;

  // Required in all subclasses to avoid silent link issues
  forceLinkHack();
}

nyx::SampleApp::~SampleApp(void)
{
  delete m_renderer;
}

void nyx::SampleApp::initUI(void)
{
  if (mTweakBar)
  {
    // add controls
    mTweakBar->addValue("Selection mode", m_selectionMode);
    mTweakBar->addValue("Draw projected centres", m_projectItems);
    mTweakBar->addValue("Enable multiple selections", m_enableMultipleSelections);

    //mTweakBar->addPadding();
    //mTweakBar->addValue("Reset selection", m_resetButtonVar, true);
    mTweakBar->syncValues();
  }

  // Change the filtering for the framerate
  mFramerate->setMaxReportRate(.2f);
  mFramerate->setReportFrames(20);

  // Disable wait for vsync
  //getGLContext()->setSwapInterval(0);
}

void nyx::SampleApp::initRendering(void)
{
  if (!requireExtension("GL_NV_vertex_buffer_unified_memory")) return;
  if (!requireExtension("GL_EXT_direct_state_access")) return;
  if (!requireExtension("GL_NV_shader_buffer_load")) return;

  NvAssetLoaderAddSearchPath("SelectionSampleApp");

  // Set the initial view 
  m_transformer->setTranslationVec(nv::vec3f(0.0f, 0.0f, -4.0f));
  m_transformer->setRotationVec(nv::vec3f(_degs2radsf(30.0f), _degs2radsf(30.0f), _degs2radsf(0.0f)));

  // set desired ground dimensions
  m_groundDims.x = 10.0f;
  m_groundDims.y = 5.0f;

  // set item dimensions
  m_itemDims.x = 0.2f;
  m_itemDims.y = 0.2f;

  // set indentations
  m_indentDims = m_itemDims * 0.1f;

  nv::vec2f itemsCount, indentedItemDims;

  indentedItemDims = m_itemDims + m_indentDims;
  itemsCount = m_groundDims / indentedItemDims;
  itemsCount.x = floor(itemsCount.x);
  itemsCount.y = floor(itemsCount.y);
  m_itemsCount.x = (uint16_t)itemsCount.x;
  m_itemsCount.y = (uint16_t)itemsCount.y;

  uint16_t totalItems = m_itemsCount.x * m_itemsCount.y;

  // calculate actual ground dimensions
  m_groundDims = itemsCount * indentedItemDims;
  m_groundDims += m_indentDims;

  m_groundPlane.distance = -0.0001f;
  m_groundPlane.normal = 0.0f;
  m_groundPlane.normal.y = 1.0f;

  // create mesh renderer
  m_renderer = new MeshRenderer();
  m_renderer->initRendering();

  m_meshes.resize(2);

  // create meshes

  m_meshes[0] = new Mesh();
  m_meshes[1] = new Mesh();

  nyx::createGroundMesh(
    (*m_meshes[0]),
    nv::vec3f(0.0f, 0.0f, 0.0f),                        // pos
    nv::vec3f(m_groundDims.x, 0.0f, m_groundDims.y),    // dims
    nv::vec4f(0.3f, 0.3f, 0.3f, 0.75f)                  // color
    );
  nyx::createGroundMesh(
    (*m_meshes[1]),
    nv::vec3f(0.0f, 0.0f, 0.0f),                        // pos
    nv::vec3f(m_itemDims.x, 0.0f, m_itemDims.y),        // dims
    nv::vec4f(0.4f, 0.4f, 0.4f, 1.0f)                   // color
    );

  m_itemCenters.resize(totalItems);
  m_selectedItems.resize(totalItems);
  m_itemProjectedCenters.resize(totalItems);
  m_perMeshUniformsData.resize(1 + totalItems);
  memset(&m_selectedItems[0], 0, sizeof(bool)* m_selectedItems.size());

  // setup ground
  nv::matrix4f groundModelMatrix;
  nv::translation(groundModelMatrix, 0.0f, -0.0001f, 0.0f);
  memset(m_perMeshUniformsData.data(), 0, MeshRenderer::PerMeshUniformsSize);
  m_perMeshUniformsData.front().model = groundModelMatrix;
  m_perMeshUniformsData.front().selected = false;

  // setup items
  uint16_t itemIndex = 1;
  for (uint16_t y = 0; y < m_itemsCount.y; y++)
  for (uint16_t x = 0; x < m_itemsCount.x; x++, itemIndex++)
  {
    nv::vec2f translation = m_indentDims;
    translation.x += x * indentedItemDims.x + indentedItemDims.x * 0.45f - m_groundDims.x * 0.5f;
    translation.y += y * indentedItemDims.y + indentedItemDims.y * 0.45f - m_groundDims.y * 0.5f;

    m_itemCenters[itemIndex - 1].x = translation.x;
    m_itemCenters[itemIndex - 1].y = 0.00f;
    m_itemCenters[itemIndex - 1].z = translation.y;

    memset(m_perMeshUniformsData.data() + itemIndex, 0, MeshRenderer::PerMeshUniformsSize);
    nv::translation(m_perMeshUniformsData[itemIndex].model, translation.x, 0.00f, translation.y);
    m_perMeshUniformsData[itemIndex].selected = false;
  }

  m_renderer->updateUniforms(m_perMeshUniformsData);

}

void nyx::SampleApp::projectItems()
{
  for (uint32_t i = 0; i < m_itemCenters.size(); i++)
  {
    nv::vec4f pos;
    pos.x = m_itemCenters[i].x;
    pos.y = m_itemCenters[i].y;
    pos.z = m_itemCenters[i].z;
    pos.w = 1.0f;

    m_itemProjectedCenters[i] = nyx::projectPoint(
      pos, m_selectionAssist.screenDimensions,
      m_transformUniformsData.modelView._array,
      m_projectionMatrix._array
      );

    if (m_selectedItems[i] == 1)
      m_selectionAssist.drawCircle(
      m_itemProjectedCenters[i].x,
      m_itemProjectedCenters[i].y,
      8.0f, 3, 0.1f, 0.0f, 0.8f
      );
    else
      m_selectionAssist.drawCircle(
      m_itemProjectedCenters[i].x,
      m_itemProjectedCenters[i].y,
      5.0f, 3
      );

  }
}

void nyx::SampleApp::draw(void)
{
  nv::matrix4f modelviewMatrix;

  glClearColor(0.5, 0.5, 0.5, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  //glEnable(GL_BLEND);
  //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  float dt, deltaTime;

  deltaTime = getFrameDeltaTime();
  if (deltaTime < m_minimumFrameDeltaTime)
    m_minimumFrameDeltaTime = deltaTime;
  dt = std::min(0.00005f / m_minimumFrameDeltaTime, .01f);
  m_t += dt;

  modelviewMatrix = m_transformer->getModelViewMat();
  m_transformUniformsData.modelView = modelviewMatrix;
  m_transformUniformsData.modelViewProjection = m_projectionMatrix * modelviewMatrix;

  m_renderer->updateUniforms(m_transformUniformsData);

  m_renderer->renderForUniformsAt((*m_meshes[0]), 0);
  m_renderer->renderForEachUniforms((*m_meshes[1]), 1);

  if (m_projectItems)
  {
    projectItems();
  }

  if (!m_selectionMode)
  {
    m_selectionAssist.updateProjectedPoints(
      m_transformUniformsData.modelView._array,
      m_projectionMatrix._array
      );
  }

  if (m_selectionMode || true)
  {
    m_selectionAssist.draw();
  }
}

void nyx::SampleApp::reshape(
  int32_t width,
  int32_t height
  )
{
  glViewport(0, 0, (GLint)width, (GLint)height);
  m_selectionAssist.setScreenDimensions(width, height);
  //nv::ortho2D(m_projectionMatrix, 0.0f, (float)width, 0.0f, (float)height);
  //nv::orthographic(m_projectionMatrix, (float)width * -0.5f, (float)width * 0.5f, (float)height * -0.5f, (float)height * 0.5f, 0.1f, 100.0f);
  nv::perspective(m_projectionMatrix, _degs2radsf(45.0f), (float)width / (float)height, 0.1f, 10.0f);

  CHECK_GL_ERROR();
}

bool nyx::SampleApp::handlePointerInput(
  NvInputDeviceType::Enum device,
  NvPointerActionType::Enum action,
  uint32_t modifiers,
  int32_t count,
  NvPointerEvent* points
  )
{
  if (m_selectionMode && !!points && !!count)
  {
    nv::vec2f screenPos;
    screenPos.x = points->m_x;
    screenPos.y = points->m_y;

    switch (action)
    {
    case NvPointerActionType::UP:
      m_selectionAssist.endSelection();

      m_selectionAssist.findPointsInSelection(
        &m_itemProjectedCenters[0],
        &m_selectedItems[0],
        m_itemCenters.size(),
        !m_enableMultipleSelections
        );
      m_selectionAssist.unprojectPoints(
        m_groundPlane,
        m_transformUniformsData.modelView._array,
        m_projectionMatrix._array
        );

      break;
    case NvPointerActionType::DOWN:
      m_selectionAssist.beginSelection();
      break;
    case NvPointerActionType::MOTION:
      m_selectionAssist.addPoint(screenPos);
      break;
    }

    return true;
  }
 /* else if (!m_selectionMode)
  {
    m_selectionAssist.updateProjectedPoints(
      m_transformUniformsData.modelView._array,
      m_projectionMatrix._array
      );
  }*/

  return false;
}

bool nyx::SampleApp::handleKeyInput(
  uint32_t code,
  NvKeyActionType::Enum action
  )
{
  return false;
}