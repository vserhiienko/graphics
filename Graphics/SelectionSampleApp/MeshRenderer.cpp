#include "pch.h"
#include "MeshRenderer.h"


nyx::MeshRenderer::MeshRenderer(void)
{
  perMeshUniformsCount = 0;

  perMeshUniformsAttribLocation = 2;
  transformUniformsLocation = 1;
}

nyx::MeshRenderer::~MeshRenderer(void)
{

}

void nyx::MeshRenderer::initRendering(void)
{
  NvAssetLoaderAddSearchPath("SelectionSampleApp");

  shader = NvGLSLProgram::createFromFiles(
    "shaders/bindless_vertex.glsl",
    "shaders/color_fragment.glsl"
    );
  /*perMeshUniformsAttribLocation
    = shader->getAttribLocation(
    "bindlessPerMeshUniformsPtr", true
    );*/
  /*transformUniformsLocation
    = shader->getUniformLocation(
    "TransformParams"
    );*/

  // create Uniform Buffer Object (UBO) for transform data and initialize 
  glGenBuffers(
    1, &transformUniforms
    );
  glNamedBufferDataEXT(
    transformUniforms,
    sizeof(TransformUniforms),
    &transformUniforms,
    GL_STREAM_DRAW // usage
    );

  // create Uniform Buffer Object (UBO) for param data and initialize
  glGenBuffers(
    1, &perMeshUniforms
    );

  CHECK_GL_ERROR();
}

void nyx::MeshRenderer::updateUniforms(TransformUniforms const &uniforms)
{
  // Give the uniform data to the GPU
  glNamedBufferSubDataEXT(
    transformUniforms,
    0, // offset
    sizeof(uniforms),
    &uniforms
    );

  CHECK_GL_ERROR();
}

void nyx::MeshRenderer::updateUniforms(PerMeshUniforms const *uniforms, uint32_t uniformsCount)
{
  if (!uniforms || !uniformsCount) return;
  perMeshUniformsCount = uniformsCount;

  // calculate buffer size
  uint32_t byteSize = uniformsCount * PerMeshUniformsSize;

  // Give the uniform data to the GPU
  glNamedBufferDataEXT(perMeshUniforms, byteSize, uniforms, GL_STREAM_DRAW); // usage

  // Get the GPU pointer for the per mesh uniform buffer and make the buffer resident on the GPU
  glBindBuffer(GL_UNIFORM_BUFFER, perMeshUniforms);
  glGetBufferParameterui64vNV(GL_UNIFORM_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &perMeshUniformsGPUPtr);
  glGetBufferParameteriv(GL_UNIFORM_BUFFER, GL_BUFFER_SIZE, &perMeshUniformsDataSize);
  glMakeBufferResidentNV(GL_UNIFORM_BUFFER, GL_READ_ONLY);
  glBindBuffer(GL_UNIFORM_BUFFER, 0);

  CHECK_GL_ERROR();
}

void nyx::MeshRenderer::updateUniforms(PerMeshUniforms::Vector const &uniforms)
{
  perMeshUniformsCount = uniforms.size();

  // Give the uniform data to the GPU
  glNamedBufferDataEXT(
    perMeshUniforms,
    uniforms.size() * PerMeshUniformsSize,
    uniforms.data(),
    GL_STREAM_DRAW // usage
    );


  // Get the GPU pointer for the per mesh uniform buffer and make the buffer resident on the GPU
  glBindBuffer(GL_UNIFORM_BUFFER, perMeshUniforms);
  glGetBufferParameterui64vNV(GL_UNIFORM_BUFFER, GL_BUFFER_GPU_ADDRESS_NV, &perMeshUniformsGPUPtr);
  glGetBufferParameteriv(GL_UNIFORM_BUFFER, GL_BUFFER_SIZE, &perMeshUniformsDataSize);
  glMakeBufferResidentNV(GL_UNIFORM_BUFFER, GL_READ_ONLY);
  glBindBuffer(GL_UNIFORM_BUFFER, 0);

  CHECK_GL_ERROR();
}

void nyx::MeshRenderer::renderForUniformsAt(Mesh const &mesh, uint32_t pos /*= 0*/)
{
  // bind uniforms
  glBindBufferBase(
    GL_UNIFORM_BUFFER,
    transformUniformsLocation,
    transformUniforms
    );

  GLuint64EXT meshUniformGPUPtr;
  // Compute a GPU pointer for the per mesh uniforms for this mesh
  meshUniformGPUPtr = perMeshUniformsGPUPtr + PerMeshUniformsSize * pos;

  // Pass a GPU pointer to the vertex shader for the per mesh uniform data via a vertex attribute
  glVertexAttribI2uiEXT(
    perMeshUniformsAttribLocation,
    (uint32_t)(meshUniformGPUPtr & 0xFFFFFFFF),
    (uint32_t)((meshUniformGPUPtr >> 32) & 0xFFFFFFFF)
    );

  shader->enable();
  nyx::Mesh::Vertex::enableVBUM();

  mesh.renderUsingVBUM();

  nyx::Mesh::Vertex::disableVBUM();
  shader->disable();

  CHECK_GL_ERROR();
}

void nyx::MeshRenderer::renderCorrespondingly(Mesh::Vector const &meshes, uint32_t offset)
{
  uint32_t pos, npos;

  // bind uniforms
  glBindBufferBase(
    GL_UNIFORM_BUFFER,
    transformUniformsLocation,
    transformUniforms
    );

  shader->enable();
  nyx::Mesh::Vertex::enableVBUM();

  pos = 0;
  npos = std::min(meshes.size(), perMeshUniformsCount - offset);

  auto mesh = meshes.data();
  GLuint64EXT meshUniformGPUPtr;

  while (pos < npos)
  {
    meshUniformGPUPtr = perMeshUniformsGPUPtr + PerMeshUniformsSize * (pos + offset);

    glVertexAttribI2iEXT(
      perMeshUniformsAttribLocation,
      (int32_t)(meshUniformGPUPtr & 0xFFFFFFFF),
      (int32_t)((meshUniformGPUPtr >> 32) & 0xFFFFFFFF)
      );
    (*mesh)->renderUsingVBUM();
    mesh++, pos++;
  }

  nyx::Mesh::Vertex::disableVBUM();
  shader->disable();

  CHECK_GL_ERROR();
}

void nyx::MeshRenderer::renderForEachUniforms(Mesh const &mesh, uint32_t offset)
{
  uint32_t pos, npos;
  GLuint64EXT meshUniformGPUPtr;

  // bind uniforms
  glBindBufferBase(
    GL_UNIFORM_BUFFER,
    transformUniformsLocation,
    transformUniforms
    );

  shader->enable();
  nyx::Mesh::Vertex::enableVBUM();
  mesh.setupBuffersUsingVBUM();

  pos = 0;
  npos = perMeshUniformsCount - offset;

  while (pos < npos)
  {
    meshUniformGPUPtr = perMeshUniformsGPUPtr + PerMeshUniformsSize * (pos + offset);

    glVertexAttribI2uiEXT(
      perMeshUniformsAttribLocation,
      (uint32_t)(meshUniformGPUPtr & 0xFFFFFFFF), // x
      (uint32_t)((meshUniformGPUPtr >> 32) & 0xFFFFFFFF) // y
      );

    mesh.draw();
    pos++;
  }

  nyx::Mesh::Vertex::disableVBUM();
  shader->disable();

  CHECK_GL_ERROR();
}