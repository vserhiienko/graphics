#pragma once

#include "pch.h"
#include "Mesh.h"
#define _degs2radsf(d) (d * NV_PI / 180.0f)

namespace nyx
{

  /// Geometric Primitives

  // The plane will be defined in the standard manner, 
  // by all points p such that p n = d
  class planef
  {
  public:
    nv::vec3f   normal;
    float       distance;
  };

  // A ray intersects a plane in 3D at a point. 
  // Let the ray be defined parametrically by p(t) = 0 + t d
  class rayf
  {
  public:
    nv::vec3f   origin;
    nv::vec3f   direction;
  };

  class linef
  {
  public:
    float k, b;
  };

  class linesegf
  {
  public:
    nyx::linef l;
    nv::vec2f a, b;
  };

  class aabbf
  {
  public:
    nv::vec3f   min;
    nv::vec3f   max;
  };

  class obbf : public aabbf
  {
  public:
    nv::vec3f         pos;
    nv::quaternionf   orient;

  };

  /// Geometric Tests & Utilities

  float dot(nv::vec2f const &a, nv::vec2f const &b);
  float cross(nv::vec2f const &a, nv::vec2f const &b);
  float substituteX(nv::vec2f const &p, nyx::linef const &l);

  void updateLineSegment(nyx::linesegf &seg);
  nv::vec3f rayAt(nyx::rayf const &ray, float t);
  nv::vec2f createPointFromLineSegs(nyx::linesegf const &ls);
  nyx::linef createLineFromPoints(nv::vec2f const &a, nv::vec2f const &b);
  nyx::linesegf createLineSegFromPoints(nv::vec2f const &a, nv::vec2f const &b);

  bool linesCross(nyx::linef const &a, nyx::linef const &b);
  bool lineSegsCross(nyx::linesegf const &a, nyx::linesegf const &b);
  bool isMouseOverPoint(nv::vec2f const &p1, nv::vec2f const &p2, float r);
  bool isPointInsidePolygon(nv::vec2f const &p, nv::vec2f const *polygon, uint32_t polygonSize, float testDistance = 10000.0f);
  bool intersects(nyx::rayf const &ray, nyx::planef const &plane, float &t);
  bool pointBelongsToLine(nv::vec2f const &p, nyx::linef const &l);
  bool areLinesEqual(nyx::linef const &a, nyx::linef const &b);
  bool areLinesParallel(nyx::linef const &a, nyx::linef const &b);
  bool areLinesPerpendicular(nyx::linef const &a, nyx::linef const &b);
  bool calculateCrossPoint(nyx::linef const &a, nyx::linef const &b, nv::vec2f &cross);
  bool calculateCrossPoint(nyx::linesegf const &a, nyx::linesegf const &b, nv::vec2f &cross);

  //void inverseMatrix4(float *src);

  void randomColor(float &r, float &g, float &b);
  void createBoxMesh(nyx::Mesh &mesh, nv::vec3f pos, nv::vec3f dim, nv::vec4f rgb);
  void createGroundMesh(nyx::Mesh &mesh, nv::vec3f pos, nv::vec3f dim, nv::vec4f rgb);
  nyx::rayf calculatePickingRay(nv::vec2f const &mouse, nv::vec2f const &screen, nv::matrix4f const &viewInv, nv::matrix4f const &projection);

  nv::vec2f projectPoint(nv::vec4f const &obj, nv::vec2f const &screenDims, float const *view, float const *projection);
  bool unprojectPointOnPlane(nv::vec2f const &screenXY, nyx::planef const &plane, nv::vec2f const &screenDims, float const *viewInv, float const *projection, nv::vec3f &world);
}