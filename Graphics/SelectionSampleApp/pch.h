#pragma once

#include <vector>
#include <algorithm>

#include <NV/NvLogs.h>
#include <NV/NvMath.h>
#include <NvFoundation.h>
#include <NV/NvStopWatch.h>
#include <NvUI/NvTweakBar.h>
#include <NV/NvPlatformGL.h>
#include <NvGLUtils/NvImage.h>
#include <NvAppBase/NvSampleApp.h>
#include <NvGLUtils/NvGLSLProgram.h>
#include <NvAssetLoader/NvAssetLoader.h>
#include <NvAppBase/NvFramerateCounter.h>
#include <NvAppBase/NvInputTransformer.h>