#include "WaterHFSimulation.h"
USING_NS_SOKO;
#define _Soko_enable_simulation 1

WaterHFSimulation::WaterHFSimulation()
  : dropShader(0)
  , updateShader(0)
  , normalShader(0)
  , sphereShader(0)
  , planeVertexBuffer(0)
  , planeIndexBuffer(0)
{
  const int mult = 1;

  textureW = 128 * mult;
  textureH = 128 * mult;
  planeDetailX = 1;
  planeDetailY = 1;
  textureD.x = 1.0f / float(textureW);
  textureD.y = 1.0f / float(textureH);

  causticsW = 512 * mult;
  causticsH = 512 * mult;
  causticsDetailX = 200;
  causticsDetailY = 200;

  light.x = 1.0f;
  light.y = 2.0f;
  light.z = -1.0f;
  nv::normalize(light);
}

GLuint WaterHFSimulation::getWaterTexture() const
{
  return fboA->colorTexture;
}

GLuint WaterHFSimulation::getCausticsTexture(void) const
{
  return fboCaustics->colorTexture;
}

void WaterHFSimulation::swapTextures()
{
  std::swap(fboA, fboB);
}

bool WaterHFSimulation::beginDrawTo()
{
  if (fboB)
  {
    //glGetIntegerv(GL_VIEWPORT, storedViewport); // store viewport
    //glGetIntegerv(0x8ca6, (GLint*)&fboPrev);

    fboB->bind();
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    fboB->checkStatus();
    return true; // further rendering...
  }

  return false;
}

void WaterHFSimulation::drawPlaneBuffers(GLint positionA)
{
  glEnableVertexAttribArray(positionA); sokoCheckGLError;
  glVertexAttribPointer(positionA, 3, GL_FLOAT, GL_FALSE, 0, &planeVertices[0]); sokoCheckGLError;
  glDrawElements(GL_TRIANGLE_STRIP, planeTris.size(), GL_UNSIGNED_SHORT, &planeTris[0]); sokoCheckGLError;
}

void WaterHFSimulation::drawCausticsBuffers()
{
  glEnableVertexAttribArray(causticsShaderPositionA); sokoCheckGLError;
  glVertexAttribPointer(causticsShaderPositionA, 3, GL_FLOAT, GL_FALSE, 0, &waterVertices[0]); sokoCheckGLError;
  glDrawElements(GL_TRIANGLE_STRIP, waterTris.size(), GL_UNSIGNED_SHORT, &waterTris[0]); sokoCheckGLError;
}

void WaterHFSimulation::endDrawTo()
{
  // xxxA is always up-to-date resource
  swapTextures();

  // restore viewport
  //glBindFramebuffer(GL_FRAMEBUFFER, fboPrev);
  //glViewport(storedViewport[0], storedViewport[1], storedViewport[2], storedViewport[3]); 
}

void WaterHFSimulation::addDrop(float x, float y, float r, float s)
{
#if _Soko_enable_simulation
  if (beginDrawTo())
  {
    dropShader->enable();
    dropShader->bindTexture2D(dropShaderTextureU, 0, getWaterTexture());
    dropShader->setUniform2f(dropShaderCenterU, x, y);
    dropShader->setUniform1f(dropShaderRadiusU, r);
    dropShader->setUniform1f(dropShaderStrengthU, s);
    drawPlaneBuffers(dropShaderPositionA);
    dropShader->disable();
    endDrawTo();
  }
#endif
}

void WaterHFSimulation::moveSphere(nv::vec2f oldCenter, nv::vec2f newCenter, float r)
{
#if _Soko_enable_simulation && 0
  if (beginDrawTo())
  {
    sphereShader->enable();
    bindTexture(sphereShaderTextureU);
    glUniform2f(sphereShaderOldCenterU, oldCenter.x, oldCenter.y); sokoCheckGLError;
    glUniform2f(sphereShaderNewCenterU, newCenter.x, newCenter.y); sokoCheckGLError;
    glUniform1f(sphereShaderRadiusU, r); sokoCheckGLError;
    drawPlaneBuffers(sphereShaderPositionA);
    sphereShader->disable();
    endDrawTo();
  }
#endif
}

void WaterHFSimulation::stepSimulation()
{
#if _Soko_enable_simulation
  if (beginDrawTo())
  {
    updateShader->enable();
    updateShader->bindTexture2D(updateShaderTextureU, 0, getWaterTexture());
    updateShader->setUniform2f(updateShaderDeltaU, textureD.x, textureD.y);
    drawPlaneBuffers(updateShaderPositionA);
    updateShader->disable();
    endDrawTo();
  }
#endif
}

void WaterHFSimulation::updateNormals()
{
#if _Soko_enable_simulation
  if (beginDrawTo())
  {
    normalShader->enable();
    normalShader->bindTexture2D(normalShaderTextureU, 0, getWaterTexture());
    normalShader->setUniform2f(normalShaderDeltaU, textureD.x, textureD.y);
    drawPlaneBuffers(normalShaderPositionA);
    normalShader->disable();
    endDrawTo();
  }
#endif
}

void WaterHFSimulation::updateCaustics()
{
  if (!fboCaustics || !causticsShader) 
    return;

#if _Soko_enable_simulation
  fboCaustics->bind();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  causticsShader->enable();
  causticsShader->bindTexture2D(causticsShaderWaterU, 0, getWaterTexture());
  causticsShader->setUniform3f(causticsShaderLightU, light.x, light.y, light.z);
  drawCausticsBuffers();
  causticsShader->disable();
#endif
}

bool WaterHFSimulation::initializeTextures()
{
  sokoTraceFuncBegin;

  GLint 
    width = textureW, 
    height = textureH;

  GLint 
    format = GL_RGBA, 
    intFormat = GL_RGBA,
    type = GL_UNSIGNED_BYTE;

  fboDesc.width = textureW;
  fboDesc.height = textureH;
  fboDesc.color.type = type;
  fboDesc.color.format = format;
  fboDesc.color.filter = GL_NEAREST;
  fboDesc.color.wrap = GL_CLAMP_TO_EDGE;
  fboDesc.depth.type = GL_UNSIGNED_INT;
  fboDesc.depth.format = GL_DEPTH_COMPONENT;
  fboDesc.depth.filter = GL_NEAREST;
  fboDesc.depth.wrap = GL_CLAMP_TO_EDGE;
  fboA = new NvSimpleFBO(fboDesc);
  fboB = new NvSimpleFBO(fboDesc);

  format = GL_RGBA;
  type = GL_UNSIGNED_BYTE;
  fboDesc.width = textureW;
  fboDesc.height = textureH;
  fboCaustics = new NvSimpleFBO(fboDesc);

  sokoTraceFuncEnd;

  return !sokoCheckGLError;
}

bool WaterHFSimulation::initializeShaders()
{
  sokoTraceFuncBegin;

  // reload shaders
  if (dropShader) delete dropShader, dropShader = 0;
  if (updateShader) delete updateShader, updateShader = 0;
  if (normalShader) delete normalShader, normalShader = 0;
  if (sphereShader) delete sphereShader, sphereShader = 0;

  dropShader = NvGLSLProgram::createFromFiles(
    "shaders/water.glsl.vert", 
    "shaders/waterDrop.frag"
    ); sokoCheckGLError;
  updateShader = NvGLSLProgram::createFromFiles(
    "shaders/water.glsl.vert", 
    "shaders/waterUpdate.frag"
    ); sokoCheckGLError;
  normalShader = NvGLSLProgram::createFromFiles(
    "shaders/water.glsl.vert", 
    "shaders/waterNormal.frag"
    ); sokoCheckGLError;
  sphereShader = NvGLSLProgram::createFromFiles(
    "shaders/water.glsl.vert", 
    "shaders/waterSphere.frag"
    ); sokoCheckGLError;
  causticsShader = NvGLSLProgram::createFromFiles(
    "shaders/waterCaustics.vert",
    "shaders/waterCaustics.frag"
    ); sokoCheckGLError;

  if (!!dropShader
    && !!updateShader
    && !!normalShader
    && !!sphereShader
    && dropShader->getProgram()
    && updateShader->getProgram()
    && normalShader->getProgram()
    && sphereShader->getProgram()
    && !sokoCheckGLError
    )
  {
    dropShaderPositionA = dropShader->getAttribLocation("a_position"); sokoCheckGLError;
    dropShaderTextureU = dropShader->getUniformLocation("u_texture"); sokoCheckGLError;
    dropShaderCenterU = dropShader->getUniformLocation("u_center"); sokoCheckGLError;
    dropShaderRadiusU = dropShader->getUniformLocation("u_radius"); sokoCheckGLError;
    dropShaderStrengthU = dropShader->getUniformLocation("u_strength"); sokoCheckGLError;
    sokoLogD("dropShader us: %d %d %d %d"
      , dropShaderTextureU
      , dropShaderCenterU
      , dropShaderRadiusU
      , dropShaderStrengthU
      );
    updateShaderPositionA = updateShader->getAttribLocation("a_position"); sokoCheckGLError;
    updateShaderTextureU = updateShader->getUniformLocation("u_texture"); sokoCheckGLError;
    updateShaderDeltaU = updateShader->getUniformLocation("u_delta"); sokoCheckGLError;
    sokoLogD("updateShader us: %d %d"
      , updateShaderTextureU
      , updateShaderDeltaU
      );
    normalShaderPositionA = normalShader->getAttribLocation("a_position"); sokoCheckGLError;
    normalShaderTextureU = normalShader->getUniformLocation("u_texture"); sokoCheckGLError;
    normalShaderDeltaU = normalShader->getUniformLocation("u_delta"); sokoCheckGLError;
    sokoLogD("normalShader us: %d %d"
      , normalShaderTextureU
      , normalShaderDeltaU
      );
    sphereShaderPositionA = sphereShader->getAttribLocation("a_position"); sokoCheckGLError;
    sphereShaderTextureU = sphereShader->getUniformLocation("u_texture"); sokoCheckGLError;
    sphereShaderOldCenterU = sphereShader->getUniformLocation("u_oldCenter"); sokoCheckGLError;
    sphereShaderNewCenterU = sphereShader->getUniformLocation("u_newCenter"); sokoCheckGLError;
    sphereShaderRadiusU = sphereShader->getUniformLocation("u_radius"); sokoCheckGLError;
    sokoLogD("sphereShader us: %d %d %d %d"
      , sphereShaderTextureU
      , sphereShaderOldCenterU
      , sphereShaderNewCenterU
      , sphereShaderRadiusU
      );
    causticsShaderPositionA = causticsShader->getAttribLocation("position"); sokoCheckGLError;
    causticsShaderWaterU = causticsShader->getUniformLocation("water"); sokoCheckGLError;
    causticsShaderLightU = causticsShader->getUniformLocation("light"); sokoCheckGLError;
    sokoLogD("causticsShader us: %d %d"
      , causticsShaderWaterU
      , causticsShaderLightU
      );

    sokoTraceFuncEnd;

    return dropShader->getProgram()
      && updateShader->getProgram()
      && normalShader->getProgram()
      && sphereShader->getProgram()
      && !sokoCheckGLError;
  }
  else
  {
    sokoLogE("failed to compile shaders");
    sokoTraceFuncEnd;
    return false;
  }
}

bool WaterHFSimulation::initializePlane()
{
  sokoTraceFuncBegin;

  planeVertices.clear();
  planeNormals.clear();
  planeTris.clear();
  planeCoords.clear();

  waterVertices.clear();
  waterNormals.clear();
  waterTris.clear();
  waterCoords.clear();

  /// create mesh

  for (int y = 0; y <= planeDetailY; y++)
  {
    float t = (float)y / (float)planeDetailY;
    for (int x = 0; x <= planeDetailX; x++)
    {
      float s = (float)x / (float)planeDetailX;
      planeVertices.push_back(nv::vec3f(2 * s - 1, 2 * t - 1, 0));
      planeCoords.push_back(nv::vec2f(s, t));
      planeNormals.push_back(nv::vec3f(0, 0, 1));

      if (x < planeDetailX && y < planeDetailY)
      {
        int i = x + y * (planeDetailX + 1);

        planeTris.push_back(i);
        planeTris.push_back(i + 1);
        planeTris.push_back(i + planeDetailX + 1);
        planeTris.push_back(i + planeDetailX + 1);
        planeTris.push_back(i + 1);
        planeTris.push_back(i + planeDetailX + 2);
      }
    }
  }

  for (int y = 0; y <= causticsDetailY; y++)
  {
    float t = (float)y / (float)causticsDetailY;
    for (int x = 0; x <= causticsDetailX; x++)
    {
      float s = (float)x / (float)causticsDetailX;
      waterVertices.push_back(nv::vec3f(2 * s - 1, 2 * t - 1, 0));
      waterCoords.push_back(nv::vec2f(s, t));
      waterNormals.push_back(nv::vec3f(0, 0, 1));

      if (x < causticsDetailX && y < causticsDetailY)
      {
        int i = x + y * (planeDetailX + 1);
        waterTris.push_back(i);
        waterTris.push_back(i + 1);
        waterTris.push_back(i + causticsDetailX + 1);
        waterTris.push_back(i + causticsDetailX + 1);
        waterTris.push_back(i + 1);
        waterTris.push_back(i + causticsDetailX + 2);
      }
    }
  }



  sokoTraceFuncEnd;

  return !sokoCheckGLError;
}

bool WaterHFSimulation::init()
{
  return initializeTextures() // first, initialize textures & render/frame buffers
    && initializeShaders() // download water simulation shaders & check for uniforms
    && initializePlane(); // initialize vertex/index buffers for plane mesh
}

