#pragma once

#include <NV/NvLogs.h>
#include <NV/NvStopWatch.h>
#include <NvUI/NvTweakBar.h>
#include <NvGLUtils/NvImage.h>
#include <NvAppBase/NvSampleApp.h>
#include <NvGLUtils/NvGLSLProgram.h>
#include <NvAssetLoader/NvAssetLoader.h>
#include <NvAppBase/NvFramerateCounter.h>

namespace nyx
{

  class BasicComputeSampleApp
    : public NvSampleApp
  {
  public:
    BasicComputeSampleApp(NvPlatformContext *context);
    ~BasicComputeSampleApp();

    void initUI(void);
    void initRendering(void);
    void draw(void);
    void reshape(int32_t width, int32_t height);
    bool handleGamepadChanged(uint32_t changedPadFlags);
    void configurationCallback(NvEGLConfiguration& config);

  public:

    void invertColors(GLuint inputTex, GLuint outputTex, int width, int height);
    void drawImage(GLuint texture);

    NvFramerateCounter* m_framerate;
    NvGLSLProgram* m_blitProg;
    NvGLSLProgram* m_computeProg;
    NvImage * m_sourceImage;
    GLuint m_sourceTexture;
    GLuint m_resultTexture;
    bool m_enableFilter;
    float m_aspectRatio;
    int m_workgroupSize;

  };
}
