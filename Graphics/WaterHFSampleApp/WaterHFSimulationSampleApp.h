#pragma once
#include <TextureRenderer.h>
#include <WaterHFSimulation.h>

namespace soko
{
  class WaterHFSimulationSampleApp
    : public NvSampleApp
  {
  public:
    WaterHFSimulationSampleApp(NvPlatformContext *context);
    ~WaterHFSimulationSampleApp(void);


    void initUI(void);
    void initRendering(void);
    void update(void);
    void draw(void);
    void reshape(int32_t width, int32_t height);
    bool handleGamepadChanged(uint32_t changedPadFlags);
    void configurationCallback(NvEGLConfiguration& config);

    virtual bool handlePointerInput(NvInputDeviceType::Enum device, NvPointerActionType::Enum action, uint32_t modifiers, int32_t count, NvPointerEvent* points);
    virtual bool handleKeyInput(uint32_t code, NvKeyActionType::Enum action);

  public:
    void freeGLBindings();

    WaterHFSimulation simulator;
    TextureRenderer renderer;
    float m_aspectRatio;

    bool drawWater;
    bool updateWater;

  };
}
