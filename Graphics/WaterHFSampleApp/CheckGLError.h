#pragma once
#include <Log.h>

namespace soko
{
  inline static bool checkGLError(void)
  {
    GLint error = glGetError();
    if (error)
    {
      const char* errorString = 0;
      switch (error)
      {
      case GL_INVALID_ENUM: errorString = "GL_INVALID_ENUM"; break;
      case GL_INVALID_FRAMEBUFFER_OPERATION: errorString = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
      case GL_INVALID_VALUE: errorString = "GL_INVALID_VALUE"; break;
      case GL_INVALID_OPERATION: errorString = "GL_INVALID_OPERATION"; break;
      case GL_OUT_OF_MEMORY: errorString = "GL_OUT_OF_MEMORY"; break;
      default: errorString = "unknown error"; break;
      }

      sokoLogE("%s", errorString);
    }

    return error != 0;
  }

  inline static bool checkGLError(const char* file, int32_t line)
  {
    GLint error = glGetError();
    if (error)
    {
      const char* errorString = 0;
      switch (error)
      {
      case GL_INVALID_ENUM: errorString = "GL_INVALID_ENUM"; break;
      case GL_INVALID_FRAMEBUFFER_OPERATION: errorString = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
      case GL_INVALID_VALUE: errorString = "GL_INVALID_VALUE"; break;
      case GL_INVALID_OPERATION: errorString = "GL_INVALID_OPERATION"; break;
      case GL_OUT_OF_MEMORY: errorString = "GL_OUT_OF_MEMORY"; break;
      default: errorString = "unknown error"; break;
      }

      sokoLogE("%s:%d: %s", file, line, errorString);
    }

    return error != 0;
  }
}

#define sokoCheckGLError soko::checkGLError(__FILE__, __LINE__)
#if defined(_DEBUG) || defined(DEBUG) || defined(PROFILE)
#define sokoCheckGLErrorD soko::checkGLError(__FILE__, __LINE__)
#else
#define sokoCheckGLErrorD
#endif