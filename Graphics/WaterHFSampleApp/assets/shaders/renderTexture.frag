
#extension GL_ARB_texture_float : enable
#extension GL_OES_standard_derivatives : enable
precision mediump float;

uniform sampler2D u_sourceTex;
varying vec2 v_texCoord;
void main(void)
{
	vec4 info = abs(texture2D(u_sourceTex, v_texCoord));
    gl_FragColor = info; //sqrt(sqrt(sqrt(info)));
	//gl_FragColor.y = gl_FragColor.w;
    //gl_FragColor.xyz = sqrt(info.z / 100.0f);
	//gl_FragColor.xy *= 0.01f;
}