
#extension GL_ARB_texture_float : enable
#extension GL_OES_standard_derivatives : enable
precision mediump float;

const float PI = 3.141592653589793;
uniform sampler2D u_texture;
uniform vec2 u_center;
uniform float u_radius;
uniform float u_strength;
varying vec2 v_coord;
void main()
 {
    /* get vertex info */
    vec4 info = texture2D(u_texture, v_coord);
    /* add the drop to the height */
    float drop = max(0.0, 1.0 - length(u_center * 0.5 + 0.5 - v_coord) / u_radius);
    drop = 0.5 - cos(drop * PI) * 0.5;
    info.r += drop * u_strength;
    gl_FragColor = info;
}

