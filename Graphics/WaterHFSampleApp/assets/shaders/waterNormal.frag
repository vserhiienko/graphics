
#extension GL_ARB_texture_float : enable
#extension GL_OES_standard_derivatives : enable
precision mediump float;

uniform sampler2D u_texture;
uniform vec2 u_delta;
varying vec2 v_coord;
void main() 
{
    /* get vertex info */
    vec4 info = texture2D(u_texture, v_coord);
    /* update the normal */
    vec3 dx = vec3(u_delta.x, texture2D(u_texture, vec2(v_coord.x + u_delta.x, v_coord.y)).r - info.r, 0.0);
    vec3 dy = vec3(0.0, texture2D(u_texture, vec2(v_coord.x, v_coord.y + u_delta.y)).r - info.r, u_delta.y);
    info.ba = normalize(cross(dy, dx)).xz;
    gl_FragColor = info;
}

