uniform sampler2D u_texture;
uniform vec3 u_oldCenter;
uniform vec3 u_newCenter;
uniform float u_radius;
varying vec2 v_coord;

float volumeInSphere(vec3 center)
{
    vec3 toCenter = vec3(v_coord.x * 2.0 - 1.0, 0.0, v_coord.y * 2.0 - 1.0) - center;
    float t = length(toCenter) / u_radius;
    float dy = exp(-pow(t * 1.5, 6.0));
    float ymin = min(0.0, center.y - dy);
    float ymax = min(max(0.0, center.y + dy), ymin + 2.0 * dy);
    return (ymax - ymin) * 0.1;
}

    
void main() {
    /* get vertex info */
    vec4 info = texture2D(u_texture, v_coord);
    /* add the old volume */
    info.r += volumeInSphere(u_oldCenter);
    /* subtract the new volume */
    info.r -= volumeInSphere(u_newCenter);
    gl_FragColor = info;
}

