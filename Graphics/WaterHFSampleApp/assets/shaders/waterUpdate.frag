precision mediump float;

uniform sampler2D u_texture;
uniform vec2 u_delta;
varying vec2 v_coord;
void main() 
{
    /* get vertex info */
    vec4 info = texture2D(u_texture, v_coord);
    /* calculate average neighbor height */
    vec2 dx = vec2(u_delta.x, 0.0);
    vec2 dy = vec2(0.0, u_delta.y);
    float average = (
        texture(u_texture, v_coord - dx).r +
        texture(u_texture, v_coord - dy).r +
        texture(u_texture, v_coord + dx).r +
        texture(u_texture, v_coord + dy).r
      ) * 0.25;

	float v = (average - info.r) + info.g;
    //v *= 0.999999;
	info.g = v;

    /* move the vertex along the velocity */
    info.r += v;
    gl_FragColor = info;
}

