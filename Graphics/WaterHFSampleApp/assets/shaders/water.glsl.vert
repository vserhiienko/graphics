attribute vec4 a_position;
varying vec2 v_coord;
void main()
{
  v_coord = a_position.xy * 0.5 + 0.5;
  gl_Position = vec4(a_position.xyz, 1.0);
}

