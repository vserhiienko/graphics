#pragma once
#include <CheckGLError.h>

namespace soko
{
  class TextureRenderer
  {
    // vertex & texture coords buffers
    float textureVertices[8];
    float textureCoords[8];

    // shader & attributes & uniforms
    NvGLSLProgram *blitProg;
    GLint positionA;
    GLint texCoordA;
    GLint sourceTextureU;

  public:
    TextureRenderer(void);

  public:
    bool init(void);
    void setSurfaceSize(nv::vec2f);
    void draw(GLuint);

  };
}