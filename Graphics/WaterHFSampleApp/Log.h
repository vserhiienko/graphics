#pragma once
#include <Windows.h>
#include <NV/NvLogs.h>
#include <NV/NvMath.h>
#include <NV/NvStopWatch.h>
#include <NvUI/NvTweakBar.h>
#include <NvGLUtils/NvImage.h>
#include <NvAppBase/NvSampleApp.h>
#include <NvGLUtils/NvGLSLProgram.h>
#include <NvAssetLoader/NvAssetLoader.h>
#include <NvAppBase/NvFramerateCounter.h>

namespace soko
{
  inline static void trace(
    _In_ const char* errLvl, 
    _In_ const char* fmt, 
    _In_opt_ ...
    )
  {
    const int length = 512;
    char buffer[length];
    va_list ap;

    va_start(ap, fmt);
    vsnprintf_s(buffer, length - 1, fmt, ap);
    OutputDebugStringA(errLvl);
    OutputDebugStringA(buffer);
    OutputDebugStringA("\n");
    va_end(ap);

  }
}

#define sokoLogD(fmt,...) soko::trace("@   " __FUNCTION__ ": ", fmt, __VA_ARGS__)
#define sokoLogI(fmt,...) soko::trace("+   " __FUNCTION__ ": ", fmt, __VA_ARGS__)
#define sokoLogW(fmt,...) soko::trace("!   " __FUNCTION__ ": ", fmt, __VA_ARGS__)
#define sokoLogE(fmt,...) soko::trace("#   " __FUNCTION__ ": ", fmt, __VA_ARGS__)
#define sokoTraceFuncBegin soko::trace("|>- " __FUNCTION__ "", "")
#define sokoTraceFuncEnd soko::trace("-<| " __FUNCTION__ "", "")

#define USING_NS_SOKO using namespace soko
