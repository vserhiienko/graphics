#pragma once
#include <CheckGLError.h>
#include <NvGLUtils/NvSimpleFBO.h>
#define _Soko_use_builtin_render_texture 0

namespace soko
{
  // water simulation program, runs on GPU
  // . requires the OES_texture_float extension
  // . requires the OES_texture_float_linear extension
  // . requires the OES_framebuffer_object extension
  class WaterHFSimulation
  {
    GLuint fboPrev;
    NvSimpleFBO *fboA;
    NvSimpleFBO *fboB;
    NvSimpleFBO::Desc fboDesc;

    GLint storedViewport[4]; // to restore previous state

    int textureW, textureH; // texture parameters
    nv::vec2f textureD; // texture step

    /// shaders & uniforms
    // water simulation

    NvGLSLProgram *dropShader; // creates disturbance
    GLint dropShaderPositionA;
    GLint dropShaderTextureU;
    GLint dropShaderCenterU;
    GLint dropShaderRadiusU;
    GLint dropShaderStrengthU;
    
    NvGLSLProgram *updateShader; // propagates disturbance
    GLint updateShaderPositionA;
    GLint updateShaderTextureU;
    GLint updateShaderDeltaU;
   
    NvGLSLProgram *normalShader;  // updates surface normals
    GLint normalShaderPositionA;
    GLint normalShaderTextureU;
    GLint normalShaderDeltaU;
    
    NvGLSLProgram *sphereShader; // reacts to sphere movement
    GLint sphereShaderPositionA;
    GLint sphereShaderTextureU;
    GLint sphereShaderOldCenterU;
    GLint sphereShaderNewCenterU;
    GLint sphereShaderRadiusU;

    // water rendering

    int causticsW, causticsH; // texture parameters
    NvSimpleFBO *fboCaustics;
    NvGLSLProgram *causticsShader; // reacts to sphere movement
    GLint causticsShaderPositionA;
    GLint causticsShaderLightU;
    GLint causticsShaderWaterU;

    // ...

    // plane mesh & parameters
    int planeDetailX, planeDetailY;
    std::vector<nv::vec3f> planeVertices;
    std::vector<uint16_t> planeTris;
    std::vector<nv::vec3f> planeNormals;
    std::vector<nv::vec2f> planeCoords;

    nv::vec3f light;
    int causticsDetailX, causticsDetailY;
    std::vector<nv::vec3f> waterVertices;
    std::vector<uint16_t> waterTris;
    std::vector<nv::vec3f> waterNormals;
    std::vector<nv::vec2f> waterCoords;

    // plane buffers
    GLuint planeVertexBuffer;
    GLuint planeIndexBuffer;

    /// initialization

    bool initializeTextures(void);
    bool initializeShaders(void);
    bool initializePlane(void);

    /// simulation calls

    bool beginDrawTo();
    void drawPlaneBuffers(GLint);
    void drawCausticsBuffers(void);
    void endDrawTo(void);
    void swapTextures(void);

  public:
    WaterHFSimulation(void);

  public:
    bool init(void);
    void addDrop(float x, float y, float radius, float strength);
    void moveSphere(nv::vec2f oldCenter, nv::vec2f newCenter, float radius);
    void stepSimulation(void);
    void updateNormals(void);
    void updateCaustics(void);
    GLuint getWaterTexture(void) const;
    GLuint getCausticsTexture(void) const;

  };
}

/*
updateCaustics = function(water) {
	if (!this.causticsShader) return;
	var this_ = this;
	this.causticTex.drawTo(function() {
	gl.clear(gl.COLOR_BUFFER_BIT);
	water.textureA.bind(0);
	this_.causticsShader.uniforms({
		light: this_.lightDir,
		water: 0,
		sphereCenter: this_.sphereCenter,
		sphereRadius: this_.sphereRadius
	}).draw(this_.waterMesh);
	});
};
*/

