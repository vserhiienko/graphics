#include "TextureRenderer.h"
USING_NS_SOKO;

TextureRenderer::TextureRenderer(void)
{
  textureCoords[0] = 1.0f; textureCoords[1] = 0.0f;
  textureCoords[2] = 0.0f; textureCoords[3] = 0.0f;
  textureCoords[4] = 1.0f; textureCoords[5] = 1.0f;
  textureCoords[6] = 0.0f; textureCoords[7] = 1.0f;
  setSurfaceSize(nv::vec2f(800, 600)); // initial guess
}

void TextureRenderer::draw(GLuint textureId)
{
  blitProg->enable(); sokoCheckGLError;
  blitProg->bindTexture2D(sourceTextureU, 0, textureId);
  glVertexAttribPointer(positionA, 2, GL_FLOAT, GL_FALSE, 0, textureVertices); sokoCheckGLError;
  glVertexAttribPointer(texCoordA, 2, GL_FLOAT, GL_FALSE, 0, textureCoords); sokoCheckGLError;
  glEnableVertexAttribArray(positionA); sokoCheckGLError;
  glEnableVertexAttribArray(texCoordA); sokoCheckGLError;
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); sokoCheckGLError;
  blitProg->disable();
}

bool TextureRenderer::init(void)
{
  sokoTraceFuncBegin;

  blitProg = NvGLSLProgram::createFromFiles(
    "shaders/renderTexture.vert", 
    "shaders/renderTexture.frag"
    ); sokoCheckGLError;

  if (!!blitProg && !!blitProg->getProgram())
  {
    sokoLogI("shader compilation succeeded");
    positionA = blitProg->getAttribLocation("a_position"); sokoCheckGLError;
    texCoordA = blitProg->getAttribLocation("a_texCoord"); sokoCheckGLError;
    sourceTextureU = blitProg->getUniformLocation("u_sourceTex"); sokoCheckGLError;
    sokoLogI("blit as: %d %d us: %d", positionA, texCoordA, sourceTextureU);

    sokoTraceFuncEnd;
    return !sokoCheckGLError;
  }
  else
  {
    sokoLogE("shader compilation failed");
    sokoTraceFuncEnd;
    return false;
  }
}

void TextureRenderer::setSurfaceSize(nv::vec2f surfaceSize)
{
  const float aspectRatio = surfaceSize.y / surfaceSize.x;
  textureVertices[0] = +aspectRatio; textureVertices[1] = -1.0f;
  textureVertices[2] = -aspectRatio; textureVertices[3] = -1.0f;
  textureVertices[4] = +aspectRatio; textureVertices[5] = +1.0f;
  textureVertices[6] = -aspectRatio; textureVertices[7] = +1.0f;
}

