#include <WaterHFSimulationSampleApp.h>
using namespace soko;

WaterHFSimulationSampleApp::WaterHFSimulationSampleApp(NvPlatformContext *context)
  : NvSampleApp(context, "WaterHFSimulation:Soko")
{
  m_aspectRatio = 1.0f;

  forceLinkHack();
  drawWater = false;
  updateWater = false;

  NVWindowsLog("WaterHFSimulationSampleApp::WaterHFSimulationSampleApp()\n");
}

WaterHFSimulationSampleApp::~WaterHFSimulationSampleApp()
{
  NVWindowsLog("WaterHFSimulationSampleApp::~WaterHFSimulationSampleApp()\n");
}

void WaterHFSimulationSampleApp::initUI(void)
{
  if (mTweakBar)
  {
    mTweakBar->addValue("Draw water", drawWater);
  }
}

void WaterHFSimulationSampleApp::initRendering(void)
{
  if (!requireMinAPIVersion(NvGfxAPIVersionGL4())) return;
  NvAssetLoaderAddSearchPath("WaterHFSampleApp");

  glClearColor(0.2f, 0.0f, 0.2f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  SwapBuffers();
}

bool WaterHFSimulationSampleApp::handlePointerInput(
  NvInputDeviceType::Enum device,
  NvPointerActionType::Enum action,
  uint32_t modifiers,
  int32_t count,
  NvPointerEvent* points
  )
{
  static unsigned i = 0;

  switch (action)
  {
  case NvPointerActionType::MOTION:
  {
  } break;
  case NvPointerActionType::DOWN:
  {
    updateWater = true;
    float random1 = (float)rand() / (float)RAND_MAX * 2.0f - 1.0f;
    float random2 = (float)rand() / (float)RAND_MAX * 2.0f - 1.0f;
    simulator.addDrop(
      random1, random2,
      0.3f, 0.2f * ((i & 1) ? -1.0f : 1.0f)
      );
    i++;
  } break;
  case NvPointerActionType::UP:
  {
    updateWater = false;
  } break;
  }

  return false;
}
bool WaterHFSimulationSampleApp::handleKeyInput(
  uint32_t code,
  NvKeyActionType::Enum action
  )
{
  return false;
}

void WaterHFSimulationSampleApp::update(void)
{
  if (updateWater || true)
  {
    freeGLBindings();
    simulator.stepSimulation();
    //simulator.updateNormals();
    simulator.updateCaustics();
    freeGLBindings();
  }
}

void WaterHFSimulationSampleApp::draw(void)
{
  glBindFramebuffer(GL_FRAMEBUFFER, getMainFBO());
  glViewport(0, 0, NvSampleApp::m_width, NvSampleApp::m_height);
  glClearColor(0.2f, 0.0f, 0.2f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  if (drawWater) renderer.draw(simulator.getWaterTexture());
  else renderer.draw(simulator.getCausticsTexture());
}

void WaterHFSimulationSampleApp::reshape(int32_t width, int32_t height)
{
  glViewport(0, 0, (GLint)width, (GLint)height);
  m_aspectRatio = (float)height / (float)width;

  simulator.init();
  SwapBuffers();

  renderer.init();
  renderer.setSurfaceSize(nv::vec2f((float)width, (float)height));

  CHECK_GL_ERROR();
}

bool WaterHFSimulationSampleApp::handleGamepadChanged(uint32_t changedPadFlags)
{
  return false;
}

void WaterHFSimulationSampleApp::freeGLBindings()
{
  glBindFramebuffer(GL_FRAMEBUFFER, getMainFBO());
  glBindRenderbuffer(GL_RENDERBUFFER, 0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
  CHECK_GL_ERROR();
}

void WaterHFSimulationSampleApp::configurationCallback(NvEGLConfiguration& config)
{
  config.depthBits = 24;
  config.stencilBits = 0;
  config.apiVer = NvGfxAPIVersionGL4();
}

NvAppBase* NvAppFactory(NvPlatformContext* platform) {
  return new WaterHFSimulationSampleApp(platform);
}
