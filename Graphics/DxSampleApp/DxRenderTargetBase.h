#pragma once
#include <DxMath.h>
#include <DxTypes.h>

namespace dx
{
  // Direct3D rendering objects. Required for 3D.
  class DxRenderTargetBase
  {
  public:
    dx::ViewportDescription viewport;
    dx::RTView renderTargetView;
    dx::DSView depthStencilView;
    DirectX::Color clearColor;

  public:
    inline DxRenderTargetBase() : viewport(), clearColor()
    {
      clearColor.A(1.0f);
    }

    // Clears render target view, depth stencil view.
    inline void clear(_In_ dx::IContext3 *context)
    {
      context->ClearRenderTargetView(renderTargetView.Get(), clearColor);
      context->ClearDepthStencilView(depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0ui8);
    }

    // Sets render target view, depth stencil view and viewport to context.
    inline void bindToPipeline(_In_ dx::IContext3 *context)
    {
      context->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());
      context->RSSetViewports(1, &viewport);
    }

    // Sets render target view, depth stencil view to null.
    inline void unbindFromPipeline(_In_ IContext3 *context)
    {
      static IRTView *nullRTV[] = { 0 };
      static IDSView *nullDSV = 0;
      context->OMSetRenderTargets(0, (IRTView * const*)nullRTV, nullDSV);
    }
  };
}
