#pragma once

#include <math.h>
#include <Windows.h>
#include <Windowsx.h>
#include <wincodec.h>

#include <wrl.h>
#include <d2d1_2.h>
#include <d3d11_2.h>
#include <dwrite_2.h>
#include <d2d1_2helper.h>
#include <d2d1effects_1.h>

namespace dx
{
  typedef HANDLE Handle, Object;
  typedef DWORD DisplayOrientation;
  typedef HWND HWindow, WindowHandle;
  typedef HRESULT HResult, ResultHandle;

  typedef Microsoft::WRL::ComPtr<IDXGISwapChain1> DxgiSwapchain;
  typedef Microsoft::WRL::ComPtr<IDXGISwapChain1> Swapchain;
  typedef Microsoft::WRL::ComPtr<IDXGIDevice1> DxgiDevice;
  typedef Microsoft::WRL::ComPtr<IDXGIFactory1> DxgiFactory;
  typedef Microsoft::WRL::ComPtr<IDXGISurface1> DxgiSurface;
  typedef Microsoft::WRL::ComPtr<IDXGIAdapter> DxgiAdapter;
  typedef Microsoft::WRL::ComPtr<ID3D11RasterizerState1> RasterizerState;
  typedef Microsoft::WRL::ComPtr<ID3D11DeviceContext1> Direct3DContext;
  typedef Microsoft::WRL::ComPtr<ID3D11DeviceContext1> Context3;
  typedef Microsoft::WRL::ComPtr<ID3D11Device1> Direct3DDevice;
  typedef Microsoft::WRL::ComPtr<ID3D11Device1> Device3;
  typedef Microsoft::WRL::ComPtr<ID3D11BlendState1> BlendState;
  typedef Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> UAView;
  typedef Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> SRView;
  typedef Microsoft::WRL::ComPtr<ID3D11RenderTargetView> RTView;
  typedef Microsoft::WRL::ComPtr<ID3D11DepthStencilView> DSView;
  typedef Microsoft::WRL::ComPtr<ID3D11SamplerState> SamplerState;
  typedef Microsoft::WRL::ComPtr<ID3D11InputLayout> VSLayout;
  typedef Microsoft::WRL::ComPtr<ID3D11VertexShader> VShader;
  typedef Microsoft::WRL::ComPtr<ID3D11HullShader> HShader;
  typedef Microsoft::WRL::ComPtr<ID3D11DomainShader> DShader;
  typedef Microsoft::WRL::ComPtr<ID3D11GeometryShader> GShader;
  typedef Microsoft::WRL::ComPtr<ID3D11PixelShader> PShader;
  typedef Microsoft::WRL::ComPtr<ID3D11ComputeShader> CShader;
  typedef Microsoft::WRL::ComPtr<ID3D11Texture1D> Direct3DTexture1;
  typedef Microsoft::WRL::ComPtr<ID3D11Texture2D> Direct3DTexture2;
  typedef Microsoft::WRL::ComPtr<ID3D11Texture3D> Direct3DTexture3;
  typedef Microsoft::WRL::ComPtr<ID3D11Texture1D> Texture1;
  typedef Microsoft::WRL::ComPtr<ID3D11Texture2D> Texture2;
  typedef Microsoft::WRL::ComPtr<ID3D11Texture3D> Texture3;
  typedef Microsoft::WRL::ComPtr<ID3D11Buffer> Direct3DBuffer;
  typedef Microsoft::WRL::ComPtr<ID3D11Resource> Direct3DResource;
  typedef Microsoft::WRL::ComPtr<ID3D11Buffer> Buffer;
  typedef Microsoft::WRL::ComPtr<ID3D11DepthStencilState> DepthStencilState;
  typedef Microsoft::WRL::ComPtr<ID3D11DepthStencilState> DepthStencil;
  typedef Microsoft::WRL::ComPtr<ID3D11RasterizerState1> RasterizerState;
  typedef Microsoft::WRL::ComPtr<ID3D11RasterizerState1> Rasterizer;
  typedef Microsoft::WRL::ComPtr<ID2D1Factory1> Direct2DFactory;
  typedef Microsoft::WRL::ComPtr<ID2D1Device> Direct2DDevice;
  typedef Microsoft::WRL::ComPtr<ID2D1DeviceContext> Direct2DContext;
  typedef Microsoft::WRL::ComPtr<ID2D1Bitmap1> Direct2DBitmap;
  typedef Microsoft::WRL::ComPtr<IDWriteFactory1> DirectWriteFactory;
  typedef Microsoft::WRL::ComPtr<IWICImagingFactory2> WICImagingFactory;
  typedef Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> Direct2DSolidColorBrush;
  typedef Microsoft::WRL::ComPtr<ID2D1RadialGradientBrush> Direct2DRadialGradientBrush;
  typedef Microsoft::WRL::ComPtr<ID2D1LinearGradientBrush> Direct2DLinearGradientBrush;
  typedef Microsoft::WRL::ComPtr<ID2D1GradientStopCollection1> Direct2DGradientStopCollection;
  typedef Microsoft::WRL::ComPtr<ID2D1DrawingStateBlock> Direct2DDrawingStateBlock;
  typedef Microsoft::WRL::ComPtr<IDWriteTextLayout> DirectWriteTextLayout;
  typedef Microsoft::WRL::ComPtr<IDWriteTextFormat> DirectWriteTextFormat;
  typedef Microsoft::WRL::ComPtr<IWICBitmapDecoder> WICBitmapDecoder;
  typedef Microsoft::WRL::ComPtr<IWICStream> WICStream;
  typedef Microsoft::WRL::ComPtr<IWICBitmapFrameDecode> WICBitmapFrameDecode;
  typedef Microsoft::WRL::ComPtr<IWICFormatConverter> WICFormatConverter;
  typedef Microsoft::WRL::ComPtr<ID2D1PathGeometry1> Direct2DPathGeometry;
  typedef Microsoft::WRL::ComPtr<ID2D1GeometrySink> Direct2DGeometrySink;

  typedef Direct2DDevice::InterfaceType IDirect2DDevice;
  typedef Direct2DContext::InterfaceType IDirect2DContext;
  typedef RasterizerState::InterfaceType IRasterizerState;
  typedef Rasterizer::InterfaceType IRasterizer;
  typedef DepthStencil::InterfaceType IDepthStencil;
  typedef DepthStencilState::InterfaceType IDepthStencilState;
  typedef Direct3DResource::InterfaceType IDirect3DResource;
  typedef DxgiSwapchain::InterfaceType IDxgiSwapchain;
  typedef DxgiSurface::InterfaceType IDxgiSurface;
  typedef Swapchain::InterfaceType ISwapchain;
  typedef SamplerState::InterfaceType ISamplerState;
  typedef BlendState::InterfaceType IBlendState;
  typedef Direct3DTexture1::InterfaceType IDirect3DTexture1;
  typedef Direct3DTexture2::InterfaceType IDirect3DTexture2;
  typedef Direct3DTexture3::InterfaceType IDirect3DTexture3;
  typedef Texture1::InterfaceType ITexture1;
  typedef Texture2::InterfaceType ITexture2;
  typedef Texture3::InterfaceType ITexture3;
  typedef Direct3DContext::InterfaceType IDirect3DContext;
  typedef Direct3DDevice::InterfaceType IDirect3DDevice;
  typedef Context3::InterfaceType IContext3;
  typedef Device3::InterfaceType IDevice3;
  typedef VSLayout::InterfaceType IVSLayout;
  typedef GShader::InterfaceType IGShader;
  typedef VShader::InterfaceType IVShader;
  typedef PShader::InterfaceType IPShader;
  typedef HShader::InterfaceType IHShader;
  typedef DShader::InterfaceType IDShader;
  typedef CShader::InterfaceType ICShader;
  typedef UAView::InterfaceType IUAView;
  typedef SRView::InterfaceType ISRView;
  typedef Buffer::InterfaceType IBuffer;
  typedef RTView::InterfaceType IRTView;
  typedef DSView::InterfaceType IDSView;
  typedef Direct2DBitmap::InterfaceType IDirect2DBitmap;
  typedef Direct2DFactory::InterfaceType IDirect2DFactory;
  typedef DirectWriteFactory::InterfaceType IDirectWriteFactory;
  typedef WICImagingFactory::InterfaceType IWICImagingFactory;
  typedef DirectWriteTextLayout::InterfaceType IDirectWriteTextLayout;
  typedef DirectWriteTextFormat::InterfaceType IDirectWriteTextFormat;
  typedef Direct2DSolidColorBrush::InterfaceType IDirect2DSolidColorBrush;
  typedef Direct2DRadialGradientBrush::InterfaceType IDirect2DRadialGradientBrush;
  typedef Direct2DLinearGradientBrush::InterfaceType IDirect2DLinearGradientBrush;
  typedef WICFormatConverter::InterfaceType IWICFormatConverter;
  typedef WICBitmapFrameDecode::InterfaceType IWICBitmapFrameDecode;
  typedef WICStream::InterfaceType IWICStream;
  typedef WICBitmapDecoder::InterfaceType IWICBitmapDecoder;

  typedef D3D11_TEXTURE1D_DESC Texture1Description;
  typedef D3D11_TEXTURE2D_DESC Texture2Description;
  typedef D3D11_TEXTURE3D_DESC Texture3Description;
  typedef D3D11_SHADER_RESOURCE_VIEW_DESC SRVDescription;
  typedef D3D11_UNORDERED_ACCESS_VIEW_DESC UAVDescription;
  typedef D3D11_RENDER_TARGET_VIEW_DESC RTVDescription;
  typedef D3D11_DEPTH_STENCIL_VIEW_DESC DSVDescription;
  typedef D3D11_BUFFER_DESC BufferDescription;
  typedef D3D11_SAMPLER_DESC SamplerStateDescription;
  typedef D3D11_BLEND_DESC1 BlendStateDescription;
  typedef CD3D11_BLEND_DESC1 CBlendStateDescription;
  typedef D3D11_RASTERIZER_DESC1 RasterizerStateDescription;
  typedef D3D11_RASTERIZER_DESC1 RasterizerDescription;
  typedef CD3D11_RASTERIZER_DESC1 CRasterizerStateDescription;
  typedef CD3D11_RASTERIZER_DESC1 CRasterizerDescription;
  typedef D3D11_RENDER_TARGET_BLEND_DESC1 RTBlendDescription;
  typedef DXGI_SWAP_CHAIN_DESC1 SwapchainDescription;
  typedef D2D1_BITMAP_PROPERTIES1 BitmapProperties;
  typedef CD3D11_TEXTURE1D_DESC CTexture1Description;
  typedef CD3D11_TEXTURE2D_DESC CTexture2Description;
  typedef CD3D11_TEXTURE3D_DESC CTexture3Description;
  typedef CD3D11_SHADER_RESOURCE_VIEW_DESC CSRVDescription;
  typedef CD3D11_UNORDERED_ACCESS_VIEW_DESC CUAVDescription;
  typedef CD3D11_RENDER_TARGET_VIEW_DESC CRTVDescription;
  typedef CD3D11_DEPTH_STENCIL_VIEW_DESC CDSVDescription;
  typedef CD3D11_BUFFER_DESC CBufferDescription;

  typedef D3D11_BOX Region, Box;
  typedef D3D11_VIEWPORT ViewportDescription;
  typedef CD3D11_VIEWPORT CViewportDescription;
  typedef D3D11_DEPTH_STENCIL_DESC DepthStencilStateDescription;
  typedef D3D11_DEPTH_STENCIL_DESC DepthStencilDescription;
  typedef CD3D11_DEPTH_STENCIL_DESC CDepthStencilStateDescription;
  typedef CD3D11_DEPTH_STENCIL_DESC CDepthStencilDescription;
  typedef CD3D11_SAMPLER_DESC CSampleStateDescription;
  typedef D3D11_MAPPED_SUBRESOURCE MappedSubresourceData;
  typedef D3D11_MAPPED_SUBRESOURCE MappedSubresource;
  typedef D3D11_SUBRESOURCE_DATA SubresourceData;
  typedef D3D11_SUBRESOURCE_DATA Subresource;
  typedef DXGI_ADAPTER_DESC DxgiAdapterDescription;
  typedef DXGI_ADAPTER_DESC AdapterDescription;
  typedef D3D11_INPUT_ELEMENT_DESC VSInputElementDescription;
  typedef D3D11_SO_DECLARATION_ENTRY GSSODeclarationEntry;
  typedef D3D11_PRIMITIVE_TOPOLOGY PrimitiveTopology;

  typedef DWRITE_TEXT_METRICS DirectWriteTextMetrics;
  typedef DWRITE_PARAGRAPH_ALIGNMENT DirectWriteParagraphAlignment;
  typedef DWRITE_TEXT_ALIGNMENT DirectWriteTextAlignment;
  typedef DWRITE_FONT_STRETCH DirectWriteFontStretch;
  typedef DWRITE_FONT_WEIGHT DirectWriteFontWeight;
  typedef DWRITE_FONT_STYLE DirectWriteFontStyle;
  typedef DXGI_MODE_ROTATION DxgiModeRotation;
  typedef D3D_FEATURE_LEVEL FeatureLevel;
  typedef D3D_DRIVER_TYPE DriverType;
  typedef DXGI_FORMAT DxgiFormat;


  typedef struct RectF
  {
    float top, left, bottom, right;
    inline float GetWidth() { return right - left; }
    inline float GetHeight() { return bottom - top; }

    const RectF &operator=(const RectF& other)
    {
      top = other.top;
      bottom = other.bottom;
      left = other.left;
      right = other.right;
      return *this;
    }

    const RectF &operator=(const RECT& other)
    {
      top = (float)other.top;
      bottom = (float)other.bottom;
      left = (float)other.left;
      right = (float)other.right;
      return *this;
    }

  } Rect;

  typedef struct SizeF
  {
    float Width, Height;

    inline SizeF(float s = 0) : Width(s), Height(s) { }
    inline SizeF(float w, float h) : Width(w), Height(h) { }

    inline bool operator==(const SizeF& size)
    {
      return Width == size.Width && Height == size.Height;
    }

    inline const SizeF &operator=(const SizeF &other)
    {
      Width = other.Width;
      Height = other.Height;
      return *this;
    }

    inline bool operator!=(const SizeF& size)
    {
      return Width != size.Width || Height != size.Height;
    }

  } Size;
}
