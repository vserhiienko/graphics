#pragma once
#include <DxTypes.h>
#include <DxResourceManager.h>

namespace dx
{
  // Strongly typed wrapper around a D3D constant buffer.
  template<typename T> class ConstantBuffer
  {
  public:
    ConstantBuffer() {}

    dx::HResult create(_In_ IDirect3DDevice* device, const char *debugName = NULL)
    {
      dx::HResult result = S_OK;
      dx::BufferDescription desc = { 0 };

      desc.ByteWidth = sizeof(T);
      desc.Usage = D3D11_USAGE_DYNAMIC;
      desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
      desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

      result = device->CreateBuffer(&desc, nullptr, m_buffer.ReleaseAndGetAddressOf());
      if (SUCCEEDED(result)) dx::DxResourceManager::setDebugName(m_buffer.Get(), (debugName ? debugName : "DxSampleApp_ConstantBuffer"));

      return result;
    }

    dx::HResult setData(_In_ IDirect3DContext* deviceContext, _In_ T const& value)
    {
      dx::HResult result = S_OK;
      if (!m_buffer.Get()) return E_FAIL;

      dx::MappedSubresource mappedResource;

      result = deviceContext->Map(m_buffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

      if (SUCCEEDED(result))
      {
        *(T*)mappedResource.pData = value;
        deviceContext->Unmap(m_buffer.Get(), 0);
      }

      return result;
    }

    IBuffer *const getBuffer(void) const { return m_buffer.Get(); }

    IBuffer *const *getBufferAddress(void) const { return m_buffer.GetAddressOf(); }

  private:

    dx::Buffer m_buffer;

    ConstantBuffer(ConstantBuffer const&);
    ConstantBuffer& operator= (ConstantBuffer const&);
  };
}