template <class TTarget> static dx::HResult dx::DxResourceManager::setDebugName(
  _In_ TTarget *target, _In_ std::wstring const &name
  )
{
  return setDebugName(target, std::string(name.begin(), name.end()));
}

template <class TTarget> static dx::HResult dx::DxResourceManager::setDebugName(
  _In_ TTarget *target, _In_ std::string const &name
  )
{
  if (target) return target->SetPrivateData(WKPDID_D3DDebugObjectName, (unsigned)name.size(), (dx::Handle) name.c_str());
  else return E_POINTER;
}

template <typename TIndex>
::HRESULT dx::DxResourceManager::createIndexBuffer(
  _In_ dx::IDirect3DDevice* pDevice,
  _In_ unsigned uNumIndices,
  _Const_ TIndex const *pIndexData,
  _Outref_result_nullonfailure_ dx::IBuffer **ppIndexBuffer
  )
{
  dx::HResult result = S_OK;

  dx::BufferDescription indexBufferDesc = { 0 };
  indexBufferDesc.ByteWidth = uNumIndices * sizeof TIndex;
  indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
  indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;

  dx::SubresourceData indexBufferData = { 0 };
  indexBufferData.pSysMem = pIndexData;

  return pDevice->CreateBuffer(&indexBufferDesc, &indexBufferData, ppIndexBuffer);
}

template <class TBufferContent>
::HRESULT dx::DxResourceManager::createVertexBuffer(
  _In_ dx::IDirect3DDevice* pDevice,
  _In_ unsigned uNumBufferElements,
  _Const_ TBufferContent const *pInitialData,
  _Outref_result_nullonfailure_ dx::IBuffer **ppBuffer
  )
{
  BufferDescription BufferDescriptor = { 0 };
  BufferDescriptor.ByteWidth = uNumBufferElements * sizeof(TBufferContent);
  BufferDescriptor.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  //BufferDescriptor.Usage = D3D11_USAGE_IMMUTABLE;
  BufferDescriptor.Usage = D3D11_USAGE_DEFAULT;

  SubresourceData BufferInitData = { 0 };
  BufferInitData.pSysMem = pInitialData;

  return pDevice->CreateBuffer(&BufferDescriptor, &BufferInitData, ppBuffer);
}

template <class TBufferContent>
::HRESULT dx::DxResourceManager::createConstantBuffer(
  _In_ dx::IDirect3DDevice* d3dDevice,
  _Const_ _Maybenull_ TBufferContent const *pInitialData,
  _Outref_result_nullonfailure_ dx::IBuffer **ppBuffer
  )
{
  BufferDescription BufferDescriptor = { 0 };
  BufferDescriptor.ByteWidth = sizeof(TBufferContent);
  //BufferDescriptor.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
  BufferDescriptor.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
  //BufferDescriptor.Usage = D3D11_USAGE_DYNAMIC;
  BufferDescriptor.Usage = D3D11_USAGE_DEFAULT;

  if (pInitialData)
  {
    SubresourceData BufferInitData = { 0 };
    BufferInitData.pSysMem = pInitialData;

    return d3dDevice->CreateBuffer(&BufferDescriptor, &BufferInitData, ppBuffer);
  }
  else
  {
    return d3dDevice->CreateBuffer(&BufferDescriptor, nullptr, ppBuffer);
  }
}

template <class TBufferContent>
::HRESULT dx::DxResourceManager::createStructuredBuffer(
  _In_ dx::IDirect3DDevice* d3dDevice,
  _In_ unsigned uNumBufferElements,
  _Const_ _Maybenull_ TBufferContent const *pInitialData,
  _Outref_result_nullonfailure_ IBuffer **ppBuffer,
  _Outref_result_nullonfailure_ ISRView **ppSRView,
  _Outref_result_nullonfailure_ IUAView **ppUAView
  )
{
  ::HRESULT result = S_OK;

  BufferDescription BufferDescriptor = { 0 };
  BufferDescriptor.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
  BufferDescriptor.ByteWidth = uNumBufferElements * sizeof(TBufferContent);
  BufferDescriptor.StructureByteStride = sizeof(TBufferContent);
  BufferDescriptor.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
  BufferDescriptor.Usage = D3D11_USAGE_DEFAULT;

  if (pInitialData)
  {
    SubresourceData BufferInitData = { 0 };
    BufferInitData.pSysMem = pInitialData;

    result = d3dDevice->CreateBuffer(&BufferDescriptor, &BufferInitData, ppBuffer);
  }
  else
  {
    result = d3dDevice->CreateBuffer(&BufferDescriptor, nullptr, ppBuffer);
  }

  if (ppSRView)
  {
    SRVDescription BufferSRViewDesc; dx::zeroMemory(BufferSRViewDesc);
    BufferSRViewDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
    BufferSRViewDesc.Buffer.ElementWidth = uNumBufferElements;

    result = d3dDevice->CreateShaderResourceView(*ppBuffer, &BufferSRViewDesc, ppSRView);
  }

  if (ppUAView)
  {
    UAVDescription BufferUAViewDesc; dx::zeroMemory(BufferUAViewDesc);
    BufferUAViewDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
    BufferUAViewDesc.Buffer.NumElements = uNumBufferElements;

    result = d3dDevice->CreateUnorderedAccessView(*ppBuffer, &BufferUAViewDesc, ppUAView);
  }

  return result;
}