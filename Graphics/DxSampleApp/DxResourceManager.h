#pragma once
#include <DxTypes.h>
#include <DxUtils.h>
#include <DxAssetManager.h>

namespace dx
{
  template <class TDirectXResource> struct ReadResource
  {
    TDirectXResource resource;
    dx::SRView readView;
  };

  template <class TDirectXResource> struct WriteResource
  {
    TDirectXResource resource;
    dx::UAView writeView;
  };

  template <class TDirectXResource> struct ActiveResource
  {
    TDirectXResource resource;
    dx::SRView readView;
    dx::UAView writeView;
  };

  template <class TDirectXResource> struct PingPongResource
  {
    ActiveResource<TDirectXResource> ping;
    ActiveResource<TDirectXResource> pong;

    inline void PingPong()
    {
      ping.readView.Swap(pong.readView);
      ping.writeView.Swap(pong.writeView);
    }
  };

  class DxResourceManager
  {
  public:
    typedef concurrency::task<dx::HResult> HResultTask;

  public:
    template <class TTarget> static dx::HResult setDebugName(_In_ TTarget *target, _In_ std::string const &name);
    template <class TTarget> static dx::HResult setDebugName(_In_ TTarget *target, _In_ std::wstring const &name);

  public:

    static void defaults(_Inout_ dx::DepthStencilStateDescription&);
    static void defaults(_Inout_ dx::RasterizerStateDescription&);
    static void defaults(_Inout_ dx::SamplerStateDescription&);
    static std::wstring getFileExtension(_In_ const std::wstring&);
    static std::string getFileExtension(_In_ const std::string&);
    static bool isDDSFile(_In_ const std::wstring&);
    static bool isDDSFile(_In_ const std::string&);

  public:
    static dx::HResult loadTexture(
      _In_ dx::IDirect3DDevice *d3dDevice,
      _In_ dx::IWICImagingFactory *wic,
      _In_ std::wstring assetName,
      _Outptr_result_nullonfailure_ ITexture2 **texAddress,
      _Outptr_opt_result_nullonfailure_ ISRView **resoureViewAddress
      );

  public:
    static dx::HResult createInputLayout(
      _In_ dx::IDirect3DDevice *d3dDevice,
      _In_reads_bytes_(bytecodeSz) ::BYTE const *bytecode,
      _In_  unsigned bytecodeSz,
      _In_reads_opt_(elementCount) const dx::VSInputElementDescription elements[],
      _In_ unsigned elementCount,
      _Outptr_opt_result_nullonfailure_ dx::IVSLayout **layoutAddress,
      _In_opt_ std::wstring wszDebugName = L""
      );

    static dx::HResult loadShader(
      _In_ dx::IDirect3DDevice *d3dDevice,
      _In_ std::wstring assetName,
      _In_reads_opt_(elementCount) const dx::VSInputElementDescription elements[],
      _In_ unsigned elementCount,
      _Outptr_result_nullonfailure_ dx::IVShader **shaderAddress,
      _Outptr_opt_result_nullonfailure_ dx::IVSLayout **layoutAddress
      );
    static dx::HResult loadShader(
      _In_ dx::IDirect3DDevice *d3dDevice,
      _In_ std::wstring assetName,
      _In_reads_opt_(entryCount) const dx::GSSODeclarationEntry entries[],
      _In_ unsigned entryCount,
      _In_reads_opt_(strideCount) const  unsigned bufferStrides[],
      _In_ unsigned strideCount,
      _In_ unsigned rasterizedStream,
      _Outptr_result_nullonfailure_ dx::IGShader **shaderAddress
      );
    static dx::HResult loadShader(
      _In_ dx::IDirect3DDevice *d3dDevice,
      _In_ std::wstring assetName,
      _Outptr_result_nullonfailure_ dx::IHShader **shaderAddress
      );
    static dx::HResult loadShader(
      _In_ dx::IDirect3DDevice *d3dDevice,
      _In_ std::wstring assetName,
      _Outptr_result_nullonfailure_ dx::IDShader **shaderAddress
      );
    static dx::HResult loadShader(
      _In_ dx::IDirect3DDevice *d3dDevice,
      _In_ std::wstring assetName,
      _Outptr_result_nullonfailure_ dx::IGShader **shaderAddress
      );
    static dx::HResult loadShader(
      _In_ dx::IDirect3DDevice *d3dDevice,
      _In_ std::wstring assetName,
      _Outptr_result_nullonfailure_ dx::IPShader **shaderAddress
      );
    static dx::HResult loadShader(
      _In_ dx::IDirect3DDevice *d3dDevice,
      _In_ std::wstring assetName,
      _Outptr_result_nullonfailure_ dx::ICShader **shaderAddress
      );

  public:
    static dx::HResult createWICTextureFromMemory(
      _In_ dx::IDirect3DDevice* d3dDevice,
      _In_ dx::IDirect3DContext* d3dContext,
      _In_reads_bytes_(wicDataSize) const uint8_t* wicData,
      _In_ size_t wicDataSize,
      _Outptr_opt_ dx::IDirect3DResource** texture,
      _Outptr_opt_ dx::ISRView** textureView,
      _In_ size_t maxsize = 0
      );
    static dx::HResult createWICTextureFromFile(
      _In_ dx::IDirect3DDevice* d3dDevice,
      _In_ dx::IDirect3DContext* d3dContext,
      _In_z_ const wchar_t* filePath,
      _Outptr_opt_ dx::IDirect3DResource** texture,
      _Outptr_opt_ dx::ISRView** textureView,
      _In_ size_t maxsize = 0
      );
    static dx::HResult createWICTextureFromMemoryEx(
      _In_ dx::IDirect3DDevice* d3dDevice,
      _In_ dx::IDirect3DContext* d3dContext,
      _In_reads_bytes_(wicDataSize) const uint8_t* wicData,
      _In_ size_t wicDataSize,
      _In_ size_t maxsize,
      _In_ D3D11_USAGE usage,
      _In_ unsigned int bindFlags,
      _In_ unsigned int cpuAccessFlags,
      _In_ unsigned int miscFlags,
      _In_ bool forceSRGB,
      _Outptr_opt_ dx::IDirect3DResource** texture,
      _Outptr_opt_ dx::ISRView** textureView
      );
    static dx::HResult createWICTextureFromFileEx(
      _In_ dx::IDirect3DDevice* d3dDevice,
      _In_ dx::IDirect3DContext* d3dContext,
      _In_z_ const wchar_t* filePath,
      _In_ size_t maxsize,
      _In_ D3D11_USAGE usage,
      _In_ unsigned int bindFlags,
      _In_ unsigned int cpuAccessFlags,
      _In_ unsigned int miscFlags,
      _In_ bool forceSRGB,
      _Outptr_opt_ dx::IDirect3DResource** textureAddress,
      _Outptr_opt_ dx::ISRView** textureView
      );

  public:
    template <class TBufferContent>
    static ::HRESULT createVertexBuffer(
      _In_ dx::IDirect3DDevice* d3dDevice,
      _In_::UINT32 dataElementCount,
      _Const_ TBufferContent const *initData,
      _Outref_result_nullonfailure_ IBuffer **bufferAddress
      );
    template <typename TIndex = unsigned short>
    static ::HRESULT createIndexBuffer(
      _In_ dx::IDirect3DDevice* d3dDevice,
      _In_ unsigned dataElementCount,
      _Const_ TIndex const *data,
      _Outref_result_nullonfailure_ dx::IBuffer **bufferAddress
      );
    template <class TBufferContent>
    static ::HRESULT createConstantBuffer(
      _In_ dx::IDirect3DDevice* d3dDevice,
      _Const_ _Maybenull_ TBufferContent const *initData,
      _Outref_result_nullonfailure_ IBuffer **bufferAddress
      );
    template <class TBufferContent>
    static ::HRESULT createStructuredBuffer(
      _In_ dx::IDirect3DDevice* d3dDevice,
      _In_ unsigned dataElementCount,
      _Const_ TBufferContent const *initData,
      _Outref_result_nullonfailure_ IBuffer **bufferAddress,
      _Outref_result_nullonfailure_ ISRView **srViewAddress,
      _Outref_result_nullonfailure_ IUAView **uaViewAddress
      );
  };
#include "DxResourceManager.cpp.h"
};
