#include "DxDeviceResources.h"
#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

#include <algorithm>

// 0-degree Z-rotation.
const DirectX::XMFLOAT4X4 dx::ScreenRotation::rotation0(
  1.0f, 0.0f, 0.0f, 0.0f,
  0.0f, 1.0f, 0.0f, 0.0f,
  0.0f, 0.0f, 1.0f, 0.0f,
  0.0f, 0.0f, 0.0f, 1.0f
  );

// 90-degree Z-rotation.
const DirectX::XMFLOAT4X4 dx::ScreenRotation::rotation90(
  0.0f, 1.0f, 0.0f, 0.0f,
  -1.0f, 0.0f, 0.0f, 0.0f,
  0.0f, 0.0f, 1.0f, 0.0f,
  0.0f, 0.0f, 0.0f, 1.0f
  );

// 180-degree Z-rotation.
const DirectX::XMFLOAT4X4 dx::ScreenRotation::rotation180(
  -1.0f, 0.0f, 0.0f, 0.0f,
  0.0f, -1.0f, 0.0f, 0.0f,
  0.0f, 0.0f, 1.0f, 0.0f,
  0.0f, 0.0f, 0.0f, 1.0f
  );

// 270-degree Z-rotation.
const DirectX::XMFLOAT4X4 dx::ScreenRotation::rotation270(
  0.0f, -1.0f, 0.0f, 0.0f,
  1.0f, 0.0f, 0.0f, 0.0f,
  0.0f, 0.0f, 1.0f, 0.0f,
  0.0f, 0.0f, 0.0f, 1.0f
  );

const float dx::DxDeviceResources::dipsPerInch = 96.0f;

bool dx::DxDeviceResources::updateDpi()
{
  HDC device = 0;
  int status = 0;

  device = GetDC(host);
  dpi.x = (float)GetDeviceCaps(device, LOGPIXELSX);
  dpi.y = (float)GetDeviceCaps(device, LOGPIXELSY);
  logicalDpi = dpi.x;
  status = ReleaseDC(host, device);

  return status != 0;
}

bool dx::DxDeviceResources::updateLogicalSizeAndCurrentOrientation()
{
  bool status_devmode = 0;
  bool status_monitor = 0;
  RECT clientRect = { 0 };
  HMONITOR monitorHandle = 0;
  DEVMODEA screenSettings = { 0 };
  MONITORINFOEXA monitorInfo;

  screenSettings.dmSize = sizeof DEVMODEA;
  status_devmode = EnumDisplaySettingsExA(
    nullptr, ENUM_CURRENT_SETTINGS,
    &screenSettings,
    EDS_ROTATEDMODE
    ) != 0;

  if (status_devmode)
    currentOrientation = screenSettings.dmDisplayOrientation;

  status_monitor = FALSE;
  monitorHandle = MonitorFromWindow(host, MONITOR_DEFAULTTOPRIMARY);
  if (monitorHandle != NULL && monitorHandle != INVALID_HANDLE_VALUE)
  {
    ZeroMemory(&monitorInfo, sizeof monitorInfo);
    monitorInfo.cbSize = sizeof monitorInfo;

    if (::GetMonitorInfoA(monitorHandle, &monitorInfo))
    {
      logicalSize.Width = (float)(monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left);
      logicalSize.Height = (float)(monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top);
      status_monitor = TRUE;
    }

    status_devmode = EnumDisplaySettingsExA(
      monitorInfo.szDevice, ENUM_CURRENT_SETTINGS,
      &screenSettings,
      EDS_ROTATEDMODE
      ) != 0;
  }

  if (windowed)
  {
    status_devmode = GetClientRect(host, &clientRect) != 0;
    logicalSize.Width = (float)clientRect.right;
    logicalSize.Height = (float)clientRect.bottom;
  }
  else
  {
    if (status_devmode)
      logicalSize.Width = (float)screenSettings.dmPelsWidth,
      logicalSize.Height = (float)screenSettings.dmPelsHeight;
  }

  return status_devmode || status_monitor;
}

dx::HResult dx::DxDeviceResources::setWindow(_In_ dx::WindowHandle window)
{
  int status = 0;

  host = window;
  nativeOrientation = DMDO_DEFAULT;

  status = updateDpi();
  status = updateLogicalSizeAndCurrentOrientation();

  return createWindowSizeDependentResources();
}

void dx::DxDeviceResources::registerDeviceNotify(
  _In_ dx::IDxDeviceNotify* notify
  )
{
  deviceNotify = notify;
}

dx::HResult dx::DxDeviceResources::setLogicalSize(
  _In_ dx::Size logicalSize
  )
{
  if (logicalSize != logicalSize)
  {
    if (windowed) logicalSize = logicalSize;
    else updateLogicalSizeAndCurrentOrientation();
    return createWindowSizeDependentResources();

  }
  else return S_OK;
}

dx::HResult dx::DxDeviceResources::setDpi(
  _In_ float dpix,
  _In_ float dpiy
  )
{
  if (dpi.x != dpix || dpi.y != dpiy)
  {
    int status = 0;

    dpi.x = dpix;
    dpi.y = dpiy;

    // When the display DPI changes, the logical size of the window (measured in Dips) 
    // also changes and needs to be updated.
    status = updateLogicalSizeAndCurrentOrientation();
    return createWindowSizeDependentResources();

  }
  else return S_OK;
}

dx::HResult dx::DxDeviceResources::setCurrentOrientation(
  _In_ dx::DisplayOrientation currentOrientation
  )
{
  if (currentOrientation != currentOrientation)
  {
    currentOrientation = currentOrientation;
    return createWindowSizeDependentResources();

  }
  else return S_OK;
}

dx::DxDeviceResources::DxDeviceResources(
  D3D_DRIVER_TYPE driver,
  DXGI_USAGE swapchainBuffersUsage,
  DXGI_FORMAT swapchainBuffersFormat,
  DXGI_SWAP_EFFECT swapchainEffect,
  DXGI_ALPHA_MODE swapchainAlphaMode,
  unsigned swapchainBufferCount,
  bool supportD2D,
  bool allowFullscreen,
  bool doCreateDeviceResources
  ) : DxRenderTargetBase(),
  renderTargetUsage(swapchainBuffersUsage),
  renderTargetFormat(swapchainBuffersFormat),
  swapchainBufferCount(swapchainBufferCount),
  swapchainAlphaMode(swapchainAlphaMode),
  swapchainEffect(swapchainEffect),
  d2dSupport(supportD2D),
  driver(driver)
{
  swapchainFlags = allowFullscreen ? DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH : (DXGI_SWAP_CHAIN_FLAG)0;

  dx::HResult error;
  if (doCreateDeviceResources)
  {
    error = createDeviceIndependentResources();
    valid = SUCCEEDED(error); if (FAILED(error)) return;

    error = createDeviceResources();
    valid = SUCCEEDED(error); if (FAILED(error)) return;
  }
}

dx::DxDeviceResources::~DxDeviceResources()
{
  cleanup();
}

void dx::DxDeviceResources::cleanup()
{
  if (swapchain.Get()) swapchain->SetFullscreenState(FALSE, nullptr), swapchain = nullptr;
  if (context.Get()) context = nullptr;
  if (device.Get()) device = nullptr;
}

dx::HResult dx::DxDeviceResources::createDeviceIndependentResources()
{
  dx::HResult result = S_OK;
  return result;
}

bool dx::DxDeviceResources::sdkLayersAvailable()
{
  dx::HResult hr = D3D11CreateDevice(
    nullptr,
    D3D_DRIVER_TYPE_NULL, // There is no need to create a real hardware device.
    0,
    D3D11_CREATE_DEVICE_DEBUG, // Check for the SDK layers.
    nullptr, // Any feature level will do.
    0,
    D3D11_SDK_VERSION, // Always set this to D3D11_SDK_VERSION for Windows Store apps.
    nullptr, // No need to keep the D3D device reference.
    nullptr, // No need to know the feature level.
    nullptr // No need to keep the D3D device context reference.
    );

  return SUCCEEDED(hr);
}

dx::HResult dx::DxDeviceResources::createDeviceResources()
{
  dx::HResult result;

  // This flag adds support for surfaces with a different color channel ordering
  // than the API default. It is required for compatibility with Direct2D.
  UINT creationFlags = d2dSupport ? D3D11_CREATE_DEVICE_BGRA_SUPPORT : 0;

#if defined(_DEBUG)
  if (sdkLayersAvailable())
  {
    // If the project is in a debug build, enable debugging via SDK Layers with this flag.
    creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
  }
#endif

  // This array defines the set of DirectX hardware feature levels this app will support.
  // Note the ordering should be preserved.
  D3D_FEATURE_LEVEL featureLevels[] =
  {
    D3D_FEATURE_LEVEL_11_1,
    D3D_FEATURE_LEVEL_11_0,
    D3D_FEATURE_LEVEL_10_1,
    D3D_FEATURE_LEVEL_10_0,
    D3D_FEATURE_LEVEL_9_3,
    D3D_FEATURE_LEVEL_9_2,
    D3D_FEATURE_LEVEL_9_1
  };

  // Create the Direct3D 11 API device object and a corresponding context.
  Microsoft::WRL::ComPtr<ID3D11Device> deviceUnderlayer;
  Microsoft::WRL::ComPtr<ID3D11DeviceContext> contextUnderlayer;

  dx::HResult hr = D3D11CreateDevice(
    nullptr,					// Specify nullptr to use the default adapter.
    driver,						// Create a device using the hardware graphics driver.
    0,							// Should be 0 unless the driver is D3D_DRIVER_TYPE_SOFTWARE.
    creationFlags,				// Set debug and Direct2D compatibility flags.
    featureLevels,				// List of feature levels this app can support.
    ARRAYSIZE(featureLevels),	// Size of the list above.
    D3D11_SDK_VERSION,			// Always set this to D3D11_SDK_VERSION for Windows Store apps.
    &deviceUnderlayer,					// Returns the Direct3D device created.
    &features,			// Returns feature level of device created.
    &contextUnderlayer					// Returns the device immediate context.
    );

  if (FAILED(hr))
  {
    // If the initialization fails, fall back to the WARP device.
    result = D3D11CreateDevice(
      nullptr,
      D3D_DRIVER_TYPE_WARP, // Create a WARP device instead of a hardware device.
      0,
      creationFlags,
      featureLevels,
      ARRAYSIZE(featureLevels),
      D3D11_SDK_VERSION,
      &deviceUnderlayer,
      &features,
      &contextUnderlayer
      );

    if (FAILED(result))
      return result;
  }

  // Store pointers to the Direct3D 11.1 API device and immediate context.
  result = deviceUnderlayer.As(&device);
  if (FAILED(result)) return result;
  result = contextUnderlayer.As(&context);
  if (FAILED(result)) return result;

  deviceRestored(this);
  return result;
}

dx::HResult dx::DxDeviceResources::validateDevice()
{
  using namespace Microsoft::WRL;

  // The D3D Device is no longer valid if the default adapter changed since the device
  // was created or if the device has been removed.

  // First, get the information for the default adapter from when the device was created.

  dx::HResult result;

  Microsoft::WRL::ComPtr<IDXGIDevice> dxgiDevice;
  result = (device.As(&dxgiDevice));
  if (FAILED(result)) return result;

  Microsoft::WRL::ComPtr<IDXGIAdapter> deviceAdapter;
  result = (dxgiDevice->GetAdapter(&deviceAdapter));
  if (FAILED(result)) return result;

  Microsoft::WRL::ComPtr<IDXGIFactory1> deviceFactory;
  result = (deviceAdapter->GetParent(IID_PPV_ARGS(&deviceFactory)));
  if (FAILED(result)) return result;

  Microsoft::WRL::ComPtr<IDXGIAdapter1> previousDefaultAdapter;
  result = (deviceFactory->EnumAdapters1(0, &previousDefaultAdapter));
  if (FAILED(result)) return result;

  DXGI_ADAPTER_DESC previousDesc;
  result = (previousDefaultAdapter->GetDesc(&previousDesc));
  if (FAILED(result)) return result;

  // Next, get the information for the current default adapter.

  Microsoft::WRL::ComPtr<IDXGIFactory1> currentFactory;
  result = (CreateDXGIFactory1(IID_PPV_ARGS(&currentFactory)));
  if (FAILED(result)) return result;

  Microsoft::WRL::ComPtr<IDXGIAdapter1> currentDefaultAdapter;
  result = (currentFactory->EnumAdapters1(0, &currentDefaultAdapter));
  if (FAILED(result)) return result;

  DXGI_ADAPTER_DESC currentDesc;
  result = (currentDefaultAdapter->GetDesc(&currentDesc));
  if (FAILED(result)) return result;

  // If the adapter LUIDs don't match, or if the device reports that it has been removed,
  // a new D3D device must be created.

  if (previousDesc.AdapterLuid.LowPart != currentDesc.AdapterLuid.LowPart ||
    previousDesc.AdapterLuid.HighPart != currentDesc.AdapterLuid.HighPart ||
    FAILED(device->GetDeviceRemovedReason()))
  {
    // Release references to resources related to the old device.

    dxgiDevice = nullptr;
    deviceAdapter = nullptr;
    deviceFactory = nullptr;
    previousDefaultAdapter = nullptr;

    // Create a new device and swap chain.
    result = handleDeviceLost();
  }

  return result;
}

dx::HResult dx::DxDeviceResources::handleDeviceLost()
{
  dx::HResult result;
  swapchain = nullptr;

  if (deviceNotify) deviceNotify->onDeviceLost();
  deviceLost(this);

  result = createDeviceResources();
  if (FAILED(result)) return result;

  result = createWindowSizeDependentResources();
  if (FAILED(result)) return result;

  if (deviceNotify) deviceNotify->onDeviceRestored();
  deviceRestored(this);

  return S_OK;
}

dx::HResult dx::DxDeviceResources::createWindowSizeDependentResources()
{
  dx::HResult result = S_OK;

  if (swapchainEffect == DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL)
  {
    if (renderTargetFormat != DXGI_FORMAT_R16G16B16A16_FLOAT &&
      renderTargetFormat != DXGI_FORMAT_B8G8R8A8_UNORM &&
      renderTargetFormat != DXGI_FORMAT_R8G8B8A8_UNORM)
      swapchainEffect = DXGI_SWAP_EFFECT_DISCARD;
  }

  //swapchainEffect = DXGI_SWAP_EFFECT_DISCARD;
  DXGI_SCALING scaling = swapchainEffect != DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL ?
    DXGI_SCALING::DXGI_SCALING_STRETCH :
    DXGI_SCALING::DXGI_SCALING_NONE;

  // Clear the previous window size specific context.
  dx::IRTView* nullViews[] = { nullptr };
  context->OMSetRenderTargets(ARRAYSIZE(nullViews), nullViews, nullptr);
  renderTargetView = nullptr;
  depthStencilView = nullptr;

  swapchainResizing(this);

  context->Flush();

  // Calculate the necessary render target size in pixels.
  // Prevent zero size DirectX content from being created.
  outputSize.Width = dips2Pels(logicalSize.Width, dpi.x);
  outputSize.Height = dips2Pels(logicalSize.Height, dpi.y);
  outputSize.Width = std::max(outputSize.Width, 8.f);
  outputSize.Height = std::max(outputSize.Height, 8.f);


  // The width and height of the swap chain must be based on the window's
  // natively-oriented width and height. If the window is not in the native
  // orientation, the dimensions must be reversed.
  dx::DxgiModeRotation displayRotation = computeDisplayRotation();

  bool swapDimensions = displayRotation == DXGI_MODE_ROTATION_ROTATE90 || displayRotation == DXGI_MODE_ROTATION_ROTATE270;
  renderTargetSize.Width = swapDimensions ? outputSize.Height : outputSize.Width;
  renderTargetSize.Height = swapDimensions ? outputSize.Width : outputSize.Height;

  if (swapchain.Get() != nullptr)
  {
    // If the swap chain already exists, resize it.
    result = swapchain->ResizeBuffers(
      swapchainBufferCount,
      static_cast<UINT>(renderTargetSize.Width), // 0
      static_cast<UINT>(renderTargetSize.Height), // 0
      renderTargetFormat,
      swapchainFlags
      );

    if (result == DXGI_ERROR_DEVICE_REMOVED || result == DXGI_ERROR_DEVICE_RESET)
    {
      // If the device was removed for any reason, a new device and swap chain will need to be created.
      handleDeviceLost();

      // Everything is set up now. Do not continue execution of this method. HandleDeviceLost will reenter this method 
      // and correctly set up the new device.
      return S_OK;
    }
    else
    {
      if (FAILED(result))
        return result;
    }
  }
  else
  {
#define _Dx_core_window_swapchain 1

#if !_Dx_core_window_swapchain
    DXGI_SWAP_CHAIN_DESC desc;

    DXGI_SWAP_CHAIN_DESC1 desc1;
    DXGI_SWAP_CHAIN_FULLSCREEN_DESC fsDesc;

    desc.BufferDesc.Width = static_cast<UINT>(renderTargetSize.Width);
    desc.BufferDesc.Height = static_cast<UINT>(renderTargetSize.Height);
    desc.BufferDesc.Format = renderTargetFormat;
    desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    desc.BufferDesc.RefreshRate.Denominator = 1;
    desc.BufferDesc.RefreshRate.Numerator = 60;
    desc.SampleDesc.Quality = 0;
    desc.SampleDesc.Count = 1;
    desc.BufferCount = swapchainBufferCount;
    desc.BufferUsage = renderTargetUsage;
    desc.SwapEffect = swapchainEffect;
    desc.Flags = swapchainFlags;
    desc.OutputWindow = host;
    desc.Windowed = windowed;

    Microsoft::WRL::ComPtr<IDXGIDevice1> dxgiDevice;
    result = device.As(&dxgiDevice);
    if (FAILED(result)) return result;

    Microsoft::WRL::ComPtr<IDXGIAdapter> dxgiAdapter;
    result = dxgiDevice->GetAdapter(&dxgiAdapter);
    if (FAILED(result)) return result;

    Microsoft::WRL::ComPtr<IDXGIFactory> dxgiFactory;
    result = dxgiAdapter->GetParent(IID_PPV_ARGS(&dxgiFactory));
    if (FAILED(result)) return result;

    Microsoft::WRL::ComPtr<IDXGISwapChain> swapchainUnderlayer;
    result = dxgiFactory->CreateSwapChain(device.Get(), &desc, swapchainUnderlayer.ReleaseAndGetAddressOf());
    if (FAILED(result)) return result;

    result = swapchainUnderlayer.As(&swapchain);
    if (FAILED(result)) return result;

#else

    // Otherwise, create a new one using the same adapter as the existing Direct3D device.
    DXGI_SWAP_CHAIN_DESC1 swapChainDesc = { 0 };

    swapChainDesc.Width = static_cast<UINT>(renderTargetSize.Width); // 0
    swapChainDesc.Height = static_cast<UINT>(renderTargetSize.Height); // 0
    swapChainDesc.Format = renderTargetFormat; // This is the most common swap chain format.
    swapChainDesc.Stereo = false;
    swapChainDesc.SampleDesc.Quality = 0;
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.BufferCount = swapchainBufferCount;
    swapChainDesc.BufferUsage = renderTargetUsage;
    swapChainDesc.SwapEffect = swapchainEffect;
    swapChainDesc.Flags = swapchainFlags;
    swapChainDesc.AlphaMode = swapchainAlphaMode;

    swapChainDesc.Scaling = DXGI_SCALING_STRETCH;

    DXGI_SWAP_CHAIN_FULLSCREEN_DESC swapChainFsDesc = { 0 };
    swapChainFsDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapChainFsDesc.RefreshRate.Denominator = 1;
    swapChainFsDesc.RefreshRate.Numerator = 60;
    swapChainFsDesc.Windowed = windowed;

    //OutputDebugString(__TEXT("\tForcing swapchain fullscreen scaling to centered\n"));
    //OutputDebugString(__TEXT("\tForcing swapchain fullscreen scaling to stretched\n"));
    //swapChainFsDesc.Scaling = DXGI_MODE_SCALING_CENTERED;
    //swapChainFsDesc.Scaling = DXGI_MODE_SCALING_STRETCHED;
    swapChainFsDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

    // This sequence obtains the DXGI factory that was used to create the Direct3D device above.
    Microsoft::WRL::ComPtr<IDXGIDevice1> dxgiDevice;
    result = device.As(&dxgiDevice);
    if (FAILED(result)) return result;

    Microsoft::WRL::ComPtr<IDXGIAdapter> dxgiAdapter;
    result = dxgiDevice->GetAdapter(&dxgiAdapter);
    if (FAILED(result)) return result;

    Microsoft::WRL::ComPtr<IDXGIFactory2> dxgiFactory;
    result = dxgiAdapter->GetParent(IID_PPV_ARGS(&dxgiFactory));
    if (FAILED(result)) return result;

    result = dxgiFactory->CreateSwapChainForHwnd(
      device.Get(), host,
      &swapChainDesc, &swapChainFsDesc,
      nullptr, swapchain.ReleaseAndGetAddressOf()
      );
    if (FAILED(result)) return result;

#endif

    if (vsync)
    {
      // Ensure that DXGI does not queue more than one frame at a time. This both reduces latency and
      // ensures that the application will only render after each VSync, minimizing power consumption.
      result = dxgiDevice->SetMaximumFrameLatency(1);
      if (FAILED(result)) return result;
    }
  }

  if (!windowed) // && FALSE)
  {
    BOOL fullscreen;
    Microsoft::WRL::ComPtr<IDXGIOutput> output;
    if (SUCCEEDED(swapchain->GetFullscreenState(
      &fullscreen, output.ReleaseAndGetAddressOf()
      ))) output = nullptr;
    else fullscreen = false;

    //::ShowWindow(host, SW_MINIMIZE);
    //::ShowWindow(host, SW_RESTORE);
  }

  swapchain->SetFullscreenState(
    !windowed, NULL
    );

  // Set the proper orientation for the swap chain, and generate 2D and
  // 3D matrix transformations for rendering to the rotated swap chain.
  // Note the rotation angle for the 2D and 3D transforms are different.
  // This is due to the difference in coordinate spaces. Additionally,
  // the 3D matrix is specified explicitly to avoid rounding errors.

  switch (displayRotation)
  {
  case DXGI_MODE_ROTATION_IDENTITY:
    orientationXform3D = DirectX::XMLoadFloat4x4(&ScreenRotation::rotation0);
    break;
  case DXGI_MODE_ROTATION_ROTATE90:
    orientationXform3D = DirectX::XMLoadFloat4x4(&ScreenRotation::rotation270);
    break;
  case DXGI_MODE_ROTATION_ROTATE180:
    orientationXform3D = DirectX::XMLoadFloat4x4(&ScreenRotation::rotation180);
    break;
  case DXGI_MODE_ROTATION_ROTATE270:
    orientationXform3D = DirectX::XMLoadFloat4x4(&ScreenRotation::rotation90);
    break;
  }

  // Create a render target view of the swap chain back buffer.
  Microsoft::WRL::ComPtr<ID3D11Texture2D> backBuffer;
  result = swapchain->GetBuffer(0, IID_PPV_ARGS(&backBuffer));
  if (FAILED(result)) return result;

  D3D11_TEXTURE2D_DESC backBufferDesc;
  backBuffer->GetDesc(&backBufferDesc);
  actualRenderTargetSize.Width = (float)backBufferDesc.Width;
  actualRenderTargetSize.Height = (float)backBufferDesc.Height;

  result = device->CreateRenderTargetView(
    backBuffer.Get(), nullptr,
    &renderTargetView
    );
  if (FAILED(result)) return result;

  // Create a depth stencil view for use with 3D rendering if needed.
  CD3D11_TEXTURE2D_DESC depthStencilDesc(
    DXGI_FORMAT_D24_UNORM_S8_UINT,
    static_cast<UINT>(actualRenderTargetSize.Width),
    static_cast<UINT>(actualRenderTargetSize.Height),
    1, // This depth stencil view has only one texture.
    1, // Use a single mipmap level.
    D3D11_BIND_DEPTH_STENCIL
    );

  Microsoft::WRL::ComPtr<ID3D11Texture2D> depthStencil;
  result = device->CreateTexture2D(
    &depthStencilDesc, nullptr,
    &depthStencil
    );
  if (FAILED(result)) return result;

  CD3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc(D3D11_DSV_DIMENSION_TEXTURE2D);
  result = device->CreateDepthStencilView(
    depthStencil.Get(),
    &depthStencilViewDesc,
    &depthStencilView
    );
  if (FAILED(result)) return result;

  viewport = CD3D11_VIEWPORT(
    0.0f, 0.0f,
    actualRenderTargetSize.Width,
    actualRenderTargetSize.Height
    );

  context->RSSetViewports(1, &viewport);

  swapchainResized(this);
  return result;
}

dx::DxgiModeRotation dx::DxDeviceResources::computeDisplayRotation()
{
  switch (currentOrientation)
  {
  case DMDO_90: return DXGI_MODE_ROTATION_ROTATE90;
  case DMDO_180: return DXGI_MODE_ROTATION_ROTATE180;
  case DMDO_270: return DXGI_MODE_ROTATION_ROTATE270;
  case DMDO_DEFAULT: return DXGI_MODE_ROTATION_IDENTITY;
  } return DXGI_MODE_ROTATION_UNSPECIFIED;
}

dx::HResult dx::DxDeviceResources::trim()
{
  return S_OK;
}

float dx::DxDeviceResources::dips2Pels(float dips, float dpi)
{
  return floorf(dips * dpi / dipsPerInch + 0.5f); // Round to nearest integer.
}

dx::HResult dx::DxDeviceResources::present()
{
  static dx::HResult result = S_OK;

  // The first argument instructs DXGI to block until VSync, putting the application
  // to sleep until the next VSync. This ensures we don't waste any cycles rendering
  // frames that will never be displayed to the screen.
  result = swapchain->Present(vsync, 0);

  // If the device was removed either by a disconnection or a driver upgrade, we 
  // must recreate all device resources.
  if (result == DXGI_ERROR_DEVICE_REMOVED || result == DXGI_ERROR_DEVICE_RESET)
  {
    return handleDeviceLost();
  }

  return S_OK;
}
