#include <Windows.h>

#define _Dx_sample_app_parse_cmd_args 0

#if _Dx_sample_app_parse_cmd_args
#include <tclap/CmdLine.h>
#endif

#include "DxSampleApp.h"
#include "DxUtils.h"

#if defined(_DEBUG) || defined(PROFILE)
#ifndef _CRTDBG_MAP_ALLOC
#define _CRTDBG_MAP_ALLOC
#endif
#include <stdlib.h>
#include <crtdbg.h>
#endif


bool dx::DxSampleApp::onMain()
{
  deviceResources.vsync = true;
  deviceResources.windowed = true;
  deviceResources.clearColor = DirectX::Color(0.1f, 0.1f, 0.1f, 1.0f);
  return true;
}

bool dx::DxSampleApp::onAppLoop()
{
  return true;
}

void dx::DxSampleApp::beginScene()
{
  deviceResources.clear(deviceResources.context.Get());
  deviceResources.bindToPipeline(deviceResources.context.Get());
}

void dx::DxSampleApp::endScene()
{
  deviceResources.present();
  deviceResources.unbindFromPipeline(deviceResources.context.Get());
}

void dx::DxSampleApp::onDeviceLost()
{
}

void dx::DxSampleApp::onDeviceRestored()
{
}

// program entry
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
#if _Dx_sample_app_parse_cmd_args
  bool useLargest = true;
  bool usePrimary = false;
  bool fullscreenMode = false;

  try
  {
    std::string args = lpCmdLine;
    std::stringstream argsStream(args);
    std::istream_iterator<std::string> argsIt(argsStream), argsEnd;
    std::vector<std::string> tokens(argsIt, argsEnd);

    TCLAP::CmdLine cmd("DirectX Sample Framework", '=', "0.99", false);
    TCLAP::SwitchArg fullscreenSwitch("f", "fullscreen", "Run sample in fullscreen mode", cmd, false);
    TCLAP::SwitchArg largestMonitorSwitch("l", "use-largest", "Use largest by working area monitor", cmd, true);
    TCLAP::SwitchArg primaryMonitorSwitch("p", "use-primary", "Use primary monitor", cmd, false);
    TCLAP::SwitchArg leakCheckSwitch("m", "leak-check", "Use primary monitor", cmd, false);
    cmd.parse(tokens);

    usePrimary = primaryMonitorSwitch.getValue();
    useLargest = !usePrimary || largestMonitorSwitch.getValue();
    fullscreenMode = fullscreenSwitch.getValue();

#if defined(_DEBUG) || defined(PROFILE)
    if (leakCheckSwitch.getValue())
    {
      _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
      _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
    }
#endif

  }
  catch (std::exception const &e)
  {
    dx::trace("dx:main: failed to parse cmd");
  }
#endif

  CoInitialize(0);
  if (auto sampleApp = dx::DxSampleAppFactory())
  {
    if (sampleApp->onMain())
    {
      sampleApp->window.create();
      sampleApp->window.hooks() = sampleApp;
      sampleApp->deviceResources.registerDeviceNotify(sampleApp);
      sampleApp->deviceResources.setWindow(sampleApp->window.handle);
      sampleApp->handleSizeChanged(&sampleApp->window);

      MSG e; dx::zeroMemory(e);
      if (sampleApp->onAppLoop()) while (true)
      {
        if (PeekMessage(&e, sampleApp->window.handle, 0, 0, PM_REMOVE)) TranslateMessage(&e), DispatchMessage(&e);
        if (e.message == WM_QUIT) break; else sampleApp->handlePaint(&sampleApp->window);
      }
    }

    delete sampleApp;
    return EXIT_SUCCESS;
  }

  CoUninitialize();
  return EXIT_FAILURE;
}


