#pragma once
#include <DxWindow.h>
#include <DxDeviceResources.h>

namespace dx
{
  class DxSampleApp 
    : public DxWindowHooks
    , public IDxDeviceNotify
  {
  public:
    DxSampleApp(D3D_DRIVER_TYPE driver = D3D_DRIVER_TYPE_HARDWARE)
      : deviceResources(driver)
    {
    }
    virtual ~DxSampleApp() { }

    DxWindow window;
    DxDeviceResources deviceResources;

    /// <summary>
    /// This function will be called just after application started and sample instance was created.
    /// It should return true, otherwise application closes.
    /// </summary>
    virtual bool onMain(void);
    /// <summary>
    /// This function will be called just before entering a main message loop.
    /// It should return true, otherwise application does not enter a message loop and closes.
    /// </summary>
    virtual bool onAppLoop(void);
    /// <summary>
    /// This function will be called after GPU device was lost.
    /// </summary>
    virtual void onDeviceLost(void);
    /// <summary>
    /// This function will be called after GPU device was restored.
    /// </summary>
    virtual void onDeviceRestored(void);

    /// <summary>
    /// Initializes rendering loop: clears back buffer, binds default render target and depth stencil views to the pipeline.
    /// </summary>
    void beginScene(void);
    /// <summary>
    /// Finalizes rendering loop: presents back buffer, unbinds default render target and depth stencil views from the pipeline.
    /// </summary>
    void endScene(void);

  };

  extern DxSampleApp *DxSampleAppFactory(void);
}

