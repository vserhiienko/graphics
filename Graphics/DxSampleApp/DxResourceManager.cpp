#include "DxUtils.h"
#include "DxConverter.h"
#include "DxAssetManager.h"
#include "DxResourceManager.h"

#include <memory>
#include <cassert>

//using namespace Nena;
//using namespace Nena::Utility;
//using namespace Nena::PlatformHelp;
//using namespace Nena::Graphics;
//using namespace Nena::Graphics::Resources;

//-------------------------------------------------------------------------------------
// WIC Pixel Format Translation Data
//-------------------------------------------------------------------------------------
struct WICTranslate
{
  GUID wic;
  DXGI_FORMAT format;
};

static WICTranslate g_WICFormats[] =
{
  { GUID_WICPixelFormat128bppRGBAFloat, DXGI_FORMAT_R32G32B32A32_FLOAT },

  { GUID_WICPixelFormat64bppRGBAHalf, DXGI_FORMAT_R16G16B16A16_FLOAT },
  { GUID_WICPixelFormat64bppRGBA, DXGI_FORMAT_R16G16B16A16_UNORM },

  { GUID_WICPixelFormat32bppRGBA, DXGI_FORMAT_R8G8B8A8_UNORM },
  { GUID_WICPixelFormat32bppBGRA, DXGI_FORMAT_B8G8R8A8_UNORM }, // DXGI 1.1
  { GUID_WICPixelFormat32bppBGR, DXGI_FORMAT_B8G8R8X8_UNORM }, // DXGI 1.1

  { GUID_WICPixelFormat32bppRGBA1010102XR, DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM }, // DXGI 1.1
  { GUID_WICPixelFormat32bppRGBA1010102, DXGI_FORMAT_R10G10B10A2_UNORM },

#ifdef DXGI_1_2_FORMATS

  { GUID_WICPixelFormat16bppBGRA5551, DXGI_FORMAT_B5G5R5A1_UNORM },
  { GUID_WICPixelFormat16bppBGR565, DXGI_FORMAT_B5G6R5_UNORM },

#endif // DXGI_1_2_FORMATS

  { GUID_WICPixelFormat32bppGrayFloat, DXGI_FORMAT_R32_FLOAT },
  { GUID_WICPixelFormat16bppGrayHalf, DXGI_FORMAT_R16_FLOAT },
  { GUID_WICPixelFormat16bppGray, DXGI_FORMAT_R16_UNORM },
  { GUID_WICPixelFormat8bppGray, DXGI_FORMAT_R8_UNORM },

  { GUID_WICPixelFormat8bppAlpha, DXGI_FORMAT_A8_UNORM },
};

//-------------------------------------------------------------------------------------
// WIC Pixel Format nearest conversion table
//-------------------------------------------------------------------------------------

struct WICConvert
{
  GUID source;
  GUID target;
};

static WICConvert g_WICConvert[] =
{
  // Note target GUID in this conversion table must be one of those directly supported formats (above).

  { GUID_WICPixelFormatBlackWhite, GUID_WICPixelFormat8bppGray }, // DXGI_FORMAT_R8_UNORM

  { GUID_WICPixelFormat1bppIndexed, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM 
  { GUID_WICPixelFormat2bppIndexed, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM 
  { GUID_WICPixelFormat4bppIndexed, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM 
  { GUID_WICPixelFormat8bppIndexed, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM 

  { GUID_WICPixelFormat2bppGray, GUID_WICPixelFormat8bppGray }, // DXGI_FORMAT_R8_UNORM 
  { GUID_WICPixelFormat4bppGray, GUID_WICPixelFormat8bppGray }, // DXGI_FORMAT_R8_UNORM 

  { GUID_WICPixelFormat16bppGrayFixedPoint, GUID_WICPixelFormat16bppGrayHalf }, // DXGI_FORMAT_R16_FLOAT 
  { GUID_WICPixelFormat32bppGrayFixedPoint, GUID_WICPixelFormat32bppGrayFloat }, // DXGI_FORMAT_R32_FLOAT 

#ifdef DXGI_1_2_FORMATS

  { GUID_WICPixelFormat16bppBGR555, GUID_WICPixelFormat16bppBGRA5551 }, // DXGI_FORMAT_B5G5R5A1_UNORM

#else

  { GUID_WICPixelFormat16bppBGR555, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM
  { GUID_WICPixelFormat16bppBGRA5551, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM
  { GUID_WICPixelFormat16bppBGR565, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM

#endif // DXGI_1_2_FORMATS

  { GUID_WICPixelFormat32bppBGR101010, GUID_WICPixelFormat32bppRGBA1010102 }, // DXGI_FORMAT_R10G10B10A2_UNORM

  { GUID_WICPixelFormat24bppBGR, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM 
  { GUID_WICPixelFormat24bppRGB, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM 
  { GUID_WICPixelFormat32bppPBGRA, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM 
  { GUID_WICPixelFormat32bppPRGBA, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM 

  { GUID_WICPixelFormat48bppRGB, GUID_WICPixelFormat64bppRGBA }, // DXGI_FORMAT_R16G16B16A16_UNORM
  { GUID_WICPixelFormat48bppBGR, GUID_WICPixelFormat64bppRGBA }, // DXGI_FORMAT_R16G16B16A16_UNORM
  { GUID_WICPixelFormat64bppBGRA, GUID_WICPixelFormat64bppRGBA }, // DXGI_FORMAT_R16G16B16A16_UNORM
  { GUID_WICPixelFormat64bppPRGBA, GUID_WICPixelFormat64bppRGBA }, // DXGI_FORMAT_R16G16B16A16_UNORM
  { GUID_WICPixelFormat64bppPBGRA, GUID_WICPixelFormat64bppRGBA }, // DXGI_FORMAT_R16G16B16A16_UNORM

  { GUID_WICPixelFormat48bppRGBFixedPoint, GUID_WICPixelFormat64bppRGBAHalf }, // DXGI_FORMAT_R16G16B16A16_FLOAT 
  { GUID_WICPixelFormat48bppBGRFixedPoint, GUID_WICPixelFormat64bppRGBAHalf }, // DXGI_FORMAT_R16G16B16A16_FLOAT 
  { GUID_WICPixelFormat64bppRGBAFixedPoint, GUID_WICPixelFormat64bppRGBAHalf }, // DXGI_FORMAT_R16G16B16A16_FLOAT 
  { GUID_WICPixelFormat64bppBGRAFixedPoint, GUID_WICPixelFormat64bppRGBAHalf }, // DXGI_FORMAT_R16G16B16A16_FLOAT 
  { GUID_WICPixelFormat64bppRGBFixedPoint, GUID_WICPixelFormat64bppRGBAHalf }, // DXGI_FORMAT_R16G16B16A16_FLOAT 
  { GUID_WICPixelFormat64bppRGBHalf, GUID_WICPixelFormat64bppRGBAHalf }, // DXGI_FORMAT_R16G16B16A16_FLOAT 
  { GUID_WICPixelFormat48bppRGBHalf, GUID_WICPixelFormat64bppRGBAHalf }, // DXGI_FORMAT_R16G16B16A16_FLOAT 

  { GUID_WICPixelFormat128bppPRGBAFloat, GUID_WICPixelFormat128bppRGBAFloat }, // DXGI_FORMAT_R32G32B32A32_FLOAT 
  { GUID_WICPixelFormat128bppRGBFloat, GUID_WICPixelFormat128bppRGBAFloat }, // DXGI_FORMAT_R32G32B32A32_FLOAT 
  { GUID_WICPixelFormat128bppRGBAFixedPoint, GUID_WICPixelFormat128bppRGBAFloat }, // DXGI_FORMAT_R32G32B32A32_FLOAT 
  { GUID_WICPixelFormat128bppRGBFixedPoint, GUID_WICPixelFormat128bppRGBAFloat }, // DXGI_FORMAT_R32G32B32A32_FLOAT 
  { GUID_WICPixelFormat32bppRGBE, GUID_WICPixelFormat128bppRGBAFloat }, // DXGI_FORMAT_R32G32B32A32_FLOAT 

  { GUID_WICPixelFormat32bppCMYK, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM 
  { GUID_WICPixelFormat64bppCMYK, GUID_WICPixelFormat64bppRGBA }, // DXGI_FORMAT_R16G16B16A16_UNORM
  { GUID_WICPixelFormat40bppCMYKAlpha, GUID_WICPixelFormat64bppRGBA }, // DXGI_FORMAT_R16G16B16A16_UNORM
  { GUID_WICPixelFormat80bppCMYKAlpha, GUID_WICPixelFormat64bppRGBA }, // DXGI_FORMAT_R16G16B16A16_UNORM

#if defined(_WIN32_WINNT_WIN8) && (_WIN32_WINNT >= _WIN32_WINNT_WIN8) || defined(_WIN7_PLATFORM_UPDATE)
  { GUID_WICPixelFormat32bppRGB, GUID_WICPixelFormat32bppRGBA }, // DXGI_FORMAT_R8G8B8A8_UNORM
  { GUID_WICPixelFormat64bppRGB, GUID_WICPixelFormat64bppRGBA }, // DXGI_FORMAT_R16G16B16A16_UNORM
  { GUID_WICPixelFormat64bppPRGBAHalf, GUID_WICPixelFormat64bppRGBAHalf }, // DXGI_FORMAT_R16G16B16A16_FLOAT 
#endif

  // We don't support n-channel formats
};

static bool g_WIC2 = false;

//--------------------------------------------------------------------------------------
namespace DirectX
{

  bool _IsWIC2()
  {
    return g_WIC2;
  }

  dx::IWICImagingFactory* _GetWIC()
  {
    static dx::IWICImagingFactory* s_Factory = nullptr;

    if (s_Factory)
      return s_Factory;

#if 0 //defined(_WIN32_WINNT_WIN8) && (_WIN32_WINNT >= _WIN32_WINNT_WIN8) || defined(_WIN7_PLATFORM_UPDATE)
    dx::HResult hr = CoCreateInstance(
      CLSID_WICImagingFactory2,
      nullptr,
      CLSCTX_INPROC_SERVER,
      __uuidof(IWICImagingFactory2),
      (LPVOID*)&s_Factory
      );

    if (SUCCEEDED(hr))
    {
      // WIC2 is available on Windows 8 and Windows 7 SP1 with KB 2670838 installed
      g_WIC2 = true;
    }
    else
    {
      hr = CoCreateInstance(
        CLSID_WICImagingFactory1,
        nullptr,
        CLSCTX_INPROC_SERVER,
        __uuidof(dx::IWICImagingFactory),
        (LPVOID*)&s_Factory
        );

      if (FAILED(hr))
      {
        s_Factory = nullptr;
        return nullptr;
      }
    }
#else
    dx::HResult hr = CoCreateInstance(
      CLSID_WICImagingFactory,
      nullptr,
      CLSCTX_INPROC_SERVER,
      __uuidof(::IWICImagingFactory),
      (LPVOID*)&s_Factory
      );

    if (FAILED(hr))
    {
      s_Factory = nullptr;
      return nullptr;
    }
#endif

    return s_Factory;
  }

} // namespace DirectX

using namespace DirectX;

//---------------------------------------------------------------------------------
static DXGI_FORMAT _WICToDXGI(const GUID& guid)
{
  for (size_t i = 0; i < _countof(g_WICFormats); ++i)
  {
    if (memcmp(&g_WICFormats[i].wic, &guid, sizeof(GUID)) == 0)
      return g_WICFormats[i].format;
  }

#if defined(_WIN32_WINNT_WIN8) && (_WIN32_WINNT >= _WIN32_WINNT_WIN8) || defined(_WIN7_PLATFORM_UPDATE)
  if (g_WIC2)
  {
    if (memcmp(&GUID_WICPixelFormat96bppRGBFloat, &guid, sizeof(GUID)) == 0)
      return DXGI_FORMAT_R32G32B32_FLOAT;
  }
#endif

  return DXGI_FORMAT_UNKNOWN;
}

//---------------------------------------------------------------------------------
static size_t _WICBitsPerPixel(REFGUID targetGuid)
{
  dx::IWICImagingFactory* pWIC = _GetWIC();
  if (!pWIC)
    return 0;

  Microsoft::WRL::ComPtr<IWICComponentInfo> cinfo;
  if (FAILED(pWIC->CreateComponentInfo(targetGuid, &cinfo)))
    return 0;

  WICComponentType type;
  if (FAILED(cinfo->GetComponentType(&type)))
    return 0;

  if (type != WICPixelFormat)
    return 0;

  Microsoft::WRL::ComPtr<IWICPixelFormatInfo> pfinfo;
  if (FAILED(cinfo.As(&pfinfo)))
    return 0;

  UINT bpp;
  if (FAILED(pfinfo->GetBitsPerPixel(&bpp)))
    return 0;

  return bpp;
}


//--------------------------------------------------------------------------------------
static DXGI_FORMAT MakeSRGB(_In_ DXGI_FORMAT format)
{
  switch (format)
  {
  case DXGI_FORMAT_R8G8B8A8_UNORM:
    return DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;

  case DXGI_FORMAT_BC1_UNORM:
    return DXGI_FORMAT_BC1_UNORM_SRGB;

  case DXGI_FORMAT_BC2_UNORM:
    return DXGI_FORMAT_BC2_UNORM_SRGB;

  case DXGI_FORMAT_BC3_UNORM:
    return DXGI_FORMAT_BC3_UNORM_SRGB;

  case DXGI_FORMAT_B8G8R8A8_UNORM:
    return DXGI_FORMAT_B8G8R8A8_UNORM_SRGB;

  case DXGI_FORMAT_B8G8R8X8_UNORM:
    return DXGI_FORMAT_B8G8R8X8_UNORM_SRGB;

  case DXGI_FORMAT_BC7_UNORM:
    return DXGI_FORMAT_BC7_UNORM_SRGB;

  default:
    return format;
  }
}


//---------------------------------------------------------------------------------
static dx::HResult CreateTextureFromWIC(_In_ ID3D11Device* d3dDevice,
  _In_opt_ ID3D11DeviceContext* d3dContext,
  _In_ dx::IWICBitmapFrameDecode *frame,
  _In_ size_t maxsize,
  _In_ D3D11_USAGE usage,
  _In_ unsigned int bindFlags,
  _In_ unsigned int cpuAccessFlags,
  _In_ unsigned int miscFlags,
  _In_ bool forceSRGB,
  _Out_opt_ ID3D11Resource** texture,
  _Out_opt_ ID3D11ShaderResourceView** textureView)
{
  UINT width, height;
  dx::HResult hr = frame->GetSize(&width, &height);
  if (FAILED(hr))
    return hr;

  assert(width > 0 && height > 0);

  if (!maxsize)
  {
    // This is a bit conservative because the hardware could support larger textures than
    // the Feature Level defined minimums, but doing it this way is much easier and more
    // performant for WIC than the 'fail and retry' model used by DDSTextureLoader

    switch (d3dDevice->GetFeatureLevel())
    {
    case D3D_FEATURE_LEVEL_9_1:
    case D3D_FEATURE_LEVEL_9_2:
      maxsize = 2048 /*D3D_FL9_1_REQ_TEXTURE2D_U_OR_V_DIMENSION*/;
      break;

    case D3D_FEATURE_LEVEL_9_3:
      maxsize = 4096 /*D3D_FL9_3_REQ_TEXTURE2D_U_OR_V_DIMENSION*/;
      break;

    case D3D_FEATURE_LEVEL_10_0:
    case D3D_FEATURE_LEVEL_10_1:
      maxsize = 8192 /*D3D10_REQ_TEXTURE2D_U_OR_V_DIMENSION*/;
      break;

    default:
      maxsize = D3D11_REQ_TEXTURE2D_U_OR_V_DIMENSION;
      break;
    }
  }

  assert(maxsize > 0);

  UINT twidth, theight;
  if (width > maxsize || height > maxsize)
  {
    float ar = static_cast<float>(height) / static_cast<float>(width);
    if (width > height)
    {
      twidth = static_cast<UINT>(maxsize);
      theight = static_cast<UINT>(static_cast<float>(maxsize)* ar);
    }
    else
    {
      theight = static_cast<UINT>(maxsize);
      twidth = static_cast<UINT>(static_cast<float>(maxsize) / ar);
    }
    assert(twidth <= maxsize && theight <= maxsize);
  }
  else
  {
    twidth = width;
    theight = height;
  }

  // Determine format
  WICPixelFormatGUID pixelFormat;
  hr = frame->GetPixelFormat(&pixelFormat);
  if (FAILED(hr))
    return hr;

  WICPixelFormatGUID convertGUID;
  memcpy(&convertGUID, &pixelFormat, sizeof(WICPixelFormatGUID));

  size_t bpp = 0;

  DXGI_FORMAT format = _WICToDXGI(pixelFormat);
  if (format == DXGI_FORMAT_UNKNOWN)
  {
    if (memcmp(&GUID_WICPixelFormat96bppRGBFixedPoint, &pixelFormat, sizeof(WICPixelFormatGUID)) == 0)
    {
#if defined(_WIN32_WINNT_WIN8) && (_WIN32_WINNT >= _WIN32_WINNT_WIN8) || defined(_WIN7_PLATFORM_UPDATE)
      if (g_WIC2)
      {
        memcpy(&convertGUID, &GUID_WICPixelFormat96bppRGBFloat, sizeof(WICPixelFormatGUID));
        format = DXGI_FORMAT_R32G32B32_FLOAT;
      }
      else
#endif
      {
        memcpy(&convertGUID, &GUID_WICPixelFormat128bppRGBAFloat, sizeof(WICPixelFormatGUID));
        format = DXGI_FORMAT_R32G32B32A32_FLOAT;
      }
    }
    else
    {
      for (size_t i = 0; i < _countof(g_WICConvert); ++i)
      {
        if (memcmp(&g_WICConvert[i].source, &pixelFormat, sizeof(WICPixelFormatGUID)) == 0)
        {
          memcpy(&convertGUID, &g_WICConvert[i].target, sizeof(WICPixelFormatGUID));

          format = _WICToDXGI(g_WICConvert[i].target);
          assert(format != DXGI_FORMAT_UNKNOWN);
          bpp = _WICBitsPerPixel(convertGUID);
          break;
        }
      }
    }

    if (format == DXGI_FORMAT_UNKNOWN)
      return HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED);
  }
  else
  {
    bpp = _WICBitsPerPixel(pixelFormat);
  }

#if (_WIN32_WINNT >= _WIN32_WINNT_WIN8) || defined(_WIN7_PLATFORM_UPDATE)
  if ((format == DXGI_FORMAT_R32G32B32_FLOAT) && d3dContext != 0 && textureView != 0)
  {
    // Special case test for optional device support for autogen mipchains for R32G32B32_FLOAT 
    UINT fmtSupport = 0;
    hr = d3dDevice->CheckFormatSupport(DXGI_FORMAT_R32G32B32_FLOAT, &fmtSupport);
    if (FAILED(hr) || !(fmtSupport & D3D11_FORMAT_SUPPORT_MIP_AUTOGEN))
    {
      // Use R32G32B32A32_FLOAT instead which is required for Feature Level 10.0 and up
      memcpy(&convertGUID, &GUID_WICPixelFormat128bppRGBAFloat, sizeof(WICPixelFormatGUID));
      format = DXGI_FORMAT_R32G32B32A32_FLOAT;
      bpp = 128;
    }
  }
#endif

  if (!bpp)
    return E_FAIL;

  // Handle sRGB formats
  if (forceSRGB)
  {
    format = MakeSRGB(format);
  }
  else
  {
    Microsoft::WRL::ComPtr<IWICMetadataQueryReader> metareader;
    if (SUCCEEDED(frame->GetMetadataQueryReader(&metareader)))
    {
      GUID containerFormat;
      if (SUCCEEDED(metareader->GetContainerFormat(&containerFormat)))
      {
        // Check for sRGB colorspace metadata
        bool sRGB = false;

        PROPVARIANT value;
        PropVariantInit(&value);

        if (memcmp(&containerFormat, &GUID_ContainerFormatPng, sizeof(GUID)) == 0)
        {
          // Check for sRGB chunk
          if (SUCCEEDED(metareader->GetMetadataByName(L"/sRGB/RenderingIntent", &value)) && value.vt == VT_UI1)
          {
            sRGB = true;
          }
        }
        else if (SUCCEEDED(metareader->GetMetadataByName(L"System.Image.ColorSpace", &value)) && value.vt == VT_UI2 && value.uiVal == 1)
        {
          sRGB = true;
        }

        PropVariantClear(&value);

        if (sRGB)
          format = MakeSRGB(format);
      }
    }
  }

  // Verify our target format is supported by the current device
  // (handles WDDM 1.0 or WDDM 1.1 device driver cases as well as DirectX 11.0 Runtime without 16bpp format support)
  UINT support = 0;
  hr = d3dDevice->CheckFormatSupport(format, &support);
  if (FAILED(hr) || !(support & D3D11_FORMAT_SUPPORT_TEXTURE2D))
  {
    // Fallback to RGBA 32-bit format which is supported by all devices
    memcpy(&convertGUID, &GUID_WICPixelFormat32bppRGBA, sizeof(WICPixelFormatGUID));
    format = DXGI_FORMAT_R8G8B8A8_UNORM;
    bpp = 32;
  }

  // Allocate temporary memory for image
  size_t rowPitch = (twidth * bpp + 7) / 8;
  size_t imageSize = rowPitch * theight;

  std::unique_ptr<uint8_t[]> temp(new (std::nothrow) uint8_t[imageSize]);
  if (!temp)
    return E_OUTOFMEMORY;

  // Load image data
  if (memcmp(&convertGUID, &pixelFormat, sizeof(GUID)) == 0
    && twidth == width
    && theight == height)
  {
    // No format conversion or resize needed
    hr = frame->CopyPixels(0, static_cast<UINT>(rowPitch), static_cast<UINT>(imageSize), temp.get());
    if (FAILED(hr))
      return hr;
  }
  else if (twidth != width || theight != height)
  {
    // Resize
    dx::IWICImagingFactory* pWIC = _GetWIC();
    if (!pWIC)
      return E_NOINTERFACE;

    Microsoft::WRL::ComPtr<IWICBitmapScaler> scaler;
    hr = pWIC->CreateBitmapScaler(&scaler);
    if (FAILED(hr))
      return hr;

    hr = scaler->Initialize(frame, twidth, theight, WICBitmapInterpolationModeFant);
    if (FAILED(hr))
      return hr;

    WICPixelFormatGUID pfScaler;
    hr = scaler->GetPixelFormat(&pfScaler);
    if (FAILED(hr))
      return hr;

    if (memcmp(&convertGUID, &pfScaler, sizeof(GUID)) == 0)
    {
      // No format conversion needed
      hr = scaler->CopyPixels(0, static_cast<UINT>(rowPitch), static_cast<UINT>(imageSize), temp.get());
      if (FAILED(hr))
        return hr;
    }
    else
    {
      Microsoft::WRL::ComPtr<dx::IWICFormatConverter> FC;
      hr = pWIC->CreateFormatConverter(&FC);
      if (FAILED(hr))
        return hr;

      hr = FC->Initialize(scaler.Get(), convertGUID, WICBitmapDitherTypeErrorDiffusion, 0, 0, WICBitmapPaletteTypeCustom);
      if (FAILED(hr))
        return hr;

      hr = FC->CopyPixels(0, static_cast<UINT>(rowPitch), static_cast<UINT>(imageSize), temp.get());
      if (FAILED(hr))
        return hr;
    }
  }
  else
  {
    // Format conversion but no resize
    dx::IWICImagingFactory* pWIC = _GetWIC();
    if (!pWIC)
      return E_NOINTERFACE;

    Microsoft::WRL::ComPtr<dx::IWICFormatConverter> FC;
    hr = pWIC->CreateFormatConverter(&FC);
    if (FAILED(hr))
      return hr;

    hr = FC->Initialize(frame, convertGUID, WICBitmapDitherTypeErrorDiffusion, 0, 0, WICBitmapPaletteTypeCustom);
    if (FAILED(hr))
      return hr;

    hr = FC->CopyPixels(0, static_cast<UINT>(rowPitch), static_cast<UINT>(imageSize), temp.get());
    if (FAILED(hr))
      return hr;
  }

  // See if format is supported for auto-gen mipmaps (varies by feature level)
  bool autogen = false;
  if (d3dContext != 0 && textureView != 0) // Must have context and shader-view to auto generate mipmaps
  {
    UINT fmtSupport = 0;
    hr = d3dDevice->CheckFormatSupport(format, &fmtSupport);
    if (SUCCEEDED(hr) && (fmtSupport & D3D11_FORMAT_SUPPORT_MIP_AUTOGEN))
    {
      autogen = true;
    }
  }

  // Create texture
  D3D11_TEXTURE2D_DESC desc;
  desc.Width = twidth;
  desc.Height = theight;
  desc.MipLevels = (autogen) ? 0 : 1;
  desc.ArraySize = 1;
  desc.Format = format;
  desc.SampleDesc.Count = 1;
  desc.SampleDesc.Quality = 0;
  desc.Usage = usage;
  desc.CPUAccessFlags = cpuAccessFlags;

  if (autogen)
  {
    desc.BindFlags = bindFlags | D3D11_BIND_RENDER_TARGET;
    desc.MiscFlags = miscFlags | D3D11_RESOURCE_MISC_GENERATE_MIPS;
  }
  else
  {
    desc.BindFlags = bindFlags;
    desc.MiscFlags = miscFlags;
  }

  D3D11_SUBRESOURCE_DATA initData;
  initData.pSysMem = temp.get();
  initData.SysMemPitch = static_cast<UINT>(rowPitch);
  initData.SysMemSlicePitch = static_cast<UINT>(imageSize);

  ID3D11Texture2D* tex = nullptr;
  hr = d3dDevice->CreateTexture2D(&desc, (autogen) ? nullptr : &initData, &tex);
  if (SUCCEEDED(hr) && tex != 0)
  {
    if (textureView != 0)
    {
      D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
      memset(&SRVDesc, 0, sizeof(SRVDesc));
      SRVDesc.Format = desc.Format;

      SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
      SRVDesc.Texture2D.MipLevels = (autogen) ? -1 : 1;

      hr = d3dDevice->CreateShaderResourceView(tex, &SRVDesc, textureView);
      if (FAILED(hr))
      {
        tex->Release();
        return hr;
      }

      if (autogen)
      {
        assert(d3dContext != 0);
        d3dContext->UpdateSubresource(tex, 0, nullptr, temp.get(), static_cast<UINT>(rowPitch), static_cast<UINT>(imageSize));
        d3dContext->GenerateMips(*textureView);
      }
    }

    if (texture != 0)
    {
      *texture = tex;
    }
    else
    {
      dx::DxResourceManager::setDebugName(tex, "DxSampleApp_WICTextureLoaderResource");
      tex->Release();
    }
  }

  return hr;
}

//--------------------------------------------------------------------------------------
////_Use_decl_annotations_
dx::HResult dx::DxResourceManager::createWICTextureFromMemory(
  IDirect3DDevice* d3dDevice,
  IDirect3DContext* d3dContext,
  const uint8_t* wicData,
  size_t wicDataSize,
  IDirect3DResource** texture,
  ISRView** textureView,
  size_t maxsize
  )
{
  return createWICTextureFromMemoryEx(
    d3dDevice, 
    d3dContext, 
    wicData, 
    wicDataSize, 
    maxsize,
    D3D11_USAGE_DEFAULT,
    D3D11_BIND_SHADER_RESOURCE,
    0, 
    0, 
    false,
    texture, 
    textureView
    );
}

////_Use_decl_annotations_
dx::HResult dx::DxResourceManager::createWICTextureFromMemoryEx(
  IDirect3DDevice* d3dDevice,
  IDirect3DContext* d3dContext,
  const uint8_t* wicData,
  size_t wicDataSize,
  size_t maxsize,
  D3D11_USAGE usage,
  unsigned int bindFlags,
  unsigned int cpuAccessFlags,
  unsigned int miscFlags,
  bool forceSRGB,
  IDirect3DResource** texture,
  ISRView** textureView
  )
{
  if (texture)
  {
    *texture = nullptr;
  }
  if (textureView)
  {
    *textureView = nullptr;
  }

  if (!d3dDevice || !wicData || (!texture && !textureView))
    return E_INVALIDARG;

  if (!wicDataSize)
    return E_FAIL;

#ifdef _M_AMD64
  if (wicDataSize > 0xFFFFFFFF)
    return HRESULT_FROM_WIN32(ERROR_FILE_TOO_LARGE);
#endif

  dx::IWICImagingFactory* pWIC = _GetWIC();
  if (!pWIC)
    return E_NOINTERFACE;

  // Create input stream for memory
  Microsoft::WRL::ComPtr<dx::IWICStream> stream;
  dx::HResult hr = pWIC->CreateStream(&stream);
  if (FAILED(hr))
    return hr;

  hr = stream->InitializeFromMemory(const_cast<uint8_t*>(wicData), static_cast<DWORD>(wicDataSize));
  if (FAILED(hr))
    return hr;

  // Initialize WIC
  Microsoft::WRL::ComPtr<dx::IWICBitmapDecoder> decoder;
  hr = pWIC->CreateDecoderFromStream(stream.Get(), 0, WICDecodeMetadataCacheOnDemand, &decoder);
  if (FAILED(hr))
    return hr;

  Microsoft::WRL::ComPtr<dx::IWICBitmapFrameDecode> frame;
  hr = decoder->GetFrame(0, &frame);
  if (FAILED(hr))
    return hr;

  hr = CreateTextureFromWIC(d3dDevice, d3dContext, frame.Get(), maxsize,
    usage, bindFlags, cpuAccessFlags, miscFlags, forceSRGB,
    texture, textureView);
  if (FAILED(hr))
    return hr;

  if (texture != 0 && *texture != 0)
  {
    dx::DxResourceManager::setDebugName(*texture, "DxSampleApp_WICTextureLoaderResource");
    //SetDebugObjectName(*texture, "DxSampleApp_WICTextureLoaderResource");
  }

  if (textureView != 0 && *textureView != 0)
  {
    dx::DxResourceManager::setDebugName(*textureView, "DxSampleApp_WICTextureLoaderResource");
    //SetDebugObjectName(*textureView, "DxSampleApp_WICTextureLoaderResource");
  }

  return hr;
}

//--------------------------------------------------------------------------------------
//_Use_decl_annotations_
dx::HResult dx::DxResourceManager::createWICTextureFromFile(
  IDirect3DDevice* d3dDevice,
  IDirect3DContext* d3dContext,
  const wchar_t* fileName,
  IDirect3DResource** texture,
  ISRView** textureView,
  size_t maxsize
  )
{
  return createWICTextureFromFileEx(d3dDevice, d3dContext, fileName, maxsize,
    D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, false,
    texture, textureView);
}

//_Use_decl_annotations_
dx::HResult dx::DxResourceManager::createWICTextureFromFileEx(
  IDirect3DDevice* d3dDevice,
  IDirect3DContext* d3dContext,
  const wchar_t* fileName,
  size_t maxsize,
  D3D11_USAGE usage,
  unsigned int bindFlags,
  unsigned int cpuAccessFlags,
  unsigned int miscFlags,
  bool forceSRGB,
  IDirect3DResource** texture,
  ISRView** textureView
  )
{
  if (texture)
  {
    *texture = nullptr;
  }
  if (textureView)
  {
    *textureView = nullptr;
  }

  if (!d3dDevice || !fileName || (!texture && !textureView))
    return E_INVALIDARG;

  dx::IWICImagingFactory* pWIC = _GetWIC();
  if (!pWIC)
    return E_NOINTERFACE;

  // Initialize WIC
  Microsoft::WRL::ComPtr<dx::IWICBitmapDecoder> decoder;
  dx::HResult hr = pWIC->CreateDecoderFromFilename(fileName, 0, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &decoder);
  if (FAILED(hr))
    return hr;

  Microsoft::WRL::ComPtr<dx::IWICBitmapFrameDecode> frame;
  hr = decoder->GetFrame(0, &frame);
  if (FAILED(hr))
    return hr;

  hr = CreateTextureFromWIC(d3dDevice, d3dContext, frame.Get(), maxsize,
    usage, bindFlags, cpuAccessFlags, miscFlags, forceSRGB,
    texture, textureView);

#if !defined(NO_D3D11_DEBUG_NAME) && ( defined(_DEBUG) || defined(PROFILE) )
  if (SUCCEEDED(hr))
  {
    if (texture != 0 || textureView != 0)
    {
      CHAR strFileA[MAX_PATH];
      int result = WideCharToMultiByte(CP_ACP,
        WC_NO_BEST_FIT_CHARS,
        fileName,
        -1,
        strFileA,
        MAX_PATH,
        nullptr,
        FALSE
        );
      if (result > 0)
      {
        const CHAR* pstrName = strrchr(strFileA, '\\');
        if (!pstrName)
        {
          pstrName = strFileA;
        }
        else
        {
          pstrName++;
        }

        if (texture != 0 && *texture != 0)
        {
          (*texture)->SetPrivateData(WKPDID_D3DDebugObjectName,
            static_cast<UINT>(strnlen_s(pstrName, MAX_PATH)),
            pstrName
            );
        }

        if (textureView != 0 && *textureView != 0)
        {
          (*textureView)->SetPrivateData(WKPDID_D3DDebugObjectName,
            static_cast<UINT>(strnlen_s(pstrName, MAX_PATH)),
            pstrName
            );
        }
      }
    }
  }
#endif

  return hr;
}



// just sets default values to desc provided / use before setting your own configuration
void dx::DxResourceManager::defaults(dx::DepthStencilStateDescription &depthStencilDesc)
{
  // Set up the description of the stencil state.
  depthStencilDesc.DepthEnable = true;
  depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
  depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
  depthStencilDesc.StencilEnable = true;
  depthStencilDesc.StencilReadMask = 0xFF;
  depthStencilDesc.StencilWriteMask = 0xFF;

  // Stencil operations if pixel is front-facing.
  depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
  depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
  depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
  depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

  // Stencil operations if pixel is back-facing.
  depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
  depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
  depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
  depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

}
void dx::DxResourceManager::defaults(dx::RasterizerStateDescription &rasterDesc)
{
  rasterDesc.AntialiasedLineEnable = false;
  rasterDesc.CullMode = D3D11_CULL_BACK;
  rasterDesc.DepthBias = 0;
  rasterDesc.DepthBiasClamp = 0.0f;
  rasterDesc.DepthClipEnable = true;
  rasterDesc.FillMode = D3D11_FILL_SOLID;
  rasterDesc.FrontCounterClockwise = false;
  rasterDesc.MultisampleEnable = false;
  rasterDesc.ScissorEnable = false;
  rasterDesc.SlopeScaledDepthBias = 0.0f;
}
void dx::DxResourceManager::defaults(dx::SamplerStateDescription &samplerDesc)
{
  samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
  samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.MipLODBias = 0.0f;
  samplerDesc.MaxAnisotropy = 1;
  samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
  samplerDesc.BorderColor[0] = 0;
  samplerDesc.BorderColor[1] = 0;
  samplerDesc.BorderColor[2] = 0;
  samplerDesc.BorderColor[3] = 0;
  samplerDesc.MinLOD = 0;
  samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
}
std::wstring dx::DxResourceManager::getFileExtension(const std::wstring& file)
{
  if (file.find_last_of(L".") != std::wstring::npos)
    return file.substr(file.find_last_of(L".") + 1);
  return L"";
}
std::string dx::DxResourceManager::getFileExtension(const std::string& file)
{
  if (file.find_last_of(".") != std::string::npos)
    return file.substr(file.find_last_of(".") + 1);
  return "";
}
bool dx::DxResourceManager::isDDSFile(const std::wstring& file)
{
  std::wstring extension = getFileExtension(file);
  return std::wstring(L"DDS") == extension || std::wstring(L"dds") == extension;
}
bool dx::DxResourceManager::isDDSFile(const std::string& file)
{
  std::string extension = getFileExtension(file);
  return std::string("DDS") == extension || std::string("dds") == extension;
}

dx::HResult dx::DxResourceManager::createInputLayout(
  _In_ dx::IDirect3DDevice *pDevice,
  _In_reads_bytes_(uBytecodeSize) ::BYTE const *pBytecode,
  _In_ unsigned uBytecodeSize,
  _In_reads_opt_(uLayoutDescNumElements) dx::VSInputElementDescription const pLayoutDescElements[],
  _In_ unsigned uLayoutDescNumElements,
  _Outptr_opt_result_nullonfailure_ dx::IVSLayout **ppLayout,
  _In_opt_ std::wstring wszDebugName
  )
{
  dx::HResult result = S_OK;

  if (!pDevice) return E_POINTER;

  if (!pLayoutDescElements)
  {
    // If no input layout is specified, use the BasicVertex layout.
    const VSInputElementDescription layoutDesc[] =
    {
      { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
      { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
      { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };

    result = pDevice->CreateInputLayout(
      layoutDesc, ARRAYSIZE(layoutDesc),
      pBytecode, uBytecodeSize,
      ppLayout
      );
  }
  else
  {
    result = pDevice->CreateInputLayout(
      pLayoutDescElements,
      uLayoutDescNumElements,
      pBytecode,
      uBytecodeSize,
      ppLayout
      );
  }

  if (FAILED(result)) return result;

  if (wszDebugName.size())
  {
    result = setDebugName(*ppLayout, wszDebugName);
    if (FAILED(result)) return result;
  }

  return result;
}

dx::HResult dx::DxResourceManager::loadShader(
  _In_ IDirect3DDevice *pDevice,
  _In_ std::wstring wszFileName,
  _In_reads_opt_(uLayoutDescNumElements) const dx::VSInputElementDescription pLayoutDescElements[],
  _In_ unsigned uLayoutDescNumElements,
  _Outptr_result_nullonfailure_ dx::IVShader **ppShader,
  _Outptr_opt_result_nullonfailure_ dx::IVSLayout **ppLayout
  )
{
  struct AssetHook : DxAssetHook
  {
  public:
    dx::HResult result;
    dx::VSInputElementDescription const *elements;
    IDirect3DDevice *device;
    dx::IVSLayout **layout;
    dx::IVShader **shader;
    std::wstring assetName;
    unsigned elementCount;

    virtual void onAssetLoaded(unsigned assetId, DxAssetBuffer const &buffer)
    {
      if (device && buffer.dataLength)
      {
        result = device->CreateVertexShader(
          buffer.data, buffer.dataLength,
          nullptr, shader
          );

        if (layout && SUCCEEDED(result))
        {
          result = createInputLayout(
            device, buffer.data, buffer.dataLength,
            elements, elementCount,
            layout, assetName
            );
        }
      }
    }
  };

  AssetHook assetCallback;
  assetCallback.result = E_FAIL;
  assetCallback.device = pDevice;
  assetCallback.shader = ppShader;
  assetCallback.layout = ppLayout;
  assetCallback.elements = pLayoutDescElements;
  assetCallback.elementCount = uLayoutDescNumElements;
  assetCallback.assetName = wszFileName;
  DxAssetManager::loadAsset(wszFileName, 0, &assetCallback);
  assetCallback.assetName = wszFileName;
  if (FAILED(assetCallback.result))
  {
    std::string assetFile;
    dx::DxConverter::string16To8(assetFile, wszFileName);
    dx::trace("error: failed to load vshader \"%s\"", assetFile.c_str());
  }
  else
  {
    assetCallback.result = setDebugName(*ppShader, wszFileName);
  }

  return assetCallback.result;
}

dx::HResult dx::DxResourceManager::loadShader(
  _In_ IDirect3DDevice *pDevice,
  _In_ std::wstring wszFileName,
  _In_reads_opt_(uNumEntries) const dx::GSSODeclarationEntry streamOutDeclaration[],
  _In_ unsigned uNumEntries,
  _In_reads_opt_(uNumStrides) const unsigned pBufferStrides[],
  _In_ unsigned uNumStrides,
  _In_ unsigned uRasterizedStream,
  _Outptr_result_nullonfailure_ dx::IGShader **ppShader
  )
{
  struct AssetHook : DxAssetHook
  {
  public:
    dx::HResult result;
    dx::GSSODeclarationEntry const *entries;
    unsigned const *bufferStrides;
    unsigned entryCount;
    unsigned strideCount;
    unsigned rasterizedStream;
    IDirect3DDevice *device;
    dx::IGShader **shader;
    virtual void onAssetLoaded(unsigned assetId, DxAssetBuffer const &buffer)
    {
      if (device && buffer.dataLength)
      {
        result = device->CreateGeometryShaderWithStreamOutput(
          buffer.data, buffer.dataLength, entries, entryCount,
          bufferStrides, strideCount, rasterizedStream, nullptr,
          shader
          );
      }
    }
  };

  AssetHook assetCallback;
  assetCallback.result = E_FAIL;
  assetCallback.device = pDevice;
  assetCallback.shader = ppShader;
  assetCallback.entryCount = uNumEntries;
  assetCallback.strideCount = uNumStrides;
  assetCallback.bufferStrides = pBufferStrides;
  assetCallback.entries = streamOutDeclaration;

  DxAssetManager::loadAsset(wszFileName, 0, &assetCallback);
  if (FAILED(assetCallback.result))
  {
    std::string assetFile;
    dx::DxConverter::string16To8(assetFile, wszFileName);
    dx::trace("error: failed to load gsoshader \"%s\"", assetFile.c_str());
  }
  else
  {
    assetCallback.result = setDebugName(*ppShader, wszFileName);
  }

  return assetCallback.result;
}

dx::HResult dx::DxResourceManager::loadShader(
  _In_ IDirect3DDevice *pDevice,
  _In_ std::wstring wszFileName,
  _Outptr_result_nullonfailure_ dx::IHShader **ppShader
  )
{

  struct AssetHook : DxAssetHook
  {
  public:
    dx::HResult result;
    IDirect3DDevice *device;
    dx::IHShader **shader;
    virtual void onAssetLoaded(unsigned assetId, DxAssetBuffer const &buffer)
    {
      if (device && buffer.dataLength)
      {
        result = device->CreateHullShader(
          buffer.data, buffer.dataLength,
          nullptr, shader
          );
      }
    }
  };

  AssetHook assetCallback;
  assetCallback.result = E_FAIL;
  assetCallback.device = pDevice;
  assetCallback.shader = ppShader;
  DxAssetManager::loadAsset(wszFileName, 0, &assetCallback);
  if (FAILED(assetCallback.result))
  {
    std::string assetFile;
    dx::DxConverter::string16To8(assetFile, wszFileName);
    dx::trace("error: failed to load hshader \"%s\"", assetFile.c_str());
  }
  else
  {
    assetCallback.result = setDebugName(*ppShader, wszFileName);
  }

  return assetCallback.result;
}

dx::HResult dx::DxResourceManager::loadShader(
  _In_ IDirect3DDevice *pDevice,
  _In_ std::wstring wszFileName,
  _Outptr_result_nullonfailure_ dx::IDShader **ppShader
  )
{

  struct AssetHook : DxAssetHook
  {
  public:
    dx::HResult result;
    IDirect3DDevice *device;
    dx::IDShader **shader;
    virtual void onAssetLoaded(unsigned assetId, DxAssetBuffer const &buffer)
    {
      if (device && buffer.dataLength)
      {
        result = device->CreateDomainShader(
          buffer.data, buffer.dataLength,
          nullptr, shader
          );
      }
    }
  };

  AssetHook assetCallback;
  assetCallback.result = E_FAIL;
  assetCallback.device = pDevice;
  assetCallback.shader = ppShader;
  DxAssetManager::loadAsset(wszFileName, 0, &assetCallback);
  if (FAILED(assetCallback.result))
  {
    std::string assetFile;
    dx::DxConverter::string16To8(assetFile, wszFileName);
    dx::trace("error: failed to load dshader \"%s\"", assetFile.c_str());
  }
  else
  {
    assetCallback.result = setDebugName(*ppShader, wszFileName);
  }

  return assetCallback.result;
}

dx::HResult dx::DxResourceManager::loadShader(
  _In_ IDirect3DDevice *pDevice,
  _In_ std::wstring wszFileName,
  _Outptr_result_nullonfailure_ dx::IGShader **ppShader
  )
{

  struct AssetHook : DxAssetHook
  {
  public:
    dx::HResult result;
    IDirect3DDevice *device;
    dx::IGShader **shader;
    virtual void onAssetLoaded(unsigned assetId, DxAssetBuffer const &buffer)
    {
      if (device && buffer.dataLength)
      {
        result = device->CreateGeometryShader(
          buffer.data, buffer.dataLength,
          nullptr, shader
          );
      }
    }
  };

  AssetHook assetCallback;
  assetCallback.result = E_FAIL;
  assetCallback.device = pDevice;
  assetCallback.shader = ppShader;
  DxAssetManager::loadAsset(wszFileName, 0, &assetCallback);
  if (FAILED(assetCallback.result))
  {
    std::string assetFile;
    dx::DxConverter::string16To8(assetFile, wszFileName);
    dx::trace("error: failed to load gshader \"%s\"", assetFile.c_str());
  }
  else
  {
    assetCallback.result = setDebugName(*ppShader, wszFileName);
  }

  return assetCallback.result;
}

dx::HResult dx::DxResourceManager::loadShader(
  _In_ IDirect3DDevice *pDevice,
  _In_ std::wstring wszFileName,
  _Outptr_result_nullonfailure_ dx::IPShader **ppShader
  )
{

  struct AssetHook : DxAssetHook
  {
  public:
    dx::HResult result;
    IDirect3DDevice *device;
    dx::IPShader **shader;
    virtual void onAssetLoaded(unsigned assetId, DxAssetBuffer const &buffer)
    {
      if (device && buffer.dataLength)
      {
        result = device->CreatePixelShader(
          buffer.data, buffer.dataLength,
          nullptr, shader
          );
      }
    }
  };

  AssetHook assetCallback;
  assetCallback.result = E_FAIL;
  assetCallback.device = pDevice;
  assetCallback.shader = ppShader;
  DxAssetManager::loadAsset(wszFileName, 0, &assetCallback);
  if (FAILED(assetCallback.result))
  {
    std::string assetFile;
    dx::DxConverter::string16To8(assetFile, wszFileName);
    dx::trace("error: failed to load pshader \"%s\"", assetFile.c_str());
  }
  else
  {
    assetCallback.result = setDebugName(*ppShader, wszFileName);
  }

  return assetCallback.result;
}

dx::HResult dx::DxResourceManager::loadShader(
  _In_ IDirect3DDevice *pDevice,
  _In_ std::wstring wszFileName,
  _Outptr_result_nullonfailure_ dx::ICShader **ppShader
  )
{

  struct AssetHook : DxAssetHook
  {
  public:
    dx::HResult result;
    IDirect3DDevice *device;
    dx::ICShader **shader;
    virtual void onAssetLoaded(unsigned assetId, DxAssetBuffer const &buffer)
    {
      if (device && buffer.dataLength)
      {
        result = device->CreateComputeShader(
          buffer.data, buffer.dataLength,
          nullptr, shader
          );
      }
    }
  };

  AssetHook assetCallback;
  assetCallback.result = E_FAIL;
  assetCallback.device = pDevice;
  assetCallback.shader = ppShader;
  DxAssetManager::loadAsset(wszFileName, 0, &assetCallback);

  if (FAILED(assetCallback.result))
  {
    std::string assetFile;
    dx::DxConverter::string16To8(assetFile, wszFileName);
    dx::trace("error: failed to load pshader \"%s\"", assetFile.c_str());
  }
  else
  {
    assetCallback.result = setDebugName(*ppShader, wszFileName);
  }

  return assetCallback.result;
}
