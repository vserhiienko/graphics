#include "DxUtils.h"
#include "DxConverter.h"
#include "DxAssetManager.h"

std::atomic_uint dx::DxAssetManager::loadingResourceCount;
std::vector<std::string> dx::DxAssetManager::searchPath;

void dx::DxAssetManager::addSearchPath(std::string const &path)
{
  std::vector<std::string>::iterator src = searchPath.begin();

  while (src != searchPath.end())
  {
    if (!(*src).compare(path)) return;
    src++;
  }

  searchPath.push_back(path);
}

void dx::DxAssetManager::removeSearchPath(std::string const &path)
{
  std::vector<std::string>::iterator src = searchPath.begin();

  while (src != searchPath.end())
  {
    if (!(*src).compare(path))
    {
      searchPath.erase(src);
      return;
    }

    src++;
  }
}


bool dx::DxAssetBuffer::alloc(size_t sz)
{
  if (data) dealloc();
  data = (unsigned char*)malloc(sz + 1);
  dataLength = sz;
  data[dataLength] = '\0';
  return !!data;
}

void dx::DxAssetBuffer::dealloc()
{
  if (data) free((void *)data);
}


void dx::DxAssetManager::loadAsset(std::wstring const &filePath16, unsigned assetId, dx::DxAssetHook *assetHook)
{
  if (filePath16.size())
  {
    std::string filePath;
    DxConverter::string16To8(filePath, filePath16);
    loadAsset(filePath, assetId, assetHook);
  }
}

bool dx::DxAssetManager::directoryExists(std::string const &szPath)
{
  DWORD dwAttrib = GetFileAttributesA(szPath.c_str());
  return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
         (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

bool dx::DxAssetManager::fileExists(std::string const &szPath)
{
  DWORD dwAttrib = GetFileAttributesA(szPath.c_str());
  return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
        !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

std::string dx::DxAssetManager::findRelativePathFor(_In_ std::string const &filePath)
{
  static const int32_t s_upStepCount = 10;

  std::string upPath;
  std::string fullPath;

  // loop N times up the hierarchy, testing at each level

  for (int32_t i = 0; i < s_upStepCount; i++)
  {
    std::vector<std::string>::iterator src = searchPath.begin();
    bool looping = true;

    while (looping)
    {
      fullPath.assign(upPath);  // reset to current upPath.
      if (src != searchPath.end())
      {
        fullPath.append(*src);
        fullPath.append("/assets/");
        src++;
      }
      else 
      {
        fullPath.append("assets/");
        looping = false;
      }

      fullPath.append(filePath);
      if (fileExists(fullPath)) looping = false;
    }

    if (!looping) return fullPath;
    else upPath.append("../");
  }

  return fullPath;
}

void dx::DxAssetManager::loadAsset(std::string const &filePath, unsigned assetId, dx::DxAssetHook *assetHook)
{
  if (assetHook)
  {
    FILE *fp = NULL;

    // loop N times up the hierarchy, testing at each level
    std::string upPath;
    std::string fullPath;

    for (int32_t i = 0; i < 10; i++)
    {
      std::vector<std::string>::iterator src = searchPath.begin();
      bool looping = true;

      while (looping)
      {
        fullPath.assign(upPath);  // reset to current upPath.
        if (src != searchPath.end())
        {
          //sprintf_s(fullPath, "%s%s/assets/%s", upPath, *src, filePath);
          fullPath.append(*src);
          fullPath.append("/assets/");
          src++;
        }
        else {
          //sprintf_s(fullPath, "%sassets/%s", upPath, filePath);
          fullPath.append("assets/");
          looping = false;
        }
        fullPath.append(filePath);

#ifdef DEBUG
        fprintf(stderr, "Trying to open %s\n", fullPath.c_str());
#endif
        if ((fopen_s(&fp, fullPath.c_str(), "rb") != 0) || (fp == NULL)) fp = NULL;
        else looping = false;
      }

      if (fp) break;

      upPath.append("../");
    }

    if (fp)
    {
      long length;

      fseek(fp, 0, SEEK_END);
      length = ftell(fp);
      fseek(fp, 0, SEEK_SET);

      dx::DxAssetBuffer buffer;

      if (buffer.alloc(length))
      {
        fread(buffer.data, 1, length, fp);
      }

      fclose(fp);

#ifdef _DEBUG
      dx::trace("dx:asset: %s (%s) [%d, 0x%0x] [%.1fKb]", filePath.c_str(), fullPath.c_str(), assetId, assetHook, float(buffer.dataLength) / 1024.f);
#endif

      assetHook->onAssetLoaded(assetId, buffer);
      buffer.dealloc();
      return;
    }
  }

  assetHook->onAssetNotFound(assetId);
}

void dx::DxAssetManager::loadAssetAsync(std::wstring filePath16, unsigned assetId, dx::DxAssetHook *assetHook)
{
  if (filePath16.size())
  {
    std::string filePath;
    DxConverter::string16To8(filePath, filePath16);
    loadAssetAsync(filePath, assetId, assetHook);
  }
}

void dx::DxAssetManager::loadAssetAsync(std::string assetName, unsigned assetId, dx::DxAssetHook *assetHook)
{
  (concurrency::create_task([=]()
  {
    loadingResourceCount++;
    loadAsset(assetName, assetId, assetHook);
    loadingResourceCount--;
    return;
  }));
}

