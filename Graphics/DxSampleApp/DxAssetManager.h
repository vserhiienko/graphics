#pragma once
#include <Windows.h>
#include <string>
#include <vector>
#include <concurrent_queue.h>
#include <atomic>
#include <ppl.h>
#include <ppltasks.h>

namespace dx
{
  class DxAssetBuffer;
  class DxAssetManager;
  class DxAssetHook;

  class DxAssetBuffer
  {
  public:
    unsigned char *data;
    size_t dataLength;

  public:
    DxAssetBuffer(void) : data(0), dataLength(0) { }
    bool alloc(_In_ size_t);
    void dealloc(void);
  };

  class DxAssetHook
  {
  public:
    virtual ~DxAssetHook()
    {
    }

    virtual void onAssetLoaded(_In_ unsigned assetId, _In_ DxAssetBuffer const &asset) = 0;
    virtual void onAssetNotFound(_In_ unsigned assetId) { }
  };

  class DxAssetManager
  {
  public:
    static std::atomic_uint loadingResourceCount;
    static std::vector<std::string> searchPath;

    static void addSearchPath(_In_ std::string const &path);
    static void removeSearchPath(_In_ std::string const &path);
    static std::string findRelativePathFor(_In_ std::string const &partialPath);
    static bool directoryExists(std::string const &path);
    static bool fileExists(std::string const &path);
    static void loadAsset(_In_ std::string const &assetName, _In_ unsigned assetId, _In_ DxAssetHook *assetHook);
    static void loadAsset(_In_ std::wstring const &assetName, _In_ unsigned assetId, _In_ DxAssetHook *assetHook);
    static void loadAssetAsync(_In_ std::string assetName, _In_ unsigned assetId, _In_ DxAssetHook *assetHook);
    static void loadAssetAsync(_In_ std::wstring assetName, _In_ unsigned assetId, _In_ DxAssetHook *assetHook);

  };
}