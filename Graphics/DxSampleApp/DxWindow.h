#pragma once

// win
#include <vector>
#include <string>
#include <sstream>
#include <DirectXMath.h>

// dx
#include <DxNeon.h>
#include <DxVirtualKeys.h>

namespace dx
{
  struct DxGenericEventArgs
  {
    dx::neon::n128 data;
    DxGenericEventArgs() {}
    inline float &mouseX(void) { return data.f32[0]; }
    inline float mouseX(void) const { return data.f32[0]; }
    inline float &mouseY(void) { return data.f32[1]; }
    inline float mouseY(void) const { return data.f32[1]; }
    inline bool mouseShift(void) const { return data.u8[8] != 0ui8; }
    inline bool mouseControl(void) const { return data.u8[9] != 0ui8; }
    inline int &hwheel(void) { return data.i32[2]; }
    inline int hwheel(void) const { return data.i32[2]; }
    inline int &wheelDelta(void) { return data.i32[1]; }
    inline int wheelDelta(void) const { return data.i32[1]; }
    inline unsigned &wheelKeystate(void) { return data.u32[0]; }
    inline unsigned wheelKeystate(void) const { return data.u32[0]; }
    inline unsigned &keyCode(void) { return data.u32[0]; }
    inline unsigned keyCode(void) const { return data.u32[0]; }
    inline unsigned &keyPrevState(void) { return data.u32[1]; }
    inline unsigned keyPrevState(void) const { return data.u32[1]; }
    inline unsigned &keyRepeatCount(void) { return data.u32[2]; }
    inline unsigned keyRepeatCount(void) const{ return data.u32[2]; }
    inline unsigned &width(void) { return data.u32[0]; }
    inline unsigned width(void) const { return data.u32[0]; }
    inline unsigned &height(void) { return data.u32[1]; }
    inline unsigned height(void) const { return data.u32[1]; }
    inline unsigned &message(void) { return data.u32[3]; }
    inline unsigned message(void) const { return data.u32[3]; }
  };

  class DxWindow;
  class DxWindowHooks
  {
  public:
    void *userData;
    inline DxWindowHooks() : userData(0) {}
    inline virtual void handlePaint(DxWindow const*) {}
    inline virtual void handleClosed(DxWindow const*) {}
    inline virtual void handleSizeChanged(DxWindow const*) {}
    inline virtual void handleKeyPressed(DxWindow const*, DxGenericEventArgs const&) {}
    inline virtual void handleKeyReleased(DxWindow const*, DxGenericEventArgs const&) {}
    inline virtual void handleMouseWheel(DxWindow const*, DxGenericEventArgs const&) {}
    inline virtual void handleMouseMoved(DxWindow const*, DxGenericEventArgs const&) {}
    inline virtual void handleMouseLeftPressed(DxWindow const*, DxGenericEventArgs const&) {}
    inline virtual void handleMouseLeftReleased(DxWindow const*, DxGenericEventArgs const&) {}
    inline virtual void handleMouseRightPressed(DxWindow const*, DxGenericEventArgs const&) {}
    inline virtual void handleMouseRightReleased(DxWindow const*, DxGenericEventArgs const&) {}
    inline virtual void handleMouseMiddlePressed(DxWindow const*, DxGenericEventArgs const&) {}
    inline virtual void handleMouseMiddleReleased(DxWindow const*, DxGenericEventArgs const&) {}
  };

  enum DxWindowState
  {
    eDxWindowState_MaxHide = SIZE_MAXHIDE,
    eDxWindowState_MaxShow = SIZE_MAXSHOW,
    eDxWindowState_Minimized = SIZE_MINIMIZED,
    eDxWindowState_Maximized = SIZE_MAXIMIZED,
    eDxWindowState_Restored = SIZE_RESTORED,
  };

  class DxMonitorTraits
  {
  public:
    typedef std::vector<DxMonitorTraits*> Collection;

  public:
    int index;
    int area;
    bool primary;

    RECT deviceWorkRect;
    HMONITOR deviceHandle;
    std::string deviceName;

    inline int x() const { return deviceWorkRect.left; }
    inline int y() const { return deviceWorkRect.top; }
    inline int width() const { return deviceWorkRect.right - x(); }
    inline int height() const { return deviceWorkRect.bottom - y(); }

  public:
    DxMonitorTraits();
  };

  class DxWindowTraits
  {
  public:
    HWND handle;
    DxWindowState state;
    DirectX::XMINT2 pos;
    DirectX::XMINT2 dimensions;
    DirectX::XMINT2 screenDimensions;

  public:
    DxWindowTraits();
  };

  class DxWindow : public DxWindowTraits
  {
    DxWindowHooks *m_hooks;
    DxMonitorTraits *m_currentMonitor;
    DxMonitorTraits *m_largestMonitor;
    DxMonitorTraits *m_primaryMonitor;
    DxMonitorTraits::Collection m_monitors;

  public:
    DxWindow();

    void create();
    void update();
    void close();

    void setWindowTitle(std::string const &);

  public:
    inline DxWindowHooks *&hooks() { return m_hooks; }
    inline DxWindowTraits &traits() { return (*this); }

  public:
    inline DxWindowHooks const *&hooks() const { return hooks(); }
    inline DxWindowTraits const &traits() const { return (*this); }

  private:
    friend class DxWindowProc;
    void onClosed();
    void updateMonitors();
    void updateWindowSpatialInfo();

  };
}