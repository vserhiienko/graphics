#pragma once
#include <DxDelegates.h>
#include <DxRenderTargetBase.h>

namespace dx
{
  // Constants used to calculate screen rotations.
  class ScreenRotation
  {
  public:
    // 0-degree Z-rotation.
    static const DirectX::XMFLOAT4X4 rotation0;
    // 90-degree Z-rotation.
    static const DirectX::XMFLOAT4X4 rotation90;
    // 180-degree Z-rotation.
    static const DirectX::XMFLOAT4X4 rotation180;
    // 270-degree Z-rotation.
    static const DirectX::XMFLOAT4X4 rotation270;
  };

  // The purpose is to notify the host application of the device being lost or created.
  //! @deprecated consider using events instead of this interface
  class IDxDeviceNotify
  {
  public:
    virtual void onDeviceLost(void) = 0;
    virtual void onDeviceRestored(void) = 0;
  };

  // Constrols all the DirectX resources
  class DxDeviceResources : public dx::DxRenderTargetBase
  {

  public:
    typedef dx::Event<void, DxDeviceResources *> Event;

  public:
    // Constructor for DxDeviceResources.
    DxDeviceResources(
      _In_opt_ D3D_DRIVER_TYPE driver = D3D_DRIVER_TYPE_HARDWARE,
      _In_opt_ DXGI_USAGE swapchainBuffersUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT, // ? SHARER_RESOURCES ? UNORDERED_ACCESS
      _In_opt_ DXGI_FORMAT swapchainBuffersFormat = DXGI_FORMAT_B8G8R8A8_UNORM, // ? DXGI_FORMAT_R16G16B16A16_FLOAT
      _In_opt_ DXGI_SWAP_EFFECT swapchainEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL, // ? DXGI_SWAP_EFFECT_DISCARD
      _In_opt_ DXGI_ALPHA_MODE swapchainAlphaMode = DXGI_ALPHA_MODE_IGNORE,
      _In_opt_ unsigned swapchainBufferCount = 2,
      _In_opt_ bool supportD2D = TRUE,
      _In_opt_ bool allowFullscreen = TRUE,
      _In_opt_ bool createDeviceResources = TRUE
      );
    // Destructor for DxDeviceResources.
    ~DxDeviceResources(void);

  public:
    // This method is called when the CoreWindow is created (or re-created).
    dx::HResult setWindow(_In_ dx::WindowHandle window);
    // Register our DeviceNotify to be informed on device lost and creation.
    void registerDeviceNotify(_In_ dx::IDxDeviceNotify* deviceNotify);
    // This method is called in the event handler for the SizeChanged event.
    dx::HResult setLogicalSize(_In_ dx::Size size);
    // When the display DPI changes, the logical size of the window (measured in Dips) also changes and needs to be updated.
    dx::HResult setDpi(_In_ float dpix, _In_ float dpiy);
    // Recreate all device resources and set them back to the current state.
    dx::HResult handleDeviceLost(void);
    // This method is called in the event handler for the OrientationChanged event.
    dx::HResult setCurrentOrientation(_In_ dx::DisplayOrientation currentOrientation);
    // Call this method when the app suspends. 
    // It provides a hint to the driver that the app is entering an idle state and that temporary buffers can be reclaimed for use by other apps.
    dx::HResult trim(void);
    // This method is called in the event handler for the DisplayContentsInvalidated event.
    dx::HResult validateDevice(void);
    // These resources need to be recreated every time the window size is changed.
    dx::HResult createWindowSizeDependentResources(void);
    // Configures resources that don't depend on the Direct3D device.
    dx::HResult createDeviceIndependentResources(void);
    // Configures the Direct3D device, and stores handles to it and the device context.
    dx::HResult createDeviceResources(void);
    // Present the contents of the swap chain to the screen.
    dx::HResult present(void);
    // Check for SDK Layer support.
    bool sdkLayersAvailable(void);
    // This method determines the rotation between the display device's native Orientation and the current display orientation.
    dx::DxgiModeRotation computeDisplayRotation(void);
    bool updateLogicalSizeAndCurrentOrientation(void);
    bool updateDpi(void);
    void cleanup(void);

  public:
    // Converts a length in device-independent pixels (DIPs) to a length in physical pixels.
    static float dips2Pels(_In_ float dips, _In_ float dpi);

  public:
    // Determines device-independent pixels (DIPs) count per 1 inch.
    static const float dipsPerInch;

  public:
    DxDeviceResources::Event deviceLost;			  // Release device resources
    DxDeviceResources::Event deviceRestored;		// Create device resources
    DxDeviceResources::Event swapchainResizing;	// Release window size dependent resources
    DxDeviceResources::Event swapchainResized;	// Create window size dependent resources

    // Direct3D objects.
    dx::Context3 context;
    dx::Device3 device;
    dx::Swapchain swapchain;

    dx::DriverType driver;
    dx::WindowHandle host;
    dx::IDxDeviceNotify *deviceNotify;

    dx::FeatureLevel features;
    dx::Size renderTargetSize;
    dx::Size actualRenderTargetSize;
    dx::Size logicalSize;
    dx::Size outputSize;

    DXGI_SWAP_CHAIN_FLAG swapchainFlags;
    DXGI_ALPHA_MODE swapchainAlphaMode;
    DXGI_SWAP_EFFECT swapchainEffect;
    DXGI_FORMAT renderTargetFormat;
    DXGI_USAGE renderTargetUsage;
    unsigned swapchainBufferCount;
    DirectX::XMFLOAT2 dpi;
    float logicalDpi;

    bool d2dSupport;
    bool windowed;
    bool vsync;
    bool valid;

    DirectX::XMMATRIX orientationXform3D;
    dx::DisplayOrientation nativeOrientation;
    dx::DisplayOrientation currentOrientation;

  };
}

