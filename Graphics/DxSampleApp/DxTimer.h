#pragma once

#include <Windows.h>

namespace dx
{
  namespace sim
  {
    class BasicTimer;
  }
}

class dx::sim::BasicTimer
{
  typedef void (BasicTimer::*UpdateDelegate)();
  UpdateDelegate updateCallback;
  unsigned __int64 ticksPerSecond64;
  unsigned __int64 oneSecondTicks64;
  unsigned __int64 startupTicks64;
  unsigned __int64 currentTicks64;
  unsigned __int64 lastTicks64;

  bool usefixedDelta;
  float fixedDelta;
  float low;

public:
  int framesPerSecond;
  float totalElapsed;
  int frameCount;
  float elapsed;

  BasicTimer() :
    ticksPerSecond64(0ui64),
    oneSecondTicks64(0ui64),
    usefixedDelta(false),
    currentTicks64(0ui64),
    startupTicks64(0ui64),
    framesPerSecond(0),
    lastTicks64(0ui64),
    fixedDelta(0.0f),
    frameCount(0),
    elapsed(0.0f)
  {
    QueryPerformanceFrequency((LARGE_INTEGER*) &ticksPerSecond64);
    QueryPerformanceCounter((LARGE_INTEGER*) &currentTicks64);
    oneSecondTicks64 = currentTicks64;
    startupTicks64 = currentTicks64;
    setFixedTimeStep(0.0f);
  }

  __forceinline void updateNonfixedStepImpl()
  {
    auto const high_elapsed = (float)(currentTicks64 - startupTicks64);
    auto const high_delta = (float)(currentTicks64 - lastTicks64);
    totalElapsed = (high_elapsed / low);
    elapsed = (high_delta / low);
  }

  __forceinline void updateFixedStepImpl()
  {
    elapsed = fixedDelta;
    totalElapsed += elapsed;
  }

  __forceinline void update()
  {
    low = (float)ticksPerSecond64;
    lastTicks64 = currentTicks64;

    QueryPerformanceCounter((LARGE_INTEGER*) &currentTicks64);
    (this->*updateCallback)();

    auto const high_sec = (float)(currentTicks64 - oneSecondTicks64);
    auto const value_sec = (high_sec / low);

    if (value_sec < 1.0f) frameCount++; else
    {
      oneSecondTicks64 = currentTicks64;
      framesPerSecond = frameCount;
      frameCount = 0;
    }
  }

  void setFixedTimeStep(float step)
  {
    if (step <= 0.0f)
    {
      fixedDelta = 0.0f;
      usefixedDelta = false;
      updateCallback = &BasicTimer::updateNonfixedStepImpl;
    }
    else
    {
      usefixedDelta = true; fixedDelta = step;
      updateCallback = &BasicTimer::updateFixedStepImpl;
    }
  }

  void reset()
  {
    framesPerSecond = 0;
    frameCount = 0;
    elapsed = 0.0f;
  }
};


