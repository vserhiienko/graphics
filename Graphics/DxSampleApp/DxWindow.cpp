#include "DxWindow.h"
#include "DxUtils.h"
#include <Windowsx.h>
#include <algorithm>

class dx::DxWindowProc
{
public:
  template <uint32_t _EventId> static DxGenericEventArgs create(WPARAM wparam, LPARAM lparam);
  template <> static DxGenericEventArgs create<dx::Message::KeyReleased>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.keyCode() = (unsigned)wparam;
    args.message() = dx::Message::KeyReleased;
    dx::CorePhysicalKeyStatus::ExtractSlim(lparam, args.keyPrevState(), args.keyRepeatCount());
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::KeyPressed>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.keyCode() = (uint32_t)wparam;
    args.message() = dx::Message::KeyPressed;
    dx::CorePhysicalKeyStatus::ExtractSlim(lparam, args.keyPrevState(), args.keyRepeatCount());
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::LeftButtonReleased>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = dx::Message::LeftButtonReleased;
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::LeftButtonPressed>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = dx::Message::LeftButtonPressed;
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::RightButtonReleased>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = dx::Message::RightButtonReleased;
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::RightButtonPressed>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = dx::Message::RightButtonPressed;
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::MiddleButtonReleased>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = dx::Message::MiddleButtonReleased;
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::MiddleButtonPressed>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.data.u8[8] = (wparam & 0x0008) != 0 ? 1ui8 : 0ui8;
    args.data.u8[9] = (wparam & 0x0004) != 0 ? 1ui8 : 0ui8;
    args.message() = dx::Message::MiddleButtonPressed;
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::MouseMove>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.mouseX() = (float)GET_X_LPARAM(lparam);
    args.mouseY() = (float)GET_Y_LPARAM(lparam);
    args.message() = dx::Message::MouseMove;
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::MouseHWheel>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.wheelKeystate() = GET_KEYSTATE_WPARAM(wparam);
    args.wheelDelta() = GET_WHEEL_DELTA_WPARAM(wparam);
    args.message() = dx::Message::MouseHWheel;
    args.hwheel() = 1;
    return args;
  }
  template <> static DxGenericEventArgs create<dx::Message::MouseWheel>(WPARAM wparam, LPARAM lparam)
  {
    DxGenericEventArgs args; args.data = { 0 };
    args.wheelKeystate() = GET_KEYSTATE_WPARAM(wparam);
    args.wheelDelta() = GET_WHEEL_DELTA_WPARAM(wparam);
    args.message() = dx::Message::MouseWheel;
    args.hwheel() = 0;
    return args;
  }

  static LRESULT CALLBACK internalProcess(dx::DxWindow &window, HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
  {
    if (window.handle == hwnd && window.m_hooks) switch (msg)
    {
    case dx::Message::Paint:                      /*dx::trace("dx: paint"); window.m_hooks->handlePaint(&window);*/ return NULL;
    case dx::Message::MouseMove:                  window.m_hooks->handleMouseMoved(&window, create<dx::Message::MouseMove>(wparam, lparam)); return NULL;
    case dx::Message::LeftButtonPressed:          window.m_hooks->handleMouseLeftPressed(&window, create<dx::Message::LeftButtonPressed>(wparam, lparam)); return NULL;
    case dx::Message::LeftButtonReleased:         window.m_hooks->handleMouseLeftReleased(&window, create<dx::Message::LeftButtonReleased>(wparam, lparam)); return NULL;
    case dx::Message::KeyPressed:                 window.m_hooks->handleKeyPressed(&window, create<dx::Message::KeyPressed>(wparam, lparam)); return NULL;
    case dx::Message::KeyReleased:                window.m_hooks->handleKeyReleased(&window, create<dx::Message::KeyReleased>(wparam, lparam)); return NULL;
    case dx::Message::RightButtonPressed:         window.m_hooks->handleMouseRightPressed(&window, create<dx::Message::LeftButtonPressed>(wparam, lparam)); return NULL;
    case dx::Message::RightButtonReleased:        window.m_hooks->handleMouseRightReleased(&window, create<dx::Message::LeftButtonPressed>(wparam, lparam)); return NULL;
    case dx::Message::MouseHWheel:                window.m_hooks->handleMouseWheel(&window, create<dx::Message::MouseWheel>(wparam, lparam)); return NULL;
    case dx::Message::MouseWheel:                 window.m_hooks->handleMouseWheel(&window, create<dx::Message::MouseWheel>(wparam, lparam)); return NULL;
    case dx::Message::Size:                       window.state = (DxWindowState)wparam; return DefWindowProc(hwnd, msg, wparam, lparam);
    case dx::Message::ExitSizeMove:               window.m_hooks->handleSizeChanged(&window); return DefWindowProc(hwnd, msg, wparam, lparam);
    case dx::Message::Destroy:                    window.onClosed(); return DefWindowProc(hwnd, msg, wparam, lparam);
    }

    return DefWindowProc(hwnd, msg, wparam, lparam);
  }
};

static LRESULT CALLBACK internalProcess(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  LONG_PTR impl = GetWindowLongPtrA(hwnd, 0);
  if (impl == NULL) return DefWindowProc(hwnd, msg, wparam, lparam);
  else return dx::DxWindowProc::internalProcess(*(dx::DxWindow*)impl, hwnd, msg, wparam, lparam);
}

static BOOL WINAPI enumMonitorsProc(HMONITOR handle, HDC context, LPRECT rect, LPARAM data)
{
  MONITORINFOEXA info;
  dx::DxMonitorTraits::Collection *monitors;

  info.cbSize = sizeof(info);
  monitors = (dx::DxMonitorTraits::Collection *)data;
  if (monitors && GetMonitorInfoA(handle, &info))
  {
    monitors->push_back(new dx::DxMonitorTraits());

    monitors->back()->deviceHandle = handle;
    monitors->back()->deviceName = info.szDevice;
    monitors->back()->deviceWorkRect = info.rcWork;
    monitors->back()->primary = info.dwFlags == MONITORINFOF_PRIMARY;

    monitors->back()->index = monitors->size() - 1;
    monitors->back()->area = monitors->back()->width() * monitors->back()->height();
  }

  return true;
}

dx::DxMonitorTraits::DxMonitorTraits()
{
  deviceHandle = 0;
  dx::zeroMemory(deviceWorkRect);
}

dx::DxWindowTraits::DxWindowTraits()
{
  dx::zeroMemory((*this));
  state = eDxWindowState_Minimized;
}

dx::DxWindow::DxWindow()
{
  hooks() = 0;
  updateMonitors();
  updateWindowSpatialInfo();
}

void dx::DxWindow::onClosed()
{
  if (!handle) return;

  UnregisterClass("DxSampleAppWindow", GetModuleHandle(NULL));
  ShowCursor(true);
  handle = nullptr;
  m_hooks->handleClosed(this);

  PostQuitMessage(0);
}

void dx::DxWindow::setWindowTitle(std::string const &title)
{
  if (!handle) return;
  SetWindowTextA(handle, title.c_str());
}

void dx::DxWindow::create()
{
  static const unsigned long style
    = WS_CLIPSIBLINGS
    | WS_CLIPCHILDREN
    | WS_VISIBLE
    | WS_POPUP;

  WNDCLASSEX wc;
  unsigned short atom;

  dx::zeroMemory(wc);
  wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
  wc.hCursor = LoadCursor(NULL, IDC_ARROW);
  wc.lpszClassName = "DxSampleAppWindow";
  wc.lpszMenuName = NULL;
  wc.lpfnWndProc = &internalProcess;
  wc.hIconSm = wc.hIcon;
  wc.hInstance = GetModuleHandleA(NULL);
  wc.cbWndExtra = sizeof(this);
  wc.cbSize = sizeof wc;
  wc.cbClsExtra = 0;

  atom = RegisterClassEx(&wc);
  if (atom == INVALID_ATOM)
  {
    auto error = GetLastError();
    return;
  }

  handle = CreateWindowEx(
    NULL, "DxSampleAppWindow", "DxSampleApp", style,
    pos.x, pos.y, dimensions.x, dimensions.y,
    NULL, NULL, GetModuleHandleA(NULL), NULL
    );

  if (handle)
  {
    SetWindowLongPtr(handle, 0, (LONG_PTR)this);
    ShowWindow(handle, SW_SHOW);
    SetFocus(handle);
    UpdateWindow(handle);
  }
}

void dx::DxWindow::update()
{
  if (handle) SetWindowPos(handle, NULL, pos.x, pos.y, dimensions.x, dimensions.y, SWP_SHOWWINDOW), UpdateWindow(handle);
}

void dx::DxWindow::close()
{
  if (handle) SendMessage(handle, WM_CLOSE, NULL, NULL), onClosed();
}

void dx::DxWindow::updateMonitors()
{
  EnumDisplayMonitors(NULL, NULL, enumMonitorsProc, (LPARAM)&m_monitors);
  std::sort(m_monitors.begin(), m_monitors.end(), [](dx::DxMonitorTraits *m1, dx::DxMonitorTraits *m2) { return m1->area > m2->area; });
  m_primaryMonitor = *std::find_if(m_monitors.begin(), m_monitors.end(), [](dx::DxMonitorTraits *m) { return m->primary; });
  m_largestMonitor = m_monitors.front();
  m_currentMonitor = m_largestMonitor;
}

void dx::DxWindow::updateWindowSpatialInfo()
{
  screenDimensions.x = m_currentMonitor->width();
  screenDimensions.y = m_currentMonitor->height();
  int desiredW = screenDimensions.x * 4 / 5;
  int desiredH = screenDimensions.y * 4 / 5;
  dimensions.x = dx::roundUp(desiredW, 16);
  dimensions.y = dx::roundUp(desiredH, 16);
  pos.x = m_currentMonitor->x() + (screenDimensions.x - dimensions.x) / 2;
  pos.y = m_currentMonitor->y() + (screenDimensions.y - dimensions.y) / 2;
}

#pragma region Win32 API

// GetMonitorInfo function
/*
GetMonitorInfo function
The GetMonitorInfo function retrieves information about a display monitor.
Syntax

bool GetMonitorInfo(
_In_   HMONITOR hMonitor,
_Out_  LPMONITORINFO lpmi
);

Parameters

hMonitor [in]
A handle to the display monitor of interest.
lpmi [out]
A pointer to a MONITORINFO or MONITORINFOEX structure that receives information about the specified display monitor.
You must set the cbSize member of the structure to sizeof(MONITORINFO) or sizeof(MONITORINFOEX) before calling the GetMonitorInfo function. Doing so lets the function determine the type of structure you are passing to it.
The MONITORINFOEX structure is a superset of the MONITORINFO structure. It has one additional member: a string that contains a name for the display monitor. Most applications have no use for a display monitor name, and so can save some bytes by using a MONITORINFO structure.
Return value

If the function succeeds, the return value is nonzero.
If the function fails, the return value is zero.
*/

// EnumDisplayMonitors function
/*
EnumDisplayMonitors function
The EnumDisplayMonitors function enumerates display monitors (including invisible pseudo-monitors associated with the mirroring drivers) that intersect a region formed by the intersection of a specified clipping rectangle and the visible region of a device context. EnumDisplayMonitors calls an application-defined MonitorEnumProc callback function once for each monitor that is enumerated. Note that GetSystemMetrics (SM_CMONITORS) counts only the display monitors.
Syntax

C++

bool EnumDisplayMonitors(
_In_  HDC hdc,
_In_  LPCRECT lprcClip,
_In_  MONITORENUMPROC lpfnEnum,
_In_  LPARAM dwData
);

Parameters

hdc [in]
A handle to a display device context that defines the visible region of interest.
If this parameter is NULL, the hdcMonitor parameter passed to the callback function will be NULL, and the visible region of interest is the virtual screen that encompasses all the displays on the desktop.
lprcClip [in]
A pointer to a RECT structure that specifies a clipping rectangle. The region of interest is the intersection of the clipping rectangle with the visible region specified by hdc.
If hdc is non-NULL, the coordinates of the clipping rectangle are relative to the origin of the hdc. If hdc is NULL, the coordinates are virtual-screen coordinates.
This parameter can be NULL if you don't want to clip the region specified by hdc.
lpfnEnum [in]
A pointer to a MonitorEnumProc application-defined callback function.
dwData [in]
Application-defined data that EnumDisplayMonitors passes directly to the MonitorEnumProc function.
Return value

If the function succeeds, the return value is nonzero.
If the function fails, the return value is zero.
*/

#pragma endregion