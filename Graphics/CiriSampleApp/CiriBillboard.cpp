#include <CiriBillboard.h>
#include <CiriResourceHelper.h>
using namespace ciri;

void BillboardDescriptor::makeLargeBillboard(BillboardDescriptor &desc)
{
    desc.upY = true;
    desc.resX = 1000;
    desc.resY = 1000;
    desc.lenX = 500;
    desc.lenY = 500;
}

void BillboardResources::updateConstanBufferSources()
{
    perFrameBuffer.setUploadHeapPointer();
}

void BillboardResources::pushConstantBuffers(
    ConstantBufferBase::Vector &collection
    )
{
    collection.push_back(&perFrameBuffer);
}

void BillboardResources::advanceConstantBufferUploadHeap(
    ConstantBufferUploadHeap &contantBufferUploadHeap
    )
{
    contantBufferUploadHeap.advanceUploadHeapSizeForType<PerFrameData>();
}

void BillboardResources::createFor(
    DeviceResources &deviceResources,
    BillboardDescriptor const &descriptor,
    ConstantBufferUploadHeap &contantBufferUploadHeap
    )
{
    auto deltaX = descriptor.lenX / descriptor.resX;
    auto deltaY = descriptor.lenY / descriptor.resY;

    auto x = (descriptor.lenX)* -0.5f;
    auto y = (descriptor.lenY)* -0.5f;

    auto xSize = (uint32_t)(descriptor.lenX / deltaX);
    auto ySize = (uint32_t)(descriptor.lenY / deltaY);

    std::vector<uint16_t> indices;
    std::vector<dx::Vector4> vertices;

    indices.reserve(xSize * 2 + ySize * 2);
    vertices.reserve(xSize * 2 + ySize * 2 + 2);

    for (unsigned i = 0; i < xSize; i++)
    {
        auto vi = (uint16_t)vertices.size();

        if (descriptor.upY)
        {
            vertices.push_back(dx::Vector4(x, 0.0f, +y, 1.0f));
            vertices.push_back(dx::Vector4(x, 0.0f, -y, 1.0f));
        }
        else
        {
            vertices.push_back(dx::Vector4(x, +y, 0.0f, 1.0f));
            vertices.push_back(dx::Vector4(x, -y, 0.0f, 1.0f));
        }

        indices.push_back(vi + 0);
        indices.push_back(vi + 1);
        x += deltaX;
    }

    if (descriptor.upY)
    {
        vertices.push_back(dx::Vector4(x, 0.0f, +y, 1.0f));
        vertices.push_back(dx::Vector4(x, 0.0f, -y, 1.0f));
    }
    else
    {
        vertices.push_back(dx::Vector4(x, +y, 0.0f, 1.0f));
        vertices.push_back(dx::Vector4(x, -y, 0.0f, 1.0f));
    }

    x = descriptor.lenX * -0.5f;
    y = descriptor.lenY * -0.5f;
    for (unsigned i = 0; i < ySize; i++)
    {
        auto hi = (uint16_t)vertices.size();

        if (descriptor.upY)
        {
            vertices.push_back(dx::Vector4(+x, 0.0f, y, 1.0f));
            vertices.push_back(dx::Vector4(-x, 0.0f, y, 1.0f));
        }
        else
        {
            vertices.push_back(dx::Vector4(+x, y, 0.0f, 1.0f));
            vertices.push_back(dx::Vector4(-x, y, 0.0f, 1.0f));
        }

        indices.push_back(hi + 0);
        indices.push_back(hi + 1);
        y += deltaY;
    }

    if (descriptor.upY)
    {
        vertices.push_back(dx::Vector4(+x, 0.0f, y, 1.0f));
        vertices.push_back(dx::Vector4(-x, 0.0f, y, 1.0f));
    }
    else
    {
        vertices.push_back(dx::Vector4(+x, y, 0.0f, 1.0f));
        vertices.push_back(dx::Vector4(-x, y, 0.0f, 1.0f));
    }

    indexCount = indices.size();

    ResourceHelper::createVertexBufferCommittedScheduled(
        deviceResources,
        vertexBuffer,
        vertexBufferUploadHeap,
        vertices.data(),
        (uint32_t)vertices.size(),
        vertexBufferView
        );
    ResourceHelper::createIndexBufferCommittedScheduled(
        deviceResources,
        indexBuffer,
        indexBufferUploadHeap,
        indices.data(),
        (uint32_t)indices.size(),
        indexBufferView
        );

}