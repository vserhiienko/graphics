#pragma once
#include <CiriDeviceResources.h>
#include <CiriConstantBuffer.h>

namespace ciri
{
    struct BillboardDescriptor
    {
    public:
        bool upY;
        float lenX, lenY;
        unsigned resX, resY;

    public:
        static void makeLargeBillboard(BillboardDescriptor &);
    };


    struct BillboardResources
    {
    public:
        struct PerFrameData
        {
            DirectX::XMFLOAT4X4 pose;
            DirectX::XMFLOAT4 color;
        };

    public:
        size_t indexCount;
        ComPtr<ID3D12Resource> indexBuffer;
        ComPtr<ID3D12Resource> vertexBuffer;
        ComPtr<ID3D12Resource> indexBufferUploadHeap;
        ComPtr<ID3D12Resource> vertexBufferUploadHeap;
        D3D12_INDEX_BUFFER_VIEW indexBufferView;
        D3D12_VERTEX_BUFFER_VIEW vertexBufferView;
        ConstantBuffer<PerFrameData> perFrameBuffer;

    public:
        void pushConstantBuffers(
            _Inout_ ConstantBufferBase::Vector &collection
            );
        void advanceConstantBufferUploadHeap(
            _Inout_ ConstantBufferUploadHeap &contantBufferUploadHeap
            );
        void updateConstanBufferSources(
            );

    public:
        void createFor(
            _In_ DeviceResources &deviceResources,
            _In_ BillboardDescriptor const &descriptor,
            _In_ ConstantBufferUploadHeap &contantBufferUploadHeap
            );

    };

}
