#pragma once

#include <windows.h>
#include <windowsx.h>
#include <wrl.h>
#include <d3d12.h>
#include <inttypes.h>

#include <vector>
#include <ppl.h>
#include <ppltasks.h>

#include <DxNeon.h>
#include <DxAnim.h>
#include <DxUtils.h>
#include <DxTimer.h>

#include <DxVirtualKeys.h>

#include <DirectXMath.h>
#include <DirectXColors.h>
#include <DirectXCollision.h>
#include <DirectXPackedVector.h>

namespace ciri
{
	using Microsoft::WRL::ComPtr;
}