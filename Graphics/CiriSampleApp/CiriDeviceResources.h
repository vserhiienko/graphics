#pragma once

#include <CiriRenderTarget.h>

namespace ciri
{
	class DeviceResources
	{
	public:
		static const uint16_t backbufferCount = 2;
		static const DXGI_FORMAT depthStencilFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;

	public:
		HRESULT lastError;

	public:
		D3D_DRIVER_TYPE driverType;
		D3D_FEATURE_LEVEL featureLevel;
		ComPtr<ID3D12Device> device;
		ComPtr<IDXGISwapChain> swapchain;
		ComPtr<ID3D12CommandQueue> defaultCQ;
		ComPtr<ID3D12CommandAllocator> defaultCA;
        ComPtr<ID3D12CommandAllocator> defaultBA;
        ComPtr<ID3D12GraphicsCommandList> setupCL;
        RenderTarget renderTarget;
#if _DEBUG
        ComPtr<ID3D12Debug> debug;
#endif

    public:
        // synchronization

        uint64_t fenceU;
        ComPtr<ID3D12Fence> fence;


	public:
		HWND windowHandle;
		uint16_t windowW, windowH;

	public:
		uint32_t 
			descriptorOffsetS,
			descriptorOffsetRTV,
			descriptorOffsetDSV, 
			descriptorOffsetSRV, 
			descriptorOffsetCBV, 
			descriptorOffsetUAV;

	public:
        DeviceResources();
        ~DeviceResources();

    public:
        bool initializeFor(HWND window_handle, bool shader_debugging);
        void finalizeResourceSetup();

        /// this will print all the device living object information
        /// to the output window

        void reportLiveObjects();

        /// all the com pointers should be allocated in destructor
        /// or inside recreation functions

        void manualCleanup();

    public:
        static void setResourceBarriers(
            ID3D12GraphicsCommandList* commandList, 
            ID3D12Resource* resources, 
            uint32_t before, uint32_t after,
            uint32_t resourceCount = 1,
            uint32_t subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES
            );

        

	protected:
		void updateDescriptorOffsets();

	};
}




