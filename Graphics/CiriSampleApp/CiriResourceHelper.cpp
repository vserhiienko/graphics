#include <CiriResourceHelper.h>
using namespace ciri;

static void createResourceCommittedScheduled(
    DeviceResources &deviceResources,
    ComPtr<ID3D12Resource> &vertexBuffer,
    ComPtr<ID3D12Resource> &vertexBufferUploadHeap,
    void *resourceData,
    uint32_t resourceDataSize
    )
{
    reportComError("CreateCommittedResource: ", deviceResources.device->CreateCommittedResource(
        &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
        D3D12_HEAP_MISC_NONE,
        &CD3D12_RESOURCE_DESC::Buffer(resourceDataSize),
        D3D12_RESOURCE_USAGE_INITIAL,
        nullptr,
        IID_PPV_ARGS(&vertexBuffer)
        ));
    reportComError("CreateCommittedResource: ", deviceResources.device->CreateCommittedResource(
        &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
        D3D12_HEAP_MISC_NONE,
        &CD3D12_RESOURCE_DESC::Buffer(resourceDataSize),
        D3D12_RESOURCE_USAGE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(&vertexBufferUploadHeap)
        ));

    D3D12_SUBRESOURCE_DATA vertexBufferSubresourceData = {};
    vertexBufferSubresourceData.pData = resourceData;
    vertexBufferSubresourceData.RowPitch = resourceDataSize;
    vertexBufferSubresourceData.SlicePitch = vertexBufferSubresourceData.RowPitch;

    DeviceResources::setResourceBarriers(deviceResources.setupCL.Get(), vertexBuffer.Get(), D3D12_RESOURCE_USAGE_INITIAL, D3D12_RESOURCE_USAGE_COPY_DEST);
    UpdateSubresources<1>(deviceResources.setupCL.Get(), vertexBuffer.Get(), vertexBufferUploadHeap.Get(), 0, 0, 1, &vertexBufferSubresourceData);
    DeviceResources::setResourceBarriers(deviceResources.setupCL.Get(), vertexBuffer.Get(), D3D12_RESOURCE_USAGE_COPY_DEST, D3D12_RESOURCE_USAGE_GENERIC_READ);
}

void ResourceHelper::createVertexBufferCommittedScheduled(
    DeviceResources &deviceResources,
    ComPtr<ID3D12Resource> &vertexBuffer,
    ComPtr<ID3D12Resource> &vertexBufferUploadHeap,
    void *vertexBufferData,
    uint32_t vertexBufferStride,
    uint32_t vertexBufferSize,
    D3D12_VERTEX_BUFFER_VIEW &vertexBufferView
    )
{
    createResourceCommittedScheduled(
        deviceResources,
        vertexBuffer,
        vertexBufferUploadHeap,
        vertexBufferData,
        vertexBufferSize
        );

    // create vert buffer table
    vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
    vertexBufferView.StrideInBytes = vertexBufferStride;
    vertexBufferView.SizeInBytes = vertexBufferSize;

    
}

void ResourceHelper::createIndexBufferCommittedScheduled(
    DeviceResources &deviceResources,
    ComPtr<ID3D12Resource> &indexBuffer,
    ComPtr<ID3D12Resource> &indexBufferUploadHeap,
    void *indexBufferData,
    DXGI_FORMAT indexBufferFormat,
    uint32_t indexBufferSize,
    D3D12_INDEX_BUFFER_VIEW &indexBufferView
    )
{
    createResourceCommittedScheduled(
        deviceResources,
        indexBuffer,
        indexBufferUploadHeap,
        indexBufferData,
        indexBufferSize
        );

    // create index buffer table
    indexBufferView.BufferLocation = indexBuffer->GetGPUVirtualAddress();
    indexBufferView.Format = indexBufferFormat;
    indexBufferView.SizeInBytes = indexBufferSize;
}