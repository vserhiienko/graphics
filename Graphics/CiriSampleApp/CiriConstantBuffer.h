#pragma once
#include <CiriDeviceResources.h>
#include <CiriMisc.h>

#define ciri_align_256 _declspec(align(256))

namespace ciri
{
    class ConstantBufferBase
    {
    public:
        typedef std::vector<ConstantBufferBase*> Vector;
        static const uint32_t placementAlignment = D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT;

    public:
        uint32_t bufferSize;
        uint8_t *uploadHeapPointer;

    public:
        ConstantBufferBase(
            _In_ uint32_t bufferSize
            );

    public:
        void create(
            _Inout_ DeviceResources &deviceResources,
            _In_ D3D12_CPU_DESCRIPTOR_HANDLE cpuDescriptorHandleDH,    // descriptor heap
            _In_ D3D12_GPU_VIRTUAL_ADDRESS gpuVirtualAddressUH         // upload heap
            );
        void setAndAdvanceUploadHeapPointer(
            _In_ uint8_t *currentPosition, 
            _Out_ uint8_t **advancePosition
            );
    };

    template <typename _Ty> 
    struct ConstantBuffer 
        : public ConstantBufferBase
    {
    public:
        _Ty *bufferSource;

    public:
        ConstantBuffer()
            : ConstantBufferBase(sizeof(_Ty))
            , bufferSource(0)
        {
        }

    public:
        void setUploadHeapPointer()
        {
            bufferSource = reinterpret_cast<_Ty*>(uploadHeapPointer);
        }
    };

    struct ConstantBufferUploadHeap
    {
    public:
        uint32_t uploadHeapSize;
        uint8_t *uploadHeapPointer;
        ComPtr<ID3D12Resource> uploadHeap;

    public:
        ConstantBufferUploadHeap();

    public:
        void create(
            _Inout_ DeviceResources &deviceResources
            );
        void createConstantBuffers(
            _Inout_ DeviceResources &deviceResources,
            _In_ D3D12_CPU_DESCRIPTOR_HANDLE startCPUDescriptorHandleDH,    // descriptor heap
            _In_ D3D12_GPU_VIRTUAL_ADDRESS startGPUVirtualAddressUH,        // upload heap
            _In_ ConstantBufferBase *constantBuffers,
            _In_ uint32_t constantBufferCount
            );

    public:
        template <typename _Ty>
        void advanceUploadHeapSizeForType(uint32_t itemOfTypeCount = 1)
        {
            uploadHeapSize += sizeof(_Ty) * itemOfTypeCount;
        }
    };

}

