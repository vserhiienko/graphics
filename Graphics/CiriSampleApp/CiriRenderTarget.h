#pragma once

#include <CiriPrecompiled.h>

namespace ciri
{
	class DeviceResources;
	class RenderTarget
	{
	public:
		HRESULT lastError;
		D3D12_RECT scissorRect;
		D3D12_VIEWPORT viewport;
		DXGI_FORMAT depthStencilFormat;

    public:
        /// resources

		ComPtr<ID3D12Resource> 
            renderTargetR[2], 
            depthStencilR[2];
        ComPtr<ID3D12Resource> 
            &renderTargetR0,
            &renderTargetR1, 
            &depthStencilR0,
            &depthStencilR1;

    public:
        /// descriptor heaps & handles

        ComPtr<ID3D12DescriptorHeap>
            renderTargetDH,
            depthStencilDH;
        D3D12_CPU_DESCRIPTOR_HANDLE
            renderTargetCDHs[2],
            depthStencilCDHs[2];
        D3D12_CPU_DESCRIPTOR_HANDLE
            &renderTargetCDH0,
            &renderTargetCDH1,
            &depthStencilCDH0,
            &depthStencilCDH1;

    public:
        bool renderTargetShaderVisible;
        bool depthStencilShaderVisible;

	public:
		RenderTarget(
			_In_opt_ bool rt_shader_visible = false, 
			_In_opt_ bool ds_shader_visible = false,
			_In_opt_ DXGI_FORMAT depth_stencil_format = DXGI_FORMAT_UNKNOWN
			);

        /// all the com pointers should be allocated in destructor
        /// or inside recreation functions

        void manualCleanup();

		void recreateRenderTargetsFor(_In_ DeviceResources &);
		void recreateDepthStencilFor(_In_ DeviceResources &);

        /// to use these functions, the 'depthStencilShaderVisible' should be set,
        /// before the 'recreateDepthStencilFor' is called
        /// otherwise, call 'beginFrameDepthStencilBarriers' once the 'recreateDepthStencilFor' is called

        void initializeFrameDepthStencilBarriers(_In_ ID3D12GraphicsCommandList* commandList);
        void beginFrameDepthStencilBarriers(_In_ ID3D12GraphicsCommandList* commandList);
        void readFrameDepthStencilBarriers(_In_ ID3D12GraphicsCommandList* commandList);
        void endFrameDepthStencilBarriers(_In_ ID3D12GraphicsCommandList* commandList);

        /// to use these functions, the 'renderTargetShaderVisible' should be set,
        /// before the 'recreateRenderTargetFor/recreateRenderTargetFrom' is called

        void attachFrameRenderTargetBarriers(_In_ ID3D12GraphicsCommandList* commandList);
        void readFrameRenderTargetBarriers(_In_ ID3D12GraphicsCommandList* commandList);
        void dettachFrameRenderTargetBarriers(_In_ ID3D12GraphicsCommandList* commandList);

        /// default render target behaviour

        void beginFrameRenderTargetBarriers(_In_ ID3D12GraphicsCommandList* commandList);
        void endFrameRenderTargetBarriers(_In_ ID3D12GraphicsCommandList* commandList);

        /// should be called after begin/initialize barriers for resources

        void clearRenderTarget(_In_ ID3D12GraphicsCommandList* commandList, _In_count_(4) float *rgba);
        void clearDepthStencil(_In_ ID3D12GraphicsCommandList* commandList, float depth, uint8_t stencil, D3D12_CLEAR_FLAG clear = D3D12_CLEAR_DEPTH);

        /// only the resource/descriptor handles will be swapped

        void swapRenderTargets(void);
        void swapDepthStencils(void);
		


	};
}