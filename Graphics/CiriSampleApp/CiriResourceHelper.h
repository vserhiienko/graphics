#pragma once

#include <CiriDeviceResources.h>
#include <CiriMisc.h>

namespace ciri
{
    class ResourceHelper
    {
    public:
        /// copy data to the intermediate upload heap and 
        /// then schedule a copy from the upload heap to the default buffer

    public:
        static void createResourceCommittedScheduled(
            DeviceResources &deviceResources,
            ComPtr<ID3D12Resource> &vertexBuffer,
            ComPtr<ID3D12Resource> &vertexBufferUploadHeap,
            void *resourceData,
            uint32_t resourceDataSize
            );

    public:
        static void createVertexBufferCommittedScheduled(
            DeviceResources &deviceResources,
            ComPtr<ID3D12Resource> &vertexBuffer,
            ComPtr<ID3D12Resource> &vertexBufferUploadHeap,
            void *vertexBufferData,
            uint32_t vertexBufferStride,
            uint32_t vertexBufferSize,
            D3D12_VERTEX_BUFFER_VIEW &vertexBufferView
            );
        static void createIndexBufferCommittedScheduled(
            DeviceResources &deviceResources,
            ComPtr<ID3D12Resource> &indexBuffer,
            ComPtr<ID3D12Resource> &indexBufferUploadHeap,
            void *indexBufferData,
            DXGI_FORMAT indexBufferFormat,
            uint32_t indexBufferSize,
            D3D12_INDEX_BUFFER_VIEW &indexBufferView
            );

    public:
        template <typename _Ty>
        static void createVertexBufferCommittedScheduled(
            DeviceResources &deviceResources,
            ComPtr<ID3D12Resource> &vertexBuffer,
            ComPtr<ID3D12Resource> &vertexBufferUploadHeap,
            _Ty *elements,
            uint32_t elementCount,
            D3D12_VERTEX_BUFFER_VIEW &vertexBufferView
            );
        template <typename _Ty>
        static void createIndexBufferCommittedScheduled(
            DeviceResources &deviceResources,
            ComPtr<ID3D12Resource> &indexBuffer,
            ComPtr<ID3D12Resource> &indexBufferUploadHeap,
            _Ty *elements,
            uint32_t indexBufferSize,
            D3D12_INDEX_BUFFER_VIEW &indexBufferView
            );
        template <>
        static void createIndexBufferCommittedScheduled<uint16_t>(
            DeviceResources &deviceResources,
            ComPtr<ID3D12Resource> &indexBuffer,
            ComPtr<ID3D12Resource> &indexBufferUploadHeap,
            uint16_t *elements,
            uint32_t indexBufferSize,
            D3D12_INDEX_BUFFER_VIEW &indexBufferView
            );
        template <>
        static void createIndexBufferCommittedScheduled<uint32_t>(
            DeviceResources &deviceResources,
            ComPtr<ID3D12Resource> &indexBuffer,
            ComPtr<ID3D12Resource> &indexBufferUploadHeap,
            uint32_t *elements,
            uint32_t indexBufferSize,
            D3D12_INDEX_BUFFER_VIEW &indexBufferView
            );
        template <>
        static void createIndexBufferCommittedScheduled<uint64_t>(
            DeviceResources &deviceResources,
            ComPtr<ID3D12Resource> &indexBuffer,
            ComPtr<ID3D12Resource> &indexBufferUploadHeap,
            uint64_t *elements,
            uint32_t indexBufferSize,
            D3D12_INDEX_BUFFER_VIEW &indexBufferView
            );
    };

#include <CiriResourceHelper.inl>

}