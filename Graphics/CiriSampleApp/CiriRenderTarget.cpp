#include <CiriMisc.h>
#include <CiriDeviceResources.h>
#include <CiriRenderTarget.h>
using namespace ciri;

RenderTarget::RenderTarget(bool rt_shader_visible, bool ds_shader_visible, DXGI_FORMAT depth_stencil_format)
    // initialize referencing
    : renderTargetR0(renderTargetR[0])
    , renderTargetR1(renderTargetR[1])
    , depthStencilR0(depthStencilR[0])
    , depthStencilR1(depthStencilR[1])
    , depthStencilCDH0(depthStencilCDHs[0])
    , depthStencilCDH1(depthStencilCDHs[1])
    , renderTargetCDH0(renderTargetCDHs[0])
    , renderTargetCDH1(renderTargetCDHs[1])
    // set pixel shader resources
	, renderTargetShaderVisible(rt_shader_visible)
	, depthStencilShaderVisible(ds_shader_visible)
    // set depth stencil format
	, depthStencilFormat(DeviceResources::depthStencilFormat)
{
	if (depth_stencil_format != DXGI_FORMAT_UNKNOWN)
        depthStencilFormat = depth_stencil_format;
}

void RenderTarget::manualCleanup()
{
    depthStencilR0 = nullptr;
    depthStencilR1 = nullptr;
    depthStencilDH = nullptr;
    renderTargetR0 = nullptr;
    renderTargetR1 = nullptr;
    renderTargetDH = nullptr;
}

void RenderTarget::recreateDepthStencilFor(DeviceResources &device_resources)
{
	if (!device_resources.swapchain.Get()) return;

	DXGI_SWAP_CHAIN_DESC swapchainDesc;
	lastError = device_resources.swapchain->GetDesc(&swapchainDesc);
	reportComError("GetDesc: ", lastError);

	uint32_t windowSzX = swapchainDesc.BufferDesc.Width;
	uint32_t windowSzY = swapchainDesc.BufferDesc.Height;

	D3D12_RESOURCE_MISC_FLAG flags = D3D12_RESOURCE_MISC_ALLOW_DEPTH_STENCIL;
	flags |= depthStencilShaderVisible
		? D3D12_RESOURCE_MISC_NONE
		: D3D12_RESOURCE_MISC_DENY_SHADER_RESOURCE;

	CD3D12_RESOURCE_DESC depthStencilRDesc(
		D3D12_RESOURCE_DIMENSION_TEXTURE_2D,
		0, // alignment
		windowSzX,
		windowSzY,
		1, 
		1, // mip w
		depthStencilFormat,
		1, 0, // sample count/quality
		D3D12_TEXTURE_LAYOUT_UNKNOWN,
		flags
		);

	D3D12_CLEAR_VALUE clearValue;
	dx::zeroMemory(clearValue);
	clearValue.Format = depthStencilFormat;
	clearValue.DepthStencil.Depth = 1.0f;
	clearValue.DepthStencil.Stencil = 0;

	lastError = device_resources.device->CreateCommittedResource(
		&CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_MISC_NONE,
		&depthStencilRDesc,
		D3D12_RESOURCE_USAGE_INITIAL,
		&clearValue,
		IID_PPV_ARGS(depthStencilR0.ReleaseAndGetAddressOf())
		);
    reportComError("CreateCommittedResource: ", lastError);

    if (depthStencilShaderVisible)
    {
        lastError = device_resources.device->CreateCommittedResource(
            &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
            D3D12_HEAP_MISC_NONE,
            &depthStencilRDesc,
            D3D12_RESOURCE_USAGE_INITIAL,
            &clearValue,
            IID_PPV_ARGS(depthStencilR1.ReleaseAndGetAddressOf())
            );
        reportComError("CreateCommittedResource: ", lastError);
    }

    D3D12_DESCRIPTOR_HEAP_FLAGS flagsDH = D3D12_DESCRIPTOR_HEAP_NONE;
    (uint32_t&)flagsDH |= depthStencilShaderVisible
        ? D3D12_DESCRIPTOR_HEAP_SHADER_VISIBLE
        : D3D12_DESCRIPTOR_HEAP_NONE;

	D3D12_DESCRIPTOR_HEAP_DESC depthStencilDHDesc = {};
	dx::zeroMemory(depthStencilDHDesc);
    depthStencilDHDesc.NumDescriptors = depthStencilShaderVisible ? 2 : 1;
	depthStencilDHDesc.Type = D3D12_DSV_DESCRIPTOR_HEAP;
    depthStencilDHDesc.Flags = flagsDH;

	lastError = device_resources.device->CreateDescriptorHeap(
		&depthStencilDHDesc, 
		IID_PPV_ARGS(depthStencilDH.ReleaseAndGetAddressOf())
		);
	reportComError("CreateDescriptorHeap: ", lastError);

    // set cpu descriptor handles
    // they are referenced by the class members
    // they are referenced by the class members

    depthStencilCDHs[0] = depthStencilDH->GetCPUDescriptorHandleForHeapStart();

    if (depthStencilShaderVisible)
    {
        depthStencilCDHs[1] = depthStencilCDHs[0].MakeOffsetted(device_resources.descriptorOffsetDSV);;
    }

	/*
    D3D12_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	dx::zeroMemory(depthStencilViewDesc);
	depthStencilViewDesc.Format = depthStencilFormat;
	depthStencilViewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;
    */

	device_resources.device->CreateDepthStencilView(
		depthStencilR0.Get(), 
        nullptr, //&depthStencilViewDesc,
        depthStencilCDH0
        );

    if (depthStencilShaderVisible)
    {
        device_resources.device->CreateDepthStencilView(
            depthStencilR1.Get(),
            nullptr, //&depthStencilViewDesc,
            depthStencilCDH1
            );
    }
}

void RenderTarget::recreateRenderTargetsFor(DeviceResources &device_resources)
{

	if (!device_resources.swapchain.Get()) return;

	// Create the viewport
	dx::zeroMemory(viewport);
	dx::zeroMemory(scissorRect);

	DXGI_SWAP_CHAIN_DESC swapchainDesc;
	lastError = device_resources.swapchain->GetDesc(&swapchainDesc);
	reportComError("GetDesc: ", lastError);

	uint32_t windowSzX = swapchainDesc.BufferDesc.Width;
	uint32_t windowSzY = swapchainDesc.BufferDesc.Height;

	viewport =
	{
		0, 0, // top x/y
		windowSzX, // w
		windowSzY, // h
		0.0f, 1.0f // min/max depth
	};

	scissorRect = 
	{ 
		0, 0, // top/left
		windowSzX, // right
		windowSzY, // bottom
	};

	// get resources
	lastError = device_resources.swapchain->GetBuffer(
		0, IID_PPV_ARGS(renderTargetR[0].ReleaseAndGetAddressOf())
		);
	reportComError("GetBuffer: ", lastError);

	lastError = device_resources.swapchain->GetBuffer(
		1, IID_PPV_ARGS(renderTargetR[1].ReleaseAndGetAddressOf())
		);
	reportComError("GetBuffer: ", lastError);

    D3D12_DESCRIPTOR_HEAP_FLAGS flagsDH = D3D12_DESCRIPTOR_HEAP_NONE;
    (uint32_t&)flagsDH |= renderTargetShaderVisible
        ? D3D12_DESCRIPTOR_HEAP_SHADER_VISIBLE
        : D3D12_DESCRIPTOR_HEAP_NONE;

	D3D12_DESCRIPTOR_HEAP_DESC renderRargetDHDesc;
	dx::zeroMemory(renderRargetDHDesc);
	renderRargetDHDesc.NumDescriptors = 2;
	renderRargetDHDesc.Type = D3D12_RTV_DESCRIPTOR_HEAP;
    renderRargetDHDesc.Flags = flagsDH;

	lastError = device_resources.device->CreateDescriptorHeap(
		&renderRargetDHDesc, 
        IID_PPV_ARGS(renderTargetDH.ReleaseAndGetAddressOf())
		);
	reportComError("CreateDescriptorHeap: ", lastError);

    renderTargetCDHs[0] = renderTargetDH->GetCPUDescriptorHandleForHeapStart();
    renderTargetCDHs[1] = renderTargetCDHs[0].MakeOffsetted(device_resources.descriptorOffsetRTV);

	device_resources.device->CreateRenderTargetView(
		renderTargetR0.Get(),
		nullptr,
        renderTargetCDH0
		);
	device_resources.device->CreateRenderTargetView(
		renderTargetR1.Get(),
		nullptr,
        renderTargetCDH1
		);
}
void RenderTarget::initializeFrameDepthStencilBarriers(ID3D12GraphicsCommandList* commandList)
{
    DeviceResources::setResourceBarriers(
        commandList,
        depthStencilR0.Get(),
        D3D12_RESOURCE_USAGE_INITIAL,
        D3D12_RESOURCE_USAGE_DEPTH
        );
}
void RenderTarget::beginFrameDepthStencilBarriers(ID3D12GraphicsCommandList* commandList)
{
    /// depthStencilR0 D3D12_RESOURCE_USAGE_INITIAL
    /// depthStencilR1 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE

    // 1st resource as depth stencil view
    DeviceResources::setResourceBarriers(
        commandList,
        depthStencilR0.Get(),
        D3D12_RESOURCE_USAGE_INITIAL,
        D3D12_RESOURCE_USAGE_DEPTH
        );

    /// depthStencilR0 D3D12_RESOURCE_USAGE_DEPTH
    /// depthStencilR1 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
}
void RenderTarget::readFrameDepthStencilBarriers(ID3D12GraphicsCommandList* commandList)
{
    /// depthStencilR0 D3D12_RESOURCE_USAGE_DEPTH
    /// depthStencilR1 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE

    DeviceResources::setResourceBarriers(
        commandList,
        depthStencilR0.Get(),
        D3D12_RESOURCE_USAGE_DEPTH,
        D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
        );
    DeviceResources::setResourceBarriers(
        commandList,
        depthStencilR1.Get(),
        D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE,
        D3D12_RESOURCE_USAGE_INITIAL
        );

    /// depthStencilR0 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
    /// depthStencilR1 D3D12_RESOURCE_USAGE_INITIAL
}
void RenderTarget::endFrameDepthStencilBarriers(ID3D12GraphicsCommandList* commandList)
{
    // depthStencilR0 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
    // depthStencilR1 D3D12_RESOURCE_USAGE_INITIAL

    swapDepthStencils();

    // depthStencilR0 D3D12_RESOURCE_USAGE_INITIAL
    // depthStencilR1 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
}


void RenderTarget::attachFrameRenderTargetBarriers(_In_ ID3D12GraphicsCommandList* commandList)
{
    /// renderTargetR0 D3D12_RESOURCE_USAGE_INITIAL
    /// renderTargetR1 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE

    DeviceResources::setResourceBarriers(
        commandList,
        renderTargetR0.Get(),
        D3D12_RESOURCE_USAGE_INITIAL,
        D3D12_RESOURCE_USAGE_RENDER_TARGET
        );

    /// renderTargetR0 D3D12_RESOURCE_USAGE_RENDER_TARGET
    /// renderTargetR1 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
}
void RenderTarget::readFrameRenderTargetBarriers(_In_ ID3D12GraphicsCommandList* commandList)
{

    /// renderTargetR0 D3D12_RESOURCE_USAGE_RENDER_TARGET
    /// renderTargetR1 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE

    DeviceResources::setResourceBarriers(
        commandList,
        renderTargetR0.Get(),
        D3D12_RESOURCE_USAGE_RENDER_TARGET,
        D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
        );
    DeviceResources::setResourceBarriers(
        commandList,
        renderTargetR1.Get(),
        D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE,
        D3D12_RESOURCE_USAGE_INITIAL
        );

    /// renderTargetR0 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
    /// renderTargetR1 D3D12_RESOURCE_USAGE_INITIAL
}
void RenderTarget::dettachFrameRenderTargetBarriers(_In_ ID3D12GraphicsCommandList* commandList)
{
    // renderTargetR0 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
    // renderTargetR1 D3D12_RESOURCE_USAGE_INITIAL

    swapRenderTargets();

    // renderTargetR0 D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
    // renderTargetR1 D3D12_RESOURCE_USAGE_INITIAL
}

void RenderTarget::beginFrameRenderTargetBarriers(ID3D12GraphicsCommandList* commandList)
{
    DeviceResources::setResourceBarriers(
        commandList, 
        renderTargetR0.Get(), 
        D3D12_RESOURCE_USAGE_PRESENT, 
        D3D12_RESOURCE_USAGE_RENDER_TARGET
        );
}

void RenderTarget::endFrameRenderTargetBarriers(ID3D12GraphicsCommandList* commandList)
{
    DeviceResources::setResourceBarriers(
        commandList,
        renderTargetR0.Get(),
        D3D12_RESOURCE_USAGE_RENDER_TARGET,
        D3D12_RESOURCE_USAGE_PRESENT
        );

    swapRenderTargets();
}

void RenderTarget::clearRenderTarget(ID3D12GraphicsCommandList* commandList, float *rgba)
{
    commandList->ClearRenderTargetView(
        renderTargetCDH0, 
        rgba, 
        nullptr, 0
        );
}

void RenderTarget::clearDepthStencil(
    ID3D12GraphicsCommandList* commandList, 
    float depth, uint8_t stencil, 
    D3D12_CLEAR_FLAG clear
    )
{
    commandList->ClearDepthStencilView(
        depthStencilCDH0, 
        clear,
        depth, 
        stencil, 
        nullptr, 0
        );
}

void RenderTarget::swapRenderTargets()
{
	//renderTargetR[0].Swap(renderTargetR[1]);
    std::swap<>(renderTargetR0, renderTargetR1);
    std::swap<>(renderTargetCDH0, renderTargetCDH1);
}

void RenderTarget::swapDepthStencils()
{
    //depthStencilR[0].Swap(depthStencilR[1]);
    std::swap<>(depthStencilR0, depthStencilR1);
    std::swap<>(depthStencilCDH0, depthStencilCDH1);
}




