
// copy data to the intermediate upload heap and 
// then schedule a copy from the upload heap to the default buffer
template <typename _Ty>
static void ResourceHelper::createVertexBufferCommittedScheduled(
    DeviceResources &deviceResources,
    ComPtr<ID3D12Resource> &vertexBuffer,
    ComPtr<ID3D12Resource> &vertexBufferUploadHeap,
    _Ty *elements,
    uint32_t elementCount,
    D3D12_VERTEX_BUFFER_VIEW &vertexBufferView
    )
{
    createVertexBufferCommittedScheduled(
        deviceResources,
        vertexBuffer,
        vertexBufferUploadHeap,
        elements,
        sizeof(_Ty) * elementCount,
        sizeof(_Ty),
        vertexBufferView
        );
}

// copy data to the intermediate upload heap and 
// then schedule a copy from the upload heap to the default buffer
template <>
static void ResourceHelper::createIndexBufferCommittedScheduled<uint16_t>(
    DeviceResources &deviceResources,
    ComPtr<ID3D12Resource> &indexBuffer,
    ComPtr<ID3D12Resource> &indexBufferUploadHeap,
    uint16_t *elements,
    uint32_t indexBufferSize,
    D3D12_INDEX_BUFFER_VIEW &indexBufferView
    )
{
    createIndexBufferCommittedScheduled(
        deviceResources,
        indexBuffer,
        indexBufferUploadHeap,
        elements,
        DXGI_FORMAT_R16_UINT,
        indexBufferSize,
        indexBufferView
        );
}

// copy data to the intermediate upload heap and 
// then schedule a copy from the upload heap to the default buffer
template <>
static void ResourceHelper::createIndexBufferCommittedScheduled<uint32_t>(
    DeviceResources &deviceResources,
    ComPtr<ID3D12Resource> &indexBuffer,
    ComPtr<ID3D12Resource> &indexBufferUploadHeap,
    uint32_t *elements,
    uint32_t indexBufferSize,
    D3D12_INDEX_BUFFER_VIEW &indexBufferView
    )
{
    createIndexBufferCommittedScheduled(
        deviceResources,
        indexBuffer,
        indexBufferUploadHeap,
        elements,
        DXGI_FORMAT_R32_UINT,
        indexBufferSize,
        indexBufferView
        );
}

// copy data to the intermediate upload heap and 
// then schedule a copy from the upload heap to the default buffer
template <>
static void ResourceHelper::createIndexBufferCommittedScheduled<uint64_t>(
    DeviceResources &deviceResources,
    ComPtr<ID3D12Resource> &indexBuffer,
    ComPtr<ID3D12Resource> &indexBufferUploadHeap,
    uint64_t *elements,
    uint32_t indexBufferSize,
    D3D12_INDEX_BUFFER_VIEW &indexBufferView
    )
{
    createIndexBufferCommittedScheduled(
        deviceResources,
        indexBuffer,
        indexBufferUploadHeap,
        elements,
        DXGI_FORMAT_R32G32_UINT,
        indexBufferSize,
        indexBufferView
        );
}