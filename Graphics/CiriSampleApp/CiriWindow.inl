
template <typename _EventCallback, typename _FrameMoveCallback, typename _CleanupCallback>
void ciri::Window::mainLoop(
	_EventCallback eventCallback,
	_FrameMoveCallback frameMoveCallback,
	_CleanupCallback cleanupCallback
	)
{
	EventArgs args;
	MSG msg = { 0 };

	while (true)
	{
		// check to see if any messages are waiting in the queue
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			// check to see if it's time to quit
            if (msg.message == WM_QUIT) 
                break;
            else 
            {
                dx::trace("mainLoop: msg 0x%0x", msg.message);

                args.store(msg);
                if (eventCallback(args))
                    continue;

                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
		}
        else
        {
            frameMoveCallback();
        }
	}

	cleanupCallback();
}