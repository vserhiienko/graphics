#include <CiriMisc.h>
#include <CiriDeviceResources.h>
using namespace ciri;

static HRESULT CreateDeviceAndSwapChain(
	_In_opt_ IDXGIAdapter* pAdapter,
	D3D_DRIVER_TYPE DriverType,
	D3D12_CREATE_DEVICE_FLAG Flags,
	D3D_FEATURE_LEVEL MinimumFeatureLevel,
	UINT SDKVersion,
	_In_opt_ CONST DXGI_SWAP_CHAIN_DESC* pSwapChainDesc,
	_In_ REFIID riidSwapchain,
	_COM_Outptr_opt_ void** ppSwapChain,
	_In_ REFIID riidDevice,
	_COM_Outptr_opt_ void** ppDevice
	)
{
	ComPtr<ID3D12Device> pDevice;
    ComPtr<IDXGIFactory> pDxgiFactory;
	ComPtr<IDXGISwapChain> pDxgiSwapChain;
    

	HRESULT hr = D3D12CreateDevice(
		pAdapter,
		DriverType,
		Flags,
		MinimumFeatureLevel,
		SDKVersion,
		IID_PPV_ARGS(&pDevice)
		);
	if (FAILED(hr)) { return hr; }

    ComPtr<IDXGIDevice> pDxgiDevice;
    if (SUCCEEDED(pDevice.As(&pDxgiDevice)))
    {
        ComPtr<IDXGIAdapter> pDxgiAdapter;
        pDxgiDevice->GetAdapter(pDxgiAdapter.ReleaseAndGetAddressOf());
        auto myAdapter = pDxgiAdapter.Get();
        auto adapterDescription = DXGI_ADAPTER_DESC();
        myAdapter->GetDesc(&adapterDescription);
    }

	hr = CreateDXGIFactory1(IID_PPV_ARGS(&pDxgiFactory));
	if (FAILED(hr)) { return hr; }

	DXGI_SWAP_CHAIN_DESC LocalSCD = *pSwapChainDesc;
	hr = pDxgiFactory->CreateSwapChain(
		pDevice.Get(),
		&LocalSCD,
		&pDxgiSwapChain
		);
	if (FAILED(hr)) { return hr; }

	hr = pDevice.Get()->QueryInterface(riidDevice, ppDevice);
	if (FAILED(hr)) { return hr; }

	hr = pDxgiSwapChain.Get()->QueryInterface(riidSwapchain, ppSwapChain);
	if (FAILED(hr))
	{
		reinterpret_cast<IUnknown*>(*ppDevice)->Release();
		return hr;
	}

	return S_OK;
}

DeviceResources::DeviceResources()
	: renderTarget(false, false, depthStencilFormat)
	, driverType(D3D_DRIVER_TYPE_UNKNOWN)
    //, featureLevel(D3D_FEATURE_LEVEL_9_1)
    , featureLevel(D3D_FEATURE_LEVEL_11_1)
{
}

DeviceResources::~DeviceResources()
{
    if (swapchain.Get()) swapchain->SetFullscreenState(false, nullptr);
    reportLiveObjects();
}

void DeviceResources::reportLiveObjects()
{
#if _DEBUG
    dx::trace("\n----------------------------------------------");
    dx::trace("reportLiveObjects: device live objects:");
    dx::trace("----------------------------------------------");
    if (debug.Get())
        debug->ReportLiveDeviceObjects(
        D3D12_RLDO_DETAIL
        );
    dx::trace("----------------------------------------------\n");
#endif 
}

void DeviceResources::manualCleanup()
{
    renderTarget.manualCleanup();

    fence = nullptr;
    setupCL = nullptr;
    defaultCA = nullptr;
    defaultBA = nullptr;
    defaultCQ = nullptr;
    swapchain = nullptr;
    debug = nullptr;
    device = nullptr;
}


bool DeviceResources::initializeFor(HWND hWnd, bool shader_debugging)
{
	windowHandle = hWnd;

	// Create the device
	// Try to make a hardware device first, fallback to WARP if that fails.
#ifdef _DEBUG
	const D3D12_CREATE_DEVICE_FLAG deviceFlags = D3D12_CREATE_DEVICE_DEBUG;
	(unsigned&)deviceFlags |= shader_debugging 
		? D3D12_CREATE_DEVICE_SHADER_DEBUGGING 
		: D3D12_CREATE_DEVICE_NONE; 
#else
	const D3D12_CREATE_DEVICE_FLAG deviceFlags = D3D12_CREATE_DEVICE_NONE;
#endif

	// create swap chain descriptor
	DXGI_SWAP_CHAIN_DESC descSwapChain;
	ZeroMemory(&descSwapChain, sizeof(descSwapChain));
	descSwapChain.BufferCount = backbufferCount;
	descSwapChain.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	descSwapChain.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	descSwapChain.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
	descSwapChain.OutputWindow = hWnd;
	descSwapChain.SampleDesc.Count = 1;
	descSwapChain.Windowed = TRUE;

	if (driverType == D3D_DRIVER_TYPE_UNKNOWN)
	{
		driverType = D3D_DRIVER_TYPE_HARDWARE;
		if (FAILED(lastError = CreateDeviceAndSwapChain(
			nullptr,
			D3D_DRIVER_TYPE_HARDWARE,
			deviceFlags,
			featureLevel,
			D3D12_SDK_VERSION,
			&descSwapChain,
            IID_PPV_ARGS(swapchain.ReleaseAndGetAddressOf()),
            IID_PPV_ARGS(device.ReleaseAndGetAddressOf())
			)))
		{
			reportComError("CreateDeviceAndSwapChain: ", lastError);

			driverType = D3D_DRIVER_TYPE_WARP;
			if (FAILED(lastError = CreateDeviceAndSwapChain(
				nullptr,
				D3D_DRIVER_TYPE_WARP,
				deviceFlags,
				featureLevel,
				D3D12_SDK_VERSION,
				&descSwapChain,
                IID_PPV_ARGS(swapchain.ReleaseAndGetAddressOf()),
                IID_PPV_ARGS(device.ReleaseAndGetAddressOf())
				)))
			{
				reportComError("CreateDeviceAndSwapChain: ", lastError);
				return false;
			}
		}
	}
	else
	{
		lastError = CreateDeviceAndSwapChain(
			nullptr,
			driverType,
			deviceFlags,
			featureLevel,
			D3D12_SDK_VERSION,
			&descSwapChain,
			IID_PPV_ARGS(swapchain.ReleaseAndGetAddressOf()),
            IID_PPV_ARGS(device.ReleaseAndGetAddressOf())
			);
    }

#if _DEBUG
    if (device.Get())
        lastError = device.As(&debug),
        reportComError("QueryInterface: ", lastError);
#endif 

    lastError = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(defaultCA.ReleaseAndGetAddressOf()));
	reportComError("CreateCommandAllocator: ", lastError);

    lastError = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_BUNDLE, IID_PPV_ARGS(defaultBA.ReleaseAndGetAddressOf()));
	reportComError("CreateCommandAllocator: ", lastError);

    D3D12_COMMAND_QUEUE_DESC queueDesc;
    ZeroMemory(&queueDesc, sizeof(queueDesc));
    queueDesc.Flags = D3D12_COMMAND_QUEUE_NONE;
    queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
    lastError = device->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(defaultCQ.ReleaseAndGetAddressOf()));
	updateDescriptorOffsets();

    lastError = device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, defaultCA.Get(), nullptr, IID_PPV_ARGS(setupCL.ReleaseAndGetAddressOf()));
    reportComError("CreateCommandList: ", lastError);

    fenceU = 0;
    lastError = device->CreateFence(fenceU, D3D12_FENCE_MISC_NONE, IID_PPV_ARGS(&fence));

    renderTarget.renderTargetShaderVisible = false;
    renderTarget.depthStencilShaderVisible = false;
    renderTarget.recreateRenderTargetsFor(*this);
    renderTarget.recreateDepthStencilFor(*this);
    renderTarget.initializeFrameDepthStencilBarriers(setupCL.Get());

	return SUCCEEDED(lastError) 
		&& SUCCEEDED(renderTarget.lastError);
}

void DeviceResources::finalizeResourceSetup()
{
    lastError = setupCL->Close();
    reportComError("CreateCommandList: ", lastError);

    ID3D12CommandList* commandLists[] = { setupCL.Get() };
    defaultCQ->ExecuteCommandLists(1, commandLists);
    ++fenceU;

    lastError = defaultCQ->Signal(fence.Get(), fenceU);
    reportComError("CreateCommandList: ", lastError);
}

void DeviceResources::updateDescriptorOffsets()
{
	if (device.Get())
		descriptorOffsetS = device->GetDescriptorHandleIncrementSize(D3D12_SAMPLER_DESCRIPTOR_HEAP),
		descriptorOffsetRTV = device->GetDescriptorHandleIncrementSize(D3D12_RTV_DESCRIPTOR_HEAP),
		descriptorOffsetDSV = device->GetDescriptorHandleIncrementSize(D3D12_DSV_DESCRIPTOR_HEAP),
		descriptorOffsetCBV = descriptorOffsetUAV = descriptorOffsetSRV = device->GetDescriptorHandleIncrementSize(D3D12_CBV_SRV_UAV_DESCRIPTOR_HEAP);
}

void DeviceResources::setResourceBarriers(
    ID3D12GraphicsCommandList* commandList,
    ID3D12Resource* resource, 
    UINT before, UINT after,
    uint32_t resourceCount,
    uint32_t subresource
    )
{
    D3D12_RESOURCE_BARRIER_DESC barrierDesc = {};

    barrierDesc.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    barrierDesc.Transition.pResource = resource;
    barrierDesc.Transition.Subresource = subresource;
    barrierDesc.Transition.StateBefore = before;
    barrierDesc.Transition.StateAfter = after;
    commandList->ResourceBarrier(resourceCount, &barrierDesc);
}