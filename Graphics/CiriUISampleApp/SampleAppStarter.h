#pragma once


#include <DxUtils.h>
#include <DxTimer.h>
#include <DxSampleApp.h>


namespace ciri
{
	class SampleAppStarter
		: public dx::DxSampleApp
		, public dx::AlignedNew<SampleAppStarter>
	{
	public:
		virtual bool onMain(void);
	};
}
