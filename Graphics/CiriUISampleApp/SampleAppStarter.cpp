#include "SampleAppStarter.h"

#include <CiriWindow.h>
#include <CiriUIWrap.h>
#include <SethUIWindowPosition.h>
#include <DxTimer.h>

#include "Scene.h"


using namespace ciri;

dx::DxSampleApp *dx::DxSampleAppFactory()
{
	return new SampleAppStarter();
}

struct UIWindowPositionDelegates
{
    UIWrap *ui;
    Window::EventArgs args;

    UIWindowPositionDelegates(UIWrap *ui)
        : ui(ui)
    {
        assert(!!ui);
    }

    void onHidden(seth::UIWindowPosition *uiPos)
    {
        dx::trace("ui.delegates.hidden");
        args.windowMessage() = WM_ENTERMENULOOP;
        ui->onMessage(args);
    }

    void onShown(seth::UIWindowPosition *uiPos)
    {
        args.windowMessage() = WM_EXITMENULOOP;
        ui->onMessage(args);
        dx::trace("ui.delegates.shown");
    }
};



//#define _Url "file:///F:/Dev/Vs/Proj/Graphics/Graphics/assets/pages/AnimatedHeaderBackgrounds/AnimatedHeaderBackgrounds/index2.html"
//#define _Url "file:///F:/Dev/Vs/Proj/Graphics/Graphics/assets/pages/AnimatedHeaderBackgrounds/AnimatedHeaderBackgrounds/index2.html"
//#define _Url "file:///F:/Dev/Vs/Proj/Graphics/Graphics/assets/pages/HexaFlip/index.html"
#define _Url "file:///F:/Dev/Vs/Proj/Graphics/Graphics/assets/pages/PageTransitions/PageTransitions/index.html"

bool SampleAppStarter::onMain(void)
{
    seth::Scene scene;
    scene.initialize();



	Window sceneWindow, uiWindow;
	sceneWindow.initialize(10, 10, 1600, 856, "CiriUISampleApp");
	uiWindow.initializeAsUI(sceneWindow);

	UIWrap ui;
	ui.resolveExternalDependencies();
	ui.initialize(uiWindow.windowHandle, GetModuleHandle(0), _Url);

    seth::UIWindowPosition uiPos;
    uiPos.uiWindow = uiWindow.windowHandle;
    uiPos.mainWindow = sceneWindow.windowHandle;
    // additional configuration for 'uiPos'
    uiPos.initialize();

    UIWindowPositionDelegates uiDelegates(&ui);
    uiPos.shown += _Dx_delegate_to(&uiDelegates, &UIWindowPositionDelegates::onShown);
    uiPos.hidden += _Dx_delegate_to(&uiDelegates, &UIWindowPositionDelegates::onHidden);

	Window::EventArgs frameMove;
	Window::EventArgs translatedArgs;
	translatedArgs.windowMessage() = WM_CREATE;
	ui.onMessage(translatedArgs);

	SetFocus(sceneWindow.windowHandle);

    dx::sim::BasicTimer timer;
    timer.reset();

	sceneWindow.mainLoopWithUI(uiWindow,
	[&](Window::EventArgs const &args)
	{
		switch (args.windowMessage())
		{
		case dx::Message::KeyReleased:
			args.translateTo(translatedArgs);

            switch (translatedArgs.keyCode())
            {
            case dx::VirtualKey::Escape: sceneWindow.sendCloseMessage();
                return Window::EventArgs::kEventCallbackResult_Quit;
            case dx::VirtualKey::H: uiPos.hideUI();
                return Window::EventArgs::kEventCallbackResult_Processed;
            case dx::VirtualKey::S: uiPos.showUI();
                return Window::EventArgs::kEventCallbackResult_Processed;
            default:
                return Window::EventArgs::kEventCallbackResult_Ignored;
            }
		default:
			return Window::EventArgs::kEventCallbackResult_Ignored;
		}
	}, 
	[&](Window::EventArgs const &args)
	{
		return false;
	}, 
	[&](void)
	{
        timer.update();

        ui.onFrameMove(frameMove);
        uiPos.onFrameMove(timer.elapsed);
	},
	[&](void)
	{
		Window::EventArgs translatedArgs;
		translatedArgs.windowMessage() = WM_CLOSE;
		ui.onMessage(translatedArgs);
		ui.release();
	});

	return false;
}
