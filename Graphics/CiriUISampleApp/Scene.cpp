#include "Scene.h"
using namespace seth;


void Scene::initialize()
{
    aega::Scene scene;
    seth::SceneBuild sceneBuild;
    aega::SceneDeserialize deserializer;

    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/sphere_r1.mb.aega");
    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/plane-2x2.ma.aega");
    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/spidey_v2.1/KevinRigv1_0.mb.aega");
    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/randomPolies.hard.ma.aega");
    deserializer.deserializeTo(&scene, "D:/Dev/Ma/IronMan1_0.0001.ma.aega");

    sceneBuild.buildFrom(scene, "(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)");
    sceneBuild.transformTree.rootNode->scalingV *= 0.01f;
    sceneBuild.transformTree.rootNode->notifyPoseOS();
    sceneBuild.transformTree.rootNode->notifyPoseWS();
    sceneBuild.transformTree.rootNode->notifyChildPosesWS();
    sceneBuild.transformTree.rootNode->evaluatePoseWS();
    sceneBuild.transformTree.rootNode->evaluateChildPosesWS();
}

void Scene::onFrameMove()
{

}

