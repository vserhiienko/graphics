
struct GSInput
{
  uint vertexIndex : Nyx_VertexIndex;
};

struct PSInput
{
  float4 position : SV_Position;
  float3 normal : Nyx_Normal;
  float fresnel : Nyx_Fresnel;
  float area[2] : Nyx_Area;
};

cbuffer GridParameters : register(cb0)
{
  /*
  .x = width
  .y = height
  .z = resolutionX
  .w = resolutionY
  */
  row_major float4x4 g_surfacePose;
  row_major float4x4 g_normalTransform;
  row_major float4x4 g_cameraView;
  row_major float4x4 g_cameraProjection;
  uint4 g_gridParams;
};

Texture2D<float> g_u : register(t0); // height field
Texture2D<float> g_v : register(t1); // velocity field
Texture2D<float2> g_g : register(t2); // gradient field
SamplerState g_linearSampler : register(s0); // texture sampler

inline float sq(in const float x) { return dot(x, x); }

inline uint gridWidth(void) { return g_gridParams.x; }
inline uint gridHeight(void) { return g_gridParams.y; }
inline uint gridResolutionX(void) { return g_gridParams.z; }
inline uint gridResolutionY(void) { return g_gridParams.w; }
inline float getHeight(in const float2 texPos) { return g_u.SampleLevel(g_linearSampler, texPos, 0); }
inline float getVelocity(in const float2 texPos) { return g_v.SampleLevel(g_linearSampler, texPos, 0); }
inline float2 getGradient(in const float2 texPos) { return g_g.SampleLevel(g_linearSampler, texPos, 0); }

inline float triangleArea(float3 a, float3 b, float3 c)
{
  float3 x = b - a;
  float3 y = c - a;
  return abs(cross(x, y)) * 0.5f;

  /*float s1 = abs(x.y * y.z - x.z * y.y);
  float s2 = abs(x.z * y.x - x.x * y.z);
  float s3 = abs(x.x * y.y - x.y * y.x);
  s1 *= s1; s2 *= s2; s3 *= s3;
  float s = 0.5f * sqrt(s1 + s2 + s3);
  return s;  */
}

[maxvertexcount(4)]
void main(
  in point GSInput input[1],
  inout TriangleStream<PSInput> output
  )
{
  uint vertexIndex, w, h;

  vertexIndex = input[0].vertexIndex;
  w = gridWidth();
  h = gridHeight();

  //printf("gs:vi: %u", vertexIndex);
  //printf("gs:w: %u", w);
  //printf("gs:h: %u", h);

  float2 gridRes;
  float2 gridDims;
  gridDims.x = float(w);
  gridDims.y = float(h);
  gridRes.x = float(gridResolutionX());
  gridRes.y = float(gridResolutionY());
  //printf("gs:resx: %f", gridRes.x);
  //printf("gs:resy: %f", gridRes.y);

  float2 offset, delta;
  offset = gridRes * -0.5f;
  delta = gridRes / gridDims;

  uint4 quadVertexIndices;
  quadVertexIndices.x = vertexIndex;
  quadVertexIndices.y = vertexIndex + 1;
  quadVertexIndices.z = vertexIndex + w;
  quadVertexIndices.w = vertexIndex + w + 1;

  uint i;
  float fresnel;
  float2 texPos[4];
  float2 gridPos;
  float3 normal;
  float4 vertexPos[4];
  float4 vertexClip;
  float3 viewDirection;

  PSInput outputElement;


  [unroll] for (i = 0; i < 4; i++)
  {
    vertexIndex = quadVertexIndices[i];
    gridPos.x = float(vertexIndex % w);
    gridPos.y = float(vertexIndex / h);
    texPos[i] = gridPos / gridDims;

    vertexPos[i].xz = gridPos * delta;
    vertexPos[i].xz += offset;
    //vertexPos.y = getVelocity(texPos[i]);
    vertexPos[i].y = getHeight(texPos[i]);
    //vertexPos[i].y = 0.1f;
    vertexPos[i].w = 1.0f;
  }

  // todo: calculate area

  float a0 = delta.x * delta.y;

  float a1 = triangleArea(
    vertexPos[0].xyz,
    vertexPos[1].xyz,
    vertexPos[2].xyz
    );
  float a2 = triangleArea(
    vertexPos[3].xyz,
    vertexPos[2].xyz,
    vertexPos[1].xyz
    );
  float a = a1 + a2;

  [unroll] for (i = 0; i < 4; i++)
  {
    normal.xz = getGradient(texPos[i]);
    normal.y = 1.0f;
    normal = normalize(normal);

    //vertexPos[i].y = 0.1f;
    vertexPos[i] = mul(vertexPos[i], g_surfacePose);
    vertexPos[i] = mul(vertexPos[i], g_cameraView);
    vertexClip = mul(vertexPos[i], g_cameraProjection);

    normal = mul(normal, (float3x3)g_normalTransform);
    normal = normalize(normal);

    viewDirection = -normalize(vertexPos[i].xyz);

    //fresnel = getHeight(texPos[i]);
    //fresnel = getVelocity(texPos);
    fresnel = 0.12f + (0.88f * sq(1.0f - abs(dot(normal, viewDirection))));

    outputElement.normal = normal.xyz;
    outputElement.fresnel = fresnel;
    outputElement.position = vertexClip;
    outputElement.area[0] = a0;
    outputElement.area[1] = a;

    output.Append(outputElement);
  }

  output.RestartStrip();
}