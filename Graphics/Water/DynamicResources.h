#pragma once
#include "pch.h"

namespace nyx
{
  class DynamicTexture
  {
  public:
    dx::DxDeviceResources *d3d;

  protected:
    unsigned pitch;
    unsigned slicePitch;
    dx::Texture2 surfaces[2];
    dx::Texture2 stagingSurface;
    dx::SRView shaderResourceViews[2];
    dx::CTexture2Description surfaceDesc;
    dx::CTexture2Description stagingSurfaceDesc;

  public:
    /*
    ensure width and height are multiple 16
    float dxgi formats:
    DXGI_FORMAT_R32_FLOAT - float // 4 bytes
    DXGI_FORMAT_R32G32_FLOAT - float2, dx::Vector2 // 8 bytes
    DXGI_FORMAT_R32G32B32_FLOAT - float3, dx::Vector3 // 12 bytes
    DXGI_FORMAT_R32G32B32A32_FLOAT - float4, dx::Vector4 // 16 bytes
    DXGI_FORMAT_R16_FLOAT - half // 2 bytes
    DXGI_FORMAT_R16G16_FLOAT - half2 // 4 bytes
    DXGI_FORMAT_R16G16B16A16_FLOAT - half4 //  8 bytes
    */
    DynamicTexture(dx::DxDeviceResources *d3d, dx::DxgiFormat format);
    void updateResource(void *data, unsigned width, unsigned height);
    dx::SRView &getShaderResourceView(void);

  protected:
    void swapSurfaces(void);
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);
    void onSurfaceResized(void *data, unsigned width, unsigned height);

  };
}