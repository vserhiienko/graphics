#pragma once
#include "Camera.h"
#include "FluidSurface.h"
#include "DynamicResources.h"

namespace nyx
{
  class FluidSurfaceRenderer
  {
  public:
    struct PerFrameGS
    {
      dx::XMMATRIX surfacePose;
      dx::XMMATRIX normalTransform;
      dx::XMMATRIX cameraView;
      dx::XMMATRIX cameraProj;
      dx::XMUINT4 gridParams;
    };

  public:
    dx::DxDeviceResources *d3d;

  public:
    PerFrameGS perFrameSource;

    dx::VShader vertexShader;
    dx::PShader pixelShader;
    dx::GShader geometryShader;
    dx::BlendState blendState;
    dx::SamplerState samplerState;
    dx::RasterizerState rasterizerState;
    dx::DepthStencilState depthStencilState;

    nyx::DynamicTexture heightResourceGS;
    nyx::DynamicTexture velocityResourceGS;
    nyx::DynamicTexture gradientResourceGS;
    dx::ConstantBuffer<PerFrameGS> perFrameResourceGS;

  public:
    FluidSurfaceRenderer(dx::DxDeviceResources*);
    void createDeviceResources(void);
    void render(Camera const &, FluidSurface const &);

  protected:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);


  };

}
