#pragma once
#include "pch.h"

namespace nyx
{
  namespace utils
  {
    template <typename _Ty> inline static _Ty min(_Ty const &a, _Ty const &b) { return (a < b) ? a : b; }
    template <typename _Ty> inline static _Ty max(_Ty const &a, _Ty const &b) { return (a > b) ? a : b; }
    template <typename _Ty> inline static _Ty lerp(_Ty const &a, _Ty const &b, _Ty const &t) { return a + t * (b - a); }
    template <typename _Ty> inline static _Ty clamp(_Ty const &x, _Ty const &a, _Ty const &b) { return utils::min(utils::max(x, a), b); }
  }

  template <typename _Ty> class Array2View
  {
  public:
    unsigned w, h;
    _Ty border;
    _Ty *data;
    _Ty **rows;
    Array2View()
      : w(0)
      , h(0)
      , data(0)
      , rows(0)
    {
    }
  };

  template <typename _Ty> class Array2 : public Array2View < _Ty >
  {
  public:
    typedef Array2View<_Ty> View;

  public:
    Array2() : View() { }
    ~Array2() { destroyArray(); }
    Array2(unsigned width, unsigned height) : Array2() { initializeArray(width, height); }

    inline void initializeArray(unsigned width, unsigned height)
    {
      destroyArray();

      View::w = width;
      View::h = height;
      rows = new _Ty*[View::h];
      data = new _Ty[View::w * View::h];
      for (unsigned i = 0; i < View::h; i++)
        View::rows[i] = &View::data[i * View::w];
    }

    inline void destroyArray()
    {
      if (View::data) delete[] View::data;
      if (View::rows) delete[] View::rows;
      View::w = 0, View::h = 0;
    }
    inline void zeroMemory()
    {
      memset((void*)data, 0, View::w * View::h * sizeof(_Ty));
    }

  public:
    inline void set(unsigned x, unsigned y, _Ty v) { rows[y][x] = v; }
    inline void setSafe(unsigned x, unsigned y, _Ty v) { if ((x < w) && (y < h)) rows[y][x] = v; }

  public:
    inline _Ty &get(unsigned x, unsigned y) 
    {
      return View::rows[y][x];
    }
    inline _Ty const &get(unsigned x, unsigned y) const
    { 
      return View::rows[y][x];
    }
    inline _Ty &clampGet(unsigned x, unsigned y)
    {
      nyx::utils::clamp(x, 0u, View::w - 1u);
      nyx::utils::clamp(y, 0u, View::h - 1u);
      return View::rows[y][x];
    }
    inline _Ty const &clampGet(unsigned x, unsigned y) const
    { 
      nyx::utils::clamp(x, 0u, View::w - 1u);
      nyx::utils::clamp(y, 0u, View::h - 1u);
      return View::rows[y][x]; 
    }
    inline _Ty const &getSafe(unsigned x, unsigned y) const
    {
      if ((x < (View::w)) && (y < (View::h))) return View::rows[y][x];
      else return View::border;
    }
    inline _Ty const &lerpGet(float x, float y) const
    {
      assert(x >= 0.0f && y >= 0.0f);
      unsigned ix = (unsigned)floorf(x), iy = (unsigned)floorf(y); float u = x - ix, v = y - iy;
      return  nyx::utils::lerp(nyx::utils::lerp(clampGet(ix, iy), clampGet(ix + 1, iy), u), nyx::utils::lerp(clampGet(ix, iy + 1), clampGet(ix + 1, iy + 1), u), v);
    }

  public:
    inline View &getView() { return (*this); }
    inline View const &getView() const { return (*this); }


  };
}