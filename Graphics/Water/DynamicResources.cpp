#include "pch.h"
#include "DynamicResources.h"


nyx::DynamicTexture::DynamicTexture(dx::DxDeviceResources *d3d, dx::DxgiFormat format)
  : surfaceDesc(format, 0, 0, 1, 1, D3D11_BIND_SHADER_RESOURCE, D3D11_USAGE_DEFAULT)
  , stagingSurfaceDesc(format, 0, 0, 1, 1, 0, D3D11_USAGE_STAGING, D3D11_CPU_ACCESS_WRITE)
  , d3d(0)
  , pitch(0)
  , slicePitch(0)
{
  onDeviceRestored(d3d);
}

void nyx::DynamicTexture::updateResource(void *buffer, unsigned width, unsigned height)
{
  if (surfaceDesc.Width != width || surfaceDesc.Height != height)
  {
    onSurfaceResized(buffer, width, height);
  }
  else
  {
    dx::Box dirtyRegion;
    dx::MappedSubresource mapped;

    dirtyRegion.front = 0;
    dirtyRegion.back = 1;
    dirtyRegion.left = 0;
    dirtyRegion.top = 0;
    dirtyRegion.right = width;
    dirtyRegion.bottom = height;

    if (SUCCEEDED(d3d->context->Map(stagingSurface.Get(), 0, D3D11_MAP_WRITE, 0, &mapped)))
    {
      assert(pitch == mapped.RowPitch);
      assert(slicePitch == mapped.DepthPitch);
      memcpy(mapped.pData, buffer, slicePitch);
      d3d->context->Unmap(stagingSurface.Get(), 0);
      d3d->context->CopySubresourceRegion(surfaces[0].Get(), 0, 0, 0, 0, stagingSurface.Get(), 0, &dirtyRegion);
    }

    swapSurfaces();
  }
}

void nyx::DynamicTexture::swapSurfaces()
{
  surfaces[0].Swap(surfaces[1]);
  shaderResourceViews[0].Swap(shaderResourceViews[1]);
}

void nyx::DynamicTexture::onSurfaceResized(void *buffer, unsigned width, unsigned height)
{
  if (width > 0 && height > 0)
  {
    surfaceDesc.Width = width;
    surfaceDesc.Height = height;
    dx::roundUp(surfaceDesc.Width, 16u);
    dx::roundUp(surfaceDesc.Height, 16u);

    stagingSurfaceDesc.Width = surfaceDesc.Width;
    stagingSurfaceDesc.Height = surfaceDesc.Height;

    switch (stagingSurfaceDesc.Format)
    {
    case DXGI_FORMAT_R16_FLOAT: pitch = surfaceDesc.Width * 2; break;
    case DXGI_FORMAT_R32_FLOAT: pitch = surfaceDesc.Width * 4; break;
    case DXGI_FORMAT_R16G16_FLOAT: pitch = surfaceDesc.Width * 4; break;
    case DXGI_FORMAT_R32G32_FLOAT: pitch = surfaceDesc.Width * 8; break;
    case DXGI_FORMAT_R16G16B16A16_FLOAT: pitch = surfaceDesc.Width * 8; break;
    case DXGI_FORMAT_R32G32B32_FLOAT: pitch = surfaceDesc.Width * 12; break;
    case DXGI_FORMAT_R32G32B32A32_FLOAT: pitch = surfaceDesc.Width * 16; break;
    default: assert(false); break;
    }

    slicePitch = pitch * surfaceDesc.Height;

    dx::Subresource subresource;
    dx::Subresource *subresourceRef = 0;

    if (buffer)
    {
      subresource.pSysMem = buffer;
      subresource.SysMemPitch = pitch;
      subresource.SysMemSlicePitch = slicePitch;
      subresourceRef = &subresource;
    }

    dx::HResult result;
    result = d3d->device->CreateTexture2D(&surfaceDesc, subresourceRef, surfaces[0].ReleaseAndGetAddressOf());
    _Dx_hresult_assert(result);
    result = d3d->device->CreateTexture2D(&surfaceDesc, subresourceRef, surfaces[1].ReleaseAndGetAddressOf());
    _Dx_hresult_assert(result);
    result = d3d->device->CreateTexture2D(&stagingSurfaceDesc, subresourceRef, stagingSurface.ReleaseAndGetAddressOf());
    _Dx_hresult_assert(result);

    dx::CSRVDescription
      viewDesc0(surfaces[0].Get(), D3D11_SRV_DIMENSION_TEXTURE2D, surfaceDesc.Format),
      viewDesc1(surfaces[1].Get(), D3D11_SRV_DIMENSION_TEXTURE2D, surfaceDesc.Format);
    result = d3d->device->CreateShaderResourceView(surfaces[0].Get(), &viewDesc0, shaderResourceViews[0].ReleaseAndGetAddressOf());
    _Dx_hresult_assert(result);
    result = d3d->device->CreateShaderResourceView(surfaces[1].Get(), &viewDesc1, shaderResourceViews[1].ReleaseAndGetAddressOf());
    _Dx_hresult_assert(result);

    dx::trace("nyx:dytex: resized");
  }
}

void nyx::DynamicTexture::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void nyx::DynamicTexture::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  if (restoredDevice)
  {
    d3d = restoredDevice;
    d3d->deviceLost += _Dx_delegate_to(this, &DynamicTexture::onDeviceLost);
    d3d->deviceRestored += _Dx_delegate_to(this, &DynamicTexture::onDeviceRestored);
  }
}

dx::SRView &nyx::DynamicTexture::getShaderResourceView()
{
  return shaderResourceViews[1]; // returns updated resource
}
