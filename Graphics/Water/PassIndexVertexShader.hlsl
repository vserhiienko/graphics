
struct VSInput
{
  uint vertexId : SV_VertexID;
};

struct GSInput
{
  uint vertexIndex : Nyx_VertexIndex;
};

GSInput main(in VSInput input)
{
  GSInput output;
  output.vertexIndex = input.vertexId;
  return output;
}