#pragma once
#include "Billboard.h"
#include "FluidSurfaceRenderer.h"

namespace nyx
{
  class WaterSampleApp
    : public dxui::DxUISampleApp
    , public dx::AlignedNew<WaterSampleApp>
  {
  public:
    std::string uiUrl;
    dx::sim::BasicTimer timer;

    nyx::OrbitCamera camera;
    nyx::CameraResources cameraResources;
    nyx::BillboardRenderer billboardRenderer;
    nyx::BillboardResources billboardResources;

    nyx::FluidSurface fluidSurface;
    nyx::FluidSurfaceRenderer fluidSurfaceRenderer;

    bool pauseSim;
    bool mousePressed;
    bool mouseRPressed;
    dx::Vector2 previousMousePos;

  public:
    WaterSampleApp();
    virtual ~WaterSampleApp();

  public: // DxSampleApp
    virtual void handlePaint(dx::DxWindow const*);

  public: // DxUISampleApp
    virtual std::string getUIURL(void);
    virtual void initializeSample(void);

  public: // DxUISampleApp
    virtual void sizeChanged(void);
    virtual void onUICreated(void);
    virtual void onUIDestroyed(void);
    virtual void keyReleased(dx::DxGenericEventArgs const&);
    virtual void pointerMoved(dx::DxGenericEventArgs const&);
    virtual void pointerWheel(dx::DxGenericEventArgs const&);
    virtual void pointerLeftPressed(dx::DxGenericEventArgs const&);
    virtual void pointerLeftReleased(dx::DxGenericEventArgs const&);
    virtual void pointerRightPressed(dx::DxGenericEventArgs const&);
    virtual void pointerRightReleased(dx::DxGenericEventArgs const&);

  };
}
