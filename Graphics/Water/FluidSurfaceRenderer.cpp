#include "pch.h"
#include "FluidSurfaceRenderer.h"


nyx::FluidSurfaceRenderer::FluidSurfaceRenderer(dx::DxDeviceResources *d3d)
  : heightResourceGS(d3d, DXGI_FORMAT_R32_FLOAT)
  , velocityResourceGS(d3d, DXGI_FORMAT_R32_FLOAT)
  , gradientResourceGS(d3d, DXGI_FORMAT_R32G32_FLOAT)
{
  onDeviceRestored(d3d);
}

void nyx::FluidSurfaceRenderer::createDeviceResources(void)
{
  assert(d3d != 0);

  dx::SamplerStateDescription samplerDesc;
  dx::RasterizerDescription rasterizerStateDesc;
  dx::BlendStateDescription blendStateDescription;
  dx::DepthStencilDescription depthDisabledStencilDesc;

  dx::DxResourceManager::defaults(samplerDesc);
  samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

  dx::zeroMemory(blendStateDescription);
  blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
  blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
  blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
  blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
  blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
  blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
  blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
  blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;

  dx::zeroMemory(rasterizerStateDesc);
  rasterizerStateDesc.AntialiasedLineEnable = false;
  rasterizerStateDesc.CullMode = D3D11_CULL_BACK;
  rasterizerStateDesc.DepthBias = 0;
  rasterizerStateDesc.DepthBiasClamp = 0.0f;
  rasterizerStateDesc.DepthClipEnable = true;
  rasterizerStateDesc.FillMode = D3D11_FILL_SOLID;
  rasterizerStateDesc.FrontCounterClockwise = true;
  rasterizerStateDesc.MultisampleEnable = false;
  rasterizerStateDesc.ScissorEnable = false;
  rasterizerStateDesc.SlopeScaledDepthBias = 0.0f;

  dx::zeroMemory(depthDisabledStencilDesc);
  dx::DxResourceManager::defaults(depthDisabledStencilDesc);

  dx::HResult result = S_OK;
  dx::DxAssetManager::addSearchPath("Water");

  result = dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"shaders/GridQuadsPixelShader.cso",
    pixelShader.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"shaders/GridQuadsGeometryShader.cso",
    geometryShader.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"shaders/PassIndexVertexShader.cso",
    0, 0, vertexShader.ReleaseAndGetAddressOf(), 0
    ); _Dx_hresult_assert(result);
  result = d3d->device->CreateSamplerState(
    &samplerDesc, samplerState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device.Get()->CreateDepthStencilState(
    &depthDisabledStencilDesc,
    depthStencilState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device.Get()->CreateRasterizerState1(
    &rasterizerStateDesc,
    rasterizerState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device.Get()->CreateBlendState1(
    &blendStateDescription,
    blendState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = perFrameResourceGS.create(
    d3d->device.Get(), "nyx:grid:gs"
    ); _Dx_hresult_assert(result);
}
void nyx::FluidSurfaceRenderer::render(Camera const &camera, FluidSurface const &water)
{
  static const auto mask = 0xffffffffu;
  static const FLOAT factor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
  static const UINT32 offsets[] = { 0u };
  static const UINT32 stencilRef = 0x01u;
  static dx::ISRView *nullSRV[3] = { 0, 0, 0 };

  assert(d3d != 0);
  auto context = d3d->context.Get();

  perFrameSource.gridParams.x = water.gridWidth;
  perFrameSource.gridParams.y = water.gridHeight;
  perFrameSource.gridParams.z = water.gridResX;
  perFrameSource.gridParams.w = water.gridResY;
  perFrameSource.surfacePose = water.pose;
  perFrameSource.cameraView = camera.view;
  perFrameSource.cameraProj = camera.projection;
  perFrameSource.normalTransform = camera.view * water.pose;
  //perFrameSource.normalTransform = camera.view * water.pose;
  perFrameSource.normalTransform = dx::XMMatrixInverse(0, perFrameSource.normalTransform);
  perFrameSource.normalTransform = dx::XMMatrixTranspose(perFrameSource.normalTransform);
  perFrameResourceGS.setData(context, perFrameSource);

  heightResourceGS.updateResource(water.u.data, water.gridWidth, water.gridHeight);
  velocityResourceGS.updateResource(water.v.data, water.gridWidth, water.gridHeight);
  gradientResourceGS.updateResource(water.g.data, water.gridWidth, water.gridHeight);

  context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
  context->IASetInputLayout(0);

  context->VSSetShader(vertexShader.Get(), nullptr, 0u);
  context->GSSetShader(geometryShader.Get(), nullptr, 0u);
  context->PSSetShader(pixelShader.Get(), nullptr, 0u);

  context->GSSetSamplers(0u, 1u, samplerState.GetAddressOf());
  context->GSSetConstantBuffers(0u, 1u, perFrameResourceGS.getBufferAddress());
  context->GSSetShaderResources(0u, 1u, heightResourceGS.getShaderResourceView().GetAddressOf());
  context->GSSetShaderResources(1u, 1u, velocityResourceGS.getShaderResourceView().GetAddressOf());
  context->GSSetShaderResources(2u, 1u, gradientResourceGS.getShaderResourceView().GetAddressOf());
  context->OMSetDepthStencilState(depthStencilState.Get(), stencilRef);
  context->OMSetBlendState(blendState.Get(), factor, mask);
  context->RSSetState(rasterizerState.Get());

  context->Draw((water.gridWidth - 1) * (water.gridHeight - 1) - 1, 0);

  context->GSSetShaderResources(0u, 3u, nullSRV);
  context->PSSetShader(nullptr, nullptr, 0u);
  context->GSSetShader(nullptr, nullptr, 0u);
  context->VSSetShader(nullptr, nullptr, 0u);
}

void nyx::FluidSurfaceRenderer::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}
void nyx::FluidSurfaceRenderer::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  if (restoredDevice)
  {
    d3d = restoredDevice;
    d3d->deviceLost += _Dx_delegate_to(this, &FluidSurfaceRenderer::onDeviceLost);
    d3d->deviceRestored += _Dx_delegate_to(this, &FluidSurfaceRenderer::onDeviceRestored);
    createDeviceResources();
  }
}