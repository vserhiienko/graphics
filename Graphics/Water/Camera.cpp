#include "Camera.h"

nyx::Camera::Camera()
{
  fovY = dx::XM_PIDIV2;
  screenW = 0.0f;
  screenH = 0.0f;
  farPlane = 100.0f;
  nearPlane = 0.01f;
  upDirection = DirectX::XMVectorSet(0.f, 1.f, 0.f, 1.f);
  eyePosition = DirectX::XMVectorSet(0.f, 0.f, -1.f, 1.f);
  focusPosition = DirectX::XMVectorSet(0.f, 0.f, +1.f, 1.f);
}

void nyx::Camera::setScreenParams(
  float w,
  float h,
  float fov,
  float n,
  float f
  )
{
  fovY = fov;
  screenW = w;
  screenH = h;
  farPlane = f;
  nearPlane = n;
  updateProjection();

  updateView();
}

void nyx::Camera::updateView()
{
  view = DirectX::XMMatrixLookAtLH(
    eyePosition, focusPosition, upDirection
    );
}

void nyx::Camera::updateProjection()
{
  projection = DirectX::Matrix::CreatePerspectiveFieldOfView(
    fovY, screenW / screenH, nearPlane, farPlane
    );
}

nyx::OrbitCamera::OrbitCamera()
{
  pitchAccA = 0.0f;
  yawAccA = 0.0f;
  pitchA = 0.0f;
  yawA = 0.0f;
}

void nyx::OrbitCamera::initialize()
{
  eyeDisplacement = eyePosition;

  updateView();
}

void nyx::OrbitCamera::onPointerInput(
  float dx, float dy, float zoom
  )
{
  dx::Vector2 angularDelta = dx::Vector2(dx, dy);
  angularDelta *= 0.005f;
  zoom *= 0.005f;

  pitchA += angularDelta.y;
  yawA += angularDelta.x;

  pitchQ.w = cosf(pitchA * 0.50f);
  pitchQ.x = sinf(pitchA * 0.50f);
  yawQ.w = cosf(yawA * 0.50f);
  yawQ.y = sinf(yawA * 0.50f);

  eyeDisplacement.y += zoom;
  focusDisplacement = dx::XMVector3Rotate(eyeDisplacement, pitchQ);
  focusDisplacement = dx::XMVector3Rotate(focusDisplacement, yawQ);
  eyePosition = focusPosition + focusDisplacement;

  updateView();
}

nyx::CameraResources::CameraResources(dx::DxDeviceResources *device) : d3d(device)
{
  assert(d3d != 0);
  d3d->deviceLost += _Dx_delegate_to(this, &CameraResources::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &CameraResources::onDeviceRestored);
  onDeviceRestored(d3d);
}

void nyx::CameraResources::update(Camera const &data)
{
  assert(d3d != 0);
  perFrameSource.view = data.view;
  perFrameSource.projection = data.projection;
  perFrameResource.setData(d3d->context.Get(), perFrameSource);
}

void nyx::CameraResources::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void nyx::CameraResources::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  perFrameResource.create(d3d->device.Get(), "nyx:camera:cb");
}

