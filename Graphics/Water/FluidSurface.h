#pragma once

#include "pch.h"
#include "Arrays.h"

namespace nyx
{
  class FluidSurface
  {
    float c2, h2;
    unsigned gridMaxX, gridMaxY;
    Array2<float> *currentHeightFieldRef;
    Array2<float> *previousHeightFieldRef;

    void updateGradients(void);
    void updateGradientsParallel(void);
    void swapHeightFieldRefs(void);

  public:
    float damping;
    dx::Matrix pose;
    unsigned gridResX, gridResY;
    unsigned gridWidth, gridHeight;
    Array2<float> velocityField, &v;
    Array2<float> heightFields[2], &u, &u0;
    Array2<dx::Vector2> gradients, &g;

  public:
    FluidSurface(void);
    void initializeSurface(unsigned width, unsigned height, float damping);
    void applyForces(dx::Vector2 gridLocation, float size, float strength);

    // http://www.matthiasmueller.info/talks/GDC2008.pdf

    void simulateParallel(float elapsedFrameTime);
    void simulate(float elapsedFrameTime);
    void simulate2(float elapsedFrameTime);

  };
}