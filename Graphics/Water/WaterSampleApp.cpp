#include "pch.h"
#include "WaterSampleApp.h"

#include <INIReader.h>

static inline float random(float min, float max)
{
  return float(rand()) / float(RAND_MAX) * (max - min) + min;
}

dx::DxSampleApp *dx::DxSampleAppFactory()
{
  return new nyx::WaterSampleApp();
}

nyx::WaterSampleApp::WaterSampleApp()
  : DxUISampleApp(true, true, D3D_DRIVER_TYPE_HARDWARE)
  //: DxUISampleApp(true, true, D3D_DRIVER_TYPE_REFERENCE)
  , camera()
  , fluidSurface()
  , fluidSurfaceRenderer(&deviceResources)
  , billboardRenderer(&deviceResources)
  , billboardResources(&deviceResources)
  , cameraResources(&deviceResources)
  , mousePressed(false)
  , pauseSim(false)
{
  INIReader iniReader("Water.ini");
  uiUrl = iniReader.Get("WaterUI", "appUiUrl", _Water_UIURL);
}

nyx::WaterSampleApp::~WaterSampleApp()
{

}

std::string nyx::WaterSampleApp::getUIURL()
{
  return uiUrl;
}

void nyx::WaterSampleApp::initializeSample()
{
  dx::trace("nyx:app: initialize sample");
  window.setWindowTitle("Water");

  camera.setScreenParams(
    deviceResources.actualRenderTargetSize.Width,
    deviceResources.actualRenderTargetSize.Height
    );
  camera.eyePosition.x = 0.1f;
  camera.eyePosition.y = 24.0f;
  camera.eyePosition.z = 0.1f;
  camera.initialize(); // orbit

  fluidSurface.gridResX = 10.0f;
  fluidSurface.gridResY = 10.0f;
  //fluidSurface.initializeSurface(64, 64, 1.0f);
  //fluidSurface.initializeSurface(128, 128, 1.0f);
  fluidSurface.initializeSurface(256, 256, 0.99f);
  //fluidSurface.initializeSurface(512, 512, 1.0f);

  timer.reset();
}

void nyx::WaterSampleApp::handlePaint(dx::DxWindow const*)
{
  static unsigned frameIndex = 0;
  static const unsigned printFrameIndex = 512;
  static const unsigned maxFrameIndex = printFrameIndex * 10;
  static const float timer_elapsed = 1.0f / 60.0f;

  timer.update();
  frameIndex = ++frameIndex % maxFrameIndex;
  if (frameIndex % printFrameIndex == 0)
    dx::trace("nyx:app: fps %d", timer.framesPerSecond);

  /// update
  // update scene
  cameraResources.update(camera);

  if (mouseRPressed)
    fluidSurface.applyForces(
    dx::Vector2(random(16, fluidSurface.gridWidth - 16), random(16, fluidSurface.gridWidth - 16)),
    random(6, 8), random(0.008f, 0.8f)
    );

  if (!pauseSim)
    fluidSurface.simulateParallel(timer_elapsed);
  //fluidSurface.simulate(timer_elapsed);

  // ...
  // update ui
  dxui::DxUIClient::updateApplication();
  updateUI(timer_elapsed);

  /// draw

  beginScene();
  // render scene
  billboardRenderer.render(cameraResources, billboardResources);
  fluidSurfaceRenderer.render(camera, fluidSurface);
  // ...
  // render ui
  renderUI();
  endScene();
}

void nyx::WaterSampleApp::sizeChanged()
{

}

void nyx::WaterSampleApp::onUICreated()
{

}

void nyx::WaterSampleApp::onUIDestroyed()
{
}

void nyx::WaterSampleApp::keyReleased(dx::DxGenericEventArgs const &args)
{
  switch (args.keyCode())
  {
  case dx::VirtualKey::Escape: window.close(); break;
  case dx::VirtualKey::P: pauseSim = !pauseSim; break;
  default: {} break;
  }

}

void nyx::WaterSampleApp::pointerMoved(dx::DxGenericEventArgs const &args)
{
  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  dx::Vector2 mouseDelta = mousePos - previousMousePos;

  if (mousePressed)
    camera.onPointerInput(mouseDelta.x, mouseDelta.y, 0);

  previousMousePos = mousePos;
}

void nyx::WaterSampleApp::pointerLeftPressed(dx::DxGenericEventArgs const &args)
{
  mousePressed = true;
}

void nyx::WaterSampleApp::pointerLeftReleased(dx::DxGenericEventArgs const &args)
{
  mousePressed = false;
}

void nyx::WaterSampleApp::pointerWheel(dx::DxGenericEventArgs const &args)
{
  if (mousePressed)
    camera.onPointerInput(0, 0, args.wheelDelta());
}

void nyx::WaterSampleApp::pointerRightPressed(dx::DxGenericEventArgs const&)
{
  mouseRPressed = true;
}

void nyx::WaterSampleApp::pointerRightReleased(dx::DxGenericEventArgs const&)
{
  mouseRPressed = false;
}