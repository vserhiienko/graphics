#include <DxUtils.h>
#include <DirectXColors.h>
#include "Billboard.h"


nyx::BillboardResources::BillboardResources(dx::DxDeviceResources *device)
  : d3d(device)
{
  assert(d3d != 0);

  resX = 1000;
  resY = 1000;
  lenX = 500;
  lenY = 500;
  d3d->deviceLost += _Dx_delegate_to(this, &BillboardResources::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &BillboardResources::onDeviceRestored);
  onDeviceRestored(d3d);
}

void nyx::BillboardResources::createDeviceResources()
{
  assert(d3d != 0);

  perFrameSource.pose = dx::Matrix::Identity();
  perFrameSource.color = (dx::XMVECTOR)(dx::Vector4)0.2f;
  perFrameSource.color.w = 0.5f;

  upY = true;
  auto deltaX = lenX / resX;
  auto deltaY = lenY / resY;

  auto x = (lenX)* -0.5f;
  auto y = (lenY)* -0.5f;

  auto xSize = (uint32_t)(lenX / deltaX);
  auto ySize = (uint32_t)(lenY / deltaY);

  std::vector<uint16_t> indices;
  std::vector<dx::Vector4> vertices;

  for (unsigned i = 0; i < xSize; i++)
  {
    auto vi = (uint16_t)vertices.size();

    if (upY)
    {
      vertices.push_back(dx::Vector4(x, 0.0f, +y, 1.0f));
      vertices.push_back(dx::Vector4(x, 0.0f, -y, 1.0f));
    }
    else
    {
      vertices.push_back(dx::Vector4(x, +y, 0.0f, 1.0f));
      vertices.push_back(dx::Vector4(x, -y, 0.0f, 1.0f));
    }

    indices.push_back(vi + 0);
    indices.push_back(vi + 1);
    x += deltaX;
  }

  if (upY)
  {
    vertices.push_back(dx::Vector4(x, 0.0f, +y, 1.0f));
    vertices.push_back(dx::Vector4(x, 0.0f, -y, 1.0f));
  }
  else
  {
    vertices.push_back(dx::Vector4(x, +y, 0.0f, 1.0f));
    vertices.push_back(dx::Vector4(x, -y, 0.0f, 1.0f));
  }

  x = lenX * -0.5f;
  y = lenY * -0.5f;
  for (unsigned i = 0; i < ySize; i++)
  {
    auto hi = (uint16_t)vertices.size();

    if (upY)
    {
      vertices.push_back(dx::Vector4(+x, 0.0f, y, 1.0f));
      vertices.push_back(dx::Vector4(-x, 0.0f, y, 1.0f));
    }
    else
    {
      vertices.push_back(dx::Vector4(+x, y, 0.0f, 1.0f));
      vertices.push_back(dx::Vector4(-x, y, 0.0f, 1.0f));
    }

    indices.push_back(hi + 0);
    indices.push_back(hi + 1);
    y += deltaY;
  }

  if (upY)
  {
    vertices.push_back(dx::Vector4(+x, 0.0f, y, 1.0f));
    vertices.push_back(dx::Vector4(-x, 0.0f, y, 1.0f));
  }
  else
  {
    vertices.push_back(dx::Vector4(+x, y, 0.0f, 1.0f));
    vertices.push_back(dx::Vector4(-x, y, 0.0f, 1.0f));
  }

  indexCount = indices.size();

  dx::BufferDescription indexBufferDesc;
  dx::BufferDescription vertexBufferDesc;
  dx::zeroMemory(indexBufferDesc);
  dx::zeroMemory(vertexBufferDesc);

  indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
  indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
  indexBufferDesc.ByteWidth = sizeof(uint16_t) * indices.size();

  vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
  vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  vertexBufferDesc.ByteWidth = sizeof(dx::Vector4) * vertices.size();

  D3D11_SUBRESOURCE_DATA indexData;
  D3D11_SUBRESOURCE_DATA vertexData;
  dx::zeroMemory(indexData);
  dx::zeroMemory(vertexData);

  indexData.pSysMem = indices.data();
  vertexData.pSysMem = vertices.data();

  d3d->device->CreateBuffer(&indexBufferDesc, &indexData, indexBuffer.ReleaseAndGetAddressOf());
  d3d->device->CreateBuffer(&vertexBufferDesc, &vertexData, vertexBuffer.ReleaseAndGetAddressOf());

  perFrameResource.create(d3d->device.Get(), "nyx:bill:cb");
  update();
}

void nyx::BillboardResources::update()
{
  perFrameResource.setData(d3d->context.Get(), perFrameSource);
}

void nyx::BillboardResources::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void nyx::BillboardResources::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  createDeviceResources();
}


nyx::BillboardRenderer::BillboardRenderer(dx::DxDeviceResources *device)
  : d3d(device)
{
  assert(d3d != 0);
  d3d->deviceLost += _Dx_delegate_to(this, &BillboardRenderer::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &BillboardRenderer::onDeviceRestored);
  onDeviceRestored(d3d);
}

void nyx::BillboardRenderer::createDeviceResources(void)
{
  assert(d3d != 0);

  dx::SamplerStateDescription samplerDesc;
  dx::RasterizerDescription rasterizerStateDesc;
  dx::BlendStateDescription blendStateDescription;
  dx::DepthStencilDescription depthDisabledStencilDesc;

  dx::DxResourceManager::defaults(samplerDesc);
  samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

  dx::zeroMemory(blendStateDescription);
  blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
  blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
  blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
  blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
  blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
  blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
  blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
  blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;

  dx::zeroMemory(rasterizerStateDesc);
  rasterizerStateDesc.AntialiasedLineEnable = false;
  rasterizerStateDesc.CullMode = D3D11_CULL_BACK;
  rasterizerStateDesc.DepthBias = 0;
  rasterizerStateDesc.DepthBiasClamp = 0.0f;
  rasterizerStateDesc.DepthClipEnable = true;
  rasterizerStateDesc.FillMode = D3D11_FILL_SOLID;
  rasterizerStateDesc.FrontCounterClockwise = false;
  rasterizerStateDesc.MultisampleEnable = false;
  rasterizerStateDesc.ScissorEnable = false;
  rasterizerStateDesc.SlopeScaledDepthBias = 0.0f;

  dx::zeroMemory(depthDisabledStencilDesc);
  dx::DxResourceManager::defaults(depthDisabledStencilDesc);
  depthDisabledStencilDesc.DepthEnable = false;
  depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
  depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
  depthDisabledStencilDesc.StencilEnable = true;
  depthDisabledStencilDesc.StencilReadMask = 0xff;
  depthDisabledStencilDesc.StencilWriteMask = 0xff;
  depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
  depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
  depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
  depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
  depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
  depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
  depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
  depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

  dx::VSInputElementDescription inputLayoutDesc[1] =
  {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  };

  dx::HResult result = S_OK;
  dx::DxAssetManager::addSearchPath("Fishes");

  result = dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"shaders/BillboardPixelShader.cso",
    pixelShader.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"shaders/BillboardVertexShader.cso",
    inputLayoutDesc,
    arraysize(inputLayoutDesc),
    vertexShader.ReleaseAndGetAddressOf(),
    inputLayout.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device->CreateSamplerState(
    &samplerDesc, samplerState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device.Get()->CreateDepthStencilState(
    &depthDisabledStencilDesc,
    depthStencilState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device.Get()->CreateRasterizerState1(
    &rasterizerStateDesc,
    rasterizerState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device.Get()->CreateBlendState1(
    &blendStateDescription,
    blendState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);


}

void nyx::BillboardRenderer::render(CameraResources &camera, BillboardResources &billboard)
{
  static const auto mask = 0xffffffffu;
  static const UINT32 stencilRef = 0x01u;
  static const FLOAT factor[] = { 0.0f, 0.0f, 0.0f, 0.0f };

  static const UINT32 offsets[] = { 0u };
  static const UINT32 strides[] = { sizeof dx::Vector4 };

  assert(d3d != 0);
  auto device = d3d->device.Get();
  auto context = d3d->context.Get();

  context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
  context->IASetVertexBuffers(0u, 1u, billboard.vertexBuffer.GetAddressOf(), strides, offsets);
  context->IASetIndexBuffer(billboard.indexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0u);
  context->IASetInputLayout(inputLayout.Get());

  context->VSSetShader(vertexShader.Get(), nullptr, 0u);
  context->PSSetShader(pixelShader.Get(), nullptr, 0u);

  context->VSSetConstantBuffers(0u, 1u, billboard.perFrameResource.getBufferAddress());
  context->VSSetConstantBuffers(1u, 1u, camera.perFrameResource.getBufferAddress());
  context->PSSetSamplers(0u, 1u, samplerState.GetAddressOf());
  //context->OMSetDepthStencilState(depthStencilState.Get(), stencilRef);
  //context->OMSetBlendState(blendState.Get(), factor, mask);
  //context->RSSetState(rasterizerState.Get());

  context->DrawIndexed(billboard.indexCount, 0u, 0u);

  context->PSSetShader(nullptr, nullptr, 0u);
  context->VSSetShader(nullptr, nullptr, 0u);
}

void nyx::BillboardRenderer::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void nyx::BillboardRenderer::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  createDeviceResources();
}