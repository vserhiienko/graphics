
struct PSInput
{
  float4 position : SV_Position;
  float3 normal : Nyx_Normal;
  float fresnel : Nyx_Fresnel;
  float area[2] : Nyx_Area;
};

#define _Nyx_ps_normal 1
#define _Nyx_ps_fresnel 0

float4 main(in const PSInput input) : SV_TARGET
{
  float4 outputColor;
  outputColor = abs(input.fresnel);

#if _Nyx_ps_fresnel
  //outputColor = input.area[1] / input.area[0];
  //outputColor = /*input.area[0] /*/ input.area[1] * 100.0f;
  //outputColor = log(abs(1.0f / input.fresnel / 100.0f)) / 2.0f;
  outputColor = saturate(abs(1.0f / input.fresnel / 300.0f));
  //outputColor.xyz = 1.0f - outputColor.xyz;
  //outputColor.xyz *= float3(1.0f, 0.0f, 1.0f);
  //outputColor.xyz *= float3(0.1f, 0.1f, 1.0f);
#else
  outputColor.xyz = abs(input.normal);
  //outputColor.xyz = abs(input.normal);
#endif 
  outputColor.a = 0.96f;

  return outputColor;

  //printf("grid:ps: %f", input.fresnel);
  //return float4(1.0f, 1.0, 1.0f, 1.0f);
}