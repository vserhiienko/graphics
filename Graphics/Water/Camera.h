#pragma once
#include "pch.h"

namespace nyx
{

  /// <summary>
  /// Maintains camera parameters
  /// </summary>
  class Camera
  {
  protected:
    float fovY;
    float nearPlane, farPlane;

    void updateProjection(void);

  public:
    dx::Matrix view;
    dx::Matrix projection;

  public:
    float screenW, screenH;
    dx::Vector3 upDirection;
    dx::Vector3 eyePosition;
    dx::Vector3 focusPosition;

  public:
    Camera(void);

  public:
    void setScreenParams(
      float screenW,
      float screenH,
      float fovY = dx::XM_PIDIV4,
      float nearPlane = 0.001f,
      float farPlane = 100.0f
      );
    void updateView(void);

  };

  /// <summary>
  /// Camera with simple orbit rotation controller
  /// </summary>
  class OrbitCamera
    : public Camera
  {
  protected:
    dx::Vector3 eyeDisplacement, focusDisplacement;
    dx::Quaternion pitchQ, yawQ;
    float pitchAccA, yawAccA;
    float pitchA, yawA;

  public:
    OrbitCamera(void);
    void initialize(void);

  public:
    void onPointerInput(
      float deltaX,
      float deltaY,
      float deltaZoom
      );
  };




  /// <summary>
  /// Maintains camera GPU resources
  /// </summary>
  class CameraResources
  {
  public:
    struct PerFrame
    {
      dx::XMMATRIX view;
      dx::XMMATRIX projection;
    };

  public:
    dx::DxDeviceResources *d3d;

    PerFrame perFrameSource;
    dx::ConstantBuffer<PerFrame> perFrameResource;

  public:
    CameraResources(dx::DxDeviceResources*);
    void update(Camera const &);
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);
  };

}