#include "pch.h"
#include "FluidSurface.h"

nyx::FluidSurface::FluidSurface()
  : g(gradients)
  , v(velocityField)
  , currentHeightFieldRef(&heightFields[0])
  , previousHeightFieldRef(&heightFields[1])
  , u((*currentHeightFieldRef))
  , u0((*previousHeightFieldRef))
{
  c2 = 0.5f;
  h2 = 1.0f;
  pose = dx::Matrix::CreateTranslation(0.0f, 4.0f, 0.0f);

  gradients.border = dx::Vector2(0, 0);
  velocityField.border = 0;
  heightFields[0].border = 0;
  heightFields[1].border = 0;
}

void nyx::FluidSurface::initializeSurface(unsigned w, unsigned h, float d)
{
  w = dx::roundUp(w, 16u);
  h = dx::roundUp(h, 16u);
  d = nyx::utils::clamp(d, 0.00f, 1.0f);

  damping = d;
  gridWidth = w;
  gridHeight = h;
  gridMaxX = gridWidth - 1;
  gridMaxY = gridHeight - 1;

  gradients.initializeArray(w, h);
  velocityField.initializeArray(w, h);
  heightFields[0].initializeArray(w, h);
  heightFields[1].initializeArray(w, h);

  gradients.zeroMemory();
  velocityField.zeroMemory();
  heightFields[0].zeroMemory();
  heightFields[1].zeroMemory();
}

void nyx::FluidSurface::simulate(float dt)
{
  for (unsigned j = 1; j < gridMaxY; j++)
  {
    for (unsigned i = 1; i < gridMaxX; i++)
    {
      v.get(i, j) += (u.get(i - 1, j) + u.get(i + 1, j) + u.get(i, j - 1) + u.get(i, j + 1)) * 0.25f - u.get(i, j);
    }
  }

  for (unsigned j = 1; j < gridMaxY; j++)
  {
    for (unsigned i = 1; i < gridMaxX; i++)
    {
      float &vel = v.get(i, j);
      vel *= damping;
      u.get(i, j) += vel;
    }
  }

  updateGradients();
}

void nyx::FluidSurface::simulateParallel(float elapsedFrameTime)
{
  concurrency::parallel_for(1u, gridMaxY, [this](unsigned j)
  {
    for (unsigned i = 1; i < gridMaxX; i++)
    {
      v.get(i, j) += (u.get(i - 1, j) + u.get(i + 1, j) + u.get(i, j - 1) + u.get(i, j + 1)) * 0.25f - u.get(i, j);
    }
  });
  concurrency::parallel_for(1u, gridMaxY, [this](unsigned j)
  {
    for (unsigned i = 1; i < gridMaxX; i++)
    {
      float &vij = v.get(i, j);
      vij *= damping;
      u.get(i, j) += vij;
    }
  });
  updateGradientsParallel();
}

void nyx::FluidSurface::simulate2(float dt)
{
  for (unsigned j = 1; j < gridMaxY; j++)
  {
    for (unsigned i = 1; i < gridMaxX; i++)
    {
      const float heights
        = u.get(i - 1, j) + u.get(i + 1, j)
        + u.get(i, j - 1) + u.get(i, j + 1)
        - 4.0f * u.get(i, j);

      const float force = heights * dt * c2 / h2;
      v.get(i, j) += force * dt *  damping;
      u.get(i, j) += v.get(i, j) * dt;
    }
  }

  updateGradients();
}

void nyx::FluidSurface::applyForces(dx::Vector2 loc, float r, float s)
{
  float x = loc.x, y = loc.y;

  assert(
    x >= 0.0f 
    && y >= 0.0f 
    && r >= 0.0f 
    && s >= 0.0f
    );

  unsigned ix = (unsigned)floorf(x);
  unsigned iy = (unsigned)floorf(y);
  unsigned ir = (unsigned)ceilf(r);

  unsigned sx = utils::max(1u, ix - ir);
  unsigned sy = utils::max(1u, iy - ir);
  unsigned ex = utils::min(ix + ir, gridWidth - 2);
  unsigned ey = utils::min(iy + ir, gridHeight - 2);

  float d;
  dx::Vector2 delta;

  for (unsigned j = sy; j <= ey; j++) 
  {
    for (unsigned i = sx; i <= ex; i++)
    {
      delta.x = x - i;
      delta.y = y - j;
      d = delta.Length();

      if (d < r) 
      {
        d = (d / r) * 3.0f;
        d *= -d;
        u.get(i, j) += expf(d) * s;
      }
    }
  }
}

void nyx::FluidSurface::updateGradients()
{
  for (unsigned j = 1; j < gridMaxY; j++)
  {
    for (unsigned i = 1; i < gridMaxX; i++)
    {
      g.get(i, j).x = u.get(i + 1, j) - u.get(i - 1, j);
      g.get(i, j).y = u.get(i, j + 1) - u.get(i, j - 1);
    }
  }
}

void nyx::FluidSurface::updateGradientsParallel()
{
  concurrency::parallel_for(1u, gridMaxY, [this](unsigned j)
  {
    for (unsigned i = 1; i < gridMaxX; i++)
    {
      g.get(i, j).x = u.get(i + 1, j) - u.get(i - 1, j);
      g.get(i, j).y = u.get(i, j + 1) - u.get(i, j - 1);
    }
  });
}

void nyx::FluidSurface::swapHeightFieldRefs()
{
  std::swap(
    currentHeightFieldRef,
    previousHeightFieldRef
    );
}
