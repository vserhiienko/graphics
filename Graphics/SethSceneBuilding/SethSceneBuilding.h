#pragma once

#include <set>
#include <map>
#include <regex>
#include <vector>
#include <concurrent_unordered_set.h>
#include <concurrent_unordered_map.h>

#include <AegaScene.h>
#include <Eigen\Eigen>

#include <GteAlignedBox.h>
#include <GteContCapsule3.h>
#include <GteContSphere3.h>
#include <GteContOrientedBox3.h>
#include <GteContEllipsoid3.h>
#include <GteContEllipsoid3MinCR.h>

#include <SethTransformTree.h>

namespace seth
{
    struct SceneNode
    {
        typedef SceneNode *Ptr;
        typedef std::vector<Ptr> PtrVector;
        typedef gte::Sphere3<float> Sphere;
        typedef gte::Capsule3<float> Capsule;
        typedef gte::Ellipsoid3<float> Ellipsoid;
        typedef gte::AlignedBox3<float> BboxA;
        typedef gte::OrientedBox3<float> BboxO;

        BboxO boundingBoxO;
        Capsule boundingCapsule;

        aega::MeshNode *boundMeshNode;
        seth::TransformTreeNode::Ptr boundTransformNode;
        seth::TransformTreeNode::Ptr boundingBoxOTransformNode;
        seth::TransformTreeNode::Ptr boundingCapsuleTransformNode;
    };

    class SceneBuild
    {
    public:
        typedef std::map<std::string const, seth::TransformTreeNode::Ptr> String2TransformNodeMap;
        typedef concurrency::concurrent_unordered_map<std::string, aega::MeshNode*> String2MeshNodeSet;
        typedef concurrency::concurrent_unordered_set<seth::TransformTreeNode::Ptr> TransformSet;
        typedef std::pair<std::string, aega::MeshNode*> String2MeshNodeSetPair;

    public:
        aega::Scene *sceneSource;
        seth::TransformTree transformTree;
        SceneNode::PtrVector sceneNodes;
        String2MeshNodeSet *meshMatches;
        String2TransformNodeMap transformMap;

    public:
        SceneBuild();

    public:
        seth::TransformTreeNode::Ptr findTransformFor(aega::MeshNode *mesh);
        seth::TransformTreeNode::Ptr getDeepestParent(seth::TransformTreeNode::Ptr transform);
        void buildFrom(aega::Scene &scene, std::string const &mesh_skip_pattern);
    };


}

