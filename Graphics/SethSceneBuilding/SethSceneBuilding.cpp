#include <ppl.h>
#include <ppltasks.h>

#include "SethSceneBuilding.h"
using namespace seth;
using namespace Eigen;

namespace seth
{
    template <typename _Func>
    static void forEach(size_t from, size_t to, _Func func)
    {
        while (from < to) func(from++);
    }

    template <typename _EigenTy, typename _DirectXTy>
    static void copyVector3_EigenDirectX(_EigenTy &a, _DirectXTy &aa)
    {
        a.x() = aa.x;
        a.y() = aa.y;
        a.z() = aa.z;
    }
    template <typename _EigenTy, typename _DirectXTy>
    static void copyVector4_EigenDirectX(_EigenTy &a, _DirectXTy &aa)
    {
        a.x() = aa.x;
        a.y() = aa.y;
        a.z() = aa.z;
        a.w() = aa.w;
    }

    template <typename _EigenTy, typename _GTETy>
    static void copyVector3_EigenGTE(_EigenTy &a, _GTETy &aa)
    {
        a.x() = aa[0];
        a.y() = aa[1];
        a.z() = aa[2];
    }

    template <typename _EigenTy, typename _GTETy>
    static void copyVector4_EigenGTE(_EigenTy &a, _GTETy &aa)
    {
        a.x() = aa[0];
        a.y() = aa[1];
        a.z() = aa[2];
        a.w() = aa[3];
    }
    template <typename _TySimpleType>
    static void safeDelete(_Outref_ _TySimpleType *&desc)
    {
        if (desc) delete desc, desc = 0;
    }

}

SceneBuild::SceneBuild()
    : meshMatches(nullptr)
{
}

seth::TransformTreeNode::Ptr SceneBuild::findTransformFor(aega::MeshNode *mesh)
{
    auto meshParentPathIt = std::find_if(mesh->parentPaths.begin(), mesh->parentPaths.end(), [&](std::string const &meshParentPath)
    { return transformMap.find(meshParentPath) != transformMap.end(); });

    if (meshParentPathIt != mesh->parentPaths.end()) return transformMap[(*meshParentPathIt)];
    else return 0;
}

seth::TransformTreeNode::Ptr SceneBuild::getDeepestParent(seth::TransformTreeNode::Ptr transform)
{
    if (!transform) return 0;
    seth::TransformTreeNode::Ptr  parent = transform->parent, child = 0;
    while (parent != 0) child = parent, parent = parent->parent;
    return child;
}

void SceneBuild::buildFrom(aega::Scene &scene, std::string const &mesh_skip_pattern)
{
    sceneSource = &scene;

#pragma region build transforms

    auto &transforms = transformTree.nodes;
    auto const x = scene.transforms.data();

    transforms.resize(scene.transforms.size());
    seth::forEach(size_t(0), scene.transforms.size(), [&](size_t node_ind)
    {
        if (x[node_ind])
        {
            transforms[node_ind] = new seth::TransformTreeNode();
            transforms[node_ind]->makeIdentity();

            transforms[node_ind]->user = x[node_ind];
            transforms[node_ind]->source = x[node_ind];

            //dx::trace("buildScene: x %s", x[node_ind]->path.c_str());

            copyVector3_EigenDirectX(transforms[node_ind]->scalingV, x[node_ind]->scale);
            copyVector4_EigenDirectX(transforms[node_ind]->rotationOSQ, x[node_ind]->rotationTS);
            copyVector4_EigenDirectX(transforms[node_ind]->orientationQ, x[node_ind]->orientation);
            copyVector3_EigenDirectX(transforms[node_ind]->translationOSV, x[node_ind]->translationTS);

            transforms[node_ind]->updatePoseOS = true;
            transforms[node_ind]->updatePoseWS = true;
            transforms[node_ind]->evaluatePoseOS();
        }
    });

    transformMap.clear();
    std::for_each(transforms.begin(), transforms.end(), [&](seth::TransformTreeNode::Ptr transform)
    {
        std::string const & transform_path = (((aega::TransformNode*)transform->user)->path);
        if (transform_path.size() > 0)
        {
            assert(transformMap.find(transform_path) == transformMap.end());
            transformMap[transform_path] = transform;
        }
    });

    //dx::trace("buildScene: %u transforms found", transforms.size());
    std::for_each(transforms.begin(), transforms.end(), [&](seth::TransformTreeNode::Ptr transform)
    {
        auto bound_node = (aega::TransformNode*)transform->user;
        //dx::trace("buildScene: processing x: %s", bound_node->path.c_str());

        std::for_each(bound_node->childPaths.begin(), bound_node->childPaths.end(), [&](std::string const &child_path)
        {
            if (transformMap.find(child_path) != transformMap.end())
            {
                //dx::trace("\t\t\t -> %s", child_path.c_str());

                assert(transformMap[child_path] != 0);
                transform->children.push_back(transformMap[child_path]);
            }
        });
        std::for_each(bound_node->parentPaths.begin(), bound_node->parentPaths.end(), [&](std::string const &parent_path)
        {
            if (transformMap.find(parent_path) != transformMap.end())
            {
                //dx::trace("\t\t\t <- %s", parent_path.c_str());

                assert(transform->parent == 0);
                assert(transformMap[parent_path] != 0);
                transform->parent = transformMap[parent_path];
            }
        });
    });

#pragma endregion 

#pragma region build meshes
    meshMatches = new String2MeshNodeSet();
    const std::regex mesh_skip_pattern_rgx(mesh_skip_pattern.empty() ? "(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)" : mesh_skip_pattern);
    concurrency::parallel_for_each(scene.meshes.begin(), scene.meshes.end(), [&](aega::MeshNode *mesh_node)
    {
        if (!std::regex_match(mesh_node->path, mesh_skip_pattern_rgx)) (*meshMatches)[mesh_node->path] = mesh_node;
    });

    TransformSet active_transforms;

    size_t mesh_ind = 0;
    size_t mesh_count = meshMatches->size();
    sceneNodes.resize(mesh_count);

    std::for_each(meshMatches->begin(), meshMatches->end(), [&](String2MeshNodeSetPair mesh_pair)
    //concurrency::parallel_for_each(meshMatches->begin(), meshMatches->end(), [&](String2MeshNodeSetPair mesh_pair)
    {
        auto &scene_item = sceneNodes[mesh_ind++];

        scene_item = new SceneNode();
        scene_item->boundMeshNode = mesh_pair.second;
        scene_item->boundingBoxOTransformNode = new seth::TransformTreeNode();
        scene_item->boundingCapsuleTransformNode = new seth::TransformTreeNode();

        scene_item->boundingBoxOTransformNode->makeIdentity();
        scene_item->boundingCapsuleTransformNode->makeIdentity();

        scene_item->boundTransformNode = findTransformFor(mesh_pair.second);
        auto mesh_transform_deepest_parent = getDeepestParent(scene_item->boundTransformNode);
        mesh_transform_deepest_parent = mesh_transform_deepest_parent
            ? mesh_transform_deepest_parent : scene_item->boundTransformNode;
        active_transforms.insert(mesh_transform_deepest_parent);

        scene_item->boundTransformNode->children.push_back(scene_item->boundingCapsuleTransformNode);
        scene_item->boundTransformNode->children.push_back(scene_item->boundingBoxOTransformNode);

        scene_item->boundingCapsuleTransformNode->parent = scene_item->boundTransformNode;
        scene_item->boundingBoxOTransformNode->parent = scene_item->boundTransformNode;

        int pointCount = scene_item->boundMeshNode->points.size();
        gte::Vector3<float> *points = (gte::Vector3<float> *)scene_item->boundMeshNode->points.data();

        gte::GetContainer(pointCount, points, scene_item->boundingBoxO);
        gte::GetContainer(pointCount, points, scene_item->boundingCapsule);

        scene_item->boundingBoxOTransformNode->updatePoseOS = true;
        scene_item->boundingBoxOTransformNode->updatePoseWS = true;
        scene_item->boundingCapsuleTransformNode->updatePoseOS = true;
        scene_item->boundingCapsuleTransformNode->updatePoseWS = true;

        float orientationData[9];
        orientationData[0 + 0] = scene_item->boundingBoxO.axis[0][0];
        orientationData[0 + 1] = scene_item->boundingBoxO.axis[0][1];
        orientationData[0 + 2] = scene_item->boundingBoxO.axis[0][2];
        orientationData[3 + 0] = scene_item->boundingBoxO.axis[1][0];
        orientationData[3 + 1] = scene_item->boundingBoxO.axis[1][1];
        orientationData[3 + 2] = scene_item->boundingBoxO.axis[1][2];
        orientationData[6 + 0] = scene_item->boundingBoxO.axis[2][0];
        orientationData[6 + 1] = scene_item->boundingBoxO.axis[2][1];
        orientationData[6 + 2] = scene_item->boundingBoxO.axis[2][2];

        scene_item->boundingBoxOTransformNode->orientationQ = Matrix3f(orientationData);
        copyVector3_EigenGTE(scene_item->boundingBoxOTransformNode->scalingV, scene_item->boundingBoxO.extent);
        copyVector3_EigenGTE(scene_item->boundingBoxOTransformNode->translationOSV, scene_item->boundingBoxO.center);

        scene_item->boundingBoxOTransformNode->evaluatePoseOS();
        scene_item->boundingCapsuleTransformNode->evaluatePoseOS();

        /*dx::trace("buildScene: attaching bbox to %s",
        ((aega::TransformNode*)scene_item->mesh_transform->user)->path.c_str()
        );*/
        /*dx::trace("buildScene: is active transform %s",
        ((aega::TransformNode*)mesh_transform_deepest_parent->user)->path.c_str()
        );*/
    });

#pragma endregion 

    safeDelete(transformTree.rootNode);
    transformTree.newRoot();

    std::for_each(active_transforms.begin(), active_transforms.end(), [&](seth::TransformTreeNode::Ptr transform)
    {
        //dx::trace("buildScene: active transform %s", ((aega::TransformNode*)transform->user)->path.c_str());
        transformTree.rootNode->children.push_back(transform);
        transform->parent = transformTree.rootNode;
    });

    meshMatches->clear();
    safeDelete(meshMatches);
}


