#include "stdafx.h"

#include "Mesh.h"
#include "V3DTime.h"
#include "Log.h"
#include "loki\Allocator.h"

#include <ppl.h>
#include <ppltasks.h>
#include <atomic>
#include <concurrent_vector.h>
#include <concurrent_unordered_set.h>
#include <concurrent_unordered_map.h>
#include "LockSRW.h"

#include "InstanceSearch.h"

#include <random>
#include <DirectXMath.h>

using namespace Visco3D;
using namespace concurrency;

struct Simple_code_profiler
{
    double t;
    inline void begin() { t = Stopwatch::GetGlobalSeconds(); }
    inline double end() { return t = Stopwatch::GetGlobalSeconds() - t; }
};


struct Mesh_instancing_check_info
{
    struct Hasher
    {
        size_t operator()(const Mesh_instancing_check_info& _Keyval) const
        {
            return _Keyval.indexH;
        }
    };
    struct Equal_to
    {
        size_t operator()(const Mesh_instancing_check_info& _Keyval1, const Mesh_instancing_check_info& _Keyval2) const
        {
            return _Keyval1.indexH == _Keyval2.indexH;
        }
    };

    typedef std::pair<Mesh_instancing_check_info const&, MeshData const &> Pair_to_mesh;
    typedef std::vector<Mesh_instancing_check_info, Loki::LokiAllocator<Mesh_instancing_check_info>> Loki_vector;
    typedef std::vector<Mesh_instancing_check_info *, Loki::LokiAllocator<Mesh_instancing_check_info *>> Loki_ptr_vector;
    typedef concurrent_unordered_multimap<Mesh_instancing_check_info const&, MeshData const &> Concurrent_multimap_to_mesh;
    typedef std::unordered_multimap<Mesh_instancing_check_info const&, MeshData const &, Hasher, Equal_to, Loki::LokiAllocator<Mesh_instancing_check_info>> Loki_unordered_multimap_to_mesh;
    typedef Loki_unordered_multimap_to_mesh::value_type Loki_unordered_multimap_to_mesh_value;
    typedef Loki_unordered_multimap_to_mesh::iterator Iterator;
    typedef Loki_unordered_multimap_to_mesh::_Pairii Iterator_range;

    size_t
        vertexC,
        indexC,
        meshC;

    size_t indexH;

    size_t ref_offset;
    size_t mesh_offset;
    Iterator mesh_base_it;
    Iterator_range mesh_it_range;

    bool operator==(Mesh_instancing_check_info const &other) const
    {
        return
            vertexC == other.vertexC &&
            indexC == other.indexC &&
            indexH == other.indexH;
    }

    size_t updateMeshCount()
    {
        return meshC = std::distance(
            mesh_it_range.first, 
            mesh_it_range.second
            );
    }

    void updateRange()
    {
        //mesh_base_it = mesh_it_range.first;
        //++mesh_it_range.first;
        //--meshC; // first element was removed
    }
};

static const float static_epsilon = 0.000001f;
struct Float_processor
{
    inline static bool areClose(float v1, float v2, float e = static_epsilon)
    {
        return abs(v1 - v2) <= e;
    }

    inline static bool areClose(vec3 v1, vec3 v2, float e = static_epsilon)
    {
        return areClose(v1.x, v2.x) 
            && areClose(v1.y, v2.y) 
            && areClose(v1.z, v2.z);
    }

    inline static bool areClose(vec4 v1, vec4 v2, float e = static_epsilon)
    {
        return areClose(v1.x, v2.x)
            && areClose(v1.y, v2.y)
            && areClose(v1.z, v2.z)
            && areClose(v1.w, v2.w);
    }

    inline static bool areCollinear(float x1, float y1, float z1,
        float x2, float y2, float z2,
        float x3, float y3, float z3,
        float e = static_epsilon
        )
    {
        float dx1 = x2 - x1;
        float dy1 = y2 - y1;
        float dz1 = z2 - z1;
        float dx2 = x3 - x1;
        float dy2 = y3 - y1;
        float dz2 = z3 - z1;
        float cx = (dy1 * dz2) - (dy2 * dz1);
        float cy = (dx2 * dz1) - (dx1 * dz2);
        float cz = (dx1 * dy2) - (dx2 * dy1);
        return areClose(cx * cx + cy * cy + cz * cz, 0, e);
    }

    inline static bool areCollinear(vec3 v1, vec3 v2, vec3 v3, float e = static_epsilon)
    {
        vec3 d1 = v2 - v1;
        vec3 d2 = v3 - v1;
        float cx = (d1.y * d2.z) - (d2.y * d1.z);
        float cy = (d2.x * d1.z) - (d1.x * d2.z);
        float cz = (d1.x * d2.y) - (d2.x * d1.y);
        return areClose(cx * cx + cy * cy + cz * cz, 0, e);
    }

    inline static void planeNormalDistance(vec3 a, vec3 b, vec3 c, vec3 &n, float &d)
    {
        vec3 edge1 = b - a;
        vec3 edge2 = c - a;
        n = glm::cross(edge1, edge2);
        d = -glm::dot(n, a);
        n = glm::normalize(n);
    }

    inline static bool areCoplanar(vec3 v1, vec3 v2, vec3 v3, vec3 v4, float e = static_epsilon)
    {
        vec3 n; float d; 
        planeNormalDistance(v1, v2, v3, n, d);
        float dd = abs(glm::dot(n, v4) + d);
        return dd < e;
    }

    inline static vec3 extractTransformScaling(mat4 const &transform)
    {
        auto data = reinterpret_cast<float const*>(&transform);
        return vec3(
            glm::length(vec3(data[0], data[1], data[2])),
            glm::length(vec3(data[4], data[5], data[6])),
            glm::length(vec3(data[8], data[9], data[10]))
            );
    }

    inline static float maxAbs(vec3 const &v)
    {
        return std::max(std::max(abs(v.x), abs(v.y)), std::max(abs(v.y), abs(v.z)));
    }

    inline static float max(vec3 const &v)
    {
        return std::max(std::max(v.x, v.y), std::max(v.y, v.z));
    }

};

typedef int Lapack_dgesv(
    int* n,		//The number of columns of the matrix A.  N >= 0. 
    int* nrhs,	//The number of columns of the matrix B.  N >= 0. 
    double* a,	//(input/output) REAL array, dimension (LDA,N)
    int* lda,	//The leading dimension of the array A.  LDA >= max(1,M).
    int* ipiv,	//IPIV    (output) INTEGER array, dimension (min(M,N))
                //The pivot indices; for 1 <= i <= min(M,N), row i of the
                //matrix was interchanged with row IPIV(i).
    double* b,	//(output) REAL array, dimension (LDA,N)
    int* ldb,	//The leading dimension of the array A.  LDA >= max(1,M).
    int* info
    );

struct Mesh_instantiation_references
{
    typedef std::vector<Mesh_instantiation_references, Loki::LokiAllocator<Mesh_instantiation_references>> Loki_vector;

    size_t this_index;
    MeshData const *mesh_data;
    Loki_vector *back_reference;
    Mesh_instantiation_references *base_reference;
};

struct Mesh_instantiation_args
{
    struct Statistic_records
    {
        volatile size_t solverCallCount;
        volatile size_t instanceCount;
        volatile size_t indicesFoundCount;
        Statistic_records()
            : solverCallCount(0)
            , instanceCount(0)
            , indicesFoundCount(0)
        {
        }
    };

    static const uint32_t min_search_vertex_count = 18; // see CS code
    static const uint32_t max_random_search_samples = 512; // see CS code
    static const uint32_t randomization_seed = 48; // see CS code

    static const uint8_t kState_Undefined = '?';
    static const uint8_t kState_Instance = 'I';
    static const uint8_t kState_Unique = 'U';
    static const uint8_t kState_Rejected = 'R';

    static Statistic_records stat_records;
    static Lapack_dgesv *dgesv;

    static void resolveLapack()
    {

        HMODULE lapack_dll = LoadLibrary(TEXT("lapack.dll"));
        Mesh_instantiation_args::dgesv = (Lapack_dgesv*)GetProcAddress(lapack_dll, "dgesv_");
    }


    // should be called before generation of the indices had started
    static void randomize() { srand(randomization_seed); }
    static _Out_range_(0, 1)float random_float() { return (float)rand() / (float)RAND_MAX; }
    static _Out_range_(0, max) uint32_t random_index(size_t max) { return (uint32_t)(random_float() * (float)max); }

    typedef std::vector<uint8_t, Loki::LokiAllocator<uint8_t>> Loki_bool_vector;
    typedef std::vector<mat4, Loki::LokiAllocator<mat4>> Loki_mat4_vector;
    typedef std::vector<Mesh_instantiation_args, Loki::LokiAllocator<Mesh_instantiation_args>> Loki_vector;

    mat4 *transform;
    uint8_t *transform_validity;

    //union { struct { uint32_t i, j, k, m; }; uint32_t indices[4]; };
    //MeshData const *base_mesh_data;

    Mesh_instancing_check_info *group_ref;
    Mesh_instantiation_references *referencing;

    void initializeReferencing(Mesh_instantiation_references::Loki_vector &back_reference, size_t ref_offset)
    {
        size_t ind;
        auto mesh_it = group_ref->mesh_it_range.first;
        for (ind = 0; ind < group_ref->meshC; ++ind, ++mesh_it)
        {
            referencing[ind].mesh_data = &(*mesh_it).second;
            referencing[ind].this_index = ref_offset + ind;
            referencing[ind].back_reference = &back_reference;
            transform_validity[ind] = kState_Undefined;
        }
    }

    template <typename _Callback>
    void forEachMeshInRange(_Callback callback)
    {
        size_t mesh_ind = 0;
        auto &it = instantiation_group->mesh_it_range.first;
        auto const &it_end = instantiation_group->mesh_it_range.second;
        while (it != it_end) callback((*it).second, mesh_ind), ++it, ++mesh_ind;
    }

    bool evaluateIndices(MeshData const *mesh_data, uint32_t *indices)
    {
        typedef Float_processor procf;

        uint32_t 
            &i = indices[0], 
            &j = indices[1], 
            &k = indices[2], 
            &m = indices[3];
        vec3 vi, vj, vk, vm;
        size_t sample_ind = 0;
        for (; sample_ind < max_random_search_samples; ++sample_ind)
        {
            i = random_index(group_ref->vertexC);
            vi = mesh_data->GetVC<vctFloat3>(i, vcuPosition);

            do { j = random_index(group_ref->vertexC); } while (i == j);
            vj = mesh_data->GetVC<vctFloat3>(j, vcuPosition);

            if (procf::areClose(vi, vj)) continue;

            do { k = random_index(group_ref->vertexC); } while (i == k || j == k);
            vk = mesh_data->GetVC<vctFloat3>(k, vcuPosition);

            if (procf::areClose(vi, vk) || procf::areClose(vj, vk)) continue;
            if (procf::areCollinear(vi, vj, vk)) continue;

            do { m = random_index(group_ref->vertexC); } while (i == m || j == m || k == m);
            vm = mesh_data->GetVC<vctFloat3>(m, vcuPosition);

            if (procf::areCoplanar(vi, vj, vk, vm)) continue;

            InterlockedIncrement(&stat_records.indicesFoundCount);

            return true;
        }



        return false;
    }

    void solveTransform(MeshData const &candidate_mesh, _Outref_ mat4 &instance_transform)
    {
        int n = 4;
        int nrhs = 4;
        int lda = 4;
        int ldb = 4;
        int info = 0;

        int ipiv[16]; ZeroMemory(ipiv, sizeof(ipiv));
        double a[16], b[16]; 

        vec3 const *src, *dst;
        for (int r = 0; r < 4; r++)
        {
            src = &base_mesh_data->GetVC<vctFloat3>(indices[r], vcuPosition);
            dst = &candidate_mesh.GetVC<vctFloat3>(indices[r], vcuPosition);

            a[r] = src->x;
            a[r + 4] = src->y;
            a[r + 8] = src->z;
            a[r + 12] = 1.0;

            b[r] = dst->x;
            b[r + 4] = dst->y;
            b[r + 8] = dst->z;
            b[r + 12] = 1.0;
        }

        int r = dgesv(
            &n,
            &nrhs,
            a,
            &lda,
            ipiv,
            b,
            &ldb,
            &info
            );

        tmat4 bb;
        memcpy(&bb, b, sizeof(b));
        instance_transform = (mat4)bb;

        InterlockedIncrement(&stat_records.solverCallCount);
    }

    bool validateTransform(MeshData const &candidate_mesh, mat4 const &instance_transform, float epsilon = static_epsilon)
    {
        vec4 base_pos, cand_pos, transformed_pos;
        for (size_t v_ind = 0; v_ind < instantiation_group->vertexC; ++v_ind)
        {
            base_pos = base_mesh_data->GetVC<vctFloat4>(v_ind, vcuPosition); base_pos.w = 1.0f;
            cand_pos = candidate_mesh.GetVC<vctFloat4>(v_ind, vcuPosition); cand_pos.w = 1.0f;
            transformed_pos = base_pos * instance_transform; transformed_pos /= transformed_pos.w;
            if (!Float_processor::areClose(cand_pos, transformed_pos, epsilon))
                return false;
        }

        InterlockedIncrement(&stat_records.instanceCount);
        return true;
    }

    void evaluateInstanceTransforms(void)
    {
        vec3 scaling;
        AABB<float> const *bb_base;
        AABB<float> const *bb_cand;
        float adaptive_epsilon = static_epsilon;
        forEachMeshInRange([&](MeshData const &mesh, size_t mesh_ind)
        {
            solveTransform(mesh, transform[mesh_ind]);

            bb_cand = &mesh.GetBounds();
            bb_base = &base_mesh_data->GetBounds();
            scaling = Float_processor::extractTransformScaling(
                transform[mesh_ind]
                );

            adaptive_epsilon = std::max(
                Float_processor::max(bb_base->getSize()),
                Float_processor::max(bb_cand->getSize())
                );
            adaptive_epsilon *= static_epsilon;
            adaptive_epsilon *= Float_processor::maxAbs(scaling);/**/

            transform_validity[mesh_ind] = validateTransform(mesh, transform[mesh_ind], adaptive_epsilon)
                ? 1ui8
                : 0ui8;
        });
    }


};

Lapack_dgesv *Mesh_instantiation_args::dgesv = 0;
Mesh_instantiation_args::Statistic_records Mesh_instantiation_args::stat_records;

void InstanceSearch::SearchIn(
    std::shared_ptr<Mesh> const *meshes,
    size_t mesheCount,
    Result &result
    )
{
    typedef Mesh_instancing_check_info::Loki_unordered_multimap_to_mesh_value Mici_Lumm2mv;
    
    Simple_code_profiler profiler;

    size_t const meshC = mesheCount;

    
    Mesh_instancing_check_info::Loki_vector check_info; // persistent data
    Mesh_instancing_check_info::Loki_ptr_vector unique_check_info;
    Mesh_instancing_check_info::Loki_unordered_multimap_to_mesh grouped_mesh_data;

    Mesh_instantiation_args::Loki_vector instantiation_args;
    Mesh_instantiation_args::Loki_mat4_vector instance_transforms;
    Mesh_instantiation_args::Loki_bool_vector instance_transforms_validity;

    Mesh_instantiation_references::Loki_vector instantiation_references;

    check_info.resize(meshC);

    // ensure we have enough memory for start
    // later unneeded memory will be freed
    unique_check_info.resize(meshC);

    auto check_info_ptr = check_info.data();
    auto unique_check_info_ptr = unique_check_info.data();
    auto const mesh_data_ptr = meshes;

    LockSRW insert_lock;

    profiler.begin();
    parallel_for(0ull, meshC, [&](size_t mesh_ind)
    {
        auto &check_info_item = *(check_info_ptr + mesh_ind);
        auto const &mesh_data_item = (mesh_data_ptr + mesh_ind)->get()->GetData();
        check_info_item.vertexC = mesh_data_item.VerticesNum();
        check_info_item.indexC = mesh_data_item.IndicesNum();
        check_info_item.indexH = mesh_data_item.GetIndicesHash64();
        insert_lock.acquireW();
        grouped_mesh_data.insert(Mici_Lumm2mv(check_info_item, mesh_data_item));
        insert_lock.releaseW();
    });
    profiler.end();
    visco_debugTrace("SearchIn: mapping stage took %f s", profiler.t);

    auto it = grouped_mesh_data.begin();
    auto const end = grouped_mesh_data.end();
    if (it == end) return;

    size_t refs_needed = 0;
    size_t transforms_needed = 0;
    size_t next_check_info_ind = 0;

    // this is the default minimal filter to reject groups 
    static auto groupping_filter = [](Mesh_instancing_check_info const &given_check_info) -> bool
    { return given_check_info.meshC > 1 && given_check_info.vertexC > Mesh_instantiation_args::min_search_vertex_count; };

    profiler.begin();
    do
    {
        Mesh_instancing_check_info &mesh_check_info = (Mesh_instancing_check_info &)it->first;
        mesh_check_info.mesh_it_range = grouped_mesh_data.equal_range(mesh_check_info);
        mesh_check_info.updateMeshCount();
        if (groupping_filter(mesh_check_info))
            *(unique_check_info_ptr + next_check_info_ind) = &mesh_check_info,
            mesh_check_info.ref_offset = refs_needed,
            refs_needed += mesh_check_info.meshC,
            /*mesh_check_info.updateRange(),
            mesh_check_info.mesh_offset = transforms_needed,
            transforms_needed += mesh_check_info.meshC,*/
            ++next_check_info_ind;
    }
    while (it = grouped_mesh_data.upper_bound((Mesh_instancing_check_info const &)it->first), it != end);
    profiler.end();
    visco_debugTrace("SearchIn: got %llu potential groups", next_check_info_ind);
    visco_debugTrace("SearchIn: groupping stage took %f s", profiler.t);

    // this is valid since we the incrementation happens after check info rejection
    size_t mesh_data_group_count = next_check_info_ind;

    unique_check_info.resize(mesh_data_group_count); // free unused


    // alloc new
    instantiation_references.resize(refs_needed);
    instance_transforms.resize(refs_needed);
    instance_transforms_validity.resize(refs_needed);
    instantiation_args.resize(mesh_data_group_count);

    auto referencing_ptr = instantiation_references.data();
    auto instance_transforms_ptr = instance_transforms.data();
    auto instance_transforms_validity_ptr = instance_transforms_validity.data();
    auto instantiation_args_ptr = instantiation_args.data();

    Mesh_instantiation_args::randomize();
    Mesh_instantiation_args::resolveLapack();

    profiler.begin();
    //parallel_for(0ull, 1ull, [&](size_t mesh_data_group_ind)
    parallel_for(0ull, mesh_data_group_count, [&](size_t mesh_data_group_ind)
    {
        auto &mesh_group_info = *(unique_check_info_ptr + mesh_data_group_ind);
        auto &instantiation_arg = *(instantiation_args_ptr + mesh_data_group_ind);

        instantiation_arg.referencing = referencing_ptr + mesh_group_info->ref_offset;
        instantiation_arg.transform = instance_transforms_ptr + mesh_group_info->ref_offset;
        instantiation_arg.transform_validity = instance_transforms_validity_ptr + mesh_group_info->ref_offset;
        instantiation_arg.group_ref = mesh_group_info;

        instantiation_arg.initializeReferencing(instantiation_references, mesh_group_info->ref_offset);



        /*instantiation_arg.base_mesh_data = &(*mesh_group_info->mesh_base_it).second;
        if (instantiation_arg.evaluateIndices())
            instantiation_arg.evaluateInstanceTransforms();*/
        //visco_debugTrace("\t\tfor group %llu found %llu meshes\n", mesh_group_info->indexH, mesh_group_info->meshC);
    });
    profiler.end();
    visco_debugTrace("SearchIn: processing mesh groups took %f s", profiler.t);

    visco_debugTrace("SearchIn: results:"
        "\n\t\t groups: %llu"
        "\n\t\t total mesh count: %llu"
        "\n\t\t potential instance count: %llu"
        "\n\t\t actual instance count: %llu"
        "\n\t\t actual mesh count: %llu"
        , unique_check_info.size()
        , mesheCount
        , transforms_needed
        , Mesh_instantiation_args::stat_records.instanceCount
        , mesheCount - Mesh_instantiation_args::stat_records.instanceCount
        );





}

