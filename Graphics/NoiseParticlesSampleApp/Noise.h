#pragma once

namespace nyx
{
  class NoiseContext
  {
  public:
    static const int p[];
    static const int permutation[];
  };

  class Noise
  {
  public:
    Noise();
    ~Noise();

  public:
    void detach(); // wont release memory if instance destroyed
    void release(); // releases memory

  public:
    float *getNoiseValues(); // returns noise values pointer
    float const *getNoiseValues() const; // returns noise values pointer

    inline int getWidth() const { return m_width; }
    inline int getHeight() const { return m_height; }
    inline int getDepth() const { return m_depth; }
    inline int getDim() const { return m_dim; }

  public:
    void createSimpleNoise(int width, int height, int dim); // generates 2d simple noise
    void createSimpleNoise(int width, int height, int depth, int dim); // generates 3d simple noise

  public:
    static float frand();
    static float sfrand();
    static float fade(float t);
    static float lerp(float t, float a, float b);
    static float grad(int hash, float x, float y, float z);
    static float noise(float x, float y, float z);

  private:
    int 
      m_width,
      m_height,
      m_depth,
      m_dim;
    float 
      *m_noise;
    
  };

  class NoiseResource
  {
  public:
    static GLuint createNoiseTexture2D(float const *values, int width, int height, GLint internalFormat, GLint format, bool mipmap);
    static GLuint createNoiseTexture3D(float const *values, int width, int height, int depth, GLint internalFormat, GLint format, bool mipmap);

  public: /// shortcuts
    static GLuint createNoiseTextureAuto(Noise const *noise);
    inline static GLuint createNoiseTexture2D_r8(float const *values, int width, int height, bool mipmap) { return createNoiseTexture2D(values, width, height, GL_R8, GL_RED, mipmap); }
    inline static GLuint createNoiseTexture2D_rgba8(float const *values, int width, int height, bool mipmap) { return createNoiseTexture2D(values, width, height, GL_RGBA8, GL_RGBA, mipmap); }
    inline static GLuint createNoiseTexture3D_r8(float const *values, int width, int height, int depth, bool mipmap) { return createNoiseTexture3D(values, width, height, depth, GL_R8, GL_RED, mipmap); }
    inline static GLuint createNoiseTexture3D_rgba8(float const *values, int width, int height, int depth, bool mipmap) { return createNoiseTexture3D(values, width, height, depth, GL_RGBA8, GL_RGBA, mipmap); }

  };

  class PiEvaluator
  {
  public:
    static void evaluate(unsigned iterations);
    static void evaluateInVolume(unsigned iterations);
  };



}