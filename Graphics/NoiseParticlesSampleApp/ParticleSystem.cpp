#include "pch.h"
#include "Noise.h"
#include "NvShaderUtils.h"
#include "ParticleSystem.h"

#include <NV/NvShaderMappings.h>
#include "assets/shaders/uniforms.h"

nyx::ParticleSystem::ParticleSystem(size_t size)
{
  m_size = size;
  m_noiseSize = 16;
  m_noiseTexture = 0;
  m_renderProgram = 0;
  m_updateProgram = 0;

  m_pos = new NvShaderBufferFloat4(size);
  m_vel = new NvShaderBufferFloat4(size);
  m_indices = new NvShaderBufferUInt(size * 6);

  uint32_t *indices = m_indices->map();
  for (size_t i = 0; i < m_size; i++)
  {
    size_t index = i << 2;
    *(indices++) = index;
    *(indices++) = index + 1;
    *(indices++) = index + 2;
    *(indices++) = index;
    *(indices++) = index + 2;
    *(indices++) = index + 3;
  }
  m_indices->unmap();

  Noise noise;
  noise.createSimpleNoise(m_noiseSize, m_noiseSize, m_noiseSize, 4);

  m_noiseTexture = NoiseResource::createNoiseTexture3D(
    noise.getNoiseValues(),   // values
    m_noiseSize,              // width
    m_noiseSize,              // height
    m_noiseSize,              // depth
    GL_RGBA8_SNORM,           // iternal format
    GL_RGBA,                  // format
    false                     // mipmaps
    );

  loadShaders();
  reset(0.5f);

}

nyx::ParticleSystem::~ParticleSystem()
{
  delete m_pos, m_pos = NULL;
  delete m_vel, m_vel = NULL;
  delete m_indices, m_indices = NULL;
  glDeleteProgram(m_renderProgram);
  glDeleteProgram(m_updateProgram);
}

void nyx::ParticleSystem::loadShaders()
{
  if (m_updateProgram) {
    glDeleteProgram(m_updateProgram);
    m_updateProgram = 0;
  }

  glGenProgramPipelines(1, &m_renderProgram);

  std::string src = NvShaderUtils::loadShaderSourceWithUniformTag("shaders/uniforms.h", "shaders/particlesCS.glsl");
  m_updateProgram = createShaderPipelineProgram(GL_COMPUTE_SHADER, src.c_str());

  glBindProgramPipeline(m_renderProgram);

  GLint loc = glGetUniformLocation(m_updateProgram, "invNoiseSize");
  glProgramUniform1f(m_updateProgram, loc, 1.0f / m_noiseSize);

  loc = glGetUniformLocation(m_updateProgram, "noiseTex3D");
  glProgramUniform1i(m_updateProgram, loc, 0);

  glBindProgramPipeline(0);
}

void nyx::ParticleSystem::reset(float size)
{
  nv::vec4f *pos = m_pos->map();
  for (size_t i = 0; i < m_size; i++) {
    pos[i] = nv::vec4f(
      Noise::sfrand() * size,
      Noise::sfrand() * size,
      Noise::sfrand() * size,
      1.0f
      );
  }
  m_pos->unmap();

  nv::vec4f *vel = m_vel->map();
  for (size_t i = 0; i < m_size; i++) {
    vel[i] = nv::vec4f(0.0f, 0.0f, 0.0f, 1.0f);
  }
  m_vel->unmap();

}

void nyx::ParticleSystem::update()
{
  // Invoke the compute shader to integrate the particles
  glBindProgramPipeline(m_renderProgram);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_3D, m_noiseTexture);

  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, m_pos->getBuffer());
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, m_vel->getBuffer());

  glDispatchCompute(m_size / WORK_GROUP_SIZE, 1, 1);

  // We need to block here on compute completion to ensure that the
  // computation is done before we render
  glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);

  glBindProgramPipeline(0);
}

GLuint nyx::ParticleSystem::createShaderPipelineProgram(GLuint target, char const *src)
{
  GLint status;
  GLuint object;

  object = glCreateShaderProgramv(target, 1, (const GLchar **)&src);

  glBindProgramPipeline(m_renderProgram);
  glUseProgramStages(m_renderProgram, GL_COMPUTE_SHADER_BIT, object);
  glValidateProgramPipeline(m_renderProgram);
  glGetProgramPipelineiv(m_renderProgram, GL_VALIDATE_STATUS, &status);

  if (status != GL_TRUE)
  {
    GLint logLength;
    glGetProgramPipelineiv(m_renderProgram, GL_INFO_LOG_LENGTH, &logLength);
    char *log = new char[logLength];
    glGetProgramPipelineInfoLog(m_renderProgram, logLength, 0, log);
    LOGI("Shader pipeline not valid:\n%s\n", log);
    delete[] log;
  }

  glBindProgramPipeline(0);
  return object;
}
