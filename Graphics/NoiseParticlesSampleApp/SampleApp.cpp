#include "pch.h"
#include "SampleApp.h"
#include "NvShaderUtils.h"
#include "Noise.h"


#include <iostream>
#include <random>
#include <ppl.h>
#include <ppltasks.h>
#include <atomic>

#include <..\g2o\g2o\core\optimizable_graph.h>

NvAppBase* NvAppFactory(NvPlatformContext* platform)
{
  return new nyx::SampleApp(platform);
}

void  nyx::SampleApp::configurationCallback(NvEGLConfiguration& config)
{
  config.depthBits = 24;
  config.stencilBits = 0;
  config.apiVer = NvGfxAPIVersionGL4();
}

nyx::SampleApp::SampleApp(NvPlatformContext* platform) : NvSampleApp(platform)
{
  mEnableAttractor = false;
  mAnimate = true;
  mReset = false;
  mTime = 0.0f;

  // Required in all subclasses to avoid silent link issues
  forceLinkHack();
}

nyx::SampleApp::~SampleApp(void)
{

}

void nyx::SampleApp::initUI(void)
{
  if (mTweakBar) {
    mTweakBar->addPadding();
    mTweakBar->addValue("Animate", mAnimate);
    mTweakBar->addValue("Enable attractor", mEnableAttractor);
    mTweakBar->addPadding();
    mTweakBar->addValue("Sprite size", mShaderParams.spriteSize, 0.0f, 0.04f);
    mTweakBar->addValue("Noise strength", mShaderParams.noiseStrength, 0.0f, 0.01f);
    mTweakBar->addValue("Noise frequency", mShaderParams.noiseFreq, 0.0f, 20.0f);
    mTweakBar->addPadding();
    mTweakBar->addValue("Reset", mReset, true);
  }
}

void nyx::SampleApp::initRendering(void)
{
#if 0
  PiEvaluator piEval;
  unsigned iterationCount = 5;

  while (iterationCount < 10e8)
  {
    concurrency::parallel_invoke(
      [&](){ piEval.evaluate(iterationCount); },
      [&](){ piEval.evaluateInVolume(iterationCount); }
    );

    iterationCount += iterationCount / 3;
  }
#endif


  m_transformer->setTranslationVec(nv::vec3f(0.0f, 0.0f, -3.0f));
  if (!requireMinAPIVersion(NvGfxAPIVersionGL4_4()))
    return;
  NvAssetLoaderAddSearchPath("NoiseParticlesSampleApp");

  std::string renderVS = NvShaderUtils::loadShaderSourceWithUniformTag("shaders/uniforms.h", "shaders/renderVS.glsl");
  mRenderProg = new NvGLSLProgram;

  int32_t len;
  NvGLSLProgram::ShaderSourceItem sources[2];
  sources[0].type = GL_VERTEX_SHADER;
  sources[0].src = renderVS.c_str();
  sources[1].type = GL_FRAGMENT_SHADER;
  sources[1].src = NvAssetLoaderRead("shaders/renderFS.glsl", len);
  mRenderProg->setSourceFromStrings(sources, 2);
  NvAssetLoaderFree((char*)sources[1].src);

  //create ubo and initialize it with the structure data
  glGenBuffers(1, &mUBO);
  glBindBuffer(GL_UNIFORM_BUFFER, mUBO);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(ShaderParams), &mShaderParams, GL_STREAM_DRAW);

  //create simple single-vertex VBO
  float vtx_data[] = { 0.0f, 0.0f, 0.0f, 1.0f };
  glGenBuffers(1, &mVBO);
  glBindBuffer(GL_ARRAY_BUFFER, mVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vtx_data), vtx_data, GL_STATIC_DRAW);

  // For now, scale back the particle count on mobile.
  int32_t particleCount = isMobilePlatform() ? (mNumParticles >> 2) : mNumParticles;

  mParticles = new ParticleSystem(particleCount);

  int cx, cy, cz;
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &cx);
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &cy);
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &cz);
  LOGI("Max compute work group count = %d, %d, %d\n", cx, cy, cz);

  int sx, sy, sz;
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &sx);
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &sy);
  glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &sz);
  LOGI("Max compute work group size  = %d, %d, %d\n", sx, sy, sz);

  CHECK_GL_ERROR();
}

void nyx::SampleApp::draw(void)
{
  glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  if (mReset) {
    mReset = false;
    mParticles->reset(0.5f);
    mTweakBar->syncValues();
  }

  //
  // Compute matrices without the legacy matrix stack support
  //
  nv::matrix4f projectionMatrix;
  nv::perspective(projectionMatrix, 45.0f * 2.0f*3.14159f / 360.0f, (float)m_width / (float)m_height, 0.1f, 10.0f);

  nv::matrix4f viewMatrix = m_transformer->getModelViewMat();

  //
  // update struct representing UBO
  //
  mShaderParams.numParticles = mParticles->getSize();
  mShaderParams.ModelView = viewMatrix;
  mShaderParams.ModelViewProjection = projectionMatrix * viewMatrix;
  mShaderParams.ProjectionMatrix = projectionMatrix;

  if (mEnableAttractor) {
    // move attractor
    const float speed = 0.2f;
    mShaderParams.attractor.x = sin(mTime*speed);
    mShaderParams.attractor.y = sin(mTime*speed*1.3f);
    mShaderParams.attractor.z = cos(mTime*speed);
    mTime += getFrameDeltaTime();

    mShaderParams.attractor.w = 0.0002f;
  }
  else {
    mShaderParams.attractor.w = 0.0f;
  }

  glActiveTexture(GL_TEXTURE0);

  // bind the buffer for the UBO, and update it with the latest values from the CPU-side struct
  glBindBufferBase(GL_UNIFORM_BUFFER, 1, mUBO);
  glBindBuffer(GL_UNIFORM_BUFFER, mUBO);
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(ShaderParams), &mShaderParams);

  if (mAnimate) {
    mParticles->update();
  }

  // draw particles
  mRenderProg->enable();

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);  // additive blend

  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);

  // reference the compute shader buffer, which we will use for the particle
  // locations (only the positions for now)
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, mParticles->getPosBuffer()->getBuffer());

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mParticles->getIndexBuffer()->getBuffer());

  glDrawElements(GL_TRIANGLES, mParticles->getSize() * 6, GL_UNSIGNED_INT, 0);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);

  glDisable(GL_BLEND);

  mRenderProg->disable();
}

void nyx::SampleApp::reshape(
  int32_t width,
  int32_t height
  )
{
  // update projection matrix in camera data
  glViewport(0, 0, (GLint)width, (GLint)height);
}

bool nyx::SampleApp::handlePointerInput(
  NvInputDeviceType::Enum device,
  NvPointerActionType::Enum action,
  uint32_t modifiers, int32_t count,
  NvPointerEvent *points
  )
{

  return false;
}

bool nyx::SampleApp::handleKeyInput(
  uint32_t code,
  NvKeyActionType::Enum action
  )
{

  return false;
}