#pragma once
#include "pch.h"
#include "NvShaderBuffer.h"

namespace nyx
{
  class ParticleSystem
  {
  public:
    typedef NvShaderBuffer<nv::vec4f> NvShaderBufferFloat4;
    typedef NvShaderBuffer<uint32_t> NvShaderBufferUInt;

  public:
    ParticleSystem(size_t size);
    ~ParticleSystem();

  public:
    void loadShaders();
    void reset(float size);
    void update();

  private:
    GLuint createShaderPipelineProgram(GLuint target, char const *src);

  private:
    size_t m_size;
    int m_noiseSize;

    GLuint m_noiseTexture;
    GLuint m_renderProgram;
    GLuint m_updateProgram;

    NvShaderBufferFloat4 *m_pos;
    NvShaderBufferFloat4 *m_vel;
    NvShaderBufferUInt *m_indices;

  public:
    inline size_t getSize() { return m_size; }
    inline NvShaderBufferFloat4 *getPosBuffer() { return m_pos; }
    inline NvShaderBufferFloat4 *getVelBuffer() { return m_vel; }
    inline NvShaderBufferUInt *getIndexBuffer() { return m_indices; }

  };
}