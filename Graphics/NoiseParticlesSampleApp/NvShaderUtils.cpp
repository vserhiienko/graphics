#include "pch.h"
#include "NvShaderUtils.h"

std::string nyx::NvShaderUtils::loadShaderSourceWithUniformTag(
  const char* uniformsFile,
  const char* srcFile
  )
{
  int32_t len;

  char *uniformsStr = NvAssetLoaderRead(uniformsFile, len);
  if (!uniformsStr)
    return "";

  char *srcStr = NvAssetLoaderRead(srcFile, len);
  if (!srcStr)
    return "";

  std::string dest = "";

  const char* uniformTag = "#UNIFORMS";

  char* uniformTagStart = strstr(srcStr, uniformTag);

  if (uniformTagStart) {
    // NULL the start of the tag
    *uniformTagStart = 0;
    dest += srcStr; // source up to tag
    dest += "\n";
    dest += uniformsStr;
    dest += "\n";
    char* uniformTagEnd = uniformTagStart + strlen(uniformTag);
    dest += uniformTagEnd;
  }
  else {
    dest += srcStr;
  }

  NvAssetLoaderFree(uniformsStr);
  NvAssetLoaderFree(srcStr);

  return dest;
}