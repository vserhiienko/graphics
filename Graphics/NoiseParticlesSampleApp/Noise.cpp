#include "pch.h"
#include "Noise.h"

#include <iostream>
#include <random>
#include <ppl.h>
#include <ppltasks.h>
#include <atomic>

#pragma region noise context

const int nyx::NoiseContext::permutation[] =
{
  151, 160, 137, 91, 90, 15,
  131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
  190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
  88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
  77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
  102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
  135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
  5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
  223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
  129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
  251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
  49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
  138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
};

const int nyx::NoiseContext::p[] =
{
  // 256
  151, 160, 137, 91, 90, 15,
  131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
  190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
  88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
  77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
  102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
  135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
  5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
  223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
  129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
  251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
  49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
  138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180,
  151, 160, 137, 91, 90, 15,
  // 256
  131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
  190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
  88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
  77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
  102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
  135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
  5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
  223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
  129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
  251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
  49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
  138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
};

#pragma endregion

float nyx::Noise::fade(float t)
{
  return t * t * t * (t * (t * 6 - 15) + 10);
}

float nyx::Noise::lerp(float t, float a, float b)
{
  return a + t * (b - a);
}

float nyx::Noise::grad(int hash, float x, float y, float z)
{
  int h = hash & 15;
  float u = h < 8 ? x : y, v = h < 4 ? y : h == 12 || h == 14 ? x : z;
  return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

float nyx::Noise::frand()
{
  return rand() / (float)RAND_MAX;
}

float nyx::Noise::sfrand()
{
  return frand() * 2.0f - 1.0f;
}

nyx::Noise::Noise()
{
  detach();
}

nyx::Noise::~Noise()
{
  release();
}

void nyx::Noise::release()
{
  if (m_noise)
  {
    delete[] m_noise;
    detach();
  }
}

void nyx::Noise::detach()
{
  m_noise = NULL;
  m_width = 0;
  m_height = 0;
  m_depth = 1;
  m_dim = 1;
}

float *nyx::Noise::getNoiseValues()
{
  return m_noise;
}

float const *nyx::Noise::getNoiseValues() const
{
  return m_noise;
}

void nyx::Noise::createSimpleNoise(int w, int h, int dim)
{
  release();
  m_width = w;
  m_height = h;
  m_depth = 1;
  m_dim = dim;
  m_noise = new float[w * h * dim];
  float *ptr = m_noise;

  for (int y = 0; y < h; y++)
  for (int x = 0; x < w; x++)
  for (int v = 0; v < dim; v++)
    *ptr++ = sfrand();
}

void nyx::Noise::createSimpleNoise(int w, int h, int d, int dim)
{
  release();

  m_width = w;
  m_height = h;
  m_depth = d;
  m_dim = dim;
  m_noise = new float[w * h * d * dim];
  float *ptr = m_noise;

  for (int z = 0; z < d; z++)
  for (int y = 0; y < h; y++)
  for (int x = 0; x < w; x++)
  for (int v = 0; v < dim; v++)
    *ptr++ = sfrand();
}

float nyx::Noise::noise(float x, float y, float z)
{
  int const
    X = (int)floorf(x) & 255,  // FIND UNIT CUBE THAT
    Y = (int)floorf(y) & 255,  // CONTAINS POINT.
    Z = (int)floorf(z) & 255,
    *p = NoiseContext::p;

  x -= floorf(x);  // FIND RELATIVE X,Y,Z
  y -= floorf(y);  // OF POINT IN CUBE.
  z -= floorf(z);

  float const
    u = fade(x),  // COMPUTE FADE CURVES
    v = fade(y),  // FOR EACH OF X,Y,Z.
    w = fade(z);

  int
    A = p[X] + Y,
    AA = p[A] + Z,
    AB = p[A + 1] + Z,  // HASH COORDINATES OF
    B = p[X + 1] + Y,
    BA = p[B] + Z,
    BB = p[B + 1] + Z;  // THE 8 CUBE CORNERS,

  return
    lerp(
    w, lerp(v, lerp(u, grad(p[AA], x, y, z),        // AND ADD
    grad(p[BA], x - 1, y, z)),                      // BLENDED
    lerp(u, grad(p[AB], x, y - 1, z),               // RESULTS
    grad(p[BB], x - 1, y - 1, z))),                 // FROM  8
    lerp(v, lerp(u, grad(p[AA + 1], x, y, z - 1),   // CORNERS
    grad(p[BA + 1], x - 1, y, z - 1)),              // OF CUBE
    lerp(u, grad(p[AB + 1], x, y - 1, z - 1),
    grad(p[BB + 1], x - 1, y - 1, z - 1)))
    );
}

GLuint nyx::NoiseResource::createNoiseTextureAuto(Noise const *noise)
{
  if (noise->getDepth() == 1)
  { // 2d
    if (noise->getDim() == 1) return createNoiseTexture2D_r8(noise->getNoiseValues(), noise->getWidth(), noise->getHeight(), false);
    else if (noise->getDim() == 4) return createNoiseTexture2D_rgba8(noise->getNoiseValues(), noise->getWidth(), noise->getHeight(), false);
  }
  else if (noise->getDepth() > 1)
  { // 3d
    if (noise->getDim() == 1) return createNoiseTexture3D_r8(noise->getNoiseValues(), noise->getWidth(), noise->getHeight(), noise->getDepth(), false);
    else if (noise->getDim() == 4) return createNoiseTexture3D_rgba8(noise->getNoiseValues(), noise->getWidth(), noise->getHeight(), noise->getDepth(), false);
  }

  return NULL;
}

GLuint nyx::NoiseResource::createNoiseTexture2D(float const *data, int w, int h, GLint internalFormat, GLint format, bool mipmap)
{
  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);

  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0, format, GL_FLOAT, data);
  if (mipmap) { glGenerateMipmap(GL_TEXTURE_2D); }

  return tex;
}

GLuint nyx::NoiseResource::createNoiseTexture3D(float const *data, int w, int h, int d, GLint internalFormat, GLint format, bool mipmap)
{
  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_3D, tex);

  //glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
  //glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexImage3D(GL_TEXTURE_3D, 0, internalFormat, w, h, d, 0, format, GL_FLOAT, data);
  if (mipmap) { glGenerateMipmap(GL_TEXTURE_3D); }

  return tex;
}

struct cube
{
  float x, y, z, w, h, d;
  bool isInside(nv::vec3f const &p)
  {
    return
      (p.x >= x && p.x <= (x + w)) &&
      (p.y >= y && p.y <= (y + h)) &&
      (p.z >= z && p.z <= (z + d));
  }
};

struct rect
{
  float x, y, w, h;
  bool isInside(nv::vec2f const &p)
  {
    return
      (p.x >= x && p.x <= (x + w)) &&
      (p.y >= y && p.y <= (y + h));
  }
};

void nyx::PiEvaluator::evaluate(unsigned iterationCount)
{
  float radius = 1.0f;

  rect r;
  r.x = -radius;
  r.y = -radius;
  r.w = radius * 2;
  r.h = radius * 2;

  std::default_random_engine generator;
  std::uniform_real_distribution<float> distribution(-radius * 1.5f, radius * 1.5f);

  unsigned circleCount = 0, quadCount = 0;
  for (unsigned i = 0u; i < iterationCount; i++)
  {
    nv::vec2f p;
    p.x = distribution(generator); // Noise::sfrand();
    p.y = distribution(generator); // Noise::sfrand();
    quadCount += !!(r.isInside(p));
    circleCount += !!(nv::length(p) <= radius);
  }

  float pi = float(circleCount) / float(quadCount) * 4.0f; // / 2.0f * 3.0f;
  NVWindowsLog("pi=%f error=%f i=%u", pi, fabs(NV_PI - pi), iterationCount);
}

void nyx::PiEvaluator::evaluateInVolume(unsigned iterationCount)
{
  float radius = 1.0f;

  cube r;
  r.x = -radius;
  r.y = -radius;
  r.z = -radius;
  r.w = radius * 2;
  r.h = radius * 2;
  r.d = radius * 2;

  std::default_random_engine generator;
  std::uniform_real_distribution<float> distribution(-radius * 1.5f, radius * 1.5f);

  unsigned circleCount = 0, quadCount = 0;
  for (unsigned i = 0u; i < iterationCount; i++)
  {
    nv::vec3f p;
    p.x = distribution(generator); // Noise::sfrand();
    p.y = distribution(generator); // Noise::sfrand();
    p.z = distribution(generator); // Noise::sfrand();
    quadCount += !!(r.isInside(p));
    circleCount += !!(nv::length(p) <= radius);
  }

  float pi = float(circleCount) / float(quadCount) * 8.0f * (3.0f / 4.0f);
  NVWindowsLog("pi=%f error=%f i=%u", pi, fabs(NV_PI - pi), iterationCount);
}
