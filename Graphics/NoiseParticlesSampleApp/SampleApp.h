#pragma once
#include "pch.h"
#include "ParticleSystem.h"
#include <NV/NvShaderMappings.h>
#include "assets/shaders/uniforms.h"

namespace nyx
{
  class SampleApp : public NvSampleApp
  {
  public: // NvSampleApp, NvAppBase 

    SampleApp(NvPlatformContext* platform);
    ~SampleApp(void);

    virtual void initUI(void) override;
    virtual void initRendering(void) override;
    virtual void draw(void) override;

    void configurationCallback(
      NvEGLConfiguration &config
      );

    virtual void reshape(
      int32_t width,
      int32_t height
      ) override;
    virtual bool handlePointerInput(
      NvInputDeviceType::Enum device,
      NvPointerActionType::Enum action,
      uint32_t modifiers, int32_t count,
      NvPointerEvent *points
      );
    virtual bool handleKeyInput(
      uint32_t code,
      NvKeyActionType::Enum action
      ) override;

  private:
    const static int mNumParticles = 1 << 20;

    NvGLSLProgram *mRenderProg;
    ParticleSystem *mParticles;
    ShaderParams mShaderParams;
    GLuint mUBO;
    GLuint mVBO;

    bool mEnableAttractor;
    bool mAnimate;
    bool mReset;
    float mTime;

  };
}