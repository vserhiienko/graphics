SamplerState linearSampler: register (s0);
Texture2D<float4> waterSimulation: register (t0);

//cbuffer SimulationData : register (cb0)
//{
//  float4 vectorPatch0;
//  float4 vectorPatch1;
//  float4 vectorPatch2;
//};
//
//float2 getSize() { return vectorPatch0.xy; }
//float2 getDelta() { return vectorPatch0.zw; }
//float2 getPosition() { return vectorPatch1.xz; }
//float getRadius() { return vectorPatch1.w; }
//float getStrength() { return vectorPatch1.z; }
//float getElapsedTime() { return vectorPatch2.x; }
//float2 getCoeffs() { return vectorPatch2.yz; }
//float2 getDamping() { return vectorPatch2.w; }

struct PSIn
{
  float4 positionClip : SV_POSITION;
  float2 textureCoords : TEXCOORD;
};

float4 main(in PSIn input) : SV_TARGET
{
  float4 outputColor = waterSimulation.Sample(linearSampler, input.textureCoords);
  if (outputColor.x >= 1.0f || outputColor.x <= 0.0f) outputColor.x = 0.001f;
  if (outputColor.y >= 1.0f || outputColor.y <= 0.0f) outputColor.y = 0.001f;
  if (outputColor.z >= 1.0f || outputColor.z <= 0.0f) outputColor.z = 0.001f;
  outputColor.x += 0.0003f; //getDelta().x;
  outputColor.y += 0.0003f; //getDelta().y;
  outputColor.z += 0.0003f;
  outputColor.w = 1.0f;
  return outputColor;
  //return outputColor;
}