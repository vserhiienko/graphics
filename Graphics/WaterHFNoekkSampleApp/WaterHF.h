#pragma once
#include <DxMath.h>
#include <DxAssetManager.h>
#include <DxUtils.h>

#include <NoekkTimer.h>
#include <NoekkSampleApp.h>
#include <NoekkResourceAssist.h>

namespace soko
{
  class WaterHFSimulationSample : protected noekk::SampleApp
  {
    struct VertexShaderPerFrameData
    {
      DirectX::XMMATRIX matrixPatch[3];

      void setWorld(DirectX::Matrix const &);
      void setCameraView(DirectX::Matrix const &);
      void setCameraProjection(DirectX::Matrix const &);
    };

    struct PixelShaderPerFrameData
      //: noekk::AlignedNew<SimulationPerFrameData>
    {
      DirectX::XMVECTOR vectorPatch[3];
      //float _padding[(256 - sizeof(DirectX::XMVECTOR[3])) / 4];

      void setSize(DirectX::Vector2 const &);
      void setDelta(DirectX::Vector2 const &);
      void setPosition(DirectX::Vector2 const &);
      void setStrength(float);
      void setRadius(float);
      void setElapsedTime(float);
      void setCoeffs(float, float);
      void setDamping(float);
    };

    struct WaterSimulationStateResources;
    friend WaterSimulationStateResources;

    struct WaterSimulationStateResources
    {
      friend WaterHFSimulationSample;

      enum StateKind { kStateKindInit, kStateKindRead, kStateKindWrite };

      WaterHFSimulationSample &deviceResource;

      Microsoft::WRL::ComPtr<ID3D12CommandAllocator> simulateBundleAlloc;
      Microsoft::WRL::ComPtr<ID3D12CommandList> stepPassBundle;
      Microsoft::WRL::ComPtr<ID3D12CommandList> dropPassBundle;

      Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> depthStencilDH;
      Microsoft::WRL::ComPtr<ID3D12Resource> depthStencilResource;

      // water simulation information storage
      Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> renderTargetDH;
      Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> resourcesDH;
      Microsoft::WRL::ComPtr<ID3D12Resource> frameVSUH;
      Microsoft::WRL::ComPtr<ID3D12Resource> framePSUH;
      Microsoft::WRL::ComPtr<ID3D12Resource> resource;
      PixelShaderPerFrameData *frameUHStart;
      //uint8_t *frameUHStart;

      WaterSimulationStateResources *referenceStateR;
      StateKind stateKind;
      uint64_t fence;

      WaterSimulationStateResources(WaterHFSimulationSample &);
      void initializeResourceBarriersForWriting(void); // init -> render target
      void initializeResourceBarriersForReading(void); // init -> pixel shader resource
      void advanceResourceBarriersForWriting(void); // pixel shader resource -> render target
      void advanceResourceBarriersForReading(void); // render target -> pixel shader resource

    };

    // common
    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> linearSamplerDH;
    Microsoft::WRL::ComPtr<ID3D12Resource> vertexBuffer;
    Microsoft::WRL::ComPtr<ID3D12Resource> indexBuffer;
    D3D12_VERTEX_BUFFER_VIEW_DESC vertexBufferView;
    D3D12_INDEX_BUFFER_VIEW_DESC indexBufferView;
    uint32_t descriptorSize;
    uint64_t fence;

    // simulation
    static DXGI_FORMAT const simulateResFormat = DXGI_FORMAT_R16G16B16A16_FLOAT;
    static uint32_t const simulateResX = 256u;
    static uint32_t const simulateResY = 256u;
    VertexShaderPerFrameData simulationPerVSFrameData;
    PixelShaderPerFrameData simulationPerPSFrameData;

    Microsoft::WRL::ComPtr<ID3D12RootSignature> simulateRS;
    Microsoft::WRL::ComPtr<ID3D12PipelineState> stepPassPSO;
    Microsoft::WRL::ComPtr<ID3D12PipelineState> dropPassPSO;
    Microsoft::WRL::ComPtr<ID3D12PipelineState> gradPassPSO;
    Microsoft::WRL::ComPtr<ID3D12Resource> simulationVSUH;
    WaterSimulationStateResources *waterStateRefs[2];
    WaterSimulationStateResources waterStateW;
    WaterSimulationStateResources waterStateR;

    // rendering
    VertexShaderPerFrameData renderingPerVSFrameData;
    Microsoft::WRL::ComPtr<ID3D12RootSignature> renderRS;
    Microsoft::WRL::ComPtr<ID3D12PipelineState> renderPassPSO;
    Microsoft::WRL::ComPtr<ID3D12Resource> renderingVSUH;

    bool mouseLBPressed;
    noekk::Vector2 screen;
    noekk::Vector2 mouseLastPosition;
    noekk::sim::BasicTimer timer;

    void loadAssets();
    void createSamplerState(void);
    void createSimulationPasses(void);
    void createSimulationMainResources(WaterSimulationStateResources &);
    void createSimulationRootSignature(void);
    void createSimulationBundle(WaterSimulationStateResources &);
    void createVertexBuffers(void);
    void swapWaterSimulationStates(void);

    void updateFrameBuffers(PixelShaderPerFrameData *);
    void populateSimulationCommandList(void);
    void stepSimulation(void);

    void createRenderingRootSignature(void);
    void createRenderingPipeline(void);

    void populateRenderingCommandList(void);
    void renderSimulation(void);

  public:
    WaterHFSimulationSample(void);
    void initializeSample(void);
    void runSample(void);

  public:
    virtual void handlePaint(noekk::Window const *);
    virtual void handleKeyReleased(noekk::Window const *, noekk::GenericEventArgs const &);
    virtual void handleMouseMoved(noekk::Window const *, noekk::GenericEventArgs const &);
    virtual void handleMouseLeftPressed(noekk::Window const *, noekk::GenericEventArgs const &);
    virtual void handleMouseLeftReleased(noekk::Window const *, noekk::GenericEventArgs const &);

  };
}
