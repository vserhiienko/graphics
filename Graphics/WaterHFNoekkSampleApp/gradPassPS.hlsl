
struct PSIn
{
  float4 positionDevice : SV_POSITION;
  float3 normalWorld : NORMAL;
  float2 textureCoords : TEXCOORD;
};

SamplerState linearSampler;
Texture2D<float4> waterSimulation;

cbuffer SimulationData
{
  float4 vectorPatch0;
  float4 vectorPatch1;
  float4 vectorPatch2;
};

static const float pi = 3.141592653589793;
float2 getSize() { return vectorPatch0.xy; }
float2 getDelta() { return vectorPatch0.zw; }
float2 getPosition() { return vectorPatch1.xy; }
float getStrength() { return vectorPatch1.z; }
float getRadius() { return vectorPatch1.w; }
float getElapsedTime() { return vectorPatch2.x; }
float2 getCoeffs() { return vectorPatch2.yz; }
float getDamping() { return vectorPatch2.w; }

float4 main(in PSIn input) : SV_TARGET
{
  float4 uij = waterSimulation.Sample(linearSampler, input.textureCoords);

  float2 horzShift = float2(getDelta().x, 0.0);
  float2 vertShift = float2(0.0, getDelta().y);
  float uim1jx = waterSimulation.Sample(linearSampler, input.textureCoords - vertShift).r;
  float uip1jx = waterSimulation.Sample(linearSampler, input.textureCoords + vertShift).r;
  float uijm1x = waterSimulation.Sample(linearSampler, input.textureCoords - horzShift).r;
  float uijp1x = waterSimulation.Sample(linearSampler, input.textureCoords + horzShift).r;

  uij.g = uijp1x - uijm1x;
  uij.a = uip1jx - uim1jx;
  return uij;
}