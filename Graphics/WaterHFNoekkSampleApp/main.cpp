
#define _CRTDBG_MAP_ALLOC
#include <iostream>
#include <stdlib.h>
#include <crtdbg.h>
#include "WaterHF.h"

void setCheckForMemortLeaks()
{
  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
}

int main(int argc, char *argv[])
{
#if _DEBUG
  //setCheckForMemortLeaks();
#endif

  soko::WaterHFSimulationSample app;
  app.initializeSample();
  app.runSample();
  return EXIT_SUCCESS;
}