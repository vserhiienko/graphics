
struct VSIn
{
  float3 position : POSITION;
  float3 normal : NORMAL;
  float2 textureCoords : TEXCOORD;
};

struct PSIn
{
  float4 positionClip : SV_POSITION;
  float3 normalWorld : NORMAL;
  float2 textureCoords : TEXCOORD;
};

cbuffer FrameBuffer
{
  row_major float4x4 world;
  row_major float4x4 cameraView;
  row_major float4x4 cameraProjection;
};

PSIn main(in VSIn input)
{
  PSIn output;
  output.textureCoords = input.textureCoords;
  output.normalWorld = mul(input.normal, (float3x3)world);
  output.positionClip = float4(input.position, 1);
  output.positionClip = mul(output.positionClip, world);
  output.positionClip = mul(output.positionClip, cameraView);
  output.positionClip = mul(output.positionClip, cameraProjection);
	return output;
}