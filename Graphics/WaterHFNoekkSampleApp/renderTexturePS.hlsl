SamplerState linearSampler;
Texture2D<float4> waterSimulation;

struct PSIn
{
  float4 positionDevice : SV_POSITION;
  float3 normalWorld : NORMAL;
  float2 textureCoords : TEXCOORD;
};

float4 main(in PSIn input) : SV_TARGET
{
  float4 outputColor = waterSimulation.Sample(linearSampler, input.textureCoords);
  return abs(outputColor);
}