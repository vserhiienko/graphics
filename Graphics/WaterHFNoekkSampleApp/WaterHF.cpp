#include "WaterHF.h"

#include <new>
#include <GeometryPrimitives.h>

#define _getWaterStateW (*waterStateRefs[0])
#define _getWaterStateR (*waterStateRefs[1])

soko::WaterHFSimulationSample::WaterSimulationStateResources::WaterSimulationStateResources(
  WaterHFSimulationSample & sample
  )
  : deviceResource(sample)
{
  stateKind = kStateKindInit;
}

void soko::WaterHFSimulationSample::createSimulationPasses()
{
  noekk::HResult result;

  noekk::LoadShaderBlobHook shaderLoadCallback;

  CD3D12_BLEND_DESC blendStateDesc(D3D12_DEFAULT);
  CD3D12_DEPTH_STENCIL_DESC depthStencilStateDesc(D3D12_DEFAULT);
  CD3D12_RASTERIZER_DESC rasterizerStateDesc(D3D12_DEFAULT);

  noekk::AssetReader::findAndReadTo(L"shaders/renderToTextureVS.cso", shaderLoadCallback.vsAssetId, &shaderLoadCallback);


  const auto inputElementCount = noekk::VertexPositionNormalTexture::inputElementCount;
  D3D12_INPUT_ELEMENT_DESC d3d12InputElements[inputElementCount];
  noekk::VertexPositionNormalTexture::setD3DInputElements(d3d12InputElements);

  D3D12_GRAPHICS_PIPELINE_STATE_DESC simulationPipelineStateDesc;
  noekk::zeroMemory(simulationPipelineStateDesc);
  simulationPipelineStateDesc.NumRenderTargets = 1;
  simulationPipelineStateDesc.pRootSignature = simulateRS.Get();
  simulationPipelineStateDesc.RTVFormats[0] = simulateResFormat;
  simulationPipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
  simulationPipelineStateDesc.VS = { shaderLoadCallback.vsBlob.data, shaderLoadCallback.vsBlob.dataLength };
  simulationPipelineStateDesc.InputLayout = { d3d12InputElements, inputElementCount };
  simulationPipelineStateDesc.DepthStencilState = depthStencilStateDesc;
  simulationPipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
  simulationPipelineStateDesc.RasterizerState = rasterizerStateDesc;
  simulationPipelineStateDesc.BlendState = blendStateDesc;
  simulationPipelineStateDesc.SampleMask = UINT_MAX;
  simulationPipelineStateDesc.SampleDesc.Count = 1;

  noekk::AssetReader::findAndReadTo(L"shaders/stepPassPS.cso", shaderLoadCallback.psAssetId, &shaderLoadCallback);
  simulationPipelineStateDesc.PS = { shaderLoadCallback.psBlob.data, shaderLoadCallback.psBlob.dataLength };
  result = device->CreateGraphicsPipelineState(&simulationPipelineStateDesc, &stepPassPSO);

  noekk::AssetReader::findAndReadTo(L"shaders/gradPassPS.cso", shaderLoadCallback.psAssetId, &shaderLoadCallback);
  simulationPipelineStateDesc.PS = { shaderLoadCallback.psBlob.data, shaderLoadCallback.psBlob.dataLength };
  result = device->CreateGraphicsPipelineState(&simulationPipelineStateDesc, &gradPassPSO);

  noekk::AssetReader::findAndReadTo(L"shaders/dropPassPS.cso", shaderLoadCallback.psAssetId, &shaderLoadCallback);
  simulationPipelineStateDesc.PS = { shaderLoadCallback.psBlob.data, shaderLoadCallback.psBlob.dataLength };
  result = device->CreateGraphicsPipelineState(&simulationPipelineStateDesc, &dropPassPSO);

}

void soko::WaterHFSimulationSample::createSimulationRootSignature()
{
  using Microsoft::WRL::ComPtr;

  noekk::HResult result;
  ComPtr<ID3DBlob> outBlob;
  ComPtr<ID3DBlob> errBlob;

  D3D12_DESCRIPTOR_RANGE descRange[4];
  D3D12_ROOT_PARAMETER rootParameters[4];
  D3D12_ROOT_SIGNATURE descRootSignature;
  descRange[0].Init(D3D12_DESCRIPTOR_RANGE_CBV, 1, 0); // cbv vs
  descRange[1].Init(D3D12_DESCRIPTOR_RANGE_CBV, 1, 0); // cbv ps
  descRange[2].Init(D3D12_DESCRIPTOR_RANGE_SRV, 1, 0); // srv
  descRange[3].Init(D3D12_DESCRIPTOR_RANGE_SAMPLER, 1, 0); // ss
  rootParameters[0].InitAsDescriptorTable(1, &descRange[0], D3D12_SHADER_VISIBILITY_VERTEX);
  rootParameters[1].InitAsDescriptorTable(1, &descRange[1], D3D12_SHADER_VISIBILITY_PIXEL);
  rootParameters[2].InitAsDescriptorTable(1, &descRange[2], D3D12_SHADER_VISIBILITY_PIXEL);
  rootParameters[3].InitAsDescriptorTable(1, &descRange[2], D3D12_SHADER_VISIBILITY_PIXEL);
  descRootSignature.Init(4, rootParameters, 0);

  result = D3D12SerializeRootSignature(
    &descRootSignature,
    D3D_ROOT_SIGNATURE_V1,
    outBlob.GetAddressOf(),
    errBlob.GetAddressOf()
    );
  result = device->CreateRootSignature(
    outBlob->GetBufferPointer(),
    outBlob->GetBufferSize(),
    __uuidof(ID3D12RootSignature),
    (void**) &simulateRS
    );
}

void soko::WaterHFSimulationSample::createSimulationBundle(WaterSimulationStateResources &stateResources)
{
  noekk::HResult result;

  result = device->CreateCommandAllocator(
    D3D12_COMMAND_LIST_TYPE_BUNDLE,
    &stateResources.simulateBundleAlloc
    );
  result = device->CreateCommandList(
    D3D12_COMMAND_LIST_TYPE_BUNDLE,
    stateResources.simulateBundleAlloc.Get(),
    stepPassPSO.Get(),
    &stateResources.stepPassBundle
    );

  // populate bundle

  auto handleForCBVVS = stateResources.referenceStateR->resourcesDH->GetGPUDescriptorHandleForHeapStart();
  auto handleForCBVPS = handleForCBVVS.MakeOffsetted(1, descriptorSize);
  auto handleForSRV = handleForCBVPS.MakeOffsetted(1, descriptorSize);
  auto handleForSS = linearSamplerDH->GetGPUDescriptorHandleForHeapStart();

  stateResources.stepPassBundle->SetGraphicsRootSignature(simulateRS.Get());
  stateResources.stepPassBundle->SetGraphicsRootDescriptorTable(0, handleForCBVVS);
  stateResources.stepPassBundle->SetGraphicsRootDescriptorTable(1, handleForCBVPS);
  stateResources.stepPassBundle->SetGraphicsRootDescriptorTable(2, handleForSRV);
  stateResources.stepPassBundle->SetGraphicsRootDescriptorTable(3, handleForSS);
  stateResources.stepPassBundle->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  stateResources.stepPassBundle->SetVertexBuffersSingleUse(0, vertexBuffer.GetAddressOf(), &vertexBufferView, 1);
  stateResources.stepPassBundle->SetIndexBufferSingleUse(indexBuffer.Get(), &indexBufferView);
  stateResources.stepPassBundle->SetPipelineState(dropPassPSO.Get());
  stateResources.stepPassBundle->DrawInstanced(6, 1, 0, 0);
  result = stateResources.stepPassBundle->Close();

}

void soko::WaterHFSimulationSample::updateFrameBuffers(PixelShaderPerFrameData *frameUHStart)
{
  memcpy(frameUHStart, &simulationPerPSFrameData, sizeof(PixelShaderPerFrameData));
}

void soko::WaterHFSimulationSample::populateSimulationCommandList(void)
{
  commandList->RSSetViewports(1, &viewport);
  commandList->RSSetScissorRects(1, &rectScissor);

  D3D12_CPU_DESCRIPTOR_HANDLE RTVDescriptors [] = { _getWaterStateW.renderTargetDH->GetCPUDescriptorHandleForHeapStart() };
  D3D12_CPU_DESCRIPTOR_HANDLE DSVDescriptors [] = { _getWaterStateW.depthStencilDH->GetCPUDescriptorHandleForHeapStart() };
  commandList->SetRenderTargets(RTVDescriptors, false, 1, DSVDescriptors);

  FLOAT clearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
  ID3D12DescriptorHeap *descHeaps [] =
  {
    _getWaterStateR.resourcesDH.Get(),
    linearSamplerDH.Get(),
  };

  commandList->ClearRenderTargetView(_getWaterStateW.renderTargetDH->GetCPUDescriptorHandleForHeapStart(), clearColor, nullptr, 0);
  commandList->ClearDepthStencilView(_getWaterStateW.depthStencilDH->GetCPUDescriptorHandleForHeapStart(), D3D11_CLEAR_DEPTH, 1.0f, 0, nullptr, 0);

  commandList->SetDescriptorHeaps(descHeaps, ARRAYSIZE(descHeaps));
  commandList->ExecuteBundle(_getWaterStateW.stepPassBundle.Get());

  _getWaterStateW.advanceResourceBarriersForReading();
  _getWaterStateR.advanceResourceBarriersForWriting();
}

void soko::WaterHFSimulationSample::populateRenderingCommandList(void)
{
  commandList->RSSetViewports(1, &viewport);
  commandList->RSSetScissorRects(1, &rectScissor);

  D3D12_CPU_DESCRIPTOR_HANDLE RTVDescriptors [] = { renderTargetDescHeap->GetCPUDescriptorHandleForHeapStart() };
  D3D12_CPU_DESCRIPTOR_HANDLE DSVDescriptors [] = { depthStencilDescHeap->GetCPUDescriptorHandleForHeapStart() };
  commandList->SetRenderTargets(RTVDescriptors, false, 1, DSVDescriptors);

  // indicate that the render target will now be used as the render target
  setResourceBarrier(commandList, renderTarget, D3D12_RESOURCE_USAGE_PRESENT, D3D12_RESOURCE_USAGE_RENDER_TARGET);

  // clear render target and depth stencil
  FLOAT clearColor[4] = { 0.15f, 0.125f, 0.3f, 1.0f };
  ID3D12DescriptorHeap *descHeaps [] =
  {
    _getWaterStateR.resourcesDH.Get(),
    linearSamplerDH.Get()
  };

  commandList->ClearRenderTargetView(renderTargetDescHeap->GetCPUDescriptorHandleForHeapStart(), clearColor, nullptr, 0);
  commandList->ClearDepthStencilView(depthStencilDescHeap->GetCPUDescriptorHandleForHeapStart(), D3D11_CLEAR_DEPTH, 1.0f, 0, nullptr, 0);
  commandList->SetDescriptorHeaps(descHeaps, ARRAYSIZE(descHeaps));
  //commandList->ExecuteBundle(renderBundle.Get());

  commandList->SetGraphicsRootSignature(renderRS.Get());
  commandList->SetPipelineState(renderPassPSO.Get());
  commandList->SetGraphicsRootDescriptorTable(0, _getWaterStateR.resourcesDH->GetGPUDescriptorHandleForHeapStart());
  commandList->SetGraphicsRootDescriptorTable(1, linearSamplerDH->GetGPUDescriptorHandleForHeapStart());
  commandList->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  commandList->SetVertexBuffersSingleUse(0, vertexBuffer.GetAddressOf(), &vertexBufferView, 1);
  commandList->SetIndexBufferSingleUse(indexBuffer.Get(), &indexBufferView);
  commandList->DrawInstanced(6, 1, 0, 0);

  setResourceBarrier(commandList, renderTarget, D3D12_RESOURCE_USAGE_RENDER_TARGET, D3D12_RESOURCE_USAGE_PRESENT);
}

void soko::WaterHFSimulationSample::stepSimulation(void)
{
  using Microsoft::WRL::ComPtr;
  if (commandList.Get() == 0) return;

  updateFrameBuffers(_getWaterStateR.frameUHStart);
  updateFrameBuffers(_getWaterStateW.frameUHStart);
  populateSimulationCommandList();
}

void soko::WaterHFSimulationSample::renderSimulation(void)
{
  using Microsoft::WRL::ComPtr;
  if (commandList.Get() == 0) return;

  populateRenderingCommandList();
}

void soko::WaterHFSimulationSample::swapWaterSimulationStates()
{
  std::swap(
    waterStateRefs[0],
    waterStateRefs[1]
    );
}

void soko::WaterHFSimulationSample::handleMouseMoved(noekk::Window const *, noekk::GenericEventArgs const &args)
{
  if (mouseLBPressed)
  {
    mouseLastPosition.x = args.mouseX();
    mouseLastPosition.y = args.mouseY();
    noekk::Vector2 screen = { (float) backbufferW , (float) backbufferH };
    noekk::Vector2 mappedPos = mouseLastPosition / screen;
    simulationPerPSFrameData.setPosition(mappedPos);
    noekk::trace("pos: %f %f", mappedPos.x, mappedPos.y);
  }
}

void soko::WaterHFSimulationSample::handleMouseLeftPressed(noekk::Window const *, noekk::GenericEventArgs const &args)
{
  if (mouseLBPressed == false)
  {
    mouseLBPressed = true;
    simulationPerPSFrameData.setStrength(0.33f);
  }
}

void soko::WaterHFSimulationSample::handleMouseLeftReleased(noekk::Window const *, noekk::GenericEventArgs const &args)
{
  mouseLBPressed = false;
  simulationPerPSFrameData.setStrength(0.00f);
}

void soko::WaterHFSimulationSample::handlePaint(noekk::Window const *)
{
  static unsigned backbufferIndex = 0;
  static unsigned backbufferCount = 2;
  if (commandList.Get() == 0) return;

  timer.update();
  simulationPerPSFrameData.setElapsedTime(timer.elapsed);

  noekk::HResult result;
  result = commandList->Reset(commandAllocator.Get(), stepPassPSO.Get());

  stepSimulation();
  renderSimulation();
  result = commandList->Close();
  commandQueue->ExecuteCommandList(commandList.Get());

  result = (swapChain->Present(1, 0));
  backbufferIndex = (1 + backbufferIndex) % backbufferCount;
  swapChain->GetBuffer(backbufferIndex, IID_PPV_ARGS(renderTarget.ReleaseAndGetAddressOf()));
  device->CreateRenderTargetView(renderTarget.Get(), nullptr, renderTargetDescHeap->GetCPUDescriptorHandleForHeapStart());

  awaitGPUResources();
  swapWaterSimulationStates();

  // print framerate

  static unsigned frameIndex = 0;
  static unsigned printFPSFrameIndex = 120;
  if (frameIndex == printFPSFrameIndex) noekk::trace("fps: %d", timer.framesPerSecond), frameIndex = 0;
  else frameIndex++;
}

void soko::WaterHFSimulationSample::loadAssets()
{
  using Microsoft::WRL::ComPtr;
  noekk::HResult result;

  mouseLBPressed = false;
  descriptorSize = device->GetDescriptorHandleIncrementSize(D3D12_CBV_SRV_UAV_DESCRIPTOR_HEAP);

  float resX = (float) simulateResX;
  float resY = (float) simulateResY;

  DirectX::Vector3 eye = { 0.0f, 0.0, -1.f };
  DirectX::Vector3 focus = { 0.0f, 0.0, +1.f };
  DirectX::Vector3 up = { 0.0f, 1.0, 0.0 };

  noekk::zeroMemory(renderingPerVSFrameData);
  noekk::zeroMemory(simulationPerVSFrameData);
  noekk::zeroMemory(simulationPerPSFrameData);

  renderingPerVSFrameData.setWorld(DirectX::XMMatrixIdentity());
  simulationPerVSFrameData.setWorld(DirectX::XMMatrixIdentity());
  renderingPerVSFrameData.setCameraView(DirectX::XMMatrixLookAtLH(eye, focus, up));
  simulationPerVSFrameData.setCameraView(DirectX::XMMatrixLookAtLH(eye, focus, up));
  renderingPerVSFrameData.setCameraProjection(DirectX::XMMatrixOrthographicLH((float) backbufferW, (float) backbufferH, 0.01f, 100.0f));
  simulationPerVSFrameData.setCameraProjection(DirectX::XMMatrixOrthographicLH((float) simulateResX, (float) simulateResY, 0.01f, 100.0f));

  simulationPerPSFrameData.setSize({ resX, resY });
  simulationPerPSFrameData.setDelta({ 1.0f / resX, 1.0f / resY });
  simulationPerPSFrameData.setCoeffs(0.5f, 1.0f);
  simulationPerPSFrameData.setDamping(0.999f);
  simulationPerPSFrameData.setRadius(0.33f);
  simulationPerPSFrameData.setStrength(0.00f);
  simulationPerPSFrameData.setPosition({ 0.5f, 0.5f });


  noekk::AssetReader::addSearchPath("WaterHFNoekkSampleApp");
  result = commandList->Reset(commandAllocator.Get(), 0);

  createSamplerState();
  //createSimulationMainResources(_getWaterStateW);
  //createSimulationMainResources(_getWaterStateR);

  //createRenderingRootSignature();
  //createSimulationRootSignature();

  //createSimulationPasses();
  //createRenderingPipeline();
  //createVertexBuffers();

  //_getWaterStateW.initializeResourceBarriersForWriting();
  //_getWaterStateR.initializeResourceBarriersForReading();

  //// finally, create simulation bundles
  //createSimulationBundle(_getWaterStateW);
  //createSimulationBundle(_getWaterStateR);

  result = commandList->Close();
  commandQueue->ExecuteCommandList(commandList.Get());

  awaitGPUResources();
}

soko::WaterHFSimulationSample::WaterHFSimulationSample()
  : SampleApp()
  , waterStateW(*this)
  , waterStateR(*this)
{
  sampleName = "WaterHFSimulation";
  waterStateRefs[0] = &waterStateW;
  waterStateRefs[1] = &waterStateR;

  _getWaterStateW.referenceStateR = waterStateRefs[1];
  _getWaterStateR.referenceStateR = waterStateRefs[0];
  _getWaterStateW.stateKind = waterStateW.kStateKindWrite;
  _getWaterStateR.stateKind = waterStateR.kStateKindRead;
  _getWaterStateW.frameUHStart = 0;
  _getWaterStateR.frameUHStart = 0;
}

void soko::WaterHFSimulationSample::initializeSample(void)
{
  SampleApp::init();
  loadAssets();
  timer.reset();
}

void soko::WaterHFSimulationSample::runSample(void)
{
  SampleApp::mainLoop();
}

void soko::WaterHFSimulationSample::handleKeyReleased(
  noekk::Window const *,
  noekk::GenericEventArgs const &args
  )
{
  if (args.keyCode() == noekk::VirtualKey::Escape)
    window.close();
}

void soko::WaterHFSimulationSample::PixelShaderPerFrameData::setSize(DirectX::Vector2 const & size)
{
  using namespace DirectX;
  XMFLOAT4 patchData;
  XMStoreFloat4(&patchData, vectorPatch[0]);
  patchData.x = size.x;
  patchData.y = size.y;
  vectorPatch[0] = XMLoadFloat4(&patchData);
}

void soko::WaterHFSimulationSample::PixelShaderPerFrameData::setDelta(DirectX::Vector2 const & delta)
{
  using namespace DirectX;
  XMFLOAT4 patchData;
  XMStoreFloat4(&patchData, vectorPatch[0]);
  patchData.z = delta.x;
  patchData.w = delta.y;
  vectorPatch[0] = XMLoadFloat4(&patchData);
}

void soko::WaterHFSimulationSample::PixelShaderPerFrameData::setPosition(DirectX::Vector2 const & position)
{
  using namespace DirectX;
  XMFLOAT4 patchData;
  XMStoreFloat4(&patchData, vectorPatch[1]);
  patchData.x = position.x;
  patchData.y = position.y;
  vectorPatch[1] = XMLoadFloat4(&patchData);
}

void soko::WaterHFSimulationSample::PixelShaderPerFrameData::setStrength(float strength)
{
  using namespace DirectX;
  XMFLOAT4 patchData;
  XMStoreFloat4(&patchData, vectorPatch[1]);
  patchData.z = strength;
  vectorPatch[1] = XMLoadFloat4(&patchData);
}

void soko::WaterHFSimulationSample::PixelShaderPerFrameData::setRadius(float radius)
{
  using namespace DirectX;
  XMFLOAT4 patchData;
  XMStoreFloat4(&patchData, vectorPatch[1]);
  patchData.w = radius;
  vectorPatch[1] = XMLoadFloat4(&patchData);
}

void soko::WaterHFSimulationSample::PixelShaderPerFrameData::setElapsedTime(float dt)
{
  using namespace DirectX;
  XMFLOAT4 patchData;
  XMStoreFloat4(&patchData, vectorPatch[2]);
  patchData.x = dt;
  vectorPatch[2] = XMLoadFloat4(&patchData);
}

void soko::WaterHFSimulationSample::PixelShaderPerFrameData::setCoeffs(float c2, float h2)
{
  using namespace DirectX;
  XMFLOAT4 patchData;
  XMStoreFloat4(&patchData, vectorPatch[2]);
  patchData.y = c2;
  patchData.z = h2;
  vectorPatch[2] = XMLoadFloat4(&patchData);
}

void soko::WaterHFSimulationSample::PixelShaderPerFrameData::setDamping(float damping)
{
  using namespace DirectX;
  XMFLOAT4 patchData;
  XMStoreFloat4(&patchData, vectorPatch[2]);
  patchData.w = damping;
  vectorPatch[2] = XMLoadFloat4(&patchData);
}

void soko::WaterHFSimulationSample::createVertexBuffers()
{
  noekk::HResult result;

  float aspectRatio = std::min(
    (float) backbufferH / (float) backbufferW,
    (float) backbufferW / (float) backbufferH
    );

  noekk::GeometricPrimitives::VertexCollection vertices;
  noekk::GeometricPrimitives::IndexCollection indices;
  noekk::GeometricPrimitives::createPlane(vertices, indices, (float) simulateResX, (float) simulateResY);

  size_t verticesRowPitch = vertices.size() * noekk::VertexPositionNormalTexture::strideByteSize;
  size_t indicesRowPitch = indices.size() * noekk::GeometricPrimitives::indexByteSize;

  CD3D11_RESOURCE_DESC resourceDesc;
  D3D11_BUFFER_DESC bufferDesc;
  noekk::zeroMemory(bufferDesc);
  bufferDesc.Usage = D3D11_USAGE_DEFAULT;
  bufferDesc.CPUAccessFlags = 0;
  bufferDesc.MiscFlags = 0;

  D3D11_SUBRESOURCE_DATA subresourceData;
  noekk::zeroMemory(subresourceData);

  bufferDesc.ByteWidth = verticesRowPitch;
  bufferDesc.StructureByteStride = noekk::VertexPositionNormalTexture::strideByteSize;
  bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

  subresourceData.SysMemPitch = bufferDesc.ByteWidth;
  subresourceData.pSysMem = (void*) &vertices[0];

  resourceDesc = bufferDesc;
  result = device->CreateDefaultResource(
    &resourceDesc, &subresourceData,
    __uuidof(ID3D12Resource), (void**) &vertexBuffer
    );

  bufferDesc.ByteWidth = indicesRowPitch;
  bufferDesc.StructureByteStride = noekk::GeometricPrimitives::indexByteSize;
  subresourceData.SysMemPitch = bufferDesc.ByteWidth;
  bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

  subresourceData.SysMemPitch = bufferDesc.ByteWidth;
  subresourceData.pSysMem = (void*) &indices[0];

  resourceDesc = bufferDesc;
  result = device->CreateDefaultResource(
    &resourceDesc, &subresourceData,
    __uuidof(ID3D12Resource), (void**) &indexBuffer
    );

  noekk::zeroMemory(vertexBufferView);
  vertexBufferView.StrideInBytes = noekk::VertexPositionNormalTexture::strideByteSize;
  vertexBufferView.SizeInBytes = verticesRowPitch;

  noekk::zeroMemory(indexBufferView);
  indexBufferView.Format = DXGI_FORMAT_R16_UINT;
  indexBufferView.SizeInBytes = indicesRowPitch;
}

void soko::WaterHFSimulationSample::createSamplerState()
{
  noekk::HResult result;

  // create sampler descriptor heap
  D3D12_DESCRIPTOR_HEAP_DESC descHeapSampler;
  noekk::zeroMemory(descHeapSampler);
  descHeapSampler.NumDescriptors = 1;
  descHeapSampler.Type = D3D12_SAMPLER_DESCRIPTOR_HEAP;
  descHeapSampler.Flags = D3D12_DESCRIPTOR_HEAP_SHADER_VISIBLE;

  // create sampler
  D3D12_SAMPLER_DESC samplerDesc;
  noekk::zeroMemory(samplerDesc);
  samplerDesc.Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
  samplerDesc.AddressU = D3D12_TEXTURE_ADDRESS_CLAMP;
  samplerDesc.AddressV = D3D12_TEXTURE_ADDRESS_CLAMP;
  samplerDesc.AddressW = D3D12_TEXTURE_ADDRESS_CLAMP;
  samplerDesc.MinLOD = 0;
  samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
  samplerDesc.MipLODBias = 0.0f;
  samplerDesc.MaxAnisotropy = 1;
  samplerDesc.ComparisonFunc = D3D12_COMPARISON_ALWAYS;

  result = device->CreateDescriptorHeap(&descHeapSampler, __uuidof(ID3D12DescriptorHeap), (void**) &linearSamplerDH);
  device->CreateSampler(&samplerDesc, linearSamplerDH->GetCPUDescriptorHandleForHeapStart());
}


void soko::WaterHFSimulationSample::WaterSimulationStateResources::initializeResourceBarriersForWriting()
{
  setResourceBarrier(
    deviceResource.commandList, depthStencilResource,
    D3D12_RESOURCE_USAGE_INITIAL,
    D3D12_RESOURCE_USAGE_DEPTH
    );
  setResourceBarrier(
    deviceResource.commandList, resource,
    D3D12_RESOURCE_USAGE_INITIAL,
    D3D12_RESOURCE_USAGE_RENDER_TARGET
    );
}

void soko::WaterHFSimulationSample::WaterSimulationStateResources::initializeResourceBarriersForReading()
{
  setResourceBarrier(
    deviceResource.commandList, depthStencilResource,
    D3D12_RESOURCE_USAGE_INITIAL,
    D3D12_RESOURCE_USAGE_DEPTH
    );
  setResourceBarrier(
    deviceResource.commandList, resource,
    D3D12_RESOURCE_USAGE_INITIAL,
    D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
    );
}

void soko::WaterHFSimulationSample::WaterSimulationStateResources::advanceResourceBarriersForWriting()
{
  stateKind = kStateKindWrite;
  setResourceBarrier(
    deviceResource.commandList, resource,
    D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE,
    D3D12_RESOURCE_USAGE_RENDER_TARGET
    );
}

void soko::WaterHFSimulationSample::WaterSimulationStateResources::advanceResourceBarriersForReading()
{
  stateKind = kStateKindRead;
  setResourceBarrier(
    deviceResource.commandList, resource,
    D3D12_RESOURCE_USAGE_RENDER_TARGET,
    D3D12_RESOURCE_USAGE_PIXEL_SHADER_RESOURCE
    );
}

void soko::WaterHFSimulationSample::createSimulationMainResources(
  WaterSimulationStateResources & stateResources
  )
{
  using Microsoft::WRL::ComPtr;

  noekk::HResult result;
  size_t bufferSize = noekk::ResourceAssist::nearest256mult(sizeof(PixelShaderPerFrameData));

  D3D12_RESOURCE_MISC_FLAG miscFlags
    = D3D12_RESOURCE_MISC_RENDER_TARGET
    | D3D12_RESOURCE_MISC_NO_TILE_MAPPING
    | D3D12_RESOURCE_MISC_NO_STREAM_OUTPUT
    | D3D12_RESOURCE_MISC_NO_UNORDERED_ACCESS;

  // create textures
  D3D12_RESOURCE_DESC texDesc;
  noekk::zeroMemory(texDesc);
  texDesc.MipLevels = 1;
  texDesc.Format = simulateResFormat;
  texDesc.Width = simulateResX;
  texDesc.Height = simulateResY;
  texDesc.MiscFlags = miscFlags;
  texDesc.DepthOrArraySize = 1;
  texDesc.SampleDesc.Count = 1;
  texDesc.SampleDesc.Quality = 0;
  texDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE_2D;

  D3D12_DESCRIPTOR_HEAP_DESC descHeapRtv;
  noekk::zeroMemory(descHeapRtv);
  descHeapRtv.Type = D3D12_RTV_DESCRIPTOR_HEAP;
  descHeapRtv.Flags = 0;
  descHeapRtv.NumDescriptors = 1;

  D3D12_DESCRIPTOR_HEAP_DESC descHeapDsv = {};
  descHeapDsv.Type = D3D12_DSV_DESCRIPTOR_HEAP;
  descHeapDsv.Flags = 0;
  descHeapDsv.NumDescriptors = 1;

  D3D12_DESCRIPTOR_HEAP_DESC descHeapCbvSrv;
  noekk::zeroMemory(descHeapCbvSrv);
  descHeapCbvSrv.Type = D3D12_CBV_SRV_UAV_DESCRIPTOR_HEAP;
  descHeapCbvSrv.Flags = D3D12_DESCRIPTOR_HEAP_SHADER_VISIBLE;
  descHeapCbvSrv.NumDescriptors = 2;

  D3D12_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
  noekk::zeroMemory(renderTargetViewDesc);
  renderTargetViewDesc.Format = texDesc.Format;
  renderTargetViewDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
  renderTargetViewDesc.Texture2D.MipSlice = 0;

  D3D12_DEPTH_STENCIL_VIEW_DESC depthStencilDesc;
  noekk::zeroMemory(depthStencilDesc);
  depthStencilDesc.Format = DXGI_FORMAT_D32_FLOAT;
  depthStencilDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
  depthStencilDesc.Flags = 0;

  D3D12_SHADER_RESOURCE_VIEW_DESC renderTargetResourceViewDesc;
  noekk::zeroMemory(renderTargetResourceViewDesc);
  renderTargetResourceViewDesc.Format = texDesc.Format;
  renderTargetResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
  renderTargetResourceViewDesc.Texture2D.MipLevels = texDesc.MipLevels;
  renderTargetResourceViewDesc.Texture2D.MostDetailedMip = 0;

  result = device->CreateCommittedResource(
    &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
    D3D12_HEAP_MISC_NONE, &texDesc, D3D12_RESOURCE_USAGE_INITIAL,
    __uuidof(ID3D12Resource), (void**) (&stateResources.resource)
    );

  switch (stateResources.stateKind)
  {
  case WaterSimulationStateResources::kStateKindWrite:
    stateResources.resource->SetName(L"waterStateWrite");
    break;
  case WaterSimulationStateResources::kStateKindRead:
    stateResources.resource->SetName(L"waterStateRead");
    break;
  }

  result = device->CreateCommittedResource(
    &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
    D3D12_HEAP_MISC_NONE,
    &CD3D12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_D32_FLOAT, texDesc.Width, texDesc.Height, (UINT) 1U, (UINT) 0U, (UINT) 1U, (UINT) 0U, D3D12_RESOURCE_MISC_DEPTH_STENCIL),
    D3D12_RESOURCE_USAGE_INITIAL,
    __uuidof(ID3D12Resource), (void**) (&stateResources.depthStencilResource)
    );

  result = device->CreateDescriptorHeap(
    &descHeapRtv, __uuidof(ID3D12DescriptorHeap),
    (void**) &stateResources.renderTargetDH
    );
  result = device->CreateDescriptorHeap(
    &descHeapDsv, __uuidof(ID3D12DescriptorHeap),
    (void**) &stateResources.depthStencilDH
    );
  result = device->CreateDescriptorHeap(
    &descHeapCbvSrv, __uuidof(ID3D12DescriptorHeap),
    (void**) &stateResources.resourcesDH
    );

  device->CreateRenderTargetView(
    stateResources.resource.Get(), &renderTargetViewDesc,
    stateResources.renderTargetDH->GetCPUDescriptorHandleForHeapStart()
    );
  device->CreateDepthStencilView(
    stateResources.depthStencilResource.Get(), &depthStencilDesc,
    stateResources.depthStencilDH->GetCPUDescriptorHandleForHeapStart()
    );
  device->CreateShaderResourceView(
    stateResources.resource.Get(), &renderTargetResourceViewDesc,
    // cbv vs, cbv ps
    stateResources.resourcesDH->GetCPUDescriptorHandleForHeapStart().MakeOffsetted(2, descriptorSize) 
    );

  result = device->CreateBuffer(
    D3D12_HEAP_TYPE_UPLOAD, bufferSize, D3D12_RESOURCE_MISC_NONE,
    __uuidof(ID3D12Resource), (void**) &stateResources.framePSUH
    );
  stateResources.framePSUH->Map(0, reinterpret_cast<void**>(&stateResources.frameUHStart));
  updateFrameBuffers(stateResources.frameUHStart);

  D3D12_CPU_DESCRIPTOR_HANDLE handle = stateResources.resourcesDH->GetCPUDescriptorHandleForHeapStart();

  D3D12_CONSTANT_BUFFER_VIEW_DESC constantBufferViewDesc = {};
  constantBufferViewDesc.OffsetInBytes = 0;
  constantBufferViewDesc.SizeInBytes = bufferSize;

  handle.Offset(1, descriptorSize); // +1 SRV
  device->CreateConstantBufferView(stateResources.framePSUH.Get(), &constantBufferViewDesc, handle);
}

void soko::WaterHFSimulationSample::createRenderingPipeline(void)
{
  noekk::HResult result;

  noekk::LoadShaderBlobHook shaderLoadCallback;
  noekk::AssetReader::findAndReadTo(L"shaders/renderToTextureVS.cso", shaderLoadCallback.vsAssetId, &shaderLoadCallback);
  noekk::AssetReader::findAndReadTo(L"shaders/renderTexturePS.cso", shaderLoadCallback.psAssetId, &shaderLoadCallback); //!

  D3D12_INPUT_ELEMENT_DESC inputLayoutElements [] =
  {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_PER_VERTEX_DATA, 0 },
  };

  CD3D12_BLEND_DESC blendStateDesc(D3D12_DEFAULT);
  CD3D12_DEPTH_STENCIL_DESC depthStencilStateDesc(D3D12_DEFAULT);
  CD3D12_RASTERIZER_DESC rasterizerStateDesc(D3D12_DEFAULT); //!

  D3D12_GRAPHICS_PIPELINE_STATE_DESC renderingPipelineStateDesc;
  noekk::zeroMemory(renderingPipelineStateDesc);
  renderingPipelineStateDesc.NumRenderTargets = 1;
  renderingPipelineStateDesc.pRootSignature = renderRS.Get(); //!
  renderingPipelineStateDesc.RTVFormats[0] = renderTargetFormat; //!
  renderingPipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
  renderingPipelineStateDesc.VS = { shaderLoadCallback.vsBlob.data, shaderLoadCallback.vsBlob.dataLength };
  renderingPipelineStateDesc.PS = { shaderLoadCallback.psBlob.data, shaderLoadCallback.psBlob.dataLength };
  renderingPipelineStateDesc.InputLayout = { inputLayoutElements, ARRAYSIZE(inputLayoutElements) };
  renderingPipelineStateDesc.DepthStencilState = depthStencilStateDesc;
  renderingPipelineStateDesc.DSVFormat = depthStencilFormat; //!
  renderingPipelineStateDesc.RasterizerState = rasterizerStateDesc;
  renderingPipelineStateDesc.BlendState = blendStateDesc;
  renderingPipelineStateDesc.SampleMask = UINT_MAX;
  renderingPipelineStateDesc.SampleDesc.Count = 1;

  result = device->CreateGraphicsPipelineState(
    &renderingPipelineStateDesc, //!
    &renderPassPSO //!
    );
}

void soko::WaterHFSimulationSample::createRenderingRootSignature()
{
  using Microsoft::WRL::ComPtr;

  D3D12_DESCRIPTOR_RANGE descRange[3];
  D3D12_ROOT_PARAMETER rootParameters[3];
  D3D12_ROOT_SIGNATURE descRootSignature;
  descRange[0].Init(D3D12_DESCRIPTOR_RANGE_CBV, 1, 0);
  descRange[1].Init(D3D12_DESCRIPTOR_RANGE_SRV, 1, 0);
  descRange[2].Init(D3D12_DESCRIPTOR_RANGE_SAMPLER, 1, 0);
  rootParameters[0].InitAsDescriptorTable(1, &descRange[0], D3D12_SHADER_VISIBILITY_VERTEX);
  rootParameters[1].InitAsDescriptorTable(1, &descRange[1], D3D12_SHADER_VISIBILITY_PIXEL);
  rootParameters[2].InitAsDescriptorTable(1, &descRange[1], D3D12_SHADER_VISIBILITY_PIXEL);
  descRootSignature.Init(3, rootParameters, 0);

  noekk::HResult result;
  ComPtr<ID3DBlob> outBlob;
  ComPtr<ID3DBlob> errBlob;

  result = D3D12SerializeRootSignature(
    &descRootSignature,
    D3D_ROOT_SIGNATURE_V1,
    outBlob.GetAddressOf(),
    errBlob.GetAddressOf()
    );
  result = device->CreateRootSignature(
    outBlob->GetBufferPointer(),
    outBlob->GetBufferSize(),
    __uuidof(ID3D12RootSignature),
    (void**) &renderRS
    );
}

void soko::WaterHFSimulationSample::VertexShaderPerFrameData::setWorld(DirectX::Matrix const &matrix)
{
  matrixPatch[0] = matrix;
}

void soko::WaterHFSimulationSample::VertexShaderPerFrameData::setCameraView(DirectX::Matrix const &matrix)
{
  matrixPatch[1] = matrix;
}

void soko::WaterHFSimulationSample::VertexShaderPerFrameData::setCameraProjection(DirectX::Matrix const &matrix)
{
  matrixPatch[2] = matrix;
}
