
cbuffer PerFrameVS
{
  row_major float4x4 world;
  row_major float4x4 cameraView;
  row_major float4x4 cameraProj;
};

struct VIn
{
  float3 position : POSITION;
  float3 normal : NORMAL;
  float2 tex : TEXCOORD;
};

struct PIn
{
  float4 position : SV_POSITION;
  float3 normal : NORMAL;
  float2 tex : TEXCOORD;
};

PIn main(in VIn input)
{
  PIn output;

  float4 position = float4(input.position.xyz, 1.0f);
  output.position = mul(position, world);
  output.position = mul(output.position, cameraView);
  output.position = mul(output.position, cameraProj);
  output.normal = mul(input.normal, (float3x3)world);
  output.normal = mul(output.normal, (float3x3)cameraView);
  output.tex = input.tex;
  return output;
}