#include "Fire.h"
#include <DxDDSLoader.h>
#include <INIReader.h>

#define _As_d3d11resource (dx::IDirect3DResource **)

void Demo::FeuerFrei::CreateDeviceDependentResources()
{
  HRESULT result = S_OK;

  dx::IDirect3DDevice *device = d3d->device.Get();
  dx::IDirect3DContext *context = d3d->context.Get();

  dx::DxAssetManager::addSearchPath("SimpleFire");

  class TextureHook : public dx::DxAssetHook
  {
  public:
    dx::DxDeviceResources *d3d;
    dx::IDirect3DResource **resourceAddress;
    dx::ISRView **shaderResourceViewAddress;

    TextureHook() : resourceAddress(0), shaderResourceViewAddress(0) { }

  public:
    virtual void onAssetNotFound(_In_ unsigned assetId) { assert(false); }
    virtual void onAssetLoaded(_In_ unsigned assetId, _In_ dx::DxAssetBuffer const &asset)
    {
      dx::HResult result = S_OK;

      if (
        d3d && resourceAddress && shaderResourceViewAddress
        && asset.data && asset.dataLength
        )
      {
        result = dx::dds::CreateDDSTextureFromMemory(
          d3d->device.Get(), 
          asset.data, asset.dataLength,
          resourceAddress, shaderResourceViewAddress
          );
      }

      _Dx_hresult_assert(result);
    }
  } hook;

  hook.d3d = d3d;

  // fire dds
  hook.resourceAddress = _As_d3d11resource FirePS.resource.ReleaseAndGetAddressOf();
  hook.shaderResourceViewAddress = FirePS.readView.ReleaseAndGetAddressOf();
  dx::DxAssetManager::loadAsset("textures/Imagen-131.dds", 1, &hook);
  //dx::DxAssetManager::loadAsset("textures/fire01.dds", 1, &hook);
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot load texture");

  // noise dds
  hook.resourceAddress = _As_d3d11resource NoisePS.resource.ReleaseAndGetAddressOf();
  hook.shaderResourceViewAddress = NoisePS.readView.ReleaseAndGetAddressOf();
  dx::DxAssetManager::loadAsset("textures/water-displacement-texture.dds", 2, &hook);
  //dx::DxAssetManager::loadAsset("textures/noise01.dds", 2, &hook);
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot load texture");

  // alpha dds
  hook.resourceAddress = _As_d3d11resource AlphaPS.resource.ReleaseAndGetAddressOf();
  hook.shaderResourceViewAddress = AlphaPS.readView.ReleaseAndGetAddressOf();
  dx::DxAssetManager::loadAsset("textures/alpha01.dds", 3, &hook);
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot load texture");

  dx::VSInputElementDescription layoutDesc[2] =
  {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  };

  result = dx::DxResourceManager::loadShader(
    device, L"shaders/FireVertexShader.cso",
    layoutDesc,
    ARRAYSIZE(layoutDesc),
    PipeVS.ReleaseAndGetAddressOf(),
    InputIA.ReleaseAndGetAddressOf()
    );
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot load shader");

  result = dx::DxResourceManager::loadShader(
    device, L"shaders/FirePixelShader.cso",
    PipePS.ReleaseAndGetAddressOf()
    );
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot load shader");

  dx::SamplerStateDescription samplerDesc;
  dx::DxResourceManager::defaults(samplerDesc);

  samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
  result = device->CreateSamplerState(
    &samplerDesc, WrapPS.ReleaseAndGetAddressOf()
    );
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot create sampler");

  samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
  samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
  samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
  result = device->CreateSamplerState(
    &samplerDesc, ClampPS.ReleaseAndGetAddressOf()
    );
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot create sampler");

  dx::RasterizerDescription rasterDesc;
  dx::zeroMemory(rasterDesc);
  dx::DxResourceManager::defaults(rasterDesc);

  rasterDesc.AntialiasedLineEnable = false;
  rasterDesc.CullMode = D3D11_CULL_BACK;
  rasterDesc.DepthBias = 0;
  rasterDesc.DepthBiasClamp = 0.0f;
  rasterDesc.DepthClipEnable = true;
  rasterDesc.FillMode = D3D11_FILL_SOLID;
  //rasterDesc.FrontCounterClockwise = true;
  rasterDesc.FrontCounterClockwise = false;
  rasterDesc.MultisampleEnable = false;
  rasterDesc.ScissorEnable = false;
  rasterDesc.SlopeScaledDepthBias = 0.0f;

  dx::BlendStateDescription blendStateDescription;
  dx::zeroMemory(blendStateDescription);

  blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
  blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
  blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
  blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
  blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
  blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
  blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
  blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;

  device->CreateRasterizerState1(&rasterDesc, RS.ReleaseAndGetAddressOf());
  device->CreateBlendState1(&blendStateDescription, BlendOM.ReleaseAndGetAddressOf());

  INIReader reader("SimpleFire.ini");

  NoiseData.SetFrameTime((float)reader.GetReal("Demo", "vsNoiseFrameTime", 0.1));
  NoiseData.SetScaleX((float)reader.GetReal("Demo", "vsNoiseScaleX", 0.1));
  NoiseData.SetScaleY((float)reader.GetReal("Demo", "vsNoiseScaleY", 0.1));
  NoiseData.SetScaleZ((float)reader.GetReal("Demo", "vsNoiseScaleZ", 0.1));
  NoiseData.SetScrollX((float)reader.GetReal("Demo", "vsNoiseScrollX", 0.1));
  NoiseData.SetScrollY((float)reader.GetReal("Demo", "vsNoiseScrollY", 0.1));
  NoiseData.SetScrollZ((float)reader.GetReal("Demo", "vsNoiseScrollZ", 0.1));
  DistortionData.SetDistortion1X((float)reader.GetReal("Demo", "psDistortion1X", 0.1));
  DistortionData.SetDistortion1Y((float)reader.GetReal("Demo", "psDistortion1Y", 0.1));
  DistortionData.SetDistortion2X((float)reader.GetReal("Demo", "psDistortion2X", 0.1));
  DistortionData.SetDistortion2Y((float)reader.GetReal("Demo", "psDistortion2Y", 0.1));
  DistortionData.SetDistortion3X((float)reader.GetReal("Demo", "psDistortion3X", 0.1));
  DistortionData.SetDistortion3Y((float)reader.GetReal("Demo", "psDistortion3Y", 0.1));
  DistortionData.SetDistortionBias((float)reader.GetReal("Demo", "psDistortionBias", 0.1));
  DistortionData.SetDistortionBias((float)reader.GetReal("Demo", "psDistortionScale", 0.1));

  result = MatricesVS.create(device, "fire:matrices");
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot create buffer");
  result = NoiseVS.create(device, "fire:noise");
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot create buffer");
  result = DistortionPS.create(device, "fire:distortion");
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot create buffer");

  Cam.AspectRatio = d3d->actualRenderTargetSize.Width / d3d->actualRenderTargetSize.Height;
  Cam.Displacement.z = -2.5f;
  Cam.UpdateView({ 0, 0 }, 0);
  Cam.UpdateProjection();

  SpatialData.Packs[0] = dx::Matrix::CreateTranslation(0, 0, 0);
  SpatialData.Packs[1] = Cam.View;
  SpatialData.Packs[2] = Cam.Projection;

  result = MatricesVS.setData(context, SpatialData);
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot update buffer");
  result = NoiseVS.setData(context, NoiseData);
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot update buffer");
  result = DistortionPS.setData(context, DistortionData);
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot update buffer");

  Vertex squareVertices[4]
  {
    { { -1, +1, 0, +1 }, { 0, 0 } },
    { { +1, +1, 0, +1 }, { 1, 0 } },
    { { +1, -1, 0, +1 }, { 1, 1 } },
    { { -1, -1, 0, +1 }, { 0, 1 } },
  };

  UINT16 squareIndices[6] =
  {
    0, 1, 2,
    0, 2, 3,
  };

  result = dx::DxResourceManager::createVertexBuffer(
    device, ARRAYSIZE(squareVertices), squareVertices,
    VerticesIA.ReleaseAndGetAddressOf()
    );
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot create buffer");

  result = dx::DxResourceManager::createIndexBuffer(
    device, ARRAYSIZE(squareIndices), squareIndices,
    IndicesIA.ReleaseAndGetAddressOf()
    );
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot create buffer");
}

void Demo::FeuerFrei::CreateDeviceIndependentResources()
{
}

void Demo::FeuerFrei::CreateWindowSizeDependentResources()
{
}

void Demo::FeuerFrei::CreateResources()
{
  CreateDeviceIndependentResources();
  CreateDeviceDependentResources();
  CreateWindowSizeDependentResources();
  timer.reset();
}

Demo::FeuerFrei::FeuerFrei(dx::DxDeviceResources *d3d)
{
  onDeviceRestored(d3d);
}

void Demo::FeuerFrei::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void Demo::FeuerFrei::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  if (restoredDevice)
  {
    d3d = restoredDevice;
    d3d->deviceLost += _Dx_delegate_to(this, &FeuerFrei::onDeviceLost);
    d3d->deviceRestored += _Dx_delegate_to(this, &FeuerFrei::onDeviceRestored);
  }
}

void Demo::FeuerFrei::Update()
{
  timer.update();
  dx::IDirect3DContext *context = d3d->context.Get();

  NoiseData.SetFrameTime(timer.totalElapsed);

  ::HRESULT result = NoiseVS.setData(context, NoiseData);
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot update buffer");
  result = DistortionPS.setData(context, DistortionData);
  _ASSERT_EXPR(SUCCEEDED(result), L"Cannot update buffer");
}

void Demo::FeuerFrei::Render()
{
  static const UINT32 strides[] = { sizeof Vertex };
  static const UINT32 offsets[] = { 0 };
  static const FLOAT factor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
  static const auto mask = 0xffffffff;

  dx::IDirect3DContext *context = d3d->context.Get();

  context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  context->IASetVertexBuffers(0, 1, VerticesIA.GetAddressOf(), strides, offsets);
  context->IASetIndexBuffer(IndicesIA.Get(), DXGI_FORMAT_R16_UINT, 0);
  context->IASetInputLayout(InputIA.Get());

  context->VSSetShader(PipeVS.Get(), nullptr, 0);
  context->PSSetShader(PipePS.Get(), nullptr, 0);

  context->VSSetConstantBuffers(0, 1, MatricesVS.getBufferAddress());
  context->VSSetConstantBuffers(1, 1, NoiseVS.getBufferAddress());

  context->PSSetConstantBuffers(0, 1, DistortionPS.getBufferAddress());
  context->PSSetShaderResources(0, 1, FirePS.readView.GetAddressOf());
  context->PSSetShaderResources(1, 1, NoisePS.readView.GetAddressOf());
  context->PSSetShaderResources(2, 1, AlphaPS.readView.GetAddressOf());
  context->PSSetSamplers(0, 1, WrapPS.GetAddressOf());
  context->PSSetSamplers(1, 1, ClampPS.GetAddressOf());

  context->RSSetState(RS.Get());
  //context->OMSetBlendState(BlendOM.Get(), factor, mask);

  context->DrawIndexed(6, 0, 0);

  context->PSSetShader(nullptr, nullptr, 0);
  context->VSSetShader(nullptr, nullptr, 0);
}



