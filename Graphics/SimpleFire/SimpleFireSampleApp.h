#pragma once
#include "pch.h"
#include "Fire.h"

namespace nyx
{
  class SimpleFireSampleApp
    : public dx::DxSampleApp
    , public dx::AlignedNew<SimpleFireSampleApp>
  {
  public:
    std::string uiUrl;
    dx::sim::BasicTimer timer;

    Demo::FeuerFrei fire;

    bool mousePressed;
    bool mouseRPressed;
    dx::Vector2 previousMousePos;

  public:
    SimpleFireSampleApp();
    virtual ~SimpleFireSampleApp();

  public: // DxSampleApp
    virtual void handlePaint(dx::DxWindow const*);

  public: // DxUISampleApp
    virtual std::string getUIURL(void);
    virtual bool onAppLoop(void);

  public: // DxUISampleApp
    virtual void sizeChanged(void);
    virtual void onUICreated(void);
    virtual void onUIDestroyed(void);
    virtual void keyReleased(dx::DxGenericEventArgs const&);
    virtual void pointerMoved(dx::DxGenericEventArgs const&);
    virtual void pointerWheel(dx::DxGenericEventArgs const&);
    virtual void pointerLeftPressed(dx::DxGenericEventArgs const&);
    virtual void pointerLeftReleased(dx::DxGenericEventArgs const&);
    virtual void pointerRightPressed(dx::DxGenericEventArgs const&);
    virtual void pointerRightReleased(dx::DxGenericEventArgs const&);

  };
}
