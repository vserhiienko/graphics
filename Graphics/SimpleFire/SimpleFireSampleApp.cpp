#include "pch.h"
#include "SimpleFireSampleApp.h"

#include <INIReader.h>

static inline float random(float min, float max)
{
  return float(rand()) / float(RAND_MAX) * (max - min) + min;
}

dx::DxSampleApp *dx::DxSampleAppFactory()
{
  return new nyx::SimpleFireSampleApp();
}

nyx::SimpleFireSampleApp::SimpleFireSampleApp()
  : DxSampleApp(D3D_DRIVER_TYPE_HARDWARE)
  //: DxUISampleApp(true, true, D3D_DRIVER_TYPE_REFERENCE)
  , fire(&deviceResources)
  , mousePressed(false)
  , mouseRPressed(false)
{
  INIReader iniReader("SimpleFire.ini");
  uiUrl = iniReader.Get("UI", "appUiUrl", _SimpleFire_UIURL);
}

nyx::SimpleFireSampleApp::~SimpleFireSampleApp()
{
}

std::string nyx::SimpleFireSampleApp::getUIURL()
{
  return uiUrl;
}

bool nyx::SimpleFireSampleApp::onAppLoop()
{
  dx::trace("nyx:app: initialize sample");
  window.setWindowTitle("SimpleFire");

  fire.CreateResources();
  timer.reset();
  return true;
}

void nyx::SimpleFireSampleApp::handlePaint(dx::DxWindow const*)
{
  static unsigned frameIndex = 0;
  static const unsigned printFrameIndex = 512;
  static const unsigned maxFrameIndex = printFrameIndex * 10;

  timer.update();
  frameIndex = ++frameIndex % maxFrameIndex;
  if (frameIndex % printFrameIndex == 0)
    dx::trace("nyx:app: fps %d", timer.framesPerSecond);

  fire.Update();
  // ...
  // update ui
  //dxui::DxUIClient::updateApplication();
  //updateUI(timer.elapsed);

  /// draw

  beginScene();
  // render scene
  fire.Render();
  // ...
  // render ui
  //renderUI();
  endScene();
}

void nyx::SimpleFireSampleApp::sizeChanged()
{

}

void nyx::SimpleFireSampleApp::onUICreated()
{

}

void nyx::SimpleFireSampleApp::onUIDestroyed()
{
}

void nyx::SimpleFireSampleApp::keyReleased(dx::DxGenericEventArgs const &args)
{
  switch (args.keyCode())
  {
  case dx::VirtualKey::Escape: window.close(); break;
  default: {} break;
  }

}

void nyx::SimpleFireSampleApp::pointerMoved(dx::DxGenericEventArgs const &args)
{
  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  if (mousePressed)
  {
    dx::Vector2 mouseDelta = mousePos - previousMousePos;
    // ...
  }

  previousMousePos = mousePos;
}

void nyx::SimpleFireSampleApp::pointerLeftPressed(dx::DxGenericEventArgs const &args)
{
  mousePressed = true;
}

void nyx::SimpleFireSampleApp::pointerLeftReleased(dx::DxGenericEventArgs const &args)
{
  mousePressed = false;
}

void nyx::SimpleFireSampleApp::pointerWheel(dx::DxGenericEventArgs const &args)
{
  if (mousePressed)
  { 
    // ...
  } 
}

void nyx::SimpleFireSampleApp::pointerRightPressed(dx::DxGenericEventArgs const&)
{
  mouseRPressed = true;
}

void nyx::SimpleFireSampleApp::pointerRightReleased(dx::DxGenericEventArgs const&)
{
  mouseRPressed = false;
}