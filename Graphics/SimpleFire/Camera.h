#pragma once
#include "pch.h"

namespace Demo
{
	struct Camera
	{
		dx::Matrix View;
    dx::Matrix Projection;

    dx::Vector3 FocusPosition; // focus
    dx::Vector3 Displacement; // eye

		float FieldOfView;
    float AspectRatio;
    float NearPlane;
    float FarPlane;

		Demo::Camera::Camera()
		{
			Displacement.z = -10.f;
			//FocusPosition.y = 2.0f;

			FieldOfView = ::dx::XM_PIDIV4;
			AspectRatio = 1.3333f;
			NearPlane = 0.010f;
			FarPlane = 100.00f;

			UpdateView({ 0, 0 }, 0);
			UpdateProjection();
		}

		void Demo::Camera::UpdateView(
			dx::Vector2 delta,
			::FLOAT zoom
			)
		{
			static dx::Vector3 up = { 0.f, 1.f, 0.f }, eye, displacement;
			static dx::Quaternion pitchQ, yawQ;
			static float pitchA, yawA;

			delta *= 0.005f;
			zoom *= 0.005f;
			pitchA += delta.y;
			yawA += delta.x;

			pitchQ.w = ::cosf(pitchA * .50f);
			pitchQ.x = ::sin(pitchA * .50f);
			yawQ.w = ::cosf(yawA *.50f);
			yawQ.y = ::sin(yawA *.50f);

			Displacement.z += zoom;
			displacement = ::dx::XMVector3Rotate(
				::dx::XMVector3Rotate(
					Displacement, pitchQ
					), 
				yawQ
				);

			eye = FocusPosition + displacement;
			View = dx::XMMatrixLookAtLH(
				eye, FocusPosition, up
				);

			/*Framework::Output(
				"Demo::Camera::UpdateView() >> pitch %f yaw %f\n",
				pitchA, yawA
				);

			Framework::Output(
				"Demo::Camera::UpdateView() >> eye %f %f %f\n",
				eye.x, eye.y, eye.z
				);*/
		}

		void Demo::Camera::UpdateProjection(
			)
		{
			Projection = dx::XMMatrixPerspectiveFovLH(
				FieldOfView, AspectRatio, NearPlane, FarPlane
				);
		}



	};
}