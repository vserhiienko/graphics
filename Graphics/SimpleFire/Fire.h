#pragma once
#include "pch.h"
#include "Camera.h"

#define _Nena_Align16
//#define _Nena_Align16 __declspec(align(16))

namespace Demo
{
	class FeuerFrei
	{
		typedef dx::ReadResource<dx::Texture2> ReadTexture2D;

		struct Vertex
		{
			dx::Vector4 Pos;
			dx::Vector2 Tex;
		};

		struct _Nena_Align16 Float4x4x3
		{
			dx::Matrix Packs[3];
		};

		struct _Nena_Align16 Float4x2
		{
			dx::Vector4 Packs[2];

			inline void SetFrameTime(FLOAT v) { Packs[0].x = v; }
			inline void SetScrollX(FLOAT v) { Packs[0].y = v; }
			inline void SetScrollY(FLOAT v) { Packs[0].z = v; }
			inline void SetScrollZ(FLOAT v) { Packs[0].w = v; }
			inline void SetScaleX(FLOAT v) { Packs[1].x = v; }
			inline void SetScaleY(FLOAT v) { Packs[1].y = v; }
			inline void SetScaleZ(FLOAT v) { Packs[1].z = v; }
			inline void SetDistortion1X(FLOAT v) { Packs[0].x = v; }
			inline void SetDistortion1Y(FLOAT v) { Packs[0].y = v; }
			inline void SetDistortion2X(FLOAT v) { Packs[0].z = v; }
			inline void SetDistortion2Y(FLOAT v) { Packs[0].w = v; }
			inline void SetDistortion3X(FLOAT v) { Packs[1].x = v; }
			inline void SetDistortion3Y(FLOAT v) { Packs[1].y = v; }
			inline void SetDistortionScale(FLOAT v) { Packs[1].z = v; }
			inline void SetDistortionBias(FLOAT v) { Packs[1].w = v; }
		};

  public:
    dx::DxDeviceResources *d3d;
    dx::sim::BasicTimer timer;

  public:
    dx::VSLayout InputIA;
		dx::Buffer VerticesIA;
		dx::Buffer IndicesIA;

		dx::VShader PipeVS;
		dx::ConstantBuffer<Float4x4x3> MatricesVS;
		dx::ConstantBuffer<Float4x2> NoiseVS;

		dx::PShader PipePS;
		dx::ConstantBuffer<Float4x2> DistortionPS;
		dx::SamplerState ClampPS;
		dx::SamplerState WrapPS;
		dx::RasterizerState RS;
		dx::BlendState BlendOM;

		FeuerFrei::ReadTexture2D AlphaPS;
		FeuerFrei::ReadTexture2D NoisePS;
		FeuerFrei::ReadTexture2D FirePS;

		FeuerFrei::Float4x4x3 SpatialData;
		FeuerFrei::Float4x2 DistortionData;
		FeuerFrei::Float4x2 NoiseData;
		Demo::Camera Cam;

  public:
    FeuerFrei(dx::DxDeviceResources *);

    void CreateResources(void);
    void Update(void);
    void Render(void);

  protected:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  protected:
    void CreateDeviceIndependentResources(void);
    void CreateDeviceDependentResources(void);
    void CreateWindowSizeDependentResources(void);

	};
}
