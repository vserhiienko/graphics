#pragma once

#include <iostream>
#include <memory>
#include <stdint.h>

#include "BasicIO.h"

namespace aega
{
  namespace io
  {
    class BasicReader
    {
    protected:
      FileWrapper::ShPtr m_file;
      std::unique_ptr<IStreamConsumer> streamConsumer;

    public:
      BasicReader(FileWrapper::ShPtr file);
      virtual ~BasicReader();

    public:
      virtual bool ReadBytes(void *data, size_t elementSize, size_t elementNum);
      bool ReadString(std::wstring &);
      bool ReadString(std::string &);
      bool ReadUInt32_7BitEnc(uint32_t &);
      bool ReadInt32_7BitEnc(int32_t &);
      bool ReadUInt32(uint32_t &);
      bool ReadInt32(int32_t &);

    public:
      template <typename _PodTy> _PodTy NextElement() { _PodTy outValue; ReadBytes(&outValue, sizeof(_PodTy), 1); return outValue; }
      template <typename _PodTy> void NextElement(_PodTy &dataRef) { ReadBytes(&dataRef, sizeof(_PodTy), 1); }
      template <typename _PodTy> void NextElement(_PodTy *dataPtr) { ReadBytes(dataPtr, sizeof(_PodTy), 1); }
      template <typename _PodTy> void NextElements(_PodTy *dataPtr, size_t arraySize) { ReadBytes(dataPtr, sizeof(_PodTy), arraySize); }

    };

    class CompressedReader
      : public BasicReader
    {
    public:
      CompressedReader(FileWrapper::ShPtr file);
      virtual ~CompressedReader();
    public:
      virtual bool ReadBytes(void *data, size_t elementSize, size_t elementNum) override;

    };
  }
}