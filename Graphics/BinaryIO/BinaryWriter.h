#pragma once

#include <iostream>
#include <memory>
#include <stdint.h>
#include "BasicIO.h"

namespace aega
{
  namespace io
  {
    class BasicWriter
    {
    protected:
      FileWrapper::ShPtr m_file;
      std::unique_ptr<IStreamProducer> streamProducer;

    public:
      BasicWriter(FileWrapper::ShPtr file);
      virtual ~BasicWriter(void);

    public:
      virtual bool WriteBytes(const void *data, size_t elementSize, size_t elementNum);
      bool WriteString(std::wstring const &);
      bool WriteString(std::string const &);
      bool WriteUInt32_7BitEnc(uint32_t);
      bool WriteInt32_7BitEnc(int32_t);
      bool WriteUInt32(uint32_t);
      bool WriteInt32(int32_t);

    public:
      template <typename _PodTy> void NextElement(_PodTy const *dataPtr) { WriteBytes(dataPtr, sizeof(_PodTy), 1); }
      template <typename _PodTy> void NextElement(_PodTy const &dataRef) { WriteBytes(&dataRef, sizeof(_PodTy), 1); }
      template <typename _PodTy> void NextElements(_PodTy const *dataPtr, size_t arraySize = 1) { m_writer->WriteBytes(dataPtr, sizeof(_PodTy), arraySize); }
    };

    class CompressedWriter
      : public BasicWriter
    {
    public:
      CompressedWriter(FileWrapper::ShPtr file);
      virtual ~CompressedWriter(void);
    public:
      virtual bool WriteBytes(const void *data, size_t elementSize, size_t elementNum) override;
    };
  }
}