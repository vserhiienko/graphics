#pragma once

#include <Windows.h>
#include <memory>
#include <wrl.h>
#include <mutex>

#ifdef GetNextSibling
#undef GetNextSibling
#endif
#ifdef GetFirstSibling
#undef GetFirstSibling
#endif
#ifdef GetFirstChild
#undef GetFirstChild
#endif

namespace aega
{
  template <typename _TySimpleType>
  static void zeroMemory(_Outref_ _TySimpleType &ref)
  {
    memset(&ref, 0, sizeof(ref));
  }
  template <typename _TySimpleType>
  static size_t zeroMemorySz(_Outref_ _TySimpleType &ref)
  {
    zeroMemory(ref);
    return sizeof(ref);
  }
  template <typename _TySimpleType>
  static void safeDelete(_Outref_ _TySimpleType *&desc)
  {
    if (desc) delete desc, desc = 0;
  }
  template <typename _TySimpleType>
  static void safeDeleteArray(_Outref_ _TySimpleType *&desc)
  {
    if (desc) delete[] desc, desc = 0;
  }
  template <typename _TySimpleType>
  static void safeRelease(_Outref_ _TySimpleType *&desc)
  {
    if (desc) desc->Release(), desc = 0;
  }

  inline static void trace(_In_ const char* fmt, _In_opt_ ...)
  {
    const int length = 512;
    char buffer[length];
    va_list ap;

    va_start(ap, fmt);
    vsnprintf_s(buffer, length - 1, fmt, ap);
    OutputDebugStringA(buffer);
    OutputDebugStringA("\n");
    va_end(ap);
  }


  // __declspec(align(16)) struct MyAlignedType : public AlignedNew<MyAlignedType>
  template<typename TDerived> struct AlignedNew
  {
    static void* operator new (size_t size)
    {
      const size_t alignment = __alignof(TDerived);

      static_assert(alignment > 8,
        "AlignedNew is only useful for types with > 8 byte alignment. "
        "Did you forget a __declspec(align) on TDerived?"
        );

      void* ptr = _aligned_malloc(size, alignment);
      if (!ptr) throw std::bad_alloc();

      return ptr;
    }

    static void operator delete (void* ptr) { _aligned_free(ptr); }
    static void* operator new[](size_t size) { return operator new(size); }
    static void operator delete[](void* ptr) { operator delete(ptr); }
  };

}

#define _Dx_hresult_assert(resultHandle) assert(SUCCEEDED(resultHandle))
#define _Dx_hresult_assert_msg(resultHandle, message) _ASSERTE(SUCCEEDED(resultHandle), message)
#define _Dx_void_ret_on_hresult_failed(resultHandle) if(FAILED(resultHandle)) return;
#define _Dx_error_ret_on_hresult_failed(resultHandle) if(FAILED(resultHandle)) return 1;
#define _Dx_noerror_ret_on_hresult_failed(resultHandle) if(FAILED(resultHandle)) return 0;
