
#include <iostream>
#include "BinaryWriter.h"

using namespace aega::io;

#pragma region class BasicWriter

BasicWriter::BasicWriter(FileWrapper::ShPtr file)
  : m_file(file)
{
  streamProducer.reset(new FileProducer(*file.get()));
}
BasicWriter::~BasicWriter(void)
{
}

bool BasicWriter::WriteBytes(const void *data, size_t elementSize, size_t elementNum)
{
  // check if all the chucks were written
  size_t dataSz = elementSize * elementNum;
  return dataSz == streamProducer->Append((uint8_t*)data, dataSz);
  //return elementNum == fwrite(data, elementSize, elementNum, m_file->GetHandle());
}

bool BasicWriter::WriteString(std::string const &valueToWrite)
{
  uint16_t strLen = (uint16_t)valueToWrite.length();
  // first, store the string length, then, if it`s not empty, store the string data into the file
  return (NextElement(strLen), strLen) && WriteBytes(valueToWrite.c_str(), sizeof(char), valueToWrite.length());
}

bool BasicWriter::WriteString(std::wstring const &valueToWrite)
{
  uint16_t strLen = (uint16_t)valueToWrite.length();
  // first, store the string length, then, if it`s not empty, store the string data into the file
  return (NextElement(strLen), strLen) && WriteBytes(valueToWrite.c_str(), sizeof(wchar_t), valueToWrite.length());

}

bool BasicWriter::WriteUInt32_7BitEnc(uint32_t value)
{
  const size_t bytesNum = sizeof(uint32_t) + 0;
  for (size_t bi = 0; bi < bytesNum; ++bi)
  {
    size_t nextbi = bi + 1;
    uint8_t byteToWrite = (value >> (bi * 7)) & 0xFF;
    bool writeNextByte = (value >> (nextbi * 7)) != 0;
    if (nextbi < bytesNum)
    {
      if (writeNextByte) byteToWrite |= 0x40;
      else byteToWrite &= ~0x40;
    }
    WriteBytes(&byteToWrite, sizeof(uint8_t), 1);
    if (!writeNextByte)
      break;
  }

  return true;
}

bool BasicWriter::WriteInt32_7BitEnc(int32_t valueToWrite)
{
  return WriteUInt32_7BitEnc(*reinterpret_cast<uint32_t*>(&valueToWrite));
}

bool BasicWriter::WriteUInt32(uint32_t valueToWrite)
{
  return WriteBytes(&valueToWrite, sizeof(uint32_t), 1);
}

bool BasicWriter::WriteInt32(int32_t valueToWrite)
{
  return WriteBytes(&valueToWrite, sizeof(int32_t), 1);
}

#pragma endregion

#pragma region classes CompressedWriter, CompressedWriter::Implementation

CompressedWriter::CompressedWriter(FileWrapper::ShPtr file)
  : BasicWriter(file)
{
  streamProducer.reset(new LzmaFileAsyncProducer(*file.get()));
  //streamProducer.reset(new LzmaFileProducer(*file.get()));
}

CompressedWriter::~CompressedWriter()
{
  //delete m_impl;
  streamProducer.reset();
}

bool CompressedWriter::WriteBytes(const void *data, size_t elementSize, size_t elementNum)
{
  size_t dataSz = elementSize * elementNum;
  //streamProducer->Append((uint8_t*)data, elementSize * elementNum);
  //m_impl->Write(data, elementSize * elementNum);
  return dataSz == streamProducer->Append((uint8_t*)data, dataSz);
}
