#include "BasicIO.h"

using namespace aega::io;

File::File(void)
  : fileHandle(0)
{
}

File::~File(void)
{
  TryClose();
}

bool File::TryClose(void)
{
  if (fileHandle)
  {
    // fclose: If the stream is successfully closed, 
    // a zero value is returned.
    if (!fclose(fileHandle))
    {
      fileHandle = 0;
      fullPath = L"";
    }
    else
      return false;
  }

  return true;
}

bool File::Read(const char *fileName)
{
  std::string nameUtf8 = fileName;
  std::wstring nameUtf16(nameUtf8.begin(), nameUtf8.end());
  return Read(nameUtf16.c_str());
}

bool File::Write(const char *fileName)
{
  std::string nameUtf8 = fileName;
  std::wstring nameUtf16(nameUtf8.begin(), nameUtf8.end());
  return Write(nameUtf16.c_str());
}

bool File::Read(const wchar_t *fileName)
{
  if (TryClose())
  {
    if (!_wfopen_s(&fileHandle, fileName, L"rb"))
      // returns zero on success
    {
      fullPath = fileName;
      return true;
    }
  }
  return false;
}

bool File::Write(const wchar_t *fileName)
{
  if (TryClose())
  {
    if (!_wfopen_s(&fileHandle, fileName, L"wb"))
      // returns zero on success
    {
      fullPath = fileName;
      return true;
    }
  }
  return false;
}

bool File::ErrorOccurred(void) const
{
  if (fileHandle != 0) return ferror(fileHandle);
  else return false;
}

bool File::ClearError(void)
{
  if (fileHandle) clearerr(fileHandle);
  return true;
}

File::ShPtr File::OpenFileForReading(const char *pathToFile)
{
  ShPtr filePtr = std::make_shared<File>();
  filePtr->Read(pathToFile);
  return filePtr;
}

File::ShPtr File::OpenFileForWritting(const char *pathToFile)
{
  ShPtr filePtr = std::make_shared<File>();
  filePtr->Write(pathToFile);
  return filePtr;
}

File::ShPtr File::OpenFileForReading(const wchar_t *pathToFile)
{
  ShPtr filePtr = std::make_shared<File>();
  filePtr->Read(pathToFile);
  return filePtr;
}

File::ShPtr File::OpenFileForWritting(const wchar_t *pathToFile)
{
  ShPtr filePtr = std::make_shared<File>();
  filePtr->Write(pathToFile);
  return filePtr;
}

FileConsumer::FileConsumer(File const &file)
    : file(file)
{
    // see: rewind - Set position of stream to the beginning (http://www.cplusplus.com/reference/cstdio/rewind/)
    //fseek(file.GetHandle(), 0, SEEK_SET); // reset position
}

FileConsumer::FileConsumer(File const &file, IStreamConsumer const &consumer)
    : file(file)
    //: FileConsumer(file)
{
    //if (consumer.Good())
    //{
    //  fpos_t filePos = (fpos_t)consumer.Tell();
    //  fsetpos(file.GetHandle(), &filePos); // set previous consumer position
    //}
}

size_t FileConsumer::Tell() const
{
    return ftell(file.GetHandle());
}

bool FileConsumer::Good() const
{
    return !file.ErrorOccurred();
}

size_t FileConsumer::Consume(uint8_t *data, size_t dataSz)
{
    if (data && (dataSz != 0))
    {
        return fread(data, 1, dataSz, file.GetHandle());
        //return fread_s(data, dataSz, 1, dataSz, file.GetHandle());
    }

    return 0;
}

FileProducer::FileProducer(File &file)
    : file(file)
{
}

FileProducer::~FileProducer()
{
}

void FileProducer::Set(size_t pos)
{
    /*errno_t err =*/
    fseek(file.GetHandle(), (long)pos, SEEK_SET);
    //visco_debugTrace("Set: %i", err);
}

size_t FileProducer::Tell(void) const
{
    return ftell(file.GetHandle());
}

size_t FileProducer::Append(uint8_t const *data, size_t dataSz)
{
    if (!file.ErrorOccurred() && data && (dataSz != 0))
        return fwrite(data, 1, dataSz, file.GetHandle());
    return 0;
}
