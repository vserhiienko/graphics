#include <FileSystem.h>
using namespace ciri;

bool FileSystem::getInitialPath(std::string &path)
{
    char exe[MAX_PATH];

    HMODULE moduleHandle = GetModuleHandleA(NULL);
    if (moduleHandle != NULL)
    {
        // When passing NULL to GetModuleHandle, it returns handle of exe itself
        GetModuleFileNameA(moduleHandle, exe, (sizeof(exe)));
        PathRemoveFileSpecA(exe);
        path = exe;
        return true;
    }

    return false;
}

