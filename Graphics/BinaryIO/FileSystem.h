#pragma once

#include <iostream>
#include <Windows.h>
#include <Windowsx.h>
#include <Shlwapi.h>

namespace ciri
{
    class FileSystem
    {
    public:
        static bool getInitialPath(std::string &initialPath);

    public:
        template <typename _Callback, size_t _MaxPath = MAX_PATH>
        static bool findFolderRecursively(std::string folder, std::string pattern, _Callback fileFound);
        template <typename _Callback, size_t _MaxPath = MAX_PATH>
        static bool findFileRecursively(std::string folder, std::string pattern, _Callback fileFound);
    };

#include <FileSystem.inl>

}

