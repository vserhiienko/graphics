
#include "BinaryReader.h"

template <typename _SimpleTy> static size_t TyZeroMemorySz(_SimpleTy &dataRef) { memset(&dataRef, 0, sizeof(_SimpleTy)); return sizeof(_SimpleTy); }
template <typename _SimpleTy> static size_t TyZeroMemorySz(_SimpleTy *dataRef) { memset(dataRef, 0, sizeof(_SimpleTy)); return sizeof(_SimpleTy); }
#define TyZeroMemory(data) (void)TyZeroMemorySz(data) 

using namespace aega::io;

#pragma region class BasicReader

BasicReader::BasicReader(FileWrapper::ShPtr file)
  : m_file(file)
{
  streamConsumer.reset(new FileConsumer(*file.get()));
}

BasicReader::~BasicReader()
{
  streamConsumer.reset();
}

bool BasicReader::ReadBytes(void *data, size_t elementSize, size_t elementNum)
{
  size_t dataSz = elementSize * elementNum;
  return dataSz == streamConsumer->Consume((uint8_t*)data, dataSz);
}

bool BasicReader::ReadUInt32(uint32_t &outValue)
{
  return ReadBytes(&outValue, sizeof(uint32_t), 1);
}
bool BasicReader::ReadUInt32_7BitEnc(uint32_t &value)
{
  const size_t bytesNum = sizeof(uint32_t) + 0;
  value = 0;
  for (size_t bi = 0; bi < bytesNum; ++bi)
  {
    size_t nextbi = bi + 1;
    uint8_t byte;
    ReadBytes(&byte, sizeof(uint8_t), 1);
    bool readNextByte = ((byte >> 7) & 1) == 1;
    uint8_t mask = (nextbi < bytesNum) ? 0x7F : 0xFF;
    value |= ((uint32_t)(byte & mask)) << (bi * 7);
    if (!readNextByte)
      break;
  }
  return true;
}

bool BasicReader::ReadInt32(int32_t &outValue)
{
  return ReadBytes(&outValue, sizeof(int32_t), 1);
}

bool BasicReader::ReadInt32_7BitEnc(int32_t &outValue)
{
  return ReadUInt32_7BitEnc(reinterpret_cast<uint32_t&>(outValue));
}

bool BasicReader::ReadString(std::string &outValue)
{
  uint16_t strLen = 0;

  // read the string length 
  if (NextElement(strLen), strLen > 0)
  {
    // try allocate requied memory
    if (char *strBuff = new char[strLen + 1])
    {
      bool readingSucceeded = false;
      // try read the string from file into the allocated memory
      if (ReadBytes(strBuff, sizeof(char), strLen))
      {
        strBuff[strLen] = L'\0'; // set string end pos
        outValue = strBuff;
        readingSucceeded = true;
      }

      delete[] strBuff;
      return readingSucceeded;
    }
  }

  return false;
}

bool BasicReader::ReadString(std::wstring &outValue)
{
  uint16_t strLen = 0;

  // read the string length 
  if (NextElement(strLen), strLen > 0)
  {
    // try allocate requied memory
    if (wchar_t *strBuff = new wchar_t[strLen + 1])
    {
      bool readingSucceeded = false;
      // try read the string from file into the allocated memory
      if (ReadBytes(strBuff, sizeof(wchar_t), strLen))
      {
        strBuff[strLen] = L'\0'; // set string end pos
        outValue = strBuff;
        readingSucceeded = true;
      }

      delete[] strBuff;
      return readingSucceeded;
    }
  }

  return false;
}

#pragma endregion

#pragma region classes CompressedReader, CompressedReader::Implementation

CompressedReader::CompressedReader(FileWrapper::ShPtr file)
  : BasicReader(file)
{
  streamConsumer.reset(new LzmaFileAsyncConsumer(*file.get()));
}

CompressedReader::~CompressedReader()
{
  streamConsumer.reset();
}

bool CompressedReader::ReadBytes(void *data, size_t elementSize, size_t elementNum)
{
  return streamConsumer->Consume((uint8_t*)data, elementSize * elementNum);
}

#pragma endregion