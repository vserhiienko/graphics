#include <ppl.h>
#include <ppltasks.h>
#include <concurrent_queue.h>

// compression/decompression
#include "LzmaEnc.h"
#include "LzmaDec.h"

#include "private/BasicIO.Utils.h"

#include "BasicIO.h"

using namespace aega;
using namespace aega::io;

#define visco_debugTrace trace
#define SAFE_DELETE_PTR safeDelete

typedef uint64_t aega_lzma_size_t;

struct LzmaConsumerProducerTraits
{

  static uint8_t const kLzmaPropsSz = LZMA_PROPS_SIZE;
  static uint8_t const kLzmaPropsChunkSz = sizeof(aega_lzma_size_t);
  static uint8_t const kLzmaHeaderSz = kLzmaPropsSz + kLzmaPropsChunkSz;
  static aega_lzma_size_t const kLzmaChunkSz = 64 * 1024 * 1024; // 64mb

  static void *SzAlloc(void *p, size_t size) { (void)p; return MyAlloc(size); }
  static void *MyAlloc(size_t size) { if (size == 0) return 0; return malloc(size); }
  static void SzFree(void *p, void *address) { (void)p; MyFree(address); }
  static void MyFree(void *address) { free(address); }
};

struct ILzmaStreamProducer::LzmaContext
{
  struct CSeqInStream
  {
    ISeqInStream readerFuncTable;
    LzmaContext *self;
  };
  struct CSeqOutStream
  {
    ISeqOutStream writerFuncTable;
    LzmaContext *self;
  };

  Blob blobEnc, blobEnc2;
  aega_lzma_size_t chunkSz;
  aega_lzma_size_t writtenSz;
  size_t blobEncPos;
  aega_lzma_size_t blobEncGoodSz;

  bool encGood;
  SRes lastError;

  ISzAlloc alloc;
  CLzmaEncHandle enc;
  CLzmaEncProps encProps;
  CSeqInStream readerStream;
  CSeqOutStream writerStream;

  BlobConsumer blobConsumer;
  IStreamProducer *genericWriter;

  static void AppendChunkSzToHeader(uint8_t(&header)[LzmaConsumerProducerTraits::kLzmaHeaderSz], aega_lzma_size_t chunkSz)
  {
    uint8_t nextByte = 0;
    uint8_t headerSize = LzmaConsumerProducerTraits::kLzmaPropsSz;
    for (uint8_t i = 0; i < LzmaConsumerProducerTraits::kLzmaPropsChunkSz; i++)
      nextByte = (uint8_t)(chunkSz >> (8 * i)),
      header[headerSize++] = nextByte;
  }

  static SRes ReadFromBlobEnc(void *p, void *buf, aega_lzma_size_t *size)
  {
    if (*size == 0) return 0;
    CSeqInStream *inStream = static_cast<CSeqInStream*>(p);
    (*size) = inStream->self->blobConsumer.Consume((uint8_t*)buf, *size);
    visco_debugTrace("basicIO: reading %u", (uint32_t)*size);
    return SZ_OK;
  }

  static aega_lzma_size_t WriteToCustomProducer(void *pp, const void *buf, aega_lzma_size_t size)
  {

    if (size == 0) return SZ_OK;
    CSeqOutStream *outStream = static_cast<CSeqOutStream*>(pp);
    aega_lzma_size_t producedSz = outStream->self->genericWriter->Append((const uint8_t*)buf, size);

    outStream->self->writtenSz += size;
    visco_debugTrace("basicIO: writing %u (%u)", (uint32_t)size, (uint32_t)outStream->self->writtenSz);

    return producedSz;
  }

  LzmaContext()
    : blobEncPos(0)
    , blobEncGoodSz(0)
    , writtenSz(0)
    , blobConsumer(blobEnc)
  {
    chunkSz = LzmaConsumerProducerTraits::kLzmaChunkSz;
    blobEnc.Initialize(chunkSz);
    blobEnc2.Initialize(chunkSz);

    encGood = enc != 0;
  }

  ~LzmaContext()
  {
    //LzmaEnc_Destroy(enc, &alloc, &alloc);
  }

  void Encode()
  {
    alloc.Alloc = LzmaConsumerProducerTraits::SzAlloc;
    alloc.Free = LzmaConsumerProducerTraits::SzFree;

    uint8_t header[LzmaConsumerProducerTraits::kLzmaHeaderSz];
    aega_lzma_size_t propsSz = LzmaConsumerProducerTraits::kLzmaPropsSz;
    aega_lzma_size_t headerSz = LzmaConsumerProducerTraits::kLzmaHeaderSz;

    LzmaEncProps_Init(&encProps);
    encProps.writeEndMark = 1;
    //assert(lastError == SZ_OK);
    AppendChunkSzToHeader(header, blobEncPos);

    size_t blobEnc2Sz = blobEnc2.Size();
    size_t propsSz2 = LZMA_PROPS_SIZE;

    SRes res = LzmaEncode(
      blobEnc2.Data(), &blobEnc2Sz,
      blobEnc.Data(), blobEncPos,
      &encProps,
      header,
      &propsSz2,
      1, 0,
      &alloc, &alloc
      );

    if (res == SZ_OK)
    {
      genericWriter->Append(header, headerSz);
      genericWriter->Append((uint8_t*)&blobEnc2Sz, sizeof(blobEnc2Sz));
      genericWriter->Append(blobEnc2.Data(), blobEnc2Sz);
      visco_debugTrace("basicIO: encoding %u -> %u", blobEncPos, blobEnc2Sz);
    }

    blobEncPos = 0;
  }
};

ILzmaStreamProducer::ILzmaStreamProducer(void)
  : impl(nullptr)
{
}

ILzmaStreamProducer::~ILzmaStreamProducer(void)
{
  SAFE_DELETE_PTR(impl);
}

int ILzmaStreamProducer::InitializeCompression(int compressionFlavours, size_t chunkSize)
{
  SAFE_DELETE_PTR(impl);
  impl = new LzmaContext;
  return impl->lastError;
}

int ILzmaStreamProducer::CompressChunk()
{
  //visco_debugTrace("CompressChunk: ...");
  impl->Encode();
  return SZ_OK;
}

int ILzmaStreamProducer::FinalizeCompression(void)
{
  if (impl && impl->blobEncPos)
    CompressChunk();
  SAFE_DELETE_PTR(impl);
  return SZ_OK;
}

size_t ILzmaStreamProducer::Append(uint8_t const *data, size_t dataSz)
{
  if (impl->blobEnc.Size() == impl->blobEncPos)
    CompressChunk(),
    impl->blobEncPos = 0;

  size_t canAppendSz = impl->blobEnc.Size() - impl->blobEncPos;
  if (canAppendSz >= dataSz)
  {
    // the data can still be written
    memcpy(impl->blobEnc.Data() + impl->blobEncPos, data, dataSz);
    impl->blobEncPos += dataSz; // advance counter
  }
  else
  {
    // finish this block
    Append(data, canAppendSz);
    assert(impl->blobEncPos == impl->blobEnc.Size());

    CompressChunk();
    impl->blobEncPos = 0;

    // start the new one
    Append(data + canAppendSz, dataSz - canAppendSz);
  }

  return dataSz;
}


LzmaFileProducer::LzmaFileProducer(File &file)
  : file(file)
  , fileProducer(file)
{
  InitializeCompression(0, 0);
  impl->genericWriter = &fileProducer;
}
LzmaFileProducer::~LzmaFileProducer(void)
{
  FinalizeCompression();
}

struct ILzmaStreamConsumer::LzmaContext
{
  aega_lzma_size_t chunkSz;
  ISzAlloc alloc;
  IStreamConsumer *genericReader; // reads compressed data from file or elsewhere

  Blob packedBlob, blob;
  aega_lzma_size_t blobPos, blobGoodSz;
  aega_lzma_size_t packedBlobPos, packedBlobGoodSz;
  aega_lzma_size_t unpackedSz, packedSz;

  int res;
  int thereIsSize; /* = 1, if there is uncompressed size in headers */

  CLzmaDec state;
  unsigned char header[LZMA_PROPS_SIZE + 8];

  LzmaContext()
  {
    chunkSz = LzmaConsumerProducerTraits::kLzmaChunkSz;
    blobPos = 0, blobGoodSz = 0;
    packedBlobPos = 0, packedBlobGoodSz = 0;
    unpackedSz = 0, packedSz = 0;
    thereIsSize = 0, res = 0;
  }

  ~LzmaContext()
  {
  }

  void DecodeNextBlock()
  {
    alloc.Alloc = LzmaConsumerProducerTraits::SzAlloc;
    alloc.Free = LzmaConsumerProducerTraits::SzFree;

    genericReader->Consume(header, sizeof(header));

    unpackedSz = 0;
    for (int i = 0; i < 8; i++)
    {
      unsigned char b = header[LZMA_PROPS_SIZE + i];
      if (b != 0xFF) thereIsSize = 1;
      unpackedSz += (UInt64)b << (i * 8);
    }

    genericReader->Consume((uint8_t*)&packedSz, sizeof(packedSz));

    blob.Initialize(unpackedSz * 3 / 2);
    packedBlob.Initialize(packedSz);

    aega_lzma_size_t packedReadSz = genericReader->Consume(packedBlob.Data(), packedSz);

    size_t srcLen = packedBlob.Size();
    size_t destLen = blob.Size();
    ELzmaStatus status = LZMA_STATUS_NOT_SPECIFIED;

    SRes res = LzmaDecode(
      blob.Data(), &destLen,
      packedBlob.Data(), &srcLen,
      header, LZMA_PROPS_SIZE,
      LZMA_FINISH_END, &status,
      &alloc
      );

    visco_debugTrace("basicIO: decoding (%i == 0) %u %u -> %u %u (%i == 1)"
      , res
      , (uint32_t)unpackedSz, (uint32_t)packedSz
      , (uint32_t)destLen, (uint32_t)srcLen
      , status
      );

    blobGoodSz = destLen;
    blobPos = 0;

  }

  void Decode()
  {
    if (blobPos == blobGoodSz) DecodeNextBlock();
    assert(blobPos == 0);
  }

};

bool ILzmaStreamConsumer::Good(void) const
{
  return false;
}

size_t ILzmaStreamConsumer::Tell(void) const
{
  return 0;
}

size_t ILzmaStreamConsumer::Consume(uint8_t *data, size_t dataSz)
{
  if (dataSz == 0 || data == nullptr) return 0;

  aega_lzma_size_t consumedSz = 0;
  aega_lzma_size_t canConsumeSz = impl->blobGoodSz - impl->blobPos;

  if (canConsumeSz >= dataSz)
  {
    memcpy(data, impl->blob.Data() + impl->blobPos, dataSz);
    impl->blobPos += dataSz;
    consumedSz += dataSz;
  }
  else
  {
    visco_debugTrace("basicIO: can consume %u (%u)"
      , canConsumeSz, dataSz
      );

    consumedSz += Consume(data, canConsumeSz);
    impl->Decode();

    aega_lzma_size_t dataInsufficiencySz = dataSz - canConsumeSz;
    consumedSz += Consume(data + canConsumeSz, dataInsufficiencySz);

  }

  assert(dataSz == consumedSz);
  return consumedSz;

}

LzmaFileConsumer::LzmaFileConsumer(File &file)
  : file(file)
  , fileConsumer(file)
{
  impl = new LzmaContext();
  impl->genericReader = &fileConsumer;
}

LzmaFileConsumer::~LzmaFileConsumer(void)
{
  delete impl;
}

struct ILzmaStreamAsyncConsumer::LzmaContext
{
  static uint8_t const kLzmaPropsSz = LZMA_PROPS_SIZE;
  static uint8_t const kLzmaPropsChunkSz = sizeof(aega_lzma_size_t);
  static uint8_t const kLzmaHeaderSz = kLzmaPropsSz + kLzmaPropsChunkSz;
  static aega_lzma_size_t const kLzmaChunkSz = 64 * 1024 * 1024; // 64mb
  static aega_lzma_size_t const IN_BUF_SIZE = kLzmaChunkSz; // 64mb
  static aega_lzma_size_t const OUT_BUF_SIZE = kLzmaChunkSz; // 64mb

  struct BlobPair;
  struct UnpackTask;
  struct AsyncUnpacker;

  struct BlobPair
  {
    aega_lzma_size_t pos;
    int thereIsSize;
    aega_lzma_size_t blobPos, blobGoodSz;
    aega_lzma_size_t unpackedSz, packedSz;
    unsigned char header[LzmaConsumerProducerTraits::kLzmaHeaderSz];

    Blob packed;
    Blob unpacked;
    UnpackTask *unpackHost;
  };

  struct UnpackTask
  {
    BlobPair *attachedPair;
    concurrency::task<void> unpackTask;
  };

  struct AsyncUnpacker
  {
    LzmaContext &contextRef;

    bool eof;
    aega_lzma_size_t sz;
    std::atomic_size_t pos;
    std::atomic_size_t activeConsumes;
    std::vector<BlobPair*> pairs;
    concurrency::concurrent_queue<UnpackTask*> tasks;
    concurrency::concurrent_queue<BlobPair*> consumedPairs;

    AsyncUnpacker(LzmaContext &context, aega_lzma_size_t threads = 32)
      : contextRef(context)
      , sz(threads)
      , eof(false)
    {
      activeConsumes = 0;

      pairs.resize(sz);
      for (auto &p : pairs) p = new BlobPair();
      for (auto &p : pairs) consumedPairs.push(p);
    }

    ~AsyncUnpacker()
    {
      for (auto &p : pairs) SAFE_DELETE_PTR(p);
    }

    BlobPair *Consume()
    {
      UnpackTask *nextTask = 0;
      while (!tasks.try_pop(nextTask))
        TryUnpackMore();
      nextTask->unpackTask.wait();
      activeConsumes++;
      BlobPair *pair = nextTask->attachedPair;
      delete nextTask;
      return pair;
    }

    void OnConsumed(BlobPair *pair)
    {
      activeConsumes--;
      consumedPairs.push(pair);
      TryUnpackMore();
    }

    void TryUnpackMore()
    {
      aega_lzma_size_t consumedInd = 0;
      aega_lzma_size_t consumedCount = sz - activeConsumes.load();

      while ((consumedInd < consumedCount) && (!eof))
      {
        consumedInd++;

        BlobPair *producablePair = 0;
        if (!consumedPairs.try_pop(producablePair)) return;
        if (!producablePair) return;

        producablePair->packedSz = 0;
        producablePair->unpackedSz = 0;

        aega_lzma_size_t headerReadSz = contextRef.genericReader->Consume(producablePair->header, sizeof(producablePair->header));

        if (headerReadSz != sizeof(producablePair->header))
        {
          visco_debugTrace("basicIO: eof", (uint32_t)consumedCount);
          eof = true;
          return;
        }

        for (int i = 0; i < 8; i++)
        {
          unsigned char b = producablePair->header[LZMA_PROPS_SIZE + i];
          if (b != 0xff) producablePair->thereIsSize = 1;
          producablePair->unpackedSz += (UInt64)(b << (i * 8));
        }

        contextRef.genericReader->Consume((uint8_t*)&producablePair->packedSz, sizeof(producablePair->packedSz));

        producablePair->packed.Initialize(producablePair->packedSz);
        producablePair->unpacked.Initialize(producablePair->unpackedSz);

        producablePair->packedSz = contextRef.genericReader->Consume(producablePair->packed.Data(), producablePair->packedSz);
        producablePair->pos = contextRef.genericReader->Tell();

        UnpackTask *task = new UnpackTask();
        tasks.push(task);

        task->attachedPair = producablePair;

        visco_debugTrace("basicIO: task enqueued");
        task->unpackTask = concurrency::create_task([producablePair]()
        {
          visco_debugTrace("basicIO: task launched @ %u", (uint32_t)producablePair->pos);

          ISzAlloc alloc;
          alloc.Alloc = LzmaConsumerProducerTraits::SzAlloc;
          alloc.Free = LzmaConsumerProducerTraits::SzFree;

          size_t srcLen = producablePair->packed.Size();
          size_t destLen = producablePair->unpacked.Size();
          ELzmaStatus status = LZMA_STATUS_NOT_SPECIFIED;

          SRes res = LzmaDecode(
            producablePair->unpacked.Data(), &destLen,
            producablePair->packed.Data(), &srcLen,
            producablePair->header, LZMA_PROPS_SIZE,
            LZMA_FINISH_END, &status,
            &alloc
            );

          producablePair->blobGoodSz = destLen;
          producablePair->blobPos = 0;

          visco_debugTrace("basicIO: task done, results: (%i == 0[ok]) %u %u -> %u %u (%i == 1[mark])"
            , res
            , (uint32_t)producablePair->unpackedSz
            , (uint32_t)producablePair->packedSz
            , (uint32_t)destLen, (uint32_t)srcLen
            , status
            );

        });
      }

    }

  };

  BlobPair *pair;
  AsyncUnpacker asyncUnpacker;

  // reads compressed data from file or elsewhere
  IStreamConsumer *genericReader;

  LzmaContext()
    : asyncUnpacker(*this)
    , pair(0)
  {
  }

  ~LzmaContext()
  {
  }

  void Decode()
  {
    pair = asyncUnpacker.Consume();
  }

};

bool ILzmaStreamAsyncConsumer::Good(void) const
{
  return false;
}

size_t ILzmaStreamAsyncConsumer::Tell(void) const
{
  return 0;
}

size_t ILzmaStreamAsyncConsumer::Consume(uint8_t *data, size_t dataSz)
{
  if (dataSz == 0 || data == nullptr) return 0;

  aega_lzma_size_t consumedSz = 0;
  aega_lzma_size_t canConsumeSz = impl->pair ? (impl->pair->blobGoodSz - impl->pair->blobPos) : 0;

  if (canConsumeSz >= dataSz)
  {
    memcpy(data, impl->pair->unpacked.Data() + impl->pair->blobPos, dataSz);
    impl->pair->blobPos += dataSz;
    consumedSz += dataSz;
  }
  else
  {
    visco_debugTrace("basicIO: consumed %u, needed %u => insufficiency %u"
      , (uint32_t)canConsumeSz, (uint32_t)dataSz
      , (uint32_t)(dataSz - canConsumeSz)
      );

    consumedSz += Consume(data, canConsumeSz);
    impl->asyncUnpacker.OnConsumed(impl->pair);
    impl->Decode();

    aega_lzma_size_t dataInsufficiencySz = dataSz - canConsumeSz;
    consumedSz += Consume(data + canConsumeSz, dataInsufficiencySz);

  }

  return consumedSz;
}

LzmaFileAsyncConsumer::LzmaFileAsyncConsumer(File &file)
  : file(file)
  , fileConsumer(file)
{
  impl = new LzmaContext();
  impl->genericReader = &fileConsumer;
}

LzmaFileAsyncConsumer::~LzmaFileAsyncConsumer(void)
{
  delete impl;
}


// -----------------------------------------------------

class LockSRW
{
  SRWLOCK lock;

public:
  LockSRW() : lock(SRWLOCK_INIT) {}

public:
  inline bool tryAcquireW() { return TryAcquireSRWLockExclusive(&lock) != BOOLEAN(0); }
  inline bool tryAcquireR() { return TryAcquireSRWLockShared(&lock) != BOOLEAN(0); }
  inline void acquireW() { AcquireSRWLockExclusive(&lock); }
  inline void releaseW() { ReleaseSRWLockExclusive(&lock); }
  inline void acquireR() { AcquireSRWLockShared(&lock); }
  inline void releaseR() { ReleaseSRWLockShared(&lock); }
};

struct ILzmaStreamAsyncProducer::LzmaContext
{
  static void AppendChunkSzToHeader(
    uint8_t(&header)[LzmaConsumerProducerTraits::kLzmaHeaderSz], 
    aega_lzma_size_t chunkSz
    )
  {
    uint8_t nextByte = 0;
    uint8_t headerSize = LzmaConsumerProducerTraits::kLzmaPropsSz;
    for (uint8_t i = 0; i < LzmaConsumerProducerTraits::kLzmaPropsChunkSz; i++)
      nextByte = (uint8_t)(chunkSz >> (8 * i)),
      header[headerSize++] = nextByte;
  }

  struct BlobPool
  {
    struct Chunk
    {
      size_t ind;
      aega_lzma_size_t pos, sz;
      concurrency::event append;

      Blob blob;
      BlobPool &pool;
      uint8_t header[LzmaConsumerProducerTraits::kLzmaHeaderSz];

      Chunk(size_t ind, BlobPool &pool)
        : pos(0)
        , ind(ind)
        , pool(pool)
      {
        blob.Initialize(LzmaConsumerProducerTraits::kLzmaChunkSz);
        sz = blob.Size();
        append.reset();
      }

      SRes encodeTo(Chunk *other_chunk)
      {
        ISzAlloc alloc;
        alloc.Alloc = LzmaConsumerProducerTraits::SzAlloc;
        alloc.Free = LzmaConsumerProducerTraits::SzFree;

        aega_lzma_size_t propsSz = LzmaConsumerProducerTraits::kLzmaPropsSz;
        aega_lzma_size_t headerSz = LzmaConsumerProducerTraits::kLzmaHeaderSz;

        CLzmaEncProps encProps;
        LzmaEncProps_Init(&encProps);
        encProps.writeEndMark = 1;

        AppendChunkSzToHeader(other_chunk->header, pos);

        size_t outPackedSz = other_chunk->blob.Size();
        size_t InUnpackedSz = pos;
        size_t InPropsSz = propsSz;

        SRes result = LzmaEncode(
          other_chunk->blob.Data(), &outPackedSz,
          blob.Data(), InUnpackedSz,
          &encProps, other_chunk->header, &InPropsSz,
          1, 0, &alloc, &alloc
          );

        other_chunk->sz = outPackedSz;
        other_chunk->pos = outPackedSz;
        return result;

      }

      void appendTo(aega::io::IStreamProducer *w)
      {
        w->Append(header, sizeof(header));
        w->Append((uint8_t*)&sz, sizeof(sz));
        w->Append(blob.Data(), pos);
      }

    };

    std::vector<Chunk*> blobs;
    std::atomic_size_t blob_count;
    concurrency::event unused_blobs_available;
    concurrency::concurrent_queue<Chunk*> unused_blobs;

    BlobPool(size_t threadC = 16)
    {
      blobs.resize(threadC);

      size_t ind = 0;
      for (auto &p : blobs)
      {
        unused_blobs.push(p = new Chunk(ind++, (*this)));
      }

      blob_count = blobs.size();
    }

    ~BlobPool()
    {
      for (auto &p : blobs)
      {
        safeDelete(p);
      }

    }

    Chunk *awaitChunk()
    {
      if (!blob_count.load())
      {
        unused_blobs_available.wait();
      }

      Chunk * nextChunk = 0;
      while (!unused_blobs.try_pop(nextChunk) && nextChunk);
      --blob_count;
      if (!blob_count.load())
      {
        unused_blobs_available.reset();
      }
      nextChunk->pos = 0;
      return nextChunk;
    }

    void releaseChunk(Chunk *chunk)
    {
      size_t prev_blob_c = blob_count.load();
      ++blob_count;

      chunk->pos = 0;
      unused_blobs.push(chunk);
      if (prev_blob_c == 0)
      {
        unused_blobs_available.set();
      }
    }
  };

  BlobPool decPool;
  BlobPool encPool;
  BlobPool::Chunk *chunk;
  IStreamProducer *genericWriter;
  concurrency::task<void> writer_routine;

  LzmaContext(size_t threadCount = 16)
    : decPool(threadCount)
    , encPool(threadCount)
    , chunk(0)
  {
    chunk = decPool.awaitChunk();
    writer_routine = concurrency::create_task([]
    {
      /*visco_debugTrace(
        "<async> appending routine initialized"
        );*/
    });
  }

  ~LzmaContext()
  {
    writer_routine.wait();
  }

  void Encode()
  {
    auto chunk_to_pack = chunk;
    auto enc_chunk = encPool.awaitChunk();

    enc_chunk->append.reset();
    writer_routine = writer_routine.then([this, enc_chunk]
    {
      /*visco_debugTrace(
        "<async> waiting for chunk @ %u"
        , enc_chunk->ind
        );*/

      enc_chunk->append.wait();

      /*visco_debugTrace(
        "<async> appending chunk @ %u"
        , enc_chunk->ind
        );*/

      enc_chunk->appendTo(genericWriter);
      encPool.releaseChunk(enc_chunk);
    });

    concurrency::create_task([this, chunk_to_pack, enc_chunk]
    {
      /*visco_debugTrace(
        "<async> packing @ %u to @ %u"
        , chunk_to_pack->ind
        , enc_chunk->ind
        );*/

      chunk_to_pack->encodeTo(enc_chunk);

     /* visco_debugTrace(
        "<async> packed @ %u to @ %u (%u -> %u, %f)"
        , chunk_to_pack->ind
        , enc_chunk->ind
        , chunk_to_pack->pos
        , enc_chunk->sz
        );*/

      decPool.releaseChunk(chunk_to_pack);
      enc_chunk->append.set();
    });

    chunk = decPool.awaitChunk();
  }

};

ILzmaStreamAsyncProducer::ILzmaStreamAsyncProducer(void)
  : impl(nullptr)
{
}

ILzmaStreamAsyncProducer::~ILzmaStreamAsyncProducer(void)
{
  SAFE_DELETE_PTR(impl);
}

int ILzmaStreamAsyncProducer::InitializeCompression(int compressionFlavours, size_t chunkSize)
{
  SAFE_DELETE_PTR(impl);
  impl = new LzmaContext;
  return SZ_OK;
}

int ILzmaStreamAsyncProducer::CompressChunk()
{
  //visco_debugTrace("CompressChunk: ...");
  impl->Encode();
  return SZ_OK;
}

int ILzmaStreamAsyncProducer::FinalizeCompression(void)
{
  if (impl && impl->chunk && impl->chunk->pos)
    impl->Encode();
  SAFE_DELETE_PTR(impl);
  return SZ_OK;
}

size_t ILzmaStreamAsyncProducer::Append(uint8_t const *data, size_t dataSz)
{
  auto storage = impl->chunk;
  if (storage->sz == storage->pos)
  {
    CompressChunk();
  }

  aega_lzma_size_t canAppendSz = storage->sz - storage->pos;
  if (canAppendSz >= dataSz)
  {
    // the data can still be written
    memcpy(storage->blob.Data() + storage->pos, data, dataSz);
    storage->pos += dataSz; // advance counter
  }
  else
  {
    // finish this block
    Append(data, canAppendSz);
    assert(storage->sz == storage->pos);

    CompressChunk();

    // start the new one
    Append(data + canAppendSz, dataSz - canAppendSz);
  }

  return dataSz;
}

LzmaFileAsyncProducer::LzmaFileAsyncProducer(File &file)
  : file(file)
  , fileProducer(file)
{
  InitializeCompression(0, 0);
  impl->genericWriter = &fileProducer;
}

LzmaFileAsyncProducer::~LzmaFileAsyncProducer(void)
{
  FinalizeCompression();
}
