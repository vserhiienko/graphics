template <typename _Callback, size_t _MaxPath>
bool FileSystem::findFileRecursively(std::string folder, std::string pattern, _Callback fileFound)
{
    LPCSTR
        lpFolder = folder.c_str(),
        lpPattern = pattern.c_str();

    HANDLE hFindFile;
    CHAR szFullPattern[_MaxPath];
    WIN32_FIND_DATAA FindFileData;

    // now we are going to look for the matching files
    PathCombineA(szFullPattern, lpFolder, lpPattern);
    hFindFile = FindFirstFileA(szFullPattern, &FindFileData);
    if (hFindFile != INVALID_HANDLE_VALUE)
    {
        do
        {
            if (!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
            {
                // found a file; do something with it
                PathCombineA(szFullPattern, lpFolder, FindFileData.cFileName);
                if (fileFound(szFullPattern, lpFolder, FindFileData.cFileName))
                {
                    FindClose(hFindFile);
                    return true;
                }
                //printf("%s\n"), szFullPattern);
            }
        } while (FindNextFileA(hFindFile, &FindFileData));
        FindClose(hFindFile);
    }

    // first we are going to process any subdirectories
    PathCombineA(szFullPattern, lpFolder, "*");
    hFindFile = FindFirstFileA(szFullPattern, &FindFileData);
    if (hFindFile != INVALID_HANDLE_VALUE)
    {
        do
        {
            if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && 
                std::string(FindFileData.cFileName) != ".." &&
                std::string(FindFileData.cFileName) != ".")
            {
                // found a subdirectory; recurse into it
                PathCombineA(szFullPattern, lpFolder, FindFileData.cFileName);
                if (findFileRecursively(szFullPattern, lpPattern, fileFound))
                {
                    FindClose(hFindFile);
                    return true;
                }
            }
        } while (FindNextFileA(hFindFile, &FindFileData));
        FindClose(hFindFile);
    }

    return false;
}

template <typename _Callback, size_t _MaxPath>
bool FileSystem::findFolderRecursively(std::string folder, std::string pattern, _Callback folderFound)
{
    LPCSTR
        lpFolder = folder.c_str(),
        lpPattern = pattern.c_str();

    HANDLE hFindFile;
    CHAR szFullPattern[_MaxPath];
    WIN32_FIND_DATAA FindFileData;

    // first we are going to process any subdirectories
    PathCombineA(szFullPattern, lpFolder, "*");
    hFindFile = FindFirstFileA(szFullPattern, &FindFileData);
    if (hFindFile != INVALID_HANDLE_VALUE)
    {
        do
        {
            if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
                std::string(FindFileData.cFileName) != ".." &&
                std::string(FindFileData.cFileName) != ".")
            {
                if (std::string(FindFileData.cFileName) == pattern)
                if (folderFound(lpFolder, FindFileData.cFileName))
                {
                    FindClose(hFindFile);
                    return true;
                }

                // found a subdirectory; recurse into it
                PathCombineA(szFullPattern, lpFolder, FindFileData.cFileName);
                if (findFolderRecursively(szFullPattern, lpPattern, folderFound))
                {
                    FindClose(hFindFile);
                    return true;
                }
            }
        } while (FindNextFileA(hFindFile, &FindFileData));
        FindClose(hFindFile);
    }

    return false;
}