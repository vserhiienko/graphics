struct GSOutput
{
  float4 position : SV_POSITION;
  float4 color : COLOR;
};

cbuffer ShapeTransform : register (b0)
{
  row_major float4x4 World;
  float4 Color;
};

cbuffer CameraMatrices : register (b1)
{
  row_major float4x4 View;
  row_major float4x4 Proj;
};

static float4 cube_points[8] =
{
  { -0.5f, -0.5f, -0.5f, +1.0f },
  { -0.5f, -0.5f, +0.5f, +1.0f },
  { -0.5f, +0.5f, -0.5f, +1.0f },
  { -0.5f, +0.5f, +0.5f, +1.0f },
  { +0.5f, -0.5f, -0.5f, +1.0f },
  { +0.5f, -0.5f, +0.5f, +1.0f },
  { +0.5f, +0.5f, -0.5f, +1.0f },
  { +0.5f, +0.5f, +0.5f, +1.0f },
};

static const uint cube_indices[24] =
{
  0, 1,
  0, 2,
  2, 3,
  1, 3,

  0, 4,
  2, 6,
  1, 5,
  3, 7,

  4, 5,
  4, 6,
  5, 7,
  6, 7,
};

[maxvertexcount(24)]
void main(
  point uint _[1] : NOTT_PASS_THROUGH,
  inout LineStream<GSOutput> output
)
{
  float4 cube_points_clip[8];
  for (uint i = 0; i < 8; i++)
    cube_points_clip[i] = mul(cube_points[i], World),
    cube_points_clip[i] = mul(cube_points_clip[i], View),
    cube_points_clip[i] = mul(cube_points_clip[i], Proj);

  GSOutput element;
  element.color = Color;
  uint k = 0;
  for (uint i = 0; i < 12; ++i)
  {
    for (uint j = 0; j < 2; ++j)
      element.position = cube_points_clip[cube_indices[k]],
      output.Append(element),
      ++k;

    output.RestartStrip();
  }
}