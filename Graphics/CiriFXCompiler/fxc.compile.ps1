foreach ($file in Get-ChildItem "./" | Where-Object {($_.ToString().EndsWith(".PS.hlsl"))})
{
	$fileBase = [io.path]::GetFileNameWithoutExtension($file)
	$fileInput = $fileBase.ToString() + ".hlsl"
	$fileOutput = $fileBase.ToString() + ".cso"
	$fxcExpression = "./fxc._.exe /nologo /O3 /T ps_5_1 /Fo " + $fileOutput + " " + $fileInput
	Write-Host  $fxcExpression
	Invoke-Expression $fxcExpression
}
foreach ($file in Get-ChildItem "./" | Where-Object {($_.ToString().EndsWith(".GS.hlsl"))})
{
	$fileBase = [io.path]::GetFileNameWithoutExtension($file)
	$fileInput = $fileBase.ToString() + ".hlsl"
	$fileOutput = $fileBase.ToString() + ".cso"
	$fxcExpression = "./fxc._.exe /nologo /O3 /T gs_5_1 /Fo " + $fileOutput + " " + $fileInput
	Write-Host  $fxcExpression
	Invoke-Expression $fxcExpression
}
foreach ($file in Get-ChildItem "./" | Where-Object {($_.ToString().EndsWith(".VS.hlsl"))})
{
	$fileBase = [io.path]::GetFileNameWithoutExtension($file)
	$fileInput = $fileBase.ToString() + ".hlsl"
	$fileOutput = $fileBase.ToString() + ".cso"
	$fxcExpression = "./fxc._.exe /nologo /O3 /T vs_5_1 /Fo " + $fileOutput + " " + $fileInput
	Write-Host  $fxcExpression
	Invoke-Expression $fxcExpression
}
robocopy "./" "./../assets/shaders/" *.cso
