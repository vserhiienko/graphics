#include <BasicComputeSampleApp.h>
using namespace nyx;

BasicComputeSampleApp::BasicComputeSampleApp(NvPlatformContext *context)
: NvSampleApp(context, "BasicCompute:Nyx")
{
  m_framerate = new NvFramerateCounter(this);
  m_aspectRatio = 1.0f;
  m_enableFilter = true;
  m_workgroupSize = 16;

  forceLinkHack();

  NVWindowsLog("BasicComputeSampleApp::BasicComputeSampleApp()\n");
}

BasicComputeSampleApp::~BasicComputeSampleApp()
{
  NVWindowsLog("BasicComputeSampleApp::~BasicComputeSampleApp()\n");
}

void BasicComputeSampleApp::initUI(void)
{
  if (mTweakBar)
  {
    mTweakBar->addValue(
      "Enable filter",
      m_enableFilter
      );
  }
}

void BasicComputeSampleApp::initRendering(void)
{
  if (!requireMinAPIVersion(NvGfxAPIVersionGL4_4())) return;
  NvAssetLoaderAddSearchPath("BasicComputeSampleApp");

  m_computeProg = new NvGLSLProgram();
  m_blitProg = NvGLSLProgram::createFromFiles(
    "shaders/plain.vert",
    "shaders/plain.frag"
    );

  int32_t len;
  const int32_t sourceCount = 1;
  NvGLSLProgram::ShaderSourceItem sources[sourceCount];
  sources[0].type = GL_COMPUTE_SHADER;
  sources[0].src = NvAssetLoaderRead("shaders/invert.glsl", len);
  m_computeProg->setSourceFromStrings(sources, sourceCount);
  NvAssetLoaderFree((char*)sources[0].src);

  m_sourceImage = NvImage::CreateFromDDSFile("textures/flower1024.dds");
  m_sourceTexture = NvImage::UploadTexture(m_sourceImage);

  glBindTexture(GL_TEXTURE_2D, m_sourceTexture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, NULL);

  GLint
    type = m_sourceImage->getType(),
    width = m_sourceImage->getWidth(),
    height = m_sourceImage->getHeight(),
    format = m_sourceImage->getFormat(),
    intFormat = m_sourceImage->getInternalFormat();

  glGenTextures(1, &m_resultTexture);
  glBindTexture(GL_TEXTURE_2D, m_resultTexture);
  glTexImage2D(GL_TEXTURE_2D, 0, intFormat, width, height, 0, format, type, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  CHECK_GL_ERROR();
  glBindTexture(GL_TEXTURE_2D, NULL);

}

void BasicComputeSampleApp::drawImage(GLuint texture)
{
  float const vertexPosition[] =
  {
    +m_aspectRatio, -1.0f,
    -m_aspectRatio, -1.0f,
    +m_aspectRatio, +1.0f,
    -m_aspectRatio, +1.0f
  };

  float const textureCoords[] =
  {
    1.0f, 0.0f,
    0.0f, 0.0f,
    1.0f, 1.0f,
    0.0f, 1.0f
  };

  glUseProgram(m_blitProg->getProgram());
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);

  GLint sourceTexLocation = m_blitProg->getUniformLocation("uSourceTex");
  GLint positionAttr = m_blitProg->getAttribLocation("aPosition");
  GLint texCoordsAttr = m_blitProg->getAttribLocation("aTexCoord");

  glUniform1i(sourceTexLocation, 0);
  glVertexAttribPointer(positionAttr, 2, GL_FLOAT, GL_FALSE, 0, vertexPosition);
  glVertexAttribPointer(texCoordsAttr, 2, GL_FLOAT, GL_FALSE, 0, textureCoords);
  glEnableVertexAttribArray(positionAttr);
  glEnableVertexAttribArray(texCoordsAttr);

  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  CHECK_GL_ERROR();
}

void BasicComputeSampleApp::invertColors(
  GLuint inputTex, GLuint outputTex,
  int width, int height
  )
{
  glUseProgram(m_computeProg->getProgram());
  glBindImageTexture(0, inputTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
  glBindImageTexture(1, outputTex, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
  glDispatchCompute(width / m_workgroupSize, height / m_workgroupSize, 1);
  glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

  CHECK_GL_ERROR();
}

void BasicComputeSampleApp::draw(void)
{
  glClearColor(0.2f, 0.0f, 0.2f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  if (m_enableFilter)
  {
    invertColors(
      m_sourceTexture,
      m_resultTexture,
      m_sourceImage->getWidth(),
      m_sourceImage->getHeight()
      );
    drawImage(
      m_resultTexture
      );
  }
  else
  {
    drawImage(
      m_sourceTexture
      );
  }

  // print fps
  /*if (m_framerate->nextFrame())
  {
    NVWindowsLog(
      "fps: %.2f",
      m_framerate->getMeanFramerate()
      );
  }*/
}

void BasicComputeSampleApp::reshape(int32_t width, int32_t height)
{
  glViewport(0, 0, (GLint)width, (GLint)height);
  m_aspectRatio = (float)height / (float)width;
  CHECK_GL_ERROR();
}

bool BasicComputeSampleApp::handleGamepadChanged(uint32_t changedPadFlags)
{
  return false;
}

void BasicComputeSampleApp::configurationCallback(NvEGLConfiguration& config)
{
  config.depthBits = 24;
  config.stencilBits = 0;
  config.apiVer = NvGfxAPIVersionGL4_4();
  //config.apiVer = NvGfxAPIVersionGL4();
}

NvAppBase* NvAppFactory(NvPlatformContext* platform) {
  return new BasicComputeSampleApp(platform);
}
