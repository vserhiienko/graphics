
#ifndef _IRIJ_BASIC_EVENT_INCLUDED_
#define _IRIJ_BASIC_EVENT_INCLUDED_

#include <set>
#include <vector>
#include <FastDelegate.h>


namespace irij
{
	template <typename _SenderTy, typename _ArgsTy, typename _RetTy>
	class BasicEvent
	{
	public:
		typedef FastDelegate2<_SenderTy, _ArgsTy, _RetTy> DelegateTy;

	};
}

#endif _IRIJ_BASIC_EVENT_INCLUDED_
