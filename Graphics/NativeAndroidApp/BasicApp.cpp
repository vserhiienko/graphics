
#include <memory>
#include <FastDelegate.h>
#include <vector>

#include <BasicApp.h>
using namespace irij;

BasicApp::BasicApp(android_app *appContext)
	: appContext(appContext)
	, timerView(&timer)
{
	// Make sure glue isn't stripped.
	app_dummy();

	FastDelegate0<void> del;
	del.bind<BasicApp, BasicApp>(this, &BasicApp::mainLoop);

	std::vector<bool> vb;

	appContext->userData = this;

}

BasicApp::~BasicApp(void)
{

}

void BasicApp::mainLoop()
{

}
