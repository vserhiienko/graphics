
#pragma once

#include <time.h>

namespace irij
{
	class TimerView
	{
	public:
		typedef TimerView const *ConstPtr;

	public:
		float lastMs;
		float initMs;
		float totalMs;
		float elapsedMs;

	};

	class BasicTimer : public TimerView
	{
		float tempMs;

	public:
		inline static double now_ms_d(void)
		{
			struct timespec res;
			clock_gettime(CLOCK_REALTIME, &res);
			return 1000.0 * res.tv_sec + (double) res.tv_nsec / 1e6;
		}
	public:
		inline static float now_ms_f(void)
		{
			struct timespec res;
			clock_gettime(CLOCK_REALTIME, &res);
			return 1000.0f * res.tv_sec + (float) res.tv_nsec / 1e6f;
		}
	public:
		inline static int64_t now_ms_i64(void)
		{
			struct timespec res;
			clock_gettime(CLOCK_REALTIME, &res);
			return 1000 * res.tv_sec + (int64_t)res.tv_nsec / 1e6;
		}
	public:

		inline void resetTimer()
		{
			lastMs = initMs = now_ms_f();
		}
		inline void tick()
		{
			tempMs = now_ms_f();
			elapsedMs = tempMs - lastMs;
			totalMs = tempMs - initMs;
			lastMs = tempMs;
		}

	};

}

