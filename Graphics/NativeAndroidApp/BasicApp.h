#pragma once

#include <jni.h>
#include <errno.h>
#include <EGL/egl.h>
#include <GLES/gl.h>
#include <android/sensor.h>
#include <android/log.h>
#include <native_app_glue/android_native_app_glue.h>

#include <BasicTimer.h>

namespace irij
{
	class BasicApp
	{
		static void handleCommand(android_app* app, int32_t cmd);
		static int32_t handleInput(android_app* app, AInputEvent* e);

		BasicTimer timer;
		android_app *appContext;

		ASensorManager* sensorManager;
		const ASensor* accelerometerSensor;
		ASensorEventQueue* sensorEventQueue;

		int animating;
		EGLDisplay display;
		EGLSurface surface;
		EGLContext context;
		int32_t width;
		int32_t height;

		void onInitializeDisplay();
		void onTerminateDisplay();
		void onFrameMove();

	public:
		BasicTimer::ConstPtr timerView;

	public:
		BasicApp(android_app *appContext);
		~BasicApp(void);

		void mainLoop();

	};
}

