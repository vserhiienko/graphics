#pragma once

#include <dcomp.h>
#include <dwmapi.h>
#include <Windows.h>
#include <Windowsx.h>
#include <safeint.h>

#include <wrl.h>
#include <CiriUI.h>
#include <CiriWindow.h>

namespace ciri
{
    class UIWrap
    {
        uint32_t ciriUIVersion;
        HANDLE ciriUIHandle;
        HINSTANCE ciriUILibraryHandle;
        //ciri_ui_getVersionFuncPtr ciriUIGetVersionFunc;
        ciri_ui_handleMessageFuncPtr ciriUIOnMessageFunc;
        ciri_ui_initializeFuncPtr ciriUIOnInitializeFunc;
        ciri_ui_onFrameMoveFuncPtr ciriUIOnUpdateFunc;
        ciri_ui_releaseFuncPtr ciriUIOnReleaseFunc;

        bool resolveExternalDependencies();

    public:
        HRESULT lastError;

    public:
        HWND uiWindowHandle;
        HWND sceneWindowHandle;
        HINSTANCE instanceHandle;
        ciri::Window::EventArgs windowMessage;

    public:
        ComPtr<IDCompositionDevice> compositionDevice;
        ComPtr<IDCompositionTarget> compositionRenderTarget;
        ComPtr<IDCompositionVisual> compositionRootVisual;

    public:
        ComPtr<IUnknown> compositionUISurfaceTile;
        ComPtr<IDCompositionVisual> compositionUIChildVisual;
        ComPtr<IDCompositionTransform> compositionUITransform;
        ComPtr<IDCompositionTranslateTransform> compositionUISlideTranslationTransform;

    public:
        UIWrap();

    public:
        bool initializeWindow(
            _In_ HWND sceneWindowHandle,
            _In_ HINSTANCE instanceHandle,
            _In_ uint16_t desiredUIWidth = 0
            );

        bool initializeComposition(
            _In_ ComPtr<IDXGIDevice> dxgiDevice,
            _In_ const char *startupUrl = 0
            );
        bool onMessage(
            _In_ ciri::Window::EventArgs const &message
            );
        void onFrameMove(
            _In_ ciri::Window::EventArgs const &message
            );
        void release();

    };

}


