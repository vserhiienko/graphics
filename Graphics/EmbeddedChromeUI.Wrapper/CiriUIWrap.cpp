
#include <d3d11_1.h>
#include <atlbase.h>
#include <windowsx.h>
#include <strsafe.h>

#include <CiriMisc.h>
#include <CiriUIWrap.h>
using namespace ciri;


UIWrap::UIWrap()
    : lastError(S_OK)
{

}

LRESULT CALLBACK ciri_ui_internalWindowProc(
    HWND hWnd,
    UINT message,
    WPARAM wParam,
    LPARAM lParam
    )
{
    if (message == WM_CLOSE)
    {
        DestroyWindow(hWnd);
        return 0;
    }
    else if (message == WM_DESTROY)
    {
        PostQuitMessage(0);
        return 0;
    }
    else
        return DefWindowProc(hWnd, message, wParam, lParam);
}

bool UIWrap::initializeWindow(
    _In_ HWND parentWindow,
    _In_ HINSTANCE instance,
    _In_ uint16_t desiredUIWidth
    )
{
    static const TCHAR *windowClassName
        = TEXT("CIRI_UI_WINDOW_CLASS");

    instanceHandle = instance;
    sceneWindowHandle = parentWindow;

    if (!sceneWindowHandle)
    {
        lastError = E_HANDLE;
        return false;
    }

    RECT uiWindowRect;
    RECT sceneWindowRect;

    GetClientRect(sceneWindowHandle, &sceneWindowRect);

    if (desiredUIWidth == 0)
        desiredUIWidth = sceneWindowRect.right - sceneWindowRect.left,
        desiredUIWidth /= 3;

    uiWindowRect.left = sceneWindowRect.left + (sceneWindowRect.right - sceneWindowRect.left - desiredUIWidth);
    uiWindowRect.right = sceneWindowRect.right;
    uiWindowRect.top = sceneWindowRect.top;
    uiWindowRect.bottom = sceneWindowRect.bottom;

    // create window

    // Register the window class.
    WNDCLASSEXA wcex = { 0 };
    wcex.cbSize = (uint32_t)dx::zeroMemorySz(wcex);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = Window::internalWindowProc;
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
    wcex.lpszClassName = windowClassName;

    auto atom = RegisterClassExA(&wcex);

    //uiWindowHandle = CreateWindowExA(
    //    WS_EX_LAYERED, // Extended window style
    //    windowClassName, // Name of window class
    //    0, // Title-bar string
    //    //"CiriUIComposition", // Title-bar string
    //    WS_CHILD | WS_CLIPSIBLINGS, // Child window
    //    // Window will be resized via MoveWindow
    //    uiWindowRect.left, uiWindowRect.top,
    //    uiWindowRect.right, uiWindowRect.bottom,
    //    sceneWindowHandle, // Parent
    //    NULL, // Class menu
    //    GetModuleHandle(0), // Handle to application instance
    //    NULL // Window-creation data
    //    );
    //if (!uiWindowHandle)
    //{
    //    auto lastWin32Error = (GetLastError());
    //    lastError = HRESULT_FROM_WIN32(GetLastError());
    //    return false;
    //}

    //SetLayeredWindowAttributes(uiWindowHandle, 0, 130, LWA_ALPHA);
    ShowWindow(uiWindowHandle, SW_SHOWDEFAULT);

    return true;
}

bool UIWrap::initializeComposition(
    _In_ ComPtr<IDXGIDevice> dxgiDevice,
    _In_ const char *startupUrl
    )
{
    if (!resolveExternalDependencies())
    {
        lastError = E_ABORT;
        return false;
    }

    if (!dxgiDevice.Get()) 
    {
        lastError = E_POINTER;
        return false;
    }

    // create window compositions

    lastError = reportComErrorAndGet("DCompositionCreateDevice: ",
        DCompositionCreateDevice(dxgiDevice.Get(), IID_PPV_ARGS(&compositionDevice)
        ));
    if (FAILED(lastError)) 
        return false;

    lastError = reportComErrorAndGet("CreateTargetForHwnd: ",
        compositionDevice->CreateTargetForHwnd(sceneWindowHandle, true, &compositionRenderTarget)
        );
    if (FAILED(lastError))
        return false;

    lastError = reportComErrorAndGet("CreateVisual: ",
        compositionDevice->CreateVisual(&compositionRootVisual)
        );
    if (FAILED(lastError))
        return false;

    lastError = reportComErrorAndGet("CreateVisual: ",
        compositionDevice->CreateVisual(&compositionUIChildVisual)
        );
    if (FAILED(lastError))
        return false;

    lastError = reportComErrorAndGet("CreateSurfaceFromHwnd: ",
        compositionDevice->CreateSurfaceFromHwnd(uiWindowHandle, &compositionUISurfaceTile)
        );
    if (FAILED(lastError))
        return false;

    lastError = reportComErrorAndGet("SetContent: ",
        compositionUIChildVisual->SetContent(compositionUISurfaceTile.Get())
        );
    if (FAILED(lastError))
        return false;

    lastError = reportComErrorAndGet("AddVisual: ",
        compositionRootVisual->AddVisual(compositionUIChildVisual.Get(), true, nullptr)
        );
    if (FAILED(lastError))
        return false;

    //ciriUIVersion = (*ciriUIGetVersionFunc)();
    ciriUIHandle = (*ciriUIOnInitializeFunc)(
        uiWindowHandle, instanceHandle, 
        startupUrl ? startupUrl : "http://tympanus.net/Development/DragDropInteractions/"
        );

    return true;
}
bool UIWrap::resolveExternalDependencies()
{
    ciriUILibraryHandle = LoadLibraryA("EmbeddedChromeUI.dll");
    if (!ciriUILibraryHandle) return false;

    //ciriUIGetVersionFunc = (ciri_ui_getVersionFuncPtr)GetProcAddress(ciriUILibraryHandle, ciri_ui_getVersionFuncName); if (!ciriUIGetVersionFunc) return false;
    ciriUIOnInitializeFunc = (ciri_ui_initializeFuncPtr)GetProcAddress(ciriUILibraryHandle, ciri_ui_initializeFuncName); if (!ciriUIOnInitializeFunc) return false;
    ciriUIOnMessageFunc = (ciri_ui_handleMessageFuncPtr)GetProcAddress(ciriUILibraryHandle, ciri_ui_handleMessageFuncName); if (!ciriUIOnMessageFunc) return false;
    ciriUIOnUpdateFunc = (ciri_ui_onFrameMoveFuncPtr)GetProcAddress(ciriUILibraryHandle, ciri_ui_onFrameMoveFuncName); if (!ciriUIOnUpdateFunc) return false;
    ciriUIOnReleaseFunc = (ciri_ui_releaseFuncPtr)GetProcAddress(ciriUILibraryHandle, ciri_ui_releaseFuncName); if (!ciriUIOnReleaseFunc) return false;

    return true;
}

bool UIWrap::onMessage(ciri::Window::EventArgs const &message)
{
    return ciriUIOnMessageFunc(ciriUIHandle, (HANDLE)&message);
}

void UIWrap::onFrameMove(ciri::Window::EventArgs const &message)
{
    ciriUIOnUpdateFunc(ciriUIHandle, (HANDLE)&message);
}

void UIWrap::release()
{
    ciriUIOnReleaseFunc(ciriUIHandle);
}
