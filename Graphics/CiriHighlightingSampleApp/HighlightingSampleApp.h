
#include <CiriWindow.h>
#include <CiriUIWrap.h>
#include <CiriDeviceResources.h>

namespace ciri
{
    class SampleApp
    {
        //UIWrap ui;
        Window window;
        DeviceResources d3d;
        HWND uiWindowHandle;
        HWND sceneWindowHandle;


        // Main window 
        static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
        {
            dx::trace("WindowProc: msg 0x%0x", uMsg);
            LRESULT result = 0;

            switch (uMsg)
            {
            default:
                result = DefWindowProc(hwnd, uMsg, wParam, lParam);
            }

            return result;
        }

    public:
        uint64_t completedFenceU;

    public:
        int launch();
    };
}
