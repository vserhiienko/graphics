#include <CiriConstantBuffer.h>
using namespace ciri;

ConstantBufferBase::ConstantBufferBase(uint32_t bufferSize)
    : bufferSize(bufferSize)
    , uploadHeapPointer(0)
{
}

void ConstantBufferBase::create(
    DeviceResources &deviceResources,
    D3D12_CPU_DESCRIPTOR_HANDLE cpuDescriptorHandleDH,    // descriptor heap
    D3D12_GPU_VIRTUAL_ADDRESS gpuVirtualAddressUH         // upload heap
    )
{
    D3D12_CONSTANT_BUFFER_VIEW_DESC constantBufferViewDesc = {};
    constantBufferViewDesc.SizeInBytes = bufferSize;
    constantBufferViewDesc.BufferLocation = gpuVirtualAddressUH;
    deviceResources.device->CreateConstantBufferView(&constantBufferViewDesc, cpuDescriptorHandleDH);
}

void ConstantBufferBase::setAndAdvanceUploadHeapPointer(_In_ uint8_t *currentPosition, _Out_ uint8_t **advancePosition)
{
    uploadHeapPointer = currentPosition;
    if (advancePosition) *advancePosition = currentPosition + bufferSize;
}

ConstantBufferUploadHeap::ConstantBufferUploadHeap()
    : uploadHeapSize(0)
    , uploadHeapPointer(0)
{
}

void ConstantBufferUploadHeap::create(DeviceResources &deviceResources)
{
    // round up the heap size
    uploadHeapSize = dx::roundUp(uploadHeapSize, uploadHeapMultiple);

    reportComError("CreateCommittedResource: ",
        deviceResources.device->CreateCommittedResource(
        &CD3D12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
        D3D12_HEAP_MISC_NONE,
        &CD3D12_RESOURCE_DESC::Buffer(uploadHeapSize),
        D3D12_RESOURCE_USAGE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(&uploadHeap)
        ));
    if (uploadHeap.Get())
        uploadHeap->Map(
        0, nullptr,
        reinterpret_cast<void**>(&uploadHeapPointer)
        );
}

void ConstantBufferUploadHeap::createConstantBuffers(
    DeviceResources &deviceResources,
    D3D12_CPU_DESCRIPTOR_HANDLE startCPUDescriptorHandleDH,    // descriptor heap
    D3D12_GPU_VIRTUAL_ADDRESS startGPUVirtualAddressUH,        // upload heap
    ConstantBufferBase *constantBuffers,
    uint32_t constantBufferCount
    )
{
    uint8_t *storagePointer = uploadHeapPointer;
    ConstantBufferBase *constantBuffer = constantBuffers;
    D3D12_GPU_VIRTUAL_ADDRESS gpuVirtualAddress = startGPUVirtualAddressUH;
    D3D12_CPU_DESCRIPTOR_HANDLE cpuDescriptorHandle = startCPUDescriptorHandleDH;
    for (uint32_t ind = 0; ind < constantBufferCount; ++ind)
    {
        constantBuffer->create(deviceResources, cpuDescriptorHandle, gpuVirtualAddress);
        constantBuffer->setAndAdvanceUploadHeapPointer(storagePointer, &storagePointer);
        cpuDescriptorHandle.Offset(deviceResources.descriptorOffsetCBV);
        gpuVirtualAddress += constantBuffer->bufferSize;
    }
}
