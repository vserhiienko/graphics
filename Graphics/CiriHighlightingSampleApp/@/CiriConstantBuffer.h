#pragma once
#include <CiriDeviceR.h>
#include <CiriMisc.h>

namespace ciri
{
    class ConstantBufferBase
    {
    public:
        typedef std::vector<ConstantBufferBase*> Vector;

    public:
        uint32_t bufferSize;
        uint8_t *uploadHeapPointer;

    public:
        ConstantBufferBase(
            uint32_t bufferSize
            );

    public:
        void create(
            DeviceResources &deviceResources,
            D3D12_CPU_DESCRIPTOR_HANDLE cpuDescriptorHandleDH,    // descriptor heap
            D3D12_GPU_VIRTUAL_ADDRESS gpuVirtualAddressUH         // upload heap
            );
        void setAndAdvanceUploadHeapPointer(
            _In_ uint8_t *currentPosition, 
            _Out_ uint8_t **advancePosition
            );
    };

    template <typename _Ty> 
    struct ConstantBuffer 
        : public ConstantBufferBase
    {
    public:
        _Ty *bufferSource;

    public:
        ConstantBuffer()
            : ConstantBufferBase(sizeof _Ty)
            , bufferSource(0)
        {
        }

    public:
        void setUploadHeapPointer()
        {
            bufferSource = reinterpret_cast<_Ty*>(uploadHeapPointer);
        }
    };

    struct ConstantBufferUploadHeap
    {
    public:
        static const uint32_t uploadHeapMultiple = 256;

    public:
        uint32_t uploadHeapSize;
        uint8_t *uploadHeapPointer;
        ComPtr<ID3D12Resource> uploadHeap;

    public:
        ConstantBufferUploadHeap();

    public:
        void create(
            DeviceResources &deviceResources
            );
        void createConstantBuffers(
            DeviceResources &deviceResources,
            D3D12_CPU_DESCRIPTOR_HANDLE startCPUDescriptorHandleDH,    // descriptor heap
            D3D12_GPU_VIRTUAL_ADDRESS startGPUVirtualAddressUH,        // upload heap
            ConstantBufferBase *constantBuffers,
            uint32_t constantBufferCount
            );

    public:
        template <typename _Ty>
        void advanceUploadHeapSizeForType(uint32_t itemOfTypeCount = 1)
        {
            uploadHeapSize += sizeof(_Ty) * itemOfTypeCount;
        }
    };

}

