#pragma once
#include <CiriDeviceR.h>
#include <CiriConstantBuffer.h>

namespace ciri
{
    struct BillboardDescriptor
    {
    public:
        bool upY;
        float lenX, lenY;
        unsigned resX, resY;

    public:
        static void makeLargeBillboard(BillboardDescriptor &);
    };


    struct BillboardResources
    {
    public:
        struct PerFrameData
        {
            DirectX::XMFLOAT4X4 pose;
            DirectX::XMFLOAT4 color;
        };

    public:
        size_t indexCount;
        ComPtr<ID3D12Resource> indexBuffer;
        ComPtr<ID3D12Resource> vertexBuffer;
        ConstantBuffer<PerFrameData> perFrameBuffer;

    public:
        void pushConstantBuffers(
            _Inout_ ConstantBufferBase::Vector &collection
            );
        void advanceConstantBufferUploadHeap(
            _Inout_ ConstantBufferUploadHeap &contantBufferUploadHeap
            );
        void updateConstanBufferSources(
            );

    public:
        void createFor(
            _In_ BillboardDescriptor const &descriptor, 
            _In_ ConstantBufferUploadHeap &contantBufferUploadHeap
            );

    };

}
