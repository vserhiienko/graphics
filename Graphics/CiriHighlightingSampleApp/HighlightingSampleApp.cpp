
#include <AegaScene.h>
#include <HighlightingSampleApp.h>
#include <FileSystem.h>

#include <CiriUI.h>

using namespace ciri;

int SampleApp::launch()
{
    /*auto fileFileResult = FileSystem::findFileRecursively("D:\\Dev\\Ma", "blackpanther_v1.ma.aega", [&](std::string const &fullPath, std::string const &folder, std::string const &file)
    {
        dx::trace("findFileRecursively: found %s @ %s", file.c_str(), folder.c_str());
        if (file == "blackpanther_v1.ma.aega") return true;
        return false;
    });

    auto fileFolderResult = FileSystem::findFolderRecursively("D:\\Dev\\Ma", "blackpanther_project", [&](std::string const &fullPath, std::string const &folder)
    {
        dx::trace("findFolderRecursively: found %s @ %s", folder.c_str(), fullPath.c_str());
        if (folder == "blackpanther_project")
            return true;
        return false;
    });*/


    /*aega::Scene scene;
    aega::SceneDeserialize deserialize;
    deserialize.deserializeTo(&scene, "D:/Dev/Ma/simpleScene01.ma.aega");*/

    HRESULT resultH;

    HINSTANCE instanceHandle = GetModuleHandle(0);
    auto backbufferColor = DirectX::Colors::CornflowerBlue;

    window.initialize(10, 10, 1600, 896, "Ciri");

    //initializeWindows();

    //ui.initializeWindow(
    //    sceneWindowHandle,
    //    //window.windowHandle,
    //    instanceHandle
    //    );

    d3d.initializeFor(sceneWindowHandle, false);

   /* ComPtr<IDXGIDevice> dxgiDevice;
    if (SUCCEEDED(d3d.device.As(&dxgiDevice)))
        ui.initializeComposition(dxgiDevice);
    else return -1;*/
    d3d.finalizeResourceSetup();

    Window::EventArgs args;
    args.windowMessage() = dx::Message::Create;

    completedFenceU = d3d.fenceU;
    window.mainLoop([&](Window::EventArgs &args)
    {
        //ui.onMessage(args);

        switch (args.windowMessage())
        {
        case dx::Message::KeyReleased:
            args.translateAs<dx::Message::KeyReleased>();
            if (args.keyCode() == dx::VirtualKey::Escape)
                dx::trace("launch: closing..."),
                window.sendCloseMessage();
            return true;
        default:
            return false;
        }
    }, [&](void)
    {
        completedFenceU = d3d.fence->GetCompletedValue();
        if (completedFenceU == d3d.fenceU)
        {
            resultH = d3d.defaultCA->Reset();
            resultH = d3d.setupCL->Reset(d3d.defaultCA.Get(), nullptr);

            d3d.renderTarget.beginFrameRenderTargetBarriers(d3d.setupCL.Get());
            d3d.renderTarget.clearRenderTarget(d3d.setupCL.Get(), reinterpret_cast<float*>(&backbufferColor));
            d3d.renderTarget.clearDepthStencil(d3d.setupCL.Get(), 1.0f, 0);
            d3d.renderTarget.endFrameRenderTargetBarriers(d3d.setupCL.Get());

            d3d.setupCL->Close();
            ID3D12CommandList* commandLists[] = { d3d.setupCL.Get() };
            d3d.defaultCQ->ExecuteCommandLists(1, commandLists);
            d3d.swapchain->Present(1, 0);

            ++d3d.fenceU;
            resultH = d3d.defaultCQ->Signal(d3d.fence.Get(), d3d.fenceU);

            //ui.onFrameMove(args);
        }

    }, [&](void)
    {
        dx::trace("launch: cleanup...");

        //ui.release();

        // cleanup

        /*
        HANDLE fenceCompletionEvent = 0;
        completedFenceU = d3d.fence->GetCompletedValue();
        d3d.fence->SetEventOnCompletion(d3d.fenceU, fenceCompletionEvent);
        WaitForSingleObject(fenceCompletionEvent, INFINITE);
        */
    });

    return 0;
}

static int runSample() { SampleApp app; return app.launch(); }
int main(int, char**) { return runSample(); }
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) { return runSample(); }
