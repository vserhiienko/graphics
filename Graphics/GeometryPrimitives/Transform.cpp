
#include <algorithm>
#include <Transform.h>

#define __sqrt3 1.7320508075688772935274463415059f
#define __1over3 0.33333333333333333333333333333333f
#define __1over4 0.25000000000000000000000000000000f
#define __1over2 0.50000000000000000000000000000000f
#define __sqrt3times1over2 __sqrt3 * __1over2

soko::TransformSolver::Result soko::TransformSolver::solveSegment(
  DirectX::XMFLOAT2 p1, DirectX::XMFLOAT2 m1, // left
  DirectX::XMFLOAT2 p2, DirectX::XMFLOAT2 m2, // right
  float x, _Out_ float &y
  )
{

  Result result;
  float a, b, c, d, xx, yy, zz, t;

  a = 2.0f * p1.x + m1.x - 2.0f * p2.x + m2.x;
  b = -3.0f * p1.x - 2.0f * m1.x + 3.0f * p2.x - m2.x;
  c = m1.x;
  d = p1.x - x;

  result = solve3(a, b, c, d, xx, yy, zz);
  if (result > 0 && findHermiteTime(xx, yy, zz, t))
  {
    y = solveHermiteSegment(
      p1.y, m1.y, // left
      p2.y, m2.y, // right
      t
      );

    return Result::k1Real;
  }

  return Result::kNoReal;
}

soko::TransformSolver::Result soko::TransformSolver::solveSegmentEst(
  DirectX::XMFLOAT2 p1, DirectX::XMFLOAT2 m1, // left
  DirectX::XMFLOAT2 p2, DirectX::XMFLOAT2 m2, // right
  float x, _Out_ float &y
  )
{
  float const q = p2.x - p1.x;
  float const r = x - p1.x;
  float const t = r / q;

  y = solveHermiteSegment(
    p1.y, m1.y, // left
    p2.y, m2.y, // right
    t
    );

  return Result::k1Real;
}

float soko::TransformSolver::solveHermiteSegment(
  float p0, float m0, // left
  float p1, float m1, // right
  float t
  )
{
  float const t2 = t * t;
  float const t3 = t2 * t;
  float const h1 = 2 * t3 - 3 * t2 + 1;
  float const h2 = t3 - 2 * t2 + t;
  float const h3 = -2 * t3 + 3 * t2;
  float const h4 = t3 - t2;

  return h1 * p0 + h2 * m0 + h3 * p1 + h4 * m1;
}

soko::TransformSolver::Result soko::TransformSolver::solve2(
  float a, float b, float c,
  _Out_ float &x, _Out_ float &y
  )
{
  float d = b * b + 4 * a * c;

  if (d < 0.0f)
  {
    return Result::kNoReal;
  }
  else if (d == 0.0f)
  {
    d = sqrtf(d);
    x = y = -b * __1over2 / a;
    return Result::k2RealEq;
  }
  else
  {
    d = sqrtf(d);
    x = (-b + d) * 0.5f / a;
    y = (-b - d) * 0.5f / a;
    return Result::k2Real;
  }
}

soko::TransformSolver::Result soko::TransformSolver::solve3(
  float a, float b, float c, float d,
  _Out_ float &x, _Out_ float &y, _Out_ float &z
  )
{
  Result result;

  if (a == 0.0f)
  {
    result = solve2(b, c, d, x, y);
    return result;
  }

  float const inv3 = __1over3;
  float const inva = 1.0f / a;
  float const inva2 = inva / a;
  float const inva3 = inva2 / a;
  float const b2 = b * b;
  float const b3 = b2 * b;

  float const f = c / a - b2 * inva2 * inv3;
  float g = 2.0f * b3 * inva3 - 9.0f * b * c * inva2 + 27.0f * d * inva;
  g /= 27.0f;

  float const h = g * g * __1over4 + f * f * f / 27.0f;

  if (f == 0.0f && g == 0.0f && h == 0.0f) result = Result::k3RealEq; else
    if (h <= 0) result = Result::k3Real; else
      if (h > 0) result = Result::k1Real;

  float i, j, k, L, M, N, P, R, S, T, U;

  i = g * g * 0.2500f - h;
  i = getPower(i, 0.5000f);

  switch (result)
  {

  case Result::k3Real:
    j = getPower(i, inv3);
    k = ::acosf(-g * __1over4 / i);
    L = -j;
    M = ::cos(k * inv3);
    N = __sqrt3 * ::sin(k * inv3);
    P = b * -inv3 / a;
    x = 2 * j * ::cos(k * inv3) - (b * inv3 / a);
    y = L * (M + N) + P;
    z = L * (M - N) + P;
    break;

  case Result::k3RealEq:
    x = y = z = -getPower(d / a, inv3);
    break;

  case Result::k1Real:
    R = g * -__1over2 + ::sqrtf(h); S = getPower(R, inv3);
    T = g * -__1over2 - ::sqrtf(h); U = getPower(T, inv3);
    x = S + U - b * inv3 / a; if (!isnan(x)) { y = z = x; break; }
    y = -__1over2 * (S + U) - b * inv3 / a + i * (S - U) * __sqrt3times1over2; if (!::isnan(y)) { x = z = y; break; }
    z = -__1over2 * (S + U) - b * inv3 / a - i * (S - U) * __sqrt3times1over2; if (!::isnan(z)) { y = x = z; break; }
    break;

  }

  return result;
}

float soko::TransformSolver::getPower(
  float x, float p
  )
{
  return x < 0.0f ? -powf(-x, p) : powf(x, p);
}

float soko::TransformSolver::getMax(
  float x, float y
  )
{
  return std::max(x, y);
}

float soko::TransformSolver::getMax(
  float x, float y, float z
  )
{
  return std::max(std::max(x, y), std::max(y, z));
}

bool soko::TransformSolver::findHermiteTime(
  float x, float y, float z,
  _Out_ float &t
  )
{
  if (x <= 1.0f && x >= 0.0f) { t = x; return true; }
  if (y <= 1.0f && y >= 0.0f) { t = y; return true; }
  if (z <= 1.0f && z >= 0.0f) { t = z; return true; }

  return false;
}

soko::Transform::Curve::Key::Key()
  : previousKey(nullptr)
  , nextKey(nullptr)
  , curve(nullptr)
{
}

soko::Transform::Curve::Array::Array() 
  : sx(nullptr), sy(nullptr), sz(nullptr)
  , rx(nullptr), ry(nullptr), rz(nullptr)
  , tx(nullptr), ty(nullptr), tz(nullptr)
{
}

soko::Transform::Curve::Curve()
  : rightKey(nullptr)
  , leftKey(nullptr)
{
}



soko::Transform::Transform()
{
  DirectX::XMStoreFloat4(&translation, DirectX::g_XMIdentityR3.v);
  DirectX::XMStoreFloat4(&orientation, DirectX::g_XMIdentityR3.v);
  DirectX::XMStoreFloat4(&rotation, DirectX::g_XMIdentityR3.v);
}

float soko::Transform::Curve::resolveAt(float t, bool rec)
{
  if (rightKey)
  {
    if (rightKey->timeValue.x >= t)
    {
      if (leftKey)
      {
        if (leftKey->timeValue.x <= t)
        {
          float value;
          TransformSolver::Result result;

          result = TransformSolver::solveSegmentEst(
            leftKey->timeValue, leftKey->outputTangent,
            rightKey->timeValue, rightKey->inputTangent,
            t, value
            );

          return value;
        }
        else
        {
          rightKey = leftKey;
          leftKey = leftKey->previousKey;
          return resolveAt(t, true);
        }
      }
      else
      {
        return rightKey->timeValue.y;
      }
    }
    else
    {
      leftKey = rightKey;
      rightKey = rightKey->nextKey;
      return resolveAt(t, true);
    }
  }
  else
  {
    return leftKey->timeValue.y;
  }
}

void soko::Transform::resolveSS_animated(float time)
{
  if (channelArray.sx) scale.x = channelArray.sx->resolveAt(time);
  if (channelArray.sy) scale.y = channelArray.sy->resolveAt(time);
  if (channelArray.sz) scale.z = channelArray.sz->resolveAt(time);
}

void soko::Transform::resolveSR_animated(float time)
{
  if (channelArray.rx || channelArray.ry || channelArray.rz)
  {
    rotationEA.x = channelArray.rx ? channelArray.rx->resolveAt(time) : rotationEA.x;
    rotationEA.y = channelArray.ry ? channelArray.ry->resolveAt(time) : rotationEA.y;
    rotationEA.z = channelArray.rz ? channelArray.rz->resolveAt(time) : rotationEA.z;
    DirectX::XMStoreFloat4(&rotation, DirectX::XMQuaternionRotationRollPitchYaw(
      rotationEA.x, rotationEA.y, rotationEA.z
      ));
  }
}

void soko::Transform::resolveST_animated(float time)
{
  if (channelArray.tx) translation.x = channelArray.tx->resolveAt(time);
  if (channelArray.ty) translation.y = channelArray.ty->resolveAt(time);
  if (channelArray.tz) translation.z = channelArray.tz->resolveAt(time);
}




