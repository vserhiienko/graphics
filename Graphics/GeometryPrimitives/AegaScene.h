#pragma once

#include <vector>
#include <map>
#include <DirectXMath.h>
#include <VertexTypes.h>

namespace aega
{
  struct Node
  {
    uint32_t api;
  };

  struct DepNode : public Node
  {
    std::string name;
  };

  struct DagNode : public Node
  {
    std::string path;
  };

  struct FamilyNode : public DagNode
  {
    std::vector<std::string> parentPaths;
    std::vector<std::string> childPaths;
  };

  struct MeshNode : public FamilyNode
  {
    struct UVSet
    {
      std::string name;
      std::vector<DirectX::XMFLOAT2> uvs;
    };

    struct Triangle
    {
      struct UVSet
      {
        uint32_t uvs[3];
      };

      uint32_t points[3];
      uint32_t normals[3];
      UVSet uvSets[1];
      //std::vector<UVSet> uvSets;

      DirectX::XMFLOAT4 colours[3];
    };

    struct Polygon
    {
      std::vector<Triangle> triangles;
    };

    std::vector<DirectX::XMFLOAT3> points;
    std::vector<DirectX::XMFLOAT3> normals;
    std::vector<DirectX::XMFLOAT4> colors;
    std::vector<DirectX::XMFLOAT4> faceColors;
    std::vector<UVSet*> uvSets;
    std::vector<Polygon> polygons;

  };

  struct AnimatedPlug
  {
    std::string name;
    std::vector<std::string> curves;
  };

  struct LambertNode : public DepNode
  {
    DirectX::XMFLOAT4 color;
    DirectX::XMFLOAT4 ambientColor;
    DirectX::XMFLOAT4 incandescence;
    DirectX::XMFLOAT4 transparency;
    float diffuseCoeff;
    float glowIntensity;
    float translucenceCoeff;

    inline void *getPtr()
    {
      return &color;
    }

    inline size_t getSize()
    {
      return sizeof(DirectX::XMFLOAT4)
        + sizeof(DirectX::XMFLOAT4)
        + sizeof(DirectX::XMFLOAT4)
        + sizeof(DirectX::XMFLOAT4)
        + sizeof(float)
        + sizeof(float)
        + sizeof(float);
    }
  };

  struct ReflectShaderNode : public LambertNode
  {
    DirectX::XMFLOAT4 specularColor;
    DirectX::XMFLOAT4 reflectedColor;
    float reflectivity;

    inline void *getPtr()
    {
      return LambertNode::getPtr();
    }

    inline size_t getSize()
    {
      return sizeof(DirectX::XMFLOAT4)
        + sizeof(DirectX::XMFLOAT4)
        + sizeof(float)
        + LambertNode::getSize();
    }
  };

  struct PhongShaderNode : public ReflectShaderNode
  {
    float cosPower;

    inline void *getPtr()
    {
      return LambertNode::getPtr();
    }

    inline size_t getSize()
    {
      return sizeof(float)
        + ReflectShaderNode::getSize();
    }
  };

  struct BlinnShaderNode : public ReflectShaderNode
  {
    float eccentricity;
    float specularRollOff;

    inline void *getPtr()
    {
      return LambertNode::getPtr();
    }

    inline size_t getSize()
    {
      return sizeof(float)
        + sizeof(float)
        + ReflectShaderNode::getSize();
    }
  };

  struct AnimationCurveNode : public DepNode
  {
    struct Key
    {
      float time, value;
      uint32_t inTy;
      float ix, iy;
      uint32_t outTy;
      float ox, oy;
    };

    std::vector<Key> keys;
  };

  struct TransformNode : public FamilyNode
  {
    bool joint;
    bool animated;
    DirectX::XMFLOAT3 translationTS;
    DirectX::XMFLOAT3 scale;
    DirectX::XMFLOAT4 rotationTS;
    DirectX::XMFLOAT4 orientation;
    std::vector<AnimatedPlug*> plugs;
  };

  struct SkinComponent
  {
    uint32_t index;
    std::vector<float> weights;
  };

  struct Skin
  {
    std::string path;
    std::vector<SkinComponent*> components;
  };

  struct SkinClusterNode : public DepNode
  {
    std::vector<std::string> influences;
    std::vector<Skin*> skins;
  };

  struct IkHandleNode : public DepNode
  {
    std::string handle;
    std::string solver;
    uint32_t stickness;
    float weight;
    float poWeight;
    std::string start;
    std::string effector;
    DirectX::XMFLOAT3 pole;
  };

  class Scene
  {
  public:

    std::vector<MeshNode*> meshes;
    std::vector<TransformNode*> transforms;
    std::vector<AnimationCurveNode*> animCurves;

    std::vector<LambertNode*> lambert;
    std::vector<PhongShaderNode*> phongs;
    std::vector<BlinnShaderNode*> blinns;

    std::vector<SkinClusterNode*> clusters;
    std::vector<IkHandleNode*> iks;

    std::multimap<std::string, std::string> transformCurves;

    void clearAll();

  };

  class SceneDeserialize
  {
    struct Context;
    Context *impl;

  public:
    static const char sign[4];
    static const uint32_t version;

  public:
    SceneDeserialize();
    ~SceneDeserialize();
    void deserializeTo(Scene *scene, std::string const &aegaFile);

  private:
    uint32_t nextNodeApi();
    void deserializeTo(DagNode *dag);
    void deserializeTo(DepNode *dep);
    void deserializeTo(FamilyNode *family);
    void deserializeTo(MeshNode *mesh);
    void deserializeTo(TransformNode *transform);
    void deserializeTo(AnimationCurveNode *animCurve);
    void deserializeTo(SkinClusterNode *cluster);
    void deserializeTo(IkHandleNode *ik);

  };

  class SceneAdapter
  {
  public:
    static void adaptToCollections(
      MeshNode const &node,
      noekk::VertexCollection &vertices,
      noekk::IndexCollection &indices
      );
  };

}