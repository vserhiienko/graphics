#pragma once

#include <Mesh.h>
#include <Transform.h>

namespace soko
{
  class MayaReader;
  class GeometryLoader
  {
    MayaReader *impl;
  public:

    struct Node;
    typedef struct Item
    {
      friend Node;
      friend MayaReader;

      enum Type
      {
        kNode,
        kSkin,
        kShape,
        kTransform,
        kSkinCluster,
        kAnimationCurve,
        kInvalid,
      };

      std::string &getNameByRef(void);
      std::string  getNameCopy(void);
      std::string *GetName(void);

      Item::Type GetType(void);

      Item(void);
      virtual ~Item(void);

    protected:

      std::string m_name;
      Type m_type;

    } *Handle;

    struct Node : public Item
    {
      friend Item;
      friend MayaReader;

      std::vector<Handle> &getHandlesByRef(void);
      std::vector<Node*> &getParentsByRef(void);
      std::vector<Node*> &getKidsByRef(void);
      std::vector<Handle> *getHandles(void);
      std::vector<Node*> *getParents(void);
      std::vector<Node*> *getKids(void);

      Node *getParentNode(std::string name, _In_opt_ bool returnNew = false);
      Node *getKidNode(std::string name, _In_opt_ bool returnNew = false);
      Handle getHandle(std::string name, Item::Type type);
      Handle getHandle(std::string name);
      Handle getHandle(uint32_t index);

      Node(void);
      virtual ~Node(void);

    protected:

      std::vector<Handle> m_handles;
      std::vector<Node*> m_adults;
      std::vector<Node*> m_kids;
    };

    struct Shape : public Item
    {
      struct Polygon
      {
        struct Vertex
        {
          struct Tex
          {
            std::string uvSet;
            uint32_t uvIndex;

            Tex(void);
          };

          Tex *texCoords;
          uint32_t pointIndex;
          uint32_t normalIndex;
          uint32_t texCoordCount;
          uint32_t binormalIndex;
          uint32_t tangentIndex;

          Vertex(void);
          virtual ~Vertex(void);
        };

        Vertex *vertices;
        uint32_t vertexCount;

        Polygon(void);
        virtual ~Polygon(void);
      };

      struct UVSet
      {
        typedef std::vector<UVSet*> Vector;

        std::string name;
        std::vector<DirectX::XMFLOAT2> uvs;
        std::vector<DirectX::XMFLOAT3> binormals;
        std::vector<DirectX::XMFLOAT3> tangents;

        UVSet(void);
        virtual ~UVSet(void);
      };

      struct VectorsInSpace
      {
        uint32_t count;
        std::string space;
        DirectX::XMFLOAT3 *vectors;

        VectorsInSpace(void);
        virtual ~VectorsInSpace(void);
      };

      typedef std::vector<VectorsInSpace*> VectorsInSpaceCollection;

      UVSet::Vector sets;
      std::vector<Polygon*> polygons;
      VectorsInSpaceCollection *points;
      VectorsInSpaceCollection *normals;

      Shape(void);
      virtual ~Shape(void);
    };

    struct Transform : public Item
    {
      struct Curve : public Item
      {
        typedef std::vector<Curve*> Vector;

        struct Key
        {
          float t;
          float value;
          DirectX::XMFLOAT2 in;
          DirectX::XMFLOAT2 out;
          uint32_t tangentIn;
          uint32_t tangentOut;

          Key(void);
        };

        Key *keys;
        uint32_t type;
        uint32_t keyCount;

        Curve(void);
        virtual ~Curve(void);

      };

      DirectX::XMFLOAT3 scale;
      DirectX::XMFLOAT3 translate;
      DirectX::XMFLOAT3 translateTS;
      DirectX::XMFLOAT3 translateWS;
      DirectX::XMFLOAT3 rotateEA;
      DirectX::XMFLOAT4 rotateQ;
      DirectX::XMFLOAT4 rotateQTS;
      DirectX::XMFLOAT4 rotateQWS;
      DirectX::XMFLOAT4 rotateO;
      DirectX::XMFLOAT4 rotateOOS;
      DirectX::XMFLOAT4 rotateOTS;
      DirectX::XMFLOAT4 rotateOWS;

      bool isJoint;
      Curve::Vector *curves;

      Transform(void);
      virtual ~Transform(void);
    };

    struct SkinCluster : public Item
    {
      struct Skin : public Item
      {
        typedef std::vector<Skin*> Vector;

        struct Component
        {
          struct Influence
          {
            float weight;
            std::string fullPath;
            std::string partialPath;

            Influence(void);
          };

          Influence *influences;
          uint32_t influenceCount;

          Component(void);
          virtual ~Component(void);
        };

        Component *components;
        uint32_t componentCount;

        Skin(void);
        virtual ~Skin(void);

      };

      Skin::Vector Geometries;

      SkinCluster(void);
      virtual ~SkinCluster(void);

    };

    GeometryLoader(void);
    ~GeometryLoader(void);

    GeometryLoader::Node *getRootNode(void);
    bool parseContent(std::string const &);
    bool readFromFileAndParseContent(std::string const &);
    void clear(void);

    static void asMesh(
      GeometryLoader::Shape *shape, 
      _Out_ Mesh *mesh
      );
    static void asTransform(
      GeometryLoader::Transform *mayaTransform,
      _Out_ soko::Transform *objectTransform,
      _Out_ soko::Transform *worldTransform
      );
  };
}


