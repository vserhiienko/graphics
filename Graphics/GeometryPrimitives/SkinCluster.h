#pragma once

#include <Mesh.h>
#include <Skeleton.h>

namespace soko
{
  struct Skin
  {

    struct Cluster
    {
      struct Influence
      {
        typedef std::vector<Influence*> Vector;
        typedef Vector::iterator Iterator;

        float weight;
        soko::SkeletonJoint *joint;

        Influence(void);
      };

      struct Component
      {
        typedef std::vector<Component*> Vector;
        typedef Vector::iterator Iterator;

        Influence::Vector influences;

        Component(void);
        ~Component(void);
      };

      Component::Vector components;

      Cluster(void);
      ~Cluster(void);
    };

    std::vector<DirectX::XMFLOAT4> points;
    std::vector<DirectX::XMFLOAT3> normals;

    Mesh *mesh;
    Cluster *cluster;
    Skeleton *posedSkeleton;
    Skeleton *initialSkeleton;

    void pose(float time);

    Skin(void);
    ~Skin(void);
  };



}



