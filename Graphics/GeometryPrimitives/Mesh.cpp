#include <Mesh.h>

soko::Mesh::VertexIndices::VertexIndices() 
{
}

soko::Mesh::VertexIndices::VertexIndices(uint32_t value) 
  : p(value), n(value), t(value) 
{
}

soko::Mesh::VertexIndices::VertexIndices(uint32_t p, uint32_t n, uint32_t t) 
  : p(p), n(n), t(t)
{
}

soko::Mesh::Face::Face() 
{ 
}

soko::Mesh::Face::Face(VertexIndices v0, VertexIndices v1, VertexIndices v2)
{
  triangle[0] = v0;
  triangle[1] = v1;
  triangle[2] = v2;
}
