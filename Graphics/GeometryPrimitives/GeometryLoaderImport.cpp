
#include <rapidxml.hpp>
#include <rapidxml_utils.hpp>

#include <iostream>
#include <Windows.h>

#include "GeometryLoader.h"
#include "private/Utilities.h"

namespace soko
{
  class MayaReader
  {
  public:
    typedef GeometryLoader Data;
    typedef rapidxml::xml_document<> XMLDocument;
    typedef rapidxml::xml_node<> XMLNode;

  public:
    GeometryLoader::Node *Root;

    MayaReader();
    bool parseContent(std::string const &);
    bool readFromFileAndParseContent(std::string const &);
    void clear(void);

  protected:
    void importSkinCluster(XMLNode*);
    void importFamily(XMLNode*, Data::Node*);
    void importCurves(XMLNode*, Data::Transform*);

  protected:
    void importTranslate(XMLNode*, Data::Transform*);
    void importRotate(XMLNode*, Data::Transform*);
    void importScale(XMLNode*, Data::Transform*);

  protected:
    void importShape(XMLNode*);
    void importTransform(XMLNode*);
    void ImportPolygons(XMLNode*, Data::Shape*);
    void ImportUVSets(XMLNode*, Data::Shape*);

  protected:
    void ImportSpacedVectors(XMLNode *shapeNode, Data::Shape::VectorsInSpaceCollection *&container, std::string const &collection, std::string const &item);

  protected:
    static bool IsResultCheckedAt(uint32_t const & result, uint32_t const &at);
    static void GetUIntAttribute(XMLNode *node, std::string const &name, uint32_t &attribute, uint32_t &result);
    static void GetFloatAttribute(XMLNode *node, std::string const &name, float &attribute, uint32_t &result);
    static void GetStringAttribute(XMLNode *node, std::string const &name, std::string &attribute, uint32_t &result);
    static void GetXYZWAttributes(XMLNode *node, float &x, float &y, float &z, float *w, uint32_t &result);
    static void GetUVAttributes(XMLNode *node, float &u, float &v, uint32_t &result);
    static void GetIndexCountAttributes(XMLNode *node, uint32_t &index, uint32_t &count, uint32_t &result);
    static void GetCountAttribute(XMLNode *node, uint32_t &count, uint32_t &result);
    static void GetIndexAttribute(XMLNode *node, uint32_t &index, uint32_t &result);
    static void GetSpaceAttribute(XMLNode *node, std::string &space, uint32_t &result);
    static void GetNameAttribute(XMLNode *node, std::string &name, uint32_t &result);
    static void GetAdultCountAttribute(XMLNode *node, std::string &name, uint32_t &count, uint32_t &result);
    static void GetKidCountAttribute(XMLNode *node, std::string &name, uint32_t &count, uint32_t &result);
    static void GetAdultsNode(XMLNode *node, XMLNode *&adults);
    static void GetKidsNode(XMLNode *node, XMLNode *&kids);

  protected:
    void importShapes(void);
    void importTransforms(void);
    void importSkinClusters(void);

    XMLNode *m_dataNode;
    XMLDocument *m_document;
  };
}


soko::GeometryLoader::GeometryLoader()
{
  impl = 0;
}

soko::GeometryLoader::~GeometryLoader()
{
  clear();
}

soko::GeometryLoader::Node *soko::GeometryLoader::getRootNode()
{
  if (impl) return impl->Root;
  return nullptr;
}

bool soko::GeometryLoader::readFromFileAndParseContent(std::string const &file)
{
  clear();
  impl = new MayaReader();
  return impl->readFromFileAndParseContent(file);
}

bool soko::GeometryLoader::parseContent(std::string const &content)
{
  clear();
  impl = new MayaReader();
  return impl->parseContent(content);
}

void soko::GeometryLoader::clear()
{
  if (impl)
  {
    impl->clear();
    ReleasePointer(impl);
    impl = 0;
  }
}

soko::MayaReader::MayaReader()
{
  Root = 0;
  m_dataNode = 0;
  m_document = 0;
}

bool soko::MayaReader::readFromFileAndParseContent(std::string const &contentPath)
{
  rapidxml::file<> content(contentPath.c_str());
  return parseContent(content.data());
}

bool soko::MayaReader::parseContent(std::string const &content)
{
  if (m_document) m_document->clear();
  ReleasePointer(m_document);
  m_document = 0;
  m_dataNode = 0;

  if (m_document = new XMLDocument()) try
  {
    m_document->parse<::rapidxml::parse_default>(
      const_cast<char*>(content.data())
      );
  }
  catch (rapidxml::parse_error const &error)
  {
    trace("parseContent: rapidxml::parse_error=%s", error.what());
    return false;
  }

  m_dataNode = m_document->first_node();
  if (strcmp(m_dataNode->name(), "Data") != 0)
  {
    trace("parseContent: mesh format error: data node cannot be found");
    m_document->clear();
    return false;
  }
  else
  {
    // Exporter="Nena.v.2013.v.2"
    auto exporter = m_dataNode->first_attribute("Exporter");
    if (exporter && strcmp(exporter->value(), "Nena.v.2013.v.2") != 0)
    {
      trace("parseContent: mesh version error: should be 'Nena.v.2013.v.2'");
      m_document->clear();
      return false;
    }
  }

  ReleasePointer(Root);
  Root = new Data::Node();

  importShapes();
  importTransforms();
  importSkinClusters();

  if (m_document) m_document->clear();
  ReleasePointer(m_document);
  m_document = 0;
  m_dataNode = 0;

  return true;
}

void soko::MayaReader::clear()
{
  if (Root)
  {
    ReleaseVector(Root->m_handles);
    ReleaseVector(Root->m_kids);
    ReleasePointer(Root);
    Root = nullptr;
  }
}

bool soko::MayaReader::IsResultCheckedAt(uint32_t const & result, uint32_t const &at = 0)
{
  return (result & (1 << at)) == (1 << at);
}

void soko::MayaReader::GetUIntAttribute(XMLNode *node, std::string const &name, uint32_t &attribute, uint32_t &result)
{
  auto a = node->first_attribute(name.c_str());
  if (a) attribute = (uint32_t)std::stoul(a->value());
  result = a ? 1 : 0;
}

void soko::MayaReader::GetFloatAttribute(XMLNode *node, std::string const &name, float &attribute, uint32_t &result)
{
  auto a = node->first_attribute(name.c_str());
  if (a) attribute = std::stof(a->value());
  result = a ? 1 : 0;
}

void soko::MayaReader::GetStringAttribute(XMLNode *node, std::string const &name, std::string &attribute, uint32_t &result)
{
  auto a = node->first_attribute(name.c_str());
  if (a) attribute = a->value();
  result = a ? 1 : 0;
}

void soko::MayaReader::GetXYZWAttributes(XMLNode *node, float &x, float &y, float &z, float *w, uint32_t &result)
{
  uint32_t partialResult = 0;
  GetFloatAttribute(node, "X", x, partialResult); if (partialResult) result |= (1 << 0);
  GetFloatAttribute(node, "Y", y, partialResult); if (partialResult) result |= (1 << 1);
  GetFloatAttribute(node, "Z", z, partialResult); if (partialResult) result |= (1 << 2);
  if (w) GetFloatAttribute(node, "W", (*w), partialResult); if (partialResult) result |= (1 << 3);
}

void soko::MayaReader::GetUVAttributes(XMLNode *node, float &u, float &v, uint32_t &result)
{
  uint32_t partialResult = 0;
  GetFloatAttribute(node, "U", u, partialResult); if (partialResult) result |= (1 << 0);
  GetFloatAttribute(node, "V", v, partialResult); if (partialResult) result |= (1 << 1);
}

void soko::MayaReader::GetCountAttribute(XMLNode *node, uint32_t &count, uint32_t &result)
{
  GetUIntAttribute(node, "Count", count, result);
}

void soko::MayaReader::GetIndexAttribute(XMLNode *node, uint32_t &index, uint32_t &result)
{
  GetUIntAttribute(node, "Index", index, result);
}

void soko::MayaReader::GetIndexCountAttributes(XMLNode *node, uint32_t &index, uint32_t &count, uint32_t &result)
{
  uint32_t partialResult = 0;
  GetUIntAttribute(node, "Index", index, partialResult); if (partialResult) result |= (1 << 0);
  GetUIntAttribute(node, "Count", count, partialResult); if (partialResult) result |= (1 << 1);
}

void soko::MayaReader::GetSpaceAttribute(XMLNode *node, std::string &space, uint32_t &result)
{
  GetStringAttribute(node, "Space", space, result);
}

void soko::MayaReader::GetNameAttribute(XMLNode *node, std::string &name, uint32_t &result)
{
  GetStringAttribute(node, "Name", name, result);
}

void soko::MayaReader::GetAdultCountAttribute(XMLNode *node, std::string &name, uint32_t &count, uint32_t &result)
{
  GetUIntAttribute(node, "Dag-Kid-Count", count, result);
}

void soko::MayaReader::GetKidCountAttribute(XMLNode *node, std::string &name, uint32_t &count, uint32_t &result)
{
  GetUIntAttribute(node, "Dag-Adult-Count", count, result);
}

void soko::MayaReader::GetAdultsNode(XMLNode *node, XMLNode *&adults)
{
  adults = node->first_node("Dag-Adults");
}

void soko::MayaReader::GetKidsNode(XMLNode *node, XMLNode *&kids)
{
  kids = node->first_node("Dag-Kids");
}

void soko::MayaReader::importFamily(
  XMLNode *hostNode, Data::Node *hostData
  )
{
  uint32_t result = 0;

  XMLNode *kidsNode; GetKidsNode(hostNode, kidsNode); if (kidsNode)
  {
    uint32_t kidCount; GetCountAttribute(kidsNode, kidCount, result);
    XMLNode *kidNode = kidsNode->first_node();
    while (kidNode)
    {
      std::string kidName; GetNameAttribute(kidNode, kidName, result);

      Data::Node* kidData = hostData->getKidNode(kidName); if (!kidData)
      {
        kidData = Root->getKidNode(kidName, TRUE);
        hostData->getKids()->push_back(kidData);
        kidData->getParents()->push_back(hostData);
      }

      kidNode = kidNode->next_sibling();
    }
  }

  XMLNode *adultsNode; GetAdultsNode(hostNode, adultsNode); if (adultsNode)
  {
    uint32_t adultCount; GetCountAttribute(adultsNode, adultCount, result);

    XMLNode *adultNode = adultsNode->first_node();
    while (adultNode)
    {
      std::string adultName; GetNameAttribute(adultNode, adultName, result);
      Data::Node *adultData = hostData->getParentNode(adultName); if (!adultData)
      {
        adultData = Root->getKidNode(adultName, TRUE);
        hostData->getParents()->push_back(adultData);
        adultData->getKids()->push_back(hostData);
      }

      adultNode = adultNode->next_sibling();
    }
  }
}

void soko::MayaReader::ImportSpacedVectors(
  XMLNode *shapeNode, Data::Shape::VectorsInSpaceCollection *&container,
  std::string const &collection,
  std::string const &item
  )
{
  uint32_t result = 0;

  XMLNode *pointsNode = shapeNode->first_node(collection.c_str());

  if (pointsNode) container = new std::vector<Data::Shape::VectorsInSpace *>();

  while (pointsNode)
  {
    uint32_t pointCount;
    std::string pointsSpace;
    GetCountAttribute(pointsNode, pointCount, result);
    GetSpaceAttribute(pointsNode, pointsSpace, result);

    Data::Shape::VectorsInSpace *spaceVectors = new Data::Shape::VectorsInSpace();
    container->push_back(spaceVectors);

    spaceVectors->vectors = new DirectX::XMFLOAT3[pointCount];
    spaceVectors->space = pointsSpace;
    spaceVectors->count = pointCount;

    XMLNode *pointNode = pointsNode->first_node(item.c_str());

    while (pointNode)
    {
      uint32_t index; GetIndexAttribute(pointNode, index, result);

      GetXYZWAttributes(pointNode,
        spaceVectors->vectors[index].x,
        spaceVectors->vectors[index].y,
        spaceVectors->vectors[index].z,
        nullptr, result
        );

      pointNode = pointNode->next_sibling(item.c_str());
    }

    pointsNode = pointsNode->next_sibling(collection.c_str());
  }
}

void soko::MayaReader::ImportPolygons(XMLNode *shapeNode, Data::Shape *shapeData)
{
  uint32_t result = 0;
  uint32_t bntnIndex = 0;
  uint32_t polygonsCount = 0;
  XMLNode *polygonsNode = shapeNode->first_node("Polygons");
  GetCountAttribute(polygonsNode, polygonsCount, result);

  shapeData->polygons.resize(polygonsCount);

  XMLNode *polygonNode = polygonsNode->first_node("Polygon");
  while (polygonNode)
  {
    uint32_t vertexCount = 0; uint32_t polygonIndex;
    GetCountAttribute(polygonNode, vertexCount, result);
    GetIndexAttribute(polygonNode, polygonIndex, result);

    shapeData->polygons[polygonIndex] = new Data::Shape::Polygon;
    shapeData->polygons[polygonIndex]->vertices = new Data::Shape::Polygon::Vertex[vertexCount];
    shapeData->polygons[polygonIndex]->vertexCount = vertexCount;

    XMLNode *vertexNode = polygonNode->first_node("Vertex");
    while (vertexNode)
    {
      uint32_t vertexIndex, pointIndex, normalIndex, texcoordsCount;
      GetIndexAttribute(vertexNode, vertexIndex, result);
      GetUIntAttribute(vertexNode, "Point-Index", pointIndex, result);
      GetUIntAttribute(vertexNode, "Normal-Index", normalIndex, result);
      GetCountAttribute(vertexNode, texcoordsCount, result);

      shapeData->polygons[polygonIndex]->vertices[vertexIndex].pointIndex = pointIndex;
      shapeData->polygons[polygonIndex]->vertices[vertexIndex].normalIndex = normalIndex;
      shapeData->polygons[polygonIndex]->vertices[vertexIndex].binormalIndex = bntnIndex;
      shapeData->polygons[polygonIndex]->vertices[vertexIndex].tangentIndex = bntnIndex;
      shapeData->polygons[polygonIndex]->vertices[vertexIndex].texCoordCount = texcoordsCount;
      shapeData->polygons[polygonIndex]->vertices[vertexIndex].texCoords = new Data::Shape::Polygon::Vertex::Tex[texcoordsCount];

      uint32_t texIndex = 0;
      XMLNode *texNode = vertexNode->first_node("Tex");
      while (texNode)
      {

        std::string uvSetName; uint32_t uvIndex;
        GetStringAttribute(texNode, "UV-Set", uvSetName, result);
        GetUIntAttribute(texNode, "UV-Index", uvIndex, result);

        shapeData->polygons[polygonIndex]->vertices[vertexIndex].texCoords[texIndex].uvIndex = uvIndex;
        shapeData->polygons[polygonIndex]->vertices[vertexIndex].texCoords[texIndex].uvSet = uvSetName;

        texNode = texNode->next_sibling("Tex");
      }

      vertexNode = vertexNode->next_sibling("Vertex");
      bntnIndex++;
    }

    polygonNode = polygonNode->next_sibling("Polygon");
  }
}

void soko::MayaReader::ImportUVSets(XMLNode *shapeNode, Data::Shape *shapeData)
{
  uint32_t result = 0;
  uint32_t uvSetsCount = 0;
  XMLNode *uvSetsNode = shapeNode->first_node("UV-Sets");
  GetCountAttribute(uvSetsNode, uvSetsCount, result);

  shapeData->sets.resize(uvSetsCount);

  if (uvSetsNode && uvSetsCount)
  {
    XMLNode *uvSetNode = uvSetsNode->first_node("UV-Set");
    while (uvSetNode)
    {
      std::string uvSetName;
      uint32_t uvSetIndex;

      GetNameAttribute(uvSetNode, uvSetName, result);
      GetIndexAttribute(uvSetNode, uvSetIndex, result);

      uint32_t uvCount = 0, bnCount = 0, tnCount = 0;

      GetUIntAttribute(uvSetNode, "UV-Count", uvCount, result);
      GetUIntAttribute(uvSetNode, "Binormal-Count", bnCount, result);
      GetUIntAttribute(uvSetNode, "Tangent-Count", tnCount, result);

      shapeData->sets[uvSetIndex] = new Data::Shape::UVSet;
      shapeData->sets[uvSetIndex]->name = uvSetName;
      shapeData->sets[uvSetIndex]->uvs.resize(uvCount);
      shapeData->sets[uvSetIndex]->binormals.resize(bnCount);
      shapeData->sets[uvSetIndex]->tangents.resize(tnCount);

      XMLNode *uvNode = uvSetNode->first_node("UV"); while (uvNode)
      {
        uint32_t uvIndex;

        GetIndexAttribute(uvNode,
          uvIndex, result
          );
        GetUVAttributes(uvNode,
          shapeData->sets[uvSetIndex]->uvs[uvIndex].x,
          shapeData->sets[uvSetIndex]->uvs[uvIndex].y,
          result
          );

        uvNode = uvNode->next_sibling("UV");
      }

      XMLNode *bnNode = uvSetNode->first_node("Binormal"); while (bnNode)
      {
        uint32_t bnIndex;

        GetIndexAttribute(bnNode,
          bnIndex, result
          );
        GetXYZWAttributes(bnNode,
          shapeData->sets[uvSetIndex]->binormals[bnIndex].x,
          shapeData->sets[uvSetIndex]->binormals[bnIndex].y,
          shapeData->sets[uvSetIndex]->binormals[bnIndex].z,
          nullptr, result
          );

        bnNode = bnNode->next_sibling("Binormal");
      }

      XMLNode *tnNode = uvSetNode->first_node("Tangent"); while (tnNode)
      {
        uint32_t tnIndex;

        GetIndexAttribute(tnNode,
          tnIndex, result
          );
        GetXYZWAttributes(tnNode,
          shapeData->sets[uvSetIndex]->tangents[tnIndex].x,
          shapeData->sets[uvSetIndex]->tangents[tnIndex].y,
          shapeData->sets[uvSetIndex]->tangents[tnIndex].z,
          nullptr, result
          );

        tnNode = tnNode->next_sibling("Tangent");
      }

      uvSetNode = uvSetNode->next_sibling("UV-Set");
    }
  }
}

void soko::MayaReader::importShape(XMLNode *shapeNode)
{
  uint32_t result = 0;
  std::string shapeName;
  GetNameAttribute(shapeNode, shapeName, result);

  trace("importShape: shape 0x%0x", shapeNode);

  Data::Node *shape = Root->getKidNode(shapeName, TRUE);
  shape->m_type = Data::Node::kShape;
  shape->m_name = shapeName;

  Data::Shape *shapeData = new Data::Shape();
  Root->getHandles()->push_back(shapeData);
  shape->m_handles.push_back(shapeData);
  shapeData->m_type = Data::Node::kShape;
  shapeData->m_name = shapeName;

  importFamily(shapeNode, shape);
  ImportSpacedVectors(shapeNode, shapeData->points, "Points", "Point");
  ImportSpacedVectors(shapeNode, shapeData->normals, "Normals", "Normal");
  ImportUVSets(shapeNode, shapeData);
  ImportPolygons(shapeNode, shapeData);

  trace(" - name %s", shapeName.c_str());
  trace(" - faces: %u", shapeData->polygons.size());
  trace(" - points: %u", shapeData->points->at(0)->count);
  trace(" - normals: %u", shapeData->normals->at(0)->count);
  trace(" - uv sets: %u", shapeData->sets[0]->uvs.size());
}

void soko::MayaReader::importShapes()
{
  uint32_t result = 0;

  XMLNode *shapesNode = m_dataNode->first_node("Shapes"); if (shapesNode)
  {
    uint32_t shapeCount;
    GetCountAttribute(shapesNode, shapeCount, result); if (IsResultCheckedAt(result))
    {
      XMLNode *shapeNode = shapesNode->first_node("Shape");
      while (shapeNode)
      {
        importShape(shapeNode);
        shapeNode = shapeNode->next_sibling("Shape");
      }
    }
  }
}

void soko::MayaReader::importCurves(
  XMLNode *transformNode, Data::Transform *transformData
  )
{
  uint32_t type = 0;
  uint32_t result = 0;
  uint32_t keyCount = 0;
  std::string curveName;
  XMLNode *curveNode, *keyNode;

  curveNode = transformNode->first_node("Curve");
  if (curveNode) transformData->curves = new std::vector<Data::Transform::Curve *>();

  while (curveNode)
  {
    GetCountAttribute(curveNode, keyCount, result);
    GetUIntAttribute(curveNode, "Type", type, result);
    GetStringAttribute(curveNode, "Name", curveName, result);

    Data::Transform::Curve *curveData = new Data::Transform::Curve();
    transformData->curves->push_back(curveData);
    //Root->getHandles()->push_back(curveData);

    curveData->m_name = curveName;
    curveData->m_type = Data::Item::kAnimationCurve;
    curveData->type = type;
    curveData->keyCount = keyCount;
    curveData->keys = new Data::Transform::Curve::Key[keyCount];

    keyNode = curveNode->first_node("Key");
    while (keyNode)
    {
      uint32_t keyIndex;
      GetIndexAttribute(keyNode, keyIndex, result);

      GetFloatAttribute(keyNode, "Time", curveData->keys[keyIndex].t, result);
      GetFloatAttribute(keyNode, "Value", curveData->keys[keyIndex].value, result);
      GetFloatAttribute(keyNode, "InX", curveData->keys[keyIndex].in.x, result);
      GetFloatAttribute(keyNode, "InY", curveData->keys[keyIndex].in.y, result);
      GetFloatAttribute(keyNode, "OutX", curveData->keys[keyIndex].out.x, result);
      GetFloatAttribute(keyNode, "OutY", curveData->keys[keyIndex].out.y, result);
      GetUIntAttribute(keyNode, "InType", curveData->keys[keyIndex].tangentIn, result);
      GetUIntAttribute(keyNode, "OutType", curveData->keys[keyIndex].tangentOut, result);

      keyNode = keyNode->next_sibling("Key");
    }

    trace("importCurves: curve 0x%0x: name=%s keys=%u"
      , curveNode
      , curveName.c_str()
      , keyCount
      );

    curveNode = curveNode->next_sibling("Curve");
  }
}

void soko::MayaReader::importTranslate(
  XMLNode *transformNode, Data::Transform *transformData
  )
{
  uint32_t result = 0;
  std::string space;

  XMLNode *translateNode = transformNode->first_node("Translate"); while (translateNode)
  {
    GetSpaceAttribute(translateNode, space, result);

    if (space == "Object") GetXYZWAttributes(
      translateNode,
      transformData->translate.x,
      transformData->translate.y,
      transformData->translate.z,
      nullptr, result
      );
    else if (space == "Transform") GetXYZWAttributes(
      translateNode,
      transformData->translateTS.x,
      transformData->translateTS.y,
      transformData->translateTS.z,
      nullptr, result
      );
    else if (space == "World") GetXYZWAttributes(
      translateNode,
      transformData->translateWS.x,
      transformData->translateWS.y,
      transformData->translateWS.z,
      nullptr, result
      );

    translateNode = translateNode->next_sibling("Translate");
  }
}

void soko::MayaReader::importRotate(
  XMLNode *transformNode, Data::Transform *transformData
  )
{
  uint32_t result = 0;
  std::string space;

  XMLNode *rotateNode = transformNode->first_node("Rotate");
  XMLNode *eulerNode = rotateNode->first_node("Euler-Angles");

  GetXYZWAttributes(eulerNode,
    transformData->rotateEA.x,
    transformData->rotateEA.y,
    transformData->rotateEA.z,
    nullptr, result
    );

  XMLNode *quat = rotateNode->first_node("Quaternion"); while (quat)
  {
    std::string space; GetSpaceAttribute(quat, space, result);

    if (space == "Object") GetXYZWAttributes(
      quat,
      transformData->rotateQ.x,
      transformData->rotateQ.y,
      transformData->rotateQ.z,
      &transformData->rotateQ.w,
      result
      );
    else if (space == "Transform") GetXYZWAttributes(
      quat,
      transformData->rotateQTS.x,
      transformData->rotateQTS.y,
      transformData->rotateQTS.z,
      &transformData->rotateQTS.w,
      result
      );
    else if (space == "World") GetXYZWAttributes(
      quat,
      transformData->rotateQWS.x,
      transformData->rotateQWS.y,
      transformData->rotateQWS.z,
      &transformData->rotateQWS.w,
      result
      );

    quat = quat->next_sibling("Quaternion");
  }

  XMLNode *orient = rotateNode->first_node("Orientation"); while (orient)
  {
    std::string space; GetSpaceAttribute(orient, space, result);

    if (space == "Object") GetXYZWAttributes(
      orient,
      transformData->rotateOOS.x,
      transformData->rotateOOS.y,
      transformData->rotateOOS.z,
      &transformData->rotateOOS.w,
      result
      );
    else if (space == "*Object") GetXYZWAttributes(
      orient,
      transformData->rotateO.x,
      transformData->rotateO.y,
      transformData->rotateO.z,
      &transformData->rotateO.w,
      result
      );
    else if (space == "Transform") GetXYZWAttributes(
      orient,
      transformData->rotateOTS.x,
      transformData->rotateOTS.y,
      transformData->rotateOTS.z,
      &transformData->rotateOTS.w,
      result
      );
    else if (space == "World") GetXYZWAttributes(
      orient,
      transformData->rotateOWS.x,
      transformData->rotateOWS.y,
      transformData->rotateOWS.z,
      &transformData->rotateOWS.w,
      result
      );

    orient = orient->next_sibling("Orientation");
  }
}

void soko::MayaReader::importScale(
  XMLNode *transformNode, Data::Transform *transformData
  )
{
  uint32_t result = 0;
  std::string space;

  XMLNode *scaleNode = transformNode->first_node("Scale");
  //GetSpaceAttribute(scaleNode, space, result);

  GetXYZWAttributes(scaleNode,
    transformData->scale.x,
    transformData->scale.y,
    transformData->scale.z,
    nullptr, result
    );
}

void soko::MayaReader::importTransform(XMLNode *transformNode)
{
  uint32_t result = 0;
  std::string transformName;
  uint32_t isJoint = 0;
  GetNameAttribute(transformNode, transformName, result);
  GetUIntAttribute(transformNode, "IsJoint", isJoint, result);

  Data::Node *transform = Root->getKidNode(transformName, TRUE);
  transform->m_type = Data::Node::kTransform;
  transform->m_name = transformName;

  Data::Transform *transformData = new Data::Transform();
  transform->m_handles.push_back(transformData);
  Root->getHandles()->push_back(transformData);

  transformData->isJoint = !!result;
  transformData->m_type = Data::Node::kTransform;
  transformData->m_name = transformName;

  importFamily(transformNode, transform);
  importTranslate(transformNode, transformData);
  importRotate(transformNode, transformData);
  importScale(transformNode, transformData);
  importCurves(transformNode, transformData);
}

void soko::MayaReader::importTransforms()
{
  uint32_t result = 0;

  XMLNode *transformsNode = m_dataNode->first_node("Transforms"); if (transformsNode)
  {
    uint32_t transformsCount;
    GetCountAttribute(transformsNode, transformsCount, result); if (IsResultCheckedAt(result))
    {
      XMLNode *transformNode = transformsNode->first_node("Transform"); while (transformNode)
      {
        importTransform(transformNode);
        transformNode = transformNode->next_sibling("Transform");
      }
    }
  }
}

void soko::MayaReader::importSkinCluster(XMLNode *clusterNode)
{
  uint32_t result = 0;

  std::string clusterName; 
  uint32_t skinCount; 

  GetNameAttribute(clusterNode, clusterName, result);
  GetCountAttribute(clusterNode, skinCount, result);

  Data::SkinCluster *clusterData = new Data::SkinCluster();
  Root->getHandles()->push_back(clusterData);
  clusterData->m_type = Data::Item::kSkinCluster;
  clusterData->m_name = clusterName;

  XMLNode *skinNode = clusterNode->first_node("Skin"); while (skinNode)
  {
    Data::SkinCluster::Skin *skin = new Data::SkinCluster::Skin;
    clusterData->Geometries.push_back(skin);
    Root->getHandles()->push_back(skin);

    uint32_t skinIndex; 
    std::string skinName; 
    uint32_t componentCount; 

    GetIndexAttribute(skinNode, skinIndex, result);
    GetNameAttribute(skinNode, skinName, result);
    GetCountAttribute(skinNode, componentCount, result);

    skin->m_name = skinName;
    skin->m_type = Data::Item::kSkin;
    skin->componentCount = componentCount;
    skin->components = new Data::SkinCluster::Skin::Component[componentCount];

    trace("importSkinCluster: skin address=0x%0x name=%s components=%u"
      , skinNode
      , skinName.c_str()
      , componentCount
      );

    XMLNode *componentNode = skinNode->first_node("Component"); while (componentNode)
    {
      uint32_t componentIndex;
      uint32_t influenceCount;

      GetIndexAttribute(componentNode, componentIndex, result);
      GetCountAttribute(componentNode, influenceCount, result);

      skin->components[componentIndex].influenceCount = influenceCount;
      skin->components[componentIndex].influences = new Data::SkinCluster::Skin::Component::Influence[influenceCount];

      trace(" - component: ind=%u address=0x%0x: influences=%u"
        , componentIndex
        , componentNode
        , influenceCount
        );

      XMLNode *influenceNode = componentNode->first_node("Influence"); while (influenceNode)
      {
        uint32_t influenceIndex; 

        float influenceWeight;
        std::string fullPath;
        std::string partialPath;

        GetIndexAttribute(influenceNode, influenceIndex, result);
        GetFloatAttribute(influenceNode, "Weight", influenceWeight, result);
        GetStringAttribute(influenceNode, "Full-Path", fullPath, result);
        GetStringAttribute(influenceNode, "Partial-Path", partialPath, result);

        skin->components[componentIndex].influences[influenceIndex].weight = influenceWeight;
        skin->components[componentIndex].influences[influenceIndex].fullPath = fullPath;
        skin->components[componentIndex].influences[influenceIndex].partialPath = partialPath;

       /* trace(" -- influence address=0x%0x: %f for %s"
          , influenceNode
          , influenceWeight
          , fullPath.c_str()
          );*/

        influenceNode = influenceNode->next_sibling("Influence");
      }

      componentNode = componentNode->next_sibling("Component");
    }

    skinNode = skinNode->next_sibling("Skin");
  }
}

void soko::MayaReader::importSkinClusters()
{
  uint32_t result = 0;

  XMLNode *clustersNode = m_dataNode->first_node("Skin-Clusters"); if (clustersNode)
  {
    uint32_t transformsCount; GetCountAttribute(clustersNode, transformsCount, result);

    if (IsResultCheckedAt(result))
    {
      XMLNode *clusterNode = clustersNode->first_node("Cluster");
      while (clusterNode)
      {
        importSkinCluster(clusterNode);

        clusterNode = clusterNode->next_sibling("Cluster");
      }
    }
  }
}

























