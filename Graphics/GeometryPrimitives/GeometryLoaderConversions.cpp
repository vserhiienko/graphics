
#include <GeometryLoader.h>

void soko::GeometryLoader::asMesh(
  GeometryLoader::Shape *shape,
  _Out_ Mesh *mesh
  )
{
  if (!shape) return;

  soko::GeometryLoader::Shape::VectorsInSpace 
    *points = shape->points->at(0), 
    *normals = shape->normals->at(0);
  decltype(shape->sets[0]->uvs)
    &uvs = shape->sets[0]->uvs;

  mesh->points.resize(points->count);
  mesh->normals.resize(normals->count);
  mesh->uvs.resize(uvs.size());

  mesh->faces.resize(shape->polygons.size());
  mesh->n2pMap.resize(mesh->normals.size());

  for (uint32_t p = 0; p < shape->points->at(1)->count; p++) mesh->points[p] = (points->vectors[p]);
  for (uint32_t n = 0; n < shape->normals->at(1)->count; n++) mesh->normals[n] = (normals->vectors[n]);
  for (uint32_t t = 0; t < uvs.size(); t++) mesh->uvs[t] = (uvs[t]);

  for (uint32_t f = 0; f < shape->polygons.size(); f++)
  {
    for (uint32_t v = 0; v < shape->polygons[f]->vertexCount; v++)
    {
      mesh->faces[f].triangle[v].p = shape->polygons[f]->vertices[v].pointIndex;
      mesh->faces[f].triangle[v].n = shape->polygons[f]->vertices[v].normalIndex;
      mesh->faces[f].triangle[v].t = shape->polygons[f]->vertices[v].texCoords[0].uvIndex;
    }
  }
  for (uint32_t n = 0; n < mesh->normals.size(); n++)
  {
    bool pointIndexFound = false;

    for (uint32_t f = 0; f < mesh->faces.size() && !pointIndexFound; f++)
    {
      for (uint32_t v = 0; v < shape->polygons[f]->vertexCount; v++)
      {
        if (shape->polygons[f]->vertices[v].normalIndex == n)
        {
          mesh->n2pMap[n] = shape->polygons[f]->vertices[v].pointIndex;
          pointIndexFound = true;
          break;
        }
      }
    }

    assert(pointIndexFound);
  }
}


void soko::GeometryLoader::asTransform(
  GeometryLoader::Transform *source,
  _Out_ soko::Transform *object,
  _Out_ soko::Transform *world
  )
{
  if (source->isJoint) object->orientation = source->rotateO;
  else object->orientation = source->rotateOOS;
  if (source->isJoint) world->orientation = source->rotateO;
  else world->orientation = source->rotateOOS;

  (DirectX::XMFLOAT3&)world->rotationEA = source->rotateEA;
  (DirectX::XMFLOAT3&)object->translation = source->translateTS;
  object->rotation = source->rotateQTS;
  (DirectX::XMFLOAT3&)world->translation = source->translateWS;
  world->rotation = source->rotateQWS;

  if (source->curves)
  {
    uint32_t keyIndex = 0;
    uint32_t curveIndex = 0;

    auto sourceCurveBox = source->curves->begin();
    while (sourceCurveBox != source->curves->end())
    {
      auto sourceCurve = (*sourceCurveBox);

      if (sourceCurve->keyCount <= 1)
      {
        sourceCurveBox++;
        continue;
      }

      auto curve = new soko::Transform::Curve();
      curve->keys.resize(sourceCurve->keyCount);
      curve->name = sourceCurve->getNameCopy();
      curve->nameH = source->getNameCopy();

      keyIndex = 0;
      for (; keyIndex < sourceCurve->keyCount; keyIndex++)
      {
        curve->keys[keyIndex] = new soko::Transform::Curve::Key();

        curve->keys[keyIndex]->timeValue.y = sourceCurve->keys[keyIndex].value;
        curve->keys[keyIndex]->timeValue.x = sourceCurve->keys[keyIndex].t;
        curve->keys[keyIndex]->outputTangent = sourceCurve->keys[keyIndex].out;
        curve->keys[keyIndex]->inputTangent = sourceCurve->keys[keyIndex].in;
        curve->keys[keyIndex]->curve = curve;
      }

      keyIndex = 0;
      for (; keyIndex < sourceCurve->keyCount; keyIndex++)
      {
        if (keyIndex != sourceCurve->keyCount - 1) curve->keys[keyIndex]->nextKey = curve->keys[keyIndex + 1];
        if (keyIndex != 0) curve->keys[keyIndex]->previousKey = curve->keys[keyIndex - 1];
      }

      curve->leftKey = nullptr;
      curve->rightKey = curve->keys.front();

      object->channelMap[sourceCurve->getNameByRef()] = curve;
      sourceCurveBox++;
    }

    object->channelArray.tx = object->channelMap[source->getNameByRef() + "_translateX_AnimLayer1_inputB"];
    object->channelArray.ty = object->channelMap[source->getNameByRef() + "_translateY_AnimLayer1_inputB"];
    object->channelArray.tz = object->channelMap[source->getNameByRef() + "_translateZ_AnimLayer1_inputB"];
    object->channelArray.rx = object->channelMap[source->getNameByRef() + "_rotate_AnimLayer1_inputBX"];
    object->channelArray.ry = object->channelMap[source->getNameByRef() + "_rotate_AnimLayer1_inputBY"];
    object->channelArray.rz = object->channelMap[source->getNameByRef() + "_rotate_AnimLayer1_inputBZ"];
    object->channelArray.sx = object->channelMap[source->getNameByRef() + "_scaleX_AnimLayer1_inputB"];
    object->channelArray.sy = object->channelMap[source->getNameByRef() + "_scaleY_AnimLayer1_inputB"];
    object->channelArray.sz = object->channelMap[source->getNameByRef() + "_scaleZ_AnimLayer1_inputB"];

    object->isAnimated(true);
  }
  else
  {
    object->isAnimated(false);
  }

}

