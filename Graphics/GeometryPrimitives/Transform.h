#pragma once

#include <vector>
#include <unordered_map>
#include <DirectXMath.h>

namespace soko
{
  struct TransformSolver
  {
    enum Result
    {
      kUndefined = -2,
      kNotFound = -1,
      kNoReal = 0,
      k3Real = 1,
      k2Real = 2,
      k3RealEq = 3,
      k2RealEq = 4,
      k1Real = 5,
    };

    //! \brief Is \c good solution.
    inline static bool isGoodResult(Result const &result) { return result > 0; }
    //! \brief Is \c bad solution.
    inline static bool isBadResult(Result const &result) { return result <= 0; }

    /** \brief Solves y-value for the animation key pair.
    * \remark A bit complex and CPU-time-consuming function. One probably dont need it when animation tracks
    * \remark are baked or the intervals are small or the segments are linear-like or he doesnt want super-exact solution.
    * \param [in] p0 Left point
    * \param [in] p1 Right point
    * \param [in] m0 Input tangent
    * \param [in] m1 Output tangent
    * \param [in] x Total elapsed time (not 0..1-ranged)
    * \param [out] y Output channel value
    **/
    static TransformSolver::Result solveSegment(
      DirectX::XMFLOAT2 p0, DirectX::XMFLOAT2 m0, // left
      DirectX::XMFLOAT2 p1, DirectX::XMFLOAT2 m1, // right
      float x, _Out_ float &y
      );
    /** \brief Estimates y-value for the animation key pair.
    * \remark The returned value is an approximated solution. If your animation curves are complex, then use the function above.
    * \remark Use this one if your animation is baked, the segments have small time intervals or are linear or small errors are inrelevant.
    * \param [in] p0 Left point
    * \param [in] p1 Right point
    * \param [in] m0 Input tangent
    * \param [in] m1 Output tangent
    * \param [in] x Total elapsed time (not 0..1-ranged)
    * \param [out] y Output channel value
    **/
    static TransformSolver::Result solveSegmentEst(
      DirectX::XMFLOAT2 p0, DirectX::XMFLOAT2 m0, // left
      DirectX::XMFLOAT2 p1, DirectX::XMFLOAT2 m1, // right
      float x, _Out_ float &y
      );

  protected:
    //! \brief Solves \c hermite segment.
    static float solveHermiteSegment(
      float p0, float m0, // left
      float p1, float m1, // right
      float t
      );
    //! \brief Solves \c square equation.
    static TransformSolver::Result solve2(
      float a, float b, float c, // coeff
      _Out_ float &x, _Out_ float &y // vars
      );
    //! \brief Solves \c cubic equation.
    static TransformSolver::Result solve3(
      float a, float b, float c, float d, // coeff
      _Out_ float &x, _Out_ float &y, _Out_ float &z // vars
      );

  private:
    //! \brief Returns the max value from the provided.
    static float getMax(float x, float y);
    //! \brief Returns the max value from the provided.
    static float getMax(float x, float y, float z);
    //! \remark IEEE standard workaround. Can only slower the code, so dont use.
    static float getPower(float x, float p);
    //! \brief Finds the value that is in range 0..1. If its not found, returns zero, otherwise nonzero.
    static bool findHermiteTime(float x, float y, float z, _Out_ float &t);

  };

  struct Transform
  {
    typedef struct Curve
    {
      typedef enum Index
      {
        kSx, kSy, kSz,
        kRx, kRy, kRz,
        kTx, kTy, kTz,
        kMax
      } CurveIndex;

      typedef Curve *SXArray[kMax];

      typedef std::string String;
      typedef std::unordered_map<String, Curve*> MapToString;

      typedef std::vector<Curve*> Vector;
      typedef Vector::iterator Iterator;

      typedef struct Key
      {
        typedef ::std::vector<Key*> Vector;
        typedef Vector::iterator Iterator;

        DirectX::XMFLOAT2 timeValue;
        DirectX::XMFLOAT2 inputTangent; 
        DirectX::XMFLOAT2 outputTangent;

        Transform::Curve *curve;
        Transform::Curve::Key *nextKey;
        Transform::Curve::Key *previousKey;

        Key(void);

      } TrackKey, AnimationKey;

      float resolveAt(float time, bool rec = false);

      union Array
      {
        struct
        {
          Transform::Curve *sx;
          Transform::Curve *sy;
          Transform::Curve *sz;
          Transform::Curve *rx;
          Transform::Curve *ry;
          Transform::Curve *rz;
          Transform::Curve *tx;
          Transform::Curve *ty;
          Transform::Curve *tz;
        };
        struct 
        {
          Transform::Curve::SXArray channels; 
          inline Transform::Curve *getAt(CurveIndex i) { return channels[i]; }
          template <CurveIndex _Ind> inline Transform::Curve *get() { return channels[_Ind]; }
        };

        Array(void);
      };

      TrackKey::Vector keys;
      TrackKey *rightKey;
      TrackKey *leftKey;
      String nameH;
      String name;

      Curve(void);

    } Track, AnimationTrack, Channel;

    DirectX::XMFLOAT3 scale;
    DirectX::XMFLOAT3 rotationEA;
    DirectX::XMFLOAT4 rotation;
    DirectX::XMFLOAT4 orientation;
    DirectX::XMFLOAT4 translation;

    Transform::Curve::Array channelArray;
    Transform::Curve::MapToString channelMap;

    inline void isAnimated(bool anim)
    {
      if (anim && channelMap.size()) m_resolveSX = s_resolveSX_animated;
      else m_resolveSX = s_resolveSX_static;
    }

    inline void resolveSX(float time) { m_resolveSX(this, time); }
    inline bool isAnimated(void) { return (&s_resolveSX_animated) == m_resolveSX; }

    Transform(void);

  private:
    typedef void (*ResolveSXCallback)(Transform*, float);

    ResolveSXCallback m_resolveSX = nullptr;

    inline void resolveSX_animated(float time)
    {
      resolveSS_animated(time);
      resolveSR_animated(time);
      resolveST_animated(time);
    }

    void resolveSS_animated(float time);
    void resolveSR_animated(float time);
    void resolveST_animated(float time);

    inline static void s_resolveSX_static(Transform *transform, float time)
    {
    }

    inline static void s_resolveSX_animated(Transform *transform, float time)
    {
      transform->resolveSX_animated(time);
    }

  };
}