
#include "AegaScene.h"
#include <BinaryReader.h>
#include <private/BasicIO.Utils.h>
#include "private/Utilities.h"

const char aega::SceneDeserialize::sign[4] = { 'A', 'E', 'G', 'A' };
const uint32_t aega::SceneDeserialize::version = 0x00010001;

struct aega::SceneDeserialize::Context
{
  aega::io::BasicReader *reader;
  Context() : reader(0) { }
};

aega::SceneDeserialize::SceneDeserialize()
  : impl(new Context())
{
}

aega::SceneDeserialize::~SceneDeserialize()
{
  safeDelete(impl);
}

void aega::SceneDeserialize::deserializeTo(Scene *scene, std::string const &aegaFile)
{
  if (!scene) return;
  auto file = aega::io::File::OpenFileForReading(aegaFile.c_str());
  if (!file->GetHandle())  return;


  // ----------------------------------------------
  safeDelete(impl->reader);
  impl->reader = new aega::io::BasicReader(file);

  char aegaSign[4];
  uint32_t aegaVersion = 0;
  impl->reader->NextElement<char[4]>(aegaSign);
  impl->reader->NextElement<uint32_t>(aegaVersion);
  //if (std::string(aegaSign) != std::string(sign)) return;
  //if (version != aegaVersion) return;

  // ----------------------------------------------
  safeDelete(impl->reader);
  impl->reader = new aega::io::CompressedReader(file);

  bool done = false;
  uint32_t lastApi = 0;
  while (!done)
  {
    Node *node = 0;

    switch (lastApi = nextNodeApi())
    {
    case 7:

      scene->animCurves.push_back(new AnimationCurveNode());
      node = scene->animCurves.back();
      deserializeTo((DepNode*)scene->animCurves.back());
      deserializeTo(scene->animCurves.back());
      break;

    case 296: // mesh

      scene->meshes.push_back(new MeshNode());
      node = scene->meshes.back();
      deserializeTo((DagNode*)scene->meshes.back());
      deserializeTo((FamilyNode*)scene->meshes.back());
      deserializeTo(scene->meshes.back());
      break;

    case 110: // transform,
    case 111: // ?
    case 119: // ?,
    case 120: // ik,
    case 121: // jnt,
    case 239: // constrains:
    case 240: // - point,
    case 241: // - symmetry
    case 242: // - parent,
    case 243: // - pole
    case 244: // - scale
    case 245: // - tangent

      scene->transforms.push_back(new TransformNode());
      node = scene->transforms.back();
      deserializeTo((DagNode*)scene->transforms.back());
      deserializeTo((FamilyNode*)scene->transforms.back());
      deserializeTo(scene->transforms.back());
      break;

    case 363:

      scene->lambert.push_back(new LambertNode());
      node = scene->lambert.back();
      deserializeTo((DepNode*)scene->lambert.back());
      impl->reader->ReadBytes(scene->lambert.back()->getPtr(), 1, scene->lambert.back()->getSize());
      break;

    case 365:

      scene->blinns.push_back(new BlinnShaderNode());
      node = scene->blinns.back();
      deserializeTo((DepNode*)scene->blinns.back());
      impl->reader->ReadBytes(scene->blinns.back()->getPtr(), 1, scene->blinns.back()->getSize());
      break;

    case 366:

      scene->phongs.push_back(new PhongShaderNode());
      node = scene->phongs.back();
      deserializeTo((DepNode*)scene->phongs.back());
      impl->reader->ReadBytes(scene->phongs.back()->getPtr(), 1, scene->phongs.back()->getSize());
      break;

    case 673:

      scene->clusters.push_back(new SkinClusterNode());
      node = scene->clusters.back();
      deserializeTo((DepNode*)scene->clusters.back());
      deserializeTo(scene->clusters.back());
      break;

    case 4:

      scene->iks.push_back(new IkHandleNode());
      node = scene->iks.back();
      deserializeTo((DepNode*)scene->iks.back());
      deserializeTo(scene->iks.back());
      break;

    default:

      // if we met the node with the unknown functionality we cannot read any further
      trace("deserializeTo: unsupported api %u", lastApi);
      done = true;
      break;
    }

    if (node) node->api = lastApi;

  }

  // ----------------------------------------------
  safeDelete(impl->reader);
}

uint32_t aega::SceneDeserialize::nextNodeApi()
{
  return impl->reader->NextElement<uint32_t>();
}

void aega::SceneDeserialize::deserializeTo(DagNode *dag)
{
  if (!dag) return;

  impl->reader->ReadString(dag->path);
}

void aega::SceneDeserialize::deserializeTo(DepNode *dep)
{
  if (!dep) return;

  //reader->NextElement<uint32_t>(dep->type);
  impl->reader->ReadString(dep->name);
}

void aega::SceneDeserialize::deserializeTo(FamilyNode *family)
{
  if (!family) return;

  uint32_t count = 0;

  impl->reader->NextElement<uint32_t>(count);
  family->parentPaths.resize(count);
  for (uint32_t i = 0; i != count; ++i)
  {
    impl->reader->ReadString(family->parentPaths[i]);
  }

  impl->reader->NextElement<uint32_t>(count);
  family->childPaths.resize(count);
  for (uint32_t i = 0; i != count; ++i)
  {
    impl->reader->ReadString(family->childPaths[i]);
  }

}

void aega::SceneDeserialize::deserializeTo(MeshNode *mesh)
{
  if (!mesh) return;

  uint32_t s, count;

  s = impl->reader->NextElement<uint32_t>();
  count = impl->reader->NextElement<uint32_t>();
  mesh->points.resize(count);
  impl->reader->NextElements(mesh->points.data(), count);

  s = impl->reader->NextElement<uint32_t>();
  count = impl->reader->NextElement<uint32_t>();
  mesh->normals.resize(count);
  impl->reader->NextElements(mesh->normals.data(), count);

  count = impl->reader->NextElement<uint32_t>();

  mesh->uvSets.resize(count);
  for (uint32_t i = 0; i < count; i++)
  {
    mesh->uvSets[i] = new MeshNode::UVSet;

    impl->reader->ReadString(mesh->uvSets[i]->name);
    uint32_t uvCount = impl->reader->NextElement<uint32_t>();
    mesh->uvSets[i]->uvs.resize(uvCount);
    impl->reader->NextElements(mesh->uvSets[i]->uvs.data(), uvCount);
  }

  count = impl->reader->NextElement<uint32_t>();
  mesh->colors.resize(count);
  impl->reader->NextElements(mesh->colors.data(), count);

  count = impl->reader->NextElement<uint32_t>();
  mesh->faceColors.resize(count);
  impl->reader->NextElements(mesh->faceColors.data(), count);

  count = impl->reader->NextElement<uint32_t>();
  mesh->polygons.resize(count);
  for (uint32_t i = 0; i < count; i++)
  {
    uint32_t triCount = impl->reader->NextElement<uint32_t>();
    mesh->polygons[i].triangles.resize(triCount);
    impl->reader->NextElements(mesh->polygons[i].triangles.data(), triCount);
  }


}

void aega::SceneDeserialize::deserializeTo(TransformNode *transform)
{
  impl->reader->NextElement(transform->translationTS);
  impl->reader->NextElement(transform->scale);
  impl->reader->NextElement(transform->rotationTS);

  if (transform->joint = impl->reader->NextElement<uint8_t>() == 1ui8)
  {
    impl->reader->NextElement(transform->orientation);
  }
  else
  {
    transform->orientation.x
      = transform->orientation.y
      = transform->orientation.z
      = 0.0f;
    transform->orientation.w
      = 1.0f;
  }

  if (transform->animated = impl->reader->NextElement<uint8_t>() == 1ui8)
  {
    uint32_t plugCount = impl->reader->NextElement<uint32_t>();
    transform->plugs.resize(plugCount);

    for (uint32_t plugInd = 0; plugInd < plugCount; plugInd++)
    {
      transform->plugs[plugInd] = new AnimatedPlug();
      impl->reader->ReadString(transform->plugs[plugInd]->name);

      uint32_t curveCount = impl->reader->NextElement<uint32_t>();
      transform->plugs[plugInd]->curves.resize(curveCount);
      for (uint32_t curveInd = 0; curveInd < curveCount; curveInd++)
      {
        impl->reader->ReadString(transform->plugs[plugInd]->curves[curveInd]);
      }
    }
  }
}

void aega::SceneDeserialize::deserializeTo(AnimationCurveNode *curve)
{
  if (uint32_t keyCount = impl->reader->NextElement<uint32_t>())
  {
    curve->keys.resize(keyCount);
    impl->reader->NextElements(curve->keys.data(), keyCount);
  }
}

void aega::SceneDeserialize::deserializeTo(SkinClusterNode *cluster)
{
  uint32_t influenceCount = impl->reader->NextElement<uint32_t>();
  cluster->influences.resize(influenceCount);
  for (uint32_t influenceInd = 0; influenceInd < influenceCount; influenceInd++)
  {
    impl->reader->ReadString(cluster->influences[influenceInd]);
  }

  uint32_t geometryCount = impl->reader->NextElement<uint32_t>();
  cluster->skins.resize(geometryCount);
  for (uint32_t geometryInd = 0; geometryInd < geometryCount; geometryInd++)
  {
    cluster->skins[geometryInd] = new Skin();
    impl->reader->ReadString(cluster->skins[geometryInd]->path);
    
    uint32_t componentCount = impl->reader->NextElement<uint32_t>();
    cluster->skins[geometryInd]->components.resize(componentCount);
    for (uint32_t componentInd = 0; componentInd < componentCount; componentInd++)
    {
      cluster->skins[geometryInd]->components[componentInd] = new SkinComponent();
      cluster->skins[geometryInd]->components[componentInd]->index = impl->reader->NextElement<uint32_t>();

      uint32_t weightCount = impl->reader->NextElement<uint32_t>();
      cluster->skins[geometryInd]->components[componentInd]->weights.resize(weightCount);
      impl->reader->NextElements(cluster->skins[geometryInd]->components[componentInd]->weights.data(), weightCount);
    }
  }

}

void aega::SceneDeserialize::deserializeTo(IkHandleNode *ik)
{
  impl->reader->ReadString(ik->handle);
  impl->reader->ReadString(ik->solver);
  ik->stickness = impl->reader->NextElement<uint32_t>();
  ik->weight = impl->reader->NextElement<float>();
  ik->poWeight = impl->reader->NextElement<float>();
  impl->reader->ReadString(ik->start);
  impl->reader->ReadString(ik->effector);
  impl->reader->NextElement(ik->pole);
}

void aega::Scene::clearAll()
{
  soko::releaseCollectionDeep(clusters, [](SkinClusterNode *cluster)
  {
    soko::releaseCollectionDeep(cluster->skins, [](Skin *skin)
    {
      soko::releaseCollection(skin->components);
    });
  });

  soko::releaseCollectionDeep(transforms, [](TransformNode *transform)
  {
    soko::releaseCollection(transform->plugs);
  });

  soko::releaseCollectionDeep(meshes, [](MeshNode *mesh)
  {
    soko::releaseCollection(mesh->uvSets);
  });

  soko::releaseCollection(blinns);
  soko::releaseCollection(phongs);
  soko::releaseCollection(lambert);
  soko::releaseCollection(iks);
  soko::releaseCollection(animCurves);
}


void aega::SceneAdapter::adaptToCollections(
  MeshNode const &node,
  noekk::VertexCollection &vertices,
  noekk::IndexCollection &indices
  )
{
  size_t index = 0;
  noekk::Vertex vertex;

  for (size_t i = 0; i < node.polygons.size(); i++)
  {
    for (size_t j = 0; j < node.polygons[i].triangles.size(); j++)
    {
      auto &face = node.polygons[i].triangles[j];
      for (size_t p = 0; p < 3; ++p)
      {
        vertex.position = node.points[face.points[p]];
        vertex.normal = node.normals[face.normals[p]];
        vertex.textureCoordinate = node.uvSets[0]->uvs[face.uvSets[0].uvs[p]];
        vertices.push_back(vertex);
        indices.push_back(index++);
      }
    }
  }
}