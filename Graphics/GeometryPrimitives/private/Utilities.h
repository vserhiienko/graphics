#pragma once

namespace soko
{
  static bool hasEnding(std::string const &fullString, std::string const &ending)
  {
    if (fullString.length() >= ending.length())
      return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    else return false;
  }

  template <typename _Ptrty>
  static void ReleasePointer(_Ptrty *h)
  {
    if (h)
    {
      delete h;
      h = nullptr;
    }
  }

  template <typename _Ptrty>
  static void ReleaseArrayPointer(_Ptrty *h)
  {
    if (h)
    {
      delete[] h;
      h = nullptr;
    }
  }

  template <typename _Ptrty>
  static void ReleaseVector(std::vector <_Ptrty*> &h)
  {
    for (UINT32 i = 0; i < h.size(); i++)
    {
      ReleasePointer(h.at(i));
    }
  }

  template <typename _Ptrty>
  static void ReleaseVector(std::vector <_Ptrty*> *&h)
  {
    if (h) for (UINT32 i = 0; i < h->size(); i++)
    {
      ReleasePointer(h->at(i));
    }
  }

  template <typename _Ty, typename _DeleteElement>
  static void releaseCollectionDeep(std::vector<_Ty*> &collection, _DeleteElement del)
  {
    for (size_t i = 0; i < collection.size(); i++)
      del(collection[i]), delete collection[i], collection[i] = 0;
    collection.clear();
  }

  template <typename _Ty>
  static void releaseCollection(std::vector<_Ty*> &collection)
  {
    for (size_t i = 0; i < collection.size(); i++)
      delete collection[i], collection[i] = 0;
    collection.clear();
  }

  inline static void trace(const char* fmt, _In_opt_ ...)
  {
    const int length = 512;
    char buffer[length];
    va_list ap;

    va_start(ap, fmt);
    vsnprintf_s(buffer, length - 1, fmt, ap);
    OutputDebugStringA(buffer);
    OutputDebugStringA("\n");
    va_end(ap);
  }

}