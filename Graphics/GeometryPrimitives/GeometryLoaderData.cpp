
#include <iostream>
#include <Windows.h>

#include "GeometryLoader.h"

namespace soko
{
  template <typename _Ptrty>
  static void ReleasePointer(_Ptrty *h)
  {
    if (h)
    {
      delete h;
      h = nullptr;
    }
  }

  template <typename _Ptrty>
  static void ReleaseArrayPointer(_Ptrty *h)
  {
    if (h)
    {
      delete[] h;
      h = nullptr;
    }
  }

  template <typename _Ptrty>
  static void ReleaseVector(std::vector <_Ptrty*> &h)
  {
    for (UINT32 i = 0; i < h.size(); i++)
    {
      ReleasePointer(h.at(i));
    }
  }

  template <typename _Ptrty>
  static void ReleaseVector(std::vector <_Ptrty*> *&h)
  {
    if (h) for (UINT32 i = 0; i < h->size(); i++)
    {
      ReleasePointer(h->at(i));
    }
  }

}

soko::GeometryLoader::Item::Type soko::GeometryLoader::Item::GetType() { return m_type; }
std::string &soko::GeometryLoader::Item::getNameByRef() { return m_name; }
std::string  soko::GeometryLoader::Item::getNameCopy() { return m_name; }
std::string *soko::GeometryLoader::Item::GetName() { return &m_name; }

soko::GeometryLoader::Item::Item() 
  : m_type(kInvalid)
  , m_name("") 
{ 
}

soko::GeometryLoader::Item::~Item() 
{ 
}

std::vector<soko::GeometryLoader::Handle> &soko::GeometryLoader::Node::getHandlesByRef(void) { return m_handles; }
std::vector<soko::GeometryLoader::Node*> &soko::GeometryLoader::Node::getParentsByRef(void) { return m_adults; }
std::vector<soko::GeometryLoader::Node*> &soko::GeometryLoader::Node::getKidsByRef(void) { return m_kids; }
std::vector<soko::GeometryLoader::Handle> *soko::GeometryLoader::Node::getHandles(void) { return &m_handles; }
std::vector<soko::GeometryLoader::Node*> *soko::GeometryLoader::Node::getParents(void) { return &m_adults; }
std::vector<soko::GeometryLoader::Node*> *soko::GeometryLoader::Node::getKids(void) { return &m_kids; }

soko::GeometryLoader::Handle soko::GeometryLoader::Node::getHandle(std::string name)
{
  for (auto &n : m_handles) if (n->getNameByRef() == name) return n;
  return nullptr;
}

soko::GeometryLoader::Handle soko::GeometryLoader::Node::getHandle(std::string name, Item::Type type)
{
  for (auto &n : m_handles) if (n->getNameByRef() == name && type == n->m_type) return n;
  return nullptr;
}

soko::GeometryLoader::Handle soko::GeometryLoader::Node::getHandle(UINT32 index)
{
  if (index < m_handles.size()) return m_handles.at(index);
  else return nullptr;
}

soko::GeometryLoader::Node *soko::GeometryLoader::Node::getParentNode(std::string name, bool returnNew)
{
  for (auto &n : m_adults) if (n->getNameByRef() == name) return n;
  if (returnNew) { auto n = new Node(); m_adults.push_back(n); n->m_name = name; return n; }
  else return nullptr;
}

soko::GeometryLoader::Node *soko::GeometryLoader::Node::getKidNode(std::string name, bool returnNew)
{
  for (auto &n : m_kids) if (n->getNameByRef() == name) return n;
  if (returnNew) { auto n = new Node(); m_kids.push_back(n); n->m_name = name; return n; }
  else return nullptr;
}

soko::GeometryLoader::Node::Node() 
{ 
  m_handles.clear(); 
  m_adults.clear();
  m_kids.clear(); 
}

soko::GeometryLoader::Node::~Node() 
{
}

soko::GeometryLoader::Shape::Polygon::Vertex::Tex::Tex() 
  : uvIndex(0)
{
}

soko::GeometryLoader::Shape::Polygon::Vertex::~Vertex()
{
  ReleaseArrayPointer(texCoords);
}

soko::GeometryLoader::Shape::Polygon::Vertex::Vertex() 
  : texCoordCount(0)
  , texCoords(0)
  , pointIndex(0)
  , normalIndex(0)
{
}

soko::GeometryLoader::Shape::Polygon::Polygon() 
  : vertexCount(0)
  , vertices(0)  
{
}

soko::GeometryLoader::Shape::Polygon::~Polygon() 
{
  ReleaseArrayPointer(vertices);
}

soko::GeometryLoader::Shape::UVSet::~UVSet()
{
  uvs.clear(); 
  binormals.clear(); 
  tangents.clear();
}

soko::GeometryLoader::Shape::UVSet::UVSet()
{
  uvs.clear(); 
  binormals.clear(); 
  tangents.clear();
}

soko::GeometryLoader::Shape::VectorsInSpace::~VectorsInSpace()  
{ 
  ReleaseArrayPointer(vectors); 
}

soko::GeometryLoader::Shape::VectorsInSpace::VectorsInSpace() 
  : vectors(0)
  , count(0)
{ 
}

soko::GeometryLoader::Shape::~Shape()
{
  ReleaseVector(points);
  points->clear();
  ReleasePointer(points);

  ReleaseVector(normals);
  normals->clear();
  ReleasePointer(normals);

  ReleaseVector(polygons);
  ReleaseVector(sets);
}

soko::GeometryLoader::Shape::Shape() 
  : points(0)
  , normals(0)
{
}

soko::GeometryLoader::Transform::Curve::Key::Key() 
  : t(0)
  , value(0)
  , tangentIn(-1)
  , tangentOut(-1)
{
}

soko::GeometryLoader::Transform::Curve::~Curve()
{
  ReleaseArrayPointer(keys);
}

soko::GeometryLoader::Transform::Curve::Curve() 
  : keys(NULL)
  , keyCount(0)
{
}

soko::GeometryLoader::Transform::~Transform()
{
  if (curves) ReleaseVector(curves);
}

soko::GeometryLoader::Transform::Transform() 
  : curves(NULL)
{
  isJoint = false;
}

soko::GeometryLoader::SkinCluster::Skin::Component::Influence::Influence()
  : weight(0)
{
}

soko::GeometryLoader::SkinCluster::Skin::Component::~Component()
{
  ReleaseArrayPointer(influences);
}

soko::GeometryLoader::SkinCluster::Skin::Component::Component() 
  : influences(0)
  , influenceCount(0)
{
}

soko::GeometryLoader::SkinCluster::Skin::~Skin()
{
  ReleaseArrayPointer(components);
}

soko::GeometryLoader::SkinCluster::Skin::Skin() 
  : components(0)
  , componentCount(0)
{
}

soko::GeometryLoader::SkinCluster::~SkinCluster()
{
  Geometries.clear();
}

soko::GeometryLoader::SkinCluster::SkinCluster()
{
}

