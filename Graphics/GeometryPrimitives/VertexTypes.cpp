#include "VertexTypes.h"

noekk::VertexPositionNormalTexture::VertexPositionNormalTexture()
{
}

noekk::VertexPositionNormalTexture::VertexPositionNormalTexture(
  DirectX::XMFLOAT3 const & position,
  DirectX::XMFLOAT3 const & normal,
  DirectX::XMFLOAT2 const & textureCoordinate
  )
  : position(position)
  , normal(normal)
  , textureCoordinate(textureCoordinate)
{
}

noekk::VertexPositionNormalTexture::VertexPositionNormalTexture(
  DirectX::FXMVECTOR position, 
  DirectX::FXMVECTOR normal, 
  DirectX::FXMVECTOR textureCoordinate
  )
{
  XMStoreFloat3(&this->position, position);
  XMStoreFloat3(&this->normal, normal);
  XMStoreFloat2(&this->textureCoordinate, textureCoordinate);
}

// Use >=, not > comparison, because some D3D level 9_x hardware does not support 0xFFFF index values.

bool noekk::IndexCollection::checkIndexOverflowSilent(size_t value)
{
  if (value >= USHRT_MAX) return false; else return true;
}

void noekk::IndexCollection::checkIndexOverflow(size_t value)
{
  if (value >= USHRT_MAX) throw std::exception("checkIndexOverflow: index value out of range (cannot tesselate primitive so finely)");
}

void noekk::IndexCollection::push_back(size_t value)
{
  checkIndexOverflow(value); // give`em hell, kid
  vector::push_back((uint16_t)value);
}

