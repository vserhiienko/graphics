#include <Skeleton.h>


void soko::Skeleton::Joint::updateKinderTransformWS()
{
  uint32_t jointInd = 0;
  uint32_t jointCount = (uint32_t)Joint::kids.size();

  auto currentJoint = Joint::kids[0];
  for (; jointInd < jointCount; jointInd++, currentJoint++)
    currentJoint->updateTransformWS();
}

void soko::Skeleton::Joint::updateKinderTransformOS(float time)
{
  uint32_t jointInd = 0;
  uint32_t jointCount = (uint32_t)Joint::kids.size();

  auto currentJoint = Joint::kids[0];
  for (; jointInd < jointCount; jointInd++, currentJoint++)
    currentJoint->updateTransformOS(time);
}

DirectX::XMMATRIX soko::Skeleton::Joint::calculateTransformMatrixWS()
{
  auto rotateWSM = calculateRotationWSM();
  auto translationWSM = calculateTranslationWSM();
  return XMMatrixMultiply(rotateWSM, translationWSM);
}

DirectX::XMMATRIX soko::Skeleton::Joint::calculateTransformMatrixInverseWS()
{
  auto rotationWSM = calculateRotationTransposeWSM();

  auto translationWSV = calculateTranslationWSV();
  translationWSV = XMVector3Transform(translationWSV, rotationWSM);
  translationWSV = XMVectorNegate(translationWSV);

  auto translationWSM = XMMatrixTranslationFromVector(translationWSV);
  return XMMatrixMultiply(rotationWSM, translationWSM);
}

void soko::Skeleton::Joint::updateTransformWS()
{
  transformWS.translation = parent->transformWS.translation; // store!

  XMVECTOR translationOSV = calculateTranslationOSV(); 
  XMVECTOR translationWSV = calculateTranslationWSV(); 
  XMVECTOR parentRotationWSQ = calculateParentRotationWSQ(); 
  translationWSV = XMVectorAdd(translationWSV, XMVector3Rotate(translationOSV, parentRotationWSQ));
  XMStoreFloat4(&transformWS.translation, translationWSV); // store!

  XMVECTOR rotationOSQ = calculateRotationOSQ();
  XMVECTOR orientationOSQ = calculateOrientationOSQ();
  XMVECTOR rotationWSQ = XMQuaternionMultiply(rotationOSQ, XMQuaternionMultiply(orientationOSQ, parentRotationWSQ));
  XMStoreFloat4(&transformWS.rotation, rotationWSQ); // store!

  updateKinderTransformWS();
}

void soko::Skeleton::Joint::updateTransformOS(float time)
{
  transformOS.resolveSX(time);
  updateKinderTransformOS(time);
}

void soko::Skeleton::Handle::updateTransformWS()
{
  transformWS.translation = transformOS.translation;

  XMVECTOR rotationOSQ = calculateRotationOSQ();
  XMVECTOR orientationOSQ = calculateOrientationOSQ();
  XMVECTOR rotationWSQ = XMQuaternionMultiply(rotationOSQ, orientationOSQ);

  XMStoreFloat4(&transformWS.rotation, rotationWSQ);

  Joint::updateKinderTransformWS();
}