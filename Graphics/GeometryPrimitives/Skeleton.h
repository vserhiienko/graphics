#pragma once

#include <vector>
#include <Transform.h>

namespace soko
{
  using namespace DirectX;

  struct Skeleton
  {
    struct Joint
    {
      friend Skeleton;
      typedef std::vector<Joint*> Vector;
      typedef Vector::iterator Iterator;

      std::string name;

      soko::Transform transformOS;
      soko::Transform transformWS;

      Joint *orig;
      Joint *parent;
      Joint::Vector kids;

      Joint(void);
      ~Joint(void);

      inline bool isAffector(void) { return kids.empty(); }
      inline bool isHandle(void) { return parent == nullptr; }

      inline XMMATRIX calculateTranslationWSM(void) { return XMMatrixTranslationFromVector(XMLoadFloat4(&transformWS.translation)); }
      inline XMMATRIX calculateTranslationOSM(void) { return XMMatrixTranslationFromVector(XMLoadFloat4(&transformOS.translation)); }
      inline XMVECTOR calculateTranslationWSV(void) { return XMLoadFloat4(&transformWS.translation); }
      inline XMVECTOR calculateTranslationOSV(void) { return XMLoadFloat4(&transformOS.translation); }
      inline XMMATRIX calculateParentTranslationWSM(void) { return XMMatrixTranslationFromVector(XMLoadFloat4(&parent->transformWS.translation)); }
      inline XMMATRIX calculateParentTranslationOSM(void) { return XMMatrixTranslationFromVector(XMLoadFloat4(&parent->transformOS.translation)); }
      inline XMVECTOR calculateParentTranslationWSV(void) { return XMLoadFloat4(&parent->transformWS.translation); }
      inline XMVECTOR calculateParentTranslationOSV(void) { return XMLoadFloat4(&parent->transformOS.translation); }
      inline XMMATRIX calculateRotationTransposeWSM(void) { return XMMatrixTranspose(XMMatrixRotationQuaternion(XMLoadFloat4(&transformWS.rotation))); }
      inline XMMATRIX calculateRotationTransposeOSM(void) { return XMMatrixTranspose(XMMatrixRotationQuaternion(XMLoadFloat4(&transformOS.rotation))); }
      inline XMMATRIX calculateParentRotationTransposeWSM(void) { return XMMatrixTranspose(XMMatrixRotationQuaternion(XMLoadFloat4(&parent->transformWS.rotation))); }
      inline XMMATRIX calculateParentRotationTransposeOSM(void) { return XMMatrixTranspose(XMMatrixRotationQuaternion(XMLoadFloat4(&parent->transformOS.rotation))); }
      inline XMMATRIX calculateRotationWSM(void) { return XMMatrixRotationQuaternion(XMLoadFloat4(&transformWS.rotation)); }
      inline XMMATRIX calculateRotationOSM(void) { return XMMatrixRotationQuaternion(XMLoadFloat4(&transformOS.rotation)); }
      inline XMMATRIX calculateParentRotationWSM(void) { return XMMatrixRotationQuaternion(XMLoadFloat4(&parent->transformWS.rotation)); }
      inline XMMATRIX calculateParentRotationOSM(void) { return XMMatrixRotationQuaternion(XMLoadFloat4(&parent->transformOS.rotation)); }
      inline XMVECTOR calculateRotationWSQ(void) { return XMLoadFloat4(&transformWS.rotation); }
      inline XMVECTOR calculateRotationOSQ(void) { return XMLoadFloat4(&transformOS.rotation); }
      inline XMVECTOR calculateParentRotationWSQ(void) { return XMLoadFloat4(&parent->transformWS.rotation); }
      inline XMVECTOR calculateParentRotationOSQ(void) { return XMLoadFloat4(&parent->transformOS.rotation); }
      inline XMVECTOR calculateOrientationWSQ(void) { return XMLoadFloat4(&transformWS.orientation); }
      inline XMVECTOR calculateOrientationOSQ(void) { return XMLoadFloat4(&transformOS.orientation); }
      inline XMVECTOR calculateParentOrientationWSQ(void) { return XMLoadFloat4(&parent->transformWS.orientation); }
      inline XMVECTOR calculateParentOrientationOSQ(void) { return XMLoadFloat4(&parent->transformOS.orientation); }

      XMMATRIX calculateTransformMatrixWS(void);
      XMMATRIX calculateTransformMatrixInverseWS(void);

    private:
      void updateTransformWS(void);
      void updateKinderTransformWS(void);
      void updateTransformOS(float time);
      void updateKinderTransformOS(float time);

    };

    struct Handle : public Joint
    {
      void updateTransformWS();
      inline void update(float time)
      {
        updateTransformOS(time);
        updateTransformWS();
      }
    };

    Joint::Vector joints;
    Handle *rootJoint;

    Skeleton(void);
    ~Skeleton(void);

  };

  typedef Skeleton::Joint SkeletonJoint;
  typedef Skeleton::Handle SkeletonHandle;
}


