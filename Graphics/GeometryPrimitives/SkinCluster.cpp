
#include <iostream>
#include <Windows.h>

#include <SkinCluster.h>
#include <private/Utilities.h>

soko::Skin::Cluster::Influence::Influence()
  : joint(nullptr)
  , weight(0.0f)
{
}

soko::Skin::Cluster::Component::Component()
{
}

soko::Skin::Cluster::Component::~Component()
{
  if (influences.size())
  {
    for (auto p : influences) ReleasePointer(p);
    influences.clear();
  }
}

soko::Skin::Cluster::Cluster()
{
}

soko::Skin::Cluster::~Cluster()
{
  if (components.size())
  {
    for (auto p : components) ReleasePointer(p);
    components.clear();
  }
}

soko::Skin::Skin()
  : cluster(nullptr)
  , posedSkeleton(nullptr)
  , initialSkeleton(nullptr)
{
}

soko::Skin::~Skin()
{
}

void soko::Skin::pose(float time)
{
  using namespace DirectX;

  posedSkeleton->rootJoint->update(time);

  Skeleton::Joint *joint = 0;
  Skeleton::Joint *jointOrig = 0;
  Skeleton::Joint **joints = posedSkeleton->joints.data();
  Skeleton::Joint **jointsOrig = initialSkeleton->joints.data();

  Skin::Cluster *skinCluster = cluster;
  Skin::Cluster::Influence *influence = 0;
  Skin::Cluster::Influence **influences = 0;

  uint32_t jointIndex = 0;
  uint32_t pointIndex = 0;
  uint32_t normalIndex = 0;
  uint32_t influenceIndex = 0;
  uint32_t influenceCount = 0;
  uint32_t jointCount = (uint32_t) initialSkeleton->joints.size();
  uint32_t pointCount = (uint32_t) skinCluster->components.size();
  uint32_t normalCount = (uint32_t) normals.size();

  DirectX::XMFLOAT4 vectorOS;
  DirectX::XMVECTOR vectorBP;
  DirectX::XMMATRIX jointTransform;
  DirectX::XMMATRIX totalJointTransform;

  DirectX::XMStoreFloat4(&vectorOS, g_XMIdentityR3.v);
  vectorBP = g_XMIdentityR3.v;

  DirectX::XMMATRIX *jointRotations = new DirectX::XMMATRIX[jointCount];
  DirectX::XMMATRIX *jointTransformations = new DirectX::XMMATRIX[jointCount];

  for (jointIndex = 0; jointIndex < jointCount; jointIndex++)
  {
    joint = joints[jointIndex];
    jointOrig = jointsOrig[jointIndex];

    jointTransformations[jointIndex] = DirectX::XMMatrixMultiply(
      jointOrig->calculateTransformMatrixInverseWS(),
      joint->calculateTransformMatrixWS()
      );
    jointRotations[jointIndex] = DirectX::XMMatrixMultiply(
      jointOrig->calculateRotationTransposeWSM(), 
      joint->calculateRotationWSM()
      );
  }

  for (pointIndex = 0; pointIndex < pointCount; pointIndex++)
  {
    influenceIndex = 0;
    influenceCount = (uint32_t) skinCluster->components[pointIndex]->influences.size();
    influences = skinCluster->components[pointIndex]->influences.data();

    influence = influences[influenceIndex];
    jointTransform = jointTransformations[influenceIndex] * influence->weight;
    totalJointTransform = jointTransform;

    for (influenceIndex = 1; influenceIndex < influenceCount; influenceIndex++)
    {
      influence = influences[influenceIndex];
      jointTransform = jointTransformations[influenceIndex] * influence->weight;
      totalJointTransform += jointTransform;
    }

    (XMFLOAT3&)vectorOS = mesh->points[pointIndex];
    vectorBP = XMVector3Transform(XMLoadFloat3((XMFLOAT3*)&vectorOS), totalJointTransform);

    XMStoreFloat4(&points[pointIndex], vectorBP);
  }

  for (normalIndex = 0, pointIndex = mesh->n2pMap[normalIndex]; normalIndex < normalCount; normalIndex++)
  {
    influenceIndex = 0;
    influenceCount = (uint32_t) skinCluster->components[pointIndex]->influences.size();
    influences = skinCluster->components[pointIndex]->influences.data();

    influence = influences[influenceIndex];
    jointTransform = jointRotations[influenceIndex] * influence->weight;
    totalJointTransform = jointTransform;

    for (influenceIndex = 1; influenceIndex < influenceCount; influenceIndex++)
    {
      influence = influences[influenceIndex];
      jointTransform = jointRotations[influenceIndex] * influence->weight;
      totalJointTransform += jointTransform;
    }

    (XMFLOAT3&)vectorOS = mesh->normals[normalIndex];
    vectorBP = XMVector3Transform(XMLoadFloat3((XMFLOAT3*)&vectorOS), totalJointTransform);

    XMStoreFloat3(&normals[normalIndex], vectorBP);
  }

  delete[] jointTransformations;
  delete[] jointRotations;
}

