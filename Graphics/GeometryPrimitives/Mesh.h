#pragma once

#include <VertexTypes.h>

namespace soko
{
  struct Mesh
  {
    struct VertexIndices
    {
      uint32_t p, n, t;
      VertexIndices(void);
      VertexIndices(uint32_t value);
      VertexIndices(uint32_t p, uint32_t n, uint32_t t);

      typedef VertexIndices Triangle[3];
    };

    struct Face
    {
      VertexIndices::Triangle triangle;
      Face(void);
      Face(VertexIndices v0, VertexIndices v1, VertexIndices v2);

      typedef std::vector<Face> Vector;
    };

    Face::Vector faces;
    std::vector<DirectX::XMFLOAT3> points;
    std::vector<DirectX::XMFLOAT3> normals;
    std::vector<DirectX::XMFLOAT2> uvs;
    std::vector<uint32_t> n2pMap;

  };
}

