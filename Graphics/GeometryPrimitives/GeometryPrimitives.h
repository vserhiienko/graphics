#pragma once

#include <VertexTypes.h>

namespace noekk
{
  class GeometricPrimitives
  {
  public:
    typedef Vertex::Collection VertexCollection;
    typedef noekk::Index Index;
    typedef noekk::IndexCollection IndexCollection;
    static const size_t indexByteSize = IndexCollection::indexByteSize;

  public:
    static void checkIndexOverflow(size_t value);
    static DirectX::XMVECTOR getCircleVector(size_t i, size_t tessellation);
    static DirectX::XMVECTOR getCircleTangent(size_t i, size_t tessellation);

  public:
    static void createPlane(VertexCollection &vertices, IndexCollection &indices, float sizeX = 1, float sizeY = 1, bool rhcoords = true);
    static void createCube(VertexCollection &vertices, IndexCollection &indices, float size = 1, bool rhcoords = true);
    static void createSphere(VertexCollection &vertices, IndexCollection &indices, float diameter = 1, size_t tessellation = 16, bool rhcoords = true);
    static void createGeoSphere(VertexCollection &vertices, IndexCollection &indices, float diameter = 1, size_t tessellation = 3, bool rhcoords = true);
    static void createCylinder(VertexCollection &vertices, IndexCollection &indices, float height = 1, float diameter = 1, size_t tessellation = 32, bool rhcoords = true);
    static void createCylinderCap(VertexCollection& vertices, IndexCollection& indices, size_t tessellation, float height, float radius, bool isTop);
    static void createCone(VertexCollection &vertices, IndexCollection &indices, float diameter = 1, float height = 1, size_t tessellation = 32, bool rhcoords = true);
    static void createTorus(VertexCollection &vertices, IndexCollection &indices, float diameter = 1, float thickness = 0.333f, size_t tessellation = 32, bool rhcoords = true);
    static void createTetrahedron(VertexCollection &vertices, IndexCollection &indices, float size = 1, bool rhcoords = true);
    static void createOctahedron(VertexCollection &vertices, IndexCollection &indices, float size = 1, bool rhcoords = true);
    static void createDodecahedron(VertexCollection &vertices, IndexCollection &indices, float size = 1, bool rhcoords = true);
    static void createIcosahedron(VertexCollection &vertices, IndexCollection &indices, float size = 1, bool rhcoords = true);

  };

} // ns noekk