#pragma once

#include <vector>
#include <DirectXMath.h>

namespace noekk
{
  // Vertex struct holding position, normal vector, and texture mapping information.
  typedef struct VertexPositionNormalTexture
  {
    typedef std::vector<VertexPositionNormalTexture> Collection;

    DirectX::XMFLOAT3 position;
    DirectX::XMFLOAT3 normal;
    DirectX::XMFLOAT2 textureCoordinate;

    VertexPositionNormalTexture(void);
    VertexPositionNormalTexture(DirectX::FXMVECTOR position, DirectX::FXMVECTOR normal, DirectX::FXMVECTOR textureCoordinate);
    VertexPositionNormalTexture(DirectX::XMFLOAT3 const& position, DirectX::XMFLOAT3 const& normal, DirectX::XMFLOAT2 const& textureCoordinate);

    static const size_t strideByteSize;
    static const unsigned inputElementCount = 3;

#if defined(__d3d11_h__)
    inline static void setD3DInputElements(D3D11_INPUT_ELEMENT_DESC(&d3d11InputElements)[inputElementCount])
    {
      static const D3D11_INPUT_ELEMENT_DESC inputElements[inputElementCount] =
      {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
      };

      memcpy(d3d11InputElements, inputElements, sizeof(inputElements));
    }
#endif

#if defined(__d3d12_h__)
    inline static void setD3DInputElements(D3D12_INPUT_ELEMENT_DESC(&d3d12InputElements)[inputElementCount])
    {
      static const D3D12_INPUT_ELEMENT_DESC inputElements[inputElementCount] =
      {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_PER_VERTEX_DATA, 0 },
      };

      memcpy(d3d12InputElements, inputElements, sizeof(inputElements));
    }
#endif

  } Vertex;

  typedef Vertex::Collection VertexCollection;

  typedef unsigned short Index;

  struct IndexCollection : public std::vector<Index>
  {
    static const size_t indexByteSize = sizeof(Index);
    static void checkIndexOverflow(size_t);
    static bool checkIndexOverflowSilent(size_t);
    void push_back(size_t); // Sanity check the range of 16 bit index values.
  };
}


