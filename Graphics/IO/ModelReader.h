#pragma once
#include <iostream>
#include <memory>
#include <stdint.h>

#include "BasicIO.h"

namespace Visco3D 
{
	class Model;

	class BasicReader
	{
	protected:
        FileWrapper::ShPtr m_file;
        std::unique_ptr<IStreamConsumer> streamConsumer;

	public:
		BasicReader(FileWrapper::ShPtr file);
		virtual ~BasicReader();

	public:
		virtual bool ReadBytes(void *data, size_t elementSize, size_t elementNum);
		bool ReadString(std::wstring &);
		bool ReadUInt32_7BitEnc(uint32_t &);
		bool ReadInt32_7BitEnc(int32_t &);
		bool ReadID32_7BitEnc(int32_t &);
		bool ReadUInt16(uint16_t &);
		bool ReadUInt32(uint32_t &);
		bool ReadInt32(int32_t &);
		bool ReadID32(int32_t &);

	};

	class CompressedReader
		: public BasicReader
	{
	public:
        CompressedReader(FileWrapper::ShPtr file);
		virtual ~CompressedReader();
	public:
		virtual bool ReadBytes(void *data, size_t elementSize, size_t elementNum) override;

	};

	class ModelFileReader
	{
		static bool CheckSign(BasicReader &reader);
		static int32_t ReadFileVersion(BasicReader &reader);
		static bool IsVersionSupported(int32_t modelFileVersion);
		static bool Is7BitEncEnabled(int32_t modelFileVersion);

	public:
		enum LoadError
		{
			eLoadError_Success = 0,
			eLoadError_SignMismatch = -1,
			eLoadError_DeprecatedVersion = -2,
		};

		inline static bool Succeeded(LoadError err) { return err >= 0; }
		inline static bool Failed(LoadError err) { return err < 0; }

	public:
		class IBinaryReader
		{
		protected:
			// can be either a normal or a compressed reader
			// see: ProceedAsCompressed()
			BasicReader *m_reader;
			ModelFileReader &m_streamer;

		public:
			typedef std::shared_ptr<IBinaryReader> ShPtr;

		public:
			IBinaryReader(ModelFileReader &streamer);
            virtual ~IBinaryReader(void);
            virtual int32_t NextId32(void) = 0;
            virtual uint32_t NextUInt32(void) = 0;
            virtual int32_t NextInt32(void) = 0;
			virtual std::wstring NextString(void);

			inline BasicReader *GetUnderlyingReader() { return m_reader; }
			template <typename _PodTy> _PodTy NextElement() { _PodTy outValue; m_reader->ReadBytes(&outValue, sizeof(_PodTy), 1); return outValue; }
			template <typename _PodTy> void NextElement(_PodTy &dataRef) { m_reader->ReadBytes(&dataRef, sizeof(_PodTy), 1); }
			template <typename _PodTy> void NextElements(_PodTy *dataPtr, size_t arraySize) { m_reader->ReadBytes(dataPtr, sizeof(_PodTy), arraySize); }

			void ProceedAsCompressed(void);
		};

	public:
		class IReaderStrategy
		{
		protected:
			ModelFileReader &m_streamer;

		public:
			typedef std::shared_ptr<IReaderStrategy> ShPtr;
			IReaderStrategy(ModelFileReader &streamer);
			virtual bool Read(Model &model) = 0;
		};
		friend IReaderStrategy;

	public:
		ModelFileReader(void);
		LoadError Load(FileWrapper::ShPtr file, Model &model);

	public:
		inline FileWrapper::FileHandle GetFileHandle(void) const { return m_file->GetHandle(); }
		inline IBinaryReader::ShPtr GetFileReader(void) { return m_reader; }
		inline int32_t GetFileVersion(void) const { return m_fileV; }

	protected:
		void BuildStrategies(void);

	protected:
		int32_t m_fileV; // model file version
		FileWrapper::ShPtr m_file; // file to read the model from
		IBinaryReader::ShPtr m_reader; // reader (either a normal or encoded integers)

		// file-version-dependent reader strategies
		//IReaderStrategy::ShPtr m_flagsRS;
		//IReaderStrategy::ShPtr m_materialRS;
		//IReaderStrategy::ShPtr m_compressedFlagRS;

		void ReadModelFlags(Model&);
		void ReadIdRanges(Model&);
		void HandleZCompressionFlag(Model&);
		void ReadMaterials(Model&);
		void ReadMeshVertexLayouts(Model&);
		void ReadMeshes(Model&);
		void ReadNodes(Model&);
		void ReadAnimations(Model&);
		void ReadSkeletons(Model&);
		void FinalizeLoading(Model&);

		void ReadMaterial(int id, Model&);
		void ReadMeshVertexLayout(int id, Model&);
		void ReadMesh(int id, Model&);
		void ReadNode(Model::Node&, Model&);
		void ReadMeshLinkGeometry(Model::Node&, Model::MeshLink&, Model&);
		void ReadAnimation(Model&);
		
	};
}