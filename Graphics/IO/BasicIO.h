#pragma once
#include <iostream>
#include <memory>
#include <stdint.h>

namespace Visco3D
{
    class File
    {
    public:
        typedef FILE *FileHandle;
        typedef std::shared_ptr<File> ShPtr;

    public:
        File(void);
        ~File(void);

    public:
        bool Read(const wchar_t *pathToFile);
        bool Write(const wchar_t *pathToFile);
        bool ErrorOccurred(void) const;
        bool ClearError(void);
        bool TryClose(void);

    public:
        inline FileHandle GetHandle(void) const { return fileHandle; }
        inline std::wstring const &GetFullPath(void) const { return fullPath; }

    public:
        static ShPtr OpenFileForReading(const wchar_t *pathToFile);
        static ShPtr OpenFileForWritting(const wchar_t *pathToFile);

    private:
        mutable FileHandle fileHandle;
        std::wstring fullPath;
    };

    typedef File FileWrapper;

    class Blob
    {
        bool wrapper;
        size_t dataSz;
        std::unique_ptr<uint8_t[]> data;

    public:
        typedef std::shared_ptr<Blob> ShPtr;
        typedef std::shared_ptr<Blob const> ShConstPtr;
        typedef std::unique_ptr<uint8_t[]> UnPtr;

    public:
        Blob();
        ~Blob();

    public:
        inline bool Good(void) const { return dataSz != 0; }
        inline size_t Size(void) const { return dataSz; }
        inline uint8_t *Data(void) { return data.get(); }
        inline uint8_t const *Data(void) const { return data.get(); }

    public:
        bool Initialize(size_t dataSz);
        void Wrap(uint8_t *data, size_t dataSz);
        void Copy(uint8_t const *data, size_t dataSz);
        void Clear(void);

    public:
        static ShPtr CreatePtr(size_t dataSz = 0);
        static ShPtr CreatePtr(std::wstring const &filePath, size_t offset = 0, size_t size = 0);
        static ShPtr CreatePtr(FileWrapper::ShPtr fileWrapper, size_t offset = 0, size_t size = 0);

    };

    class IStreamConsumer
    {
    public:
        virtual ~IStreamConsumer() { }
        virtual bool Good(void) const = 0;
        virtual size_t Tell(void) const = 0;
        virtual size_t Consume(uint8_t *data, size_t dataSz) = 0;
    };

    class IStreamProducer
    {
    public:
        virtual ~IStreamProducer() { }
        virtual void Set(size_t) { }
        virtual size_t Tell(void) const { return 0; }
        virtual size_t Append(uint8_t const *data, size_t dataSz) = 0;
    };

    class BlobConsumer : public IStreamConsumer
    {
    protected:
        size_t currentPos;
        Blob const &blob;

    public:
        BlobConsumer(Blob const &blob);
        BlobConsumer(Blob const &blob, IStreamConsumer const &previousConsumer);

    public:
        void Reset() { currentPos = 0; }
        virtual bool Good() const override;
        virtual size_t Tell() const override;
        virtual size_t Consume(uint8_t *data, size_t dataSz) override;
    };

    class FileConsumer : public IStreamConsumer
    {
    protected:
        File const &file;

    public:
        FileConsumer(File const &file);
        FileConsumer(File const &file, IStreamConsumer const &previousConsumer);

    public:
        virtual bool Good(void) const override;
        virtual size_t Tell(void) const override;
        virtual size_t Consume(uint8_t *data, size_t dataSz) override;

    };

    class BlobProducer : public IStreamProducer
    {
    protected:
        Blob &blob;
        std::ostringstream ss;
        void Finalize(void);

    public:
        BlobProducer(Blob &blob);
        virtual ~BlobProducer(void);

    public:
        virtual size_t Append(uint8_t const *data, size_t dataSz) override;

    };

    class FileProducer : public IStreamProducer
    {
    protected:
        File &file;

    public:
        FileProducer(File &file);
        virtual ~FileProducer(void);

    public:
        virtual void Set(size_t) override;
        virtual size_t Tell(void) const override;
        virtual size_t Append(uint8_t const *data, size_t dataSz) override;

    };

    class IZStreamConsumer : public IStreamConsumer
    {
        struct ZContext;
        friend ZContext;
        ZContext *impl;

    protected:
        virtual size_t OnNextCompressedChunk(Blob &compressedData) = 0;
        int InitializeDecompression(size_t chunkSize, IStreamConsumer *consumer);
        int DecompressChunk(size_t &);
        int FinalizeDecompression(void);

    public:
        static uint32_t const kChunkSize_16Kb = 16 * 1024;
        static uint32_t const kChunkSize_32Kb = 32 * 1024;
        static uint32_t const kChunkSize_64Kb = 64 * 1024;
        static uint32_t const kChunkSize_Default = kChunkSize_16Kb;

    public:
        IZStreamConsumer(void);
        virtual ~IZStreamConsumer(void);

    public:
        virtual bool Good(void) const override;
        virtual size_t Tell(void) const override;
        virtual size_t Consume(uint8_t *data, size_t dataSz) override;

    };

    class IZStreamProducer : public IStreamProducer
    {
        struct ZContext;
        friend ZContext;
        ZContext *impl;

    protected:
        virtual void OnNextCompressedChunk(Blob const &compressedData, size_t compressedDataSz) = 0;
        int InitializeCompression(int compressionFlavours, size_t chunkSize);
        int CompressChunk(bool lastChunk);
        int FinalizeCompression(void);

    public:
        static uint32_t const kChunkSize_16Kb = 16 * 1024;
        static uint32_t const kChunkSize_32Kb = 32 * 1024;
        static uint32_t const kChunkSize_64Kb = 64 * 1024;
        static uint32_t const kChunkSize_Default = kChunkSize_16Kb;

    public:
        IZStreamProducer(void);
        virtual ~IZStreamProducer(void);

    public:
        virtual size_t Append(uint8_t const *data, size_t dataSz) override;
    };

    class ILzmaStreamConsumer : public IStreamConsumer
    {
    protected:
        struct LzmaContext;
        friend LzmaContext;
        LzmaContext *impl;

    public:
        ILzmaStreamConsumer(void) { }
        virtual ~ILzmaStreamConsumer(void) { }

    public:
        virtual bool Good(void) const override;
        virtual size_t Tell(void) const override;
        virtual size_t Consume(uint8_t *data, size_t dataSz) override;

    };

    class ILzmaStreamAsyncConsumer : public IStreamConsumer
    {
    protected:
        struct LzmaContext;
        friend LzmaContext;
        LzmaContext *impl;

    public:
        ILzmaStreamAsyncConsumer(void) { }
        virtual ~ILzmaStreamAsyncConsumer(void) { }

    public:
        virtual bool Good(void) const override;
        virtual size_t Tell(void) const override;
        virtual size_t Consume(uint8_t *data, size_t dataSz) override;

    };

    class ILzmaStreamProducer : public IStreamProducer
    {
    protected:
        struct LzmaContext;
        friend LzmaContext;
        LzmaContext *impl;

    protected:
        int InitializeCompression(int compressionFlavours, size_t chunkSize);
        int CompressChunk(void);
        int FinalizeCompression(void);

    public:
        ILzmaStreamProducer(void);
        virtual ~ILzmaStreamProducer(void);

    public:
        virtual size_t Append(uint8_t const *data, size_t dataSz) override;

    };

    class ZFileConsumer : public IZStreamConsumer
    {
    protected:
        FileConsumer fileConsumer;

    public:
        ZFileConsumer(File &file, size_t chunkSize = kChunkSize_32Kb);
        virtual ~ZFileConsumer(void);

    public:
        virtual size_t OnNextCompressedChunk(Blob &compressedData) override;

    };

    class ZFileProducer : public IZStreamProducer
    {
    protected:
        File &file;
        FileProducer fileProducer;

    public:
        ZFileProducer(File &file, size_t chunkSize = kChunkSize_64Kb);
        virtual ~ZFileProducer(void);

    public:
        virtual void OnNextCompressedChunk(Blob const &compressedData, size_t compressedDataSz) override;

    };

    class LzmaFileConsumer : public ILzmaStreamConsumer
    {
    protected:
        File &file;
        FileConsumer fileConsumer;

    public:
        LzmaFileConsumer(File &file);
        virtual ~LzmaFileConsumer(void);
    };

    class LzmaFileAsyncConsumer : public ILzmaStreamAsyncConsumer
    {
    protected:
        File &file;
        FileConsumer fileConsumer;

    public:
        LzmaFileAsyncConsumer(File &file);
        virtual ~LzmaFileAsyncConsumer(void);
    };

    class LzmaFileProducer : public ILzmaStreamProducer
    {
    protected:
        File &file;
        FileProducer fileProducer;

    public:
        LzmaFileProducer(File &file);
        virtual ~LzmaFileProducer(void);

    };

}