#include "stdafx.h"
#include "V3DAssert.h"
#include "BasicIO.h"
#include "Log.h"
using namespace Visco3D;

Blob::Blob()
    : wrapper(false)
    , dataSz(0)
{
}

Blob::~Blob()
{
    Clear();
}

bool Blob::Initialize(size_t sz)
{
    if (dataSz == sz) return true;
    else Clear();
    void *allocatedMemory = nullptr;
    if ((dataSz = sz) && (allocatedMemory = malloc(dataSz)))
    {
        data.reset(new (allocatedMemory)uint8_t[dataSz]);
        return true;
    }

    return false;
}


void Blob::Wrap(uint8_t *src, size_t srcSz)
{
    Clear();
    wrapper = true;
    if (dataSz = srcSz)
        data.reset(new (src)uint8_t[dataSz]),
        memcpy(data.get(), src, dataSz);
}

void Blob::Copy(uint8_t const *src, size_t srcSz)
{
    if (Initialize(srcSz))
        memcpy(data.get(), src, dataSz);
}

void Blob::Clear()
{
    if (!wrapper) data.reset(nullptr);
    else data.release();
    wrapper = false;
    dataSz = 0;
}

Blob::ShPtr Blob::CreatePtr(size_t dataSz)
{
    auto outBlob = std::make_shared<Blob>();
    outBlob->Initialize(dataSz);
    return outBlob;
}

Blob::ShPtr Blob::CreatePtr(std::wstring const &filePath, size_t offset, size_t size)
{
    return CreatePtr(FileWrapper::OpenFileForReading(filePath.c_str()), offset, size);
}

Blob::ShPtr Blob::CreatePtr(FileWrapper::ShPtr fileWrapper, size_t offset, size_t size)
{
    size_t fileSz = 0;
    if (fileWrapper->GetHandle() && !fileWrapper->ErrorOccurred())
    {
        size_t pos = ftell(fileWrapper->GetHandle());
        fseek(fileWrapper->GetHandle(), 0, SEEK_END);
        size_t end = ftell(fileWrapper->GetHandle());
        fseek(fileWrapper->GetHandle(), pos, SEEK_SET);

        if (size == 0 && ((pos + offset) < end))
        {
            size_t offsettedPos = pos + offset;
            fseek(fileWrapper->GetHandle(), offsettedPos, SEEK_SET);
            fileSz = end - pos - offset;
        }
        else if ((pos + offset + size) <= end)
        {
            size_t offsettedPos = pos + offset;
            fseek(fileWrapper->GetHandle(), offsettedPos, SEEK_SET);
            fileSz = size;
        }
        else
        {
            fileSz = 0;
        }
    }

    auto outBlob = CreatePtr(fileSz);
    if (fileSz) fread(
        (void*)outBlob->Data(), 1, fileSz,
        fileWrapper->GetHandle()
        );
    return outBlob;
}

BlobConsumer::BlobConsumer(Blob const &blob)
    : blob(blob)
    , currentPos(0)
{
}
BlobConsumer::BlobConsumer(Blob const &blob, IStreamConsumer const &consumer)
    : blob(blob)
    , currentPos(0)
{
    if (consumer.Good())
        currentPos = consumer.Tell();
}

size_t BlobConsumer::Tell() const
{
    return currentPos;
}

bool BlobConsumer::Good() const
{
    return (blob.Size() > currentPos);
}

size_t BlobConsumer::Consume(uint8_t *data, size_t dataSz)
{
    if (Good() && data && (dataSz != 0))
    {
        size_t consumeSz = std::min(blob.Size() - currentPos, dataSz);
        memcpy(data, blob.Data(), consumeSz);
        currentPos += consumeSz;
        return consumeSz;
    }

    return 0;
}

BlobProducer::BlobProducer(Blob &blob)
    : blob(blob)
{
}

BlobProducer::~BlobProducer()
{
    Finalize();
}

void BlobProducer::Finalize()
{
    auto str = ss.str();
    ss.str(std::string());
    blob.Copy((uint8_t const *)str.data(), str.size());
}

size_t BlobProducer::Append(uint8_t const *data, size_t dataSz)
{
    ss.write((const char*)data, dataSz);
    return dataSz;
}
