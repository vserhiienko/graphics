#include "stdafx.h"

#include <ppl.h>
#include <ppltasks.h>
#include <concurrent_queue.h>
#include <atomic>
#include <future>
#include <functional>
#include <queue>

#include "V3DAssert.h"
#include "Log.h"

// compression/decompression
#include <zlib.h>
#include "LZMA\LzmaEnc.h"
#include "LZMA\LzmaDec.h"
#include "BasicIO.h"

using namespace Visco3D;

struct LzmaConsumerProducerTraits
{
    static uint8_t const kLzmaPropsSz = LZMA_PROPS_SIZE;
    static uint8_t const kLzmaPropsChunkSz = sizeof(size_t);
    static uint8_t const kLzmaHeaderSz = kLzmaPropsSz + kLzmaPropsChunkSz;
    static size_t const kLzmaChunkSz = 64 * 1024 * 1024; // 64mb

    static void *SzAlloc(void *p, size_t size) { (void)p; return MyAlloc(size); }
    static void *MyAlloc(size_t size) { if (size == 0) return 0; return malloc(size); }
    static void SzFree(void *p, void *address) { (void)p; MyFree(address); }
    static void MyFree(void *address) { free(address); }
};

struct ILzmaStreamProducer::LzmaContext
{
    struct CSeqInStream
    {
        ISeqInStream readerFuncTable;
        LzmaContext *self;
    };
    struct CSeqOutStream
    {
        ISeqOutStream writerFuncTable;
        LzmaContext *self;
    };

    Blob blobEnc, blobEnc2;
    size_t chunkSz;
    size_t writtenSz;
    size_t blobEncPos;
    size_t blobEncGoodSz;

    bool encGood;
    SRes lastError;

    ISzAlloc alloc;
    CLzmaEncHandle enc;
    CLzmaEncProps encProps;
    CSeqInStream readerStream;
    CSeqOutStream writerStream;

    BlobConsumer blobConsumer;
    IStreamProducer *genericWriter;

    static void AppendChunkSzToHeader(uint8_t(&header)[LzmaConsumerProducerTraits::kLzmaHeaderSz], size_t chunkSz)
    {
        uint8_t nextByte = 0;
        uint8_t headerSize = LzmaConsumerProducerTraits::kLzmaPropsSz;
        for (uint8_t i = 0; i < LzmaConsumerProducerTraits::kLzmaPropsChunkSz; i++)
            nextByte = (uint8_t)(chunkSz >> (8 * i)),
            header[headerSize++] = nextByte;
    }

    static SRes ReadFromBlobEnc(void *p, void *buf, size_t *size)
    {
        if (*size == 0) return 0;
        CSeqInStream *inStream = static_cast<CSeqInStream*>(p);
        (*size) = inStream->self->blobConsumer.Consume((uint8_t*)buf, *size);
        visco_debugTrace("read from blob: %u", (uint32_t)*size);
        return SZ_OK;
    }

    static size_t WriteToCustomProducer(void *pp, const void *buf, size_t size)
    {

        if (size == 0) return SZ_OK;
        CSeqOutStream *outStream = static_cast<CSeqOutStream*>(pp);
        size_t producedSz = outStream->self->genericWriter->Append((const uint8_t*)buf, size);

        outStream->self->writtenSz += size;
        visco_debugTrace("\twrite to file: %u (%u)", (uint32_t)size, (uint32_t)outStream->self->writtenSz);

        return producedSz;
    }

    LzmaContext()
        : blobEncPos(0)
        , blobEncGoodSz(0)
        , writtenSz(0)
        , blobConsumer(blobEnc)
    {
        chunkSz = LzmaConsumerProducerTraits::kLzmaChunkSz;
        blobEnc.Initialize(chunkSz);
        blobEnc2.Initialize(chunkSz);

        encGood = enc != 0;
    }

    ~LzmaContext()
    {
        //LzmaEnc_Destroy(enc, &alloc, &alloc);
    }

    void Encode()
    {
        alloc.Alloc = LzmaConsumerProducerTraits::SzAlloc;
        alloc.Free = LzmaConsumerProducerTraits::SzFree;

        uint8_t header[LzmaConsumerProducerTraits::kLzmaHeaderSz];
        size_t propsSz = LzmaConsumerProducerTraits::kLzmaPropsSz;
        size_t headerSz = LzmaConsumerProducerTraits::kLzmaHeaderSz;

        LzmaEncProps_Init(&encProps);
        encProps.writeEndMark = 1;
        assert(lastError == SZ_OK);
        AppendChunkSzToHeader(header, blobEncPos);

        size_t blobEnc2Sz = blobEnc2.Size();
        size_t propsSz2 = LZMA_PROPS_SIZE;

        SRes res = LzmaEncode(
            blobEnc2.Data(), &blobEnc2Sz,
            blobEnc.Data(), blobEncPos,
            &encProps,
            header,
            &propsSz2,
            1, 0,
            &alloc, &alloc
            );

        if (res == SZ_OK)
        {
            genericWriter->Append(header, headerSz);
            genericWriter->Append((uint8_t*)&blobEnc2Sz, sizeof(blobEnc2Sz));
            genericWriter->Append(blobEnc2.Data(), blobEnc2Sz);
            visco_debugTrace("\t\t %u -> %u", blobEncPos, blobEnc2Sz);
        }

        blobEncPos = 0;
    }
};

ILzmaStreamProducer::ILzmaStreamProducer(void)
    : impl(nullptr)
{
}

ILzmaStreamProducer::~ILzmaStreamProducer(void)
{
    SAFE_DELETE_PTR(impl);
}

int ILzmaStreamProducer::InitializeCompression(int compressionFlavours, size_t chunkSize)
{
    SAFE_DELETE_PTR(impl);
    impl = new LzmaContext;
    return impl->lastError;
}

int ILzmaStreamProducer::CompressChunk()
{
    visco_debugTrace("CompressChunk: ...");
    impl->Encode();
    return SZ_OK;
}

int ILzmaStreamProducer::FinalizeCompression(void)
{
    if (impl && impl->blobEncPos)
        CompressChunk();
    SAFE_DELETE_PTR(impl);
    return SZ_OK;
}

size_t ILzmaStreamProducer::Append(uint8_t const *data, size_t dataSz)
{
    if (impl->blobEnc.Size() == impl->blobEncPos)
        CompressChunk(),
        impl->blobEncPos = 0;

    size_t canAppendSz = impl->blobEnc.Size() - impl->blobEncPos;
    if (canAppendSz >= dataSz)
    {
        // the data can still be written
        memcpy(impl->blobEnc.Data() + impl->blobEncPos, data, dataSz);
        impl->blobEncPos += dataSz; // advance counter
    }
    else
    {
        // finish this block
        Append(data, canAppendSz);
        assert(impl->blobEncPos == impl->blobEnc.Size());

        CompressChunk();
        impl->blobEncPos = 0;

        // start the new one
        Append(data + canAppendSz, dataSz - canAppendSz);
    }

    return dataSz;
}


LzmaFileProducer::LzmaFileProducer(File &file)
    : file(file)
    , fileProducer(file)
{
    InitializeCompression(0, 0);
    impl->genericWriter = &fileProducer;
}
LzmaFileProducer::~LzmaFileProducer(void)
{
    FinalizeCompression();
}

struct ILzmaStreamConsumer::LzmaContext
{
    size_t chunkSz;
    ISzAlloc alloc;
    IStreamConsumer *genericReader; // reads compressed data from file or elsewhere

    Blob packedBlob, blob;
    size_t blobPos, blobGoodSz;
    size_t packedBlobPos, packedBlobGoodSz;
    size_t unpackedSz, packedSz;

    int res;
    int thereIsSize; /* = 1, if there is uncompressed size in headers */

    CLzmaDec state;
    unsigned char header[LZMA_PROPS_SIZE + 8];

    LzmaContext()
    {
        chunkSz = LzmaConsumerProducerTraits::kLzmaChunkSz;
        blobPos = 0, blobGoodSz = 0;
        packedBlobPos = 0, packedBlobGoodSz = 0;
        unpackedSz = 0, packedSz = 0;
        thereIsSize = 0, res = 0;
    }

    ~LzmaContext()
    {
    }

    void DecodeNextBlock()
    {
        alloc.Alloc = LzmaConsumerProducerTraits::SzAlloc;
        alloc.Free = LzmaConsumerProducerTraits::SzFree;

        genericReader->Consume(header, sizeof(header));

        unpackedSz = 0;
        for (int i = 0; i < 8; i++)
        {
            unsigned char b = header[LZMA_PROPS_SIZE + i];
            if (b != 0xFF) thereIsSize = 1;
            unpackedSz += (UInt64)b << (i * 8);
        }

        genericReader->Consume((uint8_t*)&packedSz, sizeof(packedSz));

        blob.Initialize(unpackedSz * 3 / 2);
        packedBlob.Initialize(packedSz);

        size_t packedReadSz = genericReader->Consume(packedBlob.Data(), packedSz);

        size_t srcLen = packedBlob.Size();
        size_t destLen = blob.Size();
        ELzmaStatus status = LZMA_STATUS_NOT_SPECIFIED;

        SRes res = LzmaDecode(
            blob.Data(), &destLen,
            packedBlob.Data(), &srcLen,
            header, LZMA_PROPS_SIZE,
            LZMA_FINISH_END, &status,
            &alloc
            );

        visco_debugTrace("Decode: (%i == 0) %u %u -> %u %u (%i == 1)"
            , res
            , (uint32_t)unpackedSz, (uint32_t)packedSz
            , (uint32_t)destLen, (uint32_t)srcLen
            , status
            );

        blobGoodSz = destLen;
        blobPos = 0;

    }

    void Decode()
    {
        if (blobPos == blobGoodSz) DecodeNextBlock();
        assert(blobPos == 0);
    }

};

bool ILzmaStreamConsumer::Good(void) const
{
    return false;
}

size_t ILzmaStreamConsumer::Tell(void) const
{
    return 0;
}

size_t ILzmaStreamConsumer::Consume(uint8_t *data, size_t dataSz)
{
    if (dataSz == 0 || data == nullptr) return 0;

    size_t consumedSz = 0;
    size_t canConsumeSz = impl->blobGoodSz - impl->blobPos;

    if (canConsumeSz >= dataSz)
    {
        memcpy(data, impl->blob.Data() + impl->blobPos, dataSz);
        impl->blobPos += dataSz;
        consumedSz += dataSz;
    }
    else
    {
        visco_debugTrace("Consume: can consume %u (%u)"
            , canConsumeSz, dataSz
            );

        consumedSz += Consume(data, canConsumeSz);
        impl->Decode();

        size_t dataInsufficiencySz = dataSz - canConsumeSz;
        consumedSz += Consume(data + canConsumeSz, dataInsufficiencySz);

    }

    assert(dataSz == consumedSz);
    return consumedSz;

}

LzmaFileConsumer::LzmaFileConsumer(File &file)
    : file(file)
    , fileConsumer(file)
{
    impl = new LzmaContext();
    impl->genericReader = &fileConsumer;
}

LzmaFileConsumer::~LzmaFileConsumer(void)
{
    delete impl;
}

struct ILzmaStreamAsyncConsumer::LzmaContext
{
    static uint8_t const kLzmaPropsSz = LZMA_PROPS_SIZE;
    static uint8_t const kLzmaPropsChunkSz = sizeof(size_t);
    static uint8_t const kLzmaHeaderSz = kLzmaPropsSz + kLzmaPropsChunkSz;
    static size_t const kLzmaChunkSz = 64 * 1024 * 1024; // 64mb
    static size_t const IN_BUF_SIZE = kLzmaChunkSz; // 64mb
    static size_t const OUT_BUF_SIZE = kLzmaChunkSz; // 64mb

    struct BlobPair;
    struct UnpackTask;
    struct AsyncUnpacker;

    struct BlobPair
    {
        size_t pos;
        int thereIsSize;
        size_t blobPos, blobGoodSz;
        size_t unpackedSz, packedSz;
        unsigned char header[LzmaConsumerProducerTraits::kLzmaHeaderSz];

        Blob packed;
        Blob unpacked;
        UnpackTask *unpackHost;
    };

    struct UnpackTask
    {
        BlobPair *attachedPair;
        concurrency::task<void> unpackTask;
    };

    struct AsyncUnpacker
    {
        LzmaContext &contextRef;

        bool eof;
        size_t sz;
        std::atomic_size_t pos;
        std::atomic_size_t activeConsumes;
        std::vector<BlobPair*> pairs;
        concurrency::concurrent_queue<UnpackTask*> tasks;
        concurrency::concurrent_queue<BlobPair*> consumedPairs;

        AsyncUnpacker(LzmaContext &context, size_t threads = 32)
            : contextRef(context)
            , sz(threads)
            , eof(false)
        {
            activeConsumes = 0;

            pairs.resize(sz);
            for (auto &p : pairs) p = new BlobPair();
            for (auto &p : pairs) consumedPairs.push(p);
        }

        ~AsyncUnpacker()
        {
            for (auto &p : pairs) SAFE_DELETE_PTR(p);
        }

        BlobPair *Consume()
        {
            UnpackTask *nextTask = 0;
            while (!tasks.try_pop(nextTask))
                TryUnpackMore();
            nextTask->unpackTask.wait();
            activeConsumes++;
            BlobPair *pair = nextTask->attachedPair;
            delete nextTask;
            return pair;
        }

        void OnConsumed(BlobPair *pair)
        {
            activeConsumes--;
            consumedPairs.push(pair);
            TryUnpackMore();
        }

        void TryUnpackMore()
        {
            size_t consumedInd = 0;
            size_t consumedCount = sz - activeConsumes.load();

            while ((consumedInd < consumedCount) && (!eof))
            {
                consumedInd++;

                BlobPair *producablePair = 0;
                if (!consumedPairs.try_pop(producablePair)) return;
                if (!producablePair) return;

                producablePair->packedSz = 0;
                producablePair->unpackedSz = 0;

                size_t headerReadSz = contextRef.genericReader->Consume(producablePair->header, sizeof(producablePair->header));

                if (headerReadSz != sizeof(producablePair->header))
                {
                    visco_debugTrace("TryUnpackMore: eof", (uint32_t)consumedCount);
                    eof = true;
                    return;
                }

                for (int i = 0; i < 8; i++)
                {
                    unsigned char b = producablePair->header[LZMA_PROPS_SIZE + i];
                    if (b != 0xff) producablePair->thereIsSize = 1;
                    producablePair->unpackedSz += (UInt64)(b << (i * 8));
                }

                contextRef.genericReader->Consume((uint8_t*)&producablePair->packedSz, sizeof(producablePair->packedSz));

                producablePair->packed.Initialize(producablePair->packedSz);
                producablePair->unpacked.Initialize(producablePair->unpackedSz);

                producablePair->packedSz = contextRef.genericReader->Consume(producablePair->packed.Data(), producablePair->packedSz);
                producablePair->pos = contextRef.genericReader->Tell();

                UnpackTask *task = new UnpackTask();
                tasks.push(task);

                task->attachedPair = producablePair;

                visco_debugTrace("TryUnpackMore: task enqueued");
                task->unpackTask = concurrency::create_task([producablePair]()
                {
                    visco_debugTrace("TryUnpackMore: task launched @ %u", (uint32_t)producablePair->pos);

                    ISzAlloc alloc;
                    alloc.Alloc = LzmaConsumerProducerTraits::SzAlloc;
                    alloc.Free = LzmaConsumerProducerTraits::SzFree;

                    size_t srcLen = producablePair->packed.Size();
                    size_t destLen = producablePair->unpacked.Size();
                    ELzmaStatus status = LZMA_STATUS_NOT_SPECIFIED;

                    SRes res = LzmaDecode(
                        producablePair->unpacked.Data(), &destLen,
                        producablePair->packed.Data(), &srcLen,
                        producablePair->header, LZMA_PROPS_SIZE,
                        LZMA_FINISH_END, &status,
                        &alloc
                        );

                    producablePair->blobGoodSz = destLen;
                    producablePair->blobPos = 0;

                    visco_debugTrace("TryUnpackMore: task done, results: (%i == 0[ok]) %u %u -> %u %u (%i == 1[mark])"
                        , res
                        , (uint32_t)producablePair->unpackedSz
                        , (uint32_t)producablePair->packedSz
                        , (uint32_t)destLen, (uint32_t)srcLen
                        , status
                        );

                });
            }

        }

    };

    BlobPair *pair;
    AsyncUnpacker asyncUnpacker;

    // reads compressed data from file or elsewhere
    IStreamConsumer *genericReader;

    LzmaContext()
        : asyncUnpacker(*this)
        , pair(0)
    {
    }

    ~LzmaContext()
    {
    }

    void Decode()
    {
        pair = asyncUnpacker.Consume();
    }

};

bool ILzmaStreamAsyncConsumer::Good(void) const
{
    return false;
}

size_t ILzmaStreamAsyncConsumer::Tell(void) const
{
    return 0;
}

size_t ILzmaStreamAsyncConsumer::Consume(uint8_t *data, size_t dataSz)
{
    if (dataSz == 0 || data == nullptr) return 0;

    size_t consumedSz = 0;
    size_t canConsumeSz = impl->pair ? (impl->pair->blobGoodSz - impl->pair->blobPos) : 0;

    if (canConsumeSz >= dataSz)
    {
        memcpy(data, impl->pair->unpacked.Data() + impl->pair->blobPos, dataSz);
        impl->pair->blobPos += dataSz;
        consumedSz += dataSz;
    }
    else
    {
        visco_debugTrace("Consume: consumed %u, needed %u => insufficiency %u"
            , (uint32_t)canConsumeSz, (uint32_t)dataSz
            , (uint32_t)(dataSz - canConsumeSz)
            );

        consumedSz += Consume(data, canConsumeSz);
        impl->asyncUnpacker.OnConsumed(impl->pair);
        impl->Decode();

        size_t dataInsufficiencySz = dataSz - canConsumeSz;
        consumedSz += Consume(data + canConsumeSz, dataInsufficiencySz);

    }

    return consumedSz;
}

LzmaFileAsyncConsumer::LzmaFileAsyncConsumer(File &file)
    : file(file)
    , fileConsumer(file)
{
    impl = new LzmaContext();
    impl->genericReader = &fileConsumer;
}

LzmaFileAsyncConsumer::~LzmaFileAsyncConsumer(void)
{
    delete impl;
}