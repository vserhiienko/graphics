
#include "stdafx.h"
#include <iostream>

#include <zlib.h>

#include "V3DAssert.h"
#include "Model.h"
#include "ModelWriter.h"
#include "Log.h"
#include "MathUtils.h"

using namespace Visco3D;

#pragma region class BasicWriter


BasicWriter::BasicWriter(FileWrapper::ShPtr file)
    : m_file(file)
{
    streamProducer.reset(new FileProducer(*file.get()));
}
BasicWriter::~BasicWriter(void)
{
}

bool BasicWriter::WriteBytes(const void *data, size_t elementSize, size_t elementNum)
{
    // check if all the chucks were written
    size_t dataSz = elementSize * elementNum;
    return dataSz == streamProducer->Append((uint8_t*)data, dataSz);
    //return elementNum == fwrite(data, elementSize, elementNum, m_file->GetHandle());
}

bool BasicWriter::WriteUInt16(uint16_t valueToWrite)
{
    return WriteBytes(&valueToWrite, sizeof(uint16_t), 1);
}

bool BasicWriter::WriteString(std::wstring const &valueToWrite)
{
    uint16_t strLen = (uint16_t)valueToWrite.length();
    // first, store the string length, then, if it`s not empty, store the string data into the file
    return WriteUInt16(strLen) && strLen && WriteBytes(valueToWrite.c_str(), sizeof(wchar_t), valueToWrite.length());

}

bool BasicWriter::WriteUInt32_7BitEnc(uint32_t value)
{
    const size_t bytesNum = sizeof(uint32_t) + 0;
    for (size_t bi = 0; bi < bytesNum; ++bi)
    {
        size_t nextbi = bi + 1;
        uint8_t byteToWrite = (value >> (bi * 7)) & 0xFF;
        bool writeNextByte = (value >> (nextbi * 7)) != 0;
        if (nextbi < bytesNum)
        {
            SET_BITS(byteToWrite, 7, writeNextByte ? 1 : 0);
        }
        WriteBytes(&byteToWrite, sizeof(uint8_t), 1);
        if (!writeNextByte)
            break;
    }

    return true;
}

bool BasicWriter::WriteInt32_7BitEnc(int32_t valueToWrite)
{
    return WriteUInt32_7BitEnc(*reinterpret_cast<uint32_t*>(&valueToWrite));
}

bool BasicWriter::WriteID32_7BitEnc(int32_t value)
{
    return WriteUInt32_7BitEnc((uint32_t)(value + 1));
}

bool BasicWriter::WriteUInt32(uint32_t valueToWrite)
{
    return WriteBytes(&valueToWrite, sizeof(uint32_t), 1);
}

bool BasicWriter::WriteInt32(int32_t valueToWrite)
{
    return WriteBytes(&valueToWrite, sizeof(int32_t), 1);
}

bool BasicWriter::WriteID32(int32_t valueToWrite)
{
    ++valueToWrite;
    return WriteBytes(&valueToWrite, sizeof(int32_t), 1);
}

#pragma endregion

#pragma region classes CompressedWriter, CompressedWriter::Implementation

/*
typedef struct CompressedWriter::Implementation
{
    static const size_t chunkSize = 1024 * 1024;

    std::vector<uint8_t> inBuffer;
    std::vector<uint8_t> outBuffer;

    FILE* nativeFile;
    z_stream strm;
    bool read;
    size_t readBuffPos;

    std::stringstream ss;

    Implementation(FILE* file)
    {
        read = false;
        nativeFile = file;

        inBuffer.reserve(chunkSize);
        outBuffer.reserve(chunkSize);
        readBuffPos = 0;

        strm.zalloc = Z_NULL;
        strm.zfree = Z_NULL;
        strm.opaque = Z_NULL;

        strm.next_in = NULL;
        strm.avail_in = NULL;
        strm.next_out = NULL;
        strm.avail_out = NULL;

        strm.next_in = reinterpret_cast<uint8_t *>(inBuffer.data());
        strm.avail_in = chunkSize;
        strm.next_out = reinterpret_cast<uint8_t *>(outBuffer.data());
        strm.avail_out = chunkSize;

        int result = Z_OK;
        if (read)
        {
            result = inflateInit(&strm);
            inBuffer.resize(chunkSize);
        }
        else
        {
            result = deflateInit(&strm, Z_BEST_COMPRESSION);
            outBuffer.resize(chunkSize);
        }


        V3D_ASSERT(result);
    }

    std::string compress_string(const std::string& str,
        int compressionlevel = Z_BEST_COMPRESSION)
    {
        z_stream zs;                        // z_stream is zlib's control structure
        memset(&zs, 0, sizeof(zs));

        if (deflateInit(&zs, compressionlevel) != Z_OK)
            return "";
        //throw(std::runtime_error("deflateInit failed while compressing."));

        zs.next_in = (Bytef*)str.data();
        zs.avail_in = str.size();           // set the z_stream's input

        int ret;
        char outbuffer[32768];
        std::string outstring;

        // retrieve the compressed bytes blockwise
        do {
            zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
            zs.avail_out = sizeof(outbuffer);

            ret = deflate(&zs, Z_FINISH);

            if (outstring.size() < zs.total_out) {
                // append the block to the output string
                outstring.append(outbuffer,
                    zs.total_out - outstring.size());
            }
        } while (ret == Z_OK);

        deflateEnd(&zs);

        if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
            std::ostringstream oss;
            oss << "Exception during zlib compression: (" << ret << ") " << zs.msg;
            return "";
            //throw(std::runtime_error(oss.str()));
        }

        return outstring;
    }

    void Write(const void *in_data, size_t in_data_size)
    {
#if 0
        ss.write((const char *)in_data, in_data_size); 

#else

        while (in_data_size != 0)
        {
            size_t data_size_to_copy = in_data_size;
            if ((inBuffer.size() + data_size_to_copy) > chunkSize)
                data_size_to_copy = chunkSize - inBuffer.size();

            size_t prevBufferSize = inBuffer.size();
            inBuffer.resize(prevBufferSize + data_size_to_copy);
            memcpy(&inBuffer[prevBufferSize], in_data, data_size_to_copy);

            if ((in_data_size - data_size_to_copy) > 0)
                in_data = &((const uint8_t*)in_data)[data_size_to_copy];
            in_data_size -= data_size_to_copy;

            if (inBuffer.size() == chunkSize)
                FlushWrite();
        }

#endif
    }

    void FlushWrite(bool doFlush = false)
    {
        strm.avail_in = (uInt)inBuffer.size();
        strm.next_in = reinterpret_cast<uint8_t *>(inBuffer.data());

        int ret, flush, have;
        flush = doFlush ? Z_FINISH : Z_NO_FLUSH;

        if (!inBuffer.empty())
        {
            // run deflate() on input until output buffer not full, finish
            // compression if all of source has been read in
            do {
                strm.avail_out = (uInt)outBuffer.size();
                strm.next_out = reinterpret_cast<uint8_t *>(outBuffer.data());
                ret = deflate(&strm, flush);    //* no bad return value 
                V3D_ASSERT(ret != Z_STREAM_ERROR);  //* state not clobbered 
                have = (int)outBuffer.size() - strm.avail_out;
                if (fwrite(outBuffer.data(), 1, have, nativeFile) != have || ferror(nativeFile)) {
                    (void)deflateEnd(&strm);
                    V3D_ASSERT(false);
                    break;
                }
            } while (strm.avail_out == 0);
        }
        V3D_ASSERT(strm.avail_in == 0);     //* all input will be used 

        if (flush == Z_FINISH)
        {
            strm.avail_in = 0;
            strm.next_in = NULL;
            while (ret == Z_OK)
            {
                ret = deflate(&strm, Z_FINISH);    //* no bad return value 
                V3D_ASSERT(ret != Z_STREAM_ERROR);  //* state not clobbered 
                have = (int)outBuffer.size() - strm.avail_out;
                if (fwrite(outBuffer.data(), 1, have, nativeFile) != have || ferror(nativeFile)) {
                    (void)deflateEnd(&strm);
                    V3D_ASSERT(false);
                    break;
                }
            }
        }

        inBuffer.clear();
    }

    ~Implementation()
    {
        // for the ModelWriter 'read' will always be set to 'false'
        // so the redundant 'if' branch was removed, along with the 'Read' and 'FlushRead'
        //FlushWrite(true);
        //(void)deflateEnd(&strm);

        auto compressedData = compress_string(ss.str());

        if (!compressedData.empty())
        fwrite(compressedData.c_str(), 1, compressedData.size(), nativeFile);
    }

} ZipFile;

*/

CompressedWriter::CompressedWriter(FileWrapper::ShPtr file)
    : BasicWriter(file)
    //, m_impl(new ZipFile(file->GetHandle()))
{
    streamProducer.reset(new LzmaFileProducer(*file.get()));
    //streamProducer.reset(new ZFileProducer(*file.get()));
}

CompressedWriter::~CompressedWriter()
{
    //delete m_impl;
    streamProducer.reset();
}

bool CompressedWriter::WriteBytes(const void *data, size_t elementSize, size_t elementNum)
{
    size_t dataSz = elementSize * elementNum;
    //streamProducer->Append((uint8_t*)data, elementSize * elementNum);
    //m_impl->Write(data, elementSize * elementNum);
    return dataSz == streamProducer->Append((uint8_t*)data, dataSz);
}

#pragma endregion

namespace Visco3DImpl
{
    static mat4 iTM(1);
    static mat4x3 iTM4x3(1);
    static vec4ubn emptyColor(1.0f, 1.0f, 1.0f, 1.0f);
    static vec4ubn emptyColor2(0.0f, 0.0f, 0.0f, 0.0f);

#pragma region class DefaultWriter

    class DefaultWriter : public ModelFileWriter::IBinaryWriter
    {
    public:
        DefaultWriter(ModelFileWriter &s)
            : IBinaryWriter(s)
        {
        }
        virtual void NextUInt32(uint32_t ui32) override
        {
            m_writer->WriteUInt32(ui32);
        }
        virtual void NextInt32(int32_t i32) override
        {
            m_writer->WriteInt32(i32);
        }
        virtual void NextId32(int32_t id) override
        {
            m_writer->WriteID32(id);
        }

    };

#pragma endregion

#pragma region class Enc7BitWriter

    class Enc7BitWriter : public ModelFileWriter::IBinaryWriter
    {
    public:
        Enc7BitWriter(ModelFileWriter &s)
            : IBinaryWriter(s)
        {
        }
        virtual void NextUInt32(uint32_t ui32) override
        {
            m_writer->WriteUInt32_7BitEnc(ui32);
        }
        virtual void NextInt32(int32_t i32) override
        {
            m_writer->WriteInt32_7BitEnc(i32);
        }
        virtual void NextId32(int32_t id) override
        {
            m_writer->WriteID32_7BitEnc(id);
        }
    };

#pragma endregion

}

using namespace Visco3DImpl;

#pragma region class ModelFileWriter::IWriter

ModelFileWriter::IBinaryWriter::IBinaryWriter(ModelFileWriter &streamer)
    : m_streamer(streamer)
{
    m_writer = new BasicWriter(streamer.m_file);
}

ModelFileWriter::IBinaryWriter::~IBinaryWriter(void)
{
    SAFE_DELETE_PTR(m_writer);
}

void ModelFileWriter::IBinaryWriter::NextString(std::wstring const &text)
{
    m_writer->WriteString(text);
}

void ModelFileWriter::IBinaryWriter::ProceedAsCompressed(void)
{
    BasicWriter *compressedWriter = new CompressedWriter(m_streamer.m_file);
    SAFE_DELETE_PTR(m_writer);
    m_writer = compressedWriter;
}

#pragma endregion

ModelFileWriter::ModelFileWriter()
{
}

void ModelFileWriter::PrepareForSaving(FileWrapper::ShPtr file, Model &model)
{
    model.SetFullPath(file->GetFullPath().c_str());
    model.m_rootNodeId = maxValue(model.m_rootNodeId, INVALID_UID);
    model.m_flags = SET_FLAG(model.m_flags, Model::mffContainsRootNode, model.m_rootNodeId != INVALID_UID ? 1 : 0);
    model.m_flags = SET_FLAG(model.m_flags, Model::mffZCompression, 1); // force compression
    //model.m_flags = SET_FLAG(model.m_flags, Model::mffZCompression, 0); // force normal writes
    model.UpdateBounds();
    model.UpdateVertexLayouts();
    model.UpdateIdsRanges();
}

void ModelFileWriter::Save(FileWrapper::ShPtr file, Model &model)
{
    m_file = file;

    // the lastest model file version supports encoded integers
    //m_writer = std::make_shared<DefaultWriter>(*this);
    m_writer = std::make_shared<Enc7BitWriter>(*this);

    m_writer->NextElement<decltype(Model::kModelSign)>(Model::kModelSign); // force to char array
    m_writer->NextElement<>(Model::VERSION);

    m_writer->NextElement(model.m_flags);
    m_writer->NextElement(model.m_uuid);

    if (model.m_flags & Model::mffZCompression)
    {
        size_t compressedFileSize = 0; // TODO
        m_writer->NextElement<uint64_t>(compressedFileSize);
    }

    if (model.m_flags & Model::mffContainsRootNode)
    {
        m_writer->NextId32(model.m_rootNodeId);
    }

    WriteBounds(model);
    WriteIdRanges(model);
    WriteMaterials(model);

    if (model.m_flags & Model::mffZCompression)
        m_writer->ProceedAsCompressed();

    WriteMeshVertexLayouts(model);
    WriteMeshes(model);
    WriteNodes(model);
    WriteAnimations(model);

    m_writer.reset();
    m_file.reset();
}

void ModelFileWriter::FinalizeWriting(Model &)
{
    m_file.reset();
}

void ModelFileWriter::WriteAnimations(Model  &model)
{
    //write animations

    uint32_t animatedNodesNum = (uint32_t)(model.m_animations.size() + model.m_animationsNamed.size() + model.m_animationsById.size());
    m_writer->NextUInt32(animatedNodesNum);

    if (animatedNodesNum > 0)
        m_writer->NextElement(model.m_frameRate);

    for (Model::NODE_ANIMATIONS_MAP::const_iterator itAnim = model.m_animations.begin(); itAnim != model.m_animations.end(); ++itAnim)
    {
        int nodeIdx = itAnim->first;
        const NODE_ANIMATION_CHANNELS_VEC& channels = itAnim->second;

        WriteAnimation(nodeIdx, NODE_MESHLINK_ID(INVALID_UID, 0xFFFFFFFF), model.m_nodes[nodeIdx].name, channels, model);
    }

    for (Model::NODE_ANIMATIONS_MAP1::const_iterator itAnim = model.m_animationsNamed.begin(); itAnim != model.m_animationsNamed.end(); ++itAnim)
    {
        const WString& nodeName = itAnim->first;
        const NODE_ANIMATION_CHANNELS_VEC& channels = itAnim->second;

        WriteAnimation(-1, NODE_MESHLINK_ID(INVALID_UID, 0xFFFFFFFF), nodeName, channels, model);
    }

    for (Model::NODE_ANIMATIONS_MAP2::const_iterator itAnim = model.m_animationsById.begin(); itAnim != model.m_animationsById.end(); ++itAnim)
    {
        const NODE_MESHLINK_ID& nodeName = itAnim->first;
        const NODE_ANIMATION_CHANNELS_VEC& channels = itAnim->second;

        WriteAnimation(-1, nodeName, L"", channels, model);
    }

    uint32_t skeletonsNum = (uint32_t)model.m_skeletons.size();
    m_writer->NextUInt32(skeletonsNum);

    if (skeletonsNum > 0)
    {
        for (size_t skIdx = 0; skIdx < model.m_skeletons.size(); ++skIdx)
        {
            uint32_t bonesNum = (uint32_t)model.m_skeletons[skIdx]->boneNodesIndices.size();
            m_writer->NextUInt32(bonesNum);

            m_writer->NextElements(model.m_skeletons[skIdx]->boneNodesIndices.data(), bonesNum);
            m_writer->NextElements(model.m_skeletons[skIdx]->bonesBounds.data(), bonesNum);
        }

        uint32_t nodesSkeletonMapSize = (uint32_t)model.m_nodesSkeletonMap.size();
        m_writer->NextUInt32(nodesSkeletonMapSize);
        for (Model::NODE_SKELETON_MAP::const_iterator itNodeIdxSkIdx = model.m_nodesSkeletonMap.begin(); itNodeIdxSkIdx != model.m_nodesSkeletonMap.end(); ++itNodeIdxSkIdx)
        {
            m_writer->NextId32(itNodeIdxSkIdx->first);
            m_writer->NextId32(itNodeIdxSkIdx->second);
        }
    }

    uint32_t lookAtLinksNum = (uint32_t)model.m_lookAtLinksMap.size();
    m_writer->NextUInt32(lookAtLinksNum);

    for (Model::LOOK_AT_LINKS_MAP::const_iterator itLookAt = model.m_lookAtLinksMap.begin();
        itLookAt != model.m_lookAtLinksMap.end(); ++itLookAt)
    {
        int nodeIdx = itLookAt->first;
        int lookAtTarget = itLookAt->second;
        m_writer->NextId32(nodeIdx);
        m_writer->NextId32(lookAtTarget);
    }
}

void ModelFileWriter::WriteAnimation(int nodeIdx, const NODE_MESHLINK_ID& id, const WString& nodeName, const NODE_ANIMATION_CHANNELS_VEC& channels, Model  &model)
{
    m_writer->NextId32(nodeIdx);

    m_writer->NextId32(id.first);
    if (id.first != INVALID_UID)
        m_writer->NextId32(id.second);

    m_writer->NextString(nodeName);

    uint32_t channelsNum = (uint32_t)channels.size();
    m_writer->NextUInt32(channelsNum);

    for (uint32_t k = 0; k < channelsNum; ++k)
    {
        const NodeAnimationChannel& channel = channels[k];

        m_writer->NextUInt32(channel.targetComponents);

        //process track

        const AnimationTrack& track = *channel.animTrack.get();

        V3D_ASSERT(track.IsValid());

        //process general track params

        int framesNum = (int)track.m_timeline.size();
        int targetComponentsNum = (int)track.m_targetComponentsNum;
        int erpType = (int)track.m_erpType;

        m_writer->NextUInt32(framesNum);
        m_writer->NextUInt32(targetComponentsNum);
        m_writer->NextUInt32(erpType);

        //process frames data

        const size_t floatSize = sizeof(float);
        m_writer->GetUnderlyingWriter()->WriteBytes(
            track.m_timeline.data(),
            floatSize, framesNum
            );

        for (int j = 0; j < framesNum; ++j)
            m_writer->GetUnderlyingWriter()->WriteBytes(
            track.m_framesData[j].data(),
            floatSize, targetComponentsNum
            );

        //process tangents (used for bezier, hermite, etc. interpolations)

        if (track.m_erpType != AnimationTrack::LINEAR && track.m_erpType != AnimationTrack::STEP)
        {
            for (int j = 0; j < framesNum; ++j)
            {
                m_writer->NextElements(track.m_inTangents[j].data(), targetComponentsNum);
                m_writer->NextElements(track.m_outTangents[j].data(), targetComponentsNum);
            }
        }
        if (track.m_erpType == AnimationTrack::MIXED)
        {
            m_writer->NextElements(track.m_mixedErpTypes.data(), framesNum);
        }
    }
}

void ModelFileWriter::WriteNodes(Model  &model)
{
    //write nodes

    uint32_t nodesNum = (uint32_t)model.m_nodes.size();
    m_writer->NextUInt32(nodesNum);

    for (uint32_t i = 0; i < nodesNum; ++i)
        WriteNode(model.m_nodes[i], model);

    VIS3D_LOG_FDEBUG(L"Write nodes pos " << ftell(m_file->GetHandle()));
}

void ModelFileWriter::WriteNode(Model::Node const &node, Model  &model)
{
    int nodeFlags = (int)node.flags;
    SET_FLAG(nodeFlags, Model::NameAbsent, node.name.empty());
    SET_FLAG(nodeFlags, Model::UserPropertiesAbsent, node.userProps.empty());
    SET_FLAG(nodeFlags, Model::ParentIdAbsent, node.parentNodeIndex < 0);
    SET_FLAG(nodeFlags, Model::SpecifiedIdAbsent, 1);
    SET_FLAG(nodeFlags, Model::TransformAbsent, node.transform == iTM4x3);

    uint8_t nodeFlagsByte = (uint8_t)nodeFlags;
    m_writer->NextElement(nodeFlagsByte);

    if ((nodeFlags & Model::NameAbsent) == 0)
        m_writer->NextString(node.name);

    if ((nodeFlags & Model::UserPropertiesAbsent) == 0)
        m_writer->NextString(node.userProps);

    if ((nodeFlags & Model::ParentIdAbsent) == 0)
        m_writer->NextId32(node.parentNodeIndex);

    if ((nodeFlags & Model::SpecifiedIdAbsent) == 0)
        m_writer->NextId32(node.specifiedId);

    if ((nodeFlags & Model::TransformAbsent) == 0)
        m_writer->NextElement(node.transform);

    uint32_t meshLinksNum = (uint32_t)node.meshLinks.size();
    m_writer->NextUInt32(meshLinksNum);

    for (uint32_t meshLinkIdx = 0; meshLinkIdx < meshLinksNum; ++meshLinkIdx)
    {
        const Model::MeshLink& ml = node.meshLinks[meshLinkIdx];

        uint8_t mlFlags = ml.flags;

        bool dontWritePrimType = ml.primitiveType == Model::ptMesh || ml.primitiveType == Model::ptNoPrimitive;
        SET_FLAG(mlFlags, Model::PrimitiveTypeAbsent, dontWritePrimType);
        bool isMesh = ml.primitiveType == Model::ptMesh;

        bool hasGeometry = (isMesh && ml.meshIndex >= 0) || (!isMesh && ml.primitiveType != Model::ptNoPrimitive);
        V3D_ASSERT(hasGeometry);

        SET_FLAG(mlFlags, Model::ColorAbsent, ml.color == emptyColor);
        SET_FLAG(mlFlags, Model::MaterialAbsent, ml.materialIndex < 0);
        SET_FLAG(mlFlags, Model::MeshTransformAbsent, ml.meshTransform == iTM4x3);

        m_writer->NextElement(mlFlags);

        if (dontWritePrimType)
            m_writer->NextId32(ml.meshIndex);
        else
            m_writer->NextElement(ml.primitiveType);

        if ((mlFlags & Model::MaterialAbsent) == 0)
            m_writer->NextId32(ml.materialIndex);

        if ((mlFlags & Model::MeshTransformAbsent) == 0)
            m_writer->NextElement(ml.meshTransform);

        if ((mlFlags & Model::ColorAbsent) == 0)
            m_writer->NextElement(ml.color);

        if ((model.m_flags & Model::mffSpatialIndices) != 0)
            m_writer->NextUInt32(ml.spatialIndex);

        if (ml.primitiveType == Model::ptSpline && ml.splineData.get())
        {
            m_writer->NextElement(ml.splineData->header);
            m_writer->NextElements(ml.splineData->knots.data(), ml.splineData->header.knotsCount);
        }
        else if (ml.primitiveType > Model::ptMesh && ml.primitiveType != Model::ptSpline)
        {
            uint8_t arraySize = (uint8_t)ml.primitiveData.size();
            m_writer->NextElement(arraySize);

            if (arraySize > 0)
                m_writer->NextElements(ml.primitiveData.data(), arraySize);
        }
    }

}

void ModelFileWriter::WriteMeshes(Model  &model)
{
    //write meshes

    uint32_t meshesNum = (uint32_t)model.m_meshes.size();
    m_writer->NextUInt32(meshesNum);

    for (uint32_t i = 0; i < meshesNum; ++i) WriteMesh(i, model);
    VIS3D_LOG_FDEBUG(L"Write meshes pos " << ftell(m_file->GetHandle()));
}

void ModelFileWriter::WriteMesh(int id, Model  &model)
{
    MeshData& md = model.m_meshes[id]->GetData();

    uint8_t flags = 0;

    switch (md.IndexSize())
    {
    case Model::ByteIndices: flags = Model::ByteIndices; break;
    case Model::ShortIndices: flags = Model::ShortIndices; break;
    }

    const MeshData::Topology * topo = md.GetTopology();
    uint32_t topologyBlockNum = topo == 0 ? 0 : (uint32_t)topo->boxes.size();
    if (topologyBlockNum == 0) flags |= Model::TopologyAbsent;

    if (model.m_meshes[id]->GetPrimitiveType() == Mesh::primitiveTriangles)
        flags |= Model::TriangleListPrim;

    if (md.SubmeshesNum() == 0)
        flags |= Model::SubmeshesAbsent;

    m_writer->NextElement<>(flags);

    uint8_t primType = model.m_meshes[id]->GetPrimitiveType();
    if ((flags & Model::TriangleListPrim) == 0)
        m_writer->NextElement<>(primType);

    int vertexLayoutId = model.m_meshesVertexLayoutsMap[id];
    m_writer->NextUInt32(vertexLayoutId);

    //write data

    uint32_t vertsNum = md.VerticesNum();
    uint32_t indsNum = md.IndicesNum();
    uint32_t submeshesNum = md.SubmeshesNum();

    m_writer->NextUInt32(vertsNum);
    m_writer->NextUInt32(indsNum);
    if ((flags & Model::SubmeshesAbsent) == 0)
        m_writer->NextUInt32(submeshesNum);

    //verts
    size_t vertexSize = md.VertexSize();
    m_writer->GetUnderlyingWriter()->WriteBytes(
        md.GetVerticesBuffer(),
        vertexSize, vertsNum
        );

    //inds
    size_t indexSize = md.IndexSize();
    m_writer->GetUnderlyingWriter()->WriteBytes(
        md.GetIndicesBuffer(),
        indexSize, indsNum
        );

    //submeshes
    if (submeshesNum > 0)
        m_writer->NextElements(md.GetSubmeshes(), submeshesNum);

    //bounds
    m_writer->NextElement(md.GetBounds());

    //area
    m_writer->NextElement(md.GetArea());
    m_writer->NextElement(md.GetAxisAlignedArea());

    //topology
    if (topologyBlockNum > 0)
    {
        m_writer->NextUInt32(topologyBlockNum);
        if (topologyBlockNum > 0)
        {
            m_writer->NextElements(topo->boxes.data(), topologyBlockNum);
            for (uint32_t i = 0; i < topologyBlockNum; ++i)
            {
                uint32_t blockTrisNum = (uint32_t)topo->triangles[i].size();

                V3D_ASSERT(blockTrisNum);
                m_writer->NextUInt32(blockTrisNum);
                for (size_t j = 0; j < blockTrisNum; ++j)
                    m_writer->NextUInt32(topo->triangles[i][j]);
            }
        }
    }
}

void ModelFileWriter::WriteMeshVertexLayouts(Model  &model)
{
    // write layouts
    //model.UpdateVertexLayouts();
    uint32_t vLayoutsNum = (uint32_t)model.m_vertexLayouts.size();

    m_writer->NextUInt32(vLayoutsNum);

    for (uint32_t i = 0; i < vLayoutsNum; ++i)
        WriteMeshVertexLayout(i, model);

    VIS3D_LOG_FDEBUG(L"Write mesh vertex layouts pos " << ftell(m_file->GetHandle()));
}

void ModelFileWriter::WriteMeshVertexLayout(int id, Model  &model)
{
    //write vertex format

    const Model::VERTEX_LAYOUT& vcDescs = model.m_vertexLayouts[id];

    uint8_t vcDescsNum = (uint8_t)vcDescs.size();
    m_writer->NextElement(vcDescsNum);

    if (vcDescsNum == 0) return;
    for (size_t i = 0; i < vcDescs.size(); ++i)
    {
        uint8_t usage = vcDescs[i].usage;
        uint8_t type = vcDescs[i].type;
        m_writer->NextElement(usage);
        m_writer->NextElement(type);
    }

}

void ModelFileWriter::WriteMaterials(Model  &model)
{
    // write materials
    uint32_t matNum = (uint32_t)model.m_materials.size();
    m_writer->NextUInt32(matNum);

    for (uint32_t i = 0; i < matNum; ++i)
        WriteMaterial(i, model);

    VIS3D_LOG_FDEBUG(L"Write materials pos " << ftell(m_file->GetHandle()));
}
void ModelFileWriter::WriteMaterial(int id, Model  &model)
{
    const MaterialData * mat = &model.m_materials[id]->GetData();
    WString subresourceRelPath = model.MakeRelativePathForSubresource(
        model.m_materials[id]->GetFullPath(), model.GetFullPath());
    m_writer->NextString(subresourceRelPath);

    if (subresourceRelPath.length() == 0)
    {
        m_writer->NextString(mat->m_name);
        m_writer->NextString(mat->m_shading);

        float shininess = mat->m_glossiness * 100.f;
        float roughness = 0.5f;

        m_writer->NextElement(mat->m_ambient);
        m_writer->NextElement(mat->m_diffuse);
        m_writer->NextElement(mat->m_specular);
        m_writer->NextElement(mat->m_emission);
        m_writer->NextElement(shininess);
        m_writer->NextElement(mat->m_transparency);
        m_writer->NextElement(roughness);

        uint32_t texNum = (uint32_t)mat->m_textures.size();
        m_writer->NextElement<>(texNum);

        for (uint32_t j = 0; j < texNum; ++j)
        {
            subresourceRelPath = model.MakeRelativePathForSubresource(mat->m_textures[j].name, model.GetFullPath());
            m_writer->NextString(subresourceRelPath); // ! potential bug
            //m_writer->NextString(mat->m_textures[j].name);
            m_writer->NextElement(mat->m_textures[j].weight);
        }
    }

}

void ModelFileWriter::WriteIdRanges(Model  &model)
{
    // write ids ranges
    uint32_t rangesNum = (uint32_t)model.m_idsRanges.size();
    m_writer->NextUInt32(rangesNum);

    for (uint32_t i = 0; i < rangesNum; ++i)
    {
        const UIDsRange& range = model.m_idsRanges[i];
        m_writer->NextId32(range.startId);
        m_writer->NextUInt32(range.count);
    }
}

void ModelFileWriter::WriteBounds(Model  &model)
{
    // write bounds
    m_writer->NextElement<>(model.m_bounds);
}


size_t ModelFileWriter::CalculateModelFileSizeForMaterials(Model  &model)
{
    size_t materialTotalSz = 0;
    size_t materialCount = model.m_materials.size();
    size_t materialStatSz = 4 * sizeof(vec3) + 3 * sizeof(float);
    materialTotalSz += materialCount * materialStatSz;

    for (size_t materialInd = 0; materialInd < materialCount; ++materialInd)
    {
        auto material = model.m_materials[materialInd]->GetData();

        //64
        materialTotalSz += model.MakeRelativePathForSubresource(
            model.m_materials[materialInd]->GetFullPath(), model.GetFullPath()
            ).size() * sizeof(wchar_t);
        materialTotalSz += material.m_name.size() * sizeof(wchar_t);
        materialTotalSz += material.m_shading.size() * sizeof(wchar_t);

        size_t textureCount = material.m_textures.size();
        materialTotalSz += textureCount * sizeof(float);
        for (uint32_t texInd = 0; texInd < textureCount; ++texInd)
        {
            auto texture = material.m_textures[texInd];
            materialTotalSz += model.MakeRelativePathForSubresource(texture.name, model.GetFullPath()).size() * sizeof(wchar_t);
        }
    }

    return materialTotalSz;
}
size_t ModelFileWriter::CalculateModelFileSizeForMesh(Model  &model)
{
    size_t layoutsSize = model.m_vertexLayouts.size() * 2 /** sizeof(uint8_t)*/;

    size_t meshCount = model.m_meshes.size();
    for (size_t meshInd = 0; meshInd < meshCount; ++meshInd)
    {
        auto md = model.m_meshes[meshInd]->GetData();
        uint32_t vertsNum = md.VerticesNum();
        uint32_t indsNum = md.IndicesNum();
        uint32_t submeshesNum = md.SubmeshesNum();

    }

    return 0;

}
size_t ModelFileWriter::CalculateModelFileSizeForNodes(Model  &model)
{
    return 0;
}
size_t ModelFileWriter::CalculateModelFileSizeForAnimation(Model  &model)
{
    return 0;

}

size_t ModelFileWriter::CalculateModelFileSize(Model &)
{
    return 0;

}