
#pragma once
#include <iostream>
#include <memory>
#include <stdint.h>
#include "BasicIO.h"

namespace Visco3D
{
	class Model;
	class BasicWriter
	{
    protected:
		FileWrapper::ShPtr m_file;
        std::unique_ptr<IStreamProducer> streamProducer;

	public:
		BasicWriter(FileWrapper::ShPtr file);
		virtual ~BasicWriter(void);

	public:
		virtual bool WriteBytes(const void *data, size_t elementSize, size_t elementNum);
		bool WriteString(std::wstring const &);
		bool WriteUInt32_7BitEnc(uint32_t);
		bool WriteInt32_7BitEnc(int32_t);
		bool WriteID32_7BitEnc(int32_t);
		bool WriteUInt16(uint16_t);
		bool WriteUInt32(uint32_t);
		bool WriteInt32(int32_t);
		bool WriteID32(int32_t);
	};

	class CompressedWriter
		: public BasicWriter
	{
	public:
		CompressedWriter(FileWrapper::ShPtr file);
		virtual ~CompressedWriter(void);
	public:
		virtual bool WriteBytes(const void *data, size_t elementSize, size_t elementNum) override;
	};

	class ModelFileWriter
	{
	public:
		class IBinaryWriter
		{
		protected:
			// can be either a normal or a compressed writer
			// see: ProceedAsCompressed()
			BasicWriter *m_writer;
			ModelFileWriter &m_streamer;

		public:
			typedef std::shared_ptr<IBinaryWriter> ShPtr;

		public:
			IBinaryWriter(ModelFileWriter &streamer);
			virtual ~IBinaryWriter(void);
			virtual void NextString(std::wstring const &);

			/// those writes can be either encoded or normal
			virtual void NextUInt32(uint32_t) = 0;
			virtual void NextInt32(int32_t) = 0;
			virtual void NextId32(int32_t) = 0;

			inline BasicWriter *GetUnderlyingWriter() { return m_writer; }
			template <typename _PodTy> void NextElement(_PodTy const *dataPtr) { m_writer->WriteBytes(dataPtr, sizeof(_PodTy), 1); }
			template <typename _PodTy> void NextElement(_PodTy const &dataRef) { m_writer->WriteBytes(&dataRef, sizeof(_PodTy), 1); }
			template <typename _PodTy> void NextElements(_PodTy const *dataPtr, size_t arraySize = 1) { m_writer->WriteBytes(dataPtr, sizeof(_PodTy), arraySize); }

			void ProceedAsCompressed(void);
		};
	public:
		ModelFileWriter(void);

        // utility method, that
        // - sets model file path
        // - does model file flags fixes
        // - updates mesh bboxes
        // - updates mesh id ranges
        // - updates mesh vertex layouts
        // ToFix: const Model&! for all WriteXXX functions
        void PrepareForSaving(FileWrapper::ShPtr, Model&);
        void Save(FileWrapper::ShPtr, Model&);
        size_t CalculateModelFileSize(Model&);

	protected:
		FileWrapper::ShPtr m_file;
		IBinaryWriter::ShPtr m_writer; 

		void WriteBounds(Model&);
		void WriteIdRanges(Model&);
		void WriteMaterials(Model&);
		void WriteMeshVertexLayouts(Model&);
		void WriteMeshes(Model&);
		void WriteNodes(Model&);
		void WriteAnimations(Model&);

		void WriteMaterial(int id, Model&);
		void WriteMeshVertexLayout(int id, Model&);
		void WriteMesh(int id, Model&);
		void WriteNode(Model::Node const&, Model&);
		void WriteAnimation(int nodeIdx, const NODE_MESHLINK_ID& id, const WString& nodeName, const NODE_ANIMATION_CHANNELS_VEC& channels, Model&);

		void FinalizeWriting(Model&);

        size_t CalculateModelFileSizeForMaterials(Model&);
        size_t CalculateModelFileSizeForMesh(Model&);
        size_t CalculateModelFileSizeForNodes(Model&);
        size_t CalculateModelFileSizeForAnimation(Model&);


	};
}


