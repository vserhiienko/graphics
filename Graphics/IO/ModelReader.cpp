#include "stdafx.h"

#include <zlib.h>

#include "V3DAssert.h"
#include "Model.h"
#include "ModelReader.h"
#include "Log.h"

#include "V3DTime.h"

template <typename _SimpleTy> static size_t TyZeroMemorySz(_SimpleTy &dataRef) { memset(&dataRef, 0, sizeof(_SimpleTy)); return sizeof(_SimpleTy); }
template <typename _SimpleTy> static size_t TyZeroMemorySz(_SimpleTy *dataRef) { memset(dataRef, 0, sizeof(_SimpleTy)); return sizeof(_SimpleTy); }
#define TyZeroMemory(data) (void)TyZeroMemorySz(data) 



using namespace Visco3D;

#pragma region class BasicReader

BasicReader::BasicReader(FileWrapper::ShPtr file)
	: m_file(file)
{
    streamConsumer.reset(new FileConsumer(*file.get()));
}

BasicReader::~BasicReader()
{
    streamConsumer.reset();
}

bool BasicReader::ReadBytes(void *data, size_t elementSize, size_t elementNum)
{
	//FileWrapper::FileHandle fileHandle = m_file->GetHandle();
	//return elementNum == fread(data, elementSize, elementNum, fileHandle);
    size_t dataSz = elementSize * elementNum;
    return dataSz == streamConsumer->Consume((uint8_t*)data, dataSz);
}

bool BasicReader::ReadUInt16(uint16_t &outValue)
{
	return ReadBytes(&outValue, sizeof(uint16_t), 1);
}
bool BasicReader::ReadUInt32(uint32_t &outValue)
{
	return ReadBytes(&outValue, sizeof(uint32_t), 1);
}
bool BasicReader::ReadUInt32_7BitEnc(uint32_t &value)
{
	const size_t bytesNum = sizeof(uint32_t) + 0;
	value = 0;
	for (size_t bi = 0; bi < bytesNum; ++bi)
	{
		size_t nextbi = bi + 1;
		uint8_t byte;
		ReadBytes(&byte, sizeof(uint8_t), 1);
		bool readNextByte = ((byte >> 7) & 1) == 1;
		uint8_t mask = (nextbi < bytesNum) ? 0x7F : 0xFF;
		value |= ((uint32_t)(byte & mask)) << (bi * 7);
		if (!readNextByte)
			break;
	}
	return true;
}

bool BasicReader::ReadInt32(int32_t &outValue)
{
	return ReadBytes(&outValue, sizeof(int32_t), 1);
}

bool BasicReader::ReadInt32_7BitEnc(int32_t &outValue)
{
	return ReadUInt32_7BitEnc(reinterpret_cast<uint32_t&>(outValue));
}
bool BasicReader::ReadID32(int32_t &outValue)
{
	return ReadBytes(&outValue, sizeof(int32_t), 1);
}
bool BasicReader::ReadID32_7BitEnc(int32_t &outValue)
{
	bool result = ReadInt32_7BitEnc(outValue); --outValue;
	return result;
}

bool BasicReader::ReadString(std::wstring &outValue)
{
	uint16_t strLen = 0;

	// read the string length 
	if (ReadUInt16(strLen) && strLen > 0)
	{
		// try allocate requied memory
		if (wchar_t *strBuff = new wchar_t[strLen + 1])
		{
			bool readingSucceeded = false;
			// try read the string from file into the allocated memory
			if (ReadBytes(strBuff, sizeof(wchar_t), strLen))
			{
				strBuff[strLen] = L'\0'; // set string end pos
				outValue = strBuff;
				readingSucceeded = true;
			}

			delete[] strBuff;
			return readingSucceeded;
		}
	}

	return false;
}

#pragma endregion

#pragma region classes CompressedReader, CompressedReader::Implementation

/*
typedef struct CompressedReader::Implementation
{
	static const size_t chunkSize = 1024 * 1024;

	std::vector<uint8_t> inBuffer;
	std::vector<uint8_t> outBuffer;

	FILE* nativeFile;
	z_stream strm;
	bool read;
	size_t readBuffPos;

    std::string buffer;

    std::string decompress_string(const std::string& str)
    {
        z_stream zs;                        // z_stream is zlib's control structure
        memset(&zs, 0, sizeof(zs));

        if (inflateInit(&zs) != Z_OK)
            //throw(std::runtime_error("inflateInit failed while decompressing."));
            return "";

        zs.next_in = (Bytef*)str.data();
        zs.avail_in = str.size();

        int ret;
        char outbuffer[32768];
        std::string outstring;

        // get the decompressed bytes blockwise using repeated calls to inflate
        do {
            zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
            zs.avail_out = sizeof(outbuffer);

            ret = inflate(&zs, 0);

            if (outstring.size() < zs.total_out) {
                outstring.append(outbuffer,
                    zs.total_out - outstring.size());
            }

        } while (ret == Z_OK);

        inflateEnd(&zs);

        if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
            std::ostringstream oss;
            oss << "Exception during zlib decompression: (" << ret << ") "
                << zs.msg;
            throw(std::runtime_error(oss.str()));
        }

        return outstring;
    }


	Implementation(FILE* file)
	{
#if 0
        read = true;
        readBuffPos = 0;
        nativeFile = file;

        fpos_t pos = ftell(nativeFile);
        fseek(nativeFile, 0, SEEK_END);
        auto end = ftell(nativeFile);
        auto sz = end - pos;

        std::string compressedBuffer;
        compressedBuffer.resize(sz);

        fsetpos(nativeFile, &pos);
        fread((void*)compressedBuffer.data(), 1, sz, nativeFile);
        buffer = decompress_string(compressedBuffer);

#else

		inBuffer.reserve(chunkSize);
		outBuffer.reserve(chunkSize);
		readBuffPos = 0;

		strm.zalloc = Z_NULL;
		strm.zfree = Z_NULL;
		strm.opaque = Z_NULL;

		strm.next_in = NULL;
		strm.avail_in = NULL;
		strm.next_out = NULL;
		strm.avail_out = NULL;

		strm.next_in = reinterpret_cast<uint8_t *>(inBuffer.data());
		strm.avail_in = chunkSize;
		strm.next_out = reinterpret_cast<uint8_t *>(outBuffer.data());
		strm.avail_out = chunkSize;

		int result = Z_OK;
		if (read)
		{

			result = inflateInit(&strm);
            //inflat
			inBuffer.resize(chunkSize);
            FlushRead(); //+
		}
		else
		{
			result = deflateInit(&strm, Z_BEST_COMPRESSION);
			outBuffer.resize(chunkSize);
		}
		V3D_ASSERT(result);
#endif
	}

	~Implementation()
	{
		// for the ModelReader 'read' will always be set to 'true'
		// so the redundant branching was removed, along with the 'Write' and 'FlushWrite' methods
#if 0
		(void)inflateEnd(&strm);
#endif
	}

	bool Read(void *out_data, size_t out_data_size)
	{
#if 0
        auto data = (const char*)buffer.data();
        memcpy(out_data, data + readBuffPos, out_data_size);
        readBuffPos += out_data_size;

#else 
		while (out_data_size != 0)
		{
            //size_t bytesLeft = inBuffer.size() - readBuffPos;
            size_t bytesLeft = outBuffer.size() - readBuffPos;

			size_t data_size_to_copy = out_data_size;
			if (data_size_to_copy > bytesLeft)
			{
				//data_size_to_copy = bytesLeft;
				return false;
			}

            memcpy(out_data, &outBuffer[readBuffPos], data_size_to_copy);

			readBuffPos += data_size_to_copy;
			out_data_size -= data_size_to_copy;
			out_data = &((uint8_t*)out_data)[data_size_to_copy];

			if (readBuffPos == outBuffer.size())
				FlushRead();

			return true;
		}

		return false;

#endif

        return true;
	}

	void FlushRead(bool doFlush = false)
	{
		outBuffer.resize(chunkSize);

		int ret;
		unsigned have;

		// decompress until deflate stream ends or end of file
		do {
			strm.avail_in = (uInt)fread(inBuffer.data(), 1, chunkSize, nativeFile);
			if (ferror(nativeFile)) {
				(void)inflateEnd(&strm);
				V3D_ASSERT(false);
				break;
			}
			if (strm.avail_in == 0)
				break;
			strm.next_in = inBuffer.data();

			// run inflate() on input until output buffer not full
			do {
				strm.avail_out = (uInt)outBuffer.size();
				strm.next_out = outBuffer.data();
				ret = inflate(&strm, Z_NO_FLUSH);
				V3D_ASSERT(ret != Z_STREAM_ERROR);  //* state not clobbered
				switch (ret) {
				case Z_NEED_DICT:
					ret = Z_DATA_ERROR;     //* and fall through
				case Z_DATA_ERROR:
				case Z_MEM_ERROR:
					(void)inflateEnd(&strm);
					V3D_ASSERT(false);
					break;
				}
				have = chunkSize - strm.avail_out;
			} while (strm.avail_out == 0);



			//* done when inflate() says it's done
		} while (ret != Z_STREAM_END);
	}

} ZipFile;
*/


CompressedReader::CompressedReader(FileWrapper::ShPtr file)
	: BasicReader(file)
	//, m_impl(new ZipFile(file->GetHandle()))
{
    streamConsumer.reset(new LzmaFileAsyncConsumer(*file.get()));
    //streamConsumer.reset(new LzmaFileConsumer(*file.get()));
    //streamConsumer.reset(new ZFileConsumer(*file.get()));
}

CompressedReader::~CompressedReader()
{
    streamConsumer.reset();
	//delete m_impl;
}

bool CompressedReader::ReadBytes(void *data, size_t elementSize, size_t elementNum)
{
    return streamConsumer->Consume((uint8_t*)data, elementSize * elementNum);
    //return m_impl->Read(data, elementSize * elementNum);
}

#pragma endregion

namespace Visco3DImpl
{
	static vec4ubn emptyColor(1.0f, 1.0f, 1.0f, 1.0f);

#pragma region class DefaultReader

	class DefaultReader : public ModelFileReader::IBinaryReader
	{
	public:
		DefaultReader(ModelFileReader &s)
			: IBinaryReader(s)
		{
		}
		virtual uint32_t NextUInt32(void)
		{
			uint32_t outValue;
			m_reader->ReadUInt32(outValue);
			return outValue;
		}
		virtual int32_t NextInt32(void)
		{
			int32_t outValue;
			m_reader->ReadInt32(outValue);
			return outValue;
		}
		virtual int32_t NextId32(void)
		{
			int32_t outValue;
			m_reader->ReadID32(outValue);
			return outValue;
		}

	};

#pragma endregion

#pragma region class Enc7BitReader

	class Enc7BitReader : public ModelFileReader::IBinaryReader
	{
	public:
		Enc7BitReader(ModelFileReader &s)
			: IBinaryReader(s)
		{
		}
		virtual uint32_t NextUInt32(void)
		{
			uint32_t outValue;
			m_reader->ReadUInt32_7BitEnc(outValue);
			return outValue;
		}
		virtual int32_t NextInt32(void)
		{
			int32_t outValue;
			m_reader->ReadInt32_7BitEnc(outValue);
			return outValue;
		}
		virtual int32_t NextId32(void)
		{
			int32_t outValue;
			m_reader->ReadID32_7BitEnc(outValue);
			return outValue;
		}

	};

#pragma endregion

}

using namespace Visco3DImpl;

#pragma region class ModelFileReader::IBinaryReader

ModelFileReader::IBinaryReader::IBinaryReader(ModelFileReader &streamer)
	: m_streamer(streamer)
{
	m_reader = new BasicReader(streamer.m_file);
}

ModelFileReader::IBinaryReader::~IBinaryReader(void)
{
	SAFE_DELETE_PTR(m_reader);
}

std::wstring ModelFileReader::IBinaryReader::NextString(void)
{
	std::wstring text;
	m_reader->ReadString(text);
	return text;
}

void ModelFileReader::IBinaryReader::ProceedAsCompressed(void)
{
	BasicReader *compressReader = new CompressedReader(m_streamer.m_file);
	SAFE_DELETE_PTR(m_reader);
	m_reader = compressReader;
}

#pragma endregion 

#pragma region class ModelFileReader 

ModelFileReader::ModelFileReader(void)
	: m_fileV(0)
{
}

bool ModelFileReader::Is7BitEncEnabled(int32_t v){ return v >= Model::kModelFileVersion_7BitEncIntroduced; }
bool ModelFileReader::IsVersionSupported(int32_t v){ return v >= Model::kModelFileVersion_MinSupported; }

int32_t ModelFileReader::ReadFileVersion(BasicReader &reader)
{
	int32_t version = 0;
	if (reader.ReadInt32(version)) return version;
	return Model::kModelFileVersion_Invalid;
}

bool ModelFileReader::CheckSign(BasicReader &reader)
{
	char modelSign[4];
	if (reader.ReadBytes(modelSign, ARRAYSIZE(Model::kModelSign), 1))
		return strncmp(modelSign, Model::kModelSign, ARRAYSIZE(Model::kModelSign)) == 0;
	return false;
}

//struct ReadFlagsStrategyV16 : public ModelFileReader::IReaderStrategy
//{
//public: virtual bool Read(Model &model) { model.m_flags = m_streamer.GetFileReader()->NextUInt32(); }
//};
//
//struct ReadFlagsStrategy : public ModelFileReader::IReaderStrategy
//{
//public: virtual bool Read(Model &model) { }
//};

void ModelFileReader::BuildStrategies(void)
{
	// if version is greater or equal 16, 
	// assign ReadFlagsStrategyV16 strategy
	// and so forth...
}

ModelFileReader::LoadError ModelFileReader::Load(FileWrapper::ShPtr file, Model &model)
{
    auto secs = Stopwatch::GetGlobalSeconds();

	m_file = file;
	{
		BasicReader reader(file);
		if (!CheckSign(reader))
			return eLoadError_SignMismatch;
		m_fileV = ReadFileVersion(reader);
	}

	if (!IsVersionSupported(m_fileV))
		return eLoadError_DeprecatedVersion;

	if (Is7BitEncEnabled(m_fileV))
		m_reader = std::make_shared<Enc7BitReader>(*this);
	else
		m_reader = std::make_shared<DefaultReader>(*this);

	model.m_version = m_fileV;
    TyZeroMemorySz(model.m_ModelLoadStatisic);
	model.SetFullPath(file->GetFullPath().c_str());

	//BuildStrategies();
	// then run strategies...
	// or

	ReadModelFlags(model);
	ReadIdRanges(model);
    ReadMaterials(model);
    HandleZCompressionFlag(model);
	ReadMeshVertexLayouts(model);
	ReadMeshes(model);
	ReadNodes(model);
	ReadAnimations(model);
	ReadSkeletons(model);
	FinalizeLoading(model);

    auto elapsed = Stopwatch::GetGlobalSeconds() - secs;

    visco_debugTrace("ModelFileReader::Load: "
        "tris %u vertices %u indices %u splines %u nodes %u\n"
        "\t\t\t elapsed time = %f"
        , model.m_ModelLoadStatisic.NumTriangles
        , model.m_ModelLoadStatisic.NumVertices
        , model.m_ModelLoadStatisic.NumIndices
        , model.m_ModelLoadStatisic.NumSplines
        , model.m_ModelLoadStatisic.NumVisualNodes
        , elapsed
        );

	return eLoadError_Success;
}

void ModelFileReader::ReadIdRanges(Model &model)
{
	uint64_t compressedDataSize = 0;
	if (m_fileV >= 24)
	{
		model.m_uuid = m_reader->NextElement<uuid_t>();

		if (model.m_flags & model.mffZCompression)
			compressedDataSize = m_reader->NextElement<uint64_t>();
		if (model.m_flags & model.mffContainsRootNode)
			model.m_rootNodeId = m_reader->NextId32();

		//read bounds

		m_reader->NextElement(model.m_bounds);
		model.m_boundsNeedUpdate = false;

		//read ids ranges

		uint32_t rangesNum = m_reader->NextUInt32();
		model.m_idsRanges.resize((size_t)rangesNum);

		for (uint32_t i = 0; i < rangesNum; ++i)
		{
			UIDsRange& range = model.m_idsRanges[i];
			range.startId = m_reader->NextUInt32();
			range.count = m_reader->NextUInt32();
		}
	}
	else
	{
		UuidCreate(&model.m_uuid);

		uint8_t spatialIndicesType = 0;
		if (model.m_version >= 22)
			spatialIndicesType = m_reader->NextElement<uint8_t>();
		if (spatialIndicesType != 0)
			model.m_flags |= model.mffSpatialIndices;
	}
}

void ModelFileReader::FinalizeLoading(Model &model)
{
	using namespace std;

	m_reader.reset();
	m_file.reset();

	for (set<int>::const_iterator itMshIdx = model.m_meshesToCalcArea.begin(); itMshIdx != model.m_meshesToCalcArea.end(); ++itMshIdx)
	{
		model.m_meshes[*itMshIdx]->GetData().CalculateArea();
		model.m_meshes[*itMshIdx]->GetData().CalcCoverageIntegrity();
	}
}

void ModelFileReader::ReadSkeletons(Model &model)
{
	if (model.m_version >= 18)
	{
		uint32_t skeletonsNum = m_reader->NextUInt32();

		if (skeletonsNum > 0)
		{
			for (size_t skIdx = 0; skIdx < skeletonsNum; ++skIdx)
			{
				uint32_t bonesNum = m_reader->NextUInt32();

				Model::SKELETON_PTR skeleton(new Model::Skeleton(bonesNum));
				m_reader->NextElements(skeleton->boneNodesIndices.data(), bonesNum);
				m_reader->NextElements(skeleton->bonesBounds.data(), bonesNum);
				model.AddSkeleton(skeleton);
			}

			int nodesSkeletonMapSize = m_reader->NextUInt32();
			for (int i = 0; i < nodesSkeletonMapSize; ++i)
			{
				int nodeIdx = m_reader->NextId32();
				int skIdx = m_reader->NextId32();
				model.AssignNodeSkeleton(nodeIdx, skIdx);
			}
		}
	}

	if (model.m_version >= 21)
	{
		int lookAtLinksNum = m_reader->NextUInt32();

		if (lookAtLinksNum > 0)
		{
			for (size_t i = 0; i < lookAtLinksNum; ++i)
			{
				int nodeIdx = m_reader->NextId32();
				int lookAtTarget = m_reader->NextId32();
				model.m_lookAtLinksMap[nodeIdx] = lookAtTarget;
			}
		}
	}
}

void ModelFileReader::ReadAnimation(Model &model)
{
	int nodeIdx = m_reader->NextUInt32();

	NODE_MESHLINK_ID id(INVALID_UID, 0xFFFFFFFF);
	if (model.m_version >= 24)
	{
		id.first = m_reader->NextId32();
		if (id.first != INVALID_UID)
			id.second = m_reader->NextId32();
	}

	WString nodeName;
	if (model.m_version >= 20)
		nodeName = m_reader->NextString();
	else
		nodeName = model.m_nodes[nodeIdx].name;

	uint32_t channelsNum = m_reader->NextUInt32();
	NODE_ANIMATION_CHANNELS_VEC channels(channelsNum);

	for (uint32_t k = 0; k < channelsNum; ++k)
	{
		NodeAnimationChannel& channel = channels[k];
		channel.targetMeshLinkIdx = 0xFFFFFFFF;
		if (id.first != INVALID_UID)
			channel.targetMeshLinkIdx = id.second;

		channel.targetComponents = m_reader->NextUInt32();

		//process track

		//process general track params

		int framesNum = m_reader->NextUInt32();
		int targetComponentsNum = m_reader->NextUInt32();
		int erpType = m_reader->NextUInt32();

		channel.frameRate = model.m_frameRate;

		channel.animTrack.reset(new AnimationTrack(framesNum, targetComponentsNum));
		AnimationTrack& track = *channel.animTrack.get();

		V3D_ASSERT(track.IsValid());

		track.m_erpType = (AnimationTrack::InterpolationType)erpType;

		//process frames data

		const size_t floatSize = sizeof(float);

		m_reader->NextElements(track.m_timeline.data(), framesNum);

		for (int j = 0; j < framesNum; ++j)
			m_reader->NextElements(track.m_framesData[j].data(), targetComponentsNum);

		//process tangents (used for bezier, hermite, etc. interpolations)

		if (model.m_version <= 20)
		{
			int tangentsNum = 0;
			tangentsNum = m_reader->NextInt32();
			track.m_inTangents.resize(tangentsNum);

			if (tangentsNum > 0)
				m_reader->NextElements(track.m_inTangents.data(), tangentsNum);
		}
		else
		{
			if (track.m_erpType != AnimationTrack::LINEAR && track.m_erpType != AnimationTrack::STEP)
			{
				track.InitTangents();
				for (int j = 0; j < framesNum; ++j)
				{
					m_reader->NextElements(track.m_inTangents[j].data(), targetComponentsNum);
					m_reader->NextElements(track.m_outTangents[j].data(), targetComponentsNum);
				}
			}
			if (track.m_erpType == AnimationTrack::MIXED)
			{
				track.m_mixedErpTypes.resize(framesNum);
				m_reader->NextElements(track.m_mixedErpTypes.data(), targetComponentsNum);
			}
		}
	}

	if (nodeIdx >= 0)
		model.m_animations[nodeIdx] = channels;
	else
	{
		if (id.first != INVALID_UID)
			model.m_animationsById[id] = channels;
		else
			model.m_animationsNamed[nodeName] = channels;
	}
}

void ModelFileReader::ReadAnimations(Model &model)
{
	uint32_t animatedNodesNum = m_reader->NextUInt32();
	if (animatedNodesNum > 0 && model.m_version >= 19)
		model.m_frameRate = m_reader->NextElement<float>();

	for (uint32_t i = 0; i < animatedNodesNum; ++i)
		ReadAnimation(model);
}

void ModelFileReader::ReadMeshLinkGeometry(Model::Node& node, Model::MeshLink& ml, Model &model)
{
	using namespace std;
	if (ml.primitiveType == Model::ptNoPrimitive ||
		ml.primitiveType == Model::ptMesh)
	{
		return;
	}

	model.m_ModelLoadStatisic.NumPrimitives++;


	if (ml.primitiveType == Model::ptSpline)
	{
		SplineDesc * splineData = new SplineDesc;
		ml.splineData.reset(splineData);
		splineData->header = m_reader->NextElement<SplineDesc::Header>();
		splineData->knots.resize(splineData->header.knotsCount);

        if (model.m_version <= 23)
        {
            struct SplineKnotV23
            {
                vec3 pos;
                vec3 inVec;
                vec3 outVec;
                int16_t matId;
            };

            vector<SplineKnotV23> splineKnots(splineData->header.knotsCount);
            m_reader->NextElements(splineKnots.data(), splineKnots.size());

            for (size_t i = 0; i < splineKnots.size(); ++i)
            {
                splineData->knots[i].pos = splineKnots[i].pos;
                splineData->knots[i].inVec = splineKnots[i].inVec;
                splineData->knots[i].outVec = splineKnots[i].outVec;
            }
        }
        else
        {
            m_reader->NextElements(splineData->knots.data(), splineData->header.knotsCount);
        }

		if (splineData->header.knotsCount > MeshData::MAX_SPLINE_KNOTS_NUM)
		{
			int subSplineIndex = -1;
			wstring objName = node.name;
			VIS3D_LOG_WARN(L"Object \"" << objName << "\" has spline(" << subSplineIndex << L") with " << splineData->header.knotsCount << L" knots! This is more than maximum allowed of " << MeshData::MAX_SPLINE_KNOTS_NUM << " knots.");
		}

		model.m_ModelLoadStatisic.NumSplines++;
	}
	else
	{
        size_t arraySize = (size_t)m_reader->NextElement<uint8_t>();
		if (arraySize > 0)
		{
			ml.primitiveData.resize(arraySize);
			m_reader->NextElements(ml.primitiveData.data(), arraySize);
		}

		model.SetPrimitiveStatistic(ml.primitiveType);
	}
}

void ModelFileReader::ReadNodes(Model &model)
{
	//read nodes

	uint32_t nodesNum = m_reader->NextUInt32();
	model.m_ModelLoadStatisic.NumVisualNodes = nodesNum;
	model.m_nodes.resize(nodesNum);

	for (uint32_t i = 0; i < nodesNum; ++i)
	{
		Model::Node& node = model.m_nodes[i];
		ReadNode(node, model);
	}

	VIS3D_LOG_FDEBUG(L"Read nodes pos " << ftell(m_file->GetHandle()));

	if (model.m_version >= 24)
	{
		int nodeIdxOffs = 0;
		for (int i = 0; i < model.m_idsRanges.size(); ++i)
		{
			const UIDsRange& range = model.m_idsRanges[i];

			if (range.startId != INVALID_UID)
			{
				for (uint32_t j = 0; j < range.count; ++j)
				{
					Model::Node& node = model.m_nodes[nodeIdxOffs + j];
					node.specifiedId = range.startId + j;
				}
			}

			nodeIdxOffs += range.count;
		}
	}

	mat4 flipYZ = mat4(
		1, 0, 0, 0,
		0, 0, 1, 0,
		0, 1, 0, 0,
		0, 0, 0, 1
		);

	if (model.m_version < 23 && !model.m_nodes.empty())
	{
		if (model.m_nodesParts[Model::NODE_PART_TRANSFORM] == 0)
		{
			for (uint32_t i = 0; i < nodesNum; ++i)
			{
				Model::Node& node = model.m_nodes[i];

				for (int j = 0; j < node.meshLinks.size(); ++j)
				{
					Model::MeshLink& ml = node.meshLinks[j];
					ml.meshTransform = (mat4x3)(flipYZ * ((mat4)ml.meshTransform));
				}
			}
		}
		else
		{
			model.m_nodes[0].transform = (mat4x3)(flipYZ * ((mat4)model.m_nodes[0].transform));
		}
	}
}

void ModelFileReader::ReadNode(Model::Node &node, Model &model)
{
	uint8_t nodeFlagsByte = 0;
	if (model.m_version >= 16)
		nodeFlagsByte = m_reader->NextElement<uint8_t>();
	node.flags = (int)nodeFlagsByte;

	if ((node.flags & (model.m_version > 23 ? Model::NameAbsent : Model::NameAbsentV23)) == 0)
		node.name = m_reader->NextString();

	if ((node.flags & (model.m_version > 23 ? Model::UserPropertiesAbsent : Model::UserPropertiesAbsentV23)) == 0)
		node.userProps = m_reader->NextString();

	if (model.m_version <= 23)
	{
		node.meshLinks.resize(1);
		Model::MeshLink& ml = node.meshLinks[0];

		if ((node.flags & Model::ColorAbsentV23) == 0)
			m_reader->NextElement(ml.color);

		if ((node.flags & Model::MaterialAbsentV23) == 0)
			ml.materialIndex = m_reader->NextElement<int>(); //!!! NextInt32()
		else
			ml.materialIndex = -1;

		if ((node.flags & Model::MeshAbsentV23) == 0)
		{
			ml.meshIndex = m_reader->NextElement<int>(); //!!! NextInt32()
			ml.primitiveType = Model::ptMesh;
		}
		else
		{
			ml.primitiveType = Model::ptNoPrimitive;
			ml.meshIndex = Model::ptNoPrimitiveV23;
		}

		SET_FLAG(ml.flags, Model::PrimitiveTypeAbsent, ml.meshIndex >= Model::ptNoPrimitiveV23);
		if (ml.meshIndex < Model::ptNoPrimitiveV23)
		{
			ml.primitiveType = (-ml.meshIndex);
			ml.meshIndex = -1;
		}

		bool hasGeometry = ml.primitiveType > Model::ptMesh || (ml.primitiveType == Model::ptMesh && ml.meshIndex >= 0);
        if (hasGeometry)
            model.m_ModelLoadStatisic.NumVisualNodes++;
        else
            model.m_ModelLoadStatisic.NumGroupingNodes++;

		node.parentNodeIndex = m_reader->NextElement<int>();
		++model.m_nodesParts[Model::NODE_PART_HIERARCHY];

		if ((node.flags & Model::TransformAbsentV23) == 0)
		{
			mat4 transform = m_reader->NextElement<mat4>();
			node.transform = (mat4x3)transform;
			++model.m_nodesParts[Model::NODE_PART_TRANSFORM];
		}
		else
			node.transform = mat4x3(1);

		if (model.m_version >= 15 &&
			hasGeometry &&
			((node.flags & Model::MeshTransformAbsentV23) == 0))
		{
			mat4 transform = m_reader->NextElement<mat4>();
			ml.meshTransform = (mat4x3)transform;
		}
		else
			ml.meshTransform = mat4x3(1);

		if (model.m_version >= 22 && hasGeometry && (model.m_flags & Model::mffSpatialIndices) != 0)
			ml.spatialIndex = m_reader->NextElement<uint32_t>();
		else
			ml.spatialIndex = 0;

		node.specifiedId = INVALID_UID;

		ReadMeshLinkGeometry(node, ml, model);

		if (!hasGeometry)
			node.meshLinks.clear();
		else
			++model.m_nodesParts[Model::NODE_PART_MESHLINKS];
	}
	else
	{
		if ((node.flags & Model::ParentIdAbsent) == 0)
		{
			node.parentNodeIndex = m_reader->NextId32();
			if (node.parentNodeIndex >= 0)
				++model.m_nodesParts[Model::NODE_PART_HIERARCHY];
		}
		else
			node.parentNodeIndex = -1;

		if ((node.flags & Model::SpecifiedIdAbsent) == 0)
			node.specifiedId = m_reader->NextId32();
		else
			node.specifiedId = INVALID_UID;

		if ((node.flags & Model::TransformAbsent) == 0)
		{
			node.transform = m_reader->NextElement<mat4x3>();
			++model.m_nodesParts[Model::NODE_PART_TRANSFORM];
		}
		else
			node.transform = mat4x3(1);

		uint32_t meshLinksNum = m_reader->NextUInt32();
		node.meshLinks.resize(meshLinksNum);

		if (meshLinksNum != 0)
			model.m_nodesParts[Model::NODE_PART_MESHLINKS] += meshLinksNum;

		for (uint32_t meshLinkIdx = 0; meshLinkIdx < meshLinksNum; ++meshLinkIdx)
		{
			Model::MeshLink& ml = node.meshLinks[meshLinkIdx];

			uint8_t mlFlagsByte = m_reader->NextElement<uint8_t>();
			ml.flags = mlFlagsByte;

			bool isMesh = (mlFlagsByte & Model::PrimitiveTypeAbsent) != 0;

			if (isMesh)
			{
				ml.meshIndex = m_reader->NextId32();
				ml.primitiveType = Model::ptMesh;
			}
			else
			{
				ml.primitiveType = m_reader->NextElement<uint8_t>();
				ml.meshIndex = -1;
			}

			bool hasGeometry = (isMesh && ml.meshIndex >= 0) || (!isMesh && ml.primitiveType != Model::ptNoPrimitive);
			if (!hasGeometry)
				V3D_ASSERT(hasGeometry);

			if ((ml.flags & Model::MaterialAbsent) == 0)
				ml.materialIndex = m_reader->NextId32();
			else
				ml.materialIndex = -1;

			if ((ml.flags & Model::MeshTransformAbsent) == 0)
			{
				mat4x3 transform = m_reader->NextElement<mat4x3>();
				ml.meshTransform = (mat4x3)transform;
			}
			else
				ml.meshTransform = mat4x3(1);

			if ((ml.flags & Model::ColorAbsent) == 0)
				ml.color = m_reader->NextElement<vec4ubn>();
			else
				ml.color = emptyColor;

			if ((model.m_flags & Model::mffSpatialIndices) != 0)
				ml.spatialIndex = m_reader->NextUInt32();
			else
				ml.spatialIndex = 0;

			ReadMeshLinkGeometry(node, ml, model);
		}
	}
}

void ModelFileReader::ReadMeshes(Model &model)
{
	//read meshes

	uint32_t meshesNum = m_reader->NextUInt32();
	model.m_ModelLoadStatisic.NumMeshes = meshesNum;
	model.SetMeshesCount(meshesNum);
	for (uint32_t i = 0; i < meshesNum; ++i) ReadMesh(i, model);

	VIS3D_LOG_FDEBUG(L"Read meshes pos " << ftell(m_file->GetHandle()));
}

void ModelFileReader::ReadMesh(int id, Model &model)
{
	using namespace std;
	MeshData& md = model.m_meshes[id]->GetData();

	uint8_t flags = 0;
	if (model.m_version > 15)
	{
		flags = m_reader->NextElement<uint8_t>();

		uint8_t primType = Model::TriangleList;
		if (model.m_version <= 23 || (flags & Model::TriangleListPrim) == 0)
			primType = m_reader->NextElement<uint8_t>();
		V3D_ASSERT(primType == Model::TriangleList);

		if (model.m_version <= 23)
		{
			uint8_t vertexCommonLayout = 0;
			vertexCommonLayout = m_reader->NextElement<uint8_t>();

			switch (vertexCommonLayout)
			{
			case Model::Custom:
			{
				uint8_t vcDescsNum = 0;
				vcDescsNum = m_reader->NextElement<uint8_t>();
				assert(vcDescsNum != 0);
				vector<VertexComponentDesc> vcDescs(vcDescsNum);
				m_reader->NextElements(vcDescs.data(), vcDescs.size());
				md.DescribeVertexFormat(vcDescs.data(), (uint32_t)vcDescs.size());
			}
				break;
			case Model::PositionNormal:
				md.DescribeVertexFormat(VERTEX_PN, 2);
				break;
			case Model::PositionNormalTexture:
				md.DescribeVertexFormat(VERTEX_PNT, 3);
				break;
			default:
				assert(false);
			}
		}
		else
		{
            uint32_t vertexLayoutId = m_reader->NextUInt32();

            /*visco_debugTrace("ReadMesh: id %d vertex format %u"
                , id, vertexLayoutId
                );*/

			Model::VERTEX_LAYOUT& vcDescs = model.m_vertexLayouts[vertexLayoutId];
			md.DescribeVertexFormat(vcDescs.data(), (uint32_t)vcDescs.size());
		}

		size_t indexSize = 4;
		Model::ModelFileMeshFlags indicesType = (Model::ModelFileMeshFlags)(flags & Model::IndicesMask);
		switch (indicesType)
		{
		case Model::IntegerIndices:		indexSize = 4; break;
		case Model::ByteIndices:		indexSize = 1; break;
		case Model::ShortIndices:		indexSize = 2; break;
		}

		md.SetIndexSize(indexSize);
	}
	else
	{
		uint8_t vcDescsNum = 0;
		vcDescsNum = m_reader->NextElement<uint8_t>();
		if (vcDescsNum == 0) return;
		vector<VertexComponentDesc> vcDescs(vcDescsNum);
		m_reader->NextElements(vcDescs.data(), vcDescs.size());
		md.DescribeVertexFormat(vcDescs.data(), (uint32_t)vcDescs.size());
	}

	uint32_t vertsNum = m_reader->NextUInt32();
	uint32_t indsNum = m_reader->NextUInt32();
	uint32_t submeshesNum = 0;
	if ((flags & Model::SubmeshesAbsent) == 0)
		submeshesNum = m_reader->NextUInt32();

	model.m_ModelLoadStatisic.NumVertices = vertsNum;
	model.m_ModelLoadStatisic.NumIndices = indsNum;

	md.CreateEmpty(vertsNum, indsNum, submeshesNum);

	//verts
	size_t vertexSize = md.VertexSize();
	m_reader->GetUnderlyingReader()->ReadBytes(md.GetVerticesBuffer(), vertexSize, vertsNum);

	//inds
	size_t indexSize = md.IndexSize();
	m_reader->GetUnderlyingReader()->ReadBytes(md.GetIndicesBuffer(), indexSize, indsNum);


	//submeshes
	if (submeshesNum > 0)
		m_reader->NextElements(md.GetSubmeshes(), submeshesNum);

	//bounds
	m_reader->NextElement(md.GetBounds());

	if (model.m_version >= 22)
	{
		//area
		md.GetArea() = m_reader->NextElement<float>();
		md.GetAxisAlignedArea() = m_reader->NextElement<vec3>();
	}

    /*visco_debugTrace("ReadMesh: area %f, aa area %f %f %f"
        , md.GetArea()
        , md.GetAxisAlignedArea().x
        , md.GetAxisAlignedArea().y
        , md.GetAxisAlignedArea().z
        );*/

	if (model.m_version >= 14 &&
		(model.m_version < 16 ||
		(flags & Model::TopologyAbsent) == 0))
	{
		//topology
		uint32_t topologyBlockNum = m_reader->NextUInt32();
		if (topologyBlockNum > 0)
		{
			MeshData::Topology * topo = md.CreateTopology(topologyBlockNum);
			m_reader->NextElements(topo->boxes.data(), topologyBlockNum);
			for (uint32_t i = 0; i < topologyBlockNum; ++i)
			{
				uint32_t blockTrisNum = m_reader->NextUInt32();
				V3D_ASSERT(blockTrisNum);

				topo->triangles[i].resize(blockTrisNum);
				for (size_t j = 0; j < blockTrisNum; ++j)
					topo->triangles[i][j] = m_reader->NextUInt32();
			}
		}
	}

	model.m_meshes[id]->Initialize();
}

void ModelFileReader::ReadMeshVertexLayouts(Model &model)
{
	//read mesh vertex layouts
	if (model.m_version >= 24)
	{
		uint32_t vLayoutsNum = m_reader->NextUInt32();
		model.m_vertexLayouts.resize(vLayoutsNum);

		for (uint32_t i = 0; i < vLayoutsNum; ++i)
			ReadMeshVertexLayout(i, model);
	}

	VIS3D_LOG_FDEBUG(L"Read mesh vertex layouts pos " << ftell(m_file->GetHandle()));
}

void ModelFileReader::ReadMeshVertexLayout(int id, Model &model)
{
	//read vertex format
	Model::VERTEX_LAYOUT& vcDescs = model.m_vertexLayouts[id];

	uint8_t vcDescsNum = 0;
	vcDescsNum = m_reader->NextElement<uint8_t>();
	V3D_ASSERT(vcDescsNum);

	vcDescs.resize(vcDescsNum);
	for (size_t i = 0; i < vcDescs.size(); ++i)
	{
		uint8_t usage;
		uint8_t type;
		usage = m_reader->NextElement<uint8_t>();
		type = m_reader->NextElement<uint8_t>();
		vcDescs[i].usage = (VertexComponentUsage)usage;
		vcDescs[i].type = (VertexComponentType)type;
	}
}

void ModelFileReader::ReadMaterials(Model &model)
{
	//read materials

	uint32_t matNum = m_reader->NextUInt32();
	model.m_materials.resize(matNum, NULL);
	for (uint32_t i = 0; i < matNum; ++i)
		ReadMaterial(i, model);

	if (!matNum)
	{
		//m_materials.resize(1);
		//m_materials[0] = m_materialStorage->GetOrLoadResource(L"Default.visco3dmat.xml");
	}

	VIS3D_LOG_FDEBUG(L"Read materials pos " << ftell(m_file->GetHandle()));

}

void ModelFileReader::ReadMaterial(int id, Model &modelRef)
{
	Model *model = &modelRef;

	WString matFileName;
	matFileName = m_reader->NextString();
	if (matFileName.length() > 0)
	{
		//external material, load from storage

		matFileName = model->MakeAbsolutePathForSubresource(matFileName, model->GetFullPath());

		model->m_materials[id] = model->m_materialStorage->GetOrLoadResource(matFileName.c_str());

		if (model->m_materials[id] == NULL)
		{
			VIS3D_LOG_ERROR("Model::ReadMaterial: Failed to load material \"" << matFileName.c_str() << "\"!!!")
		}
	}
	else
	{
		//local material, load it from model file

		model->m_materials[id] = model->GetMaterialFactory()->Create();
		model->m_materials[id]->SetReferenceDelegate(model);

		MaterialData * mat = &model->m_materials[id]->GetDataForWriting();

		mat->m_name = m_reader->NextString();
		mat->m_shading = m_reader->NextString();

		float shininess = 0;
		float roughness = 0;

		mat->m_ambient = m_reader->NextElement<vec3>();
		mat->m_diffuse = m_reader->NextElement<vec3>();
		mat->m_specular = m_reader->NextElement<vec3>();
		mat->m_emission = m_reader->NextElement<vec3>();
		shininess = m_reader->NextElement<float>();
		mat->m_transparency = m_reader->NextElement<float>();
		roughness = m_reader->NextElement<float>();

		mat->m_glossiness = shininess / 100.f;

		//vec3 smallValue(0.05f, 0.05f, 0.05f);
		//if(glm::all(glm::lessThanEqual(mat->m_diffuse, smallValue)))
		//	mat->m_diffuse = vec3(1,1,1);

		mat->m_ambient.r = glm::clamp(mat->m_ambient.r, 0.0f, 0.5f);
		mat->m_ambient.g = glm::clamp(mat->m_ambient.g, 0.0f, 0.5f);
		mat->m_ambient.b = glm::clamp(mat->m_ambient.b, 0.0f, 0.5f);

		int texNum = 0;
		texNum = m_reader->NextInt32();
		mat->m_textures.resize(texNum);

		for (int j = 0; j < texNum; ++j)
		{
			mat->m_textures[j].name = m_reader->NextString();
			mat->m_textures[j].weight = m_reader->NextElement<float>();
			mat->m_textures[j].name = model->MakeAbsolutePathForSubresource(mat->m_textures[j].name, model->GetFullPath());
		}

		//if(texNum <= MaterialData::texBump || mat->m_textures[MaterialData::texBump].name.length() == 0)
		//	mat->SetTexture(MaterialData::texBump, L"default_normal_map.png");

		model->m_materials[id]->Initialize();
	}
}

void ModelFileReader::HandleZCompressionFlag(Model &model)
{
	//start decompression from here
	if (model.m_flags & model.mffZCompression)
		m_reader->ProceedAsCompressed();
}

void ModelFileReader::ReadModelFlags(Model &model)
{
	if (model.m_version >= 16) model.m_flags = m_reader->NextElement<uint32_t>();
	else model.m_flags = 0;
}

#pragma endregion