struct GSOutput
{
  float4 position : SV_POSITION;
  float4 color : COLOR;
};

cbuffer ShapeTransform : register (b0)
{
  row_major float4x4 World;
  float4 Color;
};

cbuffer CameraMatrices : register (b1)
{
  row_major float4x4 View;
  row_major float4x4 Proj;
};

static float4 cube_points[8] =
{
  { -0.5f, -0.5f, -0.5f, +1.0f },
  { -0.5f, -0.5f, +0.5f, +1.0f },
  { -0.5f, +0.5f, -0.5f, +1.0f },
  { -0.5f, +0.5f, +0.5f, +1.0f },
  { +0.5f, -0.5f, -0.5f, +1.0f },
  { +0.5f, -0.5f, +0.5f, +1.0f },
  { +0.5f, +0.5f, -0.5f, +1.0f },
  { +0.5f, +0.5f, +0.5f, +1.0f },
};

[maxvertexcount(8)]
void main(
  point uint _[1] : NOTT_PASS_THROUGH,
  inout PointStream< GSOutput > output
  )
{
  GSOutput element;
  float4 cube_point;
  for (uint i = 0; i < 8; i++)
  {
    cube_point = mul(cube_points[i], World);
    cube_point = mul(cube_point, View);
    cube_point = mul(cube_point, Proj);
    element.position = cube_point;
    element.color = Color;
    output.Append(element);
  }
}