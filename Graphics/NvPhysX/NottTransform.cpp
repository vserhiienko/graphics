
#include <ppl.h>
#include <ppltasks.h>

#include "NottTransform.h"

using namespace nott;

Transform::Transform()
{
  user = 0;
  parent = 0;
  updatePoseTS = true;
  updatePoseWS = true;

  nott::copyValues(scaling, DirectX::g_XMZero);
  nott::copyValues(translationTS, DirectX::g_XMZero);
  nott::copyValues(rotationQTS, DirectX::g_XMIdentityR3);
  nott::copyValues(orientationQ, DirectX::g_XMIdentityR3);

  poseTS = DirectX::XMMatrixIdentity();
  poseWS = DirectX::XMMatrixIdentity();

}

void Transform::notifyPoseWS()
{
  if (auto child_at = children.data()) concurrency::parallel_for(size_t(0), children.size(), [&](size_t const child_ind)
  {
    child_at[child_ind]->updatePoseWS = true;
    child_at[child_ind]->notifyPoseWS();
  });
}