
template <> void Transform::evaluatePoseTS<Transform::kEvaluateRecursiveParallel>(void)
{
  if (auto child_at = children.data()) concurrency::parallel_for(size_t(0), children.size(), [&](size_t const child_ind)
  {
    child_at[child_ind]->evaluatePoseTS<kEvaluateThis>();
    child_at[child_ind]->evaluatePoseTS<kEvaluateRecursiveParallel>();
  });
}

template <> void Transform::evaluatePoseWS<Transform::kEvaluateRecursiveParallel>(void)
{
  if (auto child_at = children.data()) concurrency::parallel_for(size_t(0), children.size(), [&](size_t const child_ind)
  {
    child_at[child_ind]->evaluatePoseWS<kEvaluateThis>();
    child_at[child_ind]->evaluatePoseWS<kEvaluateRecursiveParallel>();
  });
}

template <> void Transform::evaluatePoseTS<Transform::kEvaluateRecursive>(void)
{
  if (auto child_at = children.data()) nott::forEach(size_t(0), children.size(), [&](size_t const child_ind)
  {
    child_at[child_ind]->evaluatePoseTS<kEvaluateThis>();
    child_at[child_ind]->evaluatePoseTS<kEvaluateRecursive>();
  });
}

template <> void Transform::evaluatePoseWS<Transform::kEvaluateRecursive>(void)
{
  if (auto child_at = children.data()) nott::forEach(size_t(0), children.size(), [&](size_t const child_ind)
  {
    child_at[child_ind]->evaluatePoseWS<kEvaluateThis>();
    child_at[child_ind]->evaluatePoseWS<kEvaluateRecursive>();
  });
}

template <> void Transform::evaluatePoseTS<Transform::kEvaluateThis>(void)
{
  if (updatePoseTS)
  {
    poseTS = DirectX::XMMatrixIdentity();
    poseTS = DirectX::XMMatrixMultiply(poseTS, DirectX::XMMatrixScaling(scaling.x, scaling.y, scaling.z));
    poseTS = DirectX::XMMatrixMultiply(poseTS, DirectX::XMMatrixRotationQuaternion(DirectX::XMLoadFloat4(&orientationQ)));
    poseTS = DirectX::XMMatrixMultiply(poseTS, DirectX::XMMatrixTranslation(translationTS.x, translationTS.y, translationTS.z));
    poseTS = DirectX::XMMatrixMultiply(poseTS, DirectX::XMMatrixRotationQuaternion(DirectX::XMLoadFloat4(&rotationQTS)));
    updatePoseTS = false;
  }
}

template <> void Transform::evaluatePoseWS<Transform::kEvaluateThis>(void)
{
  evaluatePoseTS<kEvaluateThis>();

  if (updatePoseWS)
  {
    if (parent)
    {
      parent->evaluatePoseWS<kEvaluateThis>();
      poseWS = DirectX::XMMatrixMultiply(
        poseTS, parent->parent ? parent->poseWS : parent->poseTS
        );
    }

    updatePoseWS = false;
  }
}