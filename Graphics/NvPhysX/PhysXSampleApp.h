#pragma once
#include "pch.h"
#include "PhysXScene.h"

#include "NottSceneBuilding.h"
#include "NottViewers.h"

namespace nyx
{
  
  class PhysXSampleApp
    : public dx::DxSampleApp
    , public dx::AlignedNew<PhysXSampleApp>
  {
    // sample timer
    dx::sim::BasicTimer timer;

    bool hasUI;
    TwBar *tweakBar;
    dx::Color &clearColor;

    /*nyx::PhysXCore pxCore;
    nyx::PhysXScene pxScene;*/

    aega::Camera camera;

    aega::ICameraController *cameraController;
    aega::FreelookController freelookController;
    aega::ModelViewController mvController;

    aega::LightResources lightResources;
    aega::CameraResources cameraResources;

    aega::BillboardRenderer billboardRenderer;
    aega::BillboardResources billboardResources;

    bool sceneReady;
    aega::Scene scene;
    nott::Scene sceneBuild;

    aega::MeshRenderer meshRenderer;
    nott::BoundingBoxRenderer bboxRenderer;

    aega::MeshGPUResources::Vector meshResourcesCollection;
    nott::BoundingBoxResources::Vector bboxResourcesCollection;

    nott::Viewer viewer;

  public:
    POINT cursor_pos;
    POINT cursor_delta;
    POINT cursor_pos_centre;
    RECT screen_rect;
    RECT window_screen_rect;
    bool dragging;

  public:
    PhysXSampleApp(void);
    virtual ~PhysXSampleApp(void);

    void initializeUI(void);
    void renderScene(void);
    void renderUI(void);

  public:

    void clipCursor(void);
    void resetCursor(void);
    void unclipCursor(void);


  public:
    virtual bool onAppLoop(void);
    virtual void handlePaint(dx::DxWindow const*);
    virtual void handleSizeChanged(dx::DxWindow const*);
    virtual void handleMouseMoved(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseLeftPressed(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseLeftReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseWheel(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleKeyPressed(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);



  };
}