#pragma once

#include <AegaScene.h>
#include "AegaCamera.h"

namespace aega
{
  class MeshGPUResources
  {
  public:
    struct VertexBufferElement
    {
      uint32_t indices[9];
    };

    struct PerFrame
    {
      DirectX::XMMATRIX pose;
      DirectX::XMVECTOR color;
    };

    typedef dx::ConstantBuffer<PerFrame> PerFrameResources;
    typedef std::vector<MeshGPUResources*> Vector;

  public:
    static const size_t layoutDescSz = 3;
    static const size_t vertexOffsetSz = 0;
    static const size_t vertexStrideSz = sizeof(VertexBufferElement);
    static const dx::VSInputElementDescription layoutDesc[3];

  public:
    aega::MeshNode const *boundMesh;
    dx::DxDeviceResources *d3d;

    dx::Buffer vertexBuffer;
    dx::Buffer indexBuffer;
    dx::Buffer pointBuffer;
    dx::SRView pointBufferView;
    dx::Buffer normalBuffer;
    dx::SRView normalBufferView;
    dx::Buffer uvBuffer;
    dx::SRView uvBufferView;
    uint32_t faceCount;

    PerFrame perFrameData;
    dx::ConstantBuffer<PerFrame> perFrameBuffer;
    bool recreatePerFrameResources;

  public:
    MeshGPUResources(dx::DxDeviceResources *deviceResources);
    void createFor(aega::MeshNode const &mesh, bool createPerFrameResources = true);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

  class MeshRenderer
  {
  public:

    dx::VSLayout layout;
    dx::VShader vertexShader;
    dx::GShader geometryShader;
    dx::PShader pixelShader;
    dx::PShader pixelShaderDDL;

    dx::BlendState blendState;
    dx::RasterizerState rasterizerState;
    dx::DepthStencilState depthStencilState;

    dx::DxDeviceResources *d3d;

  public:
    MeshRenderer(dx::DxDeviceResources *deviceResources);

  public:
    void create(void);
    void render(aega::MeshGPUResources &meshRes, aega::CameraResources &cameraRes);
    void render(aega::MeshGPUResources &meshRes, aega::CameraResources &cameraRes, aega::LightResources &lightRes);
    void render(aega::MeshGPUResources &meshRes, aega::MeshGPUResources::PerFrameResources &meshPerFrameRes, aega::CameraResources &cameraRes);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

}