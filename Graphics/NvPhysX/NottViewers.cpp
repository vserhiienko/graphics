
#include "NottViewers.h"
#include <DxVirtualKeys.h>
using namespace gte;
using namespace nott;

Viewer::Viewer()
{
  forwardPressed = false;
  backwardPressed = false;
  leftPressed = false;
  rightPressed = false;
  boostPressed = false;
  upV = DirectX::g_XMIdentityR1;

  body.mForce = std::bind(&Viewer::forceHook, this
    , std::placeholders::_1
    , std::placeholders::_2
    , std::placeholders::_3
    , std::placeholders::_4
    , std::placeholders::_5
    , std::placeholders::_6
    , std::placeholders::_7
    , std::placeholders::_8
    , std::placeholders::_9
    );
  body.mTorque = std::bind(&Viewer::forceHook, this
    , std::placeholders::_1
    , std::placeholders::_2
    , std::placeholders::_3
    , std::placeholders::_4
    , std::placeholders::_5
    , std::placeholders::_6
    , std::placeholders::_7
    , std::placeholders::_8
    , std::placeholders::_9
    );
}

void Viewer::reattachCamera(aega::Camera *camera)
{
  ICameraController::reattachCamera(camera);

  body.SetMass(0.0f);
  body.SetMass(1.0f);

  Vector3<Real> position;
  position[0] = camera->eyePosition.x;
  position[1] = camera->eyePosition.y;
  position[2] = camera->eyePosition.z;

  setOrientation();

  dx::Vector3 u = camera->focusDirectionU;
  dx::Vector3 v = DirectX::g_XMIdentityR2;

  float m = sqrt(2.f + 2.f * u.Dot(v));
  dx::Vector3 w = u.Cross(v) * (1.f / m);

  Quaternion<Real> orientation;
  orientation[0] = w.x;
  orientation[1] = w.y;
  orientation[2] = w.z;
  orientation[3] = 0.5f * m;

  body.SetPosition(position);
  body.SetQOrientation(orientation);

  t = 0;
}


void Viewer::keyPressed(uint32_t vk)
{
  switch (vk)
  {
  case dx::VirtualKey::LeftShift:
    boostPressed = true;
    break;
  case dx::VirtualKey::D:
    rightPressed = true;
    break;
  case dx::VirtualKey::A:
    leftPressed = true;
    break;
  case dx::VirtualKey::W:
    forwardPressed = true;
    break;
  case dx::VirtualKey::S:
    backwardPressed = true;
    break;
  }
}

void Viewer::keyReleased(uint32_t vk)
{
  switch (vk)
  {
  case dx::VirtualKey::LeftShift:
    boostPressed = false;
    break;
  case dx::VirtualKey::D:
    rightPressed = false;
    break;
  case dx::VirtualKey::A:
    leftPressed = false;
    break;
  case dx::VirtualKey::W:
    forwardPressed = false;
    break;
  case dx::VirtualKey::S:
    backwardPressed = false;
    break;
  }
}

void Viewer::setPosition()
{
  Vector3<Real> position;
  position[0] = attachedCamera->eyePosition.x;
  position[1] = attachedCamera->eyePosition.y;
  position[2] = attachedCamera->eyePosition.z;
  body.SetPosition(position);
}

void Viewer::forkPosition()
{
  auto pos = body.GetPosition();
  attachedCamera->eyePosition.x = pos[0];
  attachedCamera->eyePosition.y = pos[1];
  attachedCamera->eyePosition.z = pos[2];
}

void Viewer::setOrientation()
{
  dx::Vector3 u = attachedCamera->focusDirectionU;
  dx::Vector3 v = DirectX::g_XMIdentityR2;

  float m = sqrt(2.f + 2.f * u.Dot(v));
  dx::Vector3 w = u.Cross(v) * (1.f / m);

  Quaternion<Real> orientation;
  orientation[0] = w.x;
  orientation[1] = w.y;
  orientation[2] = w.z;
  orientation[3] = 0.5f * m;

  body.SetQOrientation(orientation);
}

void Viewer::onFrameMove(float elapsed)
{
  using namespace DirectX;

  yawD -= yawD * 0.1f;
  pitchD -= pitchD * 0.1f;

  rotQD = XMQuaternionRotationRollPitchYaw(pitchD, yawD, 0.0f);
  focusV = XMLoadFloat3(&attachedCamera->focusDirectionU);
  focusV = XMVector3Rotate(focusV, rotQD);
  rightV = XMVector3Cross(upV, focusV);
  XMStoreFloat3(&attachedCamera->focusDirectionU, focusV);
  XMStoreFloat3(&rightU, rightV);
  rightU.Normalize();

  attachedCamera->focusPosition = attachedCamera->eyePosition + attachedCamera->focusDirectionU;

  attachedCamera->updateView();
  updatePitchYaw();

  t += elapsed;
  dt = elapsed;
  update(t, dt);
}

void Viewer::update(float t, float dt)
{
  body.Update(t, dt);
  
  forkPosition();
  setOrientation();
  attachedCamera->updateView();
}

void Viewer::updatePitchYaw()
{
  yaw = pitchZ
    ? gte::ACosEstimate<float>::Degree<3>(attachedCamera->focusDirectionU.z)
    : gte::ACosEstimate<float>::Degree<3>(attachedCamera->focusDirectionU.x);
  pitch = gte::ASinEstimate<float>::Degree<3>(attachedCamera->focusDirectionU.y);
}

void Viewer::finalizeDrag()
{
  const float thres = 0.1f;
  pitchD = std::max(-thres, pitchD);
  pitchD = std::min(thres, pitchD);
  yawD = std::max(-thres, yawD);
  yawD = std::min(thres, yawD);
}

void Viewer::updatePitchZ()
{
  pitchZ
    = abs(attachedCamera->focusDirectionU.z)
    < abs(attachedCamera->focusDirectionU.x);
}

void Viewer::beginDrag(float x, float y)
{
  previousX = x;
  previousY = y;
  dragging = true;
}

void Viewer::drag(float x, float y)
{
  if (!dragging) return;

  float diffY = attachedCamera->focusDirectionU.z < 0.0f ? -y : y;
  float diffX = x;
  pitchD += diffY * 0.0001f;
  yawD += diffX * 0.0001f;
  finalizeDrag();

  previousX = x;
  previousY = y;
}

void Viewer::zoom(float z)
{

}

void Viewer::endDrag(float x, float y)
{
  dragging = false;
}

Vector3<Viewer::Real> Viewer::forceHook(
  Real t, 
  Real mass, 
  Vector3<Real> const & position, 
  Quaternion<Real> const & orientation, 
  Vector3<Real> const & linear_moment, 
  Vector3<Real> const & angular_momentum, 
  Matrix3x3<Real> const & orientationQ, 
  Vector3<Real> const & linear_velocity, 
  Vector3<Real> const & angular_velocity 
  )
{
  const float g = 9.8f;
  Vector3<Viewer::Real> resultForce 
    = Vector3<Viewer::Real>::Zero();
  //resultForce[1] = dt * -g;

  Vector3<Viewer::Real> forwardU, rightU, upU = { 0, 1, 0 };
  forwardU[0] = attachedCamera->focusDirectionU.x;
  forwardU[1] = attachedCamera->focusDirectionU.y;
  forwardU[2] = attachedCamera->focusDirectionU.z;
  rightU = gte::Cross(upU, forwardU);

  Vector3<Viewer::Real> airDrag = linear_velocity;

  float boost = boostPressed ? 20.0f : 10.0f;
  if (rightPressed) resultForce += boost * rightU;
  if (leftPressed) resultForce -= boost * rightU;
  if (forwardPressed) resultForce += boost * forwardU;
  if (backwardPressed) resultForce -= boost * forwardU;
  resultForce -= airDrag;

  return resultForce;
}

Vector3<Viewer::Real> Viewer::torqueHook(
  Real t, 
  Real mass, 
  Vector3<Real> const & position, 
  Quaternion<Real> const & orientation, 
  Vector3<Real> const & linear_moment, 
  Vector3<Real> const & angular_momentum, 
  Matrix3x3<Real> const & orientationQ, 
  Vector3<Real> const & linear_velocity, 
  Vector3<Real> const & angular_velocity 
  )
{
  Vector3<Viewer::Real> resultTorque = Vector3<Viewer::Real>::Zero();
  return resultTorque;
}