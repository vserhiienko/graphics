#pragma once

#include <vector>
#include <DirectXMath.h>
#include <DirectXCollision.h>

#include "AegaCamera.h"
#include "NottTransform.h"

namespace nott
{
  class BoundingBoxUtility
  {
    static void bestFit(
      _Outref_ DirectX::BoundingOrientedBox &bobox,
      _Inout_ DirectX::XMFLOAT4X4 &matrix,
      _In_ std::vector<DirectX::XMFLOAT3> const &pointCloud,
      _Outref_ DirectX::XMFLOAT3 &sides
      );

  public:
    static void bestFit2(
      _Outref_ DirectX::BoundingOrientedBox &bobox,
      _In_ std::vector<DirectX::XMFLOAT3> const &pointCloud
      );
    static void bestFit(
      _Outref_ DirectX::BoundingOrientedBox &bobox, 
      _In_ std::vector<DirectX::XMFLOAT3> const &pointCloud
      );
  };

  class BoundingBoxResources
  {
  public:
    typedef std::vector<BoundingBoxResources*> Vector;

  public:
    struct PerFrame
    {
      DirectX::XMMATRIX world;
      DirectX::XMVECTOR color;
    };

  public:
    dx::DxDeviceResources *d3d;

  public:
    PerFrame perFrameSource;
    dx::ConstantBuffer<PerFrame> perFrameResource;

  public:
    BoundingBoxResources(dx::DxDeviceResources*);
    void update(void);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

  class BoundingBoxRenderer
  {
  public:
    dx::DxDeviceResources *d3d;

  public:
    dx::VShader vs;
    dx::GShader gsLines;
    dx::GShader gsPolies;
    dx::GShader gsPoints;
    dx::PShader ps;

  public:
    BoundingBoxRenderer(dx::DxDeviceResources*);
    void create(void);
    void render(BoundingBoxResources &bboxGPU, aega::CameraResources &cameraGPU);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

  struct BoundingOrientedBox
  {
    DirectX::BoundingOrientedBox bboxSource;
    BoundingBoxResources *bboxResource;
    nott::Transform *transform;
  };


}

