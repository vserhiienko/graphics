#pragma once
#include <DxUtils.h>
#include <DirectXMath.h>

#define _nott_MemoryPool_forceInline __forceinline

namespace nott
{
  template <typename _TyD, typename _TyS >
  static void copyValues(_TyD &d, _TyS const &s)
  {
    size_t const copySz
      = sizeof(_TyD) >= sizeof(_TyS)
      ? sizeof(_TyS) : sizeof(_TyD);
    memcpy(&d, &s, copySz);
  }

  template <typename _Func>
  static void forEach(size_t from, size_t to, _Func func)
  {
    while (from < to) func(from++);
  }

  class MemoryPool 
    //: public dx::AlignedNew<MemoryPool>
  {
  protected:
    static const size_t defaultSize = 1024;

    size_t vectorI;
    size_t vectorC;
    DirectX::XMVECTOR *vectors;

    size_t matrixI;
    size_t matrixC;
    DirectX::XMMATRIX *matrices;

  public:
    // returns the next vector/matrix from the array
    _nott_MemoryPool_forceInline DirectX::XMVECTOR &MemoryPool::v() { vectorI = (vectorI + 1) % vectorC; return vectors[vectorI]; }
    _nott_MemoryPool_forceInline DirectX::XMMATRIX &MemoryPool::m() { matrixI = (matrixI + 1) % matrixC; return matrices[matrixI]; }

  public:
    static MemoryPool &getDefault();
    static MemoryPool *createNew(size_t vc = defaultSize, size_t mc = defaultSize);

  public:
    MemoryPool(size_t vc = defaultSize, size_t mc = defaultSize);

  };
}