#include "PhysXSampleApp.h"

#include <DxUtils.h>
#include <AegaScene.h>

#include <tbb/task.h>
#include <regex>

#include <GteLinearSystem.h>
#include <DirectXColors.h>

#include "NottBoundingBox.h"

//#include <CiriUIWrap.h>

typedef nott::Transform::EvaluationOptions EvOtps;

static bool hasEnding(std::string const &fullString, std::string const &ending)
{
  if (fullString.length() >= ending.length()) {
    return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
  }
  else {
    return false;
  }
}


dx::DxSampleApp *dx::DxSampleAppFactory()
{
  return new nyx::PhysXSampleApp();
}

nyx::PhysXSampleApp::PhysXSampleApp()
  //: DxSampleApp(D3D_DRIVER_TYPE_REFERENCE)
  : DxSampleApp(D3D_DRIVER_TYPE_HARDWARE)
  , clearColor(deviceResources.clearColor)
  , sceneReady(false)
  , hasUI(false)
  , lightResources(&deviceResources)
  , cameraResources(&deviceResources)
  , bboxRenderer(&deviceResources)
  , billboardRenderer(&deviceResources)
  , billboardResources(&deviceResources)
  , meshRenderer(&deviceResources)
  //, cameraController(&freelookController)
  , cameraController(&mvController)
{
	//ciri::UIWrap ui;
	//ui.initialize();

}

nyx::PhysXSampleApp::~PhysXSampleApp()
{
  for (auto &res : meshResourcesCollection) { delete res; }
  for (auto &res : bboxResourcesCollection) { delete res; }

  if (sceneReady)
  {
    scene.clearAll();
  }

  // close ui
  TwDeleteAllBars();
  TwTerminate();
}

void nyx::PhysXSampleApp::initializeUI()
{
  if (hasUI) return;
  Microsoft::WRL::ComPtr<ID3D11Device> device11; // make it scoped
  if (SUCCEEDED(deviceResources.device.As(&device11)))
  {
    if (
      (hasUI = TwInit(TW_DIRECT3D11, device11.Get())) &&  // if tweak bar graphics initialization succeeded and
      !!(tweakBar = TwNewBar("Scene"))                    // if tweak bar handler creation succeeded
      )
    {
      TwDefine(" GLOBAL help='Vlad Serhiienko\nvlad.serhiinko@gmail.com' "); 
      TwWindowSize(window.dimensions.x, window.dimensions.y);

      int barW = window.dimensions.x / 4;
      int barH = window.dimensions.y / 4 * 3;
      int barSize[2] = { barW, barH };

      TwSetParam(tweakBar, NULL, "size", TW_PARAM_INT32, 2, barSize);
      TwAddVarRW(tweakBar, "ClearColor", TW_TYPE_COLOR4F, &clearColor, "group=General colormode=hls label='Background'");
    }
  }
}

bool nyx::PhysXSampleApp::onAppLoop()
{
  sceneReady = false;
  dragging = false;

  concurrency::create_task([this]()
  {
    srand(time(0));

    aega::SceneDeserialize deserializer;

    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/sphere_r1.mb.aega");
    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/plane-2x2.ma.aega");
    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/spidey_v2.1/KevinRigv1_0.mb.aega");
    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/randomPolies.hard.ma.aega");
    deserializer.deserializeTo(&scene, "F:/Dev/Ma/IronMan1_0.0001.ma.aega");
    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/simpleScene01.ma.aega");
    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/blackpanther_project_MA/blackpanther_project/scenes/blackpanther_v1.mav2.aega");
    //deserializer.deserializeTo(&scene, "D:/Dev/Ma/blackpanther_project_MA/blackpanther_project/scenes/blackpanther_v1.ma.aega");

    //const std::regex meshSkipPattern("(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)");
    //const std::regex meshSkipPattern("(.*_rig_grp.*)|(.*_anim_grp.*)|(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)");
    //const std::regex meshSkipPattern("(.*_rig_grp.*)|(.*_anim_grp.*)|(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)");

    sceneBuild.buildFrom(scene, "(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)");
    //sceneBuild.buildFrom(scene, "(.*_rig_grp.*)|(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)");
    //sceneBuild.buildFrom(scene, "(.*_rig_grp.*)|(.*_anim_grp.*)|(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)");
    //sceneBuild.buildFrom(scene, "(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)");

    sceneBuild.transform_root->scaling = DirectX::XMFLOAT3(0.01f, 0.01f, 0.01f);
    sceneBuild.transform_root->updatePoseTS = true;
    sceneBuild.transform_root->updatePoseWS = true;
    sceneBuild.transform_root->evaluatePoseTS<EvOtps::kEvaluateThis>();
    sceneBuild.transform_root->evaluatePoseWS<EvOtps::kEvaluateThis>();
    sceneBuild.transform_root->notifyPoseWS();
    sceneBuild.transform_root->evaluatePoseWS<EvOtps::kEvaluateRecursiveParallel>();

    concurrency::concurrent_queue<aega::MeshGPUResources*> meshesGPU;
    concurrency::concurrent_queue<nott::BoundingBoxResources*> bboxesGPU;
    concurrency::parallel_for_each(sceneBuild.scene_items.begin(), sceneBuild.scene_items.end(), [&](nott::SceneItem *scene_item)
    {
      auto color = DirectX::XMLoadFloat4(&DirectX::XMFLOAT4(
        1.0f - (0.5f + (float)rand() / (float)RAND_MAX * 0.5f),
        1.0f - (0.5f + (float)rand() / (float)RAND_MAX * 0.5f),
        1.0f, // - (0.5f + (float)rand() / (float)RAND_MAX * 0.5f),
        1.0f
        ));

      auto meshRes = new aega::MeshGPUResources(&deviceResources);
      meshRes->perFrameData.pose = scene_item->mesh_transform->poseWS;
      meshRes->perFrameData.color = color;
      meshRes->createFor(*scene_item->mesh_node);
      meshesGPU.push(meshRes);

      auto bboxRes = new nott::BoundingBoxResources(&deviceResources);
      bboxRes->perFrameSource.world = scene_item->mesh_bbox_transform->poseWS;
      bboxRes->perFrameSource.color = color;

      bboxesGPU.push(bboxRes);
    });

    size_t sz = meshesGPU.unsafe_size();
    meshResourcesCollection.resize(sz);

    sz = 0;
    std::for_each(meshesGPU.unsafe_begin(), meshesGPU.unsafe_end(), [&](aega::MeshGPUResources *res)
    {
      //dx::trace("onAppLoop(async): adding mesh %s", res->boundMesh->path.c_str());
      meshResourcesCollection[sz++] = res;
    });

    sz = meshesGPU.unsafe_size();
    bboxResourcesCollection.resize(sz);

    sz = 0;
    std::for_each(bboxesGPU.unsafe_begin(), bboxesGPU.unsafe_end(), [&](nott::BoundingBoxResources *res)
    {
      //dx::trace("onAppLoop(async): adding bbox");
      bboxResourcesCollection[sz++] = res;
    });

    sceneReady = true;
  });

  camera.setScreenParams(
    deviceResources.actualRenderTargetSize.Width,
    deviceResources.actualRenderTargetSize.Height
    );

  camera.eyePosition.x = 1.f;
  camera.eyePosition.y = 5.f;
  camera.eyePosition.z = 15.f;
  camera.updateView();

  //billboardResources.perFrameSource.pose = DirectX::XMMatrixTranslation(0.0f, -1.0f, 0.0f);
  billboardResources.update();

  dx::Vector3 lightDirection;
  camera.eyePosition.Normalize(lightDirection);

  lightResources.perFrameSource.ambient = DirectX::Colors::White;
  lightResources.perFrameSource.diffuse = DirectX::Colors::WhiteSmoke;
  lightResources.perFrameSource.direction = lightDirection;
  lightResources.update();

  cameraController->reattachCamera(&camera);
  //mvController.reattachCamera(&camera);

  meshRenderer.create();

  GetClipCursor(&screen_rect);
  GetWindowRect(window.handle, &window_screen_rect);

  cursor_pos_centre.x = window_screen_rect.right - window_screen_rect.left;
  cursor_pos_centre.y = window_screen_rect.bottom - window_screen_rect.top;
  cursor_pos_centre.x /= 2;
  cursor_pos_centre.y /= 2;
  cursor_pos_centre.x += window_screen_rect.left;
  cursor_pos_centre.y += window_screen_rect.top;

  window.setWindowTitle("PhysX Sample");
  dx::trace("nyx:app: initialize sample");
  timer.reset();

  /*pxCore.initializePhysX();
  pxScene.initializeScene(pxCore);

  if (pxCore.physics != 0)*/ 
  initializeUI();
 /* TwAddVarRO(tweakBar, "EuP", TW_TYPE_FLOAT, &mvController.pitch, "group='Camera'");
  TwAddVarRO(tweakBar, "EuY", TW_TYPE_FLOAT, &mvController.yaw, "group='Camera'");
  TwAddVarRO(tweakBar, "EyX", TW_TYPE_FLOAT, &mvController.attachedCamera->eyePosition.x, "group='Camera'");
  TwAddVarRO(tweakBar, "EyY", TW_TYPE_FLOAT, &mvController.attachedCamera->eyePosition.y, "group='Camera'");
  TwAddVarRO(tweakBar, "EyZ", TW_TYPE_FLOAT, &mvController.attachedCamera->eyePosition.z, "group='Camera'");
  TwAddVarRO(tweakBar, "Dir", TW_TYPE_DIR3F, &mvController.attachedCamera->focusDirectionU, "group='Camera'");*/
  //TwAddVarRO(tweakBar, "OrientD", TW_TYPE_QUAT4F, &mvController.rotQ, "group='Camera'");
  return hasUI;
}

void nyx::PhysXSampleApp::renderUI(void)
{
  TwDraw();
}

void nyx::PhysXSampleApp::handlePaint(dx::DxWindow const*)
{
  /// update

  timer.update();
  //pxScene.simulate(timer.elapsed, true);

  cameraResources.update(camera);
  cameraController->onFrameMove(timer.elapsed);
  //mvController.onFrameMove(timer.elapsed);


  /// draw

  beginScene();
  billboardRenderer.render(cameraResources, billboardResources);

  if (sceneReady)
  {
    for (auto &meshResources : meshResourcesCollection)
    {
      //meshRenderer.render(*meshResources, cameraResources);
      meshRenderer.render(*meshResources, cameraResources, lightResources);
    }
    for (auto &bboxResources : bboxResourcesCollection)
    {
      bboxResources->update();
      bboxRenderer.render(*bboxResources, cameraResources);
    }
  }

  // ...

  renderUI();
  endScene();

}

void nyx::PhysXSampleApp::handleSizeChanged(dx::DxWindow const*)
{
  camera.setScreenParams(
    deviceResources.actualRenderTargetSize.Width,
    deviceResources.actualRenderTargetSize.Height
    );
}

void nyx::PhysXSampleApp::clipCursor()
{
  dragging = true;
  ClipCursor(&window_screen_rect);
  SetCapture(window.handle);
  ShowCursor(false);
  SetCursorPos(cursor_pos_centre.x, cursor_pos_centre.y);
  GetCursorPos(&cursor_pos);
}

void nyx::PhysXSampleApp::resetCursor()
{
  GetCursorPos(&cursor_pos);
  cursor_delta.x = cursor_pos.x - cursor_pos_centre.x;
  cursor_delta.y = cursor_pos.y - cursor_pos_centre.y;
  SetCursorPos(cursor_pos_centre.x, cursor_pos_centre.y);
}

void nyx::PhysXSampleApp::unclipCursor()
{
  dragging = false;
  SetCursorPos(cursor_pos_centre.x, cursor_pos_centre.y);
  GetCursorPos(&cursor_pos);
  ShowCursor(true);
  ReleaseCapture();
  ClipCursor(&screen_rect);
}

void nyx::PhysXSampleApp::handleMouseMoved(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{

  if (!TwMouseMotion(
    static_cast<int>(args.mouseX()),
    static_cast<int>(args.mouseY())
    ))
  {
    if (dragging)
    {
      resetCursor();
     /* dx::trace("handleMouseMoved: dx=%d dy=%d"
        , cursor_delta.x
        , cursor_delta.y
        );*/

      cameraController->drag(cursor_delta.x, cursor_delta.y);
    }

    //mvController.drag(args.mouseX(), args.mouseY());


    // non-ui click
    // handle here
  }
}

void nyx::PhysXSampleApp::handleMouseLeftPressed(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  auto button = TW_MOUSE_LEFT;
  auto action = TW_MOUSE_PRESSED;

  TwMouseButton(action, button);
  if (!TwMouseMotion(
    static_cast<int>(args.mouseX()),
    static_cast<int>(args.mouseY())
    ))
  {
    clipCursor();

    cameraController->beginDrag(args.mouseX(), args.mouseY());
    // non-ui click
    // handle here
  }
}

void nyx::PhysXSampleApp::handleMouseLeftReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  auto button = TW_MOUSE_LEFT;
  auto action = TW_MOUSE_RELEASED;

  TwMouseButton(action, button);
  if (!TwMouseMotion(
    static_cast<int>(args.mouseX()),
    static_cast<int>(args.mouseY())
    ))
  {
    unclipCursor();

    cameraController->endDrag(args.mouseX(), args.mouseY());
    // non-ui click
    // handle here
  }
}

void nyx::PhysXSampleApp::handleMouseWheel(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  //cameraController->zoom((float)args.wheelDelta());
}

void nyx::PhysXSampleApp::handleKeyPressed(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  cameraController->keyPressed(args.keyCode());
}

void nyx::PhysXSampleApp::handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  switch (args.keyCode())
  {
  case dx::VirtualKey::Escape: window.close(); break;
  default: {} break;
  }

  cameraController->keyReleased(args.keyCode());
}