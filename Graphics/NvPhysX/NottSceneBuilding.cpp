
#include "NottSceneBuilding.h"

using namespace nott;

static void dumpTransform(nott::Transform::Ptr transform, size_t depth = 1)
{
  std::string padding; padding.insert(0, depth, '\t');

  if (transform)
  {
    std::for_each(transform->children.begin(), transform->children.end(), [&](nott::Transform::Ptr child_transform)
    {
      dx::trace("%s- %s", padding.c_str(), child_transform->user ? ((aega::TransformNode*)child_transform->user)->path.c_str() : "<bbox>");
      dumpTransform(child_transform, depth + 1);
    });
  }
  else
  {
    dx::trace("%s- transform is null", padding.c_str());
  }
}


nott::Transform::Ptr Scene::getDeepestParent(nott::Transform::Ptr transform)
{
  if (!transform) return 0;
  nott::Transform::Ptr parent = transform->parent, child = 0;
  while (parent != 0) child = parent, parent = parent->parent;
  return child;
}

nott::Transform::Ptr Scene::findTransformFor(aega::MeshNode *mesh)
{
  auto meshParentPathIt = std::find_if(mesh->parentPaths.begin(), mesh->parentPaths.end(), [&](std::string const &meshParentPath)
  {
    return transform_map.find(meshParentPath) != transform_map.end();
  });

  if (meshParentPathIt != mesh->parentPaths.end()) return transform_map[(*meshParentPathIt)];
  else return 0;
}

void Scene::buildFrom(aega::Scene &scene, std::string const &mesh_skip_pattern)
{
  sceneSource = &scene;

#pragma region build transforms
  auto const x = scene.transforms.data();
  transforms.resize(scene.transforms.size());
  nott::forEach(size_t(0), scene.transforms.size(), [&](size_t node_ind)
  {
    if (x[node_ind])
    {
      transforms[node_ind] = new nott::Transform();
      transforms[node_ind]->user = x[node_ind];

      //dx::trace("buildScene: x %s", x[node_ind]->path.c_str());

      transforms[node_ind]->scaling = x[node_ind]->scale;
      transforms[node_ind]->rotationQTS = x[node_ind]->rotationTS;
      transforms[node_ind]->orientationQ = x[node_ind]->orientation;
      transforms[node_ind]->translationTS = x[node_ind]->translationTS;

      transforms[node_ind]->updatePoseTS = true;
      transforms[node_ind]->updatePoseWS = true;
      transforms[node_ind]->evaluatePoseTS<EvOtps::kEvaluateThis>();
    }
  });

  transform_map.clear();
  std::for_each(transforms.begin(), transforms.end(), [&](nott::Transform *transform)
  {
    std::string const & transform_path = (((aega::TransformNode*)transform->user)->path);
    if (transform_path.size() > 0)
    {
      assert(transform_map.find(transform_path) == transform_map.end());
      transform_map[transform_path] = transform;
    }
  });

  //dx::trace("buildScene: %u transforms found", transforms.size());
  std::for_each(transforms.begin(), transforms.end(), [&](nott::Transform *transform)
  {
    auto bound_node = (aega::TransformNode*)transform->user;
    //dx::trace("buildScene: processing x: %s", bound_node->path.c_str());

    std::for_each(bound_node->childPaths.begin(), bound_node->childPaths.end(), [&](std::string const &child_path)
    {
      if (transform_map.find(child_path) != transform_map.end())
      {
        //dx::trace("\t\t\t -> %s", child_path.c_str());

        assert(transform_map[child_path] != 0);
        transform->children.push_back(transform_map[child_path]);
      }
    });
    std::for_each(bound_node->parentPaths.begin(), bound_node->parentPaths.end(), [&](std::string const &parent_path)
    {
      if (transform_map.find(parent_path) != transform_map.end())
      {
        //dx::trace("\t\t\t <- %s", parent_path.c_str());

        assert(transform->parent == 0);
        assert(transform_map[parent_path] != 0);
        transform->parent = transform_map[parent_path];
      }
    });
  });
#pragma endregion 

#pragma region build meshes
  mesh_matches = new String2MeshNodeSet();
  const std::regex mesh_skip_pattern_rgx(mesh_skip_pattern.empty() ? "(.*Orig[0-9]*$)|(.*BaseShape[0-9]*$)" : mesh_skip_pattern);
  concurrency::parallel_for_each(scene.meshes.begin(), scene.meshes.end(), [&](aega::MeshNode *mesh_node)
  {
    if (!std::regex_match(mesh_node->path, mesh_skip_pattern_rgx)) (*mesh_matches)[mesh_node->path] = mesh_node;
  });

  concurrency::concurrent_unordered_set<nott::Transform*> active_transforms;

  size_t mesh_ind = 0;
  size_t mesh_count = mesh_matches->size();
  scene_items.resize(mesh_count);

  std::for_each(mesh_matches->begin(), mesh_matches->end(), [&](String2MeshNodeSetPair mesh_pair)
  //concurrency::parallel_for_each(mesh_matches->begin(), mesh_matches->end(), [&](String2MeshNodeSetPair mesh_pair)
  {
    auto &scene_item = scene_items[mesh_ind++];

    scene_item = new SceneItem();
    scene_item->mesh_node = mesh_pair.second;
    scene_item->mesh_bbox_transform = new Transform();

    scene_item->mesh_transform = findTransformFor(mesh_pair.second);
    auto mesh_transform_deepest_parent = getDeepestParent(scene_item->mesh_transform);
    mesh_transform_deepest_parent = mesh_transform_deepest_parent
      ? mesh_transform_deepest_parent : scene_item->mesh_transform;
    active_transforms.insert(mesh_transform_deepest_parent);

    scene_item->mesh_transform->children.push_back(scene_item->mesh_bbox_transform);
    scene_item->mesh_bbox_transform->parent = scene_item->mesh_transform;

#if 0
    DirectX::BoundingBox bbox;
    DirectX::BoundingBox::CreateFromPoints(
      bbox, 
      scene_item->mesh_node->points.size(), 
      scene_item->mesh_node->points.data(), 
      sizeof(DirectX::XMFLOAT3)
      );
    scene_item->mesh_bbox_transform->translationTS = bbox.Center;
    scene_item->mesh_bbox_transform->scaling = bbox.Extents;
#else
    BoundingBoxUtility::bestFit2(scene_item->bbox, scene_item->mesh_node->points);
    scene_item->mesh_bbox_transform->orientationQ = scene_item->bbox.Orientation;
    scene_item->mesh_bbox_transform->translationTS = scene_item->bbox.Center;
    scene_item->mesh_bbox_transform->scaling = scene_item->bbox.Extents;
#endif
    scene_item->mesh_bbox_transform->scaling.x *= 2;
    scene_item->mesh_bbox_transform->scaling.y *= 2;
    scene_item->mesh_bbox_transform->scaling.z *= 2;

    //BoundingBoxUtility::bestFit2(scene_item->bbox, scene_item->mesh_node->points);
    //BoundingBoxUtility::bestFit(scene_item->bbox, scene_item->mesh_node->points);
    //scene_item->mesh_bbox_transform->rotationQTS = bbox.;
    //scene_item->mesh_bbox_transform->orientationQ = scene_item->bbox.Orientation;
    scene_item->mesh_bbox_transform->updatePoseTS = true;
    scene_item->mesh_bbox_transform->updatePoseWS = true;

    scene_item->mesh_bbox_transform->evaluatePoseTS<EvOtps::kEvaluateThis>();

    dx::trace("buildScene: got bbox for %s: c %f %f %f e %f %f %f",
      scene_item->mesh_node->path.c_str(),
      scene_item->bbox.Center.x,
      scene_item->bbox.Center.y,
      scene_item->bbox.Center.z,
      scene_item->bbox.Extents.x,
      scene_item->bbox.Extents.y,
      scene_item->bbox.Extents.z
      ); 
    /*dx::trace("buildScene: attaching bbox to %s", 
      ((aega::TransformNode*)scene_item->mesh_transform->user)->path.c_str()
      );*/
    /*dx::trace("buildScene: is active transform %s",
      ((aega::TransformNode*)mesh_transform_deepest_parent->user)->path.c_str()
      );*/
  });

#pragma endregion 

  transform_root = new nott::Transform();
  transform_root->scaling = DirectX::XMFLOAT3(0.01f, 0.01f, 0.01f);
  transform_root->updatePoseTS = true;
  transform_root->updatePoseWS = true;

  std::for_each(active_transforms.begin(), active_transforms.end(), [&](nott::Transform *transform)
  {
    dx::trace("buildScene: active transform %s", ((aega::TransformNode*)transform->user)->path.c_str());
    transform_root->children.push_back(transform);
    transform->parent = transform_root;
  });

  //dumpTransform(transform_root);
  transform_root->evaluatePoseTS<EvOtps::kEvaluateThis>();
  transform_root->evaluatePoseWS<EvOtps::kEvaluateRecursiveParallel>();

  mesh_matches->clear();
  dx::safeDelete(mesh_matches);
}