#pragma once

#include <algorithm>

#include <DxMath.h>
#include <DxConstantBuffer.h>
#include <DxDeviceResources.h>

#include <GteACosEstimate.h>
#include <GteASinEstimate.h>


namespace aega
{
  class Camera
  {
  public:
    float fovY;
    float screenW, screenH;
    float nearPlane, farPlane;

    dx::Matrix view;
    dx::Matrix projection;

    dx::Vector3 upDirection;
    dx::Vector3 eyePosition;
    dx::Vector3 focusPosition;
    dx::Vector3 focusDirection;
    dx::Vector3 focusDirectionU;

  public:
    Camera(void);

  public:
    void setScreenParams(
      float screenW,
      float screenH,
      float fovY = dx::XM_PIDIV4,
      float nearPlane = 0.001f,
      float farPlane = 100.0f
      );
    void updateView(void);
    void updateProjection(void);

  };

  class OrbitCamera
    : public Camera
  {
  protected:
    dx::Vector3 eyeDisplacement, focusDisplacement;
    dx::Quaternion pitchQ, yawQ;
    float pitchAccA, yawAccA;
    float pitchA, yawA;

  public:
    OrbitCamera(void);
    void initialize(void);

  public:
    void onPointerInput(
      float deltaX,
      float deltaY,
      float deltaZoom
      );
  };

  class ICameraController
  {
  public:
    Camera *attachedCamera;

  public:
    ICameraController();

  public:
    virtual void reattachCamera(Camera *camera);
    virtual void onFrameMove(float elapsed);

  public:
    virtual void beginDrag(float x, float y);
    virtual void drag(float x, float y);
    virtual void endDrag(float x, float y);

  public:
    virtual void keyPressed(uint32_t);
    virtual void keyReleased(uint32_t);
  };

  class ModelViewController
    : public ICameraController
  {
  protected:
    float diffX, diffY;
    bool pitchZ, invPitch;

  public:
    bool dragging;
    float pitch, yaw;
    float pitchD, yawD, zoomD;
    float previousX, previousY;
    DirectX::XMVECTOR eyeV, rotQD;
    DirectX::XMMATRIX traM, traInvM, rotM;

  public:
    ModelViewController();

  public:
    virtual void reattachCamera(Camera *camera);
    virtual void onFrameMove(float elapsed);
    virtual void beginDrag(float x, float y);
    virtual void drag(float x, float y);
    virtual void zoom(float z);
    virtual void endDrag(float x, float y);

  public:
    void finalizeFrameMove();
    void updatePitchYaw();
    void finalizeDrag();
    void finalizeZoom();

  private:
    void updatePitchZ();
  };

  class FreelookController
    : public ModelViewController
  {
  public:
    bool boost;
    bool pressingRight;
    bool pressingForward;
    bool panning;
    float rightD, forwardD;
    dx::Vector3 eyeU, rightU;
    DirectX::XMVECTOR focusV, upV, rightV, forwardV;

    float velocityMag;
    dx::Vector3 velocityDirU;

  public:
    FreelookController();

  public:
    virtual void reattachCamera(Camera *camera);
    virtual void onFrameMove(float elapsed);
    virtual void drag(float x, float y);
    virtual void keyPressed(uint32_t vk);
    virtual void keyReleased(uint32_t vk);

  public:
    void finalizeMovement();

  };

  class CameraResources
  {
  public:
    struct PerFrame
    {
      dx::XMMATRIX view;
      dx::XMMATRIX projection;
    };

  public:
    dx::DxDeviceResources *d3d;

    PerFrame perFrameSource;
    dx::ConstantBuffer<PerFrame> perFrameResource;

  public:
    CameraResources(dx::DxDeviceResources*);
    void update(Camera const &);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);
  };

  class LightResources
  {
  public:
    struct PerFrame
    {
      dx::XMVECTOR ambient;
      dx::XMVECTOR diffuse;
      dx::XMVECTOR direction;
    };
  public:

    dx::DxDeviceResources *d3d;

    PerFrame perFrameSource;
    dx::ConstantBuffer<PerFrame> perFrameResource;
  public:
    LightResources(dx::DxDeviceResources*);
    void update(void);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

}

