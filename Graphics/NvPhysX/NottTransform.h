#pragma once

#include <vector>
#include <DirectXMath.h>
#include <DirectXCollision.h>

#include <DxUtils.h>
#include "NottPools.h"

namespace nott
{
  struct Transform : public dx::AlignedNew<Transform>
  {
    typedef Transform *Ptr;
    typedef std::vector<Ptr> PtrVector;

    DirectX::XMFLOAT3 translationTS;
    DirectX::XMFLOAT3 scaling;
    DirectX::XMFLOAT4 rotationQTS;
    DirectX::XMFLOAT4 orientationQ;

    bool updatePoseTS;
    bool updatePoseWS;
    DirectX::XMMATRIX poseTS;
    DirectX::XMMATRIX poseWS;

    void *user;
    Ptr parent;
    PtrVector children;

    Transform(void);

    enum EvaluationOptions 
    { 
      kEvaluateThis = 1, 
      kEvaluateRecursive = 2,
      kEvaluateParallel = 4, 
      kEvaluateRecursiveParallel = kEvaluateRecursive | kEvaluateParallel,
    };

    void notifyPoseWS();

    template <EvaluationOptions _Opts> void evaluatePoseTS(void);
    template <EvaluationOptions _Opts> void evaluatePoseWS(void);

    template <> void evaluatePoseTS<kEvaluateRecursiveParallel>(void);
    template <> void evaluatePoseWS<kEvaluateRecursiveParallel>(void);
    template <> void evaluatePoseTS<kEvaluateRecursive>(void);
    template <> void evaluatePoseWS<kEvaluateRecursive>(void);
    template <> void evaluatePoseTS<kEvaluateThis>(void);
    template <> void evaluatePoseWS<kEvaluateThis>(void);

  };

#include "NottTransform.inl"

}