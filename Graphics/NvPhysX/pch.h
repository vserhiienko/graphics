#pragma once

#include <DxUtils.h>
#include <DxTimer.h>
//#include <DxUIClient.h>
//#include <DxUISampleApp.h>
#include <DxSampleApp.h>

#include <AntTweakBar.h>

#include <PxPhysicsAPI.h>
#include <extensions\PxExtensionsAPI.h>
#include <extensions\PxDefaultErrorCallback.h>
#include <extensions\PxDefaultAllocator.h>
#include <extensions\PxDefaultSimulationFilterShader.h>
#include <extensions\PxDefaultCpuDispatcher.h>
#include <extensions\PxShapeExt.h>
#include <extensions\PxSimpleFactory.h>
#include <foundation\PxFoundation.h>

