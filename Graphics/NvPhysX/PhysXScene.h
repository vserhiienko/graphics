#pragma once

#include "PhysXCore.h"

namespace nyx
{
  class PhysXScene
  {
  public:
    physx::PxScene *scene;
    physx::PxRigidDynamic* connectedBox = NULL;

  public:
    PhysXScene()
    {
      scene = 0;
    }
    ~PhysXScene()
    {
      if (scene) scene->release();
    }

    unsigned simulate(float timeStep, bool waitForResults)
    {
      unsigned
        errorState = 0;

      if (scene) 
        scene->simulate(timeStep),
        scene->fetchResults(waitForResults, &errorState);

      return errorState;
    }

    void initializeScene(PhysXCore &core)
    {
      using namespace physx;

      PxSceneDesc sceneDesc(core.physics->getTolerancesScale()); // Descriptor class for scenes 
      sceneDesc.gravity = PxVec3(0.0f, -9.8f, 0.0f); // Setting gravity
      sceneDesc.cpuDispatcher = PxDefaultCpuDispatcherCreate(1); // Creating default CPU dispatcher for the scene
      sceneDesc.filterShader = PxDefaultSimulationFilterShader; // Creating default collision filter shader for the scene

      scene = core.physics->createScene(sceneDesc); // Creating a scene 
      scene->setVisualizationParameter(PxVisualizationParameter::eSCALE, 1.0); // Global visualization scale which gets multiplied with the individual scales
      scene->setVisualizationParameter(PxVisualizationParameter::eCOLLISION_SHAPES, 1.0f); // Enable visualization of actor's shape
      scene->setVisualizationParameter(PxVisualizationParameter::eACTOR_AXES, 1.0f); // Enable visualization of actor's axis
      scene->setVisualizationParameter(PxVisualizationParameter::eJOINT_LIMITS, 1.0f);
      scene->setVisualizationParameter(PxVisualizationParameter::eJOINT_LOCAL_FRAMES, 1.0f);

      // Creating PhysX material (staticFriction, dynamicFriction, restitution)
      PxMaterial* material = core.physics->createMaterial(0.5f, 0.5f, 0.5f);

      //---------Creating actors-----------]
      //1-Creating static plane that will act as ground	 
      PxTransform planePos = PxTransform(PxVec3(0.0f), PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));	// Position and orientation(transform) for plane actor  
      PxRigidStatic* plane = core.physics->createRigidStatic(planePos); // Creating rigid static actor	
      plane->createShape(PxPlaneGeometry(), *material); // Defining geometry for plane actor
      scene->addActor(*plane); // Adding plane actor to PhysX scene

      //--------Fixed Joint---------//
      //Creating fixed joint between two spheres							
      {
        PxVec3 pos = PxVec3(5, 50, 10);
        PxVec3 offset = PxVec3(0, 3, 0);

        PxRigidDynamic* actor = PxCreateDynamic(dx::toRef(core.physics), PxTransform(pos), PxSphereGeometry(3.0f), *material, 1.0f);
        PxRigidDynamic* otherActor = PxCreateDynamic(dx::toRef(core.physics), PxTransform(pos + PxVec3(0, 0, 0)), PxSphereGeometry(3.0f), *material, 1.0f);

        PxFixedJoint* fixedJoint = PxFixedJointCreate(dx::toRef(core.physics), actor, PxTransform(-offset), otherActor, PxTransform(offset));
        fixedJoint->setConstraintFlag(PxConstraintFlag::eVISUALIZATION, true); //setting joint debug-visualization true 

        scene->addActor(*actor);
        scene->addActor(*otherActor);
      }

      //--------D6 Joint---------//
      // Creating D6 joint between two actors, connected actor(box) is made free to rotate around y axis.
      // We are also applying angular velocity on the connected body on each update of 'OnRender()' function.
      // This will make connected actor keep rotating around y axis.
      {
        PxVec3 pos = PxVec3(10, 10, 3);
        PxVec3 offset = PxVec3(0, 1.5, 0);

        PxRigidActor* staticActor = PxCreateStatic(dx::toRef(core.physics), PxTransform(pos), PxSphereGeometry(0.5f), *material);
        connectedBox = PxCreateDynamic(dx::toRef(core.physics), PxTransform(PxVec3(0), PxQuat(PxHalfPi, PxVec3(0, 0, 1))), PxBoxGeometry(2, 2, 15), *material, 1.0f);


        PxD6Joint* d6Joint = PxD6JointCreate(dx::toRef(core.physics), staticActor, PxTransform(-offset), connectedBox, PxTransform(offset));
        d6Joint->setConstraintFlag(PxConstraintFlag::eVISUALIZATION, true);
        d6Joint->setMotion(PxD6Axis::eSWING1, PxD6Motion::eFREE); //free to rotate around y axis

        scene->addActor(*staticActor);
        scene->addActor(*connectedBox);
      }

      //--------Spherical Joint---------//
      //Creating a series of spheres, inter-connected using spherical joint. 
      {
        PxVec3 pos = PxVec3(0, 25, 0);
        PxReal radius = 1;
        PxVec3 offset(0, 2, 0);

        PxRigidActor* prevActor = PxCreateStatic(dx::toRef(core.physics), PxTransform(pos), PxSphereGeometry(radius), *material);
        scene->addActor(*prevActor);

        for (PxU32 i = 1; i < 6; i++)
        {
          PxTransform transform = PxTransform(PxVec3(0, PxReal(i*radius*1.5), 0));
          PxRigidDynamic* dynamic = PxCreateDynamic(dx::toRef(core.physics), transform, PxSphereGeometry(radius), *material, 1.0f);

          scene->addActor(*dynamic);

          PxSphericalJoint* joint = PxSphericalJointCreate(dx::toRef(core.physics), prevActor, PxTransform(-offset), dynamic, PxTransform(offset));
          joint->setConstraintFlag(PxConstraintFlag::eVISUALIZATION, true);
          //joint->setLimitCone(PxJointLimitCone(PxPi/2, PxPi/6, 0.01f)); //Used for limiting the movement of spherical joint. 

          prevActor = dynamic;
        }
      }
    }


  };
}