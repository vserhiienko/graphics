#include "NottPools.h"
using namespace nott;

MemoryPool::MemoryPool(size_t vc, size_t mc)
{
  assert(vc != 0 && mc != 0);

  vectorI = 0;
  matrixI = 0;
  vectorC = vc;
  matrixC = mc;
  vectors = new DirectX::XMVECTOR[vectorC];
  matrices = new DirectX::XMMATRIX[matrixC];
}

MemoryPool *MemoryPool::createNew(size_t vc, size_t mc)
{
  return new MemoryPool(vc, mc);
}

MemoryPool &MemoryPool::getDefault()
{
  static MemoryPool defaultInstance;
  return defaultInstance;
}