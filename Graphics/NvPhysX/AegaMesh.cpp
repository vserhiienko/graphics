#include "AegaMesh.h"
#include <DxResourceManager.h>
#include <DirectXColors.h>

const dx::VSInputElementDescription aega::MeshGPUResources::layoutDesc[3] =
{
  { "VSVERTEX0INDICES", 0, DXGI_FORMAT_R32G32B32_UINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  { "VSVERTEX1INDICES", 0, DXGI_FORMAT_R32G32B32_UINT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  { "VSVERTEX2INDICES", 0, DXGI_FORMAT_R32G32B32_UINT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

aega::MeshGPUResources::MeshGPUResources(dx::DxDeviceResources *deviceResources)
  : boundMesh(0)
  , faceCount(0)
  , d3d(deviceResources)
{
  perFrameData.pose = DirectX::XMMatrixIdentity();
  perFrameData.color = DirectX::Colors::CornflowerBlue;

  recreatePerFrameResources = true;
  //d3d->deviceLost += Nena_Delegate_MakeBind(this, &MeshGPUResources::onDeviceLost);
  //d3d->deviceRestored += Nena_Delegate_MakeBind(this, &MeshGPUResources::onDeviceRestored);
}

void aega::MeshGPUResources::createFor(aega::MeshNode const &mesh, bool frameRes)
{
  if (!d3d) return;

  boundMesh = &mesh;
  recreatePerFrameResources = frameRes;

  faceCount = 0;
  std::vector<VertexBufferElement> vertexBufferData;
  for (size_t i = 0; i < mesh.polygons.size(); ++i)
    for (size_t j = 0; j < mesh.polygons[i].triangles.size(); ++j)
      faceCount++;

  size_t vertexInd = 0;
  vertexBufferData.resize(faceCount);
  for (size_t i = 0; i < mesh.polygons.size(); ++i)
    for (size_t j = 0; j < mesh.polygons[i].triangles.size(); ++j)
      memcpy(&vertexBufferData[vertexInd], mesh.polygons[i].triangles[j].points, sizeof(VertexBufferElement)),
      vertexInd++;

  assert(SUCCEEDED(dx::DxResourceManager::createVertexBuffer(
    d3d->device.Get(),
    vertexBufferData.size(),
    vertexBufferData.data(),
    vertexBuffer.ReleaseAndGetAddressOf()
    )));
  assert(SUCCEEDED(dx::DxResourceManager::createStructuredBuffer(
    d3d->device.Get(),
    mesh.points.size(),
    mesh.points.data(),
    pointBuffer.ReleaseAndGetAddressOf(),
    pointBufferView.ReleaseAndGetAddressOf(),
    nullptr
    )));
  assert(SUCCEEDED(dx::DxResourceManager::createStructuredBuffer(
    d3d->device.Get(),
    mesh.normals.size(),
    mesh.normals.data(),
    normalBuffer.ReleaseAndGetAddressOf(),
    normalBufferView.ReleaseAndGetAddressOf(),
    nullptr
    )));

  if (mesh.uvSets[0]->uvs.size())
    assert(SUCCEEDED(dx::DxResourceManager::createStructuredBuffer(
      d3d->device.Get(),
      mesh.uvSets[0]->uvs.size(),
      mesh.uvSets[0]->uvs.data(),
      uvBuffer.ReleaseAndGetAddressOf(),
      uvBufferView.ReleaseAndGetAddressOf(),
      nullptr
      )));

  if (recreatePerFrameResources)
    assert(SUCCEEDED(perFrameBuffer.create(
      d3d->device.Get(),
      "aega:meshPerFrameConstBuffer"
      )));

}

void aega::MeshGPUResources::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void aega::MeshGPUResources::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  if (restoredDevice && boundMesh)
  {
    d3d = restoredDevice;
    createFor(*boundMesh, recreatePerFrameResources);
  }
}

aega::MeshRenderer::MeshRenderer(dx::DxDeviceResources *deviceResources)
  : d3d(deviceResources)
{
  d3d->deviceLost += Nena_Delegate_MakeBind(this, &MeshRenderer::onDeviceLost);
  d3d->deviceRestored += Nena_Delegate_MakeBind(this, &MeshRenderer::onDeviceRestored);
}

void aega::MeshRenderer::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void aega::MeshRenderer::onDeviceRestored(dx::DxDeviceResources *restoredResources)
{
  if (restoredResources)
  {
    d3d = restoredResources;
    create();
  }
}

void aega::MeshRenderer::create()
{
  if (d3d)
  {
    assert(SUCCEEDED(dx::DxResourceManager::loadShader(
      d3d->device.Get(),
      L"./cso/ShapeVertexShader.cso",
      MeshGPUResources::layoutDesc,
      MeshGPUResources::layoutDescSz,
      vertexShader.ReleaseAndGetAddressOf(),
      layout.ReleaseAndGetAddressOf()
      )));
    assert(SUCCEEDED(dx::DxResourceManager::loadShader(
      d3d->device.Get(),
      L"./cso/ShapeGeometryShader.cso",
      geometryShader.ReleaseAndGetAddressOf()
      )));
    assert(SUCCEEDED(dx::DxResourceManager::loadShader(
      d3d->device.Get(),
      L"./cso/ShapePixelShader.cso",
      pixelShader.ReleaseAndGetAddressOf()
      )));
    assert(SUCCEEDED(dx::DxResourceManager::loadShader(
      d3d->device.Get(),
      L"./cso/ShapeDDLightPixelShader.cso",
      pixelShaderDDL.ReleaseAndGetAddressOf()
      )));
  }
}

void aega::MeshRenderer::render(
  aega::MeshGPUResources &meshGPU, 
  aega::CameraResources &cameraGPU
  )
{
  static UINT32 strides[] = { MeshGPUResources::vertexStrideSz };
  static UINT32 offsets[] = { MeshGPUResources::vertexOffsetSz };

  auto pipeline = d3d->context.Get();

  meshGPU.perFrameBuffer.setData(pipeline, meshGPU.perFrameData);

  pipeline->IASetInputLayout(layout.Get());
  pipeline->IASetVertexBuffers(0, 1, meshGPU.vertexBuffer.GetAddressOf(), strides, offsets);
  pipeline->IASetPrimitiveTopology(dx::PrimitiveTopology::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

  pipeline->VSSetShader(vertexShader.Get(), nullptr, 0);
  pipeline->GSSetShader(geometryShader.Get(), nullptr, 0);
  pipeline->PSSetShader(pixelShader.Get(), nullptr, 0);

  pipeline->GSSetConstantBuffers(0, 1, meshGPU.perFrameBuffer.getBufferAddress());
  pipeline->GSSetConstantBuffers(1, 1, cameraGPU.perFrameResource.getBufferAddress());
  pipeline->GSSetShaderResources(0, 1, meshGPU.pointBufferView.GetAddressOf());
  pipeline->GSSetShaderResources(1, 1, meshGPU.normalBufferView.GetAddressOf());
  pipeline->GSSetShaderResources(2, 1, meshGPU.uvBufferView.GetAddressOf());

  //pipeline->PSSetConstantBuffers(0, 1, Lb.GetBufferAddress());
  //pipeline->PSSetShaderResources(0, 1, ShapeTexture.Read.GetAddressOf());
  //pipeline->PSSetSamplers(0, 1, ShapeSampleType.GetAddressOf());

  pipeline->Draw(meshGPU.faceCount, 0);

  pipeline->GSSetShader(nullptr, nullptr, 0);
}

void aega::MeshRenderer::render(
  aega::MeshGPUResources &meshGPU, 
  aega::CameraResources &cameraGPU,
  aega::LightResources &lightGPU
  )
{

  static UINT32 strides[] = { MeshGPUResources::vertexStrideSz };
  static UINT32 offsets[] = { MeshGPUResources::vertexOffsetSz };

  auto pipeline = d3d->context.Get();

  meshGPU.perFrameBuffer.setData(pipeline, meshGPU.perFrameData);

  pipeline->IASetInputLayout(layout.Get());
  pipeline->IASetVertexBuffers(0, 1, meshGPU.vertexBuffer.GetAddressOf(), strides, offsets);
  pipeline->IASetPrimitiveTopology(dx::PrimitiveTopology::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

  pipeline->VSSetShader(vertexShader.Get(), nullptr, 0);
  pipeline->GSSetShader(geometryShader.Get(), nullptr, 0);
  pipeline->PSSetShader(pixelShaderDDL.Get(), nullptr, 0);

  pipeline->GSSetConstantBuffers(0, 1, meshGPU.perFrameBuffer.getBufferAddress());
  pipeline->GSSetConstantBuffers(1, 1, cameraGPU.perFrameResource.getBufferAddress());
  pipeline->GSSetShaderResources(0, 1, meshGPU.pointBufferView.GetAddressOf());
  pipeline->GSSetShaderResources(1, 1, meshGPU.normalBufferView.GetAddressOf());
  pipeline->GSSetShaderResources(2, 1, meshGPU.uvBufferView.GetAddressOf());
  pipeline->PSSetConstantBuffers(0, 1, lightGPU.perFrameResource.getBufferAddress());

  //pipeline->PSSetConstantBuffers(0, 1, Lb.GetBufferAddress());
  //pipeline->PSSetShaderResources(0, 1, ShapeTexture.Read.GetAddressOf());
  //pipeline->PSSetSamplers(0, 1, ShapeSampleType.GetAddressOf());

  pipeline->Draw(meshGPU.faceCount, 0);

  pipeline->GSSetShader(nullptr, nullptr, 0);
}

void aega::MeshRenderer::render(
  aega::MeshGPUResources &meshGPU,
  aega::MeshGPUResources::PerFrameResources &meshPerFrameGPU,
  aega::CameraResources &cameraGPU
  )
{
  static UINT32 strides[] = { MeshGPUResources::vertexStrideSz };
  static UINT32 offsets[] = { MeshGPUResources::vertexOffsetSz };

  auto pipeline = d3d->context.Get();

  pipeline->IASetInputLayout(layout.Get());
  pipeline->IASetVertexBuffers(0, 1, meshGPU.vertexBuffer.GetAddressOf(), strides, offsets);
  pipeline->IASetPrimitiveTopology(dx::PrimitiveTopology::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

  pipeline->VSSetShader(vertexShader.Get(), nullptr, 0);
  pipeline->GSSetShader(geometryShader.Get(), nullptr, 0);
  pipeline->PSSetShader(pixelShader.Get(), nullptr, 0);

  pipeline->GSSetConstantBuffers(0, 1, meshPerFrameGPU.getBufferAddress());
  pipeline->GSSetConstantBuffers(1, 1, cameraGPU.perFrameResource.getBufferAddress());
  pipeline->GSSetShaderResources(0, 1, meshGPU.pointBufferView.GetAddressOf());
  pipeline->GSSetShaderResources(1, 1, meshGPU.normalBufferView.GetAddressOf());
  pipeline->GSSetShaderResources(2, 1, meshGPU.uvBufferView.GetAddressOf());

  //pipeline->PSSetConstantBuffers(0, 1, Lb.GetBufferAddress());
  //pipeline->PSSetShaderResources(0, 1, ShapeTexture.Read.GetAddressOf());
  //pipeline->PSSetSamplers(0, 1, ShapeSampleType.GetAddressOf());

  pipeline->Draw(meshGPU.faceCount, 0);

  pipeline->GSSetShader(nullptr, nullptr, 0);
}
