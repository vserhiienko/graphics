#pragma once
#include "pch.h"

namespace nyx
{
  class PhysXCore
  {
    physx::PxDefaultAllocator defaultAllocCallback;
    physx::PxDefaultErrorCallback defaultErrorCallback;


  public: // PX
    physx::PxPhysics *physics;
    physx::PxFoundation *foundation;
    physx::PxTolerancesScale tolerances;
    physx::PxErrorCallback *errorCallback;
    physx::PxAllocatorCallback *allocCallback;

  public: // PVD
    std::string debuggerHost; // localhost
    std::string debuggerFile; // (empty by default)
    uint32_t debuggerHostPort; // 5425
    uint32_t debuggerConnectionTimeout; // ms (1 sec by default)
    physx::PxVisualDebuggerConnection *debuggerConnection;
    physx::PxVisualDebuggerConnectionHandler *debuggerConnectionHandler;

  public:
    PhysXCore();
    ~PhysXCore();

    /**
      setup before calling initializePhysX()
      @li errorCallback (will be set to default if 0)
      @li allocCallback (will be set to default if 0)
      @li debuggerHost (default debugger host locahost)
      @li debuggerHostPort (default debugger host port 5425)
      @li debuggerFile (use instead of TCP/IP - stream info to .pxd2 file, and parse later)
      @li debuggerConnectionHandler (can be 0)
    */
    bool initializePhysX();
    void releasePhysX();

  };
}

