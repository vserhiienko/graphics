

cbuffer BoidPerFrame : register (b0)
{
  row_major float4x4 World;
  float4 Color;
}

cbuffer CameraPerFrame : register (b1)
{
  row_major float4x4 CameraView;
  row_major float4x4 CameraProj;
};

//cbuffer Billboard : register (cb1)
//{
//	float4 Color;
//};

#define _cw		World
#define _cv		CameraView
#define _cp		CameraProj

struct VsInput
{
	float4 pos : POSITION;
};

struct PsInput
{
	float4 pos : SV_POSITION;
	float4 color : COLOR;
};

PsInput main(VsInput input, uint vertexId : SV_VERTEXID)
{
	PsInput output;

	output.pos = input.pos;
	output.pos.w = 1.0f;

	output.pos = mul(output.pos, _cw);
	/*printf(
		"vs: obj x %2.2f y %2.2f z %2.2f",
		output.pos.x,
		output.pos.y,
		output.pos.z
		);*/

	output.pos = mul(output.pos, _cv);
	/*printf(
		"vs: view x %2.2f y %2.2f z %2.2f",
		output.pos.x,
		output.pos.y,
		output.pos.z
		);*/

	output.pos = mul(output.pos, _cp);
	/*printf(
		"vs: proj x %2.2f y %2.2f z %2.2f",
		output.pos.x,
		output.pos.y,
		output.pos.z
		);*/

  output.color = Color; // white
	return output;
}