#pragma once


#include "AegaMesh.h"
#include "AegaCamera.h"
#include "AegaBillboard.h"

#include "NottTransform.h"
#include "NottBoundingBox.h"

#include <map>
#include <concurrent_unordered_map.h>
#include <concurrent_unordered_set.h>
#include <regex>

namespace nott
{
  struct SceneItem
  {
    typedef std::vector<SceneItem*> PtrVector;

    aega::MeshNode *mesh_node;
    DirectX::BoundingOrientedBox bbox;

    nott::Transform *mesh_transform;
    nott::Transform *mesh_bbox_transform;

  };


  class Scene
  {
  protected:
    typedef concurrency::concurrent_unordered_map<std::string, aega::MeshNode*> String2MeshNodeSet;
    typedef std::pair<std::string, aega::MeshNode*> String2MeshNodeSetPair;
    String2MeshNodeSet *mesh_matches;

  public:
    typedef nott::Transform::EvaluationOptions EvOtps;

  public:
    aega::Scene *sceneSource;
    nott::Transform::Ptr transform_root;
    nott::Transform::PtrVector transforms;
    SceneItem::PtrVector scene_items;
    std::map<std::string const, nott::Transform*> transform_map;

  public:
    //! @note returns 0 if transform == 0 or if transform is a root itself
    nott::Transform::Ptr findTransformFor(aega::MeshNode *mesh);
    nott::Transform::Ptr getDeepestParent(nott::Transform::Ptr transform);
    void buildFrom(aega::Scene &scene, std::string const &mesh_skip_pattern);



  };


}

