#include "AegaCamera.h"

#include <DxVirtualKeys.h>

static float clamp(float value, float min, float max)
{
  return std::min(std::max(value, min), max);
}

aega::Camera::Camera()
{
  fovY = dx::XM_PIDIV2;
  screenW = 0.0f;
  screenH = 0.0f;
  farPlane = 100.0f;
  nearPlane = 0.01f;
  upDirection = DirectX::XMVectorSet(0.f, 1.f, 0.f, 1.f);
  eyePosition = DirectX::XMVectorSet(0.f, 0.f, -1.f, 1.f);
  focusPosition = DirectX::XMVectorSet(0.f, 0.f, +1.f, 1.f);
}

void aega::Camera::setScreenParams(
  float w,
  float h,
  float fov,
  float n,
  float f
  )
{
  fovY = fov;
  screenW = w;
  screenH = h;
  farPlane = f;
  nearPlane = n;
  updateProjection();

  updateView();
}

void aega::Camera::updateView()
{
  focusDirection = focusDirectionU = focusPosition - eyePosition;
  focusDirectionU.Normalize();

  view = DirectX::XMMatrixLookAtLH(
    eyePosition, focusPosition, upDirection
    );
}

void aega::Camera::updateProjection()
{
  projection = DirectX::Matrix::CreatePerspectiveFieldOfView(
    fovY, screenW / screenH, nearPlane, farPlane
    );
}

aega::OrbitCamera::OrbitCamera()
{
  pitchAccA = 0.0f;
  yawAccA = 0.0f;
  pitchA = 0.0f;
  yawA = 0.0f;
}

void aega::OrbitCamera::initialize()
{
  eyeDisplacement = eyePosition;

  updateView();
}

void aega::OrbitCamera::onPointerInput(
  float dx, float dy, float zoom
  )
{
  dx::Vector2 angularDelta = dx::Vector2(dx, dy);
  angularDelta *= 0.005f;
  zoom *= 0.005f;

  pitchA += angularDelta.y;
  yawA += angularDelta.x;

  pitchQ.w = cosf(pitchA * 0.50f);
  pitchQ.x = sinf(pitchA * 0.50f);
  yawQ.w = cosf(yawA * 0.50f);
  yawQ.y = sinf(yawA * 0.50f);

  eyeDisplacement.y += zoom;
  focusDisplacement = dx::XMVector3Rotate(eyeDisplacement, pitchQ);
  focusDisplacement = dx::XMVector3Rotate(focusDisplacement, yawQ);
  eyePosition = focusPosition + focusDisplacement;

  updateView();
}

aega::CameraResources::CameraResources(dx::DxDeviceResources *device) : d3d(device)
{
  assert(d3d != 0);
  d3d->deviceLost += _Dx_delegate_to(this, &CameraResources::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &CameraResources::onDeviceRestored);
  onDeviceRestored(d3d);
}

aega::LightResources::LightResources(dx::DxDeviceResources *device) : d3d(device)
{
  assert(d3d != 0);
  d3d->deviceLost += _Dx_delegate_to(this, &LightResources::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &LightResources::onDeviceRestored);
  onDeviceRestored(d3d);
}

void aega::CameraResources::update(Camera const &data)
{
  assert(d3d != 0);
  perFrameSource.view = data.view;
  perFrameSource.projection = data.projection;
  perFrameResource.setData(d3d->context.Get(), perFrameSource);
}

void aega::LightResources::update()
{
  assert(d3d != 0);
  perFrameResource.setData(d3d->context.Get(), perFrameSource);
}

void aega::CameraResources::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void aega::LightResources::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void aega::CameraResources::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  perFrameResource.create(d3d->device.Get(), "nyx:camera:cb");
}

void aega::LightResources::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  perFrameResource.create(d3d->device.Get(), "nyx:light:cb");
}

aega::ICameraController::ICameraController()
  : attachedCamera(0)
{
}

void aega::ICameraController::reattachCamera(Camera *camera) { attachedCamera = camera; }
void aega::ICameraController::onFrameMove(float elapsed) { }
void aega::ICameraController::beginDrag(float x, float y) { }
void aega::ICameraController::drag(float x, float y) { }
void aega::ICameraController::endDrag(float x, float y) { }
void aega::ICameraController::keyPressed(uint32_t) { }
void aega::ICameraController::keyReleased(uint32_t) { }

aega::ModelViewController::ModelViewController()
  : pitchD(0)
  , yawD(0)
  , pitch(0)
  , yaw(0)
  , zoomD(0)
  , previousX(0)
  , previousY(0)
  , dragging(false)
{
}

void aega::ModelViewController::updatePitchYaw()
{
  yaw = pitchZ
    ? gte::ACosEstimate<float>::Degree<3>(attachedCamera->focusDirectionU.z)
    : gte::ACosEstimate<float>::Degree<3>(attachedCamera->focusDirectionU.x);
  pitch = gte::ASinEstimate<float>::Degree<3>(attachedCamera->focusDirectionU.y);
}

void aega::ModelViewController::reattachCamera(Camera *camera)
{
  if (attachedCamera = camera) 
    updatePitchYaw(), 
    updatePitchZ();
}

void aega::ModelViewController::updatePitchZ()
{
  pitchZ
    = abs(attachedCamera->focusDirectionU.z)
    < abs(attachedCamera->focusDirectionU.x);
}

void aega::ModelViewController::finalizeFrameMove()
{
  using namespace DirectX;

  zoomD -= zoomD * 0.1f;
  pitchD -= pitchD * 0.1f;
  yawD -= yawD * 0.1f;

  rotQD = (updatePitchZ(), pitchZ)
    ? XMQuaternionRotationRollPitchYaw(0.0f, yawD, pitchD)
    : XMQuaternionRotationRollPitchYaw(pitchD, yawD, 0.0f);

  //rotM = XMMatrixMultiply(XMMatrixRotationY(yawD), XMMatrixRotationX(pitchD));
  traM = XMMatrixTranslation(
    attachedCamera->focusPosition.x,
    attachedCamera->focusPosition.y,
    attachedCamera->focusPosition.z
    );
  traInvM = XMMatrixTranslation(
    -attachedCamera->focusPosition.x,
    -attachedCamera->focusPosition.y,
    -attachedCamera->focusPosition.z
    );

  eyeV = XMLoadFloat3(&attachedCamera->eyePosition);
  eyeV = XMVector3Transform(eyeV, traInvM);
  //eyeV = XMVector3Transform(eyeV, rotM);
  eyeV = XMVector3Rotate(eyeV, rotQD);
  eyeV = XMVector3Transform(eyeV, traM);

  XMStoreFloat3(&attachedCamera->eyePosition, eyeV);

  if (attachedCamera->focusDirection.LengthSquared() > 1.0f || zoomD < 0.0f)
    attachedCamera->eyePosition += attachedCamera->focusDirectionU * zoomD;

  attachedCamera->updateView();
  updatePitchYaw();
}

void aega::ModelViewController::finalizeDrag()
{
  const float thres = 0.1f;
  pitchD = std::max(-thres, pitchD);
  pitchD = std::min(thres, pitchD);
  yawD = std::max(-thres, yawD);
  yawD = std::min(thres, yawD);
}

void aega::ModelViewController::finalizeZoom()
{
  attachedCamera->updateView();
  updatePitchYaw();
}

void aega::ModelViewController::onFrameMove(float elapsed)
{
  pitch += pitchD * elapsed;
  yaw += yawD * elapsed;
  finalizeFrameMove();
}

void aega::ModelViewController::beginDrag(float x, float y)
{
  previousX = x;
  previousY = y;
  dragging = true;
}

void aega::ModelViewController::drag(float x, float y)
{
  if (!dragging) return;

  invPitch = pitchZ
    ? (attachedCamera->eyePosition.x < 0.0f)
    : (attachedCamera->eyePosition.z > 0.0f);

#if 0
  diffY = invPitch ? previousY - y : y - previousY;
  diffX = x - previousX;
#else
  diffY = invPitch ? -y : y;
  diffX = x;
#endif

  pitchD += diffY * 0.001f;
  yawD += diffX * 0.001f;
  finalizeDrag();

  previousX = x;
  previousY = y;
}

void aega::ModelViewController::zoom(float z)
{
  if (!dragging) return;
  zoomD += z * 0.001f;
}

void aega::ModelViewController::endDrag(float x, float y)
{
  dragging = false;
}

aega::FreelookController::FreelookController()
  : boost(false)
  , pressingRight(false)
  , pressingForward(false)
{
  upV = DirectX::g_XMIdentityR1;
}

void aega::FreelookController::reattachCamera(Camera *camera)
{
  ModelViewController::reattachCamera(camera);

  velocityMag = 0.0f;
  velocityDirU = attachedCamera->focusDirectionU;
}

void aega::FreelookController::onFrameMove(float elapsed)
{
  using namespace DirectX;

  yawD -= yawD * 0.1f;
  pitchD -= pitchD * 0.1f;

  rotQD = XMQuaternionRotationRollPitchYaw(pitchD, yawD, 0.0f);
  focusV = XMLoadFloat3(&attachedCamera->focusDirectionU);
  focusV = XMVector3Rotate(focusV, rotQD);
  rightV = XMVector3Cross(upV, focusV);
  XMStoreFloat3(&attachedCamera->focusDirectionU, focusV);
  XMStoreFloat3(&rightU, rightV);
  rightU.Normalize();

  attachedCamera->eyePosition += rightU * rightD;
  attachedCamera->eyePosition += attachedCamera->focusDirectionU * (zoomD + forwardD);
  attachedCamera->focusPosition = attachedCamera->eyePosition + attachedCamera->focusDirectionU * 1000.0f;

  attachedCamera->updateView();
  updatePitchYaw();

  zoomD -= zoomD * 0.05f;
  if (!pressingRight) rightD -= rightD * 0.3f;
  if (!pressingForward) forwardD -= forwardD * 0.3f;
}

void aega::FreelookController::finalizeMovement()
{
  static const float thres = 0.3f;
  rightD = clamp(rightD, -thres, thres);
  forwardD = clamp(forwardD, -thres, thres);
}

void aega::FreelookController::keyPressed(uint32_t vk)
{
  switch (vk)
  {
  case dx::VirtualKey::LeftShift:
    boost = true;
    break;
  case dx::VirtualKey::A:
  case dx::VirtualKey::D:
    pressingRight = true;
    break;
  case dx::VirtualKey::W:
  case dx::VirtualKey::S:
    pressingForward = true;
    break;
  }

  switch (vk)
  {
  case dx::VirtualKey::D:
    rightD += boost ? 0.1f : 0.05f;
    break;
  case dx::VirtualKey::A:
    rightD -= boost ? 0.1f : 0.05f;
    break;
  case dx::VirtualKey::W:
    forwardD += boost ? 0.1f : 0.05f;
    break;
  case dx::VirtualKey::S:
    forwardD -= boost ? 0.1f : 0.05f;
    break;
  }

  finalizeMovement();
}

void aega::FreelookController::drag(float x, float y)
{
  if (!dragging) return;

  float diffY = attachedCamera->focusDirectionU.z < 0.0f ? -y : y;
  float diffX = x;
  pitchD += diffY * 0.0001f;
  yawD += diffX * 0.0001f;
  finalizeDrag();

  previousX = x;
  previousY = y;
}

void aega::FreelookController::keyReleased(uint32_t vk)
{
  switch (vk)
  {
  case dx::VirtualKey::LeftShift:
    boost = false;
    break;
  case dx::VirtualKey::D:
  case dx::VirtualKey::A:
    pressingRight = false;
    break;
  case dx::VirtualKey::W:
  case dx::VirtualKey::S:
    pressingForward = false;
    break;
  }

  switch (vk)
  {
  case dx::VirtualKey::Space:
    forwardD += forwardD * 0.1f;
    break;
  }

  finalizeMovement();
}
