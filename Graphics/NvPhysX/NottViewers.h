#pragma once

#include "AegaCamera.h"
#include <GteRigidBody.h>

namespace nott
{
  class Viewer : public aega::ICameraController
  {
    typedef float Real;
    gte::Vector3<Real> forceHook(
      Real t,                                       // time of application
      Real mass,                                    // mass
      gte::Vector3<Real> const & position,          // position
      gte::Quaternion<Real> const & orientation,    // orientation
      gte::Vector3<Real> const & linear_moment,     // linear momentum
      gte::Vector3<Real> const & angular_momentum,  // angular momentum
      gte::Matrix3x3<Real> const & orientationQ,    // orientation
      gte::Vector3<Real> const & linear_velocity,   // linear velocity
      gte::Vector3<Real> const & angular_velocity   // angular velocity
      );
    gte::Vector3<Real> torqueHook(
      Real t,                                       // time of application
      Real mass,                                    // mass
      gte::Vector3<Real> const & position,          // position
      gte::Quaternion<Real> const & orientation,    // orientation
      gte::Vector3<Real> const & linear_moment,     // linear momentum
      gte::Vector3<Real> const & angular_momentum,  // angular momentum
      gte::Matrix3x3<Real> const & orientationQ,    // orientation
      gte::Vector3<Real> const & linear_velocity,   // linear velocity
      gte::Vector3<Real> const & angular_velocity   // angular velocity
      );

    void update(float t, float dt);
    void forkPosition();
    void setPosition();
    void setOrientation();

    bool boostPressed;
    bool forwardPressed;
    bool backwardPressed;
    bool leftPressed;
    bool rightPressed;

    float diffX, diffY;
    bool pitchZ, invPitch;

    bool dragging;
    float pitch, yaw;
    float pitchD, yawD, zoomD;
    float previousX, previousY;
    DirectX::XMVECTOR eyeV, rotQD;
    DirectX::XMMATRIX traM, traInvM, rotM;
    dx::Vector3 eyeU, rightU;
    DirectX::XMVECTOR focusV, upV, rightV, forwardV;

    void updatePitchYaw();
    void finalizeDrag();
    void finalizeZoom();
    void updatePitchZ();

  public:
    float t;
    float dt;
    gte::RigidBody<Real> body;

  public:

    Viewer();
    virtual void reattachCamera(aega::Camera *camera);
    virtual void onFrameMove(float elapsed);
    virtual void keyPressed(uint32_t vk);
    virtual void keyReleased(uint32_t vk);

  public:
    virtual void beginDrag(float x, float y);
    virtual void drag(float x, float y);
    virtual void zoom(float z);
    virtual void endDrag(float x, float y);

  };
}

