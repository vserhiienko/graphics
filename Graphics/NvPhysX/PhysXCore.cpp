#include "PhysXCore.h"

nyx::PhysXCore::PhysXCore()
  : tolerances()
  , debuggerFile()
{
  physics = 0;
  foundation = 0;
  errorCallback = 0;
  allocCallback = 0;
  debuggerHost = "127.0.0.1";
  debuggerHostPort = 5425;
  debuggerConnection = 0;
  debuggerConnectionHandler = 0;
  debuggerConnectionTimeout = 1000;
}

nyx::PhysXCore::~PhysXCore()
{
  releasePhysX();
}

void nyx::PhysXCore::releasePhysX()
{
  if (debuggerConnection) debuggerConnection->release();
  if (physics) physics->release();
  if (foundation) foundation->release();
}

bool nyx::PhysXCore::initializePhysX()
{
  releasePhysX();

  if (errorCallback == 0)
  {
    dx::trace("nyx:pxcore: using default physx error callback");
    errorCallback = &defaultErrorCallback;
  }

  if (allocCallback == 0)
  {
    dx::trace("nyx:pxcore: using default physx allocation callback");
    allocCallback = &defaultAllocCallback;
  }

  if (foundation = PxCreateFoundation(PX_PHYSICS_VERSION, dx::toRef(allocCallback), dx::toRef(errorCallback)))
  {
    if (physics = PxCreatePhysics(PX_PHYSICS_VERSION, dx::toRef(foundation), tolerances))
    {
      if (physics->getPvdConnectionManager())
      {
        if (debuggerFile.empty())
          debuggerConnection = physx::PxVisualDebuggerExt::createConnection(
          physics->getPvdConnectionManager(),
          debuggerHost.c_str(),
          debuggerHostPort,
          debuggerConnectionTimeout
          );
        else
          debuggerConnection = physx::PxVisualDebuggerExt::createConnection(
          physics->getPvdConnectionManager(),
          debuggerFile.c_str()
          );
      }

      if (debuggerConnection == 0)
        dx::trace("nyx:pxcore: visual debugger is unavailable");
      else
      {
        if (physics->getPvdConnectionManager()
          && debuggerConnectionHandler
          )
          physics->getPvdConnectionManager()->addHandler(dx::toRef(debuggerConnectionHandler)),
          dx::trace("nyx:pxcore: visual debugger connection handler is attached");

        dx::trace("nyx:pxcore: visual debugger is connected");
      }

      return true;
    }
  }

  if (foundation)
  {
    dx::trace("nyx:pxcore: failed to initialize physx foundation");
    foundation->release();
  }

  dx::trace("nyx:pxcore: failed to initialize physx device");
  return false;
}