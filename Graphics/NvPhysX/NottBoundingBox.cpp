
#include <DirectXColors.h>

#include "NottBoundingBox.h"

using namespace nott;
using namespace DirectX;

static void invertRT(
  XMFLOAT4X4 const &matrix,
  XMFLOAT3 const &p,
  XMFLOAT3 &t
  )
{
  t = p;
  t.x -= matrix._41;
  t.y -= matrix._42;
  t.z -= matrix._43;
  
  XMFLOAT4X4 rotate;
  memcpy(&rotate, &matrix, sizeof(XMFLOAT4X4));
  rotate._41 = 0.0f;
  rotate._42 = 0.0f;
  rotate._43 = 0.0f;

  XMVECTOR tt = XMVector3Transform(XMLoadFloat3(&t), XMLoadFloat4x4(&rotate));
  XMStoreFloat3(&t, tt);
}

static void rotate(
  XMFLOAT4X4 const &matrix,
  XMFLOAT3 const &p,
  XMFLOAT3 &t
  )
{
  t = p;

  XMFLOAT4X4 rotate;
  memcpy(&rotate, &matrix, sizeof(XMFLOAT4X4));
  rotate._41 = 0.0f;
  rotate._42 = 0.0f;
  rotate._43 = 0.0f;

  XMVECTOR tt = XMVector3Transform(XMLoadFloat3(&t), XMLoadFloat4x4(&rotate));
  XMStoreFloat3(&t, tt);
}

void BoundingBoxUtility::bestFit(
  DirectX::BoundingOrientedBox &bobox,
  XMFLOAT4X4 &matrix,
  std::vector<XMFLOAT3> const &point_cloud,
  XMFLOAT3 &sides
  )
{
  typedef std::vector<XMFLOAT3>::size_type _SizeTy;
  typedef std::vector<XMFLOAT3>::value_type _PointTy;

  _PointTy min, max;
  min.x = +1e9; min.y = +1e9; min.z = +1e9;
  max.x = -1e9; max.y = -1e9; max.z = -1e9;

  _SizeTy sz = point_cloud.size(), ind = 0;
  auto const &points = point_cloud.data();
  for (ind = 0; ind < sz; ++ind)
  {
    auto const &p = points[ind];
    _PointTy t;

    invertRT(matrix, p, t);
    if (t.x < min.x) min.x = t.x;
    if (t.y < min.y) min.y = t.y;
    if (t.z < min.z) min.z = t.z;
    if (t.x > max.x) max.x = t.x;
    if (t.y > max.y) max.y = t.y;
    if (t.z > max.z) max.z = t.z;

  }

  sides.x = max.x - min.x;
  sides.y = max.y - min.y;
  sides.z = max.z - min.z;

  _PointTy centre, ocentre;
  centre.x = (max.x - min.x) * 0.5f + min.x;
  centre.y = (max.y - min.y) * 0.5f + min.y;
  centre.z = (max.z - min.z) * 0.5f + min.z;

  rotate(matrix, centre, ocentre);
  matrix._41 += ocentre.x;
  matrix._42 += ocentre.y;
  matrix._43 += ocentre.z;

}

void BoundingBoxUtility::bestFit2(
  DirectX::BoundingOrientedBox &bobox,
  std::vector<XMFLOAT3> const &point_cloud
  )
{
  DirectX::BoundingOrientedBox::CreateFromPoints(
    bobox, 
    point_cloud.size(), 
    point_cloud.data(), 
    sizeof(XMFLOAT3)
    );
}

const double FM_PI = 3.141592654f;
const double FM_DEG_TO_RAD = ((2.0f * FM_PI) / 360.0f);
const double FM_RAD_TO_DEG = (360.0f / (2.0f * FM_PI));

void BoundingBoxUtility::bestFit(
  DirectX::BoundingOrientedBox &bobox,
  std::vector<XMFLOAT3> const &point_cloud
  )
{
  typedef std::vector<XMFLOAT3>::size_type _SizeTy;
  typedef std::vector<XMFLOAT3>::value_type _PointTy;

  if (point_cloud.empty()) return;

  _PointTy min, max;
  min = point_cloud[0];
  max = point_cloud[0];

  _SizeTy sz = point_cloud.size(), ind = 0;
  auto const &points = point_cloud.data();
  for (ind = 0; ind < sz; ++ind)
  {
    auto const &p = points[ind]; 
    if (p.x < min.x) min.x = p.x;
    if (p.y < min.y) min.y = p.y;
    if (p.z < min.z) min.z = p.z;
    if (p.x > max.x) max.x = p.x;
    if (p.y > max.y) max.y = p.y;
    if (p.z > max.z) max.z = p.z;
  }

  _PointTy centre;
  centre.x = (max.x - min.x) * 0.5f + min.x;
  centre.y = (max.y - min.y) * 0.5f + min.y;
  centre.z = (max.z - min.z) * 0.5f + min.z;

  float ax = 0;
  float ay = 0;
  float az = 0;

  float sweep = 45.0f; // 180 degree sweep on all three axes.
  float steps = 7.0f; // 7 steps on each axis)

  float bestVolume = 1e9;
  float angle[3];

  while (sweep >= 1)
  {
    bool found = false;
    float stepsize = sweep / steps;

    for (float x = ax - sweep; x <= ax + sweep; x += stepsize)
    {
      for (float y = ay - sweep; y <= ay + sweep; y += stepsize)
      {
        for (float z = az - sweep; z <= az + sweep; z += stepsize)
        {
          auto pmatrixV = XMMatrixRotationRollPitchYaw(
            x * FM_DEG_TO_RAD, 
            y * FM_DEG_TO_RAD, 
            z * FM_DEG_TO_RAD
            );

          XMFLOAT4X4 pmatrix;
          XMStoreFloat4x4(&pmatrix, pmatrixV);

          /*
          double pmatrix[16];
          fm_eulerMatrix(x*FM_DEG_TO_RAD, y*FM_DEG_TO_RAD, z*FM_DEG_TO_RAD, pmatrix);
          */

          pmatrix._41 = centre.x;
          pmatrix._42 = centre.y;
          pmatrix._43 = centre.z;

          XMFLOAT3 psides;

          bestFit(bobox, pmatrix, point_cloud, psides);

          float volume = psides.x * psides.y * psides.z; // the volume of the cube

          if (volume < bestVolume)
          {
            bestVolume = volume;

            bobox.Extents.x = psides.x;
            bobox.Extents.y = psides.x;
            bobox.Extents.z = psides.x;

            angle[0] = ax;
            angle[1] = ay;
            angle[2] = az;

            bobox.Center.x = pmatrix._41;
            bobox.Center.y = pmatrix._42;
            bobox.Center.z = pmatrix._43;

            XMStoreFloat4(&bobox.Orientation, XMQuaternionRotationMatrix(XMLoadFloat4x4(&pmatrix)));
            found = true; // yes, we found an improvement.
          }
        }
      }
    }

    if (found)
    {

      ax = angle[0];
      ay = angle[1];
      az = angle[2];

      sweep *= 0.5f; // sweep 1/2 the distance as the last time.
    }
    else
    {
      break; // no improvement, so just
    }

  }
}

BoundingBoxResources::BoundingBoxResources(dx::DxDeviceResources *deviceResources)
{
  d3d = deviceResources;
  perFrameSource.world = DirectX::XMMatrixIdentity();
  perFrameSource.color = DirectX::Colors::CornflowerBlue;

  //assert(d3d != 0);
  //d3d->deviceLost += _Dx_delegate_to(this, &BoundingBoxResources::onDeviceLost);
  //d3d->deviceRestored += _Dx_delegate_to(this, &BoundingBoxResources::onDeviceRestored);
  onDeviceRestored(d3d);
}

void BoundingBoxResources::update(void)
{
  assert(d3d != 0);
  perFrameResource.setData(d3d->context.Get(), perFrameSource);
}

void BoundingBoxResources::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void BoundingBoxResources::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  perFrameResource.create(d3d->device.Get(), "nott:bobox:cb");
}

BoundingBoxRenderer::BoundingBoxRenderer(dx::DxDeviceResources *d3d)
{
  assert(d3d != 0);
  d3d->deviceLost += _Dx_delegate_to(this, &BoundingBoxRenderer::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &BoundingBoxRenderer::onDeviceRestored);
  onDeviceRestored(d3d);
}

void BoundingBoxRenderer::create(void)
{
  assert(d3d != 0);

  assert(SUCCEEDED(dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"./cso/BBoxO.VS.cso",
    0,
    0,
    vs.ReleaseAndGetAddressOf(),
    0
    )));
  assert(SUCCEEDED(dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"./cso/BBoxO.Lines.GS.cso",
    gsLines.ReleaseAndGetAddressOf()
    )));
  assert(SUCCEEDED(dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"./cso/BBoxO.Polies.GS.cso",
    gsPolies.ReleaseAndGetAddressOf()
    )));
  assert(SUCCEEDED(dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"./cso/BBoxO.Points.GS.cso",
    gsPolies.ReleaseAndGetAddressOf()
    )));
  assert(SUCCEEDED(dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"./cso/BBoxO.PS.cso",
    ps.ReleaseAndGetAddressOf()
    )));
}

void BoundingBoxRenderer::render(BoundingBoxResources &bboxGPU, aega::CameraResources &cameraGPU)
{
  static UINT32 strides[] = { 0 };
  static UINT32 offsets[] = { 0 };

  auto pipeline = d3d->context.Get();

  pipeline->IASetInputLayout(0);
  pipeline->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

  pipeline->VSSetShader(vs.Get(), 0, 0);
  pipeline->GSSetShader(gsLines.Get(), 0, 0);
  pipeline->PSSetShader(ps.Get(), 0, 0);

  pipeline->GSSetConstantBuffers(0, 1, bboxGPU.perFrameResource.getBufferAddress());
  pipeline->GSSetConstantBuffers(1, 1, cameraGPU.perFrameResource.getBufferAddress());

  pipeline->Draw(1, 0);

  pipeline->GSSetShader(0, 0, 0);
}

void BoundingBoxRenderer::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void BoundingBoxRenderer::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  create();
}