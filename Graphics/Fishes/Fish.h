#pragma once
#include "Flagellum.h"

namespace nyx
{
  class Fish
    : public Boid
  {
  public:
    dx::Color mainColor;
    unsigned bodySegCount, tailSegCount, finSegCount;
    dx::Vector2 origBodySize, bodySize, tailSize, finSize;
    std::unique_ptr<Flagellum> body, tailL, tailR, finL, finR;

    unsigned food;

  public:
    Fish(dx::Vector2 location, float maxVelocity, float maxForce, int tailLength = 64);
    void update(void);
    void eat(void);

  };

  class FishRenderer
  {
  public:
    dx::DxDeviceResources *d3d;

  public:
    nyx::BoidRenderer boidRenderer;
    nyx::BoidResources boidResources;
    nyx::FlagellumRenderer flagellumRenderer;


  public:
    FishRenderer(dx::DxDeviceResources*);
    void createDeviceResources(void);
    void render(CameraResources const &, Fish const &);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

}