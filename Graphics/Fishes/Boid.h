#pragma once
#include "Utils.h"
#include <DxUICore.h>
#include <DxResourceManager.h>
#include <DxDeviceResources.h>
#include <DxConstantBuffer.h>

namespace nyx
{
  /// <summary>
  /// Containts boid visual parameters
  /// </summary>
  class BoidVisuals
  {
  public:
    dx::Vector2 size;
    dx::Color color;
  };

  /// <summary>
  /// Containts boid parameters
  /// </summary>
  class BoidView
  {
  public:
    bool hasArrive;

    dx::Vector2 loc;
    dx::Vector2 vel;
    dx::Vector2 acc;
    dx::Vector2 velMin;
    dx::Vector2 velMax;
    dx::Vector2 forceMin;
    dx::Vector2 forceMax;
    std::vector<dx::Vector2> tail;

    float forceMaxValue;
    float velMaxValue, velMaxValueSq;
    float steerArrivalDistance, steerArrivalDistanceSq;
    float steerSlowdownDistance, steerSlowdownDistanceSq;
    float wanderR, wanderD, wanderDelta, wanderTheta;

  };

  /// <summary>
  /// Controls boid parameters
  /// </summary>
  class Boid 
    : public BoidView
    , public BoidVisuals
  {
    void createBoid(dx::Vector2 location, float maxVel, float maxForce);

  public:
    Boid(dx::Vector2 location, float maxVelocity, float maxForce, int tailLength = 64);

    void setForceLimit(float forceLimit);
    void setVelocityLimit(float velLimit);

    void update(void);
    dx::Vector2 steerForce(dx::Vector2 const &target, bool slowdown);
    void seek(dx::Vector2 const &target);
    void flee(dx::Vector2 const &target);
    void arrive(dx::Vector2 const &target);
    void evade(dx::Vector2 const &target);
    void wander(void);

  };

  /// <summary>
  /// Contains boid GPU resources
  /// </summary>
  class BoidResources
  {
  public:
    struct PerFrame
    {
      dx::Matrix world;
      dx::Vector4 color;
    };
    struct Vertex
    {
      DirectX::Vector4 position;
      DirectX::Vector3 normal;
      DirectX::Vector2 tex;
    };
    typedef Vertex VertexUnit;
    typedef unsigned __int16 Index, IndexUnit;

  public:
    dx::DxDeviceResources *d3d;

    dx::Buffer indexBuffer;
    dx::Buffer vertexBuffer;
    dx::Texture2 texture;
    dx::SRView textureView;

    PerFrame perFrameSource;
    dx::ConstantBuffer<PerFrame> perFrameResource;
    static dx::VSInputElementDescription const layoutElements[3];
    static unsigned const layoutElementCount;

  public:
    BoidResources(dx::DxDeviceResources*);
    void createDeviceResources(void);
    void update(Boid const &);
    void update(dx::Vector2 const &loc, dx::Vector2 const &sz, dx::Color const &);
    void update(dx::Vector2 const &loc, dx::Vector2 const &sz, dx::Color const &, dx::Matrix const &world);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

  /// <summary>
  /// Maintains camera parameters
  /// </summary>
  class Camera
    : public Boid
  {
  protected:
    float nearPlane, farPlane;
    dx::Vector4 upDirection;
    dx::Vector4 eyePosition;
    dx::Vector4 focusPosition;

  public:
    dx::Matrix view;
    dx::Matrix projection;

  public:
    float screenW, screenH;

  public:
    Camera(void);

  public:
    void updateView(void);
    void updateProjection(void);

  };

  /// <summary>
  /// Maintains camera GPU resources
  /// </summary>
  class CameraResources
  {
  public:
    struct PerFrame
    {
      dx::XMMATRIX view;
      dx::XMMATRIX projection;
    };

  public:
    dx::DxDeviceResources *d3d;

    PerFrame perFrameSource;
    dx::ConstantBuffer<PerFrame> perFrameResource;

  public:
    CameraResources(dx::DxDeviceResources*);
    void update(Camera const &);
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);
  };


  class BoidRenderer
  {
  public:
    dx::DxDeviceResources *d3d;

  public:
    dx::VShader vertexShader;
    dx::VSLayout inputLayout;
    dx::PShader pixelShader;
    dx::BlendState blendState;
    dx::SamplerState samplerState;
    dx::RasterizerState rasterizerState;
    dx::DepthStencilState depthStencilState;

  public:
    BoidRenderer(dx::DxDeviceResources*);
    void createDeviceResources(void);
    void render(CameraResources const &, BoidResources const &) const;
    void renderTail(CameraResources const &, BoidResources &, Boid const &) const;

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

}