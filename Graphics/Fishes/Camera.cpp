#include "Boid.h"

nyx::Camera::Camera()
  : Boid(dx::Vector2(0, 0), 5, 2)
{
  screenW = 0.0f;
  screenH = 0.0f;
  farPlane = 100.0f;
  nearPlane = 0.01f;
  upDirection = DirectX::XMVectorSet(0.f, 1.f, 0.f, 1.f);
  eyePosition = DirectX::XMVectorSet(0.f, 0.f, -1.f, 1.f);
  focusPosition = DirectX::XMVectorSet(0.f, 0.f, +1.f, 1.f);
}

void nyx::Camera::updateView()
{
  eyePosition.x = loc.x;
  eyePosition.y = loc.y;
  focusPosition.x = loc.x;
  focusPosition.y = loc.y;

  view = DirectX::XMMatrixLookAtLH(
    eyePosition, focusPosition, upDirection
    );
}

void nyx::Camera::updateProjection()
{
  projection = DirectX::XMMatrixOrthographicLH(
    screenW, screenH, nearPlane, farPlane
    );
}

nyx::CameraResources::CameraResources(dx::DxDeviceResources *device) : d3d(device)
{
  assert(d3d != 0);
  d3d->deviceLost += _Dx_delegate_to(this, &CameraResources::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &CameraResources::onDeviceRestored);
  onDeviceRestored(d3d);
}

void nyx::CameraResources::update(Camera const &data)
{
  assert(d3d != 0);
  perFrameSource.view = data.view;
  perFrameSource.projection = data.projection;
  perFrameResource.setData(d3d->context.Get(), perFrameSource);
}

void nyx::CameraResources::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void nyx::CameraResources::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  perFrameResource.create(d3d->device.Get(), "nyx:camera:cb");
}

