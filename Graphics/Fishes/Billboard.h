#pragma once
#include "Utils.h"
#include "Boid.h"
#include <DxUICore.h>
#include <DxResourceManager.h>
#include <DxDeviceResources.h>
#include <DxConstantBuffer.h>

namespace nyx
{
  class BillboardDesc
  {
  public:
    bool upY;
    float lenX, lenY;
    unsigned resX, resY;
  };

  class BillboardResources
    : public BillboardDesc
  {
  public:
    struct PerFrame
    {
      dx::Matrix pose;
      dx::Color color;
    };

  public:
    dx::DxDeviceResources *d3d;

  public:
    dx::Buffer vertexBuffer;
    dx::Buffer indexBuffer;
    dx::ConstantBuffer<PerFrame> perFrameResource;

    size_t indexCount;
    PerFrame perFrameSource;


  public:
    BillboardResources(dx::DxDeviceResources*);
    void createDeviceResources(void);
    void update(void);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

  class BillboardRenderer
  {
  public:
    dx::DxDeviceResources *d3d;

  public:
    dx::VShader vertexShader;
    dx::VSLayout inputLayout;
    dx::PShader pixelShader;
    dx::BlendState blendState;
    dx::SamplerState samplerState;
    dx::RasterizerState rasterizerState;
    dx::DepthStencilState depthStencilState;

  public:
    BillboardRenderer(dx::DxDeviceResources*);
    void createDeviceResources(void);
    void render(CameraResources &, BillboardResources &);

  public:
    void onDeviceLost(dx::DxDeviceResources*);
    void onDeviceRestored(dx::DxDeviceResources*);

  };

}

