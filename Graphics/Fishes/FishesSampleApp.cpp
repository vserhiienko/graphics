#include "FishesSampleApp.h"

#include <DxUtils.h>
#include <DirectXColors.h>

#define SampleApp_StartupURL "file:///D:/Dev/Vs/Proj/Graphics/Graphics/Fishes/assets/pages/UIExample/index.html"

dx::DxSampleApp *dx::DxSampleAppFactory()
{
  return new nyx::FishesSampleApp();
}

nyx::FishesSampleApp::FishesSampleApp()
  : boid(dx::Vector2(0, 0), 2.5f, 2.5f)
  , fish(dx::Vector2(0, 0), 2.0f, 2.0f)
  , boidRenderer(&deviceResources)
  , boidResources(&deviceResources)
  , cameraResources(&deviceResources)
  , billboardRenderer(&deviceResources)
  , billboardResources(&deviceResources)
  , fishRenderer(&deviceResources)
{
  mouseMoved = false;
  mousePressed = false;
}

nyx::FishesSampleApp::~FishesSampleApp()
{
}

std::string nyx::FishesSampleApp::getUIURL()
{
  return SampleApp_StartupURL;
}

bool nyx::FishesSampleApp::onAppLoop()
{
  dx::trace("nyx:app: initialize sample");
  window.setWindowTitle("Fishes Sample");
  timer.reset();

  camera.screenW = deviceResources.actualRenderTargetSize.Width * 2;
  camera.screenH = deviceResources.actualRenderTargetSize.Height * 2;
  camera.updateProjection();
  camera.vel = dx::Vector2();

  dx::Vector2 spawnBoudaries;
  spawnBoudaries.x = camera.screenW * 0.5f;
  spawnBoudaries.y = camera.screenH * 0.5f;

  spawnBoudaries *= 0.8f;
  for (int i = 0; i < 256; i++)
  {
    boids.push_back(new Boid(dx::Vector2(0, 0), 1.0f, 1.0f, (int)random(1, 5)));
    boids.back()->loc.x = random(-spawnBoudaries.x, spawnBoudaries.x);
    boids.back()->loc.y = random(-spawnBoudaries.y, spawnBoudaries.y);
    boids.back()->vel.x = random(-0.5f, 0.5f);
    boids.back()->vel.y = random(-0.5f, 0.5f);
    boids.back()->size *= random(1.0f, 150.0f);
    boids.back()->wanderTheta = random(0.0f, dx::XM_2PI * 2.0f);
    boids.back()->color = (dx::XMVECTOR) DirectX::Colors::LightBlue;
    boids.back()->color.x *= random(0.8f, 1.0f);
    boids.back()->color.y *= random(0.8f, 1.0f);
    boids.back()->color.z *= random(0.8f, 1.0f);
    boids.back()->color.w *= random(0.05f, 0.1f);
  }

  spawnBoudaries *= 0.8f;
  for (int i = 0; i < 8; i++)
  {
    fishes.push_back(new Fish(dx::Vector2(0, 0), 2.0f, 2.0f));
    fishes.back()->loc.x = random(-spawnBoudaries.x, spawnBoudaries.x);
    fishes.back()->loc.y = random(-spawnBoudaries.y, spawnBoudaries.y);
    fishes.back()->vel.x = random(-0.5f, 0.5f);
    fishes.back()->vel.y = random(-0.5f, 0.5f);
    fishes.back()->finL->spineColor.x *= random(0.8f, 1.0f);
    fishes.back()->finL->spineColor.y *= random(0.8f, 1.0f);
    fishes.back()->finL->spineColor.z *= random(0.8f, 1.0f);
    fishes.back()->body->spineColor.x *= random(0.8f, 1.0f);
    fishes.back()->body->spineColor.y *= random(0.8f, 1.0f);
    fishes.back()->body->spineColor.z *= random(0.8f, 1.0f);
    fishes.back()->tailL->spineColor.x *= random(0.8f, 1.0f);
    fishes.back()->tailL->spineColor.y *= random(0.8f, 1.0f);
    fishes.back()->tailL->spineColor.z *= random(0.8f, 1.0f);
    fishes.back()->finR->spineColor = fishes.back()->finL->spineColor;
    fishes.back()->tailR->spineColor = fishes.back()->tailL->spineColor;
  }

  target.resize(fishes.size());
  return true;
}

void nyx::FishesSampleApp::sizeChanged()
{

}

void nyx::FishesSampleApp::onUICreated()
{
}

void nyx::FishesSampleApp::onUIDestroyed()
{

}

void nyx::FishesSampleApp::keyReleased(dx::DxGenericEventArgs const &args)
{
  switch (args.keyCode())
  {
  case dx::VirtualKey::Escape: window.close(); break;
  default: {} break;
  }
}

void nyx::FishesSampleApp::pointerMoved(dx::DxGenericEventArgs const &args)
{
  mouseMoved = true;
}

void nyx::FishesSampleApp::pointerLeftPressed(dx::DxGenericEventArgs const &args)
{
  mousePressed = true;
}

void nyx::FishesSampleApp::pointerLeftReleased(dx::DxGenericEventArgs const &args)
{
  mousePressed = false;
  fish.eat();
}


void nyx::FishesSampleApp::handlePaint(dx::DxWindow const*)
{
  /// update
  timer.update();
  // update scene
#if 1
  for (auto &b : boids)
  {
    b->wander();
    b->update();
  }
  for (auto &f : fishes)
  {
    f->wander();
    f->update();
  }
#else
  concurrency::parallel_for(size_t(0), boids.size(), [&](size_t ind)
  {
    boids[ind]->wander();
    boids[ind]->update();
  });
  concurrency::parallel_for(size_t(0), fishes.size(), [&](size_t ind)
  {
    fishes[ind]->wander();
    fishes[ind]->update();
  });
#endif

  //fish.update();
  //camera.arrive(fish.loc);
  camera.update();
  camera.updateView();
  cameraResources.update(camera);
  // update ui
  //dxui::DxUIClient::updateApplication();
  //updateUI(timer.elapsed);

  /// draw
  beginScene();
  // render scene
  //billboardRenderer.render(cameraResources, billboardResources);
  for (auto &b : boids)
  {
    boidResources.update((*b));
    boidRenderer.render(cameraResources, boidResources);
    boidRenderer.renderTail(cameraResources, boidResources, (*b));
  }
  //fishRenderer.render(cameraResources, fish);
  for (auto &f : fishes)
  {
    fishRenderer.render(cameraResources, *f);
  }
  // render ui
  //renderUI();
  endScene();

  mouseMoved = false;

}
