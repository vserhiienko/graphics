#pragma once
#include <DxMath.h>

namespace nyx
{
  static inline float random(float min, float max)
  {
    return float(rand()) / float(RAND_MAX) * (max - min) + min;
  }

  static float solveHermiteSegment(
    float p0, float m0, // left
    float p1, float m1, // right
    float t
    )
  {
    float const t2 = t * t;
    float const t3 = t2 * t;
    float const h1 = 2 * t3 - 3 * t2 + 1;
    float const h2 = t3 - 2 * t2 + t;
    float const h3 = -2 * t3 + 3 * t2;
    float const h4 = t3 - t2;

    return h1 * p0 + h2 * m0 + h3 * p1 + h4 * m1;
  }

}