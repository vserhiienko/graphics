#include "Boid.h"
#include <DxUtils.h>
#include <DirectXColors.h>
#include <DxAssetManager.h>
#include <DxDDSLoader.h>
#include <DxResourceManager.h>


dx::VSInputElementDescription const nyx::BoidResources::layoutElements[3] =
{
  { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 16
  { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 28, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 12 (28)
};

unsigned const nyx::BoidResources::layoutElementCount = 
ARRAYSIZE(nyx::BoidResources::layoutElements);

void nyx::Boid::createBoid(dx::Vector2 location, float maxVel, float maxForce)
{
  hasArrive = false;

  setForceLimit(maxForce);
  setVelocityLimit(maxVel);

  loc = location;
  acc = dx::Vector2(0.f, 0.f);

  wanderR = 5.f;
  wanderD = 100.f;
  wanderTheta = 0.f;
  wanderDelta = 0.25f;
  steerArrivalDistance = 10.0f;
  steerSlowdownDistance = 60.0f;
  steerArrivalDistanceSq = 100.0f;
  steerSlowdownDistanceSq = 3600.0f;

  size.x = 10;
  size.y = 10;
  color = (dx::XMVECTOR)DirectX::Colors::OrangeRed;

}

nyx::Boid::Boid(dx::Vector2 location, float maxVelocity, float maxForce, int tailLength)
{
  createBoid(location, maxVelocity, maxForce);
  float vx = nyx::random(-maxVelocity, maxVelocity);
  float vy = nyx::random(-maxVelocity, maxVelocity);
  vel = dx::Vector2(vx, vy);
  tail.resize(tailLength);
}

void nyx::Boid::setForceLimit(float forceLimit)
{
  forceMaxValue = fabs(forceLimit);
  forceMax = dx::Vector2(forceMaxValue, forceMaxValue);
  forceMin = dx::Vector2(-forceMaxValue, -forceMaxValue);
}

void nyx::Boid::setVelocityLimit(float velLimit)
{
  velMaxValue = fabs(velLimit);
  velMaxValueSq = velMaxValue * velMaxValue;
  velMax = dx::Vector2(velMaxValue, velMaxValue);
  velMin = dx::Vector2(-velMaxValue, -velMaxValue);
}

void nyx::Boid::update()
{
  memcpy(&tail[1], &tail[0], sizeof(dx::Vector2) * (tail.size() - 1));
  tail[0] = loc;

  vel += acc;
  vel.LimitEst(velMaxValue);
  //vel.Clamp(velMin, velMax);
  loc += vel;
  acc *= 0.0f;
}

dx::Vector2 nyx::Boid::steerForce(dx::Vector2 const &target, bool slowdown)
{
  dx::Vector2 steerDirection = target - loc;
  float steerDistanceSq = steerDirection.LengthSquared();

  if (steerDistanceSq > FLT_EPSILON)
  {
    steerDirection.Normalize();
    if (slowdown && (steerDistanceSq < steerSlowdownDistanceSq))
    {
      steerDirection *= velMaxValue * (steerDistanceSq / steerSlowdownDistanceSq);
      if (steerDistanceSq < steerArrivalDistanceSq)
      {
        hasArrive = true;
      }
    }
    else
    {
      steerDirection *= velMaxValue;
    }

    dx::Vector2 steerForce = steerDirection - vel;
    steerForce.LimitEst(forceMaxValue);
    //steerForce.Clamp(forceMin, forceMax);
    return steerForce;
  }
  return dx::Vector2(0.f, 0.f);
} // steerForce

void nyx::Boid::seek(dx::Vector2 const &target) { acc += steerForce(target, false); }
void nyx::Boid::flee(dx::Vector2 const &target) { acc -= steerForce(target, false); }
void nyx::Boid::arrive(dx::Vector2 const &target) { acc += steerForce(target, true); }

void nyx::Boid::evade(dx::Vector2 const &target)
{
  float lookAhead = dx::Vector2::DistanceSquared(loc, target) / (velMaxValueSq * 4);
  dx::Vector2 predictedTarget = target - dx::Vector2(lookAhead, lookAhead);
  flee(predictedTarget);
} // evade

void nyx::Boid::wander()
{
  wanderTheta += random(-wanderDelta, wanderDelta);

  dx::Vector2 circleLoc = vel * wanderD + loc;
  dx::Vector2 circleOff = dx::Vector2(cosf(wanderTheta), sinf(wanderTheta)) * wanderR;
  dx::Vector2 target = circleLoc + circleOff;

  seek(target);
} // wander

nyx::BoidResources::BoidResources(dx::DxDeviceResources *device)
  : d3d(device)
{
  assert(d3d != 0);
  d3d->deviceLost += _Dx_delegate_to(this, &BoidResources::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &BoidResources::onDeviceRestored);
  onDeviceRestored(d3d);
}

void nyx::BoidResources::createDeviceResources(void)
{
  assert(d3d != 0);

  dx::HResult result = S_OK;
  dx::DxAssetManager::addSearchPath("Fishes");

  class TextureHook : public dx::DxAssetHook
  {
  public:
    nyx::BoidResources *boidResources;
    TextureHook(nyx::BoidResources *resources) : boidResources(resources) { }

  public:
    virtual void onAssetNotFound(_In_ unsigned assetId) override { assert(false); }
    virtual void onAssetLoaded(_In_ unsigned assetId, _In_ dx::DxAssetBuffer const &asset) override
    {
      dx::HResult result = S_OK;
      result = dx::dds::CreateDDSTextureFromMemory(
        boidResources->d3d->device.Get(), asset.data, asset.dataLength,
        (ID3D11Resource**)boidResources->texture.ReleaseAndGetAddressOf(),
        boidResources->textureView.ReleaseAndGetAddressOf()
        ); _Dx_hresult_assert(result);
    }
  } hook(this);
  dx::DxAssetManager::loadAsset("textures/particle.dds", 0, &hook);

  float
    w = (float)1.0f * 0.5f,
    h = (float)1.0f * 0.5f;

  Vertex squareVertices[4] =
  {
    { DirectX::Vector4(-w, +h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(0, 0) },
    { DirectX::Vector4(+w, +h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(1, 0) },
    { DirectX::Vector4(+w, -h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(1, 1) },
    { DirectX::Vector4(-w, -h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(0, 1) },
  };

  Index squareIndices[6] =
  {
    0, 1, 2,
    0, 2, 3,
  };

  result = dx::DxResourceManager::createVertexBuffer(
    d3d->device.Get(), ARRAYSIZE(squareVertices), squareVertices,
    vertexBuffer.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);

  result = dx::DxResourceManager::createIndexBuffer(
    d3d->device.Get(), ARRAYSIZE(squareIndices), squareIndices,
    indexBuffer.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);

  result = perFrameResource.create(
    d3d->device.Get(), "nyx:boid:cb"
    ); _Dx_hresult_assert(result);

}

void nyx::BoidResources::update(Boid const &data)
{
  update(data.loc, data.size, data.color);
}

void nyx::BoidResources::update(
  dx::Vector2 const &loc,
  dx::Vector2 const &sz,
  dx::Color const &color
  )
{
  assert(d3d != 0);
  perFrameSource.world
    = dx::Matrix::CreateScale(sz.x, sz.y, 1.0f)
    * dx::Matrix::CreateTranslation(loc.x, loc.y, 1.0f);
  perFrameSource.color = color.ToVector4();
  perFrameResource.setData(d3d->context.Get(), perFrameSource);
}

void nyx::BoidResources::update(
  dx::Vector2 const &loc,
  dx::Vector2 const &sz,
  dx::Color const &color,
  dx::Matrix const &world
  )
{
  assert(d3d != 0);
  perFrameSource.world
    = dx::Matrix::CreateScale(sz.x, sz.y, 1.0f)
    * dx::Matrix::CreateTranslation(loc.x, loc.y, 1.0f)
    * world;
  perFrameSource.color = color.ToVector4();
  perFrameResource.setData(d3d->context.Get(), perFrameSource);
}

void nyx::BoidResources::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void nyx::BoidResources::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  createDeviceResources();
}

nyx::BoidRenderer::BoidRenderer(dx::DxDeviceResources *device)
  : d3d(device)
{
  assert(d3d != 0);
  d3d->deviceLost += _Dx_delegate_to(this, &BoidRenderer::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &BoidRenderer::onDeviceRestored);
  onDeviceRestored(d3d);
}

void nyx::BoidRenderer::createDeviceResources()
{
  assert(d3d != 0);

  dx::SamplerStateDescription samplerDesc;
  dx::RasterizerDescription rasterizerStateDesc;
  dx::BlendStateDescription blendStateDescription;
  dx::DepthStencilDescription depthDisabledStencilDesc;

  dx::DxResourceManager::defaults(samplerDesc);
  samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

  dx::zeroMemory(blendStateDescription);
  blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
  blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
  blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
  blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
  blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
  blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
  blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
  blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;

  dx::zeroMemory(rasterizerStateDesc);
  rasterizerStateDesc.AntialiasedLineEnable = false;
  rasterizerStateDesc.CullMode = D3D11_CULL_BACK;
  rasterizerStateDesc.DepthBias = 0;
  rasterizerStateDesc.DepthBiasClamp = 0.0f;
  rasterizerStateDesc.DepthClipEnable = true;
  rasterizerStateDesc.FillMode = D3D11_FILL_SOLID;
  rasterizerStateDesc.FrontCounterClockwise = false;
  rasterizerStateDesc.MultisampleEnable = false;
  rasterizerStateDesc.ScissorEnable = false;
  rasterizerStateDesc.SlopeScaledDepthBias = 0.0f;

  dx::zeroMemory(depthDisabledStencilDesc);
  dx::DxResourceManager::defaults(depthDisabledStencilDesc);
  depthDisabledStencilDesc.DepthEnable = false;
  depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
  depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
  depthDisabledStencilDesc.StencilEnable = true;
  depthDisabledStencilDesc.StencilReadMask = 0xff;
  depthDisabledStencilDesc.StencilWriteMask = 0xff;
  depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
  depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
  depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
  depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
  depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
  depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
  depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
  depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

  dx::HResult result = S_OK;
  dx::DxAssetManager::addSearchPath("Fishes");

  result = dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"shaders/BoidPixelShader.cso",
    pixelShader.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = dx::DxResourceManager::loadShader(
    d3d->device.Get(),
    L"shaders/BoidVertexShader.cso",
    (dx::VSInputElementDescription*)BoidResources::layoutElements,
    BoidResources::layoutElementCount,
    vertexShader.ReleaseAndGetAddressOf(),
    inputLayout.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device->CreateSamplerState(
    &samplerDesc, samplerState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device.Get()->CreateDepthStencilState(
    &depthDisabledStencilDesc,
    depthStencilState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device.Get()->CreateRasterizerState1(
    &rasterizerStateDesc,
    rasterizerState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);
  result = d3d->device.Get()->CreateBlendState1(
    &blendStateDescription,
    blendState.ReleaseAndGetAddressOf()
    ); _Dx_hresult_assert(result);

}

void nyx::BoidRenderer::render(CameraResources const &camera, BoidResources const &boid) const
{
  static const auto mask = 0xffffffffu;
  static const FLOAT factor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
  static const UINT32 offsets[] = { 0u };
  static const UINT32 strides[] = { sizeof BoidResources::Vertex };
  static const UINT32 stencilRef = 0x01u;
  static dx::ISRView *nullSRV[1] = { 0 };

  assert(d3d != 0);
  auto device = d3d->device.Get();
  auto context = d3d->context.Get();

  context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  context->IASetVertexBuffers(0u, 1u, boid.vertexBuffer.GetAddressOf(), strides, offsets);
  context->IASetIndexBuffer(boid.indexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0u);
  context->IASetInputLayout(inputLayout.Get());

  context->VSSetShader(vertexShader.Get(), nullptr, 0u);
  context->PSSetShader(pixelShader.Get(), nullptr, 0u);

  context->VSSetConstantBuffers(0u, 1u, boid.perFrameResource.getBufferAddress());
  context->VSSetConstantBuffers(1u, 1u, camera.perFrameResource.getBufferAddress());
  context->PSSetSamplers(0u, 1u, samplerState.GetAddressOf());
  context->PSSetShaderResources(0u, 1u, boid.textureView.GetAddressOf());
  context->OMSetDepthStencilState(depthStencilState.Get(), stencilRef);
  context->OMSetBlendState(blendState.Get(), factor, mask);
  context->RSSetState(rasterizerState.Get());

  context->DrawIndexed(6u, 0u, 0u);

  context->PSSetShaderResources(0u, 1u, nullSRV);
  context->PSSetShader(nullptr, nullptr, 0u);
  context->VSSetShader(nullptr, nullptr, 0u);

}

void nyx::BoidRenderer::renderTail(CameraResources const &camera, BoidResources &boid, Boid const &boidData) const
{

  static const auto mask = 0xffffffffu;
  static const FLOAT factor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
  static const UINT32 offsets[] = { 0u };
  static const UINT32 strides[] = { sizeof BoidResources::Vertex };
  static const UINT32 stencilRef = 0x01u;
  static dx::ISRView *nullSRV[1] = { 0 };

  assert(d3d != 0);
  auto device = d3d->device.Get();
  auto context = d3d->context.Get();

  context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  context->IASetVertexBuffers(0u, 1u, boid.vertexBuffer.GetAddressOf(), strides, offsets);
  context->IASetIndexBuffer(boid.indexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0u);
  context->IASetInputLayout(inputLayout.Get());

  context->VSSetShader(vertexShader.Get(), nullptr, 0u);
  context->PSSetShader(pixelShader.Get(), nullptr, 0u);

  dx::Color tailSegColor = boidData.color;
  dx::Vector2 tailSegSize = boidData.size;
  dx::Vector2 tailSegSizeDelta = boidData.size / (float)boidData.tail.size();
  float tailSegRDelta = tailSegColor.R() / (float)boidData.tail.size() * 0.90f;
  float tailSegGDelta = tailSegColor.G() / (float)boidData.tail.size() * 0.90f;
  float tailSegBDelta = tailSegColor.B() / (float)boidData.tail.size() * 0.90f;
  float tailSegADelta = tailSegColor.A() / (float)boidData.tail.size() * 1.10f;

  for (unsigned tailInd = 0; tailInd < boidData.tail.size(); tailInd++)
  {
    tailSegSize -= tailSegSizeDelta;
    tailSegColor.R(tailSegColor.R() - tailSegRDelta);
    tailSegColor.G(tailSegColor.G() - tailSegGDelta);
    tailSegColor.B(tailSegColor.B() - tailSegBDelta);
    tailSegColor.A(tailSegColor.A() - tailSegADelta);
    boid.update(boidData.tail[tailInd], tailSegSize, tailSegColor);

    context->VSSetConstantBuffers(0u, 1u, boid.perFrameResource.getBufferAddress());
    context->VSSetConstantBuffers(1u, 1u, camera.perFrameResource.getBufferAddress());
    context->PSSetSamplers(0u, 1u, samplerState.GetAddressOf());
    context->PSSetShaderResources(0u, 1u, boid.textureView.GetAddressOf());
    context->OMSetDepthStencilState(depthStencilState.Get(), stencilRef);
    context->OMSetBlendState(blendState.Get(), factor, mask);
    context->RSSetState(rasterizerState.Get());
    context->DrawIndexed(6u, 0u, 0u);
  }

  context->PSSetShaderResources(0u, 1u, nullSRV);
  context->PSSetShader(nullptr, nullptr, 0u);
  context->VSSetShader(nullptr, nullptr, 0u);
}

void nyx::BoidRenderer::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void nyx::BoidRenderer::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  createDeviceResources();
}
