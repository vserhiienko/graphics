#include "Fish.h"
#include <DxUtils.h>
#include <DirectXColors.h>

//dx::VSInputElementDescription const nyx::FishResources::layoutElements[3] =
//{
//  { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//  { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 16
//  { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 28, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 12 (28)
//};
//
//unsigned const nyx::FishResources::layoutElementCount =
//arraysize(nyx::FishResources::layoutElements);

nyx::Fish::Fish(dx::Vector2 location, float maxVelocity, float maxForce, int tailLength)
  : Boid(location, maxVelocity, maxForce, tailLength)
{
  food = 0;
  finSegCount = 9;
  tailSegCount = 10;
  bodySegCount = 10;

  bodySize.x = bodySegCount * 30.0f;
  bodySize.y = bodySize.x * 0.3f;
  tailSize.x = bodySize.x * 0.8f;
  tailSize.y = bodySize.y * 0.8f;
  finSize = tailSize * 0.6f;
  origBodySize = bodySize;

  finL.reset(new Flagellum(finSize.x, finSize.y, finSegCount));
  finR.reset(new Flagellum(finSize.x, finSize.y, finSegCount));
  body.reset(new Flagellum(bodySize.x, bodySize.y, bodySegCount));
  tailL.reset(new Flagellum(tailSize.x, tailSize.y, tailSegCount));
  tailR.reset(new Flagellum(tailSize.x * 0.8f, tailSize.y * 0.8f, tailSegCount));

  body->spineColor = (dx::XMVECTOR)DirectX::Colors::LightBlue;
  finL->spineColor = (dx::XMVECTOR)DirectX::Colors::LightSteelBlue;
  finR->spineColor = (dx::XMVECTOR)DirectX::Colors::LightSteelBlue;
  tailL->spineColor = (dx::XMVECTOR)DirectX::Colors::LightSteelBlue;
  tailR->spineColor = (dx::XMVECTOR)DirectX::Colors::LightSteelBlue;

}

void nyx::Fish::eat()
{
  body->incSize();
  finL->incSize();
  finR->incSize();
  tailL->incSize();
  tailL->incSize();
  tailR->incSize();
  tailR->incSize();
  food++;
}

void nyx::Fish::update()
{
  Boid::update();

  float angleTail = body->backHeading();
  float angleFin = body->frontHeading();

  float velNorm = vel.LengthEst();
  body->muscleFreq = velNorm * 0.05f;
  tailR->muscleFreq = velNorm * 0.08f;
  tailL->muscleFreq = velNorm * 0.08f;
  finR->muscleFreq = velNorm * 0.04f;
  finL->muscleFreq = velNorm * 0.04f;

  body->theta = vel.Heading();
  finL->theta = angleFin + (dx::XM_PI * 0.65f);
  finR->theta = angleFin + (dx::XM_PI * 1.35f);
  tailR->theta = angleTail + (dx::XM_PI * 0.90f);
  tailL->theta = angleTail + (dx::XM_PI * 1.10f);

  body->swim();
  tailR->swim();
  tailL->swim();
  finR->swim();
  finL->swim();

  int finSpineInd = bodySegCount / 3;
  body->pose = dx::Matrix::CreateTranslation(loc.x, loc.y, 0.0f);
  finL->pose = dx::Matrix::CreateTranslation(loc.x + body->spines[finSpineInd].x, loc.y + body->spines[finSpineInd].y, 0.0f);
  finR->pose = dx::Matrix::CreateTranslation(loc.x + body->spines[finSpineInd].x, loc.y + body->spines[finSpineInd].y, 0.0f);
  tailL->pose = dx::Matrix::CreateTranslation(loc.x + body->spines.back().x, loc.y + body->spines.back().y, 0.0f);
  tailR->pose = dx::Matrix::CreateTranslation(loc.x + body->spines.back().x, loc.y + body->spines.back().y, 0.0f);

}

nyx::FishRenderer::FishRenderer(dx::DxDeviceResources *device)
  : d3d(device)
  , boidRenderer(device)
  , boidResources(device)
  , flagellumRenderer(device)
{
  assert(d3d != 0);
  d3d->deviceLost += _Dx_delegate_to(this, &FishRenderer::onDeviceLost);
  d3d->deviceRestored += _Dx_delegate_to(this, &FishRenderer::onDeviceRestored);
  onDeviceRestored(d3d);
}

void nyx::FishRenderer::createDeviceResources()
{
  assert(d3d != 0);

}

void nyx::FishRenderer::render(CameraResources const &camera, Fish const &fish)
{
  assert(d3d != 0);

  flagellumRenderer.render(camera, (*fish.finL.get()));
  flagellumRenderer.render(camera, (*fish.finR.get()));
  flagellumRenderer.render(camera, (*fish.tailL.get()));
  flagellumRenderer.render(camera, (*fish.tailR.get()));
  flagellumRenderer.render(camera, (*fish.body.get()));
}

void nyx::FishRenderer::onDeviceLost(dx::DxDeviceResources*)
{
  d3d = 0;
}

void nyx::FishRenderer::onDeviceRestored(dx::DxDeviceResources *restoredDevice)
{
  d3d = restoredDevice;
  createDeviceResources();
}
