#pragma once
#include <DxUtils.h>
#include <DxTimer.h>
//#include <DxUIClient.h>
//#include <DxUISampleApp.h>
#include <DxSampleApp.h>

#include "Boid.h"
#include "Billboard.h"
#include "Fish.h"

namespace nyx
{
  class FishesSampleApp 
    : public dx::DxSampleApp
    , public dx::AlignedNew<FishesSampleApp>
  {
    // sample timer
    dx::sim::BasicTimer timer;

    nyx::Boid boid;
    nyx::Fish fish;
    nyx::Camera camera;

    nyx::BoidResources boidResources;
    nyx::CameraResources cameraResources;
    nyx::BillboardResources billboardResources;

    nyx::BoidRenderer boidRenderer;
    nyx::FishRenderer fishRenderer;
    nyx::BillboardRenderer billboardRenderer;

    bool mouseMoved;
    bool mousePressed;
    dx::Vector2 mousePosition;

  public:
    std::vector<nyx::Boid*> boids;
    std::vector<nyx::Boid*> target;
    std::vector<nyx::Fish*> fishes;

  public:
    FishesSampleApp();
    virtual ~FishesSampleApp();

  public: // DxSampleApp
    virtual void handlePaint(dx::DxWindow const*);

  public: // DxUISampleApp
    virtual std::string getUIURL(void);
	virtual bool onAppLoop(void);

    virtual void sizeChanged(void);
    virtual void onUICreated(void);
    virtual void onUIDestroyed(void);

    virtual void keyReleased(dx::DxGenericEventArgs const &args);
    virtual void pointerMoved(dx::DxGenericEventArgs const &args);
    virtual void pointerLeftPressed(dx::DxGenericEventArgs const &args);
    virtual void pointerLeftReleased(dx::DxGenericEventArgs const &args);

  };
}