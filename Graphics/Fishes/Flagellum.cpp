#include "Flagellum.h"
#include <DirectXColors.h>

nyx::Flagellum::Flagellum(
  float sizeX,
  float sizeY,
  size_t nodeCount
  )
{
  count = 0.f;
  thetaVel = 0.f;
  theta = dx::XM_PI;
  spineSize = (dx::Vector2)10.f;
  spines.resize(nodeCount);
  spineSizes.resize(nodeCount);
  spineColor = (dx::XMVECTOR)DirectX::Colors::FloralWhite;
  muscleFreq = 0.08f;
  muscleRange = 0.15f;
  pose = dx::Matrix::Identity();

  setSize(sizeX, sizeY);
}

void nyx::Flagellum::incSize()
{
  //setSize(size.x + 1, size.y + 1);
}

void nyx::Flagellum::setSize(float sizeX, float sizeY)
{
  spines.resize(spines.size() + 1);
  spineSizes.resize(spines.size() + 1);

  size.x = sizeX;
  size.y = sizeY;
  space.x = size.x / float(spines.size() + 1);
  space.y = size.y / 2.0f;


  float p0, m0, p1, m1;

  p0 = 0;
  m0 = size.y;
  p1 = 0;
  m1 = -size.y;

  float scale = 10;
  for (unsigned n = 0; n < spines.size(); n++)
  {
    float v = solveHermiteSegment(p0, m0, p1, m1, 1.0f - float(n) / spines.size());
    spineSizes[n].x = v * scale;
    spineSizes[n].y = v * scale;
  }
}

float nyx::Flagellum::frontHeading() const
{
  return (spines[1] - spines[0]).Heading();
}

float nyx::Flagellum::heading(int at) const
{
  if (at == 0 || at == 1) return frontHeading();
  else if (at < spines.size()) (spines[at] - spines[at - 1]).Heading();
  return 0.0f;
}

float nyx::Flagellum::backHeading() const
{
  size_t lastInd = spines.size() - 1;
  return (spines[lastInd] - spines[lastInd - 1]).Heading();
}

void nyx::Flagellum::swim()
{
  spines[0].x = cos(theta);
  spines[0].y = sin(theta);

  count += muscleFreq;
  float thetaMuscle = muscleRange * sin(count);
  float currentTheta = theta + thetaMuscle;

  auto circularDirection = dx::Vector2(
    cos(currentTheta), sin(currentTheta)
    );

  spines[1] = -space * circularDirection + spines[0];

  for (unsigned n = 2; n < spines.size(); n++) {

    dx::Vector2 s = spines[n] - spines[n - 2];
    float l = s.LengthEst();

    if (l > 0)
    {
      spines[n] = spines[n - 1] + s * (space.x / l);
    }
  }
}


nyx::FlagellumRenderer::FlagellumRenderer(dx::DxDeviceResources *device)
  : boidRenderer(device)
  , boidResources(device)
{
}

void nyx::FlagellumRenderer::render(CameraResources const &camera, Flagellum const &flagellum)
{
  unsigned spineCount = flagellum.spines.size();
  dx::Vector2 const *spines = flagellum.spines.data();
  for (unsigned spineInd = 0; spineInd < spineCount; spineInd++)
  {
    boidResources.update(spines[spineInd], flagellum.spineSizes[spineInd], flagellum.spineColor, flagellum.pose);
    boidRenderer.render(camera, boidResources);
  }
}
