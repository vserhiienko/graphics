
struct PsInput
{
  float4 Position : SV_POSITION;
  float3 Normal : NORMAL;
  float2 Coords : TEXCOORD0;
  float4 Color : COLOR;
};

Texture2D<float4> ColorTexture : register (t0);
SamplerState ColorSampler : register (s0);

float4 main(in PsInput input) : SV_TARGET
{
  float4 pelColor = ColorTexture.Sample(ColorSampler, input.Coords);
  float4 outputColor = saturate(pelColor * input.Color);
  outputColor.a *= 1.25f;
  return outputColor;
}
