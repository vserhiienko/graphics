#pragma once
#include "Boid.h"

namespace nyx
{
  class Flagellum
  {
  public:
    dx::Vector2 size;
    dx::Vector2 space;
    float theta, thetaVel, count;
    float muscleFreq, muscleRange;

    dx::Color spineColor;
    dx::Vector2 spineSize;
    std::vector<dx::Vector2> spines;
    std::vector<dx::Vector2> spineSizes;

    dx::Matrix pose;

    void setSize(float, float);

  public:
    Flagellum(float sizeX, float sizeY, size_t nodeCount = 10);
    void swim(void);
    void incSize(void);

    float frontHeading(void) const;
    float backHeading(void) const;
    float heading(int at) const;

  };

  class FlagellumRenderer
  {
  public:
    nyx::BoidRenderer boidRenderer;
    nyx::BoidResources boidResources;

  public:
    FlagellumRenderer(dx::DxDeviceResources*);
    void render(nyx::CameraResources const &, nyx::Flagellum const &);

  };

}