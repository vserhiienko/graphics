#include "SampleApp.h"

#include <DxUtils.h>

//#define SampleApp_StartupURL "file:///D:/Dev/Vs/Proj/Graphics/Graphics/UISampleApp/assets/pages/UIExample/index.html"

//#define SampleApp_StartupURL "https://support.google.com/websearch/answer/186645?form=bb&hl=en-UA"
#define SampleApp_StartupURL "file:///D:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/PageTransitions/PageTransitions/index.html"
//#define SampleApp_StartupURL "file:///D:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/TexturedText/TexturedText/index2.html"
//#define SampleApp_StartupURL "file:///D:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/AnimatedHeaderBackgrounds/AnimatedHeaderBackgrounds/index.html"
//#define SampleApp_StartupURL "file:///D:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/MultiLevelPushMenu/MultiLevelPushMenu/index3.html#"
//#define SampleApp_StartupURL "file:///D:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/InteractiveParticlesSlideshow/InteractiveParticlesSlideshow/index.html"
//#define SampleApp_StartupURL "file:///D:/Dev/Vs/Proj/Graphics/Graphics/ChromiumSenzInteractionSampleApp/assets/pages/OffCanvasMenuEffects/OffCanvasMenuEffects/elastic.html"
//#define SampleApp_StartupURL "file:///D:/Dev/Vs/Deps/Snap.svg-master/Snap.svg-master/demos/snap-ad/site/index.html"
//#define SampleApp_StartupURL "file:///D:/Dev/Vs/Deps/Snap.svg-master/Snap.svg-master/demos/snap-ad/site/index.html"
//#define SampleApp_StartupURL "http://localhost:63342/UniversityTVUI/index.html"

dx::DxSampleApp *dx::DxSampleAppFactory()
{
  return new nyx::SampleApp();
}

nyx::SampleApp::SampleApp()
//: DxUISampleApp(true, false)
{
}

nyx::SampleApp::~SampleApp()
{
}

std::string nyx::SampleApp::getUIURL()
{
  return SampleApp_StartupURL;
}

void nyx::SampleApp::initializeSample()
{
  dx::trace("nyx:app: initialize sample");
  timer.reset();
}

void nyx::SampleApp::sizeChanged()
{

}

void nyx::SampleApp::onUICreated()
{
}

void nyx::SampleApp::onUIDestroyed()
{

}

void nyx::SampleApp::keyReleased(dx::DxGenericEventArgs const &args)
{
  switch (args.keyCode())
  {
  case dx::VirtualKey::Escape: window.close(); break;
  default: { } break;
  }
}

void nyx::SampleApp::handlePaint(dx::DxWindow const*)
{
  /// update

  timer.update();
  // update scene
  // ...
  // update ui
  dxui::DxUIClient::updateApplication();
  updateUI(timer.elapsed);

  /// draw

  beginScene();
  // render scene
  // ...
  // render ui
  renderUI();
  endScene();
}
