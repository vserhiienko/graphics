#include "VolumeFluid.h"

uint32_t Demo::VolumeFluid::s_nullUAVCounts[s_maxSlotsToUnbind] = { 0u };
dx::ISRView *Demo::VolumeFluid::s_nullSRVs[s_maxSlotsToUnbind] = { nullptr };
dx::IUAView *Demo::VolumeFluid::s_nullUAVs[s_maxSlotsToUnbind] = { nullptr };

#define _shaderArgs nullptr, 0u
#define _unbind1x1 UnbindViews(1, 1)
#define _unbind2x1 UnbindViews(2, 1)
#define _unbind3x1 UnbindViews(3, 1)
#define _unbind _unbind3x1

#define _d3ddvc D3DResources->device.Get()
#define _d3dctx D3DResources->context.Get()

#define _speed0texs m_speedTxs[0].resource.GetAddressOf()
#define _speed1texs m_speedTxs[1].resource.GetAddressOf()
#define _speed2texs m_speedTxs[2].resource.GetAddressOf()

#define _speed0srvs m_speedTxs[0].readView.GetAddressOf()
#define _speed1srvs m_speedTxs[1].readView.GetAddressOf()
#define _speed2srvs m_speedTxs[2].readView.GetAddressOf()

#define _speed0uavs m_speedTxs[0].writeView.GetAddressOf(), s_nullUAVCounts
#define _speed1uavs m_speedTxs[1].writeView.GetAddressOf(), s_nullUAVCounts
#define _speed2uavs m_speedTxs[2].writeView.GetAddressOf(), s_nullUAVCounts

#define _divsrvs m_divergenceTx.readView.GetAddressOf()
#define _divuavs m_divergenceTx.writeView.GetAddressOf(), s_nullUAVCounts

#define _pressure0srvs m_pressureTxs[0].readView.GetAddressOf()
#define _pressure1srvs m_pressureTxs[1].readView.GetAddressOf()

#define _pressure0uavs m_pressureTxs[0].writeView.GetAddressOf(), s_nullUAVCounts
#define _pressure1uavs m_pressureTxs[1].writeView.GetAddressOf(), s_nullUAVCounts

#define _renderOutput m_targetUAV.GetAddressOf(), s_nullUAVCounts
#define _volumeThreadGroups m_actualVolumeSize.x, m_actualVolumeSize.y, m_actualVolumeSize.z
#define _renderThreadGroups m_actualRenderSize.x, m_actualRenderSize.y, m_actualRenderSize.z

void Demo::VolumeFluid::SwapSpeeds()
{
	using namespace dx;

	m_speedTxs[1].readView.Swap(m_speedTxs[0].readView);
	m_speedTxs[1].writeView.Swap(m_speedTxs[0].writeView);
}

void Demo::VolumeFluid::UnbindViews(
	uint32_t slotsSRV,
	uint32_t slotsUAV
	)
{
	_d3dctx->CSSetShaderResources(0, slotsSRV, s_nullSRVs);
	_d3dctx->CSSetUnorderedAccessViews(0, slotsUAV, s_nullUAVs, s_nullUAVCounts);
}

void Demo::VolumeFluid::Update()
{
	dx::HResult result;
	m_perFrameData.EyeTransform = Viewer->View;
	result = m_perFrameBff.setData(_d3dctx, m_perFrameData);

	_d3dctx->CSSetConstantBuffers(
		0, 1, m_perFrameBff.getBufferAddress()
		);
	_d3dctx->CSSetSamplers(
		0, 1, m_linearSampler.GetAddressOf()
		);

	Update_Advect(); // -> speed 1 advected
	Update_ApplyForces(); // -> speed 0 advected
	Update_Divergence(); // -> div
	Update_Pressure(); // -> pressure 0
	Update_Project(); // -> speed 1

}

void Demo::VolumeFluid::Render()
{
	Render_Volume();
	SwapSpeeds();
}

void Demo::VolumeFluid::Update_Advect() // => speed 1 advected
{
#if 1 // stable scheme

	_d3dctx->CSSetShader(m_advectCS.Get(), _shaderArgs);
	_d3dctx->CSSetShaderResources(0, 1, _speed0srvs); // reads 0 speed
	_d3dctx->CSSetUnorderedAccessViews(0, 1, _speed1uavs); // writes 1 advected
	_d3dctx->Dispatch(_volumeThreadGroups); _unbind1x1;

#else // MacCormack

  _d3dctx->CSSetShader(m_advectCS.Get(), _shaderArgs);
  _d3dctx->CSSetShaderResources(0, 1, _speed0srvs); // reads 0 speed
  _d3dctx->CSSetUnorderedAccessViews(0, 1, _speed1uavs); // writes 1 advected
  _d3dctx->Dispatch(_volumeThreadGroups); _unbind1x1;

	_d3dctx->CSSetShader(m_advectRCS.Get(), _shaderArgs);
	_d3dctx->CSSetShaderResources(0, 1, _speed0srvs); // reads 0 speed
	_d3dctx->CSSetShaderResources(1, 1, _speed1srvs); // reads 1 advected
	_d3dctx->CSSetUnorderedAccessViews(0, 1, _speed2uavs); // writes 2 advectedR
	_d3dctx->Dispatch(_volumeThreadGroups); _unbind2x1;

	_d3dctx->CSSetShader(m_advectMCS.Get(), _shaderArgs);
	_d3dctx->CSSetShaderResources(0, 1, _speed0srvs); // reads 0 speed
	_d3dctx->CSSetShaderResources(1, 1, _speed2srvs); // reads 2 advectedR
	_d3dctx->CSSetUnorderedAccessViews(0, 1, _speed1uavs); // writes 1 advectedF
	_d3dctx->Dispatch(_volumeThreadGroups); _unbind2x1;

#endif

}

void Demo::VolumeFluid::Update_ApplyForces() // => speed 0
{
	_d3dctx->CSSetShader(m_applyForcesCS.Get(), _shaderArgs);
	_d3dctx->CSSetShaderResources(0, 1, _speed1srvs);
	_d3dctx->CSSetUnorderedAccessViews(0, 1, _speed0uavs);
	_d3dctx->Dispatch(_volumeThreadGroups); _unbind;
}

void Demo::VolumeFluid::Update_Divergence()
{
	_d3dctx->CSSetShader(m_divergenceCS.Get(), _shaderArgs);
	_d3dctx->CSSetShaderResources(0, 1, _speed0srvs);
	_d3dctx->CSSetUnorderedAccessViews(0, 1, _divuavs);
	_d3dctx->Dispatch(_volumeThreadGroups); _unbind1x1;
}

void Demo::VolumeFluid::Update_Pressure()
{
	_d3dctx->CSSetShader(m_pressureCS.Get(), _shaderArgs);

	// unrolled 20=10x2 iterations

	Update_PressureIteration(); // 0 => pressure 1
  //Update_PressureIteration(); // 1 => pressure 1
  //Update_PressureIteration(); // 2 => pressure 1
  //Update_PressureIteration(); // 3 => pressure 1
  //Update_PressureIteration(); // 4 => pressure 1
	//Update_PressureIteration(); // 5 => pressure 1
	//Update_PressureIteration(); // 6 => pressure 1
	//Update_PressureIteration(); // 7 => pressure 1
	//Update_PressureIteration(); // 8 => pressure 1
	//Update_PressureIteration(); // 9 => pressure 1
	
}

void Demo::VolumeFluid::Update_PressureIteration() // => pressure 1
{
	_d3dctx->CSSetShaderResources(0, 1, _divsrvs);
	_d3dctx->CSSetShaderResources(1, 1, _pressure1srvs);
	_d3dctx->CSSetUnorderedAccessViews(0, 1, _pressure0uavs);
	_d3dctx->Dispatch(_volumeThreadGroups); _unbind2x1;

	_d3dctx->CSSetShaderResources(0, 1, _divsrvs);
	_d3dctx->CSSetShaderResources(1, 1, _pressure0srvs);
	_d3dctx->CSSetUnorderedAccessViews(0, 1, _pressure1uavs);
	_d3dctx->Dispatch(_volumeThreadGroups); _unbind2x1;
}

void Demo::VolumeFluid::Update_Project()
{
	_d3dctx->CSSetShader(m_projectCS.Get(), _shaderArgs);
	_d3dctx->CSSetShaderResources(0, 1, _speed0srvs);
	_d3dctx->CSSetShaderResources(1, 1, _pressure1srvs);
	_d3dctx->CSSetUnorderedAccessViews(0, 1, _speed1uavs);
	_d3dctx->Dispatch(_volumeThreadGroups); _unbind2x1;
}

void Demo::VolumeFluid::Render_Volume()
{
#if _VolumeFluids_Draw_to_texture

  static const auto mask = 0xffffffffu;
  static const FLOAT factor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
  static const UINT32 offsets[] = { 0u };
  static const UINT32 strides[] = { sizeof Vertex };
  static const UINT32 stencil_ref = 0x01u;
  static dx::ISRView *nullSRV[1] = { 0 };

#endif

	_d3dctx->CSSetShader(m_renderCS.Get(), _shaderArgs);
	_d3dctx->CSSetShaderResources(0, 1, _speed1srvs);
	_d3dctx->CSSetShaderResources(1, 1, _pressure1srvs);
	_d3dctx->CSSetShaderResources(2, 1, _divsrvs);
	_d3dctx->CSSetUnorderedAccessViews(0, 1, _renderOutput);
	_d3dctx->Dispatch(_renderThreadGroups); _unbind3x1;

#if _VolumeFluids_Draw_to_texture

  _d3dctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  _d3dctx->IASetVertexBuffers(0u, 1u, vertexBuffer.GetAddressOf(), strides, offsets);
  _d3dctx->IASetIndexBuffer(indexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0u);
  _d3dctx->IASetInputLayout(m_inputLayout.Get());

  _d3dctx->VSSetShader(m_renderVS.Get(), nullptr, 0u);
  _d3dctx->PSSetShader(m_renderPS.Get(), nullptr, 0u);

  _d3dctx->VSSetConstantBuffers(0u, 1u, m_perFrameVSBff.getBufferAddress());
  _d3dctx->PSSetSamplers(0u, 1u, m_linearSampler.GetAddressOf());
  _d3dctx->PSSetShaderResources(0u, 1u, m_targetSRV.GetAddressOf());

  _d3dctx->DrawIndexed(6u, 0u, 0u);

  _d3dctx->PSSetShaderResources(0u, 1u, nullSRV);
  _d3dctx->PSSetShader(nullptr, nullptr, 0u);
  _d3dctx->VSSetShader(nullptr, nullptr, 0u);

#endif

}

dx::HResult Demo::VolumeFluid::CreateDeviceIndependentResources() { return S_OK; }

Demo::VolumeFluid::VolumeFluid(dx::DxDeviceResources *D3DResources)
  : D3DResources(D3DResources)
{
  ZeroMemory(&m_perFrameData, sizeof PerFrameCS);
  ZeroMemory(&m_perFrameVSData, sizeof PerFrameCS);

	m_mbLPressed = FALSE;
	m_mbRPressed = FALSE;
	//m_mbLPressed = TRUE;
	//m_mbRPressed = TRUE;

	m_perFrameData.FocusZoom.x = 1.0f;

  DirectX::XMVECTOR upDirection = DirectX::XMVectorSet(0.f, 1.f, 0.f, 1.f);
  DirectX::XMVECTOR eyePosition = DirectX::XMVectorSet(0.f, 0.f, -1.f, 1.f);
  DirectX::XMVECTOR focusPosition = DirectX::XMVectorSet(0.f, 0.f, +1.f, 1.f);

  m_perFrameVSData.world = DirectX::XMMatrixIdentity();
  m_perFrameVSData.cameraView = DirectX::XMMatrixLookAtLH(eyePosition, focusPosition, upDirection);

	SetImpactRadius(2);
}

dx::HResult Demo::VolumeFluid::CreateWindowSizeDependentResources()
{
	dx::HResult result;

#if _VolumeFluids_Draw_to_texture

  dx::CTexture2Description renderOutputTxDesc(
    DXGI_FORMAT_R16G16B16A16_FLOAT,
    (uint32_t)D3DResources->actualRenderTargetSize.Width,
    (uint32_t)D3DResources->actualRenderTargetSize.Height, 1, 0,
    D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS
    );

  result = _d3ddvc->CreateTexture2D(
    &renderOutputTxDesc, 0, 
    m_renderOutput.ReleaseAndGetAddressOf()
    );

  dx::CSRVDescription renderOutputSRVDesc(
    m_renderOutput.Get(),
    D3D11_SRV_DIMENSION_TEXTURE2D,
    renderOutputTxDesc.Format
    );

  result = _d3ddvc->CreateShaderResourceView(
    m_renderOutput.Get(),
    &renderOutputSRVDesc,
    m_targetSRV.ReleaseAndGetAddressOf()
    );

  dx::CUAVDescription renderOutputUAVDesc(
    m_renderOutput.Get(),
    D3D11_UAV_DIMENSION_TEXTURE2D,
    renderOutputTxDesc.Format
    );

  result = _d3ddvc->CreateUnorderedAccessView(
    m_renderOutput.Get(), 
    &renderOutputUAVDesc, 
    m_targetUAV.ReleaseAndGetAddressOf()
    );

	m_perFrameData.ViewportDims.x = D3DResources->actualRenderTargetSize.Width;
  m_perFrameData.ViewportDims.y = D3DResources->actualRenderTargetSize.Height;
  result = m_perFrameBff.setData(_d3dctx, m_perFrameData);

  float farPlane = 100.0f, nearPlane = 0.01f;
  float screenW = m_perFrameData.ViewportDims.x, screenH = m_perFrameData.ViewportDims.y;
  m_perFrameVSData.cameraProjection = DirectX::XMMatrixOrthographicLH(screenW, screenH, nearPlane, farPlane);

  m_perFrameVSBff.create(_d3ddvc, "m_perFrameVSBff");
  m_perFrameVSBff.setData(_d3dctx, m_perFrameVSData);

  float
    w = (float)m_perFrameData.ViewportDims.x * 0.45f,
    h = (float)m_perFrameData.ViewportDims.y * 0.45f;

  Vertex squareVertices[4] =
  {
    { DirectX::Vector4(-w, +h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(0, 0) },
    { DirectX::Vector4(+w, +h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(1, 0) },
    { DirectX::Vector4(+w, -h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(1, 1) },
    { DirectX::Vector4(-w, -h, 0, 1), DirectX::Vector3(0, 0, -1), DirectX::Vector2(0, 1) },
  };
  Index squareIndices[6] =
  {
    0, 1, 2, 0, 2, 3,
  };

  result = dx::DxResourceManager::createVertexBuffer(
    D3DResources->device.Get(), ARRAYSIZE(squareVertices), squareVertices,
    vertexBuffer.ReleaseAndGetAddressOf()
    );
  result = dx::DxResourceManager::createIndexBuffer(
    D3DResources->device.Get(), ARRAYSIZE(squareIndices), squareIndices,
    indexBuffer.ReleaseAndGetAddressOf()
    );

#else

  dx::ScopedObject<dx::ITexture2> targetTx;
  result = D3DResources->swapchain->GetBuffer(0, IID_PPV_ARGS(&targetTx));
  result = _d3ddvc->CreateUnorderedAccessView(targetTx.Get(), nullptr, m_targetUAV.ReleaseAndGetAddressOf());

  m_perFrameData.ViewportDims.x = D3DResources->actualRenderTargetSize.Width;
  m_perFrameData.ViewportDims.y = D3DResources->actualRenderTargetSize.Height;
  result = m_perFrameBff.setData(_d3dctx, m_perFrameData);

#endif

	return result;
}

dx::HResult Demo::VolumeFluid::CreateDeviceDependentResources()
{
	dx::HResult result;

	result = CreateTextures();
	result = CreateBuffers();
	result = CreateShaders();

	return result;
}

dx::HResult Demo::VolumeFluid::CreateShaders()
{

  dx::VSInputElementDescription layout_element[] =
  {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 16
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 28, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 12 (28)
  };

	dx::HResult result;

  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/Advect3CS.cso", m_advectCS.ReleaseAndGetAddressOf());
  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/AdvectR3CS.cso", m_advectRCS.ReleaseAndGetAddressOf());
  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/AdvectMC3CS.cso", m_advectMCS.ReleaseAndGetAddressOf());
  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/ApplyForces3CS.cso", m_applyForcesCS.ReleaseAndGetAddressOf());
  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/Divergence3CS.cso", m_divergenceCS.ReleaseAndGetAddressOf());
  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/Pressure3CS.cso", m_pressureCS.ReleaseAndGetAddressOf());
  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/Project3CS.cso", m_projectCS.ReleaseAndGetAddressOf());
  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/RenderVolumeCS.cso", m_renderCS.ReleaseAndGetAddressOf());
  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/RenderVolumePS.cso", m_renderPS.ReleaseAndGetAddressOf());
  result = dx::DxResourceManager::loadShader(_d3ddvc, L"shaders/RenderVolumeVS.cso", layout_element, ARRAYSIZE(layout_element), m_renderVS.ReleaseAndGetAddressOf(), m_inputLayout.ReleaseAndGetAddressOf());

	return result;
}

dx::HResult Demo::VolumeFluid::CreateBuffers()
{
	dx::HResult result;

	result = m_perFrameBff.create(_d3ddvc);
	result = m_perFrameBff.setData(_d3dctx, m_perFrameData);

	return result;
}

void Demo::VolumeFluid::SetVolumeSize(INT32x3 vs)
{
	m_perFrameData.ViewportDims.x = D3DResources->actualRenderTargetSize.Width;
  m_perFrameData.ViewportDims.y = D3DResources->actualRenderTargetSize.Height;
	m_perFrameData.VolumeDims.x = (float) (vs.x - vs.x % s_renderThreadsX);
	m_perFrameData.VolumeDims.y = (float) (vs.y - vs.y % s_renderThreadsY);
	m_perFrameData.VolumeDims.z = (float) (vs.z - vs.z % s_volumeThreadsZ);
	m_actualVolumeSize.x = (int) (m_perFrameData.VolumeDims.x / s_volumeThreadsX);
	m_actualVolumeSize.y = (int) (m_perFrameData.VolumeDims.y / s_volumeThreadsY);
	m_actualVolumeSize.z = (int) (m_perFrameData.VolumeDims.z / s_volumeThreadsZ);
	m_actualRenderSize.x = (int) ((m_perFrameData.ViewportDims.x + s_renderThreadsX - 1) / s_renderThreadsX);
	m_actualRenderSize.y = (int) ((m_perFrameData.ViewportDims.y + s_renderThreadsY - 1) / s_renderThreadsY);
	m_actualRenderSize.z = 1;
}

void Demo::VolumeFluid::SetImpactRadius(float radius)
{
	m_perFrameData.ForceImpact.x = radius;
}

void  Demo::VolumeFluid::GetVolumeSize(INT32x3 &vs)
{
	vs.x = (int) m_perFrameData.VolumeDims.x;
	vs.y = (int) m_perFrameData.VolumeDims.y;
	vs.z = (int) m_perFrameData.VolumeDims.z;
}

dx::HResult Demo::VolumeFluid::CreateTextures()
{
	dx::HResult result;

	dx::CSampleStateDescription linearSamplerDesc(
		D3D11_FILTER_MIN_MAG_MIP_LINEAR,
		D3D11_TEXTURE_ADDRESS_WRAP,
		D3D11_TEXTURE_ADDRESS_WRAP,
		D3D11_TEXTURE_ADDRESS_WRAP,
		0, 0, D3D11_COMPARISON_NEVER,
		nullptr, 0, D3D11_FLOAT32_MAX
		);

  dx::CTexture3Description speedTxDesc(
		DXGI_FORMAT_R16G16B16A16_FLOAT,
		//DxgiFormat::DXGI_FORMAT_R32G32B32A32_FLOAT,
		(uint32_t) m_perFrameData.VolumeDims.x,
		(uint32_t) m_perFrameData.VolumeDims.y,
		(uint32_t) m_perFrameData.VolumeDims.z, 0,
		D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS
		);

  dx::CTexture3Description pressureTxDesc(
		DXGI_FORMAT_R16_FLOAT,
		//DxgiFormat::DXGI_FORMAT_R32_FLOAT,
		(uint32_t) m_perFrameData.VolumeDims.x,
		(uint32_t) m_perFrameData.VolumeDims.y,
		(uint32_t) m_perFrameData.VolumeDims.z, 0,
		D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS
		);

	result = _d3ddvc->CreateSamplerState(&linearSamplerDesc, m_linearSampler.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateTexture3D(&speedTxDesc, nullptr, m_speedTxs[0].resource.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateTexture3D(&speedTxDesc, nullptr, m_speedTxs[1].resource.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateTexture3D(&speedTxDesc, nullptr, m_speedTxs[2].resource.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateTexture3D(&pressureTxDesc, nullptr, m_pressureTxs[0].resource.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateTexture3D(&pressureTxDesc, nullptr, m_pressureTxs[1].resource.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateTexture3D(&pressureTxDesc, nullptr, m_divergenceTx.resource.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateShaderResourceView(m_speedTxs[0].resource.Get(), nullptr, m_speedTxs[0].readView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateShaderResourceView(m_speedTxs[1].resource.Get(), nullptr, m_speedTxs[1].readView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateShaderResourceView(m_speedTxs[2].resource.Get(), nullptr, m_speedTxs[2].readView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateUnorderedAccessView(m_speedTxs[0].resource.Get(), nullptr, m_speedTxs[0].writeView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateUnorderedAccessView(m_speedTxs[1].resource.Get(), nullptr, m_speedTxs[1].writeView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateUnorderedAccessView(m_speedTxs[2].resource.Get(), nullptr, m_speedTxs[2].writeView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateShaderResourceView(m_pressureTxs[0].resource.Get(), nullptr, m_pressureTxs[0].readView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateShaderResourceView(m_pressureTxs[1].resource.Get(), nullptr, m_pressureTxs[1].readView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateShaderResourceView(m_divergenceTx.resource.Get(), nullptr, m_divergenceTx.readView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateUnorderedAccessView(m_divergenceTx.resource.Get(), nullptr, m_divergenceTx.writeView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateUnorderedAccessView(m_pressureTxs[0].resource.Get(), nullptr, m_pressureTxs[0].writeView.ReleaseAndGetAddressOf());
	result = _d3ddvc->CreateUnorderedAccessView(m_pressureTxs[1].resource.Get(), nullptr, m_pressureTxs[1].writeView.ReleaseAndGetAddressOf());

	return result;
}

void Demo::VolumeFluid::OnMouseWheel(INT16 mouseWheel)
{
	if (mouseWheel > 0i16) m_perFrameData.FocusZoom.x *= 1.1f;
	else m_perFrameData.FocusZoom.x /= 1.1f;

	Viewer->UpdateView({ 0, 0 }, m_perFrameData.FocusZoom.x);
}

void Demo::VolumeFluid::OnMouseMoved(dx::Vector2 mousePos)
{
	dx::Vector2 delta = mousePos - m_prevMousePos;

	m_perFrameData.Mouse.x = (float) mousePos.x;
	m_perFrameData.Mouse.y = (float) mousePos.y;
	m_prevMousePos = mousePos;

	if (m_mbLPressed)
	{
		delta.x *= 10.0f; // m_perFrameData.VolumeDims.x;// / m_perFrameData.ViewportDims.x;
		delta.y *= 10.0f; //m_perFrameData.VolumeDims.y;// / m_perFrameData.ViewportDims.y;
		m_perFrameData.Drag.x = delta.x;
		m_perFrameData.Drag.y = delta.y;

		dx::trace("onMouseMove: drag %3.3f %3.3f", delta.x, delta.y);

	}
	else
	{
		m_perFrameData.Drag.x = 0;
		m_perFrameData.Drag.y = 0;
	}

	if (m_mbRPressed)
	{
		Viewer->UpdateView(delta, 0);
	}
}

void Demo::VolumeFluid::OnMouseRPressed(dx::Vector2 mousePos)
{
	m_perFrameData.Mouse.x = (float) mousePos.x;
	m_perFrameData.Mouse.y = (float) mousePos.y;
	m_perFrameData.Drag.x = 0;
	m_perFrameData.Drag.y = 0;
	m_prevMousePos = mousePos;
	m_mbRPressed = TRUE;
}

void Demo::VolumeFluid::OnMouseLPressed(dx::Vector2 mousePos)
{
	m_mbLPressed = TRUE;
}

void Demo::VolumeFluid::OnMouseRReleased(dx::Vector2 mousePos)
{
	m_mbRPressed = FALSE;
}

void Demo::VolumeFluid::OnMouseLReleased(dx::Vector2 mousePos)
{
	m_mbLPressed = FALSE;
}
