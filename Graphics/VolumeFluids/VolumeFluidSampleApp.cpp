
#include "VolumeFluidSampleApp.h"
#include <INIReader.h>
#include <DirectXColors.h>

static inline float random(float min, float max)
{
  return float(rand()) / float(RAND_MAX) * (max - min) + min;
}

dx::DxSampleApp *dx::DxSampleAppFactory()
{
  return new nyx::VolumeFluidSampleApp();
}

nyx::VolumeFluidSampleApp::VolumeFluidSampleApp()
#if _Nyx_Sample_with_UI
  : DxUISampleApp(true, true, D3D_DRIVER_TYPE_HARDWARE)
  //: DxUISampleApp(true, true, D3D_DRIVER_TYPE_REFERENCE)
#else
  : DxSampleApp(D3D_DRIVER_TYPE_HARDWARE)
  //: DxSampleApp(D3D_DRIVER_TYPE_REFERENCE)
#endif
  , fluids(&deviceResources)
  , mousePressed(false)
  , mouseRPressed(false)
{
  deviceResources.clearColor = (dx::XMVECTOR) DirectX::Colors::DeepSkyBlue;
#if !_VolumeFluids_Draw_to_texture
  deviceResources.renderTargetUsage |= DXGI_USAGE_UNORDERED_ACCESS; // <- uav
  deviceResources.renderTargetUsage |= DXGI_USAGE_SHADER_INPUT;
  //deviceResources.renderTargetUsage |= DXGI_USAGE_DISCARD_ON_PRESENT; // not supported
  deviceResources.renderTargetFormat = DXGI_FORMAT_R16G16B16A16_FLOAT; // <- uav
  deviceResources.swapchainAlphaMode = DXGI_ALPHA_MODE_IGNORE;
  deviceResources.swapchainEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
#endif

  INIReader iniReader("VolumeFluids.ini");
  uiUrl = iniReader.Get("UI", "appUiUrl", "http://www.google.com");
}

nyx::VolumeFluidSampleApp::~VolumeFluidSampleApp()
{
}

std::string nyx::VolumeFluidSampleApp::getUIURL()
{
  return uiUrl;
}

#if _Nyx_Sample_with_UI
void nyx::VolumeFluidSampleApp::initializeSample()
#else
bool nyx::VolumeFluidSampleApp::onAppLoop()
#endif
{
  dx::trace("nyx:app: initialize sample");

  window.setWindowTitle("VolumeFluids");
  dx::DxAssetManager::addSearchPath("VolumeFluids");

  fluids.Viewer = &camera;
  fluids.SetImpactRadius(0.5f);
  fluids.SetVolumeSize(256);
  fluids.CreateDeviceIndependentResources();
  fluids.CreateDeviceDependentResources();
  fluids.CreateWindowSizeDependentResources();
  timer.reset();
#if _Nyx_Sample_with_UI
#else
  return true;
#endif
}

void nyx::VolumeFluidSampleApp::handlePaint(dx::DxWindow const*)
{
  static unsigned frameIndex = 0;
  static const unsigned printFrameIndex = 512;
  static const unsigned maxFrameIndex = printFrameIndex * 10;

  timer.update();
  frameIndex = ++frameIndex % maxFrameIndex;
  if (frameIndex % printFrameIndex == 0)
    dx::trace("nyx:app: fps %d", timer.framesPerSecond);

#if _VolumeFluids_Draw_to_texture

  beginScene();

  fluids.Update();
  fluids.Render();

  endScene();

#else

  deviceResources.unbindFromPipeline(deviceResources.context.Get());
  fluids.Update();
  fluids.Render();
  deviceResources.bindToPipeline(deviceResources.context.Get());
  deviceResources.present();

#endif
}

void nyx::VolumeFluidSampleApp::sizeChanged()
{

}

void nyx::VolumeFluidSampleApp::onUICreated()
{

}

void nyx::VolumeFluidSampleApp::onUIDestroyed()
{
}

#if _Nyx_Sample_with_UI

void nyx::VolumeFluidSampleApp::keyReleased(dx::DxGenericEventArgs const &args)
{
  switch (args.keyCode())
  {
  case dx::VirtualKey::Escape: window.close(); break;
  default: {} break;
  }

}

void nyx::VolumeFluidSampleApp::pointerMoved(dx::DxGenericEventArgs const &args)
{
  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();

  if (mousePressed)
  {
    dx::Vector2 mouseDelta = mousePos - previousMousePos;
    fluids.OnMouseMoved(mousePos);


    // ...
  }

  previousMousePos = mousePos;
}

void nyx::VolumeFluidSampleApp::pointerLeftPressed(dx::DxGenericEventArgs const &args)
{
  mousePressed = true;

  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();
  fluids.OnMouseLPressed(mousePos);
}

void nyx::VolumeFluidSampleApp::pointerLeftReleased(dx::DxGenericEventArgs const &args)
{
  mousePressed = false;

  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();
  fluids.OnMouseLReleased(mousePos);
}

void nyx::VolumeFluidSampleApp::pointerWheel(dx::DxGenericEventArgs const &args)
{
  if (mousePressed)
  {
    // ...
  }

  fluids.OnMouseWheel(args.wheelDelta());
}

void nyx::VolumeFluidSampleApp::pointerRightPressed(dx::DxGenericEventArgs const &args)
{
  mouseRPressed = true;

  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();
  fluids.OnMouseRPressed(mousePos);
}

void nyx::VolumeFluidSampleApp::pointerRightReleased(dx::DxGenericEventArgs const &args)
{
  mouseRPressed = false;

  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();
  fluids.OnMouseRReleased(mousePos);
}

#else

void nyx::VolumeFluidSampleApp::handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  switch (args.keyCode())
  {
  case dx::VirtualKey::Escape: window.close(); break;
  default: {} break;
  }

}

void nyx::VolumeFluidSampleApp::handleMouseMoved(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();
  fluids.OnMouseMoved(mousePos);

#if 0

  if (mousePressed)
  {
    dx::Vector2 mouseDelta = mousePos - previousMousePos;


    // ...
  }

  previousMousePos = mousePos;
#endif
}

void nyx::VolumeFluidSampleApp::handleMouseLeftPressed(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  mousePressed = true;

  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();
  fluids.OnMouseLPressed(mousePos);
}

void nyx::VolumeFluidSampleApp::handleMouseLeftReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  mousePressed = false;

  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();
  fluids.OnMouseLReleased(mousePos);
}

void nyx::VolumeFluidSampleApp::handleMouseWheel(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  if (mousePressed)
  {
    // ...
  }

  fluids.OnMouseWheel(args.wheelDelta());
}

void nyx::VolumeFluidSampleApp::handleMouseRightPressed(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  mouseRPressed = true;

  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();
  fluids.OnMouseRPressed(mousePos);
}

void nyx::VolumeFluidSampleApp::handleMouseRightReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  mouseRPressed = false;

  dx::Vector2 mousePos;
  mousePos.x = args.mouseX();
  mousePos.y = args.mouseY();
  fluids.OnMouseRReleased(mousePos);
}

#endif