#pragma once

#include "Camera.h"

#include <DxUtils.h>
#include <DxTimer.h>
#include <DxUIClient.h>
#include <DxUISampleApp.h>

#define __Decl_aligned(_Cls, _Algn) typedef _declspec(align(_Algn)) _Cls : public dx::AlignedNew<_Cls>

#define _VolumeFluids_Draw_to_texture 1

namespace Demo
{
	struct VolumeFluid
	{
		typedef struct INT32x2 
		{
			union 
			{ 
				struct { int x, y; }; 
				struct { int w, h; }; 
			};

			INT32x2(int x = 0) { w = x; h = x; }
			INT32x2(int x, int y) { w = x; h = y; }

			operator dx::Vector2()
			{
				dx::Vector2 result;
				result.x = (float) x;
				result.y = (float) y;
				return result;
			}

			INT32x2 operator-(INT32x2 const &other)
			{
				INT32x2 sub;
				sub.x = x - other.x;
				sub.y = y - other.y;
				return sub;
			}
			INT32x2 operator+(INT32x2 const &other)
			{
				INT32x2 add;
				add.x = x + other.x;
				add.y = y + other.y;
				return add;
			}
			INT32x2 operator-=(INT32x2 const &other)
			{
				INT32x2 sub;
				sub.x = x - other.x;
				sub.y = y - other.y;
				return sub;
			}
			INT32x2 operator+=(INT32x2 const &other)
			{
				INT32x2 add;
				add.x = x + other.x;
				add.y = y + other.y;
				return add;
			}
		} Int32x2;
		typedef struct INT32x3 : public INT32x2 
		{ 
			union { uint32_t z, d; };

			INT32x3(int x = 0) { w = x; h = x; d = x; }
			INT32x3(int x, int y) { w = x; h = y; d = 0; }
			INT32x3(int x, int y, int z) { w = x; h = y; d = z; }

			operator dx::Vector3()
			{
				dx::Vector3 result;
				result.x = (float) x;
				result.y = (float) y;
				result.z = (float) z;
				return result;
			}

			INT32x3 operator-(INT32x3 const &other)
			{
				INT32x3 sub;
				sub.x = x - other.x;
				sub.y = y - other.y;
				sub.z = z - other.z;
				return sub;
			}
			INT32x3 operator+(INT32x3 const &other)
			{
				INT32x3 add;
				add.x = x + other.x;
				add.y = y + other.y;
				add.z = z + other.z;
				return add;
			}
			INT32x3 operator-=(INT32x3 const &other)
			{
				INT32x3 sub;
				sub.x = x - other.x;
				sub.y = y - other.y;
				sub.z = z - other.z;
				return sub;
			}
			INT32x3 operator+=(INT32x3 const &other)
			{
				INT32x3 add;
				add.x = x + other.x;
				add.y = y + other.y;
				add.z = z + other.z;
				return add;
			}

		} Int32x3;

		typedef DirectX::Matrix float4x4;
    typedef DirectX::Vector4 float4;

    typedef unsigned __int16 Index, IndexUnit;
    struct Vertex
    {
      DirectX::Vector4 position;
      DirectX::Vector3 normal;
      DirectX::Vector2 tex;
    };

		__Decl_aligned(struct PerFrameCS, 16)
		{
			float4 VolumeDims; // xyz
			float4 ViewportDims; // xy
			float4 Mouse; // xy
			float4 Drag; // xy
			float4 ForceImpact; // x
			float4x4 EyeTransform; // [4][4]
			float4 FocusZoom; // x

    } PerFrameCS;
    __Decl_aligned(struct PerFrameVS, 16)
    {
      DirectX::Matrix world;
      DirectX::Matrix cameraView;
      DirectX::Matrix cameraProjection;

    } PerFrameVS;

    typedef dx::ActiveResource<dx::Texture3> ActiveTexture3;
    typedef dx::ConstantBuffer<VolumeFluid::PerFrameCS> PerFrameCSResources;
    typedef dx::ConstantBuffer<VolumeFluid::PerFrameVS> PerFrameVSResources;

		Camera *Viewer = nullptr;
		dx::DxDeviceResources *D3DResources;

		dx::HResult CreateDeviceDependentResources();
    dx::HResult CreateDeviceIndependentResources();
    dx::HResult CreateWindowSizeDependentResources();

		void Update();
		void Render();
		void OnMouseWheel(INT16 mouseWheel);
		void OnMouseMoved(dx::Vector2 mousePos);
		void OnMouseRPressed(dx::Vector2 mousePos);
		void OnMouseLPressed(dx::Vector2 mousePos);
		void OnMouseRReleased(dx::Vector2 mousePos);
		void OnMouseLReleased(dx::Vector2 mousePos);
		void SetVolumeSize(INT32x3 volumeSize);
		void GetVolumeSize(INT32x3 &volumeSize);
		void SetImpactRadius(float radius);

    VolumeFluid(dx::DxDeviceResources *D3DResources);

	private:

		VolumeFluid(const VolumeFluid&);
		VolumeFluid operator=(const VolumeFluid&);

	private:

    dx::HResult CreateShaders();
    dx::HResult CreateBuffers();
    dx::HResult CreateTextures();

		void Update_Advect();
		void Update_ApplyForces();
		void Update_Divergence();
		void Update_Pressure();
		void Update_PressureIteration();
		void Update_Project();
		void Render_Volume();
		void SwapSpeeds();

		bool m_mbRPressed = FALSE;
    bool m_mbLPressed = FALSE;
		DirectX::Vector2 m_prevMousePos;
		VolumeFluid::INT32x3 m_actualVolumeSize;
		VolumeFluid::INT32x3 m_actualRenderSize;


    dx::SRView m_targetSRV;
    dx::UAView m_targetUAV;
    dx::Texture2 m_renderOutput;
    dx::SamplerState m_linearSampler;
    dx::Buffer indexBuffer;
    dx::Buffer vertexBuffer;

    VolumeFluid::PerFrameCS m_perFrameData;
    VolumeFluid::PerFrameVS m_perFrameVSData;
    VolumeFluid::PerFrameCSResources m_perFrameBff;
    VolumeFluid::PerFrameVSResources m_perFrameVSBff;
		VolumeFluid::ActiveTexture3 m_speedTxs[3];
		VolumeFluid::ActiveTexture3 m_pressureTxs[2];
		VolumeFluid::ActiveTexture3 m_divergenceTx;

		static const uint32_t s_volumeThreadsX = 4;
		static const uint32_t s_volumeThreadsY = 4;
		static const uint32_t s_volumeThreadsZ = 4;
		static const uint32_t s_renderThreadsX = 8;
		static const uint32_t s_renderThreadsY = 8;
		static const uint32_t s_renderThreadsz = 1;

		dx::CShader m_advectCS;
		dx::CShader m_advectRCS;
    dx::CShader m_advectMCS;
    dx::CShader m_divergenceCS;
    dx::CShader m_applyForcesCS;
    dx::CShader m_pressureCS;
    dx::CShader m_projectCS;
    dx::CShader m_renderCS;

    dx::VShader m_renderVS;
    dx::VSLayout m_inputLayout;
    dx::PShader m_renderPS;

		static const uint32_t s_maxSlotsToUnbind = 3;
		static UINT32 s_nullUAVCounts[s_maxSlotsToUnbind];
    static dx::ISRView *s_nullSRVs[s_maxSlotsToUnbind];
    static dx::IUAView *s_nullUAVs[s_maxSlotsToUnbind];

		void UnbindViews(
			uint32_t slotsSRV = s_maxSlotsToUnbind, 
      uint32_t slotsUAV = s_maxSlotsToUnbind
			);

	};
}
