
cbuffer PerFrame
{
  row_major float4x4 World;
  row_major float4x4 CameraView;
  row_major float4x4 CameraProj;
};

struct VsInput
{
  float4 Position : POSITION;
  float3 Normal : NORMAL;
  float2 Coords : TEXCOORD0;
};

struct PsInput
{
  float4 Position : SV_POSITION;
  float3 Normal : NORMAL;
  float2 Coords : TEXCOORD0;
};

PsInput main(in VsInput input)
{
  PsInput output;

  //printf("vs : main()");

  input.Position.w = 1.0f;
  output.Coords = input.Coords;
  output.Normal = input.Normal;
  output.Position = mul(input.Position, World);
  output.Position = mul(output.Position, CameraView);
  output.Position = mul(output.Position, CameraProj);

  return output;
}
