#pragma once

#include "VolumeFluid.h"

#define _Nyx_Sample_with_UI 0

namespace nyx
{
  class VolumeFluidSampleApp
#if _Nyx_Sample_with_UI
    : public dxui::DxUISampleApp
#else
    : public dx::DxSampleApp
#endif
    , public dx::AlignedNew<VolumeFluidSampleApp>
  {
  public:
    std::string uiUrl;
    dx::sim::BasicTimer timer;

    Demo::Camera camera;
    Demo::VolumeFluid fluids;

    bool mousePressed;
    bool mouseRPressed;
    dx::Vector2 previousMousePos;

  public:
    VolumeFluidSampleApp();
    virtual ~VolumeFluidSampleApp();

  public: // DxSampleApp
    virtual void handlePaint(dx::DxWindow const*);

  public: // DxUISampleApp
    virtual std::string getUIURL(void);

#if _Nyx_Sample_with_UI
    virtual void initializeSample(void);
#else
    virtual bool onAppLoop(void);
#endif

  public: // DxUISampleApp
    virtual void sizeChanged(void);
    virtual void onUICreated(void);
    virtual void onUIDestroyed(void);

#if _Nyx_Sample_with_UI
    virtual void keyReleased(dx::DxGenericEventArgs const&);
    virtual void pointerMoved(dx::DxGenericEventArgs const&);
    virtual void pointerWheel(dx::DxGenericEventArgs const&);
    virtual void pointerLeftPressed(dx::DxGenericEventArgs const&);
    virtual void pointerLeftReleased(dx::DxGenericEventArgs const&);
    virtual void pointerRightPressed(dx::DxGenericEventArgs const&);
    virtual void pointerRightReleased(dx::DxGenericEventArgs const&);
#else
    virtual void handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseMoved(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseWheel(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseLeftPressed(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseLeftReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseRightPressed(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseRightReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);
#endif

  };
}
