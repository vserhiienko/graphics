#include "SampleApp.h"

#include "MayaExporter.h"

dx::DxSampleApp *dx::DxSampleAppFactory()
{
  return new aega::MayaExporterApplication();
}

aega::MayaExporterApplication::MayaExporterApplication()
//: DxUISampleApp(true, false)
{
  dx::trace("aega:app: create sample");
}

aega::MayaExporterApplication::~MayaExporterApplication()
{
  dx::trace("aega:app: destroy sample");
}

bool aega::MayaExporterApplication::onAppLoop()
{
  timer.reset();

  MayaExporter exporter;
  //exporter.exportMayaFile("D:/Dev/Ma/keel-reconstruction-pc-processed.mb");
  //exporter.exportMayaFile("D:/Dev/Ma/IronMan1_0.0001.ma", "D:/Dev/Ma/IronMan1_0.0001.ma.v2.aega");
  //exporter.exportMayaFile("D:/Dev/Ma/simpleScene01.ma");
  exporter.exportMayaFile(
    "F:/Dev/Ma/randomPolies.hard.ma",
    "F:/Dev/Ma/randomPolies.hard.ma.aega"
    );
  /*exporter.exportMayaFile(
    "D:/Dev/Ma/blackpanther_project_MA/blackpanther_project/scenes/blackpanther_v1.ma",
    "D:/Dev/Ma/blackpanther_project_MA/blackpanther_project/scenes/blackpanther_v1.mav2.aega"
    );*/
  //exporter.exportMayaFile("D:/Dev/Ma/spidey_v2.1/KevinRigv1_0.mb");
  //exporter.exportMayaFile("D:/Dev/Ma/plane-2x2.ma");
  //exporter.exportMayaFile("D:/Dev/Ma/sphere_r1.mb");
  //exporter.exportMayaFile("d:/dev/ma/spiderman1_0.ma");
  //exporter.exportMayaFile("d:/dev/ma/humanoid2.mb");

  return true;
}

void aega::MayaExporterApplication::handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  switch (args.keyCode())
  {
  case dx::VirtualKey::Escape: window.close(); break;
  default: { } break;
  }
}

void aega::MayaExporterApplication::handlePaint(dx::DxWindow const*)
{
  /// update

  timer.update();
  // update scene
  // ...
  // update ui

  /// draw

  beginScene();
  // render scene
  // ...
  // render ui
  endScene();
}
