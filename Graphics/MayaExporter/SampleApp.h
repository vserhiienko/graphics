#pragma once
#include <DxUtils.h>
#include <DxTimer.h>
#include <DxSampleApp.h>

namespace aega
{
  class MayaExporterApplication 
    //: public dxui::DxUISampleApp
    : public dx::DxSampleApp
    , public dx::AlignedNew<MayaExporterApplication>
  {
    // sample timer
    dx::sim::BasicTimer timer; 

  public:
    MayaExporterApplication();
    virtual ~MayaExporterApplication();

  public: // DxSampleApp
    virtual bool onAppLoop(void);
    virtual void handlePaint(dx::DxWindow const*);
    virtual void handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args);

  };
}