#include "MayaExporter.h"
#include <private/BasicIO.Utils.h>

#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <maya\MItDependencyNodes.h>
#include <maya\MLibrary.h>
#include <maya\MFileIO.h>
#include <maya\MFileObject.h>
#include <maya\MGlobal.h>
#include <maya\MEulerRotation.h>
#include <maya\MString.h>
#include <maya\MVector.h>
#include <maya\MFnImageSource.h>
#include <maya\MImage.h>
#include <maya\MImageFileInfo.h>
#include <maya\MTextureManager.h>
#include <maya\MFnTransform.h>
#include <maya\MItDag.h>
#include <maya\MFnDagNode.h>
#include <maya\MFnMesh.h>
#include <maya\MPointArray.h>
#include <maya\MFloatVectorArray.h>
#include <maya\MStringArray.h>
#include <maya\MItMeshPolygon.h>
#include <maya\MPlug.h>
#include <maya\MPlugArray.h>
#include <maya\MFnAnimCurve.h>
#include <maya\MAnimUtil.h>
#include <maya\MFnIkJoint.h>
#include <maya\MFnIkHandle.h>
#include <maya\MFnIkSolver.h>
#include <maya\MFnIkEffector.h>
#include <maya\MFnSkinCluster.h>
#include <maya\MFnWeightGeometryFilter.h>
#include <maya\MDagPath.h>
#include <maya\MDagPathArray.h>
#include <maya\MQuaternion.h>
#include <maya\MItGeometry.h>
#include <maya\MFnSet.h>
#include<maya/MFnLambertShader.h>
#include<maya/MFnBlinnShader.h>
#include<maya/MFnPhongShader.h>
#include<maya/MFnContainerNode.h>

using namespace aega;
using namespace aega::io;

class MayaExporter::Context
{
public:
  MS lastStatus;
  File::ShPtr file;
  BasicWriter *fileWriter;

  size_t sectionPs;
  size_t sectionSz;

public:
  Context() : fileWriter(0) { }
  ~Context() { safeDelete(fileWriter); }

public:
  void openFile(std::string const &filePath)
  {
    file = File::OpenFileForWritting(filePath.c_str());

    fileWriter = new BasicWriter(file);
    char sign[4] = { 'A', 'E', 'G', 'A' };
    fileWriter->NextElement<char[4]>(sign);
    fileWriter->NextElement<uint32_t>(0x00010001);
    safeDelete(fileWriter);

    fileWriter = new CompressedWriter(file);
  }

  void exportDep(MFnDependencyNode &depNode)
  {
    trace("exportDep: type %u, name %s"
      , depNode.type()
      , depNode.name().length() ? depNode.name().asChar() : "---"
      );

    fileWriter->NextElement<uint32_t>(depNode.type());
    fileWriter->WriteString(depNode.name().asChar());
  }

  void exportDag(MDagPath &dagPath)
  {
    MStatus status;
    MString dagFullPath = dagPath.fullPathName(&status);
    trace("exportDag: api type %u, dag path %s", dagPath.apiType(), dagFullPath.length() ? dagFullPath.asChar() : "---");

    fileWriter->NextElement<uint32_t>(dagPath.apiType());
    fileWriter->WriteString(dagFullPath.asChar());
  }

  void exportFamily(MFnDagNode &fn)
  {
    trace("\t\t - parents %u", fn.parentCount());

    fileWriter->NextElement<uint32_t>(fn.parentCount());

    for (uint32_t i = 0; i != fn.parentCount(); ++i)
    {
      // attach a function set to the parent object
      MFnDagNode fnParent(fn.parent(i));
      trace("\t\t\t - %s", fnParent.fullPathName().length() ? fnParent.fullPathName().asChar() : "<root>");
      fileWriter->WriteString(fnParent.fullPathName().length() ? fnParent.fullPathName().asChar() : "%");
    }

    trace("\t\t - children %u", fn.childCount());
    fileWriter->NextElement<uint32_t>(fn.childCount());

    for (uint32_t i = 0; i != fn.childCount(); ++i)
    {
      // attach a function set to the child object
      MFnDagNode fnChild(fn.child(i));
      trace("\t\t\t - %s", fnChild.fullPathName().asChar());
      fileWriter->WriteString(fnChild.fullPathName().asChar());
    }
  }

  static MIntArray getLocalIndex(MIntArray & getVertices, MIntArray & getTriangle)
  {
    MIntArray   localIndex;
    unsigned    gv, gt;

    assert(getTriangle.length() == 3);    // Should always deal with a triangle

    for (gt = 0; gt < getTriangle.length(); gt++)
    {
      for (gv = 0; gv < getVertices.length(); gv++)
      {
        if (getTriangle[gt] == getVertices[gv])
        {
          localIndex.append(gv);
          break;
        }
      }

      // if nothing was added, add default "no match"
      if (localIndex.length() == gt)
        localIndex.append(-1);
    }

    return localIndex;
  }

  void exportMesh(MFnMesh &fnMesh, MDagPath &dagForMesh)
  {
    MStatus status;

    MPointArray meshPoints;
    fnMesh.getPoints(meshPoints, MSpace::kObject);

    fileWriter->NextElement<uint32_t>(MSpace::kObject);
    fileWriter->NextElement<uint32_t>(meshPoints.length());

    trace("\t\t - points %u", meshPoints.length());

    for (uint32_t i = 0; i < meshPoints.length(); i++)
    {
      meshPoints[i].cartesianize();

      float xyz[3] =
      {
        (float)meshPoints[i].x,
        (float)meshPoints[i].y,
        (float)meshPoints[i].z
      };

      fileWriter->NextElement<float[3]>(xyz);
    }

    MFloatVectorArray  meshNormals;
    fnMesh.getNormals(meshNormals);

    fileWriter->NextElement<uint32_t>(MSpace::kObject);
    fileWriter->NextElement<uint32_t>(meshNormals.length());

    trace("\t\t - normals %u", meshNormals.length());

    for (uint32_t i = 0; i < meshNormals.length(); i++)
    {
      float xyz[3] =
      {
        (float)meshNormals[i].x,
        (float)meshNormals[i].y,
        (float)meshNormals[i].z
      };

      fileWriter->NextElement<float[3]>(xyz);
    }



    // Get UVSets for this mesh
    MStringArray  UVSets;
    status = fnMesh.getUVSetNames(UVSets);
    fileWriter->NextElement<uint32_t>(UVSets.length());
    trace("\t\t - uv sets %u", UVSets.length());

    for (uint32_t i = 0; i < UVSets.length(); i++)
    {
      // Get all UVs for the first UV set.
      MFloatArray u, v;
      fnMesh.getUVs(u, v, &UVSets[i]);
      assert(u.length() == v.length());

      trace("\t\t\t - uv set %u", u.length());

      fileWriter->WriteString(UVSets[i].asChar());
      fileWriter->NextElement<uint32_t>(u.length());
      for (uint32_t j = 0; j < u.length(); j++)
        fileWriter->NextElement<float>(u[i]),
        fileWriter->NextElement<float>(v[i]);
    }

    // Get Colors for this mesh
    // (Not used in this example - included for reference)
    MColorArray  vertexColors;
    fnMesh.getVertexColors(vertexColors);
    fileWriter->NextElement<uint32_t>(vertexColors.length());

    trace("\t\t - vertex colors %u", vertexColors.length());

    for (uint32_t i = 0; i < vertexColors.length(); i++)
    {
      fileWriter->NextElement<float>(vertexColors[i].r);
      fileWriter->NextElement<float>(vertexColors[i].g);
      fileWriter->NextElement<float>(vertexColors[i].b);
      fileWriter->NextElement<float>(vertexColors[i].a);
    }

    MColorArray  faceVertexColors;
    fnMesh.getFaceVertexColors(faceVertexColors);
    fileWriter->NextElement<uint32_t>(faceVertexColors.length());
    trace("\t\t - face vertex colors %u", faceVertexColors.length());

    for (uint32_t i = 0; i < faceVertexColors.length(); i++)
    {
      fileWriter->NextElement<float>(faceVertexColors[i].r);
      fileWriter->NextElement<float>(faceVertexColors[i].g);
      fileWriter->NextElement<float>(faceVertexColors[i].b);
      fileWriter->NextElement<float>(faceVertexColors[i].a);
    }

    uint32_t polygonCount = 0;

    MItMeshPolygon  itPolygon(dagForMesh, MObject::kNullObj);
    //for ( /* nothing */; !itPolygon.isDone(); itPolygon.next())
    //{
    //  int triangleCount;
    //  status = itPolygon.numTriangles(triangleCount);
    //  polygonCount += (uint32_t)triangleCount;
    //}

    polygonCount = itPolygon.count();
    fileWriter->NextElement<uint32_t>(polygonCount);

    trace("\t\t - polies %u", polygonCount);

    itPolygon.reset();
    for ( /* nothing */; !itPolygon.isDone(); itPolygon.next())
    {
      polygonCount--;

      MIntArray polygonVertices; // object-relative indices for the vertices in this face
      itPolygon.getVertices(polygonVertices);

      std::vector<int> polygonVerticesV;
      polygonVerticesV.resize(polygonVertices.length());
      for (size_t i = 0; i < polygonVerticesV.size(); i++)
        polygonVerticesV[i] = polygonVertices[i];

      int triangleCount;
      status = itPolygon.numTriangles(triangleCount); // triangulation of this poly

      uint32_t triangleInd = 0;
      uint32_t triangleCountU = triangleCount;
      fileWriter->NextElement<uint32_t>(triangleCount);

      while (triangleCount--)
      {
        triangleCountU--;
        MPointArray nonTweaked;

        MIntArray localIndex; // face-relative vertex indices for each triangle
        MIntArray triangleVertices; // object-relative vertex indices for each triangle
        status = itPolygon.getTriangle(triangleInd, nonTweaked, triangleVertices, MSpace::kObject);

        std::vector<int> triangleVerticesV;
        triangleVerticesV.resize(triangleVertices.length());
        for (size_t i = 0; i < triangleVerticesV.size(); i++)
          triangleVerticesV[i] = triangleVertices[i];

        triangleInd++;

        localIndex = getLocalIndex(polygonVertices, triangleVertices);

        int li[3] =
        {
          localIndex[0],
          localIndex[1],
          localIndex[2],
        };

        uint32_t pointIndices[3] =
        {
          (uint32_t)triangleVertices[0],
          (uint32_t)triangleVertices[1],
          (uint32_t)triangleVertices[2],
        };

        uint32_t normalIndices[3] =
        {
          itPolygon.normalIndex(localIndex[0]),
          itPolygon.normalIndex(localIndex[1]),
          itPolygon.normalIndex(localIndex[2]),
        };

        fileWriter->NextElement<uint32_t[3]>(pointIndices);
        fileWriter->NextElement<uint32_t[3]>(normalIndices);

        if (UVSets.length() != 0u) 
        {
          int32_t uvID[3] = { 0 }; // uv values for each vertex within this polygon
          if (itPolygon.hasUVs(UVSets[0], &status) && !status.error())
          {
            for (int j = 0; j < 3; j++)
            {
              int vertex = polygonVertices[localIndex[j]];
              status = itPolygon.getUVIndex(vertex, uvID[j], &UVSets[0]);
            }

            fileWriter->NextElement<int32_t[3]>(uvID);
          }
          else
          {
            fileWriter->NextElement<int32_t[3]>({ 0, 0, 0 });
          }

          /*
          for (uint32_t i = 0; i < UVSets.length(); i++)
          {
          }
          */
        }
        else
        {
          fileWriter->NextElement<int32_t[3]>({0, 0, 0});
        }

        MColor colours[3]; // colour values for each vertex within this polygon
        for (int vtxInPolygon = 0; vtxInPolygon < 3; vtxInPolygon++)
        {
          itPolygon.getColor(colours[vtxInPolygon], localIndex[vtxInPolygon]);
        }

        // positions:
        //  { 
        //    meshPoints[triangleVertices[0]],
        //    meshPoints[triangleVertices[1]],
        //    meshPoints[triangleVertices[2]],
        //  }

        // normals:
        //  { 
        //    meshNormals[ itPolygon.normalIndex( localIndex[0] ) ],
        //    meshNormals[ itPolygon.normalIndex( localIndex[1] ) ],
        //    meshNormals[ itPolygon.normalIndex( localIndex[2] ) ],
        //  }

        // UVs are:
        //  { 
        //    { u[uvID[0]], v[uvID[0]] },
        //    { u[uvID[1]], v[uvID[1]] },
        //    { u[uvID[2]], v[uvID[2]] },
        //  }


        // colours:
        //  { 
        //    colours[0],
        //    colours[1],
        //    colours[2],
        //  }

        for (uint32_t i = 0; i < 3; i++)
        {
          fileWriter->NextElement<float>(colours[i].r);
          fileWriter->NextElement<float>(colours[i].g);
          fileWriter->NextElement<float>(colours[i].b);
          fileWriter->NextElement<float>(colours[i].a);
        }

      } // while (triangles)

      assert(triangleCountU == 0);

    } // itPolygon()

    assert(polygonCount == 0);
  }

  void exportAnimCurve(MFnAnimCurve &fn)
  {
    uint32_t iKeyCount = fn.numKeys();
    fileWriter->NextElement<uint32_t>(iKeyCount);

    trace("\t\t\t\t - %s, keys %u", fn.name().asChar(), iKeyCount);


    // dont bother if its a pointless curve....
    if (iKeyCount == 0) return;

    // get all keyframe times & values
    for (unsigned int i = 0; i < iKeyCount; i++)
    {
      float ix, iy, ox, oy;
      fn.getTangent(i, ix, iy, true);
      fn.getTangent(i, ox, oy, false);

      float t = (float)fn.time(i).as(MTime::kSeconds);
      float v = (float)fn.value(i);

      uint32_t it = fn.inTangentType(i);
      uint32_t ot = fn.outTangentType(i);

      fileWriter->NextElement<float>(t);
      fileWriter->NextElement<float>(v);
      fileWriter->NextElement<uint32_t>(it);
      fileWriter->NextElement<float>(ix);
      fileWriter->NextElement<float>(iy);
      fileWriter->NextElement<uint32_t>(ot);
      fileWriter->NextElement<float>(ox);
      fileWriter->NextElement<float>(oy);
    }

  }

  void exportTranform(MFnTransform &fn, MDagPath &dagForMesh)
  {
    MVector Translation;

    Translation = fn.getTranslation(MSpace::kTransform);

    double Scale[3];
    fn.getScale(Scale);

    MQuaternion Rotation(0, 0, 0, 1);
    fn.getRotation(Rotation);

    float translation[3] =
    {
      (float)Translation.x,
      (float)Translation.y,
      (float)Translation.z,
    };

    float scale[3] =
    {
      (float)Scale[0],
      (float)Scale[1],
      (float)Scale[2],
    };

    float rotation[4] =
    {
      (float)Rotation.x,
      (float)Rotation.y,
      (float)Rotation.z,
      (float)Rotation.w,
    };

    fileWriter->NextElement<float[3]>(translation);
    fileWriter->NextElement<float[3]>(scale);
    fileWriter->NextElement<float[4]>(rotation);

    // if the transform is an IK joint then it will contain
    // a joint orientation as well as a rotation
    if (fn.object().hasFn(MFn::kJoint))
    {
      MFnIkJoint fnJoint(fn.object());
      MQuaternion JointOrient(0, 0, 0, 1);
      fnJoint.getOrientation(JointOrient);

      float orientation[4] =
      {
        (float)JointOrient.x,
        (float)JointOrient.y,
        (float)JointOrient.z,
        (float)JointOrient.w,
      };

      fileWriter->NextElement<uint8_t>(1ui8);
      fileWriter->NextElement<float[4]>(orientation);

      trace("\t\t - joint");
    }
    else
    {
      fileWriter->NextElement<uint8_t>(0ui8);
      trace("\t\t - pose");
    }

    if (MAnimUtil::isAnimated(dagForMesh, false))
    {
      fileWriter->NextElement<uint8_t>(1ui8);

      MPlugArray animatedPlugs;
      MAnimUtil::findAnimatedPlugs(dagForMesh, animatedPlugs, false);
      fileWriter->NextElement<uint32_t>(animatedPlugs.length());

      trace("\t\t - anim plugs %u", animatedPlugs.length());

      for (uint32_t plugInd = 0; plugInd < animatedPlugs.length(); plugInd++)
      {
        MObjectArray curveObjects;
        MAnimUtil::findAnimation(animatedPlugs[plugInd], curveObjects);

        fileWriter->WriteString(animatedPlugs[plugInd].name().asChar());
        fileWriter->NextElement<uint32_t>(curveObjects.length());

        trace("\t\t\t - %s, anim curves %u", animatedPlugs[plugInd].name().asChar(), curveObjects.length());

        for (uint32_t curveInd = 0; curveInd < curveObjects.length(); curveInd++)
        {
          MFnDependencyNode curveDepNodeFn(curveObjects[curveInd]);
          fileWriter->WriteString(curveDepNodeFn.name().asChar());
          trace("\t\t\t\t - name %s", curveDepNodeFn.name().asChar());
        }
      }
    }
    else
    {
      fileWriter->NextElement<uint8_t>(0ui8);
      trace("\t\t - not animated");
    }

  }

  void exportDags()
  {
    MItDag dagMeshIt(MItDag::kBreadthFirst, MFn::kMesh);
    for (; !dagMeshIt.isDone(); dagMeshIt.next())
    {
      MDagPath dagPath;
      dagMeshIt.getPath(dagPath);

      MFnDagNode dagNode(dagPath);
      MFnMesh fnMesh(dagPath);

      exportDag(dagPath);
      exportFamily(dagNode);
      exportMesh(fnMesh, dagPath);
    }

    MItDag dagTransformIt(MItDag::kBreadthFirst, MFn::kTransform);
    for (; !dagTransformIt.isDone(); dagTransformIt.next())
    {
      MDagPath dagPath;
      dagTransformIt.getPath(dagPath);

      MFnDagNode dagNode(dagPath);
      MFnTransform fnTransform(dagPath);

      exportDag(dagPath);
      exportFamily(dagNode);
      exportTranform(fnTransform, dagPath);
    }

  }

  void exportJointCluster(MFnWeightGeometryFilter &jc)
  {
    MFnWeightGeometryFilter &fn = jc;

    MFnDependencyNode fnNode(jc.object());

    MPlugArray plugConnections;
    MPlug plug = fnNode.findPlug("matrix");
    plug.connectedTo(plugConnections, true, false);

    trace("\t\t - plugin connections %u", plugConnections.length());
    fileWriter->NextElement<uint32_t>(plugConnections.length());

    for (uint32_t i = 0; i < plugConnections.length(); i++)
    {
      MFnDependencyNode fnTransform(plugConnections[i].node());
      fileWriter->WriteString(fnTransform.name().asChar());

      trace("\t\t\t - transform %s", fnTransform.name().asChar());
    }

    // need the set fn to get the cluster members
    MFnSet setFn(jc.deformerSet());

    MSelectionList Shapes;

    // get all the shapes in the cluster
    setFn.getMembers(Shapes, true);

    trace("\t\t - shapes %u", Shapes.length());

    for (uint32_t i = 0; i < Shapes.length(); ++i)
    {
      MDagPath skinpath;
      MObject components;
      MFloatArray weights;

      // get a path to the i'th shape affected
      Shapes.getDagPath(i, skinpath, components);

      trace("\t\t\t - mesh %s", skinpath.fullPathName().asChar());

      // get the weights and vertex indices for
      // the points that are influenced.
      // Usually you can ignore the weight values
      // since they all tend to be 1.
      fn.getWeights(skinpath, components, weights);

      trace("\t\t\t - weights %u", weights.length());

      fileWriter->NextElement<uint32_t>(weights.length());
      for (uint32_t j = 0; j < weights.length(); j++)
      {
        fileWriter->NextElement<float>(weights[j]);
      }

      // output the vertex indices by iterating 
      // through the geometry components
      MItGeometry it(skinpath, components);

      uint32_t indexCount = (uint32_t)it.count();
      fileWriter->NextElement<uint32_t>(indexCount);

      trace("\t\t\t - indices %u", indexCount);

      for (; !it.isDone(); it.next())
      {
        uint32_t index = (uint32_t)it.index();
        fileWriter->NextElement<uint32_t>(index);
      }
    }

  }

  void exportSkinCluster(MFnSkinCluster &fn)
  {
    MDagPathArray infs;

    uint32_t nInfs = fn.influenceObjects(infs);
    fileWriter->NextElement<uint32_t>(nInfs);

    trace("\t\t - influence objects %u", nInfs);

    // print out the influence object names 
    for (unsigned int kk = 0; kk < nInfs; ++kk)
    {
      fileWriter->WriteString(infs[kk].fullPathName().asChar());
      trace("\t\t\t - influence object %s", infs[kk].fullPathName().asChar());
    }

    // loop through the geometries affected by this cluster
    uint32_t nGeoms = fn.numOutputConnections();
    fileWriter->NextElement<uint32_t>(nGeoms);

    trace("\t\t - output connections (geometries) %u", nGeoms);

    for (uint32_t ii = 0; ii < nGeoms; ++ii)
    {
      uint32_t index = fn.indexForOutputConnection(ii);

      // get the dag path of the ii'th geometry
      MDagPath skinPath;
      fn.getPathAtIndex(index, skinPath);

      fileWriter->WriteString(skinPath.fullPathName().asChar());
      trace("\t\t\t - skin %s", skinPath.fullPathName().asChar());

      // iterate through the components of this geometry
      MItGeometry gIter(skinPath);

      uint32_t componentCount = 0;
      for (; !gIter.isDone(); gIter.next())
        ++componentCount;
      gIter.reset();

      fileWriter->NextElement<uint32_t>(componentCount);
      trace("\t\t\t - components %u", componentCount);

      for (; !gIter.isDone(); gIter.next())
      {
        --componentCount;
        MObject comp = gIter.component();
        uint32_t componentIndex = gIter.index();

        fileWriter->NextElement<uint32_t>(componentIndex);

        MFloatArray weights;
        uint32_t influenceCount = 0;
        fn.getWeights(skinPath, comp, weights, influenceCount);

        uint32_t weightCount = weights.length();
        assert(weightCount == influenceCount);

        fileWriter->NextElement<uint32_t>(weightCount);
        for (uint32_t weightInd = 0; weightInd < weightCount; weightInd++)
        {
          fileWriter->NextElement<float>(weights[weightInd]);
        }
      }

      assert(componentCount == 0);
    }

  }

  void exportShader(MFnLambertShader &fn)
  {
    fileWriter->NextElement<float>(fn.color().r);
    fileWriter->NextElement<float>(fn.color().g);
    fileWriter->NextElement<float>(fn.color().b);
    fileWriter->NextElement<float>(fn.color().a);
    fileWriter->NextElement<float>(fn.ambientColor().r);
    fileWriter->NextElement<float>(fn.ambientColor().g);
    fileWriter->NextElement<float>(fn.ambientColor().b);
    fileWriter->NextElement<float>(fn.ambientColor().a);
    fileWriter->NextElement<float>(fn.incandescence().r);
    fileWriter->NextElement<float>(fn.incandescence().g);
    fileWriter->NextElement<float>(fn.incandescence().b);
    fileWriter->NextElement<float>(fn.incandescence().a);
    fileWriter->NextElement<float>(fn.transparency().r);
    fileWriter->NextElement<float>(fn.transparency().g);
    fileWriter->NextElement<float>(fn.transparency().b);
    fileWriter->NextElement<float>(fn.transparency().a);
    fileWriter->NextElement<float>(fn.diffuseCoeff());
    fileWriter->NextElement<float>(fn.glowIntensity());
    fileWriter->NextElement<float>(fn.translucenceCoeff());
  }

  void exportShader(MFnReflectShader &fn)
  {
    exportShader((MFnLambertShader&)fn);
    fileWriter->NextElement<float>(fn.specularColor().r);
    fileWriter->NextElement<float>(fn.specularColor().g);
    fileWriter->NextElement<float>(fn.specularColor().b);
    fileWriter->NextElement<float>(fn.specularColor().a);
    fileWriter->NextElement<float>(fn.reflectedColor().r);
    fileWriter->NextElement<float>(fn.reflectedColor().g);
    fileWriter->NextElement<float>(fn.reflectedColor().b);
    fileWriter->NextElement<float>(fn.reflectedColor().a);
    fileWriter->NextElement<float>(fn.reflectivity());
  }

  void exportShader(MFnPhongShader &fn)
  {
    exportShader((MFnReflectShader&)fn);
    fileWriter->NextElement<float>(fn.cosPower());
  }

  void exportShader(MFnBlinnShader &fn)
  {
    exportShader((MFnReflectShader&)fn);
    fileWriter->NextElement<float>(fn.eccentricity());
    fileWriter->NextElement<float>(fn.specularRollOff());
  }

  void exportIkHandle(MObject &obj)
  {
    MFnDependencyNode node(obj);

    exportDep(node);

    MFnIkHandle fn(obj);
    MFnIkSolver fnSolver(fn.solver());

    trace("\t\t - ik handle %s", fn.fullPathName().asChar());
    trace("\t\t\t - solver %s", fnSolver.name().asChar());

    fileWriter->WriteString(fn.fullPathName().asChar());
    fileWriter->WriteString(fnSolver.name().asChar());

    uint32_t stickness = fn.stickiness();
    float weight = fn.weight();
    float poWeight = fn.poWeight();

    fileWriter->NextElement<uint32_t>(stickness);
    fileWriter->NextElement<float>(weight);
    fileWriter->NextElement<float>(poWeight);

    // get the start joint and the effector
    MDagPath start, eff;
    fn.getEffector(eff);
    fn.getStartJoint(start);

    MFnDagNode fnEffector(eff);
    MFnDagNode fnStart(start);

    trace("\t\t\t - ik start %s", fnStart.fullPathName().asChar());
    trace("\t\t\t - ik effector %s", fnEffector.fullPathName().asChar());

    fileWriter->WriteString(fnStart.fullPathName().asChar());
    fileWriter->WriteString(fnEffector.fullPathName().asChar());

    // now lets hunt for a pole vector constraint 
    MPlug pvx = fn.findPlug("poleVectorX");
    MPlug pvy = fn.findPlug("poleVectorY");
    MPlug pvz = fn.findPlug("poleVectorZ");

    // get the pole vector values
    float px, py, pz;
    pvx.getValue(px);
    pvy.getValue(py);
    pvz.getValue(pz);

    fileWriter->NextElement<float>(px);
    fileWriter->NextElement<float>(py);
    fileWriter->NextElement<float>(pz);
  }

  void exportTexture(MObject &obj)
  {
    MFnDependencyNode fn(obj);
    MPlug ftn = fn.findPlug("ftn");
    MString filename; ftn.getValue(filename);

    exportDep(fn);
    fileWriter->WriteString(filename.asChar());

    trace("\t\t - texture file %s", filename.asChar());

    MImage image;
    uint8_t pixelType = MImage::kUnknown;
    if (image.readFromTextureNode(obj).error())
    {
      fileWriter->NextElement<uint8_t>(pixelType);
      trace("\t\t\t - pixels are unavailable", filename.asChar());
    }
    else
    {
      pixelType = image.pixelType();
      fileWriter->NextElement<uint8_t>(pixelType);

      // get the image size
      uint32_t w, h, d;
      image.getSize(w, h);
      d = image.depth();

      fileWriter->NextElement<uint32_t>(w);
      fileWriter->NextElement<uint32_t>(h);
      fileWriter->NextElement<uint32_t>(d);

      fileWriter->WriteBytes((void const*)image.pixels(), 1, (size_t)(w * h * d));
      trace("\t\t\t - pixels are available (%ux%u(%u) - %d)", w, h, d, image.pixelType());
    }
  }

  // _Delegate: (MObject &obj, uint32_t index)
  template <typename _Delegate> uint32_t forEachNode(MItDependencyNodes it, _Delegate delegate)
  {
    uint32_t count = 0;
    for (; !it.isDone(); it.next()) delegate(it.item(), count), ++count;
    return count;
  }

  // _Delegate: (MObject &obj, uint32_t index)
  template <typename _Delegate> uint32_t forEachNodeOfType(MFn::Type type, _Delegate delegate)
  {
    MItDependencyNodes it(type); return forEachNode(it, delegate);
  }

  void exportDeps()
  {
    MItDependencyNodes materialIt(MFn::kLambert);
    for (; !materialIt.isDone(); materialIt.next())
    {
      switch (materialIt.item().apiType())
      {
      case MFn::kPhong:
      {
        MFnPhongShader shader(materialIt.item());
        exportDep(shader);
        exportShader(shader);
      } break;
      case MFn::kLambert:
      {
        MFnLambertShader shader(materialIt.item());
        exportDep(shader);
        exportShader(shader);
      } break;
      case MFn::kBlinn:
      {
        MFnBlinnShader shader(materialIt.item());
        exportDep(shader);
        exportShader(shader);
      } break;
      }
    }

    MItDependencyNodes animCurveIt(MFn::kAnimCurve);
    for (; !animCurveIt.isDone(); animCurveIt.next())
    {
      MFnAnimCurve animCurveFn(animCurveIt.item());
      exportDep(animCurveFn);
      exportAnimCurve(animCurveFn);
    }

    MItDependencyNodes skinClusterIt(MFn::kSkinClusterFilter);
    for (; !skinClusterIt.isDone(); skinClusterIt.next())
    {
      MFnSkinCluster skinClusterFn(skinClusterIt.item());
      exportDep(skinClusterFn);
      exportSkinCluster(skinClusterFn);
    }

    MItDependencyNodes jointClusterIt(MFn::kJointCluster);
    for (; !jointClusterIt.isDone(); jointClusterIt.next())
    {
      MFnWeightGeometryFilter jointClusterFn(jointClusterIt.item());
      exportDep(jointClusterFn);
      exportJointCluster(jointClusterFn);
    }

    MItDependencyNodes ikHandleIt(MFn::kIkHandle);
    for (; !ikHandleIt.isDone(); ikHandleIt.next())
    {
      exportIkHandle(ikHandleIt.item());
    }

    /*MItDependencyNodes textureIt(MFn::kFileTexture);
    for (; !textureIt.isDone(); textureIt.next())
    {
      exportTexture(textureIt.item());
    }*/
  }

};

MayaExporter::MayaExporter() : impl(0) { }
MayaExporter::~MayaExporter() { safeDelete(impl); }

void aega::MayaExporter::exportMayaFile(std::string const &mayaFile)
{
  exportMayaFile(mayaFile, mayaFile + ".aega");
}

void aega::MayaExporter::exportMayaFile(std::string const &mayaFile, std::string const &aegaFile)
{
  // initialize maya library
  char exePath[1024] = { 0 };
  GetModuleFileNameA(GetModuleHandleA(0), exePath, sizeof(exePath));
  if (MLibrary::initialize(exePath).error()) return;
  if (MFileIO::open(mayaFile.c_str(), 0, false, MFileIO::kLoadAllReferences, true).error()) return;

  safeDelete(impl);
  impl = new Context();
  impl->openFile(aegaFile);
  impl->exportDags();
  impl->exportDeps();
  impl->fileWriter->NextElement(0ull);
  safeDelete(impl);

  MLibrary::cleanup(0, false);
}

void aega::MayaExporter::exportMesh(void)
{

}