#pragma once

#include <BasicIO.h>
#include <BinaryWriter.h>

namespace aega
{
  class MayaExporter
  {
    class Context;
    Context *impl;

  public:
    MayaExporter();
    ~MayaExporter();

  public:
    void exportMayaFile(std::string const &mayaFile);
    void exportMayaFile(std::string const &mayaFile, std::string const &aegaFile);

  protected:
    void exportMesh(void);

  };
}