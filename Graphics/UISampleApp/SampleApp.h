#pragma once
#include <DxTimer.h>
#include <DxUIClient.h>
#include <DxUISampleApp.h>

namespace nyx
{
  class SampleApp : public dxui::DxUISampleApp
  {
    // sample timer
    dx::sim::BasicTimer timer; 

  public:
    SampleApp();
    virtual ~SampleApp();

  public: // DxSampleApp
    virtual void handlePaint(dx::DxWindow const*);

  public: // DxUISampleApp
    virtual std::string getUIURL(void);
    virtual void initializeSample(void);

    virtual void sizeChanged(void);
    virtual void onUICreated(void);
    virtual void onUIDestroyed(void);

    virtual void keyReleased(dx::DxGenericEventArgs const &args);

  };
}