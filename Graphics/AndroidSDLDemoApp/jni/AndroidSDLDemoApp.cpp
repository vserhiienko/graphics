#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <android/log.h>

#include "SDL.h"

typedef struct Sprite
{
	SDL_Texture* texture;
	Uint16 w;
	Uint16 h;
} Sprite;

/* Adapted from SDL's testspriteminimal.c */
Sprite LoadSprite(const char* file, SDL_Renderer* renderer)
{
	Sprite result;
	result.texture = NULL;
	result.w = 0;
	result.h = 0;

	SDL_Surface* temp;

	/* Load the sprite image */
	temp = SDL_LoadBMP(file);
	if (temp == NULL)
	{
		fprintf(stderr, "Couldn't load %s: %s\n", file, SDL_GetError());
		return result;
	}
	result.w = temp->w;
	result.h = temp->h;

	/* Create texture from the image */
	result.texture = SDL_CreateTextureFromSurface(renderer, temp);
	if (!result.texture)
	{
		fprintf(stderr, "Couldn't create texture: %s\n", SDL_GetError());
		SDL_FreeSurface(temp);
		return result;
	}
	SDL_FreeSurface(temp);

	return result;
}

void draw(SDL_Window* window, SDL_Renderer* renderer, const Sprite sprite)
{
	int w, h;
	SDL_GetWindowSize(window, &w, &h);
	SDL_Rect destRect = { w / 2 - sprite.w / 2, h / 2 - sprite.h / 2, sprite.w, sprite.h };
	/* Blit the sprite onto the screen */
	SDL_RenderCopy(renderer, sprite.texture, NULL, &destRect);
}

int64_t getTimeNanoSecs()
{
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	return (int64_t) now.tv_sec * 1000000000LL + now.tv_nsec;
}
int64_t getTimeMicroSecs()
{
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	return (int64_t) now.tv_sec * 1000000LL + now.tv_nsec / 1000L;
}
int64_t getTimeMilliSecs()
{
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	return (int64_t) now.tv_sec * 1000LL + now.tv_nsec / 1000000L;
}

class Timer
{
	int64_t thisMs;
	int64_t initMs;
	int64_t lastMs;

public:
	int64_t elapsedMs;
	int64_t runtimeMs;

	void reset()
	{
		initMs = lastMs = getTimeMilliSecs();
	}

	void update()
	{
		thisMs = getTimeMilliSecs();
		elapsedMs = thisMs - lastMs;
		runtimeMs = thisMs - initMs;
		lastMs = thisMs;
	}
};

int main(int argc, char *argv [])
{
	SDL_Window *window;
	SDL_Renderer *renderer;

	SDL_TimerID timerId;

	if (SDL_CreateWindowAndRenderer(0, 0, 0, &window, &renderer) < 0)
		exit(2);

	/*Sprite sprite = LoadSprite("image.bmp", renderer);
	if (sprite.texture == NULL)
		exit(2);*/

	Timer timer;
	timer.reset();

	float accumSecs = 0.0f;
	uint accumFrames = 0;

	/* Main render loop */
	Uint8 done = 0;
	SDL_Event event;
	while (!done)
	{
		/* Check for events */
		(SDL_PollEvent(&event));
		{
			if (event.type == SDL_QUIT || 
				event.type == SDL_KEYDOWN || 
				event.type == SDL_FINGERDOWN)
			{
				done = 1;
			}
		}

		timer.update();
		auto elased = timer.elapsedMs;
		auto elasedS = (float) elased / 1000.0f;

		accumSecs += elasedS;
		++accumFrames;

		if (accumSecs >= 1.0f)
		{
			__android_log_print(ANDROID_LOG_INFO, "com.vladSerhiienko", "fps %u", accumFrames);
			accumSecs = 0;
			accumFrames = 0;
		}

		/* Draw a gray background */
		SDL_SetRenderDrawColor(renderer, 0xA0, 0xA0, 0xA0, 0xFF);
		SDL_RenderClear(renderer);

		//draw(window, renderer, sprite);

		/* Update the screen! */
		SDL_RenderPresent(renderer);

		//SDL_Delay(10);
	}

	exit(0);
}