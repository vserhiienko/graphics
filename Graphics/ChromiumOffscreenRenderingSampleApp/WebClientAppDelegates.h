#pragma once
#include "pch.h"

namespace nyx
{
  class WebClientApp;

  // Interface for browser delegates. All BrowserDelegates must be returned via CreateBrowserDelegates. Do not perform work in the BrowserDelegate constructor. See CefBrowserProcessHandler for documentation.
  class WebClientBrowserDelegate : public virtual CefBase
  {
  public:
    typedef std::set<CefRefPtr<WebClientBrowserDelegate>> Set;

  public:
    virtual void OnContextInitialized(CefRefPtr<WebClientApp> app) {}
    virtual void OnBeforeChildProcessLaunch(CefRefPtr<WebClientApp> app, CefRefPtr<CefCommandLine> command_line) {}
    virtual void OnRenderProcessThreadCreated(CefRefPtr<WebClientApp> app, CefRefPtr<CefListValue> extra_info) {}
  };

  // Interface for renderer delegates. All RenderDelegates must be returned via CreateRenderDelegates. Do not perform work in the RenderDelegate constructor. See CefRenderProcessHandler for documentation.
  class WebClientRendererDelegate : public CefBase
  {
  public:
    typedef std::set<CefRefPtr<WebClientRendererDelegate>> Set;

  public:
    virtual void OnWebKitInitialized(CefRefPtr<WebClientApp> app) {}
    virtual CefRefPtr<CefLoadHandler> GetLoadHandler(CefRefPtr<WebClientApp> app) { return NULL; }
    virtual void OnBrowserCreated(CefRefPtr<WebClientApp> app, CefRefPtr<CefBrowser> browser) {}
    virtual void OnBrowserDestroyed(CefRefPtr<WebClientApp> app, CefRefPtr<CefBrowser> browser) {}
    virtual void OnRenderThreadCreated(CefRefPtr<WebClientApp> app, CefRefPtr<CefListValue> extra_info) {}
    virtual bool OnBeforeNavigation(CefRefPtr<WebClientApp> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, cef_navigation_type_t navigation_type, bool is_redirect) { return false; }
    virtual void OnContextCreated(CefRefPtr<WebClientApp> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) {}
    virtual void OnContextReleased(CefRefPtr<WebClientApp> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) {}
    virtual void OnUncaughtException(CefRefPtr<WebClientApp> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context, CefRefPtr<CefV8Exception> exception, CefRefPtr<CefV8StackTrace> stackTrace) {}
    virtual void OnFocusedNodeChanged(CefRefPtr<WebClientApp> app, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefDOMNode> node) {}

    // Called when a process message is received. Return true if the message was handled and should not be passed on to other handlers. RenderDelegates should check for unique message names to avoid interfering with each other.
    virtual bool OnProcessMessageReceived(CefRefPtr<WebClientApp> app, CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message) { return false; }

  };
}