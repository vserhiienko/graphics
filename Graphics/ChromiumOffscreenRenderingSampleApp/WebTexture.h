#pragma once
#include "pch.h"

namespace nyx
{
  class Texture
  {
  public:
    bool transparent;
    int viewWidth, viewHeight;
    uint32_t textureId;

  public:
    Texture();
    ~Texture();

    void init();
    void cleanup();

    void update(
      CefRenderHandler::PaintElementType type, 
      const CefRenderHandler::RectList& dirtyRects, 
      const void* buffer, 
      int width,
      int height
      );

  };
}