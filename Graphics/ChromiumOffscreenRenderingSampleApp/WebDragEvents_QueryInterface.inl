*object = NULL;
if (IsEqualIID(iid, IID_IUnknown)) 
{

  IUnknown* obj = this;
  *object = obj;
}
else if (IsEqualIID(iid, __iid)) 
{

  _TyBaseComClass* obj = this;
  *object = obj;
}
else
{
  return E_NOINTERFACE;
}

AddRef();
return S_OK;