#pragma once
#include "pch.h"
#include "WebTexture.h"

namespace nyx
{
  class TextureRenderer
  {
  public:
    NvGLSLProgram *blitProg;

    void init();
    void cleanup();
    void render(Texture const &texture);

  private:
    GLint positionAttr;
    GLint texCoordsAttr;
    GLint sourceTexLocation;

  };
}