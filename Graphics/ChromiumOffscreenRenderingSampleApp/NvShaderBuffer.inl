
template <class T>
nyx::NvShaderBuffer<T>::NvShaderBuffer(size_t size) :
m_size(size)
{
  glGenBuffers(1, &m_buffer);

  bind();
  glBufferData(target, m_size * sizeof(T), 0, GL_STATIC_DRAW);
  unbind();
}

template <class T>
nyx::NvShaderBuffer<T>::~NvShaderBuffer()
{
  glDeleteBuffers(1, &m_buffer);
}

template <class T>
void nyx::NvShaderBuffer<T>::bind()
{
  glBindBuffer(target, m_buffer);
}

template <class T>
void nyx::NvShaderBuffer<T>::unbind()
{
  glBindBuffer(target, 0);
}

template <class T>
T *nyx::NvShaderBuffer<T>::map(GLbitfield access)
{
  bind();
  return (T *)glMapBufferRange(target, 0, m_size*sizeof(T), access);
}

template <class T>
void nyx::NvShaderBuffer<T>::unmap()
{
  bind();
  glUnmapBuffer(target);
}

template <class T>
void nyx::NvShaderBuffer<T>::dump()
{
  T *data = map(GL_MAP_READ_BIT);
  for (size_t i = 0; i<m_size; i++) {
    std::cout << i << ": " << data[i] << std::endl;
  }
  unmap();
}
