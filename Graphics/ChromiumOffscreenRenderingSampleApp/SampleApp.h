#pragma once
#include "pch.h"
#include <NV/NvShaderMappings.h>

#include "WebClientApp.h"
#include "WebClientWidget.h"
#include "WebTextureRenderer.h"

namespace nyx
{
  class SampleApp 
    : public NvSampleApp
    , public WebClientWidgetBrowserProvider
  {
  public: // NvSampleApp, NvAppBase 

    SampleApp(NvPlatformContext* platform);
    ~SampleApp(void);

    virtual void initUI(void) override;
    virtual void initRendering(void) override;
    virtual void update(void) override;
    virtual void draw(void) override;

    void configurationCallback(
      NvEGLConfiguration &config
      );

    virtual void reshape(
      int32_t width,
      int32_t height
      ) override;
    virtual bool handlePointerInput(
      NvInputDeviceType::Enum device,
      NvPointerActionType::Enum action,
      uint32_t modifiers, int32_t count,
      NvPointerEvent *points
      );
    virtual bool handleKeyInput(
      uint32_t code,
      NvKeyActionType::Enum action
      ) override;
    virtual CefRefPtr<CefBrowser> GetBrowser(
      ) override;

    virtual bool onMain();

  private:

    void initClient();

    CefRefPtr<WebClientApp> m_clientApp;
    CefRefPtr<WebClientWidget> m_widget;
    CefRefPtr<WebClientHandler> m_clientHandler;
    CefRefPtr<CefCommandLine> m_commandLine;

    char m_workingDirectory[MAX_PATH];
    bool m_clientInitialized;
    bool m_enableBrowsing;
    
    TextureRenderer m_textureRenderer;

  };
}