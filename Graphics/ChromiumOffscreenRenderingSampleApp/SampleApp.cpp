#include "pch.h"
#include "SampleApp.h"
#include "NvShaderUtils.h"

bool nyx::SampleApp::onMain()
{
  void* sandboxInfo = NULL;
  CefMainArgs mainArgs(GetModuleHandle(NULL));
  m_clientApp = new WebClientApp();
  int exit_code = CefExecuteProcess(mainArgs, m_clientApp, sandboxInfo);
  return exit_code == -1;
}

NvAppBase* NvAppFactory(NvPlatformContext* platform)
{
  //AllocConsole();
  //printf("sample app: process id: %d", GetCurrentProcessId());
  return new nyx::SampleApp(platform);
}

void nyx::SampleApp::configurationCallback(NvEGLConfiguration& config)
{
  config.depthBits = 24;
  config.stencilBits = 0;
  config.apiVer = NvGfxAPIVersionGL4();
}

nyx::SampleApp::SampleApp(NvPlatformContext* platform) : NvSampleApp(platform)
{
  m_widget = NULL;
  m_clientHandler = NULL;
  m_clientApp = NULL;
  m_clientInitialized = false;
  m_enableBrowsing = false;

  // Required in all subclasses to avoid silent link issues
  forceLinkHack();
}

nyx::SampleApp::~SampleApp(void)
{
  m_widget = NULL;
  m_clientHandler = NULL;
  m_clientApp = NULL;

  CefShutdown();
}

void nyx::SampleApp::initUI(void)
{
  if (mTweakBar)
  {
    mTweakBar->addValue("Enable browsing", m_enableBrowsing);
  }

  // Change the filtering for the framerate
  mFramerate->setMaxReportRate(.2f);
  mFramerate->setReportFrames(20);
}

CefRefPtr<CefBrowser> nyx::SampleApp::GetBrowser(
  )
{
  if (m_clientHandler.get()) return m_clientHandler->getBrowser();
  return NULL;
}

void nyx::SampleApp::initRendering(void)
{
  if (!requireMinAPIVersion(NvGfxAPIVersionGL4_4())) return;
  NvAssetLoaderAddSearchPath("ChromiumOffscreenRenderingSampleApp");

  m_textureRenderer.init();

}

void nyx::SampleApp::update(void)
{
  CefDoMessageLoopWork();
}

void nyx::SampleApp::draw(void)
{
  glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  m_textureRenderer.render(m_widget->getRenderer().getTexture());

}

void nyx::SampleApp::initClient()
{
  if (m_clientInitialized)
    return;

  // Retrieve the current working directory.
  if (_getcwd(m_workingDirectory, sizeof(m_workingDirectory)) == NULL)
  {
    m_workingDirectory[0] = 0;
    NVWindowsLog("failed to get working directory");
  }
  else
  {
    m_commandLine = CefCommandLine::CreateCommandLine();
    m_commandLine->InitFromString(::GetCommandLine());

    void* sandboxInfo = NULL;

    CefSettings settings;
    settings.no_sandbox = true;
    settings.multi_threaded_message_loop = false;
    settings.windowless_rendering_enabled = true;

    bool result = CefInitialize(CefMainArgs(GetModuleHandle(NULL)), settings, m_clientApp, sandboxInfo);
    if (!result)
    {
      errorExit("failed to initialized cef client");
    }

    bool transparent = false;
    m_clientHandler = new WebClientHandler();

    HWND windowHandle = (HWND)getGLContext()->nativeGetWindowHandle();
    m_widget = new WebClientWidget(this, transparent);
    m_widget->setWindowHandle(windowHandle);
    m_clientHandler->setRenderHandler(m_widget);

    CefWindowInfo info;
    CefBrowserSettings browserSettings;

    //info.window = windowHandle;
    info.parent_window = windowHandle;
    info.transparent_painting_enabled = transparent;
    info.windowless_rendering_enabled = true;

    CefBrowserHost::CreateBrowser(info, m_clientHandler, m_clientHandler->getStartupURL(), browserSettings, NULL);
    m_clientInitialized = true;
  }

}

void nyx::SampleApp::reshape(
  int32_t width,
  int32_t height
  )
{
  // update projection matrix in camera data
  glViewport(0, 0, (GLint)width, (GLint)height);
  initClient();

  CefRefPtr<CefBrowser> browser;
  CefRefPtr<CefBrowserHost> browserHost;

  if (m_enableBrowsing
    && (browser = GetBrowser())
    && (browserHost = browser->GetHost()))
  {
    browserHost->WasResized();
  }

}

bool nyx::SampleApp::handlePointerInput(
  NvInputDeviceType::Enum device,
  NvPointerActionType::Enum action,
  uint32_t modifiers,
  int32_t count,
  NvPointerEvent *points
  )
{
  CefMouseEvent mouseEvent;
  CefRefPtr<CefBrowser> browser;
  CefRefPtr<CefBrowserHost> browserHost;
  CefBrowserHost::MouseButtonType mouseButton;

  if (m_enableBrowsing
    && (browser = GetBrowser())
    && (browserHost = browser->GetHost()))
  {
    mouseEvent.x = points->m_x;
    mouseEvent.y = points->m_y;

    switch (points->m_id)
    {
    case 1: mouseButton = CefBrowserHost::MouseButtonType::MBT_LEFT; break;
    case 2: mouseButton = CefBrowserHost::MouseButtonType::MBT_RIGHT; break;
    case 4: mouseButton = CefBrowserHost::MouseButtonType::MBT_MIDDLE; break;
    }
    switch (action)
    {
    case NvPointerActionType::MOTION: browserHost->SendMouseMoveEvent(mouseEvent, false); break;
    case NvPointerActionType::UP: browserHost->SendMouseClickEvent(mouseEvent, mouseButton, true, 1); break;
    case NvPointerActionType::DOWN: browserHost->SendMouseClickEvent(mouseEvent, mouseButton, false, 1); break;
    }
  }

  return false;
}

bool nyx::SampleApp::handleKeyInput(
  uint32_t code,
  NvKeyActionType::Enum action
  )
{
  return false;
}