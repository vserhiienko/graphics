#pragma once
#include "pch.h"

namespace nyx
{
  class WebClientHandler
    : public CefClient
    , public CefContextMenuHandler
    , public CefDialogHandler
    , public CefDisplayHandler
    , public CefDownloadHandler
    , public CefDragHandler
    , public CefGeolocationHandler
    , public CefJSDialogHandler
    , public CefKeyboardHandler
    , public CefLifeSpanHandler
    , public CefLoadHandler
    , public CefRenderHandler
    , public CefRequestHandler
  {
  public:
    // Interface implemented to handle off-screen rendering.
    class RenderHandler : public CefRenderHandler 
    {
    public:
      virtual void OnBeforeClose(CefRefPtr<CefBrowser>) = 0;
    };

    typedef std::set<CefMessageRouterBrowserSide::Handler*> MessageHandlerSet;

    typedef enum MenuCommandId
    {
      MenuCommandId_ShowDevTools = MENU_ID_USER_FIRST,
      MenuCommandId_CloseDevTools,
    } MenuCommandId;

  public:
    WebClientHandler();
    virtual ~WebClientHandler();
    IMPLEMENT_REFCOUNTING(WebClientHandler);

    // CefClient methods
    virtual CefRefPtr<CefContextMenuHandler> GetContextMenuHandler() override;
    virtual CefRefPtr<CefDialogHandler> GetDialogHandler() override;
    virtual CefRefPtr<CefDisplayHandler> GetDisplayHandler() override;
    virtual CefRefPtr<CefDownloadHandler> GetDownloadHandler() override;
    virtual CefRefPtr<CefDragHandler> GetDragHandler() override;
    virtual CefRefPtr<CefGeolocationHandler> GetGeolocationHandler() override;
    virtual CefRefPtr<CefJSDialogHandler> GetJSDialogHandler() override;
    virtual CefRefPtr<CefKeyboardHandler> GetKeyboardHandler() override;
    virtual CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() override;
    virtual CefRefPtr<CefLoadHandler> GetLoadHandler() override;
    virtual CefRefPtr<CefRenderHandler> GetRenderHandler() override;
    virtual CefRefPtr<CefRequestHandler> GetRequestHandler() override;
    virtual bool OnProcessMessageReceived(CefRefPtr<CefBrowser>, CefProcessId, CefRefPtr<CefProcessMessage>) override;

    // CefContextMenuHandler methods
    virtual void OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model) override;
    virtual bool OnContextMenuCommand(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefContextMenuParams> params, int command_id, EventFlags event_flags) override;

    // CefDialogHandler methods
    virtual bool OnFileDialog(CefRefPtr<CefBrowser> browser, FileDialogMode mode, const CefString& title, const CefString& default_file_name, const std::vector<CefString>& accept_types, CefRefPtr<CefFileDialogCallback> callback) override;

    // CefDisplayHandler methods
    virtual void OnAddressChange(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& url) override;
    virtual void OnTitleChange(CefRefPtr<CefBrowser> browser, const CefString& title) override;
    virtual bool OnConsoleMessage(CefRefPtr<CefBrowser> browser, const CefString& message, const CefString& source, int line) override;

    // CefDownloadHandler methods
    virtual void OnBeforeDownload(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDownloadItem> download_item, const CefString& suggested_name, CefRefPtr<CefBeforeDownloadCallback> callback) override;
    virtual void OnDownloadUpdated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDownloadItem> download_item, CefRefPtr<CefDownloadItemCallback> callback) override;

    // CefDragHandler methods
    virtual bool OnDragEnter(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDragData> dragData, CefDragHandler::DragOperationsMask mask) override;

    // CefGeolocationHandler methods
    virtual bool OnRequestGeolocationPermission(CefRefPtr<CefBrowser> browser, const CefString& requesting_url, int request_id, CefRefPtr<CefGeolocationCallback> callback) override;

    // CefJSDialogHandler methods
    virtual void OnResetDialogState(CefRefPtr<CefBrowser> browser) override;
    virtual bool OnJSDialog(CefRefPtr<CefBrowser> browser, const CefString& origin_url, const CefString& accept_lang, JSDialogType dialog_type, const CefString& message_text, const CefString& default_prompt_text, CefRefPtr<CefJSDialogCallback> callback, bool& suppress_message) override;
    virtual bool OnBeforeUnloadDialog(CefRefPtr<CefBrowser> browser, const CefString& message_text, bool is_reload, CefRefPtr<CefJSDialogCallback> callback) override;

    // CefKeyboardHandler methods
    virtual bool OnPreKeyEvent(CefRefPtr<CefBrowser> browser, const CefKeyEvent& event, CefEventHandle os_event, bool* is_keyboard_shortcut) override;

    // CefLifeSpanHandler methods
    virtual bool DoClose(CefRefPtr<CefBrowser> browser) override;
    virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser) override;
    virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser) override;
    virtual bool OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name, const CefPopupFeatures& popupFeatures, CefWindowInfo& windowInfo, CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access) override;

    // CefLoadHandler methods
    virtual void OnLoadingStateChange(CefRefPtr<CefBrowser> browser, bool isLoading, bool canGoBack, bool canGoForward) override;
    virtual void OnLoadError(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, ErrorCode errorCode, const CefString& errorText, const CefString& failedUrl) override;

    // CefRequestHandler methods
    virtual bool OnBeforeBrowse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, bool is_redirect) override;
    virtual CefRefPtr<CefResourceHandler> GetResourceHandler(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request) override;
    virtual bool OnQuotaRequest(CefRefPtr<CefBrowser> browser, const CefString& origin_url, int64 new_size, CefRefPtr<CefQuotaCallback> callback) override;
    virtual void OnProtocolExecution(CefRefPtr<CefBrowser> browser, const CefString& url, bool& allow_os_execution) override;
    virtual void OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser, TerminationStatus status) override;

    // CefRenderHandler methods
    virtual bool GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override;
    virtual bool GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override;
    virtual bool GetScreenPoint(CefRefPtr<CefBrowser> browser, int viewX, int viewY, int& screenX, int& screenY) override;
    virtual bool GetScreenInfo(CefRefPtr<CefBrowser> browser, CefScreenInfo& screen_info) override;
    virtual void OnPopupShow(CefRefPtr<CefBrowser> browser, bool show) override;
    virtual void OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect) override;
    virtual void OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height) override;
    virtual void OnCursorChange(CefRefPtr<CefBrowser> browser, CefCursorHandle cursor) override;
    virtual bool StartDragging(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDragData> drag_data, CefRenderHandler::DragOperationsMask allowed_ops, int x, int y) override;
    virtual void UpdateDragCursor(CefRefPtr<CefBrowser> browser, CefRenderHandler::DragOperation operation) override;

  public:

    void setRenderHandler(CefRefPtr<RenderHandler> handler);
    CefRefPtr<RenderHandler> getRenderHandler() const;
    CefRefPtr<CefBrowser> getBrowser() const;
    int getBrowserId() const;

    // Request that all existing browser windows close.
    void closeAllBrowsers(bool force_close);

    // Returns true if the main browser window is currently closing. Used in
    // combination with DoClose() and the OS close notification to properly handle
    // 'onbeforeunload' JavaScript events during window close.
    bool isClosing() const;

    // Send a notification to the application. Notifications should not block the caller.
    enum NotificationType 
    {
      NotificationType_ConsoleMessage,
      NotificationType_DownloadComplete,
      NotificationType_DownloadError,
    };

    void sendNotification(NotificationType type);
    void showDevTools(CefRefPtr<CefBrowser> browser);
    void closeDevTools(CefRefPtr<CefBrowser> browser);

    // Returns the startup URL.
    std::string getStartupURL() const;

    void beginTracing();
    void endTracing();

    bool save(const std::string& path, const std::string& data);

  private:
    static void createMessageHandlers(MessageHandlerSet& handlers);

    // Returns the full download path for the specified file, or an empty path to
    // use the default temp directory.
    std::string getDownloadPath(const std::string& file_name);

  private:
    typedef std::list<CefRefPtr<CefBrowser> > BrowserList;  // List of any popup browser windows.
    typedef CefRefPtr<CefMessageRouterBrowserSide> MsgRouterBSCefRefPtr; // Handles the browser side of query routing. The renderer side is handled in client_renderer.cpp.

  private:

    //
    // START THREAD SAFE MEMBERS
    // The following members are thread-safe because they're initialized during
    // object construction and not changed thereafter.
    
    std::string             m_startup_url;                    // The startup URL.
    bool                    m_mouse_cursor_change_disabled;   // True if mouse cursor change is disabled.

    // END THREAD SAFE MEMBERS
    //
    
    mutable base::Lock      m_lock;                           // Lock used to protect members accessed on multiple threads. Make it mutable so that it can be used from const methods.

    //
    // START LOCK PROTECTED MEMBERS
    // The following members are accessed on multiple threads and must be protected by |lock_|.
    
    CefRefPtr<CefBrowser>   m_browser;      // The child browser window.
    int                     m_browser_id;   // The child browser id.
    bool                    m_is_closing;   // True if the main browser window is currently closing.

    // END LOCK PROTECTED MEMBERS
    //

    //
    // START UI THREAD ACCESS ONLY MEMBERS
    // The following members will only be accessed on the CEF UI thread.
   
    BrowserList               m_popup_browsers;             // List of any popup browser windows.
    CefRefPtr<RenderHandler>  m_osr_handler;                // The handler for off-screen rendering, if any.
    std::string               m_log_file;                   // Support for logging.
    std::string               m_last_download_file;         // Support for downloading files.
    bool                      m_focus_on_editable_field;    // True if an editable field currently has focus.
    MsgRouterBSCefRefPtr      m_message_router;             // Handles the browser side of query routing. The renderer side is handled in client_renderer.cpp.
    MessageHandlerSet         m_message_handler_set;        // Set of Handlers registered with the message router.
    
    static int s_browser_count; // Number of currently existing browser windows. The application will exit when the number of windows reaches 0.

    // END UI THREAD ACCESS ONLY MEMBERS
    //

  };
}