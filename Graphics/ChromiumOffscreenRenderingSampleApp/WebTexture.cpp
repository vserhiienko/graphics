#include "pch.h"
#include "WebTexture.h"


nyx::Texture::Texture()
{
  textureId = 0;

  viewWidth = 0;
  viewHeight = 0;
  transparent = false;
}

nyx::Texture::~Texture()
{
  cleanup();
}

void nyx::Texture::init()
{
  if (textureId == 0)
  {
    glGenTextures(1, &textureId);
    if (textureId == 0)
    {
      CHECK_GL_ERROR();
      return;
    }
    else
    {
      glBindTexture(GL_TEXTURE_2D, textureId);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glBindTexture(GL_TEXTURE_2D, NULL);

      NVWindowsLog("texture: initialized");
    }
  }
  else
  {
    NVWindowsLog("texture: already initialized");
  }
}

void nyx::Texture::cleanup()
{
  if (textureId != 0)
  {
    NVWindowsLog("texture: clean up");
    glDeleteTextures(1, &textureId);
  }
}

void nyx::Texture::update(
  CefRenderHandler::PaintElementType type,
  const CefRenderHandler::RectList& dirtyRects,
  const void* buffer,
  int width,
  int height
  )
{
  if (textureId && type != PET_POPUP)
  {
    if (transparent) glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureId);

    if (type == PET_VIEW)
    {
      int oldWidth = viewWidth;
      int oldHeight = viewHeight;
      viewWidth = width, viewHeight = height;

      if (oldWidth != viewWidth || oldHeight != viewHeight)
      {
        glPixelStorei(GL_UNPACK_ROW_LENGTH, viewWidth);
        glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
        glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, viewWidth, viewHeight, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, buffer);
      }
      else
      {
        // Update just the dirty rectangles.
        CefRenderHandler::RectList::const_iterator i = dirtyRects.begin();
        for (; i != dirtyRects.end(); ++i)
        {
          const CefRect& rect = *i;
          glPixelStorei(GL_UNPACK_ROW_LENGTH, viewWidth);
          glPixelStorei(GL_UNPACK_SKIP_PIXELS, rect.x);
          glPixelStorei(GL_UNPACK_SKIP_ROWS, rect.y);
          glTexSubImage2D(GL_TEXTURE_2D, 0, rect.x, rect.y, rect.width, rect.height, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, buffer);
        }
      }
    }
#if 0
    else if (
      type == PET_POPUP
      && m_popup_rect.width > 0
      && m_popup_rect.height > 0
      )
    {
      int w = width;
      int h = height;
      int skip_pixels = 0, x = m_popup_rect.x;
      int skip_rows = 0, y = m_popup_rect.y;

      // Adjust the popup to fit inside the view.
      if (x < 0) { skip_pixels = -x; x = 0; }
      if (y < 0) { skip_rows = -y; y = 0; }
      if ((x + w) > m_view_width)  { w -= x + w - m_view_width; }
      if ((y + h) > m_view_height)  { h -= y + h - m_view_height; }

      // Update the popup rectangle.
      glPixelStorei(GL_UNPACK_ROW_LENGTH, width);
      glPixelStorei(GL_UNPACK_SKIP_PIXELS, skip_pixels);
      glPixelStorei(GL_UNPACK_SKIP_ROWS, skip_rows);
      glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, w, h, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, buffer);
    }
#endif

    // Disable 2D textures.
    glBindTexture(GL_TEXTURE_2D, NULL);
    glDisable(GL_TEXTURE_2D);
    if (transparent) glDisable(GL_BLEND);

    CHECK_GL_ERROR();
  }
  else
  {
    NVWindowsLog("texture: rendering ignored (popup or not initialized)");
  }
}

