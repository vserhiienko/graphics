#include "pch.h"
#include "WebClientApp.h"
#include "WebClientWidget.h"

nyx::WebClientWidget::WebClientWidget(WebClientWidgetBrowserProvider *browserProvider, bool transparent)
: m_renderer(transparent)
, m_windowHandle(NULL)
, painting_popup_(false)
, render_task_pending_(false)
, current_drag_op_(DRAG_OPERATION_NONE)
, m_browserProvider(browserProvider)
{
  NVWindowsLog("creating web client widget");
}

nyx::WebClientWidget::~WebClientWidget()
{
  NVWindowsLog("destroying web client widget");
}

void nyx::WebClientWidget::setWindowHandle(HWND windowHandle)
{
  m_windowHandle = windowHandle;

  //drop_target_ = DropTarget::Create(this, m_windowHandle);
 // HRESULT register_res = RegisterDragDrop(m_windowHandle, drop_target_);
  //DCHECK_EQ(register_res, S_OK);
}

HWND nyx::WebClientWidget::getWindowHandle()
{
  return m_windowHandle;
}

void nyx::WebClientWidget::OnBeforeClose(CefRefPtr<CefBrowser> browser)
{
  NVWindowsLog("web client widget: before close");

  if (m_windowHandle)  RevokeDragDrop(m_windowHandle);
  if (drop_target_) drop_target_ = NULL;

  /*
  RevokeDragDrop(hWnd_);
  drop_target_ = NULL;
  DisableGL();
  ::DestroyWindow(hWnd_);
  */
}

bool nyx::WebClientWidget::GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
#if 0
  if (m_windowHandle)
  {
    RECT window_rect = { 0 };
    HWND root_window = GetAncestor(m_windowHandle, GA_ROOT);
    if (::GetWindowRect(root_window, &window_rect))
    {
      rect = CefRect(
        window_rect.left, window_rect.top,
        window_rect.right - window_rect.left,
        window_rect.bottom - window_rect.top
        );

      return true;
    }
  }
#endif

  return false;
}

bool nyx::WebClientWidget::GetViewRect(
  CefRefPtr<CefBrowser> browser,
  CefRect& rect
  )
{
  RECT clientRect;
  if (!::GetClientRect(m_windowHandle, &clientRect))
    return false;

  rect.x = rect.y = 0;
  rect.width = clientRect.right;
  rect.height = clientRect.bottom;

  return true;
}

bool nyx::WebClientWidget::GetScreenPoint(
  CefRefPtr<CefBrowser> browser, 
  int viewX, 
  int viewY,
  int& screenX, 
  int& screenY
  )
{
#if 0
  if (!::IsWindow(m_windowHandle))
    return false;

  // Convert the point from view coordinates to actual screen coordinates.
  POINT screen_pt = { viewX, viewY };
  ClientToScreen(m_windowHandle, &screen_pt);
  screenX = screen_pt.x;
  screenY = screen_pt.y;
  return true;
#else
  return false;
#endif
}

bool nyx::WebClientWidget::GetScreenInfo(CefRefPtr<CefBrowser> browser, CefScreenInfo& screen_info)
{
  return false;
}

void nyx::WebClientWidget::OnPopupShow(CefRefPtr<CefBrowser> browser, bool show)
{
  if (!show)
  {
    m_renderer.clearPopupRects();
    browser->GetHost()->Invalidate(PET_VIEW);
  }

  m_renderer.onPopupShow(browser, show);
}

void nyx::WebClientWidget::OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect)
{
  m_renderer.onPopupSize(browser, rect);
}

void nyx::WebClientWidget::render()
{
  CEF_REQUIRE_UI_THREAD();

  if (render_task_pending_)
    render_task_pending_ = false;

  m_renderer.render();
}

void nyx::WebClientWidget::OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height)
{
  if (painting_popup_)
  {
    m_renderer.onPaint(browser, type, dirtyRects, buffer, width, height);
  }
  else
  {
    m_renderer.onPaint(browser, type, dirtyRects, buffer, width, height);
    if (type == PET_VIEW && !m_renderer.popup_rect().IsEmpty())
    {
      painting_popup_ = true;
      browser->GetHost()->Invalidate(PET_POPUP);
      painting_popup_ = false;
    }

    //m_renderer.render();
  }
}

void nyx::WebClientWidget::OnCursorChange(CefRefPtr<CefBrowser> browser, CefCursorHandle cursor)
{
  if (!::IsWindow(m_windowHandle)) return;

  // Change the plugin window's cursor.
  SetClassLongPtr(m_windowHandle, GCLP_HCURSOR, static_cast<LONG>(reinterpret_cast<LONG_PTR>(cursor)));
  SetCursor(cursor);
}

bool nyx::WebClientWidget::StartDragging(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDragData> drag_data, CefRenderHandler::DragOperationsMask allowed_ops, int x, int y)
{
  if (!drop_target_) return false;
  current_drag_op_ = DRAG_OPERATION_NONE;
  CefBrowserHost::DragOperationsMask result = drop_target_->StartDragging(browser, drag_data, allowed_ops, x, y);
  current_drag_op_ = DRAG_OPERATION_NONE;
  POINT pt = {};
  GetCursorPos(&pt);
  ScreenToClient(m_windowHandle, &pt);
  browser->GetHost()->DragSourceEndedAt(pt.x, pt.y, result);
  browser->GetHost()->DragSourceSystemDragEnded();
  return true;
}

void nyx::WebClientWidget::UpdateDragCursor(CefRefPtr<CefBrowser> browser, CefRenderHandler::DragOperation operation)
{
  current_drag_op_ = operation;
}

CefBrowserHost::DragOperationsMask nyx::WebClientWidget::OnDragEnter(CefRefPtr<CefDragData> drag_data, CefMouseEvent ev, CefBrowserHost::DragOperationsMask effect)
{
  NVWindowsLog("web client widget: drag enter");
  m_browserProvider->GetBrowser()->GetHost()->DragTargetDragEnter(drag_data, ev, effect);
  m_browserProvider->GetBrowser()->GetHost()->DragTargetDragOver(ev, effect);
  return current_drag_op_;
}

CefBrowserHost::DragOperationsMask nyx::WebClientWidget::OnDragOver(CefMouseEvent ev, CefBrowserHost::DragOperationsMask effect)
{
  m_browserProvider->GetBrowser()->GetHost()->DragTargetDragOver(ev, effect);
  return current_drag_op_;
}

CefBrowserHost::DragOperationsMask nyx::WebClientWidget::OnDrop(CefMouseEvent ev, CefBrowserHost::DragOperationsMask effect)
{
  NVWindowsLog("web client widget: drag over");
  m_browserProvider->GetBrowser()->GetHost()->DragTargetDragOver(ev, effect);
  m_browserProvider->GetBrowser()->GetHost()->DragTargetDrop(ev);
  return current_drag_op_;
}

void nyx::WebClientWidget::OnDragLeave()
{
  NVWindowsLog("web client widget: drag leave");
  m_browserProvider->GetBrowser()->GetHost()->DragTargetDragLeave();
}
