#include "pch.h"
#include "WebClientApp.h"
#include "WebStringUtils.h"
#include "WebClientHandler.h"
#include "WebClientSwitches.h"
#include "WebClientRenderer.h"

int nyx::WebClientHandler::s_browser_count;

nyx::WebClientHandler::WebClientHandler()
{
  NVWindowsLog("creating client handler");

  m_browser_id = 0;
  m_is_closing = false;
  m_focus_on_editable_field = false;

  // Read command line settings.
  CefRefPtr<CefCommandLine> command_line = CefCommandLine::GetGlobalCommandLine();
  if (command_line->HasSwitch(cefclient::kUrl)) m_startup_url = command_line->GetSwitchValue(cefclient::kUrl);
  //if (m_startup_url.empty()) m_startup_url = "http://tympanus.net/Development/HoverEffectIdeas/";
  if (m_startup_url.empty()) m_startup_url = "http://tympanus.net/Development/SidebarTransitions/";
  m_mouse_cursor_change_disabled = command_line->HasSwitch(cefclient::kMouseCursorChangeDisabled);

  NVWindowsLog("startup url = \"%s\"", m_startup_url.c_str());
}

nyx::WebClientHandler::~WebClientHandler()
{
  NVWindowsLog("destroying client handler");
}

CefRefPtr<CefContextMenuHandler>  nyx::WebClientHandler::GetContextMenuHandler() { return this; }
CefRefPtr<CefDialogHandler>       nyx::WebClientHandler::GetDialogHandler() { return this; }
CefRefPtr<CefDisplayHandler>      nyx::WebClientHandler::GetDisplayHandler() { return this; }
CefRefPtr<CefDownloadHandler>     nyx::WebClientHandler::GetDownloadHandler() { return this; }
CefRefPtr<CefDragHandler>         nyx::WebClientHandler::GetDragHandler() { return this; }
CefRefPtr<CefGeolocationHandler>  nyx::WebClientHandler::GetGeolocationHandler() { return this; }
CefRefPtr<CefJSDialogHandler>     nyx::WebClientHandler::GetJSDialogHandler() { return this; }
CefRefPtr<CefKeyboardHandler>     nyx::WebClientHandler::GetKeyboardHandler() { return this; }
CefRefPtr<CefLifeSpanHandler>     nyx::WebClientHandler::GetLifeSpanHandler() { return this; }
CefRefPtr<CefLoadHandler>         nyx::WebClientHandler::GetLoadHandler() { return this; }
CefRefPtr<CefRenderHandler>       nyx::WebClientHandler::GetRenderHandler() { return this; }
CefRefPtr<CefRequestHandler>      nyx::WebClientHandler::GetRequestHandler() { return this; }

bool nyx::WebClientHandler::OnProcessMessageReceived(
  CefRefPtr<CefBrowser> browser,
  CefProcessId source_process,
  CefRefPtr<CefProcessMessage> message
  )
{
  CEF_REQUIRE_UI_THREAD();

  if (m_message_router->OnProcessMessageReceived(browser, source_process, message))
  {
    return true;
  }

  // Check for messages from the client renderer.
  std::string message_name = message->GetName();
  if (message_name == client_renderer::kFocusedNodeChangedMessage)
  {
    // A message is sent from ClientRenderDelegate to tell us whether the
    // currently focused DOM node is editable. Use of |focus_on_editable_field_|
    // is redundant with CefKeyEvent.focus_on_editable_field in OnPreKeyEvent
    // but is useful for demonstration purposes.
    m_focus_on_editable_field = message->GetArgumentList()->GetBool(0);
    return true;
  }

  return false;
}

// CefContextMenuHandler methods
void nyx::WebClientHandler::OnBeforeContextMenu(
  CefRefPtr<CefBrowser> browser,
  CefRefPtr<CefFrame> frame,
  CefRefPtr<CefContextMenuParams> params,
  CefRefPtr<CefMenuModel> model
  )
{
  CEF_REQUIRE_UI_THREAD();

  if ((params->GetTypeFlags() & (CM_TYPEFLAG_PAGE | CM_TYPEFLAG_FRAME)) != 0) {
    // Add a separator if the menu already has items.
    if (model->GetCount() > 0) model->AddSeparator();

    // Add DevTools items to all context menus.
    model->AddItem(MenuCommandId_ShowDevTools, "&Show DevTools");
    model->AddItem(MenuCommandId_CloseDevTools, "Close DevTools");

    // Test context menu features.
    //BuildTestMenu(model);
  }
}

bool nyx::WebClientHandler::OnContextMenuCommand(
  CefRefPtr<CefBrowser> browser,
  CefRefPtr<CefFrame> frame,
  CefRefPtr<CefContextMenuParams> params,
  int command_id,
  EventFlags event_flags
  )
{
  CEF_REQUIRE_UI_THREAD();

  switch (command_id) {
  case MenuCommandId_ShowDevTools:
    showDevTools(browser);
    return true;
  case MenuCommandId_CloseDevTools:
    closeDevTools(browser);
    return true;
    //default:  // Allow default handling, if any.
    //return ExecuteTestMenu(command_id);
  }

  return false;
}

// CefDialogHandler methods
bool nyx::WebClientHandler::OnFileDialog(
  CefRefPtr<CefBrowser> browser,
  FileDialogMode mode,
  const CefString& content,
  const CefString& default_file_name,
  const std::vector<CefString>& accept_types,
  CefRefPtr<CefFileDialogCallback> callback
  )
{
  CEF_REQUIRE_UI_THREAD();

  NVWindowsLog("file dialog");
  return false;
}

// CefDisplayHandler methods
void nyx::WebClientHandler::OnAddressChange(
  CefRefPtr<CefBrowser> browser,
  CefRefPtr<CefFrame> frame,
  const CefString& url
  )
{
  std::string address(url);

  NVWindowsLog(
    "address=\"%s\""
    , address.c_str()
    );
}

void nyx::WebClientHandler::OnTitleChange(
  CefRefPtr<CefBrowser> browser,
  const CefString& content
  )
{
  std::string title(content);

  NVWindowsLog(
    "title=\"%s\""
    , title.c_str()
    );
}

bool nyx::WebClientHandler::OnConsoleMessage(
  CefRefPtr<CefBrowser> browser,
  const CefString& message,
  const CefString& source,
  int line
  )
{
  CEF_REQUIRE_UI_THREAD();

  std::string msg(message), src(source);

  NVWindowsLog(
    "console: message=\"%s\" source=\"%s\""
    , msg.c_str()
    , src.c_str()
    );

  return false;
}

// CefDownloadHandler methods
void nyx::WebClientHandler::OnBeforeDownload(
  CefRefPtr<CefBrowser> browser,
  CefRefPtr<CefDownloadItem> download_item,
  const CefString& suggested_name,
  CefRefPtr<CefBeforeDownloadCallback> callback
  )
{
  CEF_REQUIRE_UI_THREAD();

  // Continue the download and show the "Save As" dialog.
  callback->Continue(getDownloadPath(suggested_name), true);
}

void nyx::WebClientHandler::OnDownloadUpdated(
  CefRefPtr<CefBrowser> browser,
  CefRefPtr<CefDownloadItem> download_item,
  CefRefPtr<CefDownloadItemCallback> callback
  )
{
  CEF_REQUIRE_UI_THREAD();

  std::string file(download_item->GetFullPath());

  if (download_item->IsInProgress())
  {
    NVWindowsLog(
      "downloading file%2d%%=\"%s\" (%u/%u)"
      , download_item->GetPercentComplete()
      , file.c_str()
      , (uint32_t)download_item->GetReceivedBytes()
      , (uint32_t)download_item->GetTotalBytes()
      );
  }
  else if (download_item->IsComplete())
  {
    NVWindowsLog(
      "downloaded file=\"%s\" (%u)"
      , file.c_str()
      , (uint32_t)download_item->GetTotalBytes()
      );

    //setLastDownloadFile(download_item->GetFullPath());
    sendNotification(NotificationType_DownloadComplete);
  }
}

// CefDragHandler methods
bool nyx::WebClientHandler::OnDragEnter(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDragData> dragData, CefDragHandler::DragOperationsMask mask)
{
  CEF_REQUIRE_UI_THREAD();

  // Forbid dragging of link URLs.
  if (mask & DRAG_OPERATION_LINK)
    return true;

  return false;
}

// CefGeolocationHandler methods
bool nyx::WebClientHandler::OnRequestGeolocationPermission(
  CefRefPtr<CefBrowser> browser,
  const CefString& requesting_url,
  int request_id,
  CefRefPtr<CefGeolocationCallback> callback
  )
{
  CEF_REQUIRE_UI_THREAD();

  // Allow geolocation access from all websites.
  callback->Continue(true);
  return true;
}

// CefJSDialogHandler methods
void nyx::WebClientHandler::OnResetDialogState(
  CefRefPtr<CefBrowser> browser
  )
{
  CEF_REQUIRE_UI_THREAD();
}

bool nyx::WebClientHandler::OnJSDialog(
  CefRefPtr<CefBrowser> browser,
  const CefString& origin_url,
  const CefString& accept_lang,
  JSDialogType dialog_type,
  const CefString& message_text,
  const CefString& default_prompt_text,
  CefRefPtr<CefJSDialogCallback> callback,
  bool& suppress_message
  )
{
  CEF_REQUIRE_UI_THREAD();
  return false;
}

bool nyx::WebClientHandler::OnBeforeUnloadDialog(
  CefRefPtr<CefBrowser> browser,
  const CefString& message_text,
  bool is_reload,
  CefRefPtr<CefJSDialogCallback> callback
  )
{
  CEF_REQUIRE_UI_THREAD();
  return false;
}

// CefKeyboardHandler methods
bool nyx::WebClientHandler::OnPreKeyEvent(
  CefRefPtr<CefBrowser> browser,
  const CefKeyEvent& event,
  CefEventHandle os_event,
  bool* is_keyboard_shortcut
  )
{
  CEF_REQUIRE_UI_THREAD();

  if (!event.focus_on_editable_field && event.windows_key_code == 0x20)
  {
    // Special handling for the space character when an input element does not
    // have focus. Handling the event in OnPreKeyEvent() keeps the event from
    // being processed in the renderer. If we instead handled the event in the
    // OnKeyEvent() method the space key would cause the window to scroll in
    // addition to showing the alert box.
    if (event.type == KEYEVENT_RAWKEYDOWN)
    {
      browser->GetMainFrame()->ExecuteJavaScript(
        "alert('You pressed the space bar!');", "", 0
        );
    }

    return true;
  }

  return false;
}

// CefLifeSpanHandler methods
bool nyx::WebClientHandler::DoClose(CefRefPtr<CefBrowser> browser)
{
  CEF_REQUIRE_UI_THREAD();

  // Closing the main window requires special handling. See the DoClose()
  // documentation in the CEF header for a detailed destription of this
  // process.
  if (getBrowserId() == browser->GetIdentifier())
  {
    base::AutoLock lock_scope(m_lock);
    // Set a flag to indicate that the window close should be allowed.
    m_is_closing = true;
  }

  // Allow the close. For windowed browsers this will result in the OS close
  // event being sent.
  return false;
}

void nyx::WebClientHandler::OnBeforeClose(CefRefPtr<CefBrowser> browser)
{
  CEF_REQUIRE_UI_THREAD();

  m_message_router->OnBeforeClose(browser);

  if (getBrowserId() == browser->GetIdentifier()) {
    {
      base::AutoLock lock_scope(m_lock);
      // Free the browser pointer so that the browser can be destroyed
      m_browser = NULL;
    }

    if (m_osr_handler.get()) {
      m_osr_handler->OnBeforeClose(browser);
      m_osr_handler = NULL;
    }
  }
  else if (browser->IsPopup())
  {
    // Remove from the browser popup list.
    BrowserList::iterator bit = m_popup_browsers.begin();
    for (; bit != m_popup_browsers.end(); ++bit)
    {
      if ((*bit)->IsSame(browser))
      {
        m_popup_browsers.erase(bit);
        break;
      }
    }
  }

  if (--s_browser_count == 0)
  {
    // All browser windows have closed.
    // Remove and delete message router handlers.
    MessageHandlerSet::const_iterator it = m_message_handler_set.begin();
    for (; it != m_message_handler_set.end(); ++it)
    {
      m_message_router->RemoveHandler(*(it));
      delete *(it);
    }

    m_message_handler_set.clear();
    m_message_router = NULL;

    NVWindowsLog(
      "all browsers were closed"
      );

    // Quit the application message loop.
    //AppQuitMessageLoop();
  }
}

void nyx::WebClientHandler::OnAfterCreated(CefRefPtr<CefBrowser> browser)
{
  CEF_REQUIRE_UI_THREAD();

  if (!m_message_router) {
    // Create the browser-side router for query handling.
    CefMessageRouterConfig config;
    m_message_router = CefMessageRouterBrowserSide::Create(config);

    // Register handlers with the router.
    createMessageHandlers(m_message_handler_set);
    MessageHandlerSet::const_iterator it = m_message_handler_set.begin();
    for (; it != m_message_handler_set.end(); ++it)
      m_message_router->AddHandler(*(it), false);
  }

  // Disable mouse cursor change if requested via the command-line flag.
  if (m_mouse_cursor_change_disabled)
    browser->GetHost()->SetMouseCursorChangeDisabled(true);

  if (!getBrowser())
  {
    base::AutoLock lock_scope(m_lock);
    // We need to keep the main child window, but not popup windows
    m_browser = browser;
    m_browser_id = browser->GetIdentifier();

    NVWindowsLog("web client handler: browser id = %d", m_browser_id);
  }
  else if (browser->IsPopup()) {
    // Add to the list of popup browsers.
    m_popup_browsers.push_back(browser);

    // Give focus to the popup browser. Perform asynchronously because the
    // parent window may attempt to keep focus after launching the popup.
    CefPostTask(TID_UI, base::Bind(&CefBrowserHost::SetFocus, browser->GetHost().get(), true));
  }

  s_browser_count++;
}

bool nyx::WebClientHandler::OnBeforePopup(
  CefRefPtr<CefBrowser> browser,
  CefRefPtr<CefFrame> frame,
  const CefString& target_url,
  const CefString& target_frame_name,
  const CefPopupFeatures& popupFeatures,
  CefWindowInfo& windowInfo,
  CefRefPtr<CefClient>& client,
  CefBrowserSettings& settings,
  bool* no_javascript_access
  )
{
  CEF_REQUIRE_IO_THREAD();

  if (browser->GetHost()->IsWindowRenderingDisabled())
  {
    NVWindowsLog(
      "web client handler: cancelling popup"
      );
    // Cancel popups in off-screen rendering mode.
    return true;
  }

  return false;
}

// CefLoadHandler methods
void nyx::WebClientHandler::OnLoadingStateChange(
  CefRefPtr<CefBrowser> browser,
  bool isLoading,
  bool canGoBack,
  bool canGoForward
  )
{
  CEF_REQUIRE_UI_THREAD();

  if (isLoading)
    NVWindowsLog(
    "web client handler: starting load"
    );
  else
    NVWindowsLog(
    "web client handler: loaded"
    );
}

void nyx::WebClientHandler::OnLoadError(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, ErrorCode errorCode, const CefString& errorText, const CefString& failedUrl)
{
  CEF_REQUIRE_UI_THREAD();

  // Don't display an error for downloaded files.
  if (errorCode == ERR_ABORTED)
  {
    NVWindowsLog(
      "web client handler: load error (aborted)"
      );

    return;
  }

  // Don't display an error for external protocols that we allow the OS to handle. See OnProtocolExecution().
  if (errorCode == ERR_UNKNOWN_URL_SCHEME)
  {
    std::string urlStr = frame->GetURL();
    if (urlStr.find("spotify:") == 0)
    {
      NVWindowsLog(
        "web client handler: load error (unknown scheme)"
        );

      return;
    }
  }

  // Display a load error message.
  std::stringstream ss;
  ss << "<html><body bgcolor=\"white\">"
    "<h3>Unexpected error</h3>"
    "<h2>Failed to load URL "
    << std::string(failedUrl)
    << " with error "
    << std::string(errorText)
    << " (" << errorCode
    << ").</h2></body></html>";
  frame->LoadStringW(ss.str(), failedUrl);

  NVWindowsLog(
    "web client handler: load error (unexpected, url=\"%s\", txt=\"%s\" err=\"%d\")"
    , std::string(failedUrl).c_str()
    , std::string(errorText).c_str()
    , errorCode
    );
}

// CefRequestHandler methods
bool nyx::WebClientHandler::OnBeforeBrowse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request, bool is_redirect)
{
  CEF_REQUIRE_UI_THREAD();
  m_message_router->OnBeforeBrowse(browser, frame);
  return false;
}

CefRefPtr<CefResourceHandler> nyx::WebClientHandler::GetResourceHandler(
  CefRefPtr<CefBrowser> browser,
  CefRefPtr<CefFrame> frame,
  CefRefPtr<CefRequest> request
  )
{
  CEF_REQUIRE_IO_THREAD();

  //std::string requestDump;
  //DumpRequestContents(request, requestDump);
  //NVWindowsLog("request dump:\n%s", requestDump.c_str());

  return NULL;
}

bool nyx::WebClientHandler::OnQuotaRequest(CefRefPtr<CefBrowser> browser, const CefString& origin_url, int64 new_size, CefRefPtr<CefQuotaCallback> callback)
{
  CEF_REQUIRE_IO_THREAD();

  static const int64 allow_mb = 20;  // 20mb.
  static const int64 max_size = 1024 * 1024 * allow_mb;  // 20mb.

  NVWindowsLog(
    "web client handler: requested quota = %.2fmb, approved: %s"
    , float(new_size) / (1024.0f * 1024.0f)
    , ((new_size <= max_size) ? "yes" : "no")
    );

  // Grant the quota request if the size is reasonable.
  callback->Continue(new_size <= max_size);
  return true;
}

void nyx::WebClientHandler::OnProtocolExecution(CefRefPtr<CefBrowser> browser, const CefString& url, bool& allow_os_execution)
{
  CEF_REQUIRE_UI_THREAD();

  std::string urlStr = url;

  // Allow OS execution of Spotify URIs.
  if (urlStr.find("spotify:") == 0) allow_os_execution = true;
}

void nyx::WebClientHandler::OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser, TerminationStatus status)
{
  CEF_REQUIRE_UI_THREAD();

  NVWindowsLog("web client handler: render process terminated");

  m_message_router->OnRenderProcessTerminated(browser);

  // Load the startup URL if that's not the website that we terminated on.
  CefRefPtr<CefFrame> frame = browser->GetMainFrame();
  std::string url = frame->GetURL();
  std::transform(url.begin(), url.end(), url.begin(), tolower);

  std::string startupURL = getStartupURL();
  if (
    startupURL != "chrome://crash"
    && !url.empty() 
    && url.find(startupURL) != 0
    ) 
  {
    frame->LoadURL(startupURL);
  }
}


// CefRenderHandler methods
bool nyx::WebClientHandler::GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return false;
  return m_osr_handler->GetRootScreenRect(browser, rect);
}

bool nyx::WebClientHandler::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return false;
  return m_osr_handler->GetViewRect(browser, rect);
}

bool nyx::WebClientHandler::GetScreenPoint(CefRefPtr<CefBrowser> browser, int viewX, int viewY, int& screenX, int& screenY)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return false;
  return m_osr_handler->GetScreenPoint(browser, viewX, viewY, screenX, screenY);
}

bool nyx::WebClientHandler::GetScreenInfo(CefRefPtr<CefBrowser> browser, CefScreenInfo& screen_info)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return false;
  return m_osr_handler->GetScreenInfo(browser, screen_info);
}

void nyx::WebClientHandler::OnPopupShow(CefRefPtr<CefBrowser> browser, bool show)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return;
  return m_osr_handler->OnPopupShow(browser, show);
}

void nyx::WebClientHandler::OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return;
  return m_osr_handler->OnPopupSize(browser, rect);
}

void nyx::WebClientHandler::OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return;
  //NVWindowsLog("web client handler: painting");
  m_osr_handler->OnPaint(browser, type, dirtyRects, buffer, width, height);
}

void nyx::WebClientHandler::OnCursorChange(CefRefPtr<CefBrowser> browser, CefCursorHandle cursor)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return;
  m_osr_handler->OnCursorChange(browser, cursor);
}

bool nyx::WebClientHandler::StartDragging(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDragData> drag_data, CefRenderHandler::DragOperationsMask allowed_ops, int x, int y)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return false;
  return m_osr_handler->StartDragging(browser, drag_data, allowed_ops, x, y);
}

void nyx::WebClientHandler::UpdateDragCursor(CefRefPtr<CefBrowser> browser, CefRenderHandler::DragOperation operation)
{
  CEF_REQUIRE_UI_THREAD();
  if (!m_osr_handler.get()) return;
  m_osr_handler->UpdateDragCursor(browser, operation);
}


void nyx::WebClientHandler::setRenderHandler(CefRefPtr<RenderHandler> handler)
{
  if (!CefCurrentlyOn(TID_UI))
  {
    NVWindowsLog(
      "web client handler: posting task on ui thread: "
      "setting render handler"
      );

    // Execute on the UI thread.
    CefPostTask(TID_UI, base::Bind(&WebClientHandler::setRenderHandler, this, handler));
    return;
  }

  m_osr_handler = handler;
}

CefRefPtr<nyx::WebClientHandler::RenderHandler> nyx::WebClientHandler::getRenderHandler() const
{
  CEF_REQUIRE_UI_THREAD();
  return m_osr_handler;
}

CefRefPtr<CefBrowser> nyx::WebClientHandler::getBrowser() const
{
  base::AutoLock lock_scope(m_lock);
  return m_browser;
}

int nyx::WebClientHandler::getBrowserId() const
{
  base::AutoLock lock_scope(m_lock);
  return m_browser_id;
}

// Request that all existing browser windows close.
void nyx::WebClientHandler::closeAllBrowsers(bool force_close)
{

  if (!CefCurrentlyOn(TID_UI))
  {
    NVWindowsLog(
      "web client handler: posting task on ui thread: "
      "closing all browsers"
      );

    // Execute on the UI thread.
    CefPostTask(TID_UI, base::Bind(&WebClientHandler::closeAllBrowsers, this, force_close));
    return;
  }

  NVWindowsLog("web client handler: closing all browsers");
  if (!m_popup_browsers.empty())
  {
    // Request that any popup browsers close.
    BrowserList::const_iterator it = m_popup_browsers.begin();
    for (; it != m_popup_browsers.end(); ++it)
      (*it)->GetHost()->CloseBrowser(force_close);
  }

  if (m_browser.get())
  {
    // Request that the main browser close.
    m_browser->GetHost()->CloseBrowser(force_close);
  }
}

// Returns true if the main browser window is currently closing. Used in
// combination with DoClose() and the OS close notification to properly handle
// 'onbeforeunload' JavaScript events during window close.
bool nyx::WebClientHandler::isClosing() const
{
  base::AutoLock lock_scope(m_lock);
  return m_is_closing;
}

void nyx::WebClientHandler::sendNotification(NotificationType type)
{
  switch (type)
  {
  case nyx::WebClientHandler::NotificationType_ConsoleMessage:
    NVWindowsLog("web client handler: send console message notification");
    break;
  case nyx::WebClientHandler::NotificationType_DownloadComplete:
    NVWindowsLog("web client handler: send download complete notification");
    break;
  case nyx::WebClientHandler::NotificationType_DownloadError:
    NVWindowsLog("web client handler: send download error notification");
    break;
  default:
    NVWindowsLog("web client handler: unrecognized notification type: 0x%0x", (uint32_t)type);
    break;
  }
}

void nyx::WebClientHandler::showDevTools(CefRefPtr<CefBrowser> browser)
{
  NVWindowsLog("web client handler: show devtools");

  /*
  CefWindowInfo windowInfo;
  CefBrowserSettings settings;
  windowInfo.SetAsPopup(browser->GetHost()->GetWindowHandle(), "DevTools");
  browser->GetHost()->ShowDevTools(windowInfo, this, settings);
  */
}

void nyx::WebClientHandler::closeDevTools(CefRefPtr<CefBrowser> browser)
{
  NVWindowsLog("web client handler: close devtools");

  /*
  browser->GetHost()->CloseDevTools();
  */
}

// Returns the startup URL.
std::string nyx::WebClientHandler::getStartupURL() const
{
  return m_startup_url;
}

void nyx::WebClientHandler::beginTracing()
{
  NVWindowsLog("begin tracing");
}

void nyx::WebClientHandler::endTracing()
{
  NVWindowsLog("end tracing");
}

bool nyx::WebClientHandler::save(const std::string& path, const std::string& data)
{
  FILE* f = NULL;
  errno_t err = fopen_s(&f, path.c_str(), "w");
  if (!f) return false;

  size_t total = 0;

  do
  {
    size_t write = fwrite(
      data.c_str() + total,
      1,
      data.size() - total,
      f
      );

    if (write == 0) break;
    total += write;

  } while (total < data.size());

  fclose(f);
  return true;
}

/*static*/ void nyx::WebClientHandler::createMessageHandlers(MessageHandlerSet& handlers)
{
  NVWindowsLog("creating message handlers");
}

// Returns the full download path for the specified file, or an empty path to
// use the default temp directory.
std::string nyx::WebClientHandler::getDownloadPath(const std::string& file_name)
{
  char szFolderPath[MAX_PATH];
  std::string path;

  // Save the file in the user's "My Documents" folder.
  if (SUCCEEDED(SHGetFolderPathA(
    NULL,
    CSIDL_PERSONAL | CSIDL_FLAG_CREATE,
    NULL,
    0,
    szFolderPath
    )))
  {
    path = CefString(szFolderPath);
    path += "\\" + file_name;
  }

  NVWindowsLog("download path = \"%s\"", path.c_str());
  return path;
}