#pragma once
#include "pch.h"

namespace nyx
{
  class WebClientWidgetBrowserProvider
  {
  public: virtual CefRefPtr<CefBrowser> GetBrowser() = 0;
  protected: virtual ~WebClientWidgetBrowserProvider() {}
  };
}