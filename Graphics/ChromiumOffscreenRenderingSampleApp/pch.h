#pragma once

#define VC_EXTRALEAN 1
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

#include <map>
#include <set>
#include <list>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>

#include <direct.h>
#include <sstream>
#include <commdlg.h>
#include <shellapi.h>
#include <shlobj.h> 
#include <atlcomcli.h>
#include <objidl.h>

#include <NV/NvLogs.h>
#include <NV/NvMath.h>
#include <NvFoundation.h>
#include <NV/NvStopWatch.h>
#include <NvUI/NvTweakBar.h>
#include <NV/NvPlatformGL.h>
#include <NvGLUtils/NvImage.h>
#include <NvAppBase/NvSampleApp.h>
#include <NvGLUtils/NvGLSLProgram.h>
#include <NvAssetLoader/NvAssetLoader.h>
#include <NvAppBase/NvFramerateCounter.h>
#include <NvAppBase/NvInputTransformer.h>

#include <cef_app.h>
#include <cef_browser.h>
#include <cef_client.h>
#include <cef_sandbox_win.h>
#include <cef_frame.h>
#include <cef_cookie.h>
#include <cef_task.h>
#include <cef_v8.h>
#include <cef_process_message.h>
#include <cef_stream.h>
#include <cef_url.h>
#include <cef_trace.h>
#include <cef_path_util.h>
#include <cef_process_util.h>

#include <base/cef_lock.h>
#include <base/cef_bind.h>
#include <wrapper/cef_helpers.h>
#include <wrapper/cef_closure_task.h>
#include <wrapper/cef_message_router.h>
#include <wrapper/cef_stream_resource_handler.h>

