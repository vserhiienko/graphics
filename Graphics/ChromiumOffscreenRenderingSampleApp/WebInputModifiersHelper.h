#pragma once
#include "pch.h"

namespace nyx
{
  int GetCefKeyboardModifiers(WPARAM, LPARAM);
  int GetCefMouseModifiers(WPARAM);
  bool GetIsKeyDown(WPARAM);
}

