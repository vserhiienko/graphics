#include "pch.h"
#include "WebClientApp.h"
#include "WebClientRenderer.h"

const char kFocusedNodeChangedMessage[] = "ClientRenderer.FocusedNodeChanged";

nyx::WebRenderer::WebRenderer(bool transparent)
{
  NVWindowsLog("creating web renderer");

  m_transparent = transparent;
  m_initialized = false;
  m_view_width = 0;
  m_view_height = 0;
  m_spin_x = 0;
  m_spin_y = 0;

  //m_texture_id = 0;
}

nyx::WebRenderer::~WebRenderer()
{
  NVWindowsLog("destroying web renderer");
  cleanup();
}

void nyx::WebRenderer::initialize()
{
  if (m_initialized) return;

  /*if (!CefCurrentlyOn(TID_UI))
  {
    NVWindowsLog(
      "web renderer: "
      "posting to ui thread: "
      "initialize"
      );

    CefPostTask(TID_UI, base::Bind(
      &WebRenderer::initialize, this
      ));

    return;
  }*/

  NVWindowsLog("initializing web renderer");

  m_texture.init();

#if 0
  glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  // Necessary for non-power-of-2 textures to render correctly.
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  // Create the texture.
  glGenTextures(1, &m_texture_id);
  //DCHECK_NE(m_texture_id, 0U);

  if (m_texture_id == 0)
  {
    CHECK_GL_ERROR();
    return;
  }

  glBindTexture(GL_TEXTURE_2D, m_texture_id);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  //glBindTexture(GL_TEXTURE_2D, 0);

  CHECK_GL_ERROR();
  NVWindowsLog("web renderer: initialized");

  m_initialized = true;
#endif

  m_initialized = m_texture.textureId != NULL;
}

void nyx::WebRenderer::cleanup()
{
  /*if (!CefCurrentlyOn(TID_UI))
  {
    NVWindowsLog(
      "web renderer: "
      "posting to ui thread: "
      "cleanup"
      );

    CefPostTask(TID_UI, base::Bind(
      &WebRenderer::cleanup, this
      ));

    return;
  }*/

  m_texture.cleanup();
  NVWindowsLog("web renderer: cleanup");
  //if (m_texture_id != 0) glDeleteTextures(1, &m_texture_id);
}

void nyx::WebRenderer::render()
{
  CEF_REQUIRE_RENDERER_THREAD();

#if 0
  if  ((m_texture_id == 0)
    || (m_initialized == false)
    || (m_view_width == 0)
    || (m_view_height == 0)) return;

  struct
  {
    float tu, tv;
    float x, y, z;
  }
  static vertices[] =
  {
    { 0.0f, 1.0f, -1.0f, -1.0f, 0.0f },
    { 1.0f, 1.0f, 1.0f, -1.0f, 0.0f },
    { 1.0f, 0.0f, 1.0f, 1.0f, 0.0f },
    { 0.0f, 0.0f, -1.0f, 1.0f, 0.0f }
  };

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // Match GL units to screen coordinates.
  //glViewport(0, 0, m_view_width, m_view_height);
  glViewport(0, 0, m_view_width, m_view_height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, m_view_width, 0, m_view_height, 0.1, 100.0);

  // Draw the background gradient.
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glBegin(GL_QUADS);
  glColor4f(1.0, 0.0, 0.0, 1.0);  // red
  glVertex2f(-1.0, -1.0);
  glVertex2f(1.0, -1.0);
  glColor4f(0.0, 0.0, 1.0, 1.0);  // blue
  glVertex2f(1.0, 1.0);
  glVertex2f(-1.0, 1.0);
  glEnd();
  glPopAttrib();

  // Rotate the view based on the mouse spin.
  if (m_spin_x != 0) glRotatef(-m_spin_x, 1.0f, 0.0f, 0.0f);
  if (m_spin_y != 0) glRotatef(-m_spin_y, 0.0f, 1.0f, 0.0f);

  if (m_transparent) {
    // Alpha blending style. Texture values have premultiplied alpha.
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    // Enable alpha blending.
    glEnable(GL_BLEND);
  }

  // Enable 2D textures.
  glEnable(GL_TEXTURE_2D);

  // Draw the facets with the texture.
  //DCHECK_NE(m_texture_id, 0U);
  glBindTexture(GL_TEXTURE_2D, m_texture_id);
  glInterleavedArrays(GL_T2F_V3F, 0, vertices);
  glDrawArrays(GL_QUADS, 0, 4);

  // Disable 2D textures.
  glDisable(GL_TEXTURE_2D);

  if (m_transparent)
  {
    // Disable alpha blending.
    glDisable(GL_BLEND);
  }

#endif

  CHECK_GL_ERROR();
}

void nyx::WebRenderer::onPaint(
  CefRefPtr<CefBrowser> browser,
  CefRenderHandler::PaintElementType type,
  const CefRenderHandler::RectList& dirtyRects,
  const void* buffer,
  int width,
  int height
  )
{
  //CEF_REQUIRE_RENDERER_THREAD();

  //if (!CefCurrentlyOn(TID_UI))
  //{
  //  // Execute on the UI thread.
  //  CefPostTask(TID_UI, base::Bind(
  //    &WebRenderer::onPaint, this,
  //    browser,
  //    type,
  //    dirtyRects,
  //    buffer,
  //    width,
  //    height
  //    ));

  //  return;
  //}

  if (m_initialized == false) initialize();

  /*if ((m_view_width == 0)
    || (m_view_height == 0)
    || (m_texture_id == 0)
    || (m_initialized == false)) return;*/

  m_texture.update(type, dirtyRects, buffer, width, height);
}

void nyx::WebRenderer::onPopupShow(CefRefPtr<CefBrowser> browser, bool show)
{
  if (!show)
  {
    NVWindowsLog("web renderer popup show");
    // Clear the popup rectangle.
    clearPopupRects();
  }
}

void nyx::WebRenderer::onPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect)
{
  if (rect.width <= 0 || rect.height <= 0) return;
  NVWindowsLog("web renderer popup size");

  m_original_popup_rect = rect;
  m_popup_rect = getPopupRectInWebView(m_original_popup_rect);
}

void nyx::WebRenderer::setSpin(float sx, float sy)
{
  m_spin_x = sx;
  m_spin_y = sy;
}

void nyx::WebRenderer::incrementSpin(float dsx, float dsy)
{
  m_spin_x -= dsx;
  m_spin_y -= dsy;
}

bool nyx::WebRenderer::isTransparent()
{
  return m_transparent;
}

void nyx::WebRenderer::setIsTransparent(bool value)
{
  m_transparent = value;
}

int nyx::WebRenderer::getViewWidth()
{
  return m_view_width;
}

int nyx::WebRenderer::getViewHeight()
{
  return m_view_height;
}

const CefRect& nyx::WebRenderer::popup_rect() const
{
  return m_popup_rect;
}

const CefRect& nyx::WebRenderer::original_popup_rect() const
{
  return m_original_popup_rect;
}

CefRect nyx::WebRenderer::getPopupRectInWebView(const CefRect& original_rect)
{
  CefRect rc(original_rect);

  // if x or y are negative, move them to 0.
  if (rc.x < 0) rc.x = 0;
  if (rc.y < 0) rc.y = 0;
  // if popup goes outside the view, try to reposition origin
  if (rc.x + rc.width > m_view_width) rc.x = m_view_width - rc.width;
  if (rc.y + rc.height > m_view_height) rc.y = m_view_height - rc.height;
  // if x or y became negative, move them to 0 again.
  if (rc.x < 0) rc.x = 0;
  if (rc.y < 0) rc.y = 0;

  return rc;
}

void nyx::WebRenderer::clearPopupRects()
{
  NVWindowsLog("web renderer clear popups");
  m_popup_rect.Set(0, 0, 0, 0);
  m_original_popup_rect.Set(0, 0, 0, 0);
}

namespace client_renderer
{
  using namespace nyx;
  const char kFocusedNodeChangedMessage[] = "ClientRenderer.FocusedNodeChanged";

  class ClientRenderDelegate
    : public WebClientRendererDelegate
  {
  public:
    ClientRenderDelegate()
      : last_node_is_editable_(false)
    {
      NVWindowsLog("creating client render delegate");
    }

    virtual void OnWebKitInitialized(
      CefRefPtr<WebClientApp> app
      ) override
    {
      NVWindowsLog("client render delegate: webkit initialized");

      // Create the renderer-side router for query handling.
      CefMessageRouterConfig config;
      message_router_ = CefMessageRouterRendererSide::Create(config);
    }

    virtual void OnContextCreated(
      CefRefPtr<WebClientApp> app,
      CefRefPtr<CefBrowser> browser,
      CefRefPtr<CefFrame> frame,
      CefRefPtr<CefV8Context> context
      ) override
    {
      NVWindowsLog("client render delegate: context created");

      message_router_->OnContextCreated(
        browser, frame, context
        );
    }

    virtual void OnContextReleased(
      CefRefPtr<WebClientApp> app,
      CefRefPtr<CefBrowser> browser,
      CefRefPtr<CefFrame> frame,
      CefRefPtr<CefV8Context> context
      ) override
    {
      NVWindowsLog("client render delegate: context released");

      message_router_->OnContextReleased(
        browser, frame, context
        );
    }

    virtual void OnFocusedNodeChanged(
      CefRefPtr<WebClientApp> app,
      CefRefPtr<CefBrowser> browser,
      CefRefPtr<CefFrame> frame,
      CefRefPtr<CefDOMNode> node
      ) override
    {
      NVWindowsLog("client render delegate: focused node changed");

      bool is_editable = (node.get() && node->IsEditable());
      if (is_editable != last_node_is_editable_)
      {
        // Notify the browser of the change in focused element type.
        last_node_is_editable_ = is_editable;
        CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create(kFocusedNodeChangedMessage);
        message->GetArgumentList()->SetBool(0, is_editable);
        browser->SendProcessMessage(PID_BROWSER, message);
      }
    }

    virtual bool OnProcessMessageReceived(
      CefRefPtr<WebClientApp> app,
      CefRefPtr<CefBrowser> browser,
      CefProcessId source_process,
      CefRefPtr<CefProcessMessage> message
      ) override
    {
      return message_router_->OnProcessMessageReceived(
        browser, source_process, message
        );
    }

  private:

    bool last_node_is_editable_;
    CefRefPtr<CefMessageRouterRendererSide> message_router_; // Handles the renderer side of query routing.

    IMPLEMENT_REFCOUNTING(ClientRenderDelegate);
  };

  void CreateRenderDelegates(WebClientRendererDelegate::Set& delegates)
  {
    NVWindowsLog("creating client delegates");
    delegates.insert(new ClientRenderDelegate);
  }

}