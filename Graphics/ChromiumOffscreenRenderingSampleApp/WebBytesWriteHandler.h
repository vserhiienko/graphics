#pragma once
#include "pch.h"

namespace nyx
{
  class BytesWriteHandler 
    : public CefWriteHandler 
  {
  public:
    explicit BytesWriteHandler(size_t grow);
    virtual ~BytesWriteHandler();

    virtual size_t Write(const void* ptr, size_t size, size_t n) override;
    virtual int Seek(int64 offset, int whence) override;
    virtual int64 Tell() override;
    virtual int Flush() override;
    virtual bool MayBlock() override { return false; }

    void* GetData() { return data_; }
    int64 GetDataSize() { return offset_; }

  protected:
    size_t Grow(size_t size);

    size_t grow_;
    void* data_;
    int64 datasize_;
    int64 offset_;

    base::Lock lock_;

    IMPLEMENT_REFCOUNTING(BytesWriteHandler);
  };
}
