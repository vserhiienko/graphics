
#include "pch.h"
#include "keel.kinect.fusion.parameters.h"

Keel::KinectFusionParams::KinectFusionParams()
: m_bPauseIntegration(false)
, m_bNearMode(true)
, m_depthImageResolution(NUI_IMAGE_RESOLUTION_640x480)
, m_colorImageResolution(NUI_IMAGE_RESOLUTION_640x480)
, m_bAutoResetReconstructionWhenLost(false)
, m_bAutoResetReconstructionOnTimeout(false) // We now try to find the camera pose, however, setting this false will no longer auto reset on .xed file playback
, m_bAutoFindCameraPoseWhenLost(true)
, m_fMinDepthThreshold(NUI_FUSION_DEFAULT_MINIMUM_DEPTH)
, m_fMaxDepthThreshold(NUI_FUSION_DEFAULT_MAXIMUM_DEPTH)
, m_bMirrorDepthFrame(false)
, m_cMaxIntegrationWeight(555)
//, m_cMaxIntegrationWeight(950)
//, m_cMaxIntegrationWeight(NUI_FUSION_DEFAULT_INTEGRATION_WEIGHT)
, m_bDisplaySurfaceNormals(true)
//, m_bDisplaySurfaceNormals(false)
, m_bCaptureColor(true)
//, m_bCaptureColor(false)
, m_cColorIntegrationInterval(3)
, m_bTranslateResetPoseByMinDepthThreshold(true)
, m_saveMeshType(Fbx)
//, m_saveMeshType(Stl)
, m_cDeltaFromReferenceFrameCalculationInterval(2)
, m_cMinSuccessfulTrackingFramesForCameraPoseFinder(45) // only update the camera pose finder initially after 45 successful frames (1.5s)
, m_cMinSuccessfulTrackingFramesForCameraPoseFinderAfterFailure(200) // resume integration following 200 successful frames after tracking failure (~7s)
, m_cMaxCameraPoseFinderPoseHistory(NUI_FUSION_CAMERA_POSE_FINDER_DEFAULT_POSE_HISTORY_COUNT)
, m_cCameraPoseFinderFeatureSampleLocationsPerFrame(NUI_FUSION_CAMERA_POSE_FINDER_DEFAULT_FEATURE_LOCATIONS_PER_FRAME_COUNT)
, m_fMaxCameraPoseFinderDepthThreshold(NUI_FUSION_CAMERA_POSE_FINDER_DEFAULT_MAX_DEPTH_THRESHOLD)
, m_fCameraPoseFinderDistanceThresholdReject(1.0f) // a value of 1.0 means no rejection
, m_fCameraPoseFinderDistanceThresholdAccept(0.1f)
, m_cMaxCameraPoseFinderPoseTests(5)
, m_cCameraPoseFinderProcessFrameCalculationInterval(5)
, m_fMaxAlignToReconstructionEnergyForSuccess(0.27f)
, m_fMinAlignToReconstructionEnergyForSuccess(0.005f)
, m_fMaxAlignPointCloudsEnergyForSuccess(0.006f)
, m_fMinAlignPointCloudsEnergyForSuccess(0.0f)
, m_cSmoothingKernelWidth(1) // 0=just copy, 1=3x3, 2=5x5, 3=7x7, here we create a 3x3 kernel
, m_fSmoothingDistanceThreshold(0.04f) // 4cm, could use up to around 0.1f
, m_cAlignPointCloudsImageDownsampleFactor(4) // 1 = no down sample (process at , m_depthImageResolution) 2=x/2,y/2, 4=x/4,y/4
, m_fMaxTranslationDelta(0.3f) // 0.15 - 0.3m per frame typical
, m_fMaxRotationDelta(20.0f) // 10-20 degrees per frame typical
{
	// Get the depth frame size from the NUI_IMAGE_RESOLUTION enum.
	// You can use NUI_IMAGE_RESOLUTION_640x480 or NUI_IMAGE_RESOLUTION_320x240 in this sample.
	// Smaller resolutions will be faster in per-frame computations, but show less detail in reconstructions.
	DWORD width = 0, height = 0;
	NuiImageResolutionToSize(m_depthImageResolution, width, height);
	m_cDepthWidth = width;
	m_cDepthHeight = height;
	m_cDepthImagePixels = m_cDepthWidth * m_cDepthHeight;

	NuiImageResolutionToSize(m_colorImageResolution, width, height);
	m_cColorWidth = width;
	m_cColorHeight = height;
	m_cColorImagePixels = m_cColorWidth*m_cColorHeight;

	// Define a cubic Kinect Fusion reconstruction volume, with the sensor at the center of the
	// front face and the volume directly in front of sensor.

	//m_reconstructionParams.voxelsPerMeter = 512; // 1000mm / 256vpm = ~3.9mm/voxel
	m_reconstructionParams.voxelsPerMeter = 512; // 1000mm / 256vpm = ~3.9mm/voxel
	//m_reconstructionParams.voxelsPerMeter = 768; // 1000mm / 256vpm = ~3.9mm/voxel
	//m_reconstructionParams.voxelsPerMeter = 256; // 1000mm / 256vpm = ~3.9mm/voxel
	//m_reconstructionParams.voxelsPerMeter = 128; // 1000mm / 256vpm = ~3.9mm/voxel

	m_reconstructionParams.voxelCountX = 512; // 512 / 256vpm = 2m wide reconstruction
	m_reconstructionParams.voxelCountY = 512; // Memory = 512*384*512 * 4bytes per voxel
	//m_reconstructionParams.voxelCountY = 384; // Memory = 512*384*512 * 4bytes per voxel
	m_reconstructionParams.voxelCountZ = 512; // This will require a GPU with at least 512MB

	// This parameter sets whether GPU or CPU processing is used. Note that the CPU will likely be 
	// too slow for real-time processing.
	m_processorType = NUI_FUSION_RECONSTRUCTION_PROCESSOR_TYPE_AMP;

	// If GPU processing is selected, we can choose the index of the device we would like to
	// use for processing by setting this zero-based index parameter. Note that setting -1 will cause
	// automatic selection of the most suitable device (specifically the DirectX11 compatible device 
	// with largest memory), which is useful in systems with multiple GPUs when only one reconstruction
	// volume is required. Note that the automatic choice will not load balance across multiple 
	// GPUs, hence users should manually select GPU indices when multiple reconstruction volumes 
	// are required, each on a separate device.
	m_deviceIndex = -1; // automatically choose device index for processing
}

bool Keel::KinectFusionParams::VolumeChanged(const KinectFusionParams& params)
{
	return
		m_reconstructionParams.voxelCountX != params.m_reconstructionParams.voxelCountX ||
		m_reconstructionParams.voxelCountY != params.m_reconstructionParams.voxelCountY ||
		m_reconstructionParams.voxelCountZ != params.m_reconstructionParams.voxelCountZ ||
		m_reconstructionParams.voxelsPerMeter != params.m_reconstructionParams.voxelsPerMeter ||
		m_processorType != params.m_processorType ||
		m_deviceIndex != params.m_deviceIndex;
}