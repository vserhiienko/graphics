#include "pch.h"
#include "keel.kinect.fusion.processor.h"

/// <summary>
/// Constructor.
/// </summary>
Keel::KinectFusionProcessor::Frame::Frame()
	: m_bIntegrationResumed(false)
	, m_pReconstructionRGBX(nullptr)
	, m_pDepthRGBX(nullptr)
	, m_pTrackingDataRGBX(nullptr)
	, m_cbImageSize(0)
	, m_fFramesPerSecond(0)
	, m_bColorCaptured(false)
	, m_deviceMemory(0)
{
	ZeroMemory(m_statusMessage, sizeof(m_statusMessage));
}

/// <summary>
/// Destructor.
/// </summary>
Keel::KinectFusionProcessor::Frame::~Frame()
{
	ZeroMemory(m_statusMessage, sizeof(m_statusMessage));
	FreeBuffers();
}

/// <summary>
/// Initializes each of the frame buffers to the given image size.
/// </summary>
/// <param name="cImageSize">Number of pixels to allocate in each frame buffer.</param>
HRESULT Keel::KinectFusionProcessor::Frame::Initialize(int cImageSize)
{
	HRESULT hr = S_OK;

	ZeroMemory(m_statusMessage, sizeof(m_statusMessage));

	ULONG cbImageSize = cImageSize * KinectFusionParams::BytesPerPixel;

	if (m_cbImageSize != cbImageSize)
	{
		FreeBuffers();

		m_cbImageSize = cbImageSize;
		m_pReconstructionRGBX = new(std::nothrow) BYTE[m_cbImageSize];
		m_pDepthRGBX = new(std::nothrow) BYTE[m_cbImageSize];
		m_pTrackingDataRGBX = new(std::nothrow) BYTE[m_cbImageSize];

		if (nullptr != m_pReconstructionRGBX ||
			nullptr != m_pDepthRGBX ||
			nullptr != m_pTrackingDataRGBX)
		{
			ZeroMemory(m_pReconstructionRGBX, m_cbImageSize);
			ZeroMemory(m_pDepthRGBX, m_cbImageSize);
			ZeroMemory(m_pTrackingDataRGBX, m_cbImageSize);
		}
		else
		{
			FreeBuffers();
			hr = E_OUTOFMEMORY;
		}
	}

	return hr;
}

/// <summary>
/// Sets the status message for the frame.
/// </summary>
/// <param name="szMessage">The status message.</param>
void Keel::KinectFusionProcessor::Frame::SetStatusMessage(const WCHAR* szMessage)
{
	StringCchCopy(m_statusMessage, ARRAYSIZE(m_statusMessage), szMessage);
}

/// <summary>
/// Frees the frame buffers.
/// </summary>
void Keel::KinectFusionProcessor::Frame::FreeBuffers()
{
	SAFE_DELETE_ARRAY(m_pReconstructionRGBX);
	SAFE_DELETE_ARRAY(m_pDepthRGBX);
	SAFE_DELETE_ARRAY(m_pTrackingDataRGBX);

	m_cbImageSize = 0;
}
