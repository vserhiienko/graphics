#include "pch.h"
#include "keel.texture.resources.h"

Keel::KinectFusionOffscreenTexture::KinectFusionOffscreenTexture()
{
	dx::zeroMemory(m_d3dReadResource);

	m_d3dResourceDesc.MiscFlags = 0;
	m_d3dResourceDesc.MipLevels = 1;
	m_d3dResourceDesc.ArraySize = 1;
	m_d3dResourceDesc.SampleDesc.Count = 1;
	m_d3dResourceDesc.SampleDesc.Quality = 0;
	m_d3dResourceDesc.Usage = D3D11_USAGE_DYNAMIC;
	m_d3dResourceDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	m_d3dResourceDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
}

HRESULT Keel::KinectFusionOffscreenTexture::DiscardDeviceResources()
{
	m_d3dReadResource.Read = nullptr;
	m_d3dReadResource.Rsrc = nullptr;
	return S_OK;
}

HRESULT Keel::KinectFusionOffscreenTexture::CreateDeviceResources(
	int iWidth,
	int iHeight,
	int iStride,
	DXGI_FORMAT eFormat
	)
{
	HRESULT hr = S_OK;
	auto trace = Dove::Engine::CreateAirForCurrentThread();

	hr = CreateWindowSizeDependentResources(
		iWidth, iHeight, iStride, eFormat,
		true // Device was changed
		);

	return hr;
}

HRESULT Keel::KinectFusionOffscreenTexture::CreateWindowSizeDependentResources(
	int iWidth,
	int iHeight,
	int iStride,
	DXGI_FORMAT eFormat,
	bool bForceRecreation
	)
{
	HRESULT hr = S_OK;

	// Resources are already created
	// and its dimensions remain the same
	if (m_d3dReadResource.Rsrc.Get()
		&& m_d3dResourceDesc.Width == iWidth
		&& m_d3dResourceDesc.Height == iHeight
		&& m_d3dResourceDesc.Format == eFormat
		)
	{
		// And there device was not changed
		if (!bForceRecreation)
		{
			return S_OK;
		}
	}

	// Otherwise create texture resource

	auto trace = Dove::Engine::CreateAirForCurrentThread();
	auto d3d = &Keel::Oasis::GetForCurrentThread()->Context.Engine->Graphics.d3d;

	m_d3dResourceDesc.Width = iWidth;
	m_d3dResourceDesc.Height = iHeight;
	m_d3dResourceDesc.Format = eFormat;
	m_iExpectedResourceSize = iWidth * iHeight * iStride;

	// Add a check if provided format corresponds to a stride
	// For now, it always does

	hr = d3d->Device->CreateTexture2D(
		&m_d3dResourceDesc, nullptr,
		m_d3dReadResource.Rsrc.ReleaseAndGetAddressOf()
		);

	if (FAILED(hr))
	{
		_com_error comError(hr);
		// Print error for the user
		return hr;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceDesc;
	dx::zeroMemory(shaderResourceDesc);
	shaderResourceDesc.Format = m_d3dResourceDesc.Format;
	shaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceDesc.Texture2D.MipLevels = m_d3dResourceDesc.MipLevels;
	shaderResourceDesc.Texture2D.MostDetailedMip = 0;

	hr = d3d->Device->CreateShaderResourceView(
		m_d3dReadResource.Rsrc.Get(),
		&shaderResourceDesc,
		m_d3dReadResource.Read.ReleaseAndGetAddressOf()
		);

	if (FAILED(hr))
	{
		_com_error comError(hr);
		// Print error for the user
		return hr;
	}

	FLOAT
		w = d3d->ActualRenderTargetSize.Width * 0.5f, 
		h = d3d->ActualRenderTargetSize.Height * 0.5f;

	Vertex squareVertices[4] =
	{
		{ dx::Vector4(-w, +h, 0, 1), dx::Vector3(0, 0, -1), dx::Vector2(0, 0) },
		{ dx::Vector4(+w, +h, 0, 1), dx::Vector3(0, 0, -1), dx::Vector2(1, 0) },
		{ dx::Vector4(+w, -h, 0, 1), dx::Vector3(0, 0, -1), dx::Vector2(1, 1) },
		{ dx::Vector4(-w, -h, 0, 1), dx::Vector3(0, 0, -1), dx::Vector2(0, 1) },
	};
	IndexUnit squareIndices[6] =
	{
		0, 1, 2,
		0, 2, 3,
	};

	hr = Assist::CreateVertexBuffer(
		d3d->Device.Get(), 
		ARRAYSIZE(squareVertices), 
		squareVertices,
		Vb.ReleaseAndGetAddressOf()
		);
	if (FAILED(hr))
	{
		_com_error comError(hr);
		// Print error for the user
		return hr;
	}

	hr = Assist::CreateIndexBuffer(
		d3d->Device.Get(), 
		ARRAYSIZE(squareIndices), 
		squareIndices,
		Ib.ReleaseAndGetAddressOf()
		);
	if (FAILED(hr))
	{
		_com_error comError(hr);
		// Print error for the user
		return hr;
	}

	return S_OK;
}

HRESULT Keel::KinectFusionOffscreenTexture::UpdateResourceContent(
	byte pBytes[],
	int iBufferSize
	)
{
	//auto spTrace = Dove::Engine::CreateAirForCurrentThread();
	static auto &x = Keel::Oasis::GetForCurrentThread()->Context;

	D3D11_MAPPED_SUBRESOURCE mapped_subresource;
	if (SUCCEEDED(x.Engine->Graphics.d3d.Context->Map(
		m_d3dReadResource.Rsrc.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0,
		&mapped_subresource
		)))
	{
		assert(iBufferSize == mapped_subresource.DepthPitch);
		memcpy(mapped_subresource.pData, pBytes, mapped_subresource.DepthPitch);
		x.Engine->Graphics.d3d.Context->Unmap(m_d3dReadResource.Rsrc.Get(), 0);

		// Texture resource was updated
		return S_OK;
	}
	else
	{
		// Failed to update texture resource
		return E_FAIL;
	}
}

HRESULT Keel::KinectFusionOffscreenTexture::UpdateResourceContent(
	std::shared_ptr<byte[]> spBytes,
	int iBufferSize
	)
{
	//auto spTrace = Dove::Engine::CreateAirForCurrentThread();
	static auto &x = Keel::Oasis::GetForCurrentThread()->Context;

	D3D11_MAPPED_SUBRESOURCE mapped_subresource;
	if (SUCCEEDED(x.Engine->Graphics.d3d.Context->Map(
		m_d3dReadResource.Rsrc.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0,
		&mapped_subresource
		)))
	{
		assert(iBufferSize == mapped_subresource.DepthPitch);
		memcpy(mapped_subresource.pData, spBytes.get(), mapped_subresource.DepthPitch);
		x.Engine->Graphics.d3d.Context->Unmap(m_d3dReadResource.Rsrc.Get(), 0);

		// Texture resource was updated
		return S_OK;
	}
	else
	{
		// Failed to update texture resource
		return E_FAIL;
	}
}


void Keel::KinectFusionDepthFloatFrame::CreateDeviceResource(
	int iWidth,
	int iHeight
	)
{
	//! Check if \c DXGI_FORMAT_R16_FLOAT is ok, if not then change back to \c DXGI_FORMAT_R32_FLOAT
	//m_kfOffscreenResource.m_d3dResourceDesc.MiscFlags = D3D11_RESOURCE_MISC_GDI_COMPATIBLE;
	m_kfOffscreenResource.CreateDeviceResources(
		iWidth, iHeight,
		sizeof(float),
		DXGI_FORMAT_R16_FLOAT
		);
}

void Keel::KinectFusionDepthFloatFrame::ChangeResourceSize(
	int iWidth,
	int iHeight
	)
{
	//! Check if \c DXGI_FORMAT_R16_FLOAT is ok, if not then change back to \c DXGI_FORMAT_R32_FLOAT
	m_kfOffscreenResource.CreateWindowSizeDependentResources(
		iWidth, iHeight,
		sizeof(float),
		DXGI_FORMAT_R16_FLOAT
		);
}

void Keel::KinectFusionColorCompressedFrame::CreateDeviceResource(
	int iWidth,
	int iHeight
	)
{
	//m_kfOffscreenResource.m_d3dResourceDesc.MiscFlags = D3D11_RESOURCE_MISC_GDI_COMPATIBLE;
	m_kfOffscreenResource.CreateDeviceResources(
		iWidth, iHeight, sizeof(byte[4]),
		DXGI_FORMAT_B8G8R8A8_UNORM
		);
}

void Keel::KinectFusionColorCompressedFrame::ChangeResourceSize(
	int iWidth,
	int iHeight
	)
{
	m_kfOffscreenResource.CreateWindowSizeDependentResources(
		iWidth, iHeight, sizeof(byte[4]),
		DXGI_FORMAT_B8G8R8A8_UNORM
		);
}