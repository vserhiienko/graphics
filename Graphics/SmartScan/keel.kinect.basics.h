#pragma once

#include "pch.h"

namespace Keel
{
	class KinectCore
	{
	public:

		typedef byte Byte;
		typedef FLOAT Real;
		typedef UINT32 Dimension;
		typedef DXGI_FORMAT VideoStreamFormat;

		static Dimension const kUndefined = UINT_ERROR;
		static KinectCore::Real const DegreesToRadians;

		enum ImageType
		{
			kDepthImageType,
			kColourImageType
		};

		struct VideoStreamProfile
		{
			Dimension Width, Height;
			VideoStreamFormat Format;
		};

		struct VideoStreamFrame
			: public VideoStreamProfile
		{
			KinectCore::Byte *Bytes;
			KinectCore::Dimension Stride;
			KinectCore::Dimension Pixel;
			KinectCore::Dimension Size;

			dx::Vector2 PrincipalPoint;
			dx::Vector2 FocalLength;
			dx::Vector2 FieldOfView;
			dx::Vector2 Scaling;
			dx::Vector2 Range;

			VideoStreamFrame();
			~VideoStreamFrame();

			void Alloc(
				_In_ KinectCore::Dimension width = kUndefined,
				_In_ KinectCore::Dimension height = kUndefined,
				_In_ KinectCore::Dimension pixel = kUndefined
				);
			void Dealloc(
				);
		};

	};

	class KinectBasics
	{
	public:

		typedef Microsoft::WRL::ComPtr<::INuiSensor> SensorHandle;
		typedef NUI_IMAGE_RESOLUTION ImageResolution;
		typedef NUI_IMAGE_TYPE ImageType;
		typedef HANDLE CoreHandle, CoreEvent;
		typedef HRESULT ErrorCode;
		typedef BOOL Boolean;
		typedef SHORT Short;
		typedef FLOAT Real;

		struct IFrame
			: public KinectCore::VideoStreamFrame
		{
			typedef UINT64 ULongLong;
			typedef std::vector<IFrame*> Vector;
			typedef KinectCore::Dimension InitializeFlags;

			static KinectBasics::ImageResolution const kDefaultDepthResolution = NUI_IMAGE_RESOLUTION_640x480;
			static KinectBasics::ImageResolution const kDefaultColorResolution = NUI_IMAGE_RESOLUTION_640x480;
			static KinectBasics::ImageType const kDefaultColorType = NUI_IMAGE_TYPE_COLOR;
			static IFrame::InitializeFlags const kNoRequirements = 0u;

			KinectBasics *Host;
			KinectBasics::ImageType Type;
			KinectBasics::ImageResolution Resolution;
			KinectBasics::CoreHandle StreamHandle;
			KinectBasics::CoreHandle NextFrameEvent;

			IFrame::ULongLong FrameId;
			IFrame::InitializeFlags Requirements;

			IFrame(KinectBasics *);
			virtual ~IFrame();
			virtual void Init();
			virtual void Quit();

			virtual void Open();
			virtual void ProcessSensor();
			Keel::KinectBasics::Boolean IsNextFrameReceived();
		};

		struct ColorFrame
			: public KinectBasics::IFrame
		{
			static KinectCore::Dimension const kBytesPerPixel = 4u;

			ColorFrame(KinectBasics *);
		};

		struct DepthFrame
			: public KinectBasics::IFrame
		{
			static KinectBasics::Short const MinDepth = 1;
			static KinectBasics::Short const MaxDepth = SHORT_MAX;

			DepthFrame(KinectBasics *);
		};

		struct DepthPlayerFrame
			: public KinectBasics::DepthFrame
		{
			DepthPlayerFrame(KinectBasics *);
			virtual void ProcessSensor() override;
		};

		KinectBasics::SensorHandle Sensor;
		KinectBasics::Boolean IsInNearMode;
		KinectBasics::ErrorCode LastError;
		KinectCore::Dimension SensorDevicesFound;
		IFrame::Vector SubmittedFrames;
		IFrame::InitializeFlags SensorRequirements;

		KinectBasics();

		void Init();
		void Quit();
		void SubmitFrame(IFrame *);
		void OpenAllFrames();
		void ProcessAllFrames();
		void CreateDeviceIndependentResources();
		static bool Succeeded(KinectBasics::ErrorCode const &);
		static bool Failed(KinectBasics::ErrorCode const &);

	private:

		KinectBasics::Boolean m_has_been_initted;
		KinectBasics::Boolean m_has_been_quitted;

	};
}