#pragma once
#include "pch.h"

namespace Keel
{
	using namespace dx;

	/// <summary>
	/// Represents a texture resource that can be updated from the CPU and read by GPU
	/// </summary>
	class KinectFusionOffscreenTexture
	{
	public:

		typedef struct Vertex
		{
			dx::Vector4 Position;
			dx::Vector3 Normal;
			dx::Vector2 Coords;
		} VertexUnit;

		typedef unsigned __int16 IndexUnit;
	public:

		KinectFusionOffscreenTexture();

		/// <summary>
		/// Represents offscreen texture resource and shader resource view
		/// </summary>

		ReadResource<Texture2> m_d3dReadResource;

		/// <summary>
		/// Stores offscreen texture resource description
		/// </summary>
		Texture2Description m_d3dResourceDesc;

		/// <summary>
		/// Vertex buffer (is required for rendering)
		/// </summary>
		Buffer Vb;

		/// <summary>
		/// Index buffer (is required for rendering)
		/// </summary>
		Buffer Ib;

		int m_iExpectedResourceSize;

		/// <summary>
		/// Forces recreation of device dependent and window size dependent resources 
		/// </summary>
		/// <param name="iWidth">Texture resource width</param>
		/// <param name="iHeight">Texture resource height</param>
		/// <param name="iHeight">Texture stride size</param>
		/// <param name="eFormat">Texture resource dxgi format</param>
		/// <returns></returns>
		HRESULT CreateDeviceResources(
			int iWidth,
			int iHeight,
			int iStride,
			DXGI_FORMAT eFormat
			);

		/// <summary>
		/// Recreates window size dependent resources if necessary
		/// </summary>
		/// <param name="iWidth">Texture resource width</param>
		/// <param name="iHeight">Texture resource height</param>
		/// <param name="iHeight">Texture stride size</param>
		/// <param name="eFormat">Texture resource dxgi format (color - rgba8_unorm, depth - r32_float)</param>
		/// <returns></returns>

		HRESULT CreateWindowSizeDependentResources(
			int iWidth,
			int iHeight,
			int iStride,
			DXGI_FORMAT eFormat,
			bool bForceRecreation = false
			);

		HRESULT DiscardDeviceResources();

		/// <summary>
		/// Updates texture resource content with buffer provided
		/// </summary>
		/// <remarks>
		/// Buffer content should correspond to earlier created resource
		/// </remarks>
		/// <returns></returns>
		HRESULT UpdateResourceContent(
			std::shared_ptr<byte[]> spBytes,
			int iBufferSize
			);

		/// <summary>
		/// Updates texture resource content with buffer provided
		/// </summary>
		/// <remarks>
		/// Buffer content should correspond to earlier created resource
		/// </remarks>
		/// <returns></returns>
		HRESULT UpdateResourceContent(
			byte pBytes[],
			int iBufferSize
			);

	};

	/// <summary>
	/// Frame common properties
	/// </summary>
	class IKinectFusionFrame
	{
	protected:

		/// <summary>
		/// Frame offscreen resources
		/// </summary>
		KinectFusionOffscreenTexture m_kfOffscreenResource;

	public:

		/// <summary>
		/// Smart pointer
		/// </summary>
		typedef std::shared_ptr<IKinectFusionFrame> SharedPointer;

	public:

		typedef dx::Buffer Buffer;
		typedef dx::Texture2 SurfaceResource;
		typedef dx::ActiveResource<SurfaceResource> ActiveSurfaceResource;

		/// <summary>
		/// Inline accessors for quick access (both for ptr and ref)
		/// </summary>

		inline KinectFusionOffscreenTexture *GetOffscreenResource() { return &m_kfOffscreenResource; }
		inline KinectFusionOffscreenTexture &GetOffscreenResourceRef() { return m_kfOffscreenResource; }
		inline ID3D11Texture2D *GetTexture() { return m_kfOffscreenResource.m_d3dReadResource.Rsrc.Get(); }
		inline ID3D11Texture2D **GetTextureAddress() { return m_kfOffscreenResource.m_d3dReadResource.Rsrc.GetAddressOf(); }
		inline ID3D11ShaderResourceView *GetShaderResourceView() { return m_kfOffscreenResource.m_d3dReadResource.Read.Get(); }
		inline ID3D11ShaderResourceView **GetShaderResourceViewAddress() { return m_kfOffscreenResource.m_d3dReadResource.Read.GetAddressOf(); }

	public:

		/// <summary> 
		/// Create device dependent resources 
		/// </summary>
		virtual void CreateDeviceResource(int iWidth, int iHeight) = 0;

		/// <summary> 
		/// Changes texture resource size if needed 
		/// </summary>
		virtual void ChangeResourceSize(int iWidth, int iHeight) = 0;
	};

	/// <summary>
	/// Direct3D Texture2D 
	/// </summary>
	class KinectFusionDepthFloatFrame : public IKinectFusionFrame
	{
	public:

		/// <summary>
		/// Create device dependent resources
		/// </summary>
		virtual void CreateDeviceResource(
			int iWidth, int iHeight
			) override;

		/// <summary>
		/// Changes texture resource size if needed
		/// </summary>
		virtual void ChangeResourceSize(
			int iWidth, int iHeight
			) override;

	};

	/// <summary>
	/// Direct3D Texture2D 
	/// </summary>
	class KinectFusionColorCompressedFrame : public IKinectFusionFrame
	{
	public:

		/// <summary>
		/// Create device dependent resources
		/// </summary>
		virtual void CreateDeviceResource(
			int iWidth, int iHeight
			) override;

		/// <summary>
		/// Changes texture resource size if needed
		/// </summary>
		virtual void ChangeResourceSize(
			int iWidth, int iHeight
			) override;

	};

}