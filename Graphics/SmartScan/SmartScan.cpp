#include "pch.h"
#include "SmartScan.h"
#include <INIReader.h>

using namespace concurrency;

static void TW_CALL launchScannerButtonHook(void *user)
{
  assert(!!user);
  auto app = static_cast<keel::SmartScanApp*>(user);
  app->toggleScanning();
}

dx::DxSampleApp *dx::DxSampleAppFactory()
{
  return new keel::SmartScanApp();
}

keel::SmartScanApp::SmartScanApp()
  : DxSampleApp()
  , clearColor(deviceResources.clearColor)
{
  dx::trace("keel:app: create app");

  tweakBar = 0;
  hasUI = false;
  scannerLaunched = false;
}

keel::SmartScanApp::~SmartScanApp()
{
  dx::trace("keel:app: destroy app");

  if (scannerLaunched)
    scannerTaskCTS.cancel(),
    scannerTask.wait();

  if (motionEst)
    motionEst->OnRawStreamsServiceReleased(kinect), 
    delete motionEst, 
    motionEst = 0;

  if (kinect) 
    kinect->quit(), 
    delete kinect, 
    kinect = 0;


  annClose();

  // close ui
  TwDeleteAllBars();
  TwTerminate();
}

void keel::SmartScanApp::initializeUI()
{
  Microsoft::WRL::ComPtr<ID3D11Device> device11; // make it scoped
  if (SUCCEEDED(deviceResources.device.As(&device11)))
  {
    if (
      // if tweak bar graphics initialization succeeded and
      (hasUI = TwInit(TW_DIRECT3D11, device11.Get())) &&
      // if tweak bar handler creation succeeded
      !!(tweakBar = TwNewBar("SmartScan"))
      )
    {
      TwDefine(" GLOBAL help='SmartScan v2.1\nVlad Serhiienko' "); 
      TwWindowSize(window.dimensions.x, window.dimensions.y);

      int barW = window.dimensions.x / 4;
      int barH = window.dimensions.y / 4 * 3;
      int barSize[2] = { barW, barH };

      TwSetParam(tweakBar, NULL, "size", TW_PARAM_INT32, 2, barSize);
      TwAddVarRW(tweakBar, "ClearColor", TW_TYPE_COLOR4F, &clearColor, "group=General colormode=hls label='Background'");
      TwAddButton(tweakBar, "ScannerToggle", &launchScannerButtonHook, this, "group=Scanner label='Rescan'");
    }
  }
}

void keel::SmartScanApp::onFrameProcessed(
  fovis::MotionEstimateStatusCode status,
  fovis::VisualOdometry &visOdom
  )
{
	auto m_vis_odom_core = &visOdom;

	switch (status)
  {
  case fovis::SUCCESS:
    //dx::trace("onFrameProcessed: ok");


	//else
	{
		auto iso_pose = m_vis_odom_core->getPose();
		auto pose_xyz = iso_pose.translation();
		auto pose_rpy = iso_pose.rotation().eulerAngles(0, 1, 2);

		auto iso_motion = m_vis_odom_core->getMotionEstimate();
		auto motion_xyz = iso_motion.translation();
		auto motion_rpy = iso_motion.rotation().eulerAngles(0, 1, 2);
		auto frame = m_vis_odom_core->getTargetFrame();

        if (
            pose_xyz(0) != 0.0f
            || pose_xyz(1) != 0.0f
            || pose_xyz(2) != 0.0f
            || status != fovis::MotionEstimateStatusCode::INSUFFICIENT_INLIERS
            )
        {
            dx::trace(
                "POSE\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f"
                //"\tm(iso): t [%.2f %.2f %.2f] o [%.2f %.2f %.2f]"
                //, status
                , pose_xyz(0)
                , pose_xyz(1)
                , pose_xyz(2)
                , pose_rpy(0) * 180 / M_PI
                , pose_rpy(1) * 180 / M_PI
                , pose_rpy(2) * 180 / M_PI
                /*, motion_xyz(0)
                , motion_xyz(1)
                , motion_xyz(2)
                , motion_rpy(0) * 180 / M_PI
                , motion_rpy(1) * 180 / M_PI
                , motion_rpy(2) * 180 / M_PI*/
                );
            //dx::trace(
            //    "VisualOdometry::OnFrameAcquired(): code %u "
            //    "pose(iso): t [%.2f %.2f %.2f] o [%.2f %.2f %.2f]"
            //    //"\tm(iso): t [%.2f %.2f %.2f] o [%.2f %.2f %.2f]"
            //    , status
            //    , pose_xyz(0)
            //    , pose_xyz(1)
            //    , pose_xyz(2)
            //    , pose_rpy(0) * 180 / M_PI
            //    , pose_rpy(1) * 180 / M_PI
            //    , pose_rpy(2) * 180 / M_PI
            //    /*, motion_xyz(0)
            //    , motion_xyz(1)
            //    , motion_xyz(2)
            //    , motion_rpy(0) * 180 / M_PI
            //    , motion_rpy(1) * 180 / M_PI
            //    , motion_rpy(2) * 180 / M_PI*/
            //    );
        }
	}

    break;
  case fovis::NO_DATA:
    dx::trace("onFrameProcessed: no data");
    break;
  case fovis::INSUFFICIENT_INLIERS:
    dx::trace("onFrameProcessed: inliers");
    break;
  case fovis::OPTIMIZATION_FAILURE:
    dx::trace("onFrameProcessed: opt failure");
    break;
  case fovis::REPROJECTION_ERROR:
    dx::trace("onFrameProcessed: reproj err");
    break;
  }
}

void keel::SmartScanApp::launchScanning(void)
{
  if (scannerLaunched) return; else scannerLaunched = true; // guard

  TwDefine("SmartScan/ScannerToggle readonly=true");
  scannerTaskCTS = cancellation_token_source(), scannerTask = create_task([this]
  {
    if (motionEst) motionEst->OnRawStreamsServiceReleased(kinect), delete motionEst, motionEst = 0;
    if (kinect) kinect->quit(), delete kinect, kinect = 0;

    kinect = new gaia::KinectRawStreams();
    motionEst = new gaia::VisualOdometry(kinect);
    TwDefine("SmartScan/ScannerToggle label='Searching for Kinect...'");
    kinect->init(); /// fixme: check if kinect device was found
    TwDefine("SmartScan/ScannerToggle label='Initializing Fovis...'");
    motionEst->OnRawStreamsServiceInitialized(kinect); /// fixme: check if visual odometry intialization succeeded
    motionEst->setFrameProcessedHook(this);

    TwDefine("SmartScan/ScannerToggle label='Scanning...' readonly=false");
    while (kinect && motionEst)
    {
      // check for cancellation. 
      if (is_task_cancellation_requested())
      {
        TwDefine("SmartScan/ScannerToggle readonly=true");

        if (motionEst)
        {
          TwDefine("SmartScan/ScannerToggle label='Destroying Fovis...'");
          motionEst->OnRawStreamsServiceReleased(kinect);
          delete motionEst;
          motionEst = 0;
        }

        if (kinect)
        {
          TwDefine("SmartScan/ScannerToggle label='Shutting down Kinect...'");

          kinect->quit();
          delete kinect;
          kinect = 0;
        }

        scannerLaunched = false;
        cancel_current_task();
      }
      else
      {
        // next iteration ...
        kinect->frameMove();
        motionEst->OnFrameAcquired(kinect);
      }
    }
  }, scannerTaskCTS.get_token());
}

void keel::SmartScanApp::doneScanning(void)
{
  scannerTaskCTS.cancel();
  scannerTask.wait();

  // handle UI
  TwDefine("SmartScan/ScannerToggle label='Rescan' readonly=false");
}

void keel::SmartScanApp::toggleScanning(void)
{
  try
  {
    if (scannerLaunched) doneScanning();
    else launchScanning();
  }
  catch (...)
  {
    dx::trace("toggleScanning: error");
  }
}

bool keel::SmartScanApp::onAppLoop()
{
  dx::trace("keel:app: initialize app");
  window.setWindowTitle("SmartScan v2.1 /Vlad Serhiienko/");
  timer.reset();


  INIReader iniReader("SmartScan.ini");
  uiUrl = iniReader.Get("SmartScanUI", "appUiUrl", _SmartScan_UIURL);
  // read user predefined args
  // ....

  initializeUI();

  return hasUI;
}

void keel::SmartScanApp::handlePaint(dx::DxWindow const*)
{
  /// update

  //kinect->frameMove();
  //motionEst->OnFrameAcquired(kinect);

  timer.update();

  // update scene
  // ...
  // update ui
 // dxui::DxUIClient::updateApplication();
  //updateUI(timer.elapsed);
  
  /// draw

  beginScene();
  // render scene
  // ...
  // render ui
  //renderUI();
  TwDraw();
  endScene();
}

void keel::SmartScanApp::handleSizeChanged(dx::DxWindow const*)
{

}

void keel::SmartScanApp::handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  switch (args.keyCode())
  {
  case dx::VirtualKey::Escape: window.close(); break;
  default: {} break;
  }

}

void keel::SmartScanApp::handleMouseMoved(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  if (!TwMouseMotion(
    static_cast<int>(args.mouseX()),
    static_cast<int>(args.mouseY())
    ))
  {
    // non-ui click
    // handle here
  }
}

void keel::SmartScanApp::handleMouseLeftPressed(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  auto button = TW_MOUSE_LEFT;
  auto action = TW_MOUSE_PRESSED;

  TwMouseButton(action, button);
  if (!TwMouseMotion(
    static_cast<int>(args.mouseX()),
    static_cast<int>(args.mouseY())
    ))
  {
    // non-ui click
    // handle here
  }
}

void keel::SmartScanApp::handleMouseLeftReleased(dx::DxWindow const*, dx::DxGenericEventArgs const &args)
{
  auto button = TW_MOUSE_LEFT;
  auto action = TW_MOUSE_RELEASED;

  TwMouseButton(action, button);
  if (!TwMouseMotion(
    static_cast<int>(args.mouseX()),
    static_cast<int>(args.mouseY())
    ))
  {
    // non-ui click
    // handle here
  }
}
