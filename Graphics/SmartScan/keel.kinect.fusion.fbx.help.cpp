#include "pch.h"
#include "keel.kinect.fusion.fbx.help.h"
#include "keel.kinect.fusion.help.h"

namespace Keel
{
	namespace KinectFusionHelpers
	{
		union IntToByte4
		{
			int32_t Integer;
			int8_t ByteArray[4];
		};
		union IntToUByte4
		{
			int32_t Integer;
			uint8_t ByteArray[4];
		};
		union UIntToByte4
		{
			uint32_t Integer;
			int8_t ByteArray[4];
		};
		union UIntToUByte4
		{
			uint32_t Integer;
			uint8_t ByteArray[4];
		};

		static char _static_tempCstring[512];
		static FbxSurfacePhong *CreatePhongMaterial(
			unsigned uniqueIdx
			, FbxMesh *pFbxMesh
			, double(&ambient)[3]
			, double(&diffuse)[3]
			, double(&emissive)[3]
			, double shininess = 0.5f
			, double transparencyFactor = 0.0f
			)
		{
			sprintf_s(_static_tempCstring, "keel::material[id=%u]", uniqueIdx);

			auto fbxPhong = FbxSurfacePhong::Create(
				pFbxMesh->GetScene(), _static_tempCstring
				);

			fbxPhong->ShadingModel.Set("Phong");
			fbxPhong->Shininess.Set(shininess);
			fbxPhong->TransparencyFactor.Set(transparencyFactor);
			fbxPhong->Ambient.Set(FbxDouble3(ambient[0], ambient[1], ambient[2]));
			fbxPhong->Diffuse.Set(FbxDouble3(diffuse[0], diffuse[1], diffuse[2]));
			fbxPhong->Emissive.Set(FbxDouble3(emissive[0], emissive[1], emissive[2]));

			return fbxPhong;
		}

	}
}

HRESULT Keel::FbxHelp::WriteFbxMeshFile(
	INuiFusionColorMesh *pFusionMesh,
	LPOLESTR lpOleFileName
	)
{
	using namespace fbxsdk_2015_1;
	//auto air = Dove::Engine::CreateAirForCurrentThread();

	if (!pFusionMesh)
	{
		// Print error
		return E_INVALIDARG;
	}

	// Create the FBX SDK manager
	FbxManager *lSdkManager(
		FbxManager::Create()
		);

	// Create an IOSettings object
	FbxIOSettings *ios(
		FbxIOSettings::Create(lSdkManager, IOSROOT)
		);
	lSdkManager->SetIOSettings(ios);

	// Create an exporter
	FbxSharedDeletePtr<FbxExporter> pFbxSceneExporter(
		FbxExporter::Create(lSdkManager, "")
		);

	// Declare the path and filename of the file to which the scene will be exported
	// In this case, the file will be in the same directory as the executable
	std::wstring wfileName(lpOleFileName ? lpOleFileName : L"");
	std::string fileName(wfileName.begin(), wfileName.end());

	if (fileName.empty())
	{
		// Print warning and file name
		//Oasis::GenerateUuid(fileName);
    fileName = "keel-reconstruction";
    //fileName.insert(0, "keel-reconstruction-");
		fileName += ".fbx";

		dx::trace(
			"FbxHelp::WriteFbxMeshFile()[File %s]",
			fileName.c_str()
			);
	}

	// Initialize the exporter
	bool lExportStatus = pFbxSceneExporter->Initialize(
		fileName.c_str(), // file name
		-1, // file format
		lSdkManager->GetIOSettings() // io
		);

	if (!lExportStatus)
	{
		// Print error
		// pFbxSceneExporter->GetStatus().GetErrorString()
		return E_FAIL;
	}

	// Create a new scene so it can be populated by the imported file.
	auto pFbxScene = FbxScene::Create(lSdkManager, "Keel::Reconstruction");

	// ... Import a scene, or build a new one ...

	// Create a node for our mesh in the scene.
	FbxNode* pFbxMeshNode = FbxNode::Create(pFbxScene, "Keel::Mesh::Node");
	// Create a mesh.
	FbxMesh* pFbxMesh = FbxMesh::Create(pFbxScene, "Keel::Mesh::Attribute");
	// Set the node attribute of the mesh node.
	pFbxMeshNode->SetNodeAttribute(pFbxMesh);

	FbxStatus fbxStatus;
	HRESULT fusionStatus;

	int const *pColors = NULL;
	int const *pIndices = NULL;
	Vector3 const *pNormals = NULL;
	Vector3 const *pVertices = NULL;

	auto colorCount = pFusionMesh->ColorCount();
	auto vertexCount = pFusionMesh->VertexCount();
	auto normalCount = pFusionMesh->NormalCount();
	auto triangleIndexCount = pFusionMesh->TriangleVertexIndexCount();

	dx::trace(
		"FbxHelp::WriteFbxMeshFile()[Vertices %u, normals %u, indices %u]",
		vertexCount, normalCount, triangleIndexCount
		);

	fusionStatus = pFusionMesh->GetVertices(&pVertices);
	if (FAILED(fusionStatus))
	{
		_com_error hr_error(fusionStatus);

		// Print error
		// Clear memory
		return fusionStatus;
	}

	fusionStatus = pFusionMesh->GetNormals(&pNormals);
	if (FAILED(fusionStatus))
	{
		_com_error hr_error(fusionStatus);

		// Print error
		// Clear memory
		return fusionStatus;
	}

	fusionStatus = pFusionMesh->GetColors(&pColors);
	if (FAILED(fusionStatus))
	{
		_com_error hr_error(fusionStatus);

		// Print error
		// Clear memory
		return fusionStatus;
	}

	fusionStatus = pFusionMesh->GetTriangleIndices(&pIndices);
	if (FAILED(fusionStatus))
	{
		_com_error hr_error(fusionStatus);

		// Print error
		// Clear memory
		return fusionStatus;
	}

	//Medusa::LscmMesh meshRef;
	//meshRef.Initialize(
	//	const_cast<Vector3 *>(pVertices),
	//	const_cast<int *>(pIndices),
	//	vertexCount,
	//	triangleIndexCount
	//	);

	//Medusa::LscmSolver lscmSolver;
	//lscmSolver.AttachMesh(meshRef);
	//lscmSolver.apply();

	// 

	pFbxMesh->SetControlPointCount((int)vertexCount);

	auto idx = 0u;

	for (idx = 0; idx < vertexCount; idx++)
	{
		pFbxMesh->SetControlPointAt(FbxVector4(
			-pVertices[idx].x,
			-pVertices[idx].y,
			pVertices[idx].z
			), idx
			);
	}

	dx::trace(
		"FbxHelp::WriteFbxMeshFile()[Vertices saved]"
		);

	auto elementNormal = pFbxMesh->CreateElementNormal();
	elementNormal->SetReferenceMode(elementNormal->eDirect);
	elementNormal->SetMappingMode(elementNormal->eByControlPoint);

	for (idx = 0; idx < normalCount; idx++)
	{
		elementNormal->GetDirectArray().Add(FbxVector4(
			-pNormals[idx].x,
			-pNormals[idx].y,
			pNormals[idx].z
			));
	}

	dx::trace(
		"FbxHelp::WriteFbxMeshFile()[Normals saved]"
		);

	auto colorIdx = 0u;
	KinectFusionHelpers::IntToUByte4 colorCast;

	auto elementColor = pFbxMesh->CreateElementVertexColor();
	elementColor->SetReferenceMode(elementColor->eDirect);
	elementColor->SetMappingMode(elementColor->eByControlPoint);

	FbxColor rgba;
	for (idx = 0, colorIdx = 0; idx < colorCount; idx++, colorIdx += 3)
	{
		colorCast.Integer = pColors[idx];

		rgba[0] = (double)colorCast.ByteArray[2];
		rgba[1] = (double)colorCast.ByteArray[1];
		rgba[2] = (double)colorCast.ByteArray[0];
		rgba[3] = (double)colorCast.ByteArray[3];

		rgba[0] /= 255.0f;
		rgba[1] /= 255.0f;
		rgba[2] /= 255.0f;
		rgba[3] /= 255.0f;

		elementColor->GetDirectArray().Add(rgba);
	}

	dx::trace(
		"FbxHelp::WriteFbxMeshFile()[Colors saved]"
		);


	for (idx = 0; idx < triangleIndexCount; idx += 3)
	{
		pFbxMesh->BeginPolygon();
		pFbxMesh->AddPolygon(pIndices[idx + 0]);
		pFbxMesh->AddPolygon(pIndices[idx + 1]);
		pFbxMesh->AddPolygon(pIndices[idx + 2]);
		pFbxMesh->EndPolygon();
	}

	dx::trace(
		"FbxHelp::WriteFbxMeshFile()[Polygons saved]"
		);

	// Add the mesh node to the root node in the scene.
	FbxNode *pFbxSceneRootNode = pFbxScene->GetRootNode();
	pFbxSceneRootNode->AddChild(pFbxMeshNode);

	// Export the scene to the file
	pFbxSceneExporter->Export(pFbxScene);

	// File format version numbers to be populated
	int lMajor, lMinor, lRevision;

	// Populate the exported file format version numbers
	FbxManager::GetFileFormatVersion(lMajor, lMinor, lRevision);

	return S_OK;
}
