#pragma once
#include <memory>
#include <algorithm>
#include <set>
#include <vector>
#include <assert.h>
#include <ppl.h>
#include <ppltasks.h>
#include <Eigen/Eigen>

#include <ANN/ANN.h>
#include <ANN/ANNx.h>
#include <ANN/ANNperf.h>

#include <fovis.hpp>

#include <DxUtils.h>
#include <DxTimer.h>
#include <DxSampleApp.h>
#include <AntTweakBar.h>
#include <DirectXColors.h>
#include <DirectXCollision.h>

#include <NuiApi.h>
#include <NuiSensor.h>
#include <NuiSkeleton.h>
#include <NuiKinectFusionApi.h>
#include <NuiKinectFusionVolume.h>
#include <NuiKinectFusionColorVolume.h>
#include <NuiKinectFusionDepthProcessor.h>
#include <NuiKinectFusionCameraPoseFinder.h>

//#include <fbxsdk.h>

/// Windows

#include <comdef.h>
#include <atlbase.h>
#include <Shobjidl.h>

#define _SmartScan_UIURL "file:///D:/Dev/Vs/Proj/Graphics/Graphics/SmartScan/assets/ui/SmartScanUI/index.html"

#ifndef SAFE_DELETE
#define SAFE_DELETE(p) { if (p) { delete (p); (p)=NULL; } }
#endif
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if (p) { delete[] (p); (p)=NULL; } }
#endif
#ifndef SAFE_FUSION_RELEASE_IMAGE_FRAME
#define SAFE_FUSION_RELEASE_IMAGE_FRAME(p) { if (p) { static_cast<void>(NuiFusionReleaseImageFrame(p)); (p)=NULL; } }
#endif
