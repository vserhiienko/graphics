#pragma once

/// App includes
#include "pch.h"

#include "itv.oasis.h"

/// KinectFusion includes
#include "keel.kinect.fusion.renderer.h"
#include "keel.kinect.fusion.processor.h"

namespace Keel
{
	/// <summary>
	/// </summary>
	class Reconstruction : public Oasis::State
	{
		friend KinectFusionProcessor;

	public:

		/// <summary>
		/// Synchonized part
		/// </summary>

	public:

		Reconstruction();
		~Reconstruction();

		/// <summary>
		/// Launches kinectfusion processor on the other thread
		/// </summary>
		virtual void Init();

		/// <summary>
		/// Stops kinect fusion reconstruction, releases all allocated resources
		/// </summary>
		virtual void Quit();

		/// <summary>
		/// Stops integrating volumes
		/// </summary>
		virtual void Suspend();

		/// <summary>
		/// Starts integrating volumes
		/// </summary>
		virtual void Resume();

		/// <summary>
		/// Handles kinect fusion frame if any
		/// </summary>
		virtual void OnFrameMove();

		/// <summary>
		/// Initialize all device dependent resources
		/// </summary>
		virtual void CreateDeviceResources();

		/// <summary>
		/// Initialize sensor and video streams
		/// </summary>
		virtual void CreateDeviceIndependentResources();

		/// <summary>
		/// Initialize or recreate all window size dependent resources
		/// </summary>
		virtual void CreateWindowSizeDependentResources();

		/// <summary>
		/// Processing keyboard events
		/// </summary>

		virtual void OnKeyPressed(_In_::UINT32);
		virtual void OnKeyReleased(_In_::UINT32);

	private:

		/// <summary>
		/// Renders images from kinect fusion
		/// </summary>
		KinectFusionRenderer m_fusionRenderer;

		/// <summary>
		/// Offscreens
		/// </summary>

		KinectFusionDepthFloatFrame m_fusionDepthOffscreen;
		KinectFusionColorCompressedFrame m_fusionColorOffscreen;

		/// <summary>
		/// Generates ortho matrix for offscreen rendering in 2d
		/// </summary>
		Demo::Camera m_offscreenViewer;

	private:

		/// <summary>
		/// Asynchronous part
		/// </summary>

		void SaveMesh();

	private:

		/// <summary>
		/// Processes depth and color streams for environment reconstruction
		/// </summary>
		KinectFusionProcessor m_fusionProcessor;

		/// <summary>
		/// Represents parameters to setup kinect fusion reconstruction pipeline
		/// </summary>
		KinectFusionParams m_fusionConfiguration;

		/// <summary>
		/// Notifies renderer about new fusion frame
		/// </summary>
		std::atomic_bool m_bHandleComletedFrame;


		std::atomic_bool m_bIsCtrlPressed;
		std::atomic_bool m_bIsSavingMesh;

	};
}