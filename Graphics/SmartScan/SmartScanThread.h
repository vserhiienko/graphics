#pragma once
#include "pch.h"

namespace keel
{
  class SmartScanApp;
  class SmartScanThread
  {
    static unsigned s_appThreadCount;

  protected:
    unsigned id; // local thread id
    std::string name; // thread name 
    SmartScanApp *app; // host application

  public:
    typedef SmartScanThread *Ptr;
    typedef std::shared_ptr<SmartScanThread> SharedPtr;
    typedef std::vector<Ptr> Collection;
    typedef dx::Event<void, Ptr> Event;

  public:
    mutable Event initted;
    mutable Event quitted;
    mutable Event resumed;
    mutable Event suspended;

  public:
    SmartScanThread(SmartScanApp *host);
    SmartScanThread(SmartScanApp *host, std::string const &name);
    virtual ~SmartScanThread(void);

  public:
    inline virtual void init(void) { initted(this); }
    inline virtual void quit(void) { quitted(this); }
    inline virtual void resume(void) { resumed(this); }
    inline virtual void suspend(void) { suspended(this); }

  public:
    inline virtual void onInitializeSample(void) { }
    inline virtual void onFrameMove(dx::sim::BasicTimer const&) { }
    inline virtual void keyPressed(dx::DxGenericEventArgs const&) { }
    inline virtual void keyReleased(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerWheel(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerMoved(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerLeftPressed(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerLeftReleased(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerRightPressed(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerRightReleased(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerMiddlePressed(dx::DxGenericEventArgs const&) { }
    inline virtual void pointerMiddleReleased(dx::DxGenericEventArgs const&) { }

  };
}