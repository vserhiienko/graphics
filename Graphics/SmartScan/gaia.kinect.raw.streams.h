#pragma once
#include "gaia.raw.streams.h"

namespace gaia
{
  class KinectRawStreams
    : public IRawStreams
  {
  public:

    typedef _Vector4 KinectVec4;
    typedef HRESULT HResult, ResultHandle;
    typedef HANDLE EventHandle, ObjectHandle;
    typedef Microsoft::WRL::ComPtr<INuiSensor> NuiSensorComPtr;
    typedef Microsoft::WRL::ComPtr<INuiCoordinateMapper> NuiCoordinateMapperComPtr;

    /// <summary>
    /// The 32-bit color format.
    /// </summary>
    struct Rgbx
    {
      union
      {
        struct { uint8_t r, g, b, x; }; 
        uint8_t bgr[4];
      };
    };

    /// <summary>
    /// The 16-bit depth format.
    /// </summary>
    struct DepthU16Frame : public utils::FrameBase<uint16_t>
    {
      float invalids[2];
    };

    /// <summary>
    /// The 16-bit depth format.
    /// </summary>
    struct DepthU32Frame : public utils::FrameBase<NUI_DEPTH_IMAGE_PIXEL>
    {
      float invalids[2];
    };

    /// <summary>
    /// The 32-bit color format.
    /// </summary>
    struct ColorRgbxFrame : public utils::FrameBase<Rgbx> 
    {
    };

  public:

    ~KinectRawStreams();

    virtual void init();
    virtual void quit();
    virtual void resume();
    virtual void suspend();
    virtual void frameMove();

    virtual void registerNotify(
      INotify::Pointer notify
      );
    virtual void unregisterNotify(
      INotify::Pointer notify
      );

    virtual bool getCameraIntrinsicsParameters(
      DirectX::Vector2 &color_focal_lengths,
      DirectX::Vector2 &depth_focal_lengths,
      DirectX::Vector2 &color_principal_point,
      DirectX::Vector2 &depth_principal_point,
      DirectX::Vector2 &color_dimensions,
      DirectX::Vector2 &depth_dimensions
      );
    virtual bool getAccelerometerReadings(
      DirectX::Vector3 &accel_readings
      );  

    KinectRawStreams();
    void processRawDepth();
    void generateGrayscaleFrame();
    void calculateColorProjection();
    void createFirstConnectedSensor();

    bool processStreamFrame(
      ObjectHandle stream_handle,
      ObjectHandle target_frame_mem,
      unsigned expected_byte_size
      );

  protected:

    bool m_block_execution;
    bool m_is_instance_valid;
    bool m_is_coords_mapper_available;
    bool m_was_state_resumed;
    bool m_was_device_disconnected;

    unsigned m_last_notify_count;
    unsigned m_last_consumer_count;

    D2D1_SIZE_U m_color_dimensions;
    D2D1_SIZE_U m_depth_dimensions;
    NUI_IMAGE_RESOLUTION m_color_res;
    NUI_IMAGE_RESOLUTION m_depth_res;

    NuiSensorComPtr m_sensor;
    NuiCoordinateMapperComPtr m_coords_mapper;

    DWORD m_wait_for_frames_interval;
    DWORD m_stream_event_handle_count;
    DWORD m_wait_all_succeeded_result;
    ObjectHandle m_depth_stream_handle;
    ObjectHandle m_color_stream_handle;
    EventHandle m_depth_next_event_handle;
    EventHandle m_color_next_event_handle;
    EventHandle m_stream_event_handles[2];

    ColorRgbxFrame m_color_frame;
    DepthU16Frame m_raw_depth_frame;
    utils::DepthF32Frame m_depth_frame;
    utils::Grayscale8Frame m_grayscale_frame;
    DepthU32Frame m_raw_depth_with_player_index_frame;

    IRawStreams::INotify::Vector m_notifies;

  private:
    static KinectRawStreams *s_instance;

  private:
    friend INotify;

  public:
    inline virtual utils::DepthF32Frame *getDepthF32Frame() override { return &m_depth_frame; }
    inline virtual utils::DepthF32Frame &getDepthF32FrameRef() override { return m_depth_frame; }
    inline virtual utils::Grayscale8Frame *getGrayscale8Frame() override { return &m_grayscale_frame; }
    inline virtual utils::Grayscale8Frame &getGrayscale8FrameRef() override { return m_grayscale_frame; }

  };
}