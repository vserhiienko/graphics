#pragma once
#include "SmartScanThread.h"
#include <GaiaVisualOdometry.h>

#include "Event.h"

namespace keel
{
  class VisualOdometryThread
    : public keel::SmartScanThread
    , public gaia::IRawStreams::INotify

  {
    bool termLoop;

  public:
    typedef std::shared_ptr<VisualOdometryThread> SharedPtr, Sp;

  public:
    gaia::VisualOdometry *processor;
    gaia::utils::DepthF32Frame *depthFrame;
    gaia::utils::Grayscale8Frame *grayscaleFrame;

  public:
    concurrency::event resumed;
    concurrency::event suspended;
    concurrency::event framesAcquired;
    concurrency::event framesProcessed;
    concurrency::task_group mainLoopTask;

  public:
    VisualOdometryThread(_In_ SmartScanApp*, _In_ gaia::IRawStreams*);
    virtual ~VisualOdometryThread(void);

    void onStreamsInitialized(_In_ keel::SmartScanThread*);
    void onStreamsReleased(_In_ keel::SmartScanThread*);
    void onStreamsSuspended(_In_ keel::SmartScanThread*);
    void onStreamsResumed(_In_ keel::SmartScanThread*);
    virtual void onFrameAcquired(gaia::IRawStreams const*);

  public:
    static VisualOdometryThread *createInstance(_In_ SmartScanApp*, _In_ gaia::IRawStreams*);

  private:
    void mainLoopRoutine(void);

  };
}

