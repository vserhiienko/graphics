#pragma once
#include "SmartScanThread.h"
#include "gaia.kine"

namespace keel
{
  class StreamingThread
    : public keel::SmartScanThread
  {
  public:
    typedef std::shared_ptr<StreamingThread> SharedPtr, Sp;
    enum DeviceType { kDeviceType_Kinect, kDeviceType_Senz };

  public:
    gaia::IRawStreams *streamer;
    concurrency::task_group mainLoopTask;

  public:
    StreamingThread(_In_ SmartScanApp*, _In_opt_ DeviceType = kDeviceType_Kinect);
    virtual ~StreamingThread(void);
    virtual void init(void) override;
    virtual void quit(void) override;
    virtual void resume(void) override;
    virtual void suspend(void) override;
    virtual void onFrameMove(_In_ dx::sim::BasicTimer const&) override;

  public:
    static StreamingThread *createInstance(_In_ SmartScanApp*, _In_opt_ DeviceType = kDeviceType_Kinect);

  private:
    void mainLoopRoutine(void);
  };
}
