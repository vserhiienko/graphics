#include "pch.h"
#include "gaia.creative.senz.raw.streams.h"

gaia::SenzRawStreams::SenzRawStreams() //: State(NULL)
{
  //m_color_dimensions.width = 320;
  //m_color_dimensions.height = 240;
  m_color_dimensions.width = 640;
  m_color_dimensions.height = 480;

  m_depth_dimensions.width = 320;
  m_depth_dimensions.height = 240;

  m_block_execution = false;
  m_is_instance_valid = false;
  m_was_state_resumed = false;
  m_was_device_disconnected = false;

  m_last_notify_count = 0;
  m_last_consumer_count = 0;
  m_uv_frame_byte_size = 0;
  m_vertex_frame_byte_size = 0;
  m_color24_frame_byte_size = 0;

  // TODO: remove these lines
  m_last_notify_count = 1;
  m_last_consumer_count = 1;
}

gaia::SenzRawStreams::~SenzRawStreams()
{
  quit();
}

void gaia::SenzRawStreams::init()
{
  m_pipeline = new UtilPipeline();

  if (m_pipeline && m_pipeline.IsValid())
  {
    // those we are using

    m_grayscale_stream_profile.frameRateMin.numerator = 30;
    m_grayscale_stream_profile.frameRateMax.numerator = 30;
    m_grayscale_stream_profile.frameRateMin.denominator = 1;
    m_grayscale_stream_profile.frameRateMax.denominator = 1;
    m_grayscale_stream_profile.imageInfo.width = m_color_dimensions.width;
    m_grayscale_stream_profile.imageInfo.height = m_color_dimensions.height;
    m_grayscale_stream_profile.imageInfo.format = PXCImage::COLOR_FORMAT_GRAY;

    m_depth_stream_profile.frameRateMin.numerator = 30;
    m_depth_stream_profile.frameRateMax.numerator = 30;
    m_depth_stream_profile.frameRateMin.denominator = 1;
    m_depth_stream_profile.frameRateMax.denominator = 1;
    m_depth_stream_profile.imageInfo.width = m_depth_dimensions.width;
    m_depth_stream_profile.imageInfo.height = m_depth_dimensions.height;
    m_depth_stream_profile.imageInfo.format = PXCImage::COLOR_FORMAT_DEPTH;

    // those we are not using

    m_color_stream_profile.frameRateMin.numerator = 30;
    m_color_stream_profile.frameRateMax.numerator = 30;
    m_color_stream_profile.frameRateMin.denominator = 1;
    m_color_stream_profile.frameRateMax.denominator = 1;
    m_color_stream_profile.imageInfo.width = m_color_dimensions.width;
    m_color_stream_profile.imageInfo.height = m_color_dimensions.height;
    m_color_stream_profile.imageInfo.format = PXCImage::COLOR_FORMAT_RGB24;

    m_vertex_stream_profile.frameRateMin.numerator = 30;
    m_vertex_stream_profile.frameRateMax.numerator = 30;
    m_vertex_stream_profile.frameRateMin.denominator = 1;
    m_vertex_stream_profile.frameRateMax.denominator = 1;
    m_vertex_stream_profile.imageInfo.width = m_depth_dimensions.width;
    m_vertex_stream_profile.imageInfo.height = m_depth_dimensions.height;
    m_vertex_stream_profile.imageInfo.format = PXCImage::COLOR_FORMAT_VERTICES;

    // no need to query ir map

    m_depth_stream_profile.imageOptions
      = PXCImage::IMAGE_OPTION_NO_IR_MAP;
    m_grayscale_stream_profile.imageOptions
      = PXCImage::IMAGE_OPTION_NO_IR_MAP;

    // unused

    m_vertex_stream_profile.imageOptions
      = PXCImage::IMAGE_OPTION_NO_IR_MAP;
    m_color_stream_profile.imageOptions
      = PXCImage::IMAGE_OPTION_NO_IR_MAP;

    // stream grayscale (8bit) instead of rgb24
    // stream depth instead of vertices

    m_pipeline->EnableImage(
      m_grayscale_stream_profile.imageInfo.format,
      m_grayscale_stream_profile.imageInfo.width,
      m_grayscale_stream_profile.imageInfo.height
      );
    m_pipeline->QueryCapture()->SetFilter(
      &m_grayscale_stream_profile // fps
      );
    m_pipeline->EnableImage(
      m_depth_stream_profile.imageInfo.format,
      m_depth_stream_profile.imageInfo.width,
      m_depth_stream_profile.imageInfo.height
      );
    m_pipeline->QueryCapture()->SetFilter(
      &m_depth_stream_profile // fps
      );

    //m_pipeline->EnableImage(
    //  m_color_stream_profile.imageInfo.format,
    //  m_color_stream_profile.imageInfo.width,
    //  m_color_stream_profile.imageInfo.height
    //  );
    //m_pipeline->QueryCapture()->SetFilter(
    //  &m_color_stream_profile // fps
    //  );
    //m_pipeline->EnableImage(
    //  m_vertex_stream_profile.imageInfo.format,
    //  m_vertex_stream_profile.imageInfo.width,
    //  m_vertex_stream_profile.imageInfo.height
    //  );
    //m_pipeline->QueryCapture()->SetFilter(
    //  &m_vertex_stream_profile // fps
    //  );

    if (
      m_pipeline->Init()
      && m_pipeline->QueryCapture()
      && m_pipeline->QueryCapture()->QueryDevice()
      )
    {
      // use raw depth data

      /*m_pipeline->QueryCapture()->QueryDevice()->SetProperty(
        PXCCapture::Device::PROPERTY_DEPTH_SMOOTHING,
        float(true)
        );*/
      m_pipeline->QueryCapture()->QueryDevice()->QueryProperty(
        PXCCapture::Device::PROPERTY_DEPTH_SATURATION_VALUE,
        &m_vertex_frame.invalids[0]
        );
      m_pipeline->QueryCapture()->QueryDevice()->QueryProperty(
        PXCCapture::Device::PROPERTY_DEPTH_LOW_CONFIDENCE_VALUE,
        &m_vertex_frame.invalids[1]
        );

      m_depth_frame.invalids[0] = m_vertex_frame.invalids[0];
      m_depth_frame.invalids[1] = m_vertex_frame.invalids[1];
      m_depth32_frame.invalids[0] = m_vertex_frame.invalids[0];
      m_depth32_frame.invalids[1] = m_vertex_frame.invalids[1];

      pxcUID projection_id;
      auto session = m_pipeline->QuerySession();
      auto device = m_pipeline->QueryCapture()->QueryDevice();
      device->QueryPropertyAsUID(PXCCapture::Device::PROPERTY_PROJECTION_SERIALIZABLE, &projection_id);
      if (session->DynamicCast<PXCMetadata>()->CreateSerializable<PXCProjection>(
        projection_id, &m_projection) != PXC_STATUS_NO_ERROR)
      {
        m_projection.ReleaseRef();
      }

      m_is_instance_valid = true;
      m_was_device_disconnected = false;

      unsigned depth_elements_count
        = m_depth_stream_profile.imageInfo.width
        * m_depth_stream_profile.imageInfo.height;
      unsigned color_elements_count
        = m_color_stream_profile.imageInfo.width
        * m_color_stream_profile.imageInfo.height;

      m_depth_frame.data.resize(depth_elements_count);
      m_depth_frame.dims.width = m_depth_stream_profile.imageInfo.width;
      m_depth_frame.dims.height = m_depth_stream_profile.imageInfo.height;

      m_depth32_frame.data.resize(depth_elements_count);
      m_depth32_frame.dims.width = m_depth_stream_profile.imageInfo.width;
      m_depth32_frame.dims.height = m_depth_stream_profile.imageInfo.height;

      m_grayscale_frame.data.resize(color_elements_count);
      m_grayscale_frame.dims.width = m_color_stream_profile.imageInfo.width;
      m_grayscale_frame.dims.height = m_color_stream_profile.imageInfo.height;

      m_depth_frame_byte_size = m_depth_frame.getSizeInBytes();

      m_uv_frame.data.resize(depth_elements_count);
      m_uv_frame.dims.width = m_depth_stream_profile.imageInfo.width;
      m_uv_frame.dims.height = m_depth_stream_profile.imageInfo.height;
      m_vertex_frame.data.resize(depth_elements_count);
      m_vertex_frame.dims.width = m_depth_stream_profile.imageInfo.width;
      m_vertex_frame.dims.height = m_depth_stream_profile.imageInfo.height;
      m_color24_frame.data.resize(color_elements_count);
      m_color24_frame.dims.width = m_color_stream_profile.imageInfo.width;
      m_color24_frame.dims.height = m_color_stream_profile.imageInfo.height;
      m_color24_frame.projection.resize(color_elements_count);
      m_color24_frame.projection_roi.width = m_color24_frame.dims.width;
      m_color24_frame.projection_roi.height = m_color24_frame.dims.height;
      m_color24_frame.projection_pitch = m_color24_frame.dims.width;

      m_uv_frame_byte_size = m_uv_frame.getSizeInBytes();
      m_vertex_frame_byte_size = m_vertex_frame.getSizeInBytes();
      m_color24_frame_byte_size = m_color24_frame.getSizeInBytes();
    }
  }

  if (!m_is_instance_valid)
  {
    quit();
  }
  else
  {
    // Perform at least one sample query
    // to start streaming...
    frameMove();
    ////Initted(this);
  }
}


bool gaia::SenzRawStreams::getCameraIntrinsicsParameters(
  DirectX::Vector2 &color_focal_lengths,
  DirectX::Vector2 &depth_focal_lengths,
  DirectX::Vector2 &color_principal_point,
  DirectX::Vector2 &depth_principal_point,
  DirectX::Vector2 &color_dimensions,
  DirectX::Vector2 &depth_dimensions
  )
{
  if (m_is_instance_valid)
    //if (m_pipeline.IsValid() && m_pipeline->QueryCapture())
  if (auto device = m_pipeline->QueryCapture()->QueryDevice())
  {
    PXCPointF32 dfl, dpp, cfl, cpp;
    if (device->QueryPropertyAsPoint(PXCCapture::Device::PROPERTY_DEPTH_FOCAL_LENGTH, &dfl) != PXC_STATUS_NO_ERROR) return false;
    if (device->QueryPropertyAsPoint(PXCCapture::Device::PROPERTY_COLOR_FOCAL_LENGTH, &cfl) != PXC_STATUS_NO_ERROR) return false;
    if (device->QueryPropertyAsPoint(PXCCapture::Device::PROPERTY_DEPTH_PRINCIPAL_POINT, &dpp) != PXC_STATUS_NO_ERROR) return false;
    if (device->QueryPropertyAsPoint(PXCCapture::Device::PROPERTY_COLOR_PRINCIPAL_POINT, &cpp) != PXC_STATUS_NO_ERROR) return false;

    color_focal_lengths.x = cfl.x;
    color_focal_lengths.y = cfl.y;
    depth_focal_lengths.x = dfl.x;
    depth_focal_lengths.y = dfl.y;
    color_principal_point.x = cpp.x;
    color_principal_point.y = cpp.y;
    depth_principal_point.x = dpp.x;
    depth_principal_point.y = dpp.y;
    color_dimensions.x = (float)m_color_dimensions.width;
    color_dimensions.y = (float)m_color_dimensions.height;
    depth_dimensions.x = (float)m_depth_dimensions.width;
    depth_dimensions.y = (float)m_depth_dimensions.height;

    return true;
  }

  return false;
}

bool gaia::SenzRawStreams::getAccelerometerReadings(
  DirectX::Vector3 &accel_readings
  )
{
  if (m_is_instance_valid)
    //if (m_pipeline.IsValid() && m_pipeline->QueryCapture())
  if (auto device = m_pipeline->QueryCapture()->QueryDevice())
  {
    if (device->QueryPropertyAs3DPoint(
      PXCCapture::Device::PROPERTY_ACCELEROMETER_READING,
      reinterpret_cast<PXCPoint3DF32*>(&accel_readings)
      ) == PXC_STATUS_NO_ERROR) return true;
  }

  return false;
}

void gaia::SenzRawStreams::quit()
{
  if (m_pipeline.IsValid())
  {
    m_pipeline->Close();

    if (auto pipeline = m_pipeline.ReleasePtr())
    {
      pipeline->Release();
    }

    ////Quitted(this);
  }
}

void gaia::SenzRawStreams::resume()
{
  m_was_state_resumed = true;
}

void gaia::SenzRawStreams::suspend()
{

  m_was_state_resumed = false;
}


void gaia::SenzRawStreams::frameMove()
{
  static auto notify_on_frame_move = [this](INotify::Pointer notify)
  {
    notify->OnFrameAcquired(this);
  };
  static auto notify_on_device_reconnected = [this](INotify::Pointer notify)
  {
    notify->OnDeviceReconnected(this);
  };
  static auto notify_on_device_disconnected = [this](INotify::Pointer notify)
  {
    notify->OnDeviceDisconnected(this);
  };

  if (m_is_instance_valid
    && m_last_notify_count)
  {
    if (m_pipeline->IsDisconnected())
    {
      if (!m_was_device_disconnected)
      {
        m_was_device_disconnected = true;

        std::_For_each(
          m_notifies.begin(),
          m_notifies.end(),
          notify_on_device_disconnected
          );
      }
    }
    else if (m_pipeline->AcquireFrame(m_block_execution))
    {
      if (m_was_device_disconnected)
      {
        m_was_device_disconnected = false;
        std::_For_each(
          m_notifies.begin(),
          m_notifies.end(),
          notify_on_device_reconnected
          );
      }

      auto color = m_pipeline->QueryImage(PXCImage::IMAGE_TYPE_COLOR);
      auto depth = m_pipeline->QueryImage(PXCImage::IMAGE_TYPE_DEPTH);

      if (color && depth)
      {
        if (color->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::COLOR_FORMAT_GRAY, &m_image_data) == PXC_STATUS_NO_ERROR)
        {
          // stream gray 8 only

          if (m_image_data.planes[0]) memcpy(m_grayscale_frame.getStorage(), m_image_data.planes[0], m_grayscale_frame.data.size());
          color->ReleaseAccess(&m_image_data);
        }

        if (depth->TryAccess(PXCImage::ACCESS_READ, &m_image_data) == PXC_STATUS_NO_ERROR)
        {
          // stream depth instead of vertices
          if (m_image_data.planes[0]) memcpy(m_depth_frame.getStorage(), m_image_data.planes[0], m_depth_frame_byte_size);
          //if (m_image_data.planes[0]) memcpy(m_vertex_frame.getStorage(), m_image_data.planes[0], m_vertex_frame_byte_size);
          if (m_image_data.planes[2]) memcpy(m_uv_frame.getStorage(), m_image_data.planes[2], m_uv_frame_byte_size);
          depth->ReleaseAccess(&m_image_data);
        }

        // stream gray 8 only
        // no need to convert
        // generateGrayscaleFrame();

        normalizeDepth();

        std::_For_each(
          m_notifies.begin(),
          m_notifies.end(),
          notify_on_frame_move
          );
      }

      m_pipeline->ReleaseFrame();
    }
  }
}

void gaia::SenzRawStreams::generateGrayscaleFrame()
{
  auto color = &m_color24_frame.data[0];
  auto grayscale = &m_grayscale_frame.data[0];
  auto pixel_count = m_color24_frame.getSizeU();
  concurrency::parallel_for(unsigned(0), pixel_count, [color, grayscale](unsigned i)
  {
    float
      r = float(color[i].r) / 255.0f,
      g = float(color[i].g) / 255.0f,
      b = float(color[i].b) / 255.0f;
    float gray
      = 0.299f * r
      + 0.587f * g
      + 0.114f * b;
    gray *= 255.0f;
    grayscale[i]
      = uint8_t(gray);
  });
}

void gaia::SenzRawStreams::normalizeDepth()
{
  auto depth16 = &m_depth_frame.data[0];
  auto depth32 = &m_depth32_frame.data[0];
  auto pixel_count = m_depth_frame.getSizeU();
  auto invalid = (int16_t)m_depth_frame.invalids[1];
  concurrency::parallel_for(unsigned(0), pixel_count, [depth16, depth32, invalid](unsigned i)
  {
    if (depth16[i] == invalid) depth32[i] = NAN;
    else depth32[i] = float(depth16[i]) / 1000.0f;
  });
}

#pragma region Notify
void gaia::SenzRawStreams::registerNotify(INotify::Pointer notify)
{
  INotify::Set unique_notifies(
    m_notifies.begin(),
    m_notifies.end()
    );
  unique_notifies.insert(notify);

  m_notifies.clear();
  m_notifies.insert(
    m_notifies.begin(),
    unique_notifies.begin(),
    unique_notifies.end()
    );

  m_last_notify_count
    = (unsigned)m_notifies.size();
}
void gaia::SenzRawStreams::unregisterNotify(INotify::Pointer notify)
{
  if (m_notifies.empty()) return;

  auto notify_iterator = std::find(
    m_notifies.begin(),
    m_notifies.end(),
    notify
    );

  if (notify_iterator != m_notifies.end())
  {
    m_notifies.erase(notify_iterator);
    m_last_notify_count
      = (unsigned)m_notifies.size();
  }
}
#pragma endregion
