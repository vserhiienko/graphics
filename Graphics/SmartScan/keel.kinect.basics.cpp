#include "pch.h"
#include "keel.kinect.basics.h"

Keel::KinectCore::Real const
Keel::KinectCore::DegreesToRadians = DirectX::XM_PI / 180.0f;

Keel::KinectCore::VideoStreamFrame::VideoStreamFrame()
{
	Bytes = nullptr;
	Width = 0u;
	Height = 0u;
	Stride = 0u;
	Size = 0u;
}

Keel::KinectCore::VideoStreamFrame::~VideoStreamFrame()
{
	Dealloc();
}

void Keel::KinectCore::VideoStreamFrame::Dealloc(
	)
{
	if (Bytes)
	{
		delete[] Bytes;
		Bytes = nullptr;
		Stride = 0u;
		Size = 0u;
	}
}

void Keel::KinectCore::VideoStreamFrame::Alloc(
	_In_ Dimension width,
	_In_ Dimension height,
	_In_ Dimension pixel
	)
{
	if (Bytes)
	{
		Dealloc();
	}

	if (Pixel == kUndefined || Pixel == 0) Pixel = pixel;
	if (Width == kUndefined || Width == 0) Width = width;
	if (Height == kUndefined || Height == 0) Height = height;
	Stride = Pixel * Width;
	Size = Stride * Height;
	Bytes = new Byte[Size];
	ZeroMemory(Bytes, Size);
}

bool Keel::KinectBasics::Succeeded(
	Keel::KinectBasics::ErrorCode const &s
	)
{
	return SUCCEEDED(s);
}

bool Keel::KinectBasics::Failed(
	Keel::KinectBasics::ErrorCode const &s
	)
{
	return FAILED(s);
}

Keel::KinectBasics::IFrame::IFrame(KinectBasics *host)
{
	::DWORD width, height;
	Resolution = kDefaultColorResolution;
	NuiImageResolutionToSize(Resolution, width, height);

	Host = host;
	FrameId = 0ull;
	Width = (KinectCore::Dimension)width;
	Height = (KinectCore::Dimension)height;
	StreamHandle = INVALID_HANDLE_VALUE;
	NextFrameEvent = INVALID_HANDLE_VALUE;
	Requirements = kNoRequirements;
	Scaling.x = ::tanf(NUI_CAMERA_DEPTH_NOMINAL_HORIZONTAL_FOV * KinectCore::DegreesToRadians * 0.5f) / (Width * 0.5f);
	Scaling.y = ::tanf(NUI_CAMERA_DEPTH_NOMINAL_VERTICAL_FOV * KinectCore::DegreesToRadians * 0.5f) / (Height * 0.5f);

	dx::trace(
		"Keel::KinectBasics::IFrame::IFrame(): Params [%d %d]\n",
		Width, Height
		);
}

Keel::KinectBasics::IFrame::~IFrame()
{
	dx::trace(
		"Keel::KinectBasics::IFrame::~IFrame()\n"
		);
}

Keel::KinectBasics::Boolean Keel::KinectBasics::IFrame::IsNextFrameReceived()
{
	return ::WaitForSingleObject(NextFrameEvent, 0) == WAIT_OBJECT_0;
}

Keel::KinectBasics::ColorFrame::ColorFrame(KinectBasics *host)
: Keel::KinectBasics::IFrame(host)
{
	Type = kDefaultColorType;
	Pixel = kBytesPerPixel;
	Requirements = NUI_INITIALIZE_FLAG_USES_COLOR;
	Format = ::DXGI_FORMAT_B8G8R8A8_UNORM;

	dx::trace(
		"Keel::KinectBasics::ColorFrame::ColorFrame(): Params [%d 0x%x 0x%x]\n",
		Pixel, Requirements, Type
		);
}

void Keel::KinectBasics::IFrame::Quit()
{
	if (NextFrameEvent != INVALID_HANDLE_VALUE)
	{
		CloseHandle(NextFrameEvent);
		NextFrameEvent = INVALID_HANDLE_VALUE;
		StreamHandle = INVALID_HANDLE_VALUE;
		dx::trace("Keel::KinectBasics::IFrame::Quit() [NextFrameEvent closed]\n");
	}
	else
	{
		dx::trace("Keel::KinectBasics::IFrame::Quit() [Ignored]\n");
	}
}

Keel::KinectBasics::DepthFrame::DepthFrame(KinectBasics *host)
: Keel::KinectBasics::IFrame(host)
{
	::DWORD width, height;

	Type = NUI_IMAGE_TYPE_DEPTH;
	Pixel = sizeof KinectBasics::Short;
	Format = ::DXGI_FORMAT_R16_SINT;
	Resolution = kDefaultDepthResolution;
	NuiImageResolutionToSize(Resolution, width, height);
	Width = (KinectCore::Dimension) width;
	Height = (KinectCore::Dimension) height;
	Requirements = NUI_INITIALIZE_FLAG_USES_DEPTH;

	dx::trace(
		"Keel::KinectBasics::DepthFrame::DepthFrame(): Params [%d 0x%x 0x%x]\n",
		Pixel, Requirements, Type
		);
}

Keel::KinectBasics::DepthPlayerFrame::DepthPlayerFrame(KinectBasics *host)
: DepthFrame(host)
{
	Type = NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX;
	Pixel = sizeof NUI_DEPTH_IMAGE_PIXEL;
	Format = ::DXGI_FORMAT_R32_SINT;
	Requirements = NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX;

	dx::trace(
		"Keel::KinectBasics::DepthPlayerFrame::DepthPlayerFrame(): Params [%d 0x%x 0x%x]\n",
		Pixel, Requirements, Type
		);
}

void Keel::KinectBasics::IFrame::Init()
{
	Alloc();
	NextFrameEvent = CreateEvent(
		NULL, TRUE, FALSE, NULL
		);
	dx::trace(
		"Keel::KinectBasics::IFrame::Init(): Params [0x%x 0x%x %d %d %d %d]\n",
		NextFrameEvent, Bytes, Width, Height, Stride, Pixel
		);
}

void Keel::KinectBasics::IFrame::Open()
{
	if (Host->Sensor.Get())
	{
		ErrorCode result = Host->Sensor->NuiImageStreamOpen(
			Type, Resolution, 0, 2,
			NextFrameEvent,
			&StreamHandle
			);

		dx::trace(
			"Keel::KinectBasics::IFrame::Open(): [ErrorCode %d]\n", result
			);
	}
	else
	{
		dx::trace(
			"Keel::KinectBasics::IFrame::Open(): [No connected devices found]\n"
			);
	}
}

void Keel::KinectBasics::IFrame::ProcessSensor()
{
	if (IsNextFrameReceived() // || TRUE
#if defined(DEBUG) || _DEBUG
		&& Host->Sensor.Get()
#endif
		)
	{
		//dx::trace(
		//	"Keel::KinectBasics::IFrame::ProcessSensor(): [Ready]\n"
		//	);

		HRESULT result;
		NUI_IMAGE_FRAME imageFrame;
		NUI_LOCKED_RECT lockedRect;

		result = Host->Sensor->NuiImageStreamGetNextFrame(
			StreamHandle, 0, &imageFrame
			);
		if (KinectBasics::Failed(result))
		{
			dx::trace(
				"Keel::KinectBasics::IFrame::ProcessSensor(): [Failed to acquire stream frame]\n"
				);
			return;
		}
		else
		{

			result = imageFrame.pFrameTexture->LockRect(0, &lockedRect, NULL, 0);
			if (KinectBasics::Failed(result))
			{
				dx::trace(
					"Keel::KinectBasics::IFrame::ProcessSensor(): [Failed to lock frame rect]\n"
					);
				return;
			}
			else
			{
				FrameId++;
				memcpy(Bytes, lockedRect.pBits, lockedRect.size);
				result = imageFrame.pFrameTexture->UnlockRect(0);

				/*dx::trace(
				"Keel::KinectBasics::IFrame::ProcessSensor(): [Frame %2d, FrameId %6d]\n", Type, (int)FrameId
				);*/
			}

			result = Host->Sensor->NuiImageStreamReleaseFrame(
				StreamHandle, &imageFrame
				);
		}
	}
	else
	{
		//dx::trace(
		//	"Keel::KinectBasics::IFrame::ProcessSensor(): [Not ready]\n"
		//	);
	}
}



void Keel::KinectBasics::DepthPlayerFrame::ProcessSensor()
{
	if (IsNextFrameReceived() // || TRUE
#if defined(DEBUG) || _DEBUG
		&& Host->Sensor.Get()
#endif
		)
	{
		HRESULT result;
		NUI_IMAGE_FRAME imageFrame;
		NUI_LOCKED_RECT lockedRect;
		Microsoft::WRL::ComPtr<INuiFrameTexture> frameTexture;

		result = Host->Sensor->NuiImageStreamGetNextFrame(
			StreamHandle, 0, &imageFrame
			);
		if (KinectBasics::Failed(result))
		{
			dx::trace(
				"Keel::KinectBasics::DepthPlayerFrame::ProcessSensor(): [Failed to acquire stream frame]\n"
				);
			return;
		}
		else
		{
			result = Host->Sensor.Get()->NuiImageFrameGetDepthImagePixelFrameTexture(
				StreamHandle, &imageFrame, nullptr, &frameTexture
				);
			if (KinectBasics::Failed(result))
			{
				dx::trace(
					"Keel::KinectBasics::DepthPlayerFrame::ProcessSensor(): [Failed to acquire frame texture]\n"
					);
				result = Host->Sensor->NuiImageStreamReleaseFrame(
					StreamHandle, &imageFrame
					);
				return;
			}

			result = frameTexture->LockRect(0, &lockedRect, NULL, 0);
			if (KinectBasics::Failed(result))
			{
				dx::trace(
					"Keel::KinectBasics::DepthPlayerFrame::ProcessSensor(): [Failed to lock frame rect]\n"
					);
				result = Host->Sensor->NuiImageStreamReleaseFrame(
					StreamHandle, &imageFrame
					);
				frameTexture = nullptr;
				return;
			}
			else
			{
				FrameId++;
				memcpy(Bytes, lockedRect.pBits, lockedRect.size);
				result = frameTexture->UnlockRect(0);

				/*dx::trace(
				"Keel::KinectBasics::DepthPlayerFrame::ProcessSensor(): [Frame %2d, FrameId %6d]\n", Type, (int)FrameId
				);*/
			}

			result = Host->Sensor->NuiImageStreamReleaseFrame(
				StreamHandle, &imageFrame
				);
		}
	}
	else
	{
		//dx::trace(
		//	"Keel::KinectBasics::IFrame::ProcessSensor(): [Not ready]\n"
		//	);
	}
}
Keel::KinectBasics::KinectBasics()
{
	m_has_been_quitted = FALSE;
	m_has_been_initted = FALSE;
	SensorRequirements = IFrame::kNoRequirements;

	dx::trace(
		"Keel::KinectBasics::KinectBasics()\n"
		);
}

void Keel::KinectBasics::Init()
{
	std::for_each(
		SubmittedFrames.begin(), SubmittedFrames.end(),
		[](IFrame *frame) { frame->Init(); }
	);

	m_has_been_initted = TRUE;

	dx::trace(
		"Keel::KinectBasics::Init(): [Openning Kinect sensor]\n"
		);
	CreateDeviceIndependentResources();
	if (Sensor.Get())
	{
		LastError = Sensor->NuiInitialize(SensorRequirements);
		dx::trace(
			"Keel::KinectBasics::Init(): [ErrorCode 0x%0x]\n", LastError
			);
	}
	else
	{
		dx::trace(
			"Keel::KinectBasics::Init(): [No connected device found]\n", LastError
			);
	}
}

void Keel::KinectBasics::Quit()
{
	if (Sensor.Get())
	{
		m_has_been_quitted = TRUE;

		dx::trace(
			"Keel::KinectBasics::Quit(): Shutting down Kinect sensor...\n"
			);
		Sensor->NuiShutdown();
		std::for_each(
			SubmittedFrames.begin(), SubmittedFrames.end(),
			[](IFrame *frame) { frame->Quit(); }
		);

		Sensor = nullptr;
		dx::trace(
			"Keel::KinectBasics::Quit(): [Ok]\n"
			);
	}
	else
	{
		dx::trace(
			"Keel::KinectBasics::Quit(): [Ignored]\n"
			);
	}
}

void Keel::KinectBasics::SubmitFrame(IFrame *frame)
{
	if (m_has_been_initted || !frame)
	{
		dx::trace(
			"Keel::KinectBasics::SubmitFrame(): Params"
			" [already initialized / request ignored]\n"
			);
	}
	else
	{
		SubmittedFrames.push_back(frame);
		SensorRequirements |= frame->Requirements;
		dx::trace(
			"Keel::KinectBasics::SubmitFrame(): Params [%d 0x%x 0x%x]\n",
			SubmittedFrames.size(), frame->Requirements, SensorRequirements
			);
	}
}

void Keel::KinectBasics::OpenAllFrames()
{
	if (Sensor.Get())
	{
		std::for_each(
			SubmittedFrames.begin(), SubmittedFrames.end(),
			[](IFrame *frame) { frame->Open(); }
		);
	}
	else
	{
		dx::trace(
			"Keel::KinectBasics::OpenAllFrames(): [No connected devices found]\n"
			);
	}
}

void Keel::KinectBasics::ProcessAllFrames()
{
	if (Sensor.Get())
	{
		std::for_each(
			SubmittedFrames.begin(),
			SubmittedFrames.end(),
			[](IFrame *frame) { frame->ProcessSensor(); }
		);
	}
	else
	{
		dx::trace(
			"Keel::KinectBasics::ProcessAllFrames(): [No connected devices found]\n"
			);
	}
}

void Keel::KinectBasics::CreateDeviceIndependentResources()
{
	int devicesFound;
	LastError = ::NuiGetSensorCount(&devicesFound);
	SensorDevicesFound = (KinectCore::Dimension) devicesFound;

	if (KinectBasics::Succeeded(LastError))
	for (KinectCore::Dimension deviceIndex = 0; deviceIndex < SensorDevicesFound; deviceIndex++)
	{
		LastError = ::NuiCreateSensorByIndex(
			deviceIndex, Sensor.ReleaseAndGetAddressOf()
			);
		if (KinectBasics::Failed(LastError)) return;

		LastError = Sensor->NuiStatus();
		if (KinectBasics::Succeeded(LastError)) break;
		else Sensor = nullptr;
	}

	dx::trace(
		"Keel::KinectBasics::CreateDeviceIndependentResources()\n\tParams [0x%x 0x%x]\n",
		Sensor.Get(), Sensor.Get() ? Sensor->NuiStatus() : KinectCore::kUndefined
		);
}


