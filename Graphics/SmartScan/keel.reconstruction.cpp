#include "pch.h"
#include "keel.reconstruction.h"
#include "keel.kinect.fusion.fbx.help.h"

Keel::Reconstruction::~Reconstruction() { }
Keel::Reconstruction::Reconstruction() : State(nullptr), m_fusionProcessor(this)
{
	Name = _Oasis_origin "keel/reconstruction";
}

void Keel::Reconstruction::Init()
{
	m_bHandleComletedFrame = false;

	m_offscreenViewer.Width = deviceResources.ActualRenderTargetSize.Width;
	m_offscreenViewer.Height = deviceResources.ActualRenderTargetSize.Height;
	m_offscreenViewer.AspectRatio = m_offscreenViewer.Width / m_offscreenViewer.Height;
	m_offscreenViewer.UpdateOrthographic();

	//auto air = Dove::Engine::CreateAirForCurrentThread();
	if (FAILED(m_fusionProcessor.StartProcessing()))
	{
		dx::trace(
			"Keel::Reconstruction::Init()"
			"[Failed to start kinect fusion processor]"
			);
	}
	else
	{
		dx::trace(
			"Keel::Reconstruction::Init()"
			"[Ok]"
			);
	}

	//Initted(this);
}

void Keel::Reconstruction::Quit()
{
	//auto air = Dove::Engine::CreateAirForCurrentThread();
	if (FAILED(m_fusionProcessor.StopProcessing()))
	{
		dx::trace(
			"Keel::Reconstruction::Quit()"
			"[Failed to stop kinect fusion processor]"
			);
	}

	//Quitted(this);
}

void Keel::Reconstruction::OnFrameMove()
{
	KinectFusionProcessor::Frame const *pFrame = NULL;
	////auto air = Dove::Engine::CreateAirForCurrentThread();

	if (m_bHandleComletedFrame.load())
	{
		m_bHandleComletedFrame = false;
		if (SUCCEEDED(m_fusionProcessor.LockFrame(&pFrame)))
		{
			if (m_fusionProcessor.IsVolumeInitialized() && pFrame->m_pDepthRGBX)
			{
				// Choose frame content to render
				// ...

				// Update texture content
				m_fusionColorOffscreen.GetOffscreenResourceRef().UpdateResourceContent(
					pFrame->m_pReconstructionRGBX, pFrame->m_cbImageSize
					//pFrame->m_pTrackingDataRGBX, pFrame->m_cbImageSize
					//pFrame->m_pDepthRGBX, pFrame->m_cbImageSize
					);

				/*dx::trace(
					"Keel::Reconstruction::OnFrameMove()[Fps %f]",
					pFrame->m_fFramesPerSecond
					);*/
			}

			m_fusionProcessor.UnlockFrame();
		}
	}

	// Render updated texture
	m_fusionRenderer.RenderFrame(
		&m_fusionColorOffscreen,
		&m_offscreenViewer,
		nullptr
		);
}

void Keel::Reconstruction::CreateDeviceResources()
{
	m_fusionRenderer.CreateDeviceResources();

	m_fusionDepthOffscreen.CreateDeviceResource(
		m_fusionProcessor.GetCurrentConfigurationByRef().m_cDepthWidth,
		m_fusionProcessor.GetCurrentConfigurationByRef().m_cDepthHeight
		);
	m_fusionColorOffscreen.CreateDeviceResource(
		m_fusionProcessor.GetCurrentConfigurationByRef().m_cDepthWidth,
		m_fusionProcessor.GetCurrentConfigurationByRef().m_cDepthHeight
		);

	//auto air = Dove::Engine::CreateAirForCurrentThread();
	dx::trace("Keel::Reconstruction::CreateDeviceResources()"
		"\n\t[ColorOffscreen {%d, %d} {0x%x, 0x%x}]"
		"\n\t[DepthOffscreen {%d, %d} {0x%x, 0x%x}]",
		m_fusionColorOffscreen.GetOffscreenResourceRef().m_d3dResourceDesc.Width,
		m_fusionColorOffscreen.GetOffscreenResourceRef().m_d3dResourceDesc.Height,
		m_fusionColorOffscreen.GetTexture(),
		m_fusionColorOffscreen.GetShaderResourceView(),
		m_fusionDepthOffscreen.GetOffscreenResourceRef().m_d3dResourceDesc.Width,
		m_fusionDepthOffscreen.GetOffscreenResourceRef().m_d3dResourceDesc.Height,
		m_fusionDepthOffscreen.GetTexture(),
		m_fusionDepthOffscreen.GetShaderResourceView()
		);

}

void Keel::Reconstruction::CreateDeviceIndependentResources()
{
	m_fusionRenderer.CreateDeviceIndependentResources();
}

void Keel::Reconstruction::CreateWindowSizeDependentResources()
{
	m_fusionRenderer.CreateWindowSizeDependentResources();
}


void Keel::Reconstruction::OnKeyPressed(_In_::UINT32 key)
{
	switch (key)
	{
	case dx::VirtualKey::Control:
		if (!m_bIsCtrlPressed.load())
		{
			m_bIsCtrlPressed.store(true);
		}
		break;
	}
}

void Keel::Reconstruction::SaveMesh()
{
	//auto air = Dove::Engine::CreateAirForCurrentThread();

	if (!m_bIsSavingMesh.load())
	{
		m_bIsSavingMesh = true;
		auto task = concurrency::create_task([this, air]()
		{
			auto previousValue = m_fusionConfiguration.m_bPauseIntegration;

			m_fusionConfiguration.m_bPauseIntegration = true;
			m_fusionProcessor.SetParams(m_fusionConfiguration);
			dx::trace("Keel::Reconstruction::SaveMesh()[Integration paused]");

			Microsoft::WRL::ComPtr<INuiFusionColorMesh> mesh;
			if (FAILED(m_fusionProcessor.CalculateMesh(mesh.GetAddressOf())))
			{
				dx::trace("Keel::Reconstruction::SaveMesh()[Mesh generation failed]");
				return;
			}

			if (FAILED(FbxHelp::WriteFbxMeshFile(mesh.Get())))
			{
				dx::trace("Keel::Reconstruction::SaveMesh()[Mesh saving failed]");
			}
			else
			{
				dx::trace("Keel::Reconstruction::SaveMesh()[Mesh was written successfully]");
			}

			m_fusionConfiguration.m_bPauseIntegration = previousValue;
			m_fusionProcessor.SetParams(m_fusionConfiguration);
			dx::trace("Keel::Reconstruction::SaveMesh()[Integration resumed]");

			m_bIsSavingMesh = false;
		});

		dx::trace("Keel::Reconstruction::SaveMesh()[Mesh generation task was launched]");
	}
	else
	{
		dx::trace("Keel::Reconstruction::SaveMesh()[Mesh saving in progress, please wait]");
	}
}

void Keel::Reconstruction::OnKeyReleased(_In_::UINT32 key)
{
	//auto air = Dove::Engine::CreateAirForCurrentThread();

	switch (key)
	{
	case dx::VirtualKey::Control:
		if (m_bIsCtrlPressed.load()) 
		{
			m_bIsCtrlPressed.store(false);
		}

		break;
	case dx::VirtualKey::R:
		if (m_bIsCtrlPressed.load())
		{
			m_fusionProcessor.ResetReconstruction();
		}

		break;
	case dx::VirtualKey::S:
		if (m_bIsCtrlPressed.load())
		{
			SaveMesh();
		}

		break;
	};

}

void Keel::Reconstruction::Resume()
{
	// Resume intergration
	// Handle camera

	m_fusionConfiguration.m_bPauseIntegration = false;
	m_fusionProcessor.SetParams(m_fusionConfiguration);

	//auto air = Dove::Engine::CreateAirForCurrentThread();
	dx::trace("Keel::Reconstruction::Resume()[Integration resumed]");

	//Resumed(this);
}

void Keel::Reconstruction::Suspend()
{
	// Suspend intergration
	// Handle camera

	m_fusionConfiguration.m_bPauseIntegration = true;
	m_fusionProcessor.SetParams(m_fusionConfiguration);

	//auto air = Dove::Engine::CreateAirForCurrentThread();
	dx::trace("Keel::Reconstruction::Suspend()[Integration suspended]");

	//Suspended(this);
}
