#pragma once
#include "pch.h"
#include "gaia.kinect.raw.streams.h"
#include "gaia.visual.odometry.h"

namespace keel
{
  class SmartScanApp
    : public dx::DxSampleApp
    , public dx::AlignedNew<SmartScanApp>
    , public gaia::VisualOdometry::FrameProcessedHook
  {

    bool hasUI;
    volatile bool scannerLaunched;
    TwBar *tweakBar;
    dx::Color &clearColor;

    std::string uiUrl;
    dx::sim::BasicTimer timer;

    gaia::KinectRawStreams *kinect;
    gaia::VisualOdometry *motionEst;

    concurrency::task<void> scannerTask;
    concurrency::cancellation_token_source scannerTaskCTS;

    // try to initialize ui
    void initializeUI(void);
    void launchScanning(void);
    void doneScanning(void);

  public:
    SmartScanApp();
    virtual ~SmartScanApp();

  public: // DxSampleApp
    virtual bool onAppLoop(void);
  public: // DxSampleApp
    virtual void handlePaint(dx::DxWindow const*);
    virtual void handleSizeChanged(dx::DxWindow const*);
    virtual void handleKeyReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseMoved(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseLeftPressed(dx::DxWindow const*, dx::DxGenericEventArgs const&);
    virtual void handleMouseLeftReleased(dx::DxWindow const*, dx::DxGenericEventArgs const&);

  public: // FrameProcessedHook
    virtual void onFrameProcessed(
      fovis::MotionEstimateStatusCode,
      fovis::VisualOdometry &
      );

    void toggleScanning(void);


  };
}
