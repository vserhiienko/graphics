#pragma once

#include "pch.h"
#include "keel.texture.resources.h"

namespace Keel
{
	/// <summary>
	/// Renders kinect frames using their spatial info
	/// </summary>
	class KinectFusionRenderer
	{
	public:

		typedef struct Vertex
		{
			dx::Vector4 Position;
			dx::Vector3 Normal;
			dx::Vector2 Coords;

		} VertexUnit;

		typedef struct PerFrame
		{
			Nena::Matrix World;
			Nena::Matrix CameraView;
			Nena::Matrix CameraProj;

		} PerFrameSource;

		typedef HRESULT HResult;
		typedef unsigned __int16 IndexUnit;
		typedef dx::Buffer Buffer;
		typedef dx::VSLayout InputLayout;
		typedef dx::PShader PixelShader;
		typedef dx::VShader VertexShader;
		typedef dx::GShader GeometryShader;
		typedef dx::SamplerState SamplerState;
		typedef dx::RasterizerState RasterizerState;
		typedef dx::BlendState BlendState;
		typedef dx::DepthStencil DepthStencil;
		typedef dx::ConstantBuffer<PerFrame> PerFrameBuffer;

		void CreateDeviceResources();
		void CreateDeviceIndependentResources();
		void CreateWindowSizeDependentResources();

		void RenderFrame(
			IKinectFusionFrame *pFrame, 
			Demo::Camera *pViewer,
			Nena::Matrix *pTransform
			);

		VertexShader Vs;		// vertex shader
		InputLayout Vi;			// input layout
		PixelShader Ps;			// pixel shader
		BlendState Bs;			// blending
		DepthStencil Ds;		// depth stencil state
		RasterizerState Rs;		// rasterizing
		SamplerState Ss;		// texture sampler

		PerFrame Pfs;			// constant buffer content
		PerFrameBuffer Pf;		// constant buffer

		// render in perspective
		// render in orthography


	};
}