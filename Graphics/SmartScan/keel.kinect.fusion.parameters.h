#pragma once
#include "pch.h"

namespace Keel
{

	enum KinectFusionMeshTypes
	{
		Stl = 0,
		Obj = 1,
		Ply = 2,
		Fbx = 2,
	};

	/// <summary>
	/// Parameters to control the behavior of the KinectFusionProcessor.
	/// </summary>
	struct KinectFusionParams
	{
		// Number of bytes per pixel (applies to both depth float and int-per-pixel color and raycast images)
		static const int BytesPerPixel = 4;

		/// <summary>
		/// Constructor
		/// </summary>
		KinectFusionParams();

		/// <summary>
		/// Indicates whether the current reconstruction volume is different than the one in the params.
		/// </summary>
		bool VolumeChanged(const KinectFusionParams& params);

		/// <summary>
		/// Reconstruction Initialization parameters
		/// </summary>
		int m_deviceIndex;
		NUI_FUSION_RECONSTRUCTION_PROCESSOR_TYPE m_processorType;

		/// <summary>
		/// Parameter to pause integration of new frames
		/// </summary>
		bool m_bPauseIntegration;

		/// <summary>
		/// Parameter to select the sensor's near mode
		/// </summary>
		bool m_bNearMode;

		/// <summary>
		/// Depth image resolution and size
		/// </summary>
		NUI_IMAGE_RESOLUTION m_depthImageResolution;
		int m_cDepthWidth;
		int m_cDepthHeight;
		int m_cDepthImagePixels;

		/// <summary>
		/// Color image resolution and size
		/// </summary>
		NUI_IMAGE_RESOLUTION m_colorImageResolution;
		int m_cColorWidth;
		int m_cColorHeight;
		int m_cColorImagePixels;

		/// <summary>
		/// The Kinect Fusion Volume Parameters
		/// </summary>
		NUI_FUSION_RECONSTRUCTION_PARAMETERS m_reconstructionParams;

		/// <summary>
		/// Parameter to enable automatic reset of the reconstruction when camera tracking is lost.
		/// Set to true in the constructor to enable auto reset on cResetOnNumberOfLostFrames
		/// number of lost frames, or false to never automatically reset on loss of camera tracking.
		/// </summary>
		bool m_bAutoResetReconstructionWhenLost;

		/// <summary>
		/// Parameter to enable automatic reset of the reconstruction when there is a large
		/// difference in timestamp between subsequent frames. This should usually be set true as 
		/// default to enable recorded .xed files to generate a reconstruction reset on looping of
		/// the playback or scrubbing, however, for debug purposes, it can be set false to prevent
		/// automatic reset on timeouts.
		/// </summary>
		bool m_bAutoResetReconstructionOnTimeout;

		/// <summary>
		/// Processing parameters
		/// </summary>
		float m_fMinDepthThreshold;
		float m_fMaxDepthThreshold;
		bool m_bMirrorDepthFrame;
		unsigned short m_cMaxIntegrationWeight;
		bool m_bDisplaySurfaceNormals;
		bool m_bCaptureColor;
		int							m_cColorIntegrationInterval;
		KinectFusionMeshTypes m_saveMeshType;
		unsigned int m_cDeltaFromReferenceFrameCalculationInterval;

		/// <summary>
		/// Here we set a high limit on the maximum residual alignment energy where we consider the tracking
		/// to have succeeded. Typically this value would be around 0.2f to 0.3f.
		/// (Lower residual alignment energy after tracking is considered better.)
		/// </summary>
		float m_fMaxAlignToReconstructionEnergyForSuccess;

		/// <summary>
		/// Here we set a low limit on the residual alignment energy, below which we reject a tracking
		/// success report and believe it to have failed. Typically this value would be around 0.005f, as
		/// values below this (i.e. close to 0 which is perfect alignment) most likely come from frames
		/// where the majority of the image is obscured (i.e. 0 depth) or mis-matched (i.e. similar depths
		/// but different scene or camera pose).
		/// </summary>
		float m_fMinAlignToReconstructionEnergyForSuccess;

		/// <summary>
		/// Parameter to translate the reconstruction based on the minimum depth setting.
		/// When set to false, the reconstruction volume +Z axis starts at the camera lens and extends
		/// into the scene. Setting this true in the constructor will move the volume forward along +Z
		/// away from the camera by the minimum depth threshold to enable capture of very small
		/// reconstruction volumes by setting a non-identity camera transformation in the
		/// ResetReconstruction call.
		/// Small volumes may work better when shifted, as the Kinect hardware has a minimum sensing
		/// limit of ~0.35m, inside which no valid depth is returned, hence it is difficult to
		/// initialize and track robustly when the majority of a small volume is inside this distance.
		/// </summary>
		bool m_bTranslateResetPoseByMinDepthThreshold;

		/// <summary>
		/// Parameter to enable automatic finding of camera pose when lost. This searches back through
		/// the camera pose history where key-frames and camera poses have been stored in the camera
		/// pose finder database to propose the most likely pose matches for the current camera input.
		/// </summary>
		bool m_bAutoFindCameraPoseWhenLost;

		/// <summary>
		/// Camera pose finder configuration parameters
		/// m_fCameraPoseFinderDistanceThresholdReject is a threshold used following the minimum distance 
		/// calculation between the input frame and the camera pose finder database. This calculated value
		/// between 0 and 1.0f must be less than or equal to the threshold in order to run the pose finder,
		/// as the input must at least be similar to the pose finder database for a correct pose to be
		/// matched. m_fCameraPoseFinderDistanceThresholdAccept is a threshold passed to the ProcessFrame 
		/// function in the camera pose finder interface. The minimum distance between the input frame and
		/// the pose finder database must be greater than or equal to this value for a new pose to be 
		/// stored in the database, which regulates how close together poses are stored in the database.
		/// </summary>
		unsigned int m_cCameraPoseFinderProcessFrameCalculationInterval;
		unsigned int m_cMaxCameraPoseFinderPoseHistory;
		unsigned int m_cCameraPoseFinderFeatureSampleLocationsPerFrame;
		float m_fMaxCameraPoseFinderDepthThreshold;
		float m_fCameraPoseFinderDistanceThresholdReject;
		float m_fCameraPoseFinderDistanceThresholdAccept;
		unsigned int m_cMaxCameraPoseFinderPoseTests;
		unsigned int m_cMinSuccessfulTrackingFramesForCameraPoseFinder;
		unsigned int m_cMinSuccessfulTrackingFramesForCameraPoseFinderAfterFailure;

		/// <summary>
		/// Here we set a high limit on the maximum residual alignment energy where we consider the tracking
		/// with AlignPointClouds to have succeeded. Typically this value would be around 0.005f to 0.006f.
		/// (Lower residual alignment energy after relocalization is considered better.)
		/// </summary>
		float m_fMaxAlignPointCloudsEnergyForSuccess;

		/// <summary>
		/// Here we set a low limit on the residual alignment energy, below which we reject a tracking
		/// success report from AlignPointClouds and believe it to have failed. This can typically be around 0.
		/// </summary>
		float m_fMinAlignPointCloudsEnergyForSuccess;

		/// <summary>
		/// Camera pose finder AlignPointClouds Camera Tracking related parameters
		/// </summary>
		unsigned int m_cAlignPointCloudsImageDownsampleFactor;
		unsigned int m_cSmoothingKernelWidth;
		float m_fSmoothingDistanceThreshold;
		float m_fMaxTranslationDelta;
		float m_fMaxRotationDelta;
	};

	
}