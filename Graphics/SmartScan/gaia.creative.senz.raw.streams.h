#pragma once
#include <pxcmetadata.h>
#include <util_pipeline.h>
#include <pxcprojection.h>
#include "gaia.raw.streams.h"

namespace gaia
{

  /// <summary>
  /// This application state provides synchronized depth and color frames acquisition
  /// </summary>
  class SenzRawStreams
    //: public medusa::Oasis::State
    : public IRawStreams
  {
    /// <summary>
    /// The 24-bit RGB24 color format. On little endian machines, the memory layout is BGR.
    /// </summary>
    struct rgb24 { union { struct { uint8_t b, g, r; }; uint8_t bgr[3]; }; };

    /// <summary>
    /// Each pixel contains three 16 - bit integers, representing the world coordinates(x, y, and depth value).
    /// </summary>
    struct vertex { int16_t x, y, depth; };

    /// <summary>
    /// The 16-bit depth format.
    /// </summary>
    struct Depth16Frame : public utils::FrameBase<int16_t> 
    {
      float invalids[2];
    };

    /// <summary>
    /// Each pixel contains three 16-bit integers, representing the world coordinates (x, y, and depth value).
    /// </summary>
    struct VertexFrame : public utils::FrameBase<vertex>
    {
      float invalids[2];
    };

    /// <summary>
    /// Each pixel contains two 32-bit floating point values in the range of 0-1, 
    /// representing the mapping of depth coordinates to the color coordinates. 
    /// The UV map values are resolution normalized.
    /// </summary>
    struct UVFrame : public utils::FrameBase<DirectX::Vector2> {};

    /// <summary>
    /// The 24-bit RGB24 color format.
    /// On little endian machines, the memory layout is BGR.
    /// </summary>
    struct Color24Frame : public utils::FrameBase<rgb24>
    {
      std::vector<DirectX::Vector2> projection;
      PXCSizeU32 projection_roi;
      unsigned projection_pitch;

    };

  public:

    ~SenzRawStreams();

    virtual void init();
    virtual void quit();
    virtual void resume();
    virtual void suspend();
    virtual void frameMove();

    virtual void registerNotify(
      INotify::Pointer notify
      );
    virtual void unregisterNotify(
      INotify::Pointer notify
      );

  public:
    virtual bool getCameraIntrinsicsParameters(
      DirectX::Vector2 &color_focal_lengths,
      DirectX::Vector2 &depth_focal_lengths,
      DirectX::Vector2 &color_principal_point,
      DirectX::Vector2 &depth_principal_point,
      DirectX::Vector2 &color_dimensions,
      DirectX::Vector2 &depth_dimensions
      );
    virtual bool getAccelerometerReadings(
      DirectX::Vector3 &accel_readings
      );

  protected:
    SenzRawStreams();
    void normalizeDepth();
    void generateGrayscaleFrame();

    bool m_block_execution;
    bool m_is_instance_valid;
    bool m_was_state_resumed;
    bool m_was_device_disconnected;

    UVFrame m_uv_frame;
    VertexFrame m_vertex_frame;
    Depth16Frame m_depth_frame;
    Color24Frame m_color24_frame;

    utils::DepthF32Frame m_depth32_frame;
    utils::Grayscale8Frame m_grayscale_frame;

    unsigned m_last_notify_count;
    unsigned m_last_consumer_count;
    size_t m_uv_frame_byte_size;
    size_t m_depth_frame_byte_size;
    size_t m_vertex_frame_byte_size;
    size_t m_color24_frame_byte_size;

    PXCImage::ImageData m_image_data;
    D2D1_SIZE_U m_color_dimensions;
    D2D1_SIZE_U m_depth_dimensions;
    PXCSmartPtr<UtilPipeline> m_pipeline;
    PXCSmartPtr<PXCProjection> m_projection;
    PXCCapture::VideoStream::DataDesc m_video_stream_desc;
    PXCCapture::VideoStream::ProfileInfo m_depth_stream_profile;
    PXCCapture::VideoStream::ProfileInfo m_vertex_stream_profile;
    PXCCapture::VideoStream::ProfileInfo m_color_stream_profile;
    PXCCapture::VideoStream::ProfileInfo m_grayscale_stream_profile;

    IRawStreams::INotify::Vector m_notifies;

  private:
    static SenzRawStreams *s_instance;

  private:
    friend INotify;

  public:

    inline virtual utils::DepthF32Frame *getDepthF32Frame() override { return &m_depth32_frame; }
    inline virtual utils::DepthF32Frame &getDepthF32FrameRef() override { return m_depth32_frame; }
    inline virtual utils::Grayscale8Frame *getGrayscale8Frame() override { return &m_grayscale_frame; }
    inline virtual utils::Grayscale8Frame &getGrayscale8FrameRef() override { return m_grayscale_frame; }

    inline UVFrame *getUVFrame() { return &m_uv_frame; }
    inline UVFrame &getUVFrameRef() { return m_uv_frame; }
    inline VertexFrame *getVertexFrame() { return &m_vertex_frame; }
    inline VertexFrame &getVertexFrameRef() { return m_vertex_frame; }
    inline Depth16Frame *getDepth16Frame() { return &m_depth_frame; }
    inline Depth16Frame &getDepth16FrameRef() { return m_depth_frame; }
    inline Color24Frame *getColor24Frame() { return &m_color24_frame; }
    inline Color24Frame &getColor24FrameRef() { return m_color24_frame; }
  };

}