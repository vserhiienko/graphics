#pragma once
#include "pch.h"

namespace gaia
{
  namespace utils
  {
    /// <summary>
    /// Provides some common methods for image frames
    /// </summary>
    template <class _TyPixel>
    struct FrameBase
    {
      /// Types

      typedef _TyPixel Pixel;
      typedef std::vector<_TyPixel> PixelVector;
      static size_t const PixelSize = sizeof(_TyPixel);
      static unsigned const PixelSizeU = sizeof(_TyPixel);

      /// Data

      PixelVector data;
      D2D1_SIZE_U dims;

      FrameBase() : data()
      {
        dims.width = 0, dims.height = 0;
      }

      /// Funcs

      inline _TyPixel *operator[](unsigned r)
      {
        return &data[0] + r * dims.height;
      }

      inline void *getStorage() { return (void *)(&data[0]); }
      inline uint8_t *getStorageInBytes() { return (uint8_t *)(&data[0]); }

      inline size_t getSize() { return data.size(); }
      inline unsigned getSizeU() { return (unsigned)data.size(); }
      inline size_t getSizeInBytes() { return getSize() * PixelSize; }
      inline unsigned getSizeInBytesU() { return getSizeU() * PixelSizeU; }

      inline size_t getPitch() { return dims.width * PixelSize; }
      inline unsigned getPitchU() { return dims.width * PixelSizeU; }
    };

    /// <summary>
    /// The 32-bit coords 2d format.
    /// </summary>
    struct CoordsL32
    {
      long x, y;
    };

    /// <summary>
    /// The 32-bit 2d coords format.
    /// </summary>
    struct ColorCoordsFrame : public FrameBase<CoordsL32> {};

    /// <summary>
    /// The 32-bit float depth format.
    /// </summary>
    struct DepthF32Frame : public FrameBase<float>
    {
      float invalids[2];
    };

    /// <summary>
    /// The 8-bit grayscale format.
    /// </summary>
    struct Grayscale8Frame : public FrameBase<uint8_t> 
    {
      ColorCoordsFrame depth_projection;
      bool depth_projection_is_available;
    };

  }
}