#pragma once
#include "pch.h"

namespace keel
{
  namespace utils
  {
    class Event
    {
    public:
      bool canReset;
      std::string eventName;
      dx::Handle eventHandle;

      Event(std::string const &name)
      {
        eventHandle = 0;
      }

      void create(bool manualReset = false, bool signaled = false)
      {
        if (eventHandle = CreateEvent(0, manualReset, signaled, eventName.empty() ? 0 : eventName.c_str()))
        {
          canReset = manualReset;
        }
      }

      void reset()
      {
        if (canReset)
        {
          ResetEvent(eventHandle);
        }
      }

      void signal()
      {
        SetEvent(eventHandle);
      }

      void close()
      {
        if (eventHandle)
        {
          CloseHandle(eventHandle);
        }
      }

      bool await(DWORD ms = 0)
      {
        if (eventHandle)
        {
          switch (WaitForSingleObject(eventHandle, ms))
          {
          case WAIT_OBJECT_0: return true;
            // catch error here
            // - timeout
            // - fail
            // - ...
          }
        }

        return false;
      }

    };
  }
}
