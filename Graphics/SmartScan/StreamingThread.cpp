#include "pch.h"
#include "StreamingThread.h"

#include <GaiaKinectStreams.h>
#include <GaiaCreativeSenzStreams.h>

keel::StreamingThread *keel::StreamingThread::createInstance(SmartScanApp *app, DeviceType deviceType)
{
  assert(!!app);
  static auto instance = new StreamingThread(app, deviceType);
  return instance;
}

keel::StreamingThread::StreamingThread(SmartScanApp *app, DeviceType deviceType)
  : SmartScanThread(app, "keel/smartScan/streaming")
{
  dx::trace("%s: instance created", name.c_str());

  switch (deviceType)
  {
  case keel::StreamingThread::kDeviceType_Kinect: 
    dx::trace("%s: device = kinect", name.c_str());
    streamer = new gaia::KinectRawStreams();
    break;
  case keel::StreamingThread::kDeviceType_Senz: 
    dx::trace("%s: device = senz", name.c_str());
    streamer = new gaia::SenzRawStreams();
    break;
  }
}

keel::StreamingThread::~StreamingThread()
{
}

void keel::StreamingThread::init()
{
  streamer->init();
  SmartScanThread::init();

  //resume();
}

void keel::StreamingThread::quit()
{
  suspend();

  streamer->quit();
  SmartScanThread::quit();
}

void keel::StreamingThread::onFrameMove(dx::sim::BasicTimer const &timer)
{
  streamer->update();
}

void keel::StreamingThread::resume()
{
  //mainLoopTask.run([this]() { mainLoopRoutine(); });
  SmartScanThread::resume();
}

void keel::StreamingThread::suspend()
{
  dx::trace("%s: suspending...", name.c_str());
  dx::trace("%s: - cancelling...", name.c_str());
  mainLoopTask.cancel();
  mainLoopTask.wait();
  dx::trace("%s: - suspending deps...", name.c_str());
  SmartScanThread::suspend();
  dx::trace("%s: suspended", name.c_str());
}

void keel::StreamingThread::mainLoopRoutine()
{
  while (
    !concurrency::is_current_task_group_canceling()
    || !concurrency::is_task_cancellation_requested()
    )
  {
    streamer->update();
  }
}
