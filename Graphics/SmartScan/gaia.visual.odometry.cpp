#include "pch.h"
#include "gaia.visual.odometry.h"

gaia::VisualOdometry::VisualOdometry(IRawStreams *streams) //: State(NULL)
{
    m_streams = streams;
    m_vis_odom_core = NULL;
    m_is_instance_valid = false;
    frameProcessedHook = 0;
}

gaia::VisualOdometry::~VisualOdometry()
{
    //quit();
}

void gaia::VisualOdometry::OnRawStreamsServiceInitialized(IRawStreams*state)
{
    if (m_streams == dynamic_cast<IRawStreams*>(state))
    {
        DirectX::Vector2 dfl, cfl, dpp, cpp, dd, cd;
        if (m_streams->getCameraIntrinsicsParameters(
            cfl, dfl, cpp, dpp, cd, dd
            ))
        {
            m_depth_camera_params.cx = dpp.x;
            m_depth_camera_params.cy = dpp.y;
            m_depth_camera_params.fx = dfl.x;
            m_depth_camera_params.fy = dfl.y;
            m_depth_camera_params.width = (int)dd.x;
            m_depth_camera_params.height = (int)dd.y;

            m_color_camera_params.cx = cpp.x;
            m_color_camera_params.cy = cpp.y;
            m_color_camera_params.fx = cfl.x;
            m_color_camera_params.fy = cfl.y;
            m_color_camera_params.width = (int)cd.x;
            m_color_camera_params.height = (int)cd.y;

            m_vis_odom_opts = fovis::VisualOdometry::getDefaultOptions();
            m_rect.reset(new fovis::Rectification(m_color_camera_params));
            m_vis_odom_core.reset(new fovis::VisualOdometry(m_rect.get(), m_vis_odom_opts));

            m_depth_image.reset(new DepthImage2(this));

            m_is_instance_valid = !!m_vis_odom_core.get();
        }

        if (m_is_instance_valid && m_streams)
            m_streams->registerNotify(this);
    }
}

void gaia::VisualOdometry::OnRawStreamsServiceReleased(IRawStreams*state)
{
    if (m_streams == dynamic_cast<IRawStreams*>(state))
    {
        if (m_is_instance_valid && m_streams)
            m_streams->unregisterNotify(this);
    }
}

void gaia::VisualOdometry::setFrameProcessedHook(FrameProcessedHook *newHook)
{
    frameProcessedHook = newHook;
}

void gaia::VisualOdometry::OnFrameAcquired(
    gaia::utils::Grayscale8Frame &grayscale,
    gaia::utils::DepthF32Frame &depth
    )
{
    if (m_is_instance_valid)
    {
        m_depth_image->OnFrameAcquired(depth);
        m_vis_odom_core->processFrame(&grayscale.data[0], m_depth_image.get());

        auto status = m_vis_odom_core->getMotionEstimateStatus();
        if (frameProcessedHook)
        {
            frameProcessedHook->onFrameProcessed(
                status, (*m_vis_odom_core)
                );
        }
        else
        {
            auto iso_pose = m_vis_odom_core->getPose();
            auto pose_xyz = iso_pose.translation();
            auto pose_rpy = iso_pose.rotation().eulerAngles(0, 1, 2);

            auto iso_motion = m_vis_odom_core->getMotionEstimate();
            auto motion_xyz = iso_motion.translation();
            auto motion_rpy = iso_motion.rotation().eulerAngles(0, 1, 2);
            auto frame = m_vis_odom_core->getTargetFrame();

            if (
                pose_xyz(0) != 0.0f
                || pose_xyz(1) != 0.0f
                || pose_xyz(2) != 0.0f
                || status != fovis::MotionEstimateStatusCode::INSUFFICIENT_INLIERS
                )
            {
                dx::trace(
                    "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f"
                    //"\tm(iso): t [%.2f %.2f %.2f] o [%.2f %.2f %.2f]"
                    //, status
                    , pose_xyz(0)
                    , pose_xyz(1)
                    , pose_xyz(2)
                    , pose_rpy(0) * 180 / M_PI
                    , pose_rpy(1) * 180 / M_PI
                    , pose_rpy(2) * 180 / M_PI
                    /*, motion_xyz(0)
                    , motion_xyz(1)
                    , motion_xyz(2)
                    , motion_rpy(0) * 180 / M_PI
                    , motion_rpy(1) * 180 / M_PI
                    , motion_rpy(2) * 180 / M_PI*/
                    );
                //dx::trace(
                //    "VisualOdometry::OnFrameAcquired(): code %u "
                //    "pose(iso): t [%.2f %.2f %.2f] o [%.2f %.2f %.2f]"
                //    //"\tm(iso): t [%.2f %.2f %.2f] o [%.2f %.2f %.2f]"
                //    , status
                //    , pose_xyz(0)
                //    , pose_xyz(1)
                //    , pose_xyz(2)
                //    , pose_rpy(0) * 180 / M_PI
                //    , pose_rpy(1) * 180 / M_PI
                //    , pose_rpy(2) * 180 / M_PI
                //    /*, motion_xyz(0)
                //    , motion_xyz(1)
                //    , motion_xyz(2)
                //    , motion_rpy(0) * 180 / M_PI
                //    , motion_rpy(1) * 180 / M_PI
                //    , motion_rpy(2) * 180 / M_PI*/
                //    );
            }
        }
    }

}

void gaia::VisualOdometry::OnFrameAcquired(IRawStreams *streams)
{
    OnFrameAcquired(
        streams->getGrayscale8FrameRef(),
        streams->getDepthF32FrameRef()
        );
}

//void gaia::VisualOdometry::OnDeviceReconnected(SenzRawStreams *streams)
//{
//
//}
//
//void gaia::VisualOdometry::OnDeviceDisconnected(SenzRawStreams *streams)
//{
//
//}
