#include "pch.h"
#include "keel.kinect.fusion.renderer.h"

#include "itv.oasis.h"

void Keel::KinectFusionRenderer::CreateDeviceIndependentResources()
{

}

void Keel::KinectFusionRenderer::CreateDeviceResources()
{

	using namespace dx;

	HResult result;
	auto &x = Keel::Oasis::GetForCurrentThread()->Context;
	auto &d3d = x.Engine->Graphics.d3d;

	SamplerStateDescription samplerDesc;
	Resources::Assist::Defaults(samplerDesc);
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	VSInputElementDescription layout_element[3] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 16
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 28, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // + 12 (28)
	};

	Resources::RasterizerDescription rasterizerStateDesc;
	Resources::BlendStateDescription blendStateDescription;
	Dove::Platform::ZeroValue(&rasterizerStateDesc);
	Dove::Platform::ZeroValue(&blendStateDescription);

	blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;
	blendStateDescription.RenderTarget[0].BlendEnable = TRUE;

	rasterizerStateDesc.AntialiasedLineEnable = false;
	rasterizerStateDesc.FrontCounterClockwise = false;
	rasterizerStateDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerStateDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerStateDesc.CullMode = D3D11_CULL_BACK;
	rasterizerStateDesc.MultisampleEnable = false;
	rasterizerStateDesc.DepthClipEnable = true;
	rasterizerStateDesc.ScissorEnable = false;
	rasterizerStateDesc.DepthBiasClamp = 0.0f;
	rasterizerStateDesc.DepthBias = 0;

	//rasterizerStateDesc.FrontCounterClockwise = true;

	DepthStencilDescription depthDisabledStencilDesc;
	Resources::Assist::Defaults(depthDisabledStencilDesc);
	depthDisabledStencilDesc.DepthEnable = false;
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	result = Assist::LoadShader(
		d3d.Device.Get(),
		L"OffscreenPS.cso",
		Ps.ReleaseAndGetAddressOf()
		);
	result = Assist::LoadShader(
		d3d.Device.Get(),
		L"OffscreenVS.cso",
		layout_element, ARRAYSIZE(layout_element),
		Vs.ReleaseAndGetAddressOf(),
		Vi.ReleaseAndGetAddressOf()
		);
	result = d3d.Device->CreateSamplerState(
		&samplerDesc, Ss.ReleaseAndGetAddressOf()
		);
	result = d3d.Device.Get()->CreateDepthStencilState(
		&depthDisabledStencilDesc,
		Ds.ReleaseAndGetAddressOf()
		);
	result = d3d.Device.Get()->CreateRasterizerState1(
		&rasterizerStateDesc,
		Rs.ReleaseAndGetAddressOf()
		);
	result = d3d.Device.Get()->CreateBlendState1(
		&blendStateDescription,
		Bs.ReleaseAndGetAddressOf()
		);
	result = Pf.Create(d3d.Device.Get());
	Pfs.World = Nena::Matrix::Identity();

}

void Keel::KinectFusionRenderer::CreateWindowSizeDependentResources()
{
	/*auto &d3d
		= Keel::Oasis::GetForCurrentThread()
		->Context.Engine->Graphics.d3d;

	Target.CreateWindowSizeDependentResources(
		d3d.ActualRenderTargetSize.Width,
		d3d.ActualRenderTargetSize.Height,
		false, false
		);*/
}

void Keel::KinectFusionRenderer::RenderFrame(
	IKinectFusionFrame *frame
	, Demo::Camera *camera
	, Nena::Matrix *world
	)
{
	using namespace dx;
	using namespace dx;

	static const auto mask = 0xffffffffu;
	static const FLOAT factor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	static const UINT32 offsets[] = { 0, 0 };
	static const UINT32 strides[] = { sizeof Vertex, sizeof Vertex };
	static const UINT32 stencil_ref = 0x01;
	static auto &d3d = Keel::Oasis::GetForCurrentThread()->Context.Engine->Graphics.d3d;

	auto context = d3d.Context.Get();

	Pfs.World = world ? (*world) : Nena::Matrix::Identity();
	Pfs.CameraProj = camera->Orthographic;
	Pfs.CameraView = camera->View;

	Pf.SetData(context, Pfs);

	auto offscreen = frame->GetOffscreenResource();

	context->IASetPrimitiveTopology(::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, offscreen->Vb.GetAddressOf(), strides, offsets);
	context->IASetIndexBuffer(offscreen->Ib.Get(), ::DXGI_FORMAT_R16_UINT, 0);
	context->IASetInputLayout(Vi.Get());

	context->VSSetShader(Vs.Get(), nullptr, 0);
	context->PSSetShader(Ps.Get(), nullptr, 0);

	context->VSSetConstantBuffers(0, 1, Pf.GetBufferAddress());
	context->PSSetShaderResources(0, 1, offscreen->m_d3dReadResource.Read.GetAddressOf());
	context->PSSetSamplers(0, 1, Ss.GetAddressOf());

	context->RSSetState(Rs.Get());
	//context->OMSetBlendState(Bs.Get(), factor, mask);
	//context->OMSetDepthStencilState(Ds.Get(), stencil_ref);

	context->DrawIndexed(6, 0, 0);

	context->PSSetShader(nullptr, nullptr, 0);
	context->VSSetShader(nullptr, nullptr, 0);
	//context->OMSetBlendState(nullptr, factor, mask);
	//context->OMSetDepthStencilState(nullptr, stencil_ref);
}