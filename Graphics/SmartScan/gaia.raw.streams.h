#pragma once
#include "pch.h"
#include "gaia.utils.frames.h"

namespace gaia
{
  class IRawStreams
  {
  public:
    typedef IRawStreams *Pointer;
    typedef std::set<IRawStreams::Pointer> Set;
    typedef std::vector<IRawStreams::Pointer> Vector;

    /// <summary>
    /// Interface that provides methods to trace streaming state
    /// </summary>
    class INotify
    {
    public:
      typedef INotify *Pointer;
      typedef std::set<INotify::Pointer> Set;
      typedef std::vector<INotify::Pointer> Vector;

    public:
      virtual void OnFrameAcquired(IRawStreams*) = 0;
      virtual void OnDeviceReconnected(IRawStreams*) {};
      virtual void OnDeviceDisconnected(IRawStreams*) {};
    };

  public:

    virtual void registerNotify(INotify::Pointer notify) = 0;
    virtual void unregisterNotify(INotify::Pointer notify) = 0;

  public:
    virtual utils::DepthF32Frame *getDepthF32Frame() = 0;
    virtual utils::DepthF32Frame &getDepthF32FrameRef() = 0;
    virtual utils::Grayscale8Frame *getGrayscale8Frame() = 0;
    virtual utils::Grayscale8Frame &getGrayscale8FrameRef() = 0;

  public:
    virtual bool getCameraIntrinsicsParameters(
      DirectX::Vector2 &color_focal_lengths,
      DirectX::Vector2 &depth_focal_lengths,
      DirectX::Vector2 &color_principal_point,
      DirectX::Vector2 &depth_principal_point,
      DirectX::Vector2 &color_dimensions,
      DirectX::Vector2 &depth_dimensions
      ) PURE;
    virtual bool getAccelerometerReadings(
      DirectX::Vector3 &accel_readings
      ) PURE;

  };
}
