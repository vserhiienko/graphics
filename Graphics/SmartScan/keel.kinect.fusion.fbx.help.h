#pragma once
#include "pch.h"

namespace Keel
{
	namespace FbxHelp
	{
		HRESULT WriteFbxMeshFile(
			INuiFusionColorMesh *pFusionMesh, 
			LPOLESTR lpOleFileName = NULL
			);
	}
}