#include "pch.h"
#include "StreamingThread.h"
#include "VisualOdometryThread.h"

keel::VisualOdometryThread *keel::VisualOdometryThread::createInstance(
  SmartScanApp *app, gaia::IRawStreams *streams
  )
{
  assert(!!app && !!streams); // ensure app and steam provider instances are provided
  static auto instance = new keel::VisualOdometryThread(app, streams);
  return instance;
}

keel::VisualOdometryThread::VisualOdometryThread(SmartScanApp *app, gaia::IRawStreams *streams)
  : SmartScanThread(app, "keel/smartScan/visualOdometry")
  , termLoop(false)
{
  dx::trace("%s: instance created", name.c_str());
}

keel::VisualOdometryThread::~VisualOdometryThread()
{
  dx::trace("%s: instance destroyed", name.c_str());
}

void keel::VisualOdometryThread::onStreamsInitialized(keel::SmartScanThread *thread)
{
  if (auto streamingThread = dynamic_cast<keel::StreamingThread*>(thread))
  {
    if (auto streamer = streamingThread->streamer)
    {
      dx::trace("%s: streams initialized", name.c_str());

      depthFrame = new gaia::utils::DepthF32Frame();
      grayscaleFrame = new gaia::utils::Grayscale8Frame();

      grayscaleFrame->depth_projection_is_available = false;
      depthFrame->invalids[0] = streamer->getDepthF32Frame()->invalids[0];
      depthFrame->invalids[1] = streamer->getDepthF32Frame()->invalids[1];

      // allocate needed frame resources
      depthFrame->copySettingsFrom(streamer->getDepthF32FrameRef());
      grayscaleFrame->copySettingsFrom(streamer->getGrayscale8FrameRef());

      processor = new gaia::VisualOdometry(streamer);
      processor->onRawStreamsServiceInitialized(streamer);
      streamer->registerNotify(this);

      onStreamsResumed(thread);
    }
  }
}

void keel::VisualOdometryThread::onStreamsReleased(keel::SmartScanThread *thread)
{
  if (auto streamingThread = dynamic_cast<keel::StreamingThread*>(thread))
  {
    dx::trace("%s: streams released", name.c_str());
    if (auto streamer = streamingThread->streamer)
    {
      processor->onRawStreamsServiceReleased(streamer);
      streamer->unregisterNotify(this);
    }
  }
}

void keel::VisualOdometryThread::onStreamsResumed(keel::SmartScanThread *thread)
{
  //mainLoopTask.run([this]() { mainLoopRoutine(); });
  SmartScanThread::resume();
}

void keel::VisualOdometryThread::onStreamsSuspended(keel::SmartScanThread *thread)
{
  dx::trace("%s: suspending...", name.c_str());
  dx::trace("%s: - cancelling ", name.c_str());
  mainLoopTask.cancel();
  mainLoopTask.wait();
  dx::trace("%s: - suspending deps", name.c_str());
  SmartScanThread::suspend();
  dx::trace("%s: suspended", name.c_str());
}

void keel::VisualOdometryThread::onFrameAcquired(
  gaia::IRawStreams const *rawStreams
  )
{
  //if (rawStreams != 0 && framesProcessed.wait(0) == 0)
  //{
  //  framesProcessed.reset();

  dx::trace("%s: acquired", name.c_str());
  depthFrame->copyDataFrom(rawStreams->getDepthF32FrameRef());
  grayscaleFrame->copyDataFrom(rawStreams->getGrayscale8FrameRef());
  processor->onFrameAcquired(*grayscaleFrame, *depthFrame);

  //  framesAcquired.set();
  //}
}

void keel::VisualOdometryThread::mainLoopRoutine()
{
  dx::trace("%s: main loop", name.c_str());

  // start receiving new frames
  framesProcessed.set();

  dx::sim::BasicTimer timer;
  timer.reset();

  while (
    !concurrency::is_current_task_group_canceling()
    || !concurrency::is_task_cancellation_requested()
    )
  {
    if (!framesAcquired.wait(1000))
    {
      framesAcquired.reset();

      // process frames
      // send camera pose

      auto &depthRef = *depthFrame;
      auto &grayscaleRef = *grayscaleFrame;
      //processor->onFrameAcquired(grayscaleRef, depthRef);

      // simulate processing at 15 fps
      Sleep(1000 / 15);

      dx::trace("%s: processed", name.c_str());
      framesProcessed.set();

      timer.update();
      dx::trace("%s: %.1f (%d)", name.c_str(), timer.elapsed, timer.framesPerSecond);
    }
  }

  dx::trace("%s: exiting main loop", name.c_str());
}
