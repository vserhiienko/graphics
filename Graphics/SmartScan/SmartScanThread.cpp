#include "pch.h"
#include "SmartScanThread.h"

unsigned keel::SmartScanThread::s_appThreadCount = 0;
keel::SmartScanThread::SmartScanThread(SmartScanApp *host, std::string const &threadName)
  : app(host)
  , name(threadName)
{
  id = s_appThreadCount++;

  if (name.empty())
  {
    std::stringstream ss;
    ss << "keel/ss/t" << s_appThreadCount;
    name = ss.str();
  }

  dx::trace("keel:thread:new '%s'", name.c_str());
}

keel::SmartScanThread::SmartScanThread(SmartScanApp *host)
  : SmartScanThread(host, "")
{
}

keel::SmartScanThread::~SmartScanThread()
{
  s_appThreadCount--;

  dx::trace("keel:thread:kill '%s'", name.c_str());
}



