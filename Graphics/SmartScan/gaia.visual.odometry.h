#pragma once
#include "gaia.raw.streams.h"

namespace gaia
{
  class VisualOdometry
    : public gaia::IRawStreams::INotify
  {
  public:
    class FrameProcessedHook
    {
    public:
      typedef std::vector<FrameProcessedHook*> Vector;
    public:
      virtual void onFrameProcessed(
        fovis::MotionEstimateStatusCode,
        fovis::VisualOdometry &
        ) = 0;
    };

  public:
    typedef std::unique_ptr<fovis::Rectification> RectificationUniquePointer;
    typedef std::unique_ptr<fovis::VisualOdometry> VisualOdometryCoreUniquePointer;

    class DepthImage2
      : public fovis::DepthSource
    {
    public:
      typedef DepthImage2 *Pointer;
      typedef Eigen::Matrix<float, 3, Eigen::Dynamic> Rays;
      typedef std::unique_ptr<DepthImage2> UniquePointer;

    public: /// fovis::DepthSource

      virtual bool haveXyz(
        int u, int v
        );
      virtual void getXyz(
        fovis::OdometryFrame * frame
        );
      virtual void refineXyz(
        fovis::FeatureMatch * matches, int num_matches,
        fovis::OdometryFrame * frame
        );
      virtual float getBaseline(
        ) const;

    public:

      DepthImage2(VisualOdometry *);
      ~DepthImage2();

      void OnFrameAcquired(IRawStreams *streams);
      void OnFrameAcquired(utils::DepthF32Frame &frame);

    protected:

      int rgbToDepthIndex(float u, float v) const;
      bool getXyzInterp(fovis::KeypointData* kpdata);

      VisualOdometry *m_core;
      utils::DepthF32Frame *m_depth32_frame_ref;

      int _rgb_width;
      int _rgb_height;
      int _depth_width;
      int _depth_height;

      Rays* _rays;
      float _fx_inv;
      float _fy_inv;
      float _x_scale;
      float _y_scale;
      float _neg_cx_div_fx;
      float _neg_cy_div_fy;

    public:

    };

  public:
    VisualOdometry(IRawStreams*);
    ~VisualOdometry();

    void setFrameProcessedHook(FrameProcessedHook*);
    void OnRawStreamsServiceInitialized(IRawStreams*);
    void OnRawStreamsServiceReleased(IRawStreams*);
    virtual void OnFrameAcquired(IRawStreams*);
    virtual void OnFrameAcquired(
      gaia::utils::Grayscale8Frame &,
      gaia::utils::DepthF32Frame &
      );

  protected:


    FrameProcessedHook *frameProcessedHook;

    bool m_is_instance_valid;

    IRawStreams *m_streams;
    RectificationUniquePointer m_rect;
    fovis::VisualOdometryOptions m_vis_odom_opts;
    fovis::CameraIntrinsicsParameters m_color_camera_params;
    fovis::CameraIntrinsicsParameters m_depth_camera_params;

    DepthImage2::UniquePointer m_depth_image;
    //DepthImage::UniquePointer m_depth_image;
    VisualOdometryCoreUniquePointer m_vis_odom_core;

  private:
    friend VisualOdometry;

  private:

  };
}