#pragma once

#include <CiriUI.h>
#include <CiriWindow.h>

#include <d2d1_2.h>
#include <d2d1_2helper.h>
#include <wrl/client.h>

namespace ciri
{
	using Microsoft::WRL::ComPtr;

	class UIWrap
	{
	public:

		HANDLE ciriUIHandle;
		HINSTANCE ciriUILibraryHandle;
		ciri_ui_handleMessageFuncPtr ciriUIOnMessageFunc;
		ciri_ui_initializeFuncPtr ciriUIOnInitializeFunc;
		ciri_ui_getBrowserWindowHandleFuncPtr ciriUIOnGetBrowserWindowHandleFunc;
		ciri_ui_onFrameMoveFuncPtr ciriUIOnUpdateFunc;
		ciri_ui_releaseFuncPtr ciriUIOnReleaseFunc;

		HWND offscreenWindowHandle;
		HWND browserWindowHandle;

		bool resolveExternalDependencies();

	public:
		void initialize(
			_In_ HWND windowHandle,
			_In_ HINSTANCE instanceHandle,
			_In_ char const *startupUrl
			);
		void resolveBrowserWindowHandle();
		void onMessage(
			_In_ Window::EventArgs const &args
			);
		void onFrameMove(
			_In_ Window::EventArgs const &args
			);
		void onPaint(
			_In_ Window::EventArgs const &args
			);
		void release();

	};
}
