#include "CiriUIWrap.h"
#include <DxUtils.h>
#include <wincodec.h>
#include <wincodecsdk.h>
using namespace ciri;

void UIWrap::resolveBrowserWindowHandle()
{
	browserWindowHandle = ciriUIOnGetBrowserWindowHandleFunc(ciriUIHandle);
}

void UIWrap::initialize(
	_In_ HWND windowHandle,
	_In_ HINSTANCE instanceHandle,
	_In_ char const *startupUrl
	)
{
	ciriUIHandle = ciriUIOnInitializeFunc(windowHandle, instanceHandle, startupUrl);
}

void UIWrap::onMessage(
	_In_ Window::EventArgs const &args
	)
{
	ciriUIOnMessageFunc(ciriUIHandle, (void*) &args);
}

void UIWrap::onFrameMove(
	_In_ Window::EventArgs const &args
	)
{
	ciriUIOnUpdateFunc(ciriUIHandle, (void*) &args);
}

void UIWrap::onPaint(
	_In_ Window::EventArgs const &args
	)
{

}

void UIWrap::release()
{
	ciriUIOnReleaseFunc(ciriUIHandle);
}

bool UIWrap::resolveExternalDependencies()
{
	ciriUILibraryHandle = LoadLibraryA("CiriUIEngine.dll");
	if (!ciriUILibraryHandle) return false;

	ciriUIOnInitializeFunc = (ciri_ui_initializeFuncPtr) GetProcAddress(ciriUILibraryHandle, ciri_ui_initializeFuncName); if (!ciriUIOnInitializeFunc) return false;
	ciriUIOnGetBrowserWindowHandleFunc = (ciri_ui_getBrowserWindowHandleFuncPtr) GetProcAddress(ciriUILibraryHandle, ciri_ui_getBrowserWindowHandleFuncName); if (!ciriUIOnGetBrowserWindowHandleFunc) return false;
	ciriUIOnMessageFunc = (ciri_ui_handleMessageFuncPtr) GetProcAddress(ciriUILibraryHandle, ciri_ui_handleMessageFuncName); if (!ciriUIOnMessageFunc) return false;
	ciriUIOnUpdateFunc = (ciri_ui_onFrameMoveFuncPtr) GetProcAddress(ciriUILibraryHandle, ciri_ui_onFrameMoveFuncName); if (!ciriUIOnUpdateFunc) return false;
	ciriUIOnReleaseFunc = (ciri_ui_releaseFuncPtr) GetProcAddress(ciriUILibraryHandle, ciri_ui_releaseFuncName); if (!ciriUIOnReleaseFunc) return false;

	return true;
}