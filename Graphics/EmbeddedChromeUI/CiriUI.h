#pragma once

#include <Windows.h>
#include <safeint.h>

#define _EmbeddedChromeUI_Version 0x00010001
#if _EmbeddedChromeUI_API_Export
#define _EmbeddedChromeUI_API __declspec(dllexport)
#else
#define _EmbeddedChromeUI_API __declspec(dllimport)
#endif


#define _EmbeddedChromeUI_StringifyFunction(exp) static char const * exp##FuncName = "" #exp "";
_EmbeddedChromeUI_StringifyFunction(ciri_ui_getVersion);
_EmbeddedChromeUI_StringifyFunction(ciri_ui_initialize);
_EmbeddedChromeUI_StringifyFunction(ciri_ui_handleMessage);
_EmbeddedChromeUI_StringifyFunction(ciri_ui_onFrameMove);
_EmbeddedChromeUI_StringifyFunction(ciri_ui_release);

extern "C"
{
    _EmbeddedChromeUI_API uint32_t ciri_ui_getVersion(
        );

    _EmbeddedChromeUI_API HANDLE ciri_ui_initialize(
        _In_ HWND windowHandle,
        _In_ HINSTANCE instanceHandle,
        _In_ char const *startupUrl
        );
    _EmbeddedChromeUI_API bool ciri_ui_handleMessage(
        _Inout_ HANDLE uiHandle,
        _Inout_ HANDLE message
        );
    _EmbeddedChromeUI_API bool ciri_ui_onFrameMove(
        _Inout_ HANDLE uiHandle,
        _Inout_ HANDLE message
        );
    _EmbeddedChromeUI_API void ciri_ui_release(
        _Inout_ HANDLE ui
        );

    typedef uint32_t(*ciri_ui_getVersionFuncPtr)(
        );
    typedef HANDLE(*ciri_ui_initializeFuncPtr)(
        _In_ HWND windowHandle,
        _In_ HINSTANCE instanceHandle,
        _In_ char const *startupUrl
        );
    typedef bool(*ciri_ui_handleMessageFuncPtr)(
        _Inout_ HANDLE uiHandle,
        _Inout_ HANDLE message
        );
    typedef bool(*ciri_ui_onFrameMoveFuncPtr)(
        _Inout_ HANDLE uiHandle,
        _Inout_ HANDLE message
        );
    typedef void(*ciri_ui_releaseFuncPtr)(
        _Inout_ HANDLE ui
        );

}



