#include <DxUtils.h>

#include "SethUIWindowPosition.h"
using namespace seth;
using namespace easing;


UIWindowPosition::UIWindowPosition()
    : mainWindow(nullptr)
    , uiWindow(nullptr)
{
    fadeSecs = 0.3f;
    frameSecs = 1.0f / 24.0f;
    elapsedSecs = 0.0f;
    currentOffset = 0.0f;
    stage = EasingStage::kResumed;
}

void UIWindowPosition::initialize()
{
    if (mainWindow && uiWindow)
    {
        GetWindowRect(mainWindow, &mainRect);
        GetWindowRect(uiWindow, &uiRect);

        top = uiRect.top;
        width = uiRect.right - uiRect.left;
        height = uiRect.bottom - uiRect.top;

        initialOffset = uiRect.left;
        initialDistance = width;

        easingIn.reset();
        easingOut.reset();

        easingIn.params.total = fadeSecs;
        easingIn.params.offset = initialOffset;
        easingIn.params.distance = initialDistance;

        easingOut.params.total = fadeSecs;
        easingOut.params.offset = initialOffset + initialDistance;
        easingOut.params.distance = -initialDistance;

        elapsedSecs = 0.0f;
        currentOffset = initialOffset;
    }
}

void UIWindowPosition::showUI()
{
    switch (stage)
    {
    /*case EasingStage::kSuspending:
        easingIn.params.offset = currentOffset;
        easingIn.params.distance = currentOffset - initialDistance + initialOffset;
        stage = EasingStage::kResuming;
        break;*/
    case EasingStage::kSuspended:
        easingOut.params.offset = initialOffset + initialDistance;
        easingOut.params.distance = -initialDistance;
        easingOut.reset();
        elapsedSecs = 0.0f;
        stage = EasingStage::kResuming;
        break;
    }

}

void UIWindowPosition::hideUI()
{
    switch (stage)
    {
    /*case EasingStage::kResuming:
        easingIn.params.offset = currentOffset;
        easingIn.params.distance = initialDistance - currentOffset + initialOffset;
        stage = EasingStage::kSuspending;
        break;*/
    case EasingStage::kResumed:
        easingIn.params.offset = initialOffset;
        easingIn.params.distance = initialDistance;
        easingIn.reset();
        elapsedSecs = 0.0f;
        stage = EasingStage::kSuspending;
        break;
    }

}

void UIWindowPosition::onFrameMove(float elapsedS)
{
    switch (stage)
    {
    case EasingStage::kResuming:
        if (!easingOut.saturated)
        {
            elapsedSecs += elapsedS;

            easingOut += elapsedS;
            currentOffset = easingOut.onFrame();

            if (easingOut.saturated)
            {
                stage = EasingStage::kResumed;
                shown((Ptr)this);
                easingOut.reset();

                elapsedSecs = 0.0f;
                SetWindowPos(
                    uiWindow, nullptr,
                    (long)currentOffset - 10, top - 10, 0, 0,
                    SWP_NOSIZE | SWP_DRAWFRAME
                    );
            }
            else if (elapsedSecs >= frameSecs)
            {
                elapsedSecs = 0.0f;
                SetWindowPos(
                    uiWindow, nullptr,
                    (long)currentOffset - 10, top - 10, 0, 0,
                    SWP_NOSIZE | SWP_DRAWFRAME
                    );
            }
        }
        break;
    case EasingStage::kSuspending:
        if (!easingIn.saturated)
        {
            elapsedSecs += elapsedS;

            easingIn += elapsedS;
            currentOffset = easingIn.onFrame();

            if (easingIn.saturated)
            {
                stage = EasingStage::kSuspended;
                hidden((Ptr)this);
                easingIn.reset();

                elapsedSecs = 0.0f;
                SetWindowPos(
                    uiWindow, nullptr,
                    (long)currentOffset - 10, top - 10, 0, 0,
                    SWP_NOSIZE | SWP_DRAWFRAME
                    );
            }
            else if (elapsedSecs >= frameSecs)
            {
                elapsedSecs = 0.0f;
                SetWindowPos(
                    uiWindow, nullptr,
                    (long)currentOffset - 10, top - 10, 0, 0,
                    SWP_NOSIZE | SWP_DRAWFRAME
                    );
            }
        }
        break;
    }

}
