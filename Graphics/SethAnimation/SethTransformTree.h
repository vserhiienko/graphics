#pragma once

#include <vector>
#include <Eigen\Eigen>
#include <AegaScene.h>

namespace seth
{
	struct TransformTreeNode
	{
		typedef TransformTreeNode *Ptr;
		typedef std::vector<Ptr> PtrVector;

		Eigen::Vector3f translationOSV;
		Eigen::Quaternionf orientationQ;
		Eigen::Quaternionf rotationOSQ;
		Eigen::Vector3f scalingV;

		bool updatePoseOS;
		bool updatePoseWS;
		Eigen::Matrix4f poseOSM;
		Eigen::Matrix4f poseWSM;
		Eigen::Affine3f poseOST;
		Eigen::Affine3f poseWST;

        void *user;
        aega::TransformNode *source;

		Ptr parent;
		PtrVector children;

		TransformTreeNode();

		void makeIdentity();
        void notifyPoseOS();
        void notifyPoseWS();
        void notifyChildPosesWS();
		void evaluatePoseOS();
		void evaluatePoseWS();
		void evaluateChildPosesWS();
	};

	struct TransformTree
	{
        typedef TransformTree *Ptr;

        TransformTreeNode::Ptr rootNode;
        TransformTreeNode::PtrVector nodes;

		TransformTree();

		void newRoot();
	};

}

