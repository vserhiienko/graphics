
#include <ppl.h>
#include <ppltasks.h>

#include "SethTransformTree.h"

using namespace Eigen;
using namespace seth;

TransformTreeNode::TransformTreeNode()
    : updatePoseOS(false)
    , updatePoseWS(false)
    , parent(nullptr)
    , user(nullptr)
    , source(nullptr)
{
}

void TransformTreeNode::makeIdentity()
{
    translationOSV = Vector3f::Zero();
    orientationQ = Quaternionf::Identity();
    rotationOSQ = Quaternionf::Identity();
    scalingV = Vector3f::Zero();
    poseOSM = Matrix4f::Identity();
    poseWSM = Matrix4f::Identity();
}

void TransformTreeNode::notifyPoseOS()
{
    updatePoseOS = true;
}

void TransformTreeNode::notifyPoseWS()
{
    updatePoseWS = true;
}

void TransformTreeNode::notifyChildPosesWS()
{
    if (const size_t child_count = children.size())
    {
        auto child_at = children.data();
        for (size_t child_ind = 0; child_ind < child_count; ++child_ind)
        {
            child_at[child_ind]->updatePoseWS = true;
            child_at[child_ind]->notifyPoseWS();
        }
    }
}


void TransformTreeNode::evaluatePoseOS()
{
    if (updatePoseOS)
    {
        poseOST
            .setIdentity();
        poseOST
            .scale(scalingV)
            .rotate(orientationQ)
            .translate(translationOSV)
            .rotate(rotationOSQ);

        poseOSM = poseOST.matrix();
        updatePoseOS = false;
    }

}

void TransformTreeNode::evaluateChildPosesWS()
{
    if (const size_t child_count = children.size())
    {
        auto child_at = children.data();
        for (size_t child_ind = 0; child_ind < child_count; ++child_ind)
        {
            child_at[child_ind]->evaluatePoseWS();
            child_at[child_ind]->evaluateChildPosesWS();
        }
    }

}

void TransformTreeNode::evaluatePoseWS()
{
    evaluatePoseOS();
    if (updatePoseWS)
    {
        if (parent)
        {
            parent->evaluatePoseWS();
            poseWST = poseOST * (parent->parent ? parent->poseWST : parent->poseOST);
            poseWSM = poseWST.matrix();
        }

        updatePoseWS = false;
    }
}

TransformTree::TransformTree()
    : rootNode(nullptr)
{

}

void TransformTree::newRoot()
{
    rootNode = new TransformTreeNode();
    rootNode->makeIdentity();
}


