#pragma once

#include <Windows.h>
#include <SethEasing.h>
#include <DxDelegates.h>

namespace seth
{
    struct UIWindowPosition
    {
        typedef UIWindowPosition *Ptr;
        typedef dx::Event<void, UIWindowPosition*> Event;

        RECT mainRect, uiRect; // window rectagles
        HWND mainWindow, uiWindow; // handles to windows

        float fadeSecs; // (set) time, while the window is hiding/appearing
        float frameSecs; // (set) time, that should elapse before calling 'SetWindowPos'
        float elapsedSecs; // time, that has elapsed since the previous frame
        float currentOffset; // current window horz position
        float initialOffset, initialDistance; // initial window x position and width
        long width, height, top; // window width, height and y position

        Event hidden, shown; // (set) window was finally shown/hidden

        easing::EasingStage stage; // current window easing stage
        easing::CubicInEasingFunction easingIn; // hidding controller
        easing::CubicOutEasingFunction easingOut; // appearing controller

        UIWindowPosition();
        void initialize();
        void showUI();
        void hideUI();
        void onFrameMove(float elapsedS);

    };
}
