#pragma once

#include <Windows.h>
#include <Windowsx.h>
#include <DxNeon.h>
#include <DxUtils.h>

namespace ciri
{
	class Window
	{
	public:
		class EventArgs
		{
		public:
			dx::neon::n256 data;

		public:
			enum State
			{
				kState_MaxHide = SIZE_MAXHIDE,
				kState_MaxShow = SIZE_MAXSHOW,
				kState_Minimized = SIZE_MINIMIZED,
				kState_Maximized = SIZE_MAXIMIZED,
				kState_Restored = SIZE_RESTORED,
			};

			enum EventCallbackResult
			{
				kEventCallbackResult_Quit,
				kEventCallbackResult_Ignored,
				kEventCallbackResult_Processed,
			};

			EventArgs(void);
			EventArgs(EventArgs const &);

		public:
            inline HWND &windowHandle(void) { return (HWND &)data.u64[0]; }
			inline uint32_t &windowMessage(void) { return (uint32_t&) data.u64[1]; }
			inline WPARAM &wparam(void) { return (WPARAM&) data.u64[2]; }
			inline LPARAM &lparam(void) { return (LPARAM&) data.u64[3]; }

			inline HWND const &windowHandle(void) const { return (HWND const &) data.u64[0]; }
			inline uint32_t windowMessage(void) const { return (uint32_t const&) data.u64[1]; }
			inline WPARAM const &wparam(void) const { return (WPARAM const&) data.u64[2]; }
			inline LPARAM const &lparam(void) const { return (LPARAM const&) data.u64[3]; }

		public:
			inline float &mouseX(void) { return data.f32[0]; }
			inline float mouseX(void) const { return data.f32[0]; }
			inline float &mouseY(void) { return data.f32[1]; }
			inline float mouseY(void) const { return data.f32[1]; }
			inline bool mouseShift(void) const { return data.u8[8] != 0ui8; }
			inline bool mouseControl(void) const { return data.u8[9] != 0ui8; }
			inline int32_t &hwheel(void) { return data.i32[2]; }
			inline int32_t hwheel(void) const { return data.i32[2]; }
			inline int32_t &wheelDelta(void) { return data.i32[1]; }
			inline int32_t wheelDelta(void) const { return data.i32[1]; }
			inline uint32_t &wheelKeystate(void) { return data.u32[0]; }
			inline uint32_t wheelKeystate(void) const { return data.u32[0]; }
			inline uint32_t &keyCode(void) { return data.u32[0]; }
			inline uint32_t keyCode(void) const { return data.u32[0]; }
			inline uint32_t &keyPrevState(void) { return data.u32[1]; }
			inline uint32_t keyPrevState(void) const { return data.u32[1]; }
			inline uint32_t &keyRepeatCount(void) { return data.u32[2]; }
			inline uint32_t keyRepeatCount(void) const{ return data.u32[2]; }
			inline uint32_t &width(void) { return data.u32[0]; }
			inline uint32_t width(void) const { return data.u32[0]; }
			inline uint32_t &height(void) { return data.u32[1]; }
			inline uint32_t height(void) const { return data.u32[1]; }
			inline State &state(void) { return (State&)data.u32[2]; }
			inline State state(void) const { return (State)data.u32[2]; }

		public:
			void clear(void);
			void store(MSG const &);
			void translate(void);
            void translateTo(EventArgs&) const;
            void copyTo(EventArgs&) const;
            void copyFrom(EventArgs const &);
            template <uint32_t _Message> void translateAs();

		public:
			static bool canTranslate(uint32_t message);
		};

	public:
		void *userHandle;
		HWND windowHandle;

    public:
        static LRESULT CALLBACK internalWindowProc(
            HWND hWnd,
            UINT message,
            WPARAM wParam,
            LPARAM lParam
			);

	public:
		Window();

	public:
		void initialize(
			int windowOffsetX,
			int windowOffsetY,
			int windowWidth,
			int windowHeight,
			TCHAR const *sampleAppName,
			WNDPROC windowProc = internalWindowProc
			);

		void initializeAsUI(
			Window &mainWindow,
			WNDPROC windowProc = internalWindowProc
			);

		void sendCloseMessage();

	public:
		template <typename _EventCallback, typename _FrameMoveCallback, typename _CleanupCallback>
		void mainLoop(
			_EventCallback eventCallback,
			_FrameMoveCallback frameMoveCallback,
			_CleanupCallback cleanupCallback
			);

	public:
		template <typename _SceneEventCallback, typename _UIEventCallback, typename _FrameMoveCallback, typename _CleanupCallback>
		void mainLoopWithUI(
			Window &uiWindow,
			_SceneEventCallback sceneEventCallback,
			_UIEventCallback uiEventCallback,
			_FrameMoveCallback frameMoveCallback,
			_CleanupCallback cleanupCallback
			);
	};

#include <CiriWindow.inl>

}

