#include <comdef.h>
#include <DxUtils.h>
#include <CiriMisc.h>

using namespace ciri;

void ciri::reportComError(char const *header, HRESULT result_handle)
{
	if (FAILED(result_handle))
	{
		_com_error err(result_handle);
		dx::trace("%s0x%08x: %s"
			, header , result_handle
			, err.ErrorMessage()
			);
	}
}



HRESULT ciri::reportComErrorAndGet(char const *header, HRESULT result_handle)
{
    reportComError(header, result_handle);
    return result_handle;
}


