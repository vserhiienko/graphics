
#include "CiriMisc.h"
#include <CiriWindow.h>
#include <DxVirtualKeys.h>
using namespace ciri;

Window::EventArgs::EventArgs(void) { dx::zeroMemory(data); }
Window::EventArgs::EventArgs(EventArgs const &other) { memcpy(&data, &other.data, sizeof(data)); }

void Window::EventArgs::translateTo(EventArgs &args) const
{
	switch (windowMessage())
	{
	case dx::Message::KeyPressed:
	case dx::Message::KeyReleased:
		args.keyCode() = (uint32_t)wparam();
		dx::CorePhysicalKeyStatus::ExtractSlim(lparam(), args.keyPrevState(), args.keyRepeatCount());
		break;

	case dx::Message::LeftButtonPressed:
	case dx::Message::LeftButtonReleased:
	case dx::Message::RightButtonPressed:
	case dx::Message::RightButtonReleased:
	case dx::Message::MiddleButtonPressed:
	case dx::Message::MiddleButtonReleased:
		args.mouseX() = (float)GET_X_LPARAM(lparam());
		args.mouseY() = (float)GET_Y_LPARAM(lparam());
		args.data.u8[8] = (wparam() & 0x0008) != 0 ? 1ui8 : 0ui8;
		args.data.u8[9] = (wparam() & 0x0004) != 0 ? 1ui8 : 0ui8;
		break;

	case dx::Message::MouseMove:
		args.mouseX() = (float)GET_X_LPARAM(lparam());
		args.mouseY() = (float)GET_Y_LPARAM(lparam());
		break;

	case dx::Message::MouseWheel:
	case dx::Message::MouseHWheel:
		args.wheelDelta() = GET_WHEEL_DELTA_WPARAM(wparam());
		args.wheelKeystate() = GET_KEYSTATE_WPARAM(wparam());
		break;

	case dx::Message::Size:
		args.state() = (State)wparam();
		args.width() = LOWORD(lparam());
		args.height() = HIWORD(lparam());
		break;
	}
}

void Window::EventArgs::clear(void)
{
	dx::zeroMemory(data);
}

void Window::EventArgs::translate()
{
	switch (windowMessage())
	{
	case dx::Message::KeyPressed:
	case dx::Message::KeyReleased:
		keyCode() = (uint32_t)wparam();
		dx::CorePhysicalKeyStatus::ExtractSlim(lparam(), keyPrevState(), keyRepeatCount());
		break;

	case dx::Message::LeftButtonPressed:
	case dx::Message::LeftButtonReleased:
	case dx::Message::RightButtonPressed:
	case dx::Message::RightButtonReleased:
	case dx::Message::MiddleButtonPressed:
	case dx::Message::MiddleButtonReleased:
		mouseX() = (float)GET_X_LPARAM(lparam());
		mouseY() = (float)GET_Y_LPARAM(lparam());
		data.u8[8] = (wparam() & 0x0008) != 0 ? 1ui8 : 0ui8;
		data.u8[9] = (wparam() & 0x0004) != 0 ? 1ui8 : 0ui8;
		break;

	case dx::Message::MouseMove:
		mouseX() = (float)GET_X_LPARAM(lparam());
		mouseY() = (float)GET_Y_LPARAM(lparam());
		break;

	case dx::Message::MouseWheel:
	case dx::Message::MouseHWheel:
		wheelDelta() = GET_WHEEL_DELTA_WPARAM(wparam());
		wheelKeystate() = GET_KEYSTATE_WPARAM(wparam());
		break;

	case dx::Message::Size:
		state() = (State)wparam();
		width() = LOWORD(lparam());
		height() = HIWORD(lparam());
		break;
	}
}

void Window::EventArgs::store(MSG const &msg)
{
    windowHandle() = msg.hwnd;
    windowMessage() = msg.message;
	wparam() = msg.wParam;
	lparam() = msg.lParam;
}

void Window::EventArgs::copyTo(EventArgs &args) const
{
    memcpy(&args, this, sizeof(EventArgs));
}

void Window::EventArgs::copyFrom(EventArgs const &args)
{
    memcpy(this, &args, sizeof(EventArgs));
}

bool Window::EventArgs::canTranslate(uint32_t message)
{
	switch (message)
	{
    case dx::Message::Paint:
    case dx::Message::MouseMove:
	case dx::Message::KeyPressed:
	case dx::Message::LeftButtonPressed:
	case dx::Message::RightButtonPressed:
	case dx::Message::MiddleButtonPressed:
	case dx::Message::LeftButtonReleased:
	case dx::Message::RightButtonReleased:
    case dx::Message::MiddleButtonReleased:
    case dx::Message::MouseWheel:
    case dx::Message::MouseHWheel:
	case dx::Message::KeyReleased:
    case dx::Message::Size:
    case dx::Message::Create:
    case dx::Message::ExitSizeMove:
		return true;
	}

	return false;
}

Window::Window()
	: userHandle(0)
{
}

LRESULT CALLBACK Window::internalWindowProc(
	HWND hWnd,
	UINT message,
	WPARAM wParam,
	LPARAM lParam
	)
{
    if (message == WM_CLOSE)
    {
        DestroyWindow(hWnd);
        return 0;
    }
	else if (message == WM_DESTROY) 
    { 
        PostQuitMessage(0);
        return 0;
    }
	else 
        return DefWindowProc(hWnd, message, wParam, lParam);
}


void Window::initializeAsUI(Window &mainWindow, WNDPROC proc)
{
	static const TCHAR *windowClassName
		= TEXT("CIRI_UI_WINDOW_CLASS");
	static const unsigned long style
		= WS_CLIPSIBLINGS
		| WS_CLIPCHILDREN
		| WS_VISIBLE
		| WS_CHILD;

	if (!proc)
		proc = internalWindowProc;

	WNDCLASSEX windowClass;
	windowClass.cbSize = (uint32_t) dx::zeroMemorySz(windowClass);
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	windowClass.lpfnWndProc = proc;
	windowClass.hInstance = GetModuleHandle(0);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.lpszClassName = windowClassName;
	windowClass.hbrBackground = (HBRUSH) GetStockObject(GRAY_BRUSH);
	RegisterClassEx(&windowClass);

	RECT sceneRect;
	GetClientRect(mainWindow.windowHandle, &sceneRect);
	sceneRect.left += (sceneRect.right - sceneRect.left) * 2 / 3;

	POINT sceneTopLeft;
	sceneTopLeft.x = 0;
	sceneTopLeft.y = 0;

	ClientToScreen(mainWindow.windowHandle, &sceneTopLeft);
	sceneRect.left += sceneTopLeft.x;
	sceneRect.right += sceneTopLeft.x;
	sceneRect.top += sceneTopLeft.y;
	sceneRect.bottom += sceneTopLeft.y;

	long width = sceneRect.right - sceneRect.left;
	long height = sceneRect.bottom - sceneRect.top;

	//sceneRect.left += width / 3;
	sceneRect.left -= 10;
	sceneRect.top -= 10;

	// create the window and store a handle to it
	windowHandle = CreateWindowEx(
		0,
		windowClassName, // name of the window class
		0, // name of the window
		style, 
		sceneRect.left, // Horizontal position
		sceneRect.top, // Vertical position
		width, // Width
		height, // Height
		mainWindow.windowHandle, // we have no parent window, NULL
		NULL, // we aren't using menus, NULL
		windowClass.hInstance, // application handle
		NULL // used with multiple windows, NULL
		);
	if (!windowHandle)
	{
		reportComError("CreateWindowEx: ", HRESULT_FROM_WIN32(GetLastError()));
	}

	ShowWindow(windowHandle, SW_SHOWDEFAULT);
}

void Window::initialize(int windowOffsetX, int windowOffsetY, int windowWidth, int windowHeight, TCHAR const *sampleAppName, WNDPROC proc)
{
    static const TCHAR *windowClassName 
        = TEXT("CIRI_WINDOW_CLASS");
    static const unsigned long style
        = WS_CLIPSIBLINGS
        | WS_CLIPCHILDREN
        | WS_VISIBLE
        | WS_POPUP;

	if (!proc)
		proc = internalWindowProc;

	WNDCLASSEX windowClass;
	windowClass.cbSize = (uint32_t)dx::zeroMemorySz(windowClass);
    windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	windowClass.lpfnWndProc = proc;
	windowClass.hInstance = GetModuleHandle(0);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    windowClass.lpszClassName = windowClassName;
    windowClass.hbrBackground = (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	RegisterClassEx(&windowClass);

	RECT windowRect = { 0, 0, windowWidth, windowHeight };
	//AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE); // adjust the size

	// create the window and store a handle to it
    windowHandle = CreateWindowEx(
        0,
        windowClassName, // name of the window class
        sampleAppName, // name of the window
		style,
		windowOffsetX,
		windowOffsetY,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL, // we have no parent window, NULL
		NULL, // we aren't using menus, NULL
		windowClass.hInstance, // application handle
		NULL // used with multiple windows, NULL
		);
    if (!windowHandle)
    {
        reportComError("CreateWindowEx: ", HRESULT_FROM_WIN32(GetLastError()));
    }

	ShowWindow(windowHandle, SW_SHOWDEFAULT);
}

void ciri::Window::sendCloseMessage()
{
    SendMessage(windowHandle, WM_CLOSE, 0, 0);
}

template <> void ciri::Window::EventArgs::translateAs<dx::Message::KeyPressed>()
{
    keyCode() = (uint32_t)wparam();
    dx::CorePhysicalKeyStatus::ExtractSlim(lparam(), keyPrevState(), keyRepeatCount());
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::KeyReleased>()
{
    keyCode() = (uint32_t)wparam();
    dx::CorePhysicalKeyStatus::ExtractSlim(lparam(), keyPrevState(), keyRepeatCount());
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::LeftButtonPressed>()
{
    mouseX() = (float)GET_X_LPARAM(lparam());
    mouseY() = (float)GET_Y_LPARAM(lparam());
    data.u8[8] = (wparam() & 0x0008) != 0 ? 1ui8 : 0ui8;
    data.u8[9] = (wparam() & 0x0004) != 0 ? 1ui8 : 0ui8;
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::LeftButtonReleased>()
{
    mouseX() = (float)GET_X_LPARAM(lparam());
    mouseY() = (float)GET_Y_LPARAM(lparam());
    data.u8[8] = (wparam() & 0x0008) != 0 ? 1ui8 : 0ui8;
    data.u8[9] = (wparam() & 0x0004) != 0 ? 1ui8 : 0ui8;
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::RightButtonPressed>()
{
    mouseX() = (float)GET_X_LPARAM(lparam());
    mouseY() = (float)GET_Y_LPARAM(lparam());
    data.u8[8] = (wparam() & 0x0008) != 0 ? 1ui8 : 0ui8;
    data.u8[9] = (wparam() & 0x0004) != 0 ? 1ui8 : 0ui8;
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::RightButtonReleased>()
{
    mouseX() = (float)GET_X_LPARAM(lparam());
    mouseY() = (float)GET_Y_LPARAM(lparam());
    data.u8[8] = (wparam() & 0x0008) != 0 ? 1ui8 : 0ui8;
    data.u8[9] = (wparam() & 0x0004) != 0 ? 1ui8 : 0ui8;
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::MiddleButtonPressed>()
{
    mouseX() = (float)GET_X_LPARAM(lparam());
    mouseY() = (float)GET_Y_LPARAM(lparam());
    data.u8[8] = (wparam() & 0x0008) != 0 ? 1ui8 : 0ui8;
    data.u8[9] = (wparam() & 0x0004) != 0 ? 1ui8 : 0ui8;
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::MiddleButtonReleased>()
{
    mouseX() = (float)GET_X_LPARAM(lparam());
    mouseY() = (float)GET_Y_LPARAM(lparam());
    data.u8[8] = (wparam() & 0x0008) != 0 ? 1ui8 : 0ui8;
    data.u8[9] = (wparam() & 0x0004) != 0 ? 1ui8 : 0ui8;
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::MouseMove>()
{
    mouseX() = (float)GET_X_LPARAM(lparam());
    mouseY() = (float)GET_Y_LPARAM(lparam());
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::MouseWheel>()
{
    wheelDelta() = GET_WHEEL_DELTA_WPARAM(wparam());
    wheelKeystate() = GET_KEYSTATE_WPARAM(wparam());
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::MouseHWheel>()
{
    wheelDelta() = GET_WHEEL_DELTA_WPARAM(wparam());
    wheelKeystate() = GET_KEYSTATE_WPARAM(wparam());
}
template <> void ciri::Window::EventArgs::translateAs<dx::Message::Size>()
{
    state() = (State)wparam();
    width() = LOWORD(lparam());
    height() = HIWORD(lparam());
}