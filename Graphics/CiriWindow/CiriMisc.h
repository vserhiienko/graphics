#pragma once
#include <Windows.h>

namespace ciri
{
    void reportComError(char const *header, HRESULT result_handle);
    HRESULT reportComErrorAndGet(char const *header, HRESULT result_handle);
}

