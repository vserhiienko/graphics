
template <typename _EventCallback, typename _FrameMoveCallback, typename _CleanupCallback>
void ciri::Window::mainLoop(
	_EventCallback eventCallback,
	_FrameMoveCallback frameMoveCallback,
	_CleanupCallback cleanupCallback
	)
{
	EventArgs args;
	MSG msg = { 0 };

	while (true)
	{
		// check to see if any messages are waiting in the queue
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			// check to see if it's time to quit
            if (msg.message == WM_QUIT) 
                break;
            else 
            {
                dx::trace("mainLoop: msg 0x%0x", msg.message);

                args.store(msg);
                if (eventCallback(args))
                    continue;

                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
		}
        else
        {
            frameMoveCallback();
        }
	}

	cleanupCallback();
}


template <typename _SceneEventCallback, typename _UIEventCallback, typename _FrameMoveCallback, typename _CleanupCallback>
void ciri::Window::mainLoopWithUI(
	Window &uiWindow,
	_SceneEventCallback sceneEventCallback,
	_UIEventCallback uiEventCallback,
	_FrameMoveCallback frameMoveCallback,
	_CleanupCallback cleanupCallback
	)
{
	EventArgs args;
	MSG msg = { 0 };

	bool keepPeekingMessages = true;
	while (keepPeekingMessages)
	{
		// check to see if any messages are waiting in the queue for this thread
		if (PeekMessage(&msg, windowHandle, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;
			else
			{
				//dx::trace("mainLoopWithUI: window: 0x%llx msg: 0x%x", msg.hwnd, msg.message);

				args.clear();
				args.store(msg);
				switch (sceneEventCallback(args))
				{
				case EventArgs::kEventCallbackResult_Quit:
				{
					dx::trace("mainLoopWithUI: quitting...");
					keepPeekingMessages = false;
					break;
				}
				case EventArgs::kEventCallbackResult_Processed:
					continue;
				case EventArgs::kEventCallbackResult_Ignored:
					TranslateMessage(&msg);
					DispatchMessage(&msg);
					break;
				}
			}
		}
		else
		{
			frameMoveCallback();
		}
	}

	cleanupCallback();
}