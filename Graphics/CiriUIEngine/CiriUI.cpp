
#include <DxUtils.h>
#include <DxNeon.h>
#include <DxVirtualKeys.h>
#include <CiriWindow.h>

#include "CiriUI.h"
#include "client_app.h"
#include "include/base/cef_bind.h"
#include "include/cef_app.h"
#include "include/cef_browser.h"
#include "include/cef_frame.h"
#include "include/cef_sandbox_win.h"
#include "include/wrapper/cef_closure_task.h"
#include "client_app.h"
#include "client_handler.h"
#include "client_switches.h"
#include "main_context_impl.h"
#include "main_message_loop_std.h"
using namespace ciri;
using namespace client;

uint32_t ciri_ui_getVersion()
{
    return 0x00010001;
}

class UIHandle
{
public:
    static UIHandle *staticHandle;
    static __forceinline UIHandle *cast(void *handle) { return static_cast<UIHandle*>(handle); }

public:
    int exitCode;

	HWND windowHandle;
	HWND browserWindowHandle;
    HINSTANCE instanceHandle;

    HDC deviceContext;
    PAINTSTRUCT paintStruct;

    std::string startupUrl;

    CefRefPtr<ClientHandler> clientHandler;
    scoped_ptr<MainContextImpl> context;
    scoped_ptr<MainMessageLoop> message_loop;

public:
    UIHandle()
        : exitCode(-1)
        , windowHandle(0)
        , instanceHandle(0)
    {
        dx::zeroMemory(paintStruct);
        dx::zeroMemory(deviceContext);
    }

public:
    int initializeUI()
    {
        dx::trace("ui.initialize");

        void* sandbox_info = NULL;
        CefMainArgs main_args(instanceHandle);
        CefRefPtr<ClientApp> app(new ClientApp);

        // Execute the secondary process, if any.
        exitCode = CefExecuteProcess(main_args, app.get(), sandbox_info);
        if (exitCode >= 0) return exitCode;

        // Create the main context object.
        context.reset(new MainContextImpl(0, NULL));

        CefSettings settings;
        settings.no_sandbox = true;
        settings.single_process = true;
        settings.windowless_rendering_enabled = false;
        settings.background_color = 0;
        settings.multi_threaded_message_loop = false;

        // Populate the settings based on command line arguments.
        context->PopulateSettings(&settings);

        // Create the main message loop object.
        message_loop.reset(new MainMessageLoopStd);

        // Initialize CEF.
        CefInitialize(main_args, settings, app.get(), sandbox_info);
    }

    void release()
    {
        dx::trace("ui.release");

        // Shut down CEF.
        CefShutdown();

        // Release objects in reverse order of creation.
        message_loop.reset();
        context.reset();
    }

    bool onFrameMove(dx::neon::n256 &args)
    {
        CefDoMessageLoopWork();
        return true;
    }

	bool resolveBrowserWindowHandle()
	{
		if (clientHandler.get())
		{
			CefRefPtr<CefBrowser> browser = clientHandler->GetBrowser();
			if (browser)
			{
				auto browserHost = browser->GetHost();
				if (browserHost)
				{
					browserWindowHandle = browserHost->GetWindowHandle();
					return true;
				}
			}
		}

		return false;
	}

    bool handleMessage(Window::EventArgs &args)
    {
        UINT const &message = args.windowMessage();
        WPARAM const &wparam = args.wparam();
        LPARAM const &lparam = args.lparam();

		switch (message)
        {
        case WM_CREATE:
        {
            dx::trace("ui.create");

            if (clientHandler.get()) 
                return true;

            RECT rect;
            GetClientRect(windowHandle, &rect);

            clientHandler = new ClientHandler();
            clientHandler->SetStartupURL(startupUrl);
            clientHandler->SetMainWindowHandle(windowHandle);

            CefWindowInfo info;
            info.SetAsChild(windowHandle, rect);

            CefBrowserSettings settings;
            settings.webgl = cef_state_t::STATE_ENABLED;
            settings.javascript = cef_state_t::STATE_ENABLED;
            settings.javascript_dom_paste = cef_state_t::STATE_ENABLED;
            settings.javascript_open_windows = cef_state_t::STATE_DISABLED;
            settings.javascript_close_windows = cef_state_t::STATE_DISABLED;
            settings.javascript_access_clipboard = cef_state_t::STATE_ENABLED;
            settings.universal_access_from_file_urls = cef_state_t::STATE_ENABLED;

            // Populate the browser settings based on command line arguments.
            MainContext::Get()->PopulateBrowserSettings(&settings);

            // Creat the new child browser window
            CefBrowserHost::CreateBrowser(
                info, 
                clientHandler.get(),
                clientHandler->GetStartupURL(), 
                settings, 
                NULL
				);
            return true;
        }

        case WM_PAINT:
            deviceContext = BeginPaint(windowHandle, &paintStruct);
            EndPaint(windowHandle, &paintStruct);
            return true;

        case WM_SETFOCUS:
            dx::trace("ui.focus");
            if (clientHandler.get())
            {
                CefRefPtr<CefBrowser> browser = clientHandler->GetBrowser();
                if (browser) browser->GetHost()->SetFocus(true);
            }
            return true;

        case WM_SIZE:
            dx::trace("ui.size");
            if (clientHandler.get() && clientHandler->GetBrowser())
                clientHandler->GetBrowser()->GetHost()->WasHidden(wparam == SIZE_MINIMIZED),
                clientHandler->GetBrowser()->GetHost()->WasResized();
            return true;

            // Notify the browser of move events so that popup windows are displayed
            // in the correct location and dismissed when the window moves.
        case WM_MOVING:
        case WM_MOVE:
            dx::trace("ui.move");
            if (clientHandler.get() && clientHandler->GetBrowser())
                clientHandler->GetBrowser()->GetHost()->NotifyMoveOrResizeStarted();
            return true;

            // Entering the menu loop for the application menu.
        case WM_ENTERMENULOOP:
            dx::trace("ui.modal.enter");
            if (!wparam) CefSetOSModalLoop(true);
            break;

            // Exiting the menu loop for the application menu.
        case WM_EXITMENULOOP:
            dx::trace("ui.modal.exit");
            if (!wparam) CefSetOSModalLoop(false);
            break;

        case WM_CLOSE:
            dx::trace("ui.close");
            if (clientHandler.get() && !clientHandler->IsClosing())
            {
                CefRefPtr<CefBrowser> browser = clientHandler->GetBrowser();
                if (browser.get()) 
                {
                    // Notify the browser window that we would like to close it. This
                    // will result in a call to ClientHandler::DoClose() if the
                    // JavaScript 'onbeforeunload' event handler allows it.
                    browser->GetHost()->CloseBrowser(false);

                    // Cancel the close.
                    return true;
                }
            }

            // Allow the close.
            break;

        case WM_DESTROY:
            // Quitting CEF is handled in ClientHandler::OnBeforeClose().
            return true;
        }

        return false;
    }
};

UIHandle *UIHandle::staticHandle = 0;

HANDLE ciri_ui_initialize(
    _In_ HWND windowHandle,
    _In_ HINSTANCE instanceHandle,
    _In_ char const *startupUrl
    )
{
    if (UIHandle::staticHandle)
        return (void*)UIHandle::staticHandle;
    else
    {
        auto ui = new UIHandle();
        ui->startupUrl = startupUrl;
        ui->windowHandle = windowHandle;
        ui->instanceHandle = instanceHandle;
        ui->initializeUI();

        UIHandle::staticHandle = ui;
        return (void*)ui;
    }
}


BOOL ciri_ui_onFrameMove(HANDLE uiHandle, HANDLE args)
{
    if (!uiHandle) return false;

    auto ui = UIHandle::cast(uiHandle);
    return ui->onFrameMove(*(dx::neon::n256*)args);
}


HWND ciri_ui_getBrowserWindowHandle(HANDLE uiHandle)
{
	if (!uiHandle) return false;

	auto ui = UIHandle::cast(uiHandle);
	ui->resolveBrowserWindowHandle();
	return ui->browserWindowHandle;
}

BOOL ciri_ui_handleMessage(HANDLE uiHandle, HANDLE args)
{
    if (!uiHandle) return false;

    auto ui = UIHandle::cast(uiHandle);
    return ui->handleMessage(*(Window::EventArgs*)args);
}

void ciri_ui_release(HANDLE uiHandle)
{
    if (!uiHandle) return;

    auto ui = UIHandle::cast(uiHandle);
    ui->release();
}