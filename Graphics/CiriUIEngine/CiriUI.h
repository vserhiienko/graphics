#pragma once

#include <Windows.h>

#define _EmbeddedChromeUI_Version 0x00010001
#if _EmbeddedChromeUI_API_Export
#define _EmbeddedChromeUI_API __declspec(dllexport)
#else
#define _EmbeddedChromeUI_API __declspec(dllimport)
#endif

#define _EmbeddedChromeUI_StringifyFunction(exp) static char const * exp##FuncName = "" #exp "";
_EmbeddedChromeUI_StringifyFunction(ciri_ui_getVersion);
_EmbeddedChromeUI_StringifyFunction(ciri_ui_initialize);
_EmbeddedChromeUI_StringifyFunction(ciri_ui_getBrowserWindowHandle);
_EmbeddedChromeUI_StringifyFunction(ciri_ui_handleMessage);
_EmbeddedChromeUI_StringifyFunction(ciri_ui_onFrameMove);
_EmbeddedChromeUI_StringifyFunction(ciri_ui_release);

extern "C"
{
    _EmbeddedChromeUI_API HANDLE ciri_ui_initialize(
        _In_ HWND windowHandle,
        _In_ HINSTANCE instanceHandle,
        _In_ char const *startupUrl
		);
	_EmbeddedChromeUI_API HWND ciri_ui_getBrowserWindowHandle(
		_Inout_ HANDLE uiHandle
		);
    _EmbeddedChromeUI_API BOOL ciri_ui_handleMessage(
        _Inout_ HANDLE uiHandle,
        _Inout_ HANDLE message
        );
	_EmbeddedChromeUI_API BOOL ciri_ui_onFrameMove(
        _Inout_ HANDLE uiHandle,
        _Inout_ HANDLE message
        );
    _EmbeddedChromeUI_API void ciri_ui_release(
        _Inout_ HANDLE ui
        );

    typedef HANDLE(*ciri_ui_initializeFuncPtr)(
        _In_ HWND windowHandle,
        _In_ HINSTANCE instanceHandle,
        _In_ char const *startupUrl
		);
	typedef HWND(*ciri_ui_getBrowserWindowHandleFuncPtr)(
		_Inout_ HANDLE uiHandle
		);
	typedef BOOL(*ciri_ui_handleMessageFuncPtr)(
        _Inout_ HANDLE uiHandle,
        _Inout_ HANDLE message
        );
	typedef BOOL(*ciri_ui_onFrameMoveFuncPtr)(
        _Inout_ HANDLE uiHandle,
        _Inout_ HANDLE message
        );
    typedef void(*ciri_ui_releaseFuncPtr)(
        _Inout_ HANDLE ui
        );

}



