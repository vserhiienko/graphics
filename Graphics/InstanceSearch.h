#pragma once

namespace Visco3D
{
    class MeshData;
    class InstanceSearch
    {
    public:
        class Result
        {
            struct Instance
            {
                size_t meshInstanceInd;
                mat4 transform;
            };

            std::map<size_t, std::vector<Instance>> instances;
        };

    public:

        void SearchIn(
            _In_ std::shared_ptr<Mesh> const *meshes,
            _In_ size_t mesheCount,
            _Outref_ Result &result
            );


    };
}

